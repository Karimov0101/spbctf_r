﻿#include<iostream>
using namespace std;
int main()
{
	uint64_t a = 0x93f3ffc2fbc7a1ce;
	uint64_t b = 6368891006275312830;//5862d8a05a385cbe
	cout << hex << a << '\n' << b << '\n';
	uint32_t c;// eax = fbc7a1ce = 4224164302
	uint32_t d;// ebx = 5a385cbe = 1513643198
	uint32_t e;
	cin >> c;
	cin >> d;
	__asm {

		mov eax, c
		mov ebx, d
		mul ebx
		xchg al, ah
		mov ebx, ebx
		lea ebx, [ebx + eax * 2]
		mov ecx, eax
		ror ebx, cl
		xor bx, ax
		and ebx, 0xffffff
		mov e, ebx
	}
	cout <<dec <<e;//e877f6 
	return 0;
}
/*mov rax, 0x93f3ffc2fbc7a1ce
mov rbx, 6368891006275312830
imul eax, ebx
xchg al, ah
mov ebx, ebx
lea ebx, [ebx + eax * 2]
mov ecx, eax
ror ebx, cl
xor bx, ax
and ebx, 0xffffff
mov rax, rbx*/