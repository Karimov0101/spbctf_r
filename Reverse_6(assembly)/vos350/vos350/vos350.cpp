﻿#include <iostream>
using namespace std;

int main()
{
	uint32_t c;//eax
	__asm {

		mov eax, 12345
		mov ecx, 11
		again:
		lea ebx, [ecx * 8 + 0xf00b42]
			xor eax, ebx
			mov edx, ecx
			and edx, 1
			test edx, edx
			jz lbl
			rol eax, cl
			jmp jumpout
			lbl :
		rol eax, 17
			jumpout :
			dec ecx
			test ecx, ecx
			jnz again
			and eax, 0x55555555
			mov c, eax
			//1078268933-flag
	}
	cout << c;
	return 0;
}
/*
mov ebx, 32767
rdtsc
xchg ebx, eax
xor eax, ebx
xor eax, edx
shl edx, 1
imul eax, 2
lea ecx, [ebx + ebx]
xor eax, 77731337
xor eax, ecx
xor ah, 77
xor ax, 31337
xor eax, edx*/
