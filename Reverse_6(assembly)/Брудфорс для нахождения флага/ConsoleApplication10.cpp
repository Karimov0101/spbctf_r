﻿#include <iostream>
using namespace std;

int main()

{
    unsigned int i;
    for (i = 0x30303030; i <= 0x7f7f7f7f; i++) {
        uint32_t temp = 0;// Переворачиваем i для xor 
        int f9 = (i & 0x000000FF);
        int f10 = (i & 0x0000FF00) >> 8;
        int f20 = (i & 0x00FF0000) >> 16; 
        int f30 = (i & 0xFF000000) >> 24;
        f9 <<= 24; f20 <<= 16; f10 <<= 8;
        temp |= f30 | f20 | f10 | f9;
        uint32_t first = 0xCAFEBABE ^ temp;
        __asm {//циклический сдвиг через асемблерные вставки
            ror first, 7
        }
        first ^= 0xDEADBEEF;
        __asm {
            rol first, 21
        }
        first ^= 0x13373389;
        //cout << hex << temp;
        uint32_t m = temp ^ first;
        int f = (m & 0x000000FF);//разбите на байты числа temp и последущее сравнение с ответом 
        int f1 = (m & 0x0000FF00) >> 8;
        int f2 = (m & 0x00FF0000) >> 16;
        int f3 = (m & 0xFF000000) >> 24;
        if (f == 0xA2)
        {
            if (f1 == 0x47)
            {
                if (f2 == 0x55)
                {
                    if (f3 == 0x39) {
                        cout << hex << temp << '\n';

                    }
                }
            }
        }

    }
}



//ответ yAhI