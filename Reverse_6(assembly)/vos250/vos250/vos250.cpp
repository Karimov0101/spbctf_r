﻿#include <iostream>
using namespace std;

int main()
{
	uint32_t c;//eax
	__asm {
		mov ebx, 32767
		rdtsc
		xchg ebx, eax
		xor eax, ebx
		xor eax, edx
		shl edx, 1
		imul eax, 2
		lea ecx, [ebx + ebx]
		xor eax, 77731337
		xor eax, ecx
		xor ah, 77
		xor ax, 31337
		xor eax, edx
		mov c, eax
	}
	cout << c;
	return 0;
}
/*
mov ebx, 32767
rdtsc
xchg ebx, eax
xor eax, ebx
xor eax, edx
shl edx, 1
imul eax, 2
lea ecx, [ebx + ebx]
xor eax, 77731337
xor eax, ecx
xor ah, 77
xor ax, 31337
xor eax, edx*/