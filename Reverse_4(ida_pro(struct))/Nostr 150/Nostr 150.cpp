#include <iostream>
#include <cstring>
int third_func(int a1)
{
  int v2; // [esp+0h] [ebp-4h]

  if ( a1 <= 2 )
    return 1;
  v2 = third_func(a1 - 1);
  return third_func(a1 - 2) + v2 + 1;
}
int main ()
{
  long long unsigned int  v4 = third_func(43);
  std::cout<<v4;
  return 0;
}
