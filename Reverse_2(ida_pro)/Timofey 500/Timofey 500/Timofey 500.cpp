﻿// Timofey 500.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>
using namespace std;
int swaper(char *a1, int a2)
{
	__int64 result; // rax
	char v3; // [rsp+13h] [rbp-Dh]
	int i; // [rsp+14h] [rbp-Ch]
	size_t v5; // [rsp+18h] [rbp-8h]

	v5 = strlen(a1);
	for (i = 0; ; i += a2)
	{
		result = i;
		if (i >= v5 - a2)
			break;
		v3 = a1[i - 1 + a2];      //v3=0 ==> стало v3=9
		a1[i - 1 + a2] = a1[i];   //(было)0=9  ==> стало 9=0
		a1[i] = v3;               //(было)9=0  ==> стало 0=9    
	}
	return result;
}

int main()
{
	char dest[136] ="_A_m_aFneSuyn_w_IypvSr_ahIEwp_gitrnMhaetevT_so!";
	//cout << static_cast<int>('_');
	cout << static_cast<int>(swaper(dest, 2)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 3)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 4)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 5)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 6)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 7)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 8)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 9)) << ' ' << dest << '\n';
	cout << static_cast<int>(swaper(dest, 10))<<' ' << dest << '\n';
	//меняем порядок изменения строки
	//I_Am_Funny_Swaper_I_Swap_Everything_That_Moves!
}