﻿#include<iostream>
#include <iomanip>
#include<math.h>
using namespace std;
int main()
{ 
	const char *a = "spbctf{1_d0_h4t3_m4th}";
	double b{};
	double res;
	int res1 = 0;
	int v3[22];
	v3[0] = 3089;  v3[1] = 9659;  v3[2] = 174;   v3[3] = 9993;
	v3[4] = 3255;  v3[5] = 9961;  v3[6] = 5876;  v3[7] = 8910;
	v3[8] = 6155;  v3[9] = 9986;  v3[10] = 4382; v3[11] = 7880;
	v3[12] = 1218; v3[13] = 8660; v3[14] = 3255; v3[15] = 8746;
	v3[16] = 6155; v3[17] = 9781; v3[18] = 4999; v3[19] = 9455;
	v3[20] = 1218; v3[21] = 7987;

	for (int j = 0; j < 22; j++)
	{
		if (j % 2 == 0) { //цикл для нахождения sin()
			for (double i = 0.0001; i < 2.0; i += 0.0001)
			{

				if (sin(i) < (v3[j] / 10000.0 + 0.0001) && sin(i) > (v3[j] / 10000.0 - 0.0001)) {
					//cout << i;
					b = i + 0.0001;
					//cout << b;
					//cout << "    v3[" << j << "] = " << static_cast<int>(sin(b)*10000.0);
					break;
				}
			}
		}
		else if (j % 2 == 1) //цикл для нахождения значения для cos
		{
			for (double i = 0.0001; i < 2.0; i += 0.0001)
			{

				if (cos(i) < (v3[j] / 10000.0 + 0.0001) && cos(i) > (v3[j] / 10000.0 - 0.0001)) {
					//cout << i;
					b = i + 0.0001;
					//cout << b;
					//cout<<"    v3["<<j<<"] = "<< static_cast<int>(cos(b)*10000.0);
					break;
				}
			}
		}

		res = b / 3.141 * 180.0;
		if (res + 0.4 >= static_cast<int>(res + 0.4)) //процесс округления  
			res1 = static_cast<int>(res + 0.4);
		else
			res1 = static_cast<int>(res);

		//cout <<"   res1:=" << res1<<'\n';

		if (res1 > 0 && res1 <= 25)
			cout << static_cast<char>(res1 + 97);
		else if (res1 > 25 && res1 <= 35)
			cout << static_cast<char>(res1 + 22);
		switch (res1)
		{
		case 36: cout << "{";break;
		case 37: cout << "}";break;
		case 38: cout << "_";break;
		}
	
	}
	return 0;
}