import java.math.BigInteger;
public class Main
{
	public static void main(String[] args) {    
        BigInteger a =  BigInteger.valueOf(3);
        BigInteger b=  BigInteger.valueOf(0);
        BigInteger c=  BigInteger.valueOf(-1);
        
        BigInteger a1 = a.multiply(BigInteger.valueOf(5));
        BigInteger b1 = b.multiply(BigInteger.valueOf(4));
        BigInteger c1 = c.multiply(BigInteger.valueOf(4));
        if(!(a1.add(b1).add(c1)).equals(BigInteger.valueOf(11)))
             return;
        a1 = a.multiply(BigInteger.valueOf(4));
        b1 = b.multiply(BigInteger.valueOf(5));
        c1 = c.multiply(BigInteger.valueOf(4));
        if(!(a1.add(b1).add(c1)).equals(BigInteger.valueOf(8)))
             return;
        a1 = a.multiply(BigInteger.valueOf(4));
        b1 = b.multiply(BigInteger.valueOf(4));
        c1 = c.multiply(BigInteger.valueOf(5));
        if(!(a1.add(b1).add(c1)).equals(BigInteger.valueOf(7)))
             return;
        
         BigInteger d = b.ONE;
         BigInteger h1= new BigInteger("cafebabedeadbeef",16).multiply(d).multiply(a).multiply(c);
         System.out.println(h1);
       
         System.out.printf("bingo spbctf{%s}\n",(Object)h1);
	}
}
