.class public Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;
.super Landroid/view/View$BaseSavedState;
.source "HorizontalPicker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wefika/horizontalpicker/HorizontalPicker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SavedState"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mSelItem:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1232
    new-instance v0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState$1;

    invoke-direct {v0}, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState$1;-><init>()V

    sput-object v0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 1212
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcel;)V

    .line 1213
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->mSelItem:I

    .line 1214
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/wefika/horizontalpicker/HorizontalPicker$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/wefika/horizontalpicker/HorizontalPicker$1;

    .prologue
    .line 1203
    invoke-direct {p0, p1}, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;

    .prologue
    .line 1208
    invoke-direct {p0, p1}, Landroid/view/View$BaseSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 1209
    return-void
.end method

.method static synthetic access$100(Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;)I
    .locals 1
    .param p0, "x0"    # Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;

    .prologue
    .line 1203
    iget v0, p0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->mSelItem:I

    return v0
.end method

.method static synthetic access$102(Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;
    .param p1, "x1"    # I

    .prologue
    .line 1203
    iput p1, p0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->mSelItem:I

    return p1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "HorizontalPicker.SavedState{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1226
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " selItem="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->mSelItem:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 1218
    invoke-super {p0, p1, p2}, Landroid/view/View$BaseSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 1220
    iget v0, p0, Lcom/wefika/horizontalpicker/HorizontalPicker$SavedState;->mSelItem:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 1221
    return-void
.end method
