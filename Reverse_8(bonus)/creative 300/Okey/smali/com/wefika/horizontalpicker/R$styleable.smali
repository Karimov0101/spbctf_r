.class public final Lcom/wefika/horizontalpicker/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/wefika/horizontalpicker/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final HorizontalPicker:[I

.field public static final HorizontalPicker_android_ellipsize:I = 0x2

.field public static final HorizontalPicker_android_marqueeRepeatLimit:I = 0x3

.field public static final HorizontalPicker_android_textColor:I = 0x1

.field public static final HorizontalPicker_android_textSize:I = 0x0

.field public static final HorizontalPicker_dividerSize:I = 0x4

.field public static final HorizontalPicker_sideItems:I = 0x5

.field public static final HorizontalPicker_values:I = 0x6


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/wefika/horizontalpicker/R$styleable;->HorizontalPicker:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x1010095
        0x1010098
        0x10100ab
        0x101021d
        0x7f040099
        0x7f04019b
        0x7f0401f8
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
