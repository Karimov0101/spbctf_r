.class final Lcom/hannesdorfmann/mosby3/PresenterManager$1;
.super Ljava/lang/Object;
.source "PresenterManager.java"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/hannesdorfmann/mosby3/PresenterManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    if-eqz p2, :cond_0

    .line 47
    const-string v1, "com.hannesdorfmann.mosby3.MosbyPresenterManagerActivityId"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "activityId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 51
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$000()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    .end local v0    # "activityId":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onActivityDestroyed(Landroid/app/Activity;)V
    .locals 4
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v2

    if-nez v2, :cond_1

    .line 80
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$000()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 81
    .local v0, "activityId":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 82
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$100()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    .line 83
    .local v1, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    if-eqz v1, :cond_0

    .line 84
    invoke-virtual {v1}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->clear()V

    .line 85
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$100()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    :cond_0
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$100()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    sget-object v3, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    .line 92
    invoke-virtual {v2, v3}, Landroid/app/Application;->unregisterActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 93
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 94
    const-string v2, "PresenterManager"

    const-string v3, "Unregistering ActivityLifecycleCallbacks"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    .end local v0    # "activityId":Ljava/lang/String;
    .end local v1    # "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    :cond_1
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$000()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    return-void
.end method

.method public onActivityPaused(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 72
    return-void
.end method

.method public onActivityResumed(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 68
    return-void
.end method

.method public onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-static {}, Lcom/hannesdorfmann/mosby3/PresenterManager;->access$000()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 59
    .local v0, "activityId":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 60
    const-string v1, "com.hannesdorfmann.mosby3.MosbyPresenterManagerActivityId"

    invoke-virtual {p2, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    return-void
.end method

.method public onActivityStarted(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 65
    return-void
.end method

.method public onActivityStopped(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 75
    return-void
.end method
