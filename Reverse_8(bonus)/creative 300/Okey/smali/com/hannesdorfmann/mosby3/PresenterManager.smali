.class public final Lcom/hannesdorfmann/mosby3/PresenterManager;
.super Ljava/lang/Object;
.source "PresenterManager.java"


# static fields
.field public static DEBUG:Z = false

.field public static final DEBUG_TAG:Ljava/lang/String; = "PresenterManager"

.field static final KEY_ACTIVITY_ID:Ljava/lang/String; = "com.hannesdorfmann.mosby3.MosbyPresenterManagerActivityId"

.field private static final activityIdMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field static final activityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

.field private static final activityScopedCacheMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/hannesdorfmann/mosby3/ActivityScopedCache;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->DEBUG:Z

    .line 40
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    .line 41
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    sput-object v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    .line 43
    new-instance v0, Lcom/hannesdorfmann/mosby3/PresenterManager$1;

    invoke-direct {v0}, Lcom/hannesdorfmann/mosby3/PresenterManager$1;-><init>()V

    sput-object v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not instantiatable!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$000()Ljava/util/Map;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic access$100()Ljava/util/Map;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    return-object v0
.end method

.method public static getActivity(Landroid/content/Context;)Landroid/app/Activity;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 216
    if-nez p0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 219
    :cond_0
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    .line 220
    check-cast p0, Landroid/app/Activity;

    .line 225
    .end local p0    # "context":Landroid/content/Context;
    :goto_0
    return-object p0

    .line 227
    .restart local p0    # "context":Landroid/content/Context;
    :cond_1
    check-cast p0, Landroid/content/ContextWrapper;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object p0

    .line 223
    .restart local p0    # "context":Landroid/content/Context;
    :cond_2
    instance-of v0, p0, Landroid/content/ContextWrapper;

    if-eqz v0, :cond_3

    .line 224
    instance-of v0, p0, Landroid/app/Activity;

    if-eqz v0, :cond_1

    .line 225
    check-cast p0, Landroid/app/Activity;

    goto :goto_0

    .line 229
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Could not find the surrounding Activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static getActivityScope(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 152
    if-nez p0, :cond_0

    .line 153
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 155
    :cond_0
    sget-object v1, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 156
    .local v0, "activityId":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 157
    const/4 v1, 0x0

    .line 160
    :goto_0
    return-object v1

    :cond_1
    sget-object v1, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    goto :goto_0
.end method

.method static getOrCreateActivityScopedCache(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/MainThread;
    .end annotation

    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 115
    if-nez p0, :cond_0

    .line 116
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Activity is null"

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 119
    :cond_0
    sget-object v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    invoke-interface {v2, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 120
    .local v0, "activityId":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 122
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    .line 123
    sget-object v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 127
    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v2

    sget-object v3, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityLifecycleCallbacks:Landroid/app/Application$ActivityLifecycleCallbacks;

    invoke-virtual {v2, v3}, Landroid/app/Application;->registerActivityLifecycleCallbacks(Landroid/app/Application$ActivityLifecycleCallbacks;)V

    .line 128
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 129
    const-string v2, "PresenterManager"

    const-string v3, "Registering ActivityLifecycleCallbacks"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :cond_1
    sget-object v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    .line 135
    .local v1, "activityScopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    if-nez v1, :cond_2

    .line 136
    new-instance v1, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    .end local v1    # "activityScopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    invoke-direct {v1}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;-><init>()V

    .line 137
    .restart local v1    # "activityScopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    sget-object v2, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    :cond_2
    return-object v1
.end method

.method public static getPresenter(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ")TP;"
        }
    .end annotation

    .prologue
    .line 173
    if-nez p0, :cond_0

    .line 174
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 177
    :cond_0
    if-nez p1, :cond_1

    .line 178
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "View id is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 181
    :cond_1
    invoke-static {p0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivityScope(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    move-result-object v0

    .line 182
    .local v0, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    if-nez v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_2
    invoke-virtual {v0, p1}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->getPresenter(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public static getViewState(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<VS:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            ")TVS;"
        }
    .end annotation

    .prologue
    .line 196
    if-nez p0, :cond_0

    .line 197
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 200
    :cond_0
    if-nez p1, :cond_1

    .line 201
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "View id is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 204
    :cond_1
    invoke-static {p0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivityScope(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    move-result-object v0

    .line 205
    .local v0, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    if-nez v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_2
    invoke-virtual {v0, p1}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->getViewState(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public static putPresenter(Landroid/app/Activity;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
            "<+",
            "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    .local p2, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter<+Lcom/hannesdorfmann/mosby3/mvp/MvpView;>;"
    if-nez p0, :cond_0

    .line 254
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 257
    :cond_0
    invoke-static {p0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getOrCreateActivityScopedCache(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    move-result-object v0

    .line 258
    .local v0, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    invoke-virtual {v0, p1, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->putPresenter(Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 259
    return-void
.end method

.method public static putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewState"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 270
    if-nez p0, :cond_0

    .line 271
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 274
    :cond_0
    invoke-static {p0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getOrCreateActivityScopedCache(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    move-result-object v0

    .line 275
    .local v0, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    invoke-virtual {v0, p1, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->putViewState(Ljava/lang/String;Ljava/lang/Object;)V

    .line 276
    return-void
.end method

.method public static remove(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 286
    if-nez p0, :cond_0

    .line 287
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Activity is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 290
    :cond_0
    invoke-static {p0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivityScope(Landroid/app/Activity;)Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    move-result-object v0

    .line 291
    .local v0, "activityScope":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    if-eqz v0, :cond_1

    .line 292
    invoke-virtual {v0, p1}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->remove(Ljava/lang/String;)V

    .line 294
    :cond_1
    return-void
.end method

.method static reset()V
    .locals 3

    .prologue
    .line 236
    sget-object v1, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityIdMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 237
    sget-object v1, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v0, "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    check-cast v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;

    .line 238
    .restart local v0    # "scopedCache":Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
    invoke-virtual {v0}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->clear()V

    goto :goto_0

    .line 241
    :cond_0
    sget-object v1, Lcom/hannesdorfmann/mosby3/PresenterManager;->activityScopedCacheMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 242
    return-void
.end method
