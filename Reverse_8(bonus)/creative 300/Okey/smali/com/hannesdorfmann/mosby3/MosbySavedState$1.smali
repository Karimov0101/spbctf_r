.class final Lcom/hannesdorfmann/mosby3/MosbySavedState$1;
.super Ljava/lang/Object;
.source "MosbySavedState.java"

# interfaces
.implements Landroid/support/v4/os/ParcelableCompatCreatorCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/hannesdorfmann/mosby3/MosbySavedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/os/ParcelableCompatCreatorCallbacks",
        "<",
        "Lcom/hannesdorfmann/mosby3/MosbySavedState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/hannesdorfmann/mosby3/MosbySavedState;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 36
    if-nez p2, :cond_0

    .line 37
    const-class v0, Lcom/hannesdorfmann/mosby3/MosbySavedState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    .line 39
    :cond_0
    new-instance v0, Lcom/hannesdorfmann/mosby3/MosbySavedState;

    invoke-direct {v0, p1, p2}, Lcom/hannesdorfmann/mosby3/MosbySavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/MosbySavedState$1;->createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/hannesdorfmann/mosby3/MosbySavedState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/hannesdorfmann/mosby3/MosbySavedState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 43
    new-array v0, p1, [Lcom/hannesdorfmann/mosby3/MosbySavedState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/hannesdorfmann/mosby3/MosbySavedState$1;->newArray(I)[Lcom/hannesdorfmann/mosby3/MosbySavedState;

    move-result-object v0

    return-object v0
.end method
