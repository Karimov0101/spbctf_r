.class final Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
.super Ljava/lang/Object;
.source "ActivityScopedCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "PresenterHolder"
.end annotation


# instance fields
.field private presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
            "<*>;"
        }
    .end annotation
.end field

.field private viewState:Ljava/lang/Object;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 162
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$002(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 0
    .param p0, "x0"    # Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    .param p1, "x1"    # Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object p1
.end method

.method static synthetic access$100(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->viewState:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$102(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 162
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->viewState:Ljava/lang/Object;

    return-object p1
.end method
