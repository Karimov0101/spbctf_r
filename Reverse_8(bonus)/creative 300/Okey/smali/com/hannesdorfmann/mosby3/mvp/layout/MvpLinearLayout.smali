.class public abstract Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;
.super Landroid/widget/LinearLayout;
.source "MvpLinearLayout.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Landroid/widget/LinearLayout;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback",
        "<TV;TP;>;"
    }
.end annotation


# instance fields
.field protected mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation
.end field

.field protected presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private retainInstance:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->retainInstance:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->retainInstance:Z

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 53
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->retainInstance:Z

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->retainInstance:Z

    .line 59
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->isInEditMode()Z

    .line 60
    return-void
.end method


# virtual methods
.method public abstract createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation
.end method

.method protected getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;-><init>(Landroid/view/View;Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;Z)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    return-object v0
.end method

.method public getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    return-object p0
.end method

.method public getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 111
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 85
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/LinearLayout;->onAttachedToWindow()V

    .line 86
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onAttachedToWindow()V

    .line 87
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 90
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 91
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onDetachedFromWindow()V

    .line 92
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 100
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 101
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 95
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    .prologue
    .line 115
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    .local p1, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 116
    return-void
.end method

.method public final superOnRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 127
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 128
    return-void
.end method

.method public final superOnSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 123
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpLinearLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
