.class final Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState$1;
.super Ljava/lang/Object;
.source "MosbyViewStateSavedState.java"

# interfaces
.implements Landroid/support/v4/os/ParcelableCompatCreatorCallbacks;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/support/v4/os/ParcelableCompatCreatorCallbacks",
        "<",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 39
    if-nez p2, :cond_0

    .line 40
    const-class v0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object p2

    .line 42
    :cond_0
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    invoke-direct {v0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    return-object v0
.end method

.method public bridge synthetic createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState$1;->createFromParcel(Landroid/os/Parcel;Ljava/lang/ClassLoader;)Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    move-result-object v0

    return-object v0
.end method

.method public newArray(I)[Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;
    .locals 1
    .param p1, "size"    # I

    .prologue
    .line 46
    new-array v0, p1, [Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    return-object v0
.end method

.method public bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState$1;->newArray(I)[Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    move-result-object v0

    return-object v0
.end method
