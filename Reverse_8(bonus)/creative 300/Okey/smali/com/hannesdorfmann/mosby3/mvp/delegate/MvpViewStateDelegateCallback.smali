.class public interface abstract Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;
.super Ljava/lang/Object;
.source "MvpViewStateDelegateCallback.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
        "<TV;TP;>;"
    }
.end annotation


# virtual methods
.method public abstract createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation
.end method

.method public abstract getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation
.end method

.method public abstract isRestoringViewState()Z
.end method

.method public abstract onNewViewStateInstance()V
.end method

.method public abstract onViewStateInstanceRestored(Z)V
.end method

.method public abstract setRestoringViewState(Z)V
.end method

.method public abstract setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation
.end method
