.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;
.super Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;
.source "FragmentMvpViewStateDelegateImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl",
        "<TV;TP;>;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "FragmentMvpDelegateImpl"


# instance fields
.field private applyViewState:Z

.field private applyViewStateFromMemory:Z

.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;ZZ)V
    .locals 1
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
    .param p3, "keepPresenterAndViewStateDuringScreenOrientationChange"    # Z
    .param p4, "keepPresenterAndViewStateOnBackstack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;ZZ)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback<TV;TP;TVS;>;"
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;-><init>(Landroid/support/v4/app/Fragment;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;ZZ)V

    .line 41
    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewState:Z

    .line 42
    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    .line 50
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 51
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 150
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onActivityCreated(Landroid/os/Bundle;)V

    .line 152
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewState:Z

    if-eqz v2, :cond_1

    .line 153
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 154
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    .line 155
    .local v0, "mvpView":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    if-nez v1, :cond_0

    .line 156
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from getViewState() is null! MvpView "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 160
    :cond_0
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 161
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    invoke-interface {v1, v0, v2}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;->apply(Lcom/hannesdorfmann/mosby3/mvp/MvpView;Z)V

    .line 162
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 163
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    invoke-interface {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->onViewStateInstanceRestored(Z)V

    .line 167
    .end local v0    # "mvpView":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    :goto_0
    return-void

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->onNewViewStateInstance()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 170
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 172
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->retainPresenterInstance()Z

    move-result v0

    .line 173
    .local v0, "keepInstance":Z
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 174
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v1, :cond_0

    .line 175
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from getViewState() is null! The MvpView that has returned null in getViewState() is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 177
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 180
    :cond_0
    if-eqz v0, :cond_1

    instance-of v2, v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    if-eqz v2, :cond_1

    .line 181
    check-cast v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    invoke-interface {v1, p1}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;->saveInstanceState(Landroid/os/Bundle;)V

    .line 183
    :cond_1
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 54
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 56
    if-eqz p2, :cond_0

    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-eqz v2, :cond_0

    .line 58
    const-string v2, "com.hannesdorfmann.mosby3.fragment.mvp.id"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 60
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 61
    const-string v2, "FragmentMvpDelegateImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MosbyView ID = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for MvpView: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 62
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 61
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :cond_0
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 67
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getViewState(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 68
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-eqz v1, :cond_2

    .line 72
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V

    .line 73
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewState:Z

    .line 74
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    .line 75
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 76
    const-string v2, "FragmentMvpDelegateImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState reused from Mosby internal cache for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 77
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 76
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_1
    :goto_0
    return-void

    .line 86
    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    :cond_2
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 87
    .restart local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v1, :cond_3

    .line 88
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from createViewState() is null! MvpView that has returned null as ViewState is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 90
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 93
    :cond_3
    if-eqz p2, :cond_6

    instance-of v2, v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    if-eqz v2, :cond_6

    move-object v2, v1

    .line 96
    check-cast v2, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    .line 97
    invoke-interface {v2, p2}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;->restoreInstanceState(Landroid/os/Bundle;)Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    move-result-object v0

    .line 99
    .local v0, "restoredViewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;
    if-eqz v0, :cond_6

    .line 103
    move-object v1, v0

    .line 104
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V

    .line 105
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewState:Z

    .line 106
    iput-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    .line 108
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-eqz v2, :cond_5

    .line 109
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 110
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The (internal) Mosby View id is null although bundle is not null. This should never happen. This seems to be a Mosby internal error. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 113
    :cond_4
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 116
    :cond_5
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 117
    const-string v2, "FragmentMvpDelegateImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Recreated ViewState from bundle for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 118
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 117
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 130
    .end local v0    # "restoredViewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;
    :cond_6
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-eqz v2, :cond_8

    .line 131
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 132
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The (internal) Mosby View id is null. This should never happen. This seems to be a Mosby internal error. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 135
    :cond_7
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 137
    :cond_8
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V

    .line 138
    iput-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewState:Z

    .line 139
    iput-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->applyViewStateFromMemory:Z

    .line 141
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_1

    .line 142
    const-string v2, "FragmentMvpDelegateImpl"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Created a new ViewState instance for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 143
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 142
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
