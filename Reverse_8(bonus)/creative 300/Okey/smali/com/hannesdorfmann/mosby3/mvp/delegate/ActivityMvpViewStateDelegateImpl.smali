.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;
.super Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;
.source "ActivityMvpViewStateDelegateImpl.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl",
        "<TV;TP;>;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "ActivityMvpViewStateDel"


# instance fields
.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;Z)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p3, "keepPresenterAndViewState"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;Z)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback<TV;TP;TVS;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;-><init>(Landroid/app/Activity;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;Z)V

    .line 53
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 54
    return-void
.end method

.method private setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;ZZ)V
    .locals 2
    .param p1    # Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "applyViewState"    # Z
    .param p3, "applyViewStateFromMemory"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;ZZ)V"
        }
    .end annotation

    .prologue
    .line 59
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    .local p1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Oops, viewState is null! This seems to be a Mosby internal bug. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V

    .line 66
    if-eqz p2, :cond_1

    .line 67
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 68
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v0

    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 69
    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;->apply(Lcom/hannesdorfmann/mosby3/mvp/MvpView;Z)V

    .line 70
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 71
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v0, p3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->onViewStateInstanceRestored(Z)V

    .line 73
    :cond_1
    return-void
.end method


# virtual methods
.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 76
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->onPostCreate(Landroid/os/Bundle;)V

    .line 78
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 79
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getViewState(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 80
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-eqz v1, :cond_1

    .line 84
    invoke-direct {p0, v1, v5, v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;ZZ)V

    .line 85
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 86
    const-string v2, "ActivityMvpViewStateDel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState reused from Mosby internal cache for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 87
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 86
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 96
    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    :cond_1
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 97
    .restart local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v1, :cond_2

    .line 98
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from createViewState() is null! MvpView that has returned null as ViewState is: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 100
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 103
    :cond_2
    if-eqz p1, :cond_5

    instance-of v2, v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    if-eqz v2, :cond_5

    move-object v2, v1

    .line 106
    check-cast v2, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    .line 107
    invoke-interface {v2, p1}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;->restoreInstanceState(Landroid/os/Bundle;)Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    move-result-object v0

    .line 109
    .local v0, "restoredViewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;
    if-eqz v0, :cond_5

    .line 113
    move-object v1, v0

    .line 114
    invoke-direct {p0, v1, v5, v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;ZZ)V

    .line 115
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->keepPresenterInstance:Z

    if-eqz v2, :cond_4

    .line 116
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 117
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The (internal) Mosby View id is null although bundle is not null. This should never happen. This seems to be a Mosby internal error. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 120
    :cond_3
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 123
    :cond_4
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 124
    const-string v2, "ActivityMvpViewStateDel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Recreated ViewState from bundle for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 125
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 124
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 137
    .end local v0    # "restoredViewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;
    :cond_5
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->keepPresenterInstance:Z

    if-eqz v2, :cond_7

    .line 138
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 139
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "The (internal) Mosby View id is null. This should never happen. This seems to be a Mosby internal error. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 142
    :cond_6
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 144
    :cond_7
    invoke-direct {p0, v1, v4, v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;ZZ)V

    .line 146
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_8

    .line 147
    const-string v2, "ActivityMvpViewStateDel"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Created a new ViewState instance for view: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 148
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " viewState: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 147
    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_8
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->onNewViewStateInstance()V

    goto/16 :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 157
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 159
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->keepPresenterInstance:Z

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    invoke-static {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v0

    .line 160
    .local v0, "keepInstance":Z
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 161
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v1, :cond_0

    .line 162
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from getViewState() is null! The MvpView that has returned null in getViewState() is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;

    .line 164
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 167
    :cond_0
    if-eqz v0, :cond_1

    instance-of v2, v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    if-eqz v2, :cond_1

    .line 168
    check-cast v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;

    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    invoke-interface {v1, p1}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableViewState;->saveInstanceState(Landroid/os/Bundle;)V

    .line 170
    :cond_1
    return-void
.end method
