.class public abstract Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "MvpViewStateFragment.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<TV;TP;>;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
        "<TV;TP;TVS;>;"
    }
.end annotation


# instance fields
.field private restoringViewState:Z

.field protected viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TVS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->restoringViewState:Z

    return-void
.end method


# virtual methods
.method protected getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    const/4 v1, 0x1

    .line 55
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate;

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;

    invoke-direct {v0, p0, p0, v1, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpViewStateDelegateImpl;-><init>(Landroid/support/v4/app/Fragment;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;ZZ)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate;

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate;

    return-object v0
.end method

.method public getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation

    .prologue
    .line 63
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    return-object v0
.end method

.method public isRestoringViewState()Z
    .locals 1

    .prologue
    .line 75
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->restoringViewState:Z

    return v0
.end method

.method public onViewStateInstanceRestored(Z)V
    .locals 0
    .param p1, "instanceStateRetained"    # Z

    .prologue
    .line 80
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    return-void
.end method

.method public setRestoringViewState(Z)V
    .locals 0
    .param p1, "restoringViewState"    # Z

    .prologue
    .line 71
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    iput-boolean p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->restoringViewState:Z

    .line 72
    return-void
.end method

.method public setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment<TV;TP;TVS;>;"
    .local p1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateFragment;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 68
    return-void
.end method
