.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;
.super Ljava/lang/Object;
.source "ViewGroupMvpDelegateImpl.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
        "<TV;TP;>;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "ViewGroupMvpDelegateImp"


# instance fields
.field private final activity:Landroid/app/Activity;

.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback",
            "<TV;TP;>;"
        }
    .end annotation
.end field

.field private final isInEditMode:Z

.field private final keepPresenterDuringScreenOrientationChange:Z

.field private mosbyViewId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "keepPresenterDuringScreenOrientationChange"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback",
            "<TV;TP;>;Z)V"
        }
    .end annotation

    .prologue
    .line 64
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback<TV;TP;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    if-nez p1, :cond_0

    .line 66
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "View is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    if-nez p2, :cond_1

    .line 70
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "MvpDelegateCallback is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    .line 74
    iput-boolean p3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    .line 76
    invoke-virtual {p1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    .line 78
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    if-nez v0, :cond_2

    .line 79
    invoke-interface {p2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    .line 83
    :goto_0
    return-void

    .line 81
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    goto :goto_0
.end method

.method private getContext()Landroid/content/Context;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 107
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 108
    .local v0, "c":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 109
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Context returned from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_0
    return-object v0
.end method

.method private restoreSavedState(Lcom/hannesdorfmann/mosby3/MosbySavedState;)V
    .locals 1
    .param p1, "state"    # Lcom/hannesdorfmann/mosby3/MosbySavedState;

    .prologue
    .line 277
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    invoke-virtual {p1}, Lcom/hannesdorfmann/mosby3/MosbySavedState;->getMosbyViewId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 278
    return-void
.end method


# virtual methods
.method protected createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    .line 95
    .local v1, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v1, :cond_0

    .line 96
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Presenter returned from createPresenter() is null."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 98
    :cond_0
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    if-eqz v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 100
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 101
    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putPresenter(Landroid/app/Activity;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 103
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    return-object v1
.end method

.method public onAttachedToWindow()V
    .locals 5

    .prologue
    .line 115
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    if-eqz v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    const/4 v0, 0x0

    .line 118
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 121
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 122
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 123
    const-string v2, "ViewGroupMvpDelegateImp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "new Presenter instance created: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    .line 145
    .local v1, "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    if-nez v1, :cond_5

    .line 146
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MvpView returned from getMvpView() is null. Returned by "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    .end local v1    # "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    :cond_3
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getPresenter(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 127
    .restart local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_4

    .line 130
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 131
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 132
    const-string v2, "ViewGroupMvpDelegateImp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No Presenter instance found in cache, although MosbyView ID present. This was caused by process death, therefore new Presenter instance created: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 137
    :cond_4
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_2

    .line 138
    const-string v2, "ViewGroupMvpDelegateImp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Presenter instance reused from internal cache: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 150
    .restart local v1    # "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    :cond_5
    if-nez v0, :cond_6

    .line 151
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Oops, Presenter is null. This seems to be a Mosby internal bug. Please report this issue here: https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 161
    :cond_6
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v2, v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 162
    invoke-interface {v0, v1}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V

    .line 170
    sget-boolean v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v2, :cond_0

    .line 171
    const-string v2, "ViewGroupMvpDelegateImp"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MvpView attached to Presenter. MvpView: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "   Presenter: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 8

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 178
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    if-eqz v5, :cond_1

    .line 254
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v2

    .line 181
    .local v2, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v2, :cond_2

    .line 182
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Presenter returned from delegateCallback.getPresenter() is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 186
    :cond_2
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    if-eqz v5, :cond_8

    .line 188
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    invoke-static {v5, v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v5

    if-nez v5, :cond_5

    move v0, v3

    .line 191
    .local v0, "destroyedPermanently":Z
    :goto_1
    if-eqz v0, :cond_6

    .line 194
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 195
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    .line 196
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and removing presenter permanently from internal cache because the hosting Activity will be destroyed permanently"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 195
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 202
    :cond_3
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 204
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 206
    :cond_4
    iput-object v7, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 207
    invoke-interface {v2, v4}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    goto :goto_0

    .end local v0    # "destroyedPermanently":Z
    :cond_5
    move v0, v4

    .line 188
    goto :goto_1

    .line 209
    .restart local v0    # "destroyedPermanently":Z
    :cond_6
    iget-boolean v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    invoke-static {v4, v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v1

    .line 212
    .local v1, "detachedBecauseOrientationChange":Z
    if-eqz v1, :cond_0

    .line 214
    sget-boolean v4, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v4, :cond_7

    .line 215
    const-string v4, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    .line 216
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " temporarily because of orientation change"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 215
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_7
    invoke-interface {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    goto/16 :goto_0

    .line 239
    .end local v0    # "destroyedPermanently":Z
    .end local v1    # "detachedBecauseOrientationChange":Z
    :cond_8
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_9

    .line 240
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    .line 241
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " permanently"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 240
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    :cond_9
    invoke-interface {v2, v4}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    .line 248
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 250
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 252
    :cond_a
    iput-object v7, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 284
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    if-eqz v1, :cond_0

    .line 294
    :goto_0
    return-void

    .line 286
    :cond_0
    instance-of v1, p1, Lcom/hannesdorfmann/mosby3/MosbySavedState;

    if-nez v1, :cond_1

    .line 287
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v1, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->superOnRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 291
    check-cast v0, Lcom/hannesdorfmann/mosby3/MosbySavedState;

    .line 292
    .local v0, "savedState":Lcom/hannesdorfmann/mosby3/MosbySavedState;
    invoke-direct {p0, v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->restoreSavedState(Lcom/hannesdorfmann/mosby3/MosbySavedState;)V

    .line 293
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-virtual {v0}, Lcom/hannesdorfmann/mosby3/MosbySavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->superOnRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 260
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl<TV;TP;>;"
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->isInEditMode:Z

    if-eqz v1, :cond_1

    const/4 v0, 0x0

    .line 267
    :cond_0
    :goto_0
    return-object v0

    .line 262
    :cond_1
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;->superOnSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 264
    .local v0, "superState":Landroid/os/Parcelable;
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->keepPresenterDuringScreenOrientationChange:Z

    if-eqz v1, :cond_0

    .line 265
    new-instance v1, Lcom/hannesdorfmann/mosby3/MosbySavedState;

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Lcom/hannesdorfmann/mosby3/MosbySavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method
