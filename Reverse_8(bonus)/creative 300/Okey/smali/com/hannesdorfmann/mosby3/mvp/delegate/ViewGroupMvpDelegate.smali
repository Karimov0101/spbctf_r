.class public interface abstract Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
.super Ljava/lang/Object;
.source "ViewGroupMvpDelegate.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract onAttachedToWindow()V
.end method

.method public abstract onDetachedFromWindow()V
.end method

.method public abstract onRestoreInstanceState(Landroid/os/Parcelable;)V
.end method

.method public abstract onSaveInstanceState()Landroid/os/Parcelable;
.end method
