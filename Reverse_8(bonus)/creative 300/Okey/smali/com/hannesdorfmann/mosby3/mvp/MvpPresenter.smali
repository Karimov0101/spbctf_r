.class public interface abstract Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
.super Ljava/lang/Object;
.source "MvpPresenter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation
.end method

.method public abstract detachView(Z)V
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation
.end method
