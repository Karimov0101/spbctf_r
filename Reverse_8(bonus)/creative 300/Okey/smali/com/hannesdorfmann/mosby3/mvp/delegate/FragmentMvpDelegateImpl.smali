.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;
.super Ljava/lang/Object;
.source "FragmentMvpDelegateImpl.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegate",
        "<TV;TP;>;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "FragmentMvpVSDelegate"

.field protected static final KEY_MOSBY_VIEW_ID:Ljava/lang/String; = "com.hannesdorfmann.mosby3.fragment.mvp.id"


# instance fields
.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
            "<TV;TP;>;"
        }
    .end annotation
.end field

.field protected fragment:Landroid/support/v4/app/Fragment;

.field protected final keepPresenterInstanceDuringScreenOrientationChanges:Z

.field protected final keepPresenterOnBackstack:Z

.field protected mosbyViewId:Ljava/lang/String;

.field private onViewCreatedCalled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;ZZ)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "keepPresenterDuringScreenOrientationChange"    # Z
    .param p4, "keepPresenterOnBackstack"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/Fragment;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
            "<TV;TP;>;ZZ)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback<TV;TP;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onViewCreatedCalled:Z

    .line 68
    if-nez p2, :cond_0

    .line 69
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "MvpDelegateCallback is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_0
    if-nez p1, :cond_1

    .line 73
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Fragment is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_1
    if-nez p3, :cond_2

    if-eqz p4, :cond_2

    .line 77
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "It is not possible to keep the presenter on backstack, but NOT keep presenter through screen orientation changes. Keep presenter on backstack also requires keep presenter through screen orientation changes to be enabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_2
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    .line 83
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 84
    iput-boolean p3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    .line 86
    iput-boolean p4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterOnBackstack:Z

    .line 87
    return-void
.end method

.method private createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 98
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 99
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_0

    .line 100
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Presenter returned from createPresenter() is null. Activity is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 101
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 103
    :cond_0
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-eqz v1, :cond_1

    .line 104
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 105
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putPresenter(Landroid/app/Activity;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 107
    :cond_1
    return-object v0
.end method

.method private getActivity()Landroid/app/Activity;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 172
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 173
    .local v0, "activity":Landroid/app/Activity;
    if-nez v0, :cond_0

    .line 174
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Activity returned by Fragment.getActivity() is null. Fragment is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 178
    :cond_0
    return-object v0
.end method

.method private getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 190
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    .line 191
    .local v0, "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    if-nez v0, :cond_0

    .line 192
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "View returned from getMvpView() is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 194
    :cond_0
    return-object v0
.end method

.method private getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 182
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 183
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_0

    .line 184
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Presenter returned from getPresenter() is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 186
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 265
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 269
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "saved"    # Landroid/os/Bundle;

    .prologue
    .line 287
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 290
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onDestroyView()V
    .locals 6

    .prologue
    .line 217
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onViewCreatedCalled:Z

    .line 219
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 220
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->retainPresenterInstance()Z

    move-result v2

    .line 222
    .local v2, "retainPresenterInstance":Z
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    .line 223
    .local v1, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    invoke-interface {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    .line 224
    if-nez v2, :cond_0

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 227
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 230
    :cond_0
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_1

    .line 231
    const-string v3, "FragmentMvpVSDelegate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "detached MvpView from Presenter. MvpView "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 232
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   Presenter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 231
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    const-string v3, "FragmentMvpVSDelegate"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Retaining presenter instance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    .line 236
    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 235
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_1
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 273
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 244
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 248
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 276
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterOnBackstack:Z

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p1, :cond_1

    .line 278
    const-string v0, "com.hannesdorfmann.mosby3.fragment.mvp.id"

    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    sget-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v0, :cond_1

    .line 281
    const-string v0, "FragmentMvpVSDelegate"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saving MosbyViewId into Bundle. ViewId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 252
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onViewCreatedCalled:Z

    if-nez v0, :cond_0

    .line 253
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "It seems that you are using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 254
    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " as headless (UI less) fragment (because onViewCreated() has not been called or maybe delegation misses that part). Having a Presenter without a View (UI) doesn\'t make sense. Simply use an usual fragment instead of an MvpFragment if you want to use a UI less Fragment"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 257
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 261
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "bundle"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 112
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    const/4 v0, 0x0

    .line 114
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-eqz p2, :cond_3

    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    if-eqz v1, :cond_3

    .line 116
    const-string v1, "com.hannesdorfmann.mosby3.fragment.mvp.id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 118
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 119
    const-string v1, "FragmentMvpVSDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MosbyView ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for MvpView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 120
    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 119
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 124
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getPresenter(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .restart local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-eqz v0, :cond_2

    .line 128
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 129
    const-string v1, "FragmentMvpVSDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reused presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 130
    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 129
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_1
    :goto_0
    if-nez v0, :cond_4

    .line 157
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Oops, Presenter is null. This seems to be a Mosby internal bug. Please report this issue here: https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_2
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 137
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 138
    const-string v1, "FragmentMvpVSDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No presenter found although view Id was here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Most likely this was caused by a process death. New Presenter created"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 143
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 138
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 150
    :cond_3
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 151
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 152
    const-string v1, "FragmentMvpVSDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 161
    :cond_4
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1, v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 162
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V

    .line 164
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 165
    const-string v1, "FragmentMvpVSDelegate"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " attached to Presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    :cond_5
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->onViewCreatedCalled:Z

    .line 169
    return-void
.end method

.method protected retainPresenterInstance()Z
    .locals 4

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl<TV;TP;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 199
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 200
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 201
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterInstanceDuringScreenOrientationChanges:Z

    .line 212
    :cond_0
    :goto_0
    return v1

    .line 204
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v2

    .line 205
    goto :goto_0

    .line 208
    :cond_2
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->keepPresenterOnBackstack:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-static {v3}, Landroid/support/v4/app/BackstackAccessor;->isFragmentOnBackStack(Landroid/support/v4/app/Fragment;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 212
    :cond_3
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/FragmentMvpDelegateImpl;->fragment:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->isRemoving()Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method
