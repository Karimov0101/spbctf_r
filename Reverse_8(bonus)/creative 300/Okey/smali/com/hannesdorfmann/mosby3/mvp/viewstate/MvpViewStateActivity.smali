.class public abstract Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;
.source "MvpViewStateActivity.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpActivity",
        "<TV;TP;>;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback",
        "<TV;TP;TVS;>;"
    }
.end annotation


# instance fields
.field protected restoringViewState:Z

.field protected viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TVS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->restoringViewState:Z

    return-void
.end method


# virtual methods
.method protected getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation

    .prologue
    .line 49
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpViewStateDelegateImpl;-><init>(Landroid/app/Activity;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpViewStateDelegateCallback;Z)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;

    return-object v0
.end method

.method public getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    return-object v0
.end method

.method public isRestoringViewState()Z
    .locals 1

    .prologue
    .line 70
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->restoringViewState:Z

    return v0
.end method

.method public onViewStateInstanceRestored(Z)V
    .locals 0
    .param p1, "instanceStateRetained"    # Z

    .prologue
    .line 75
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    return-void
.end method

.method public setRestoringViewState(Z)V
    .locals 0
    .param p1, "restoringViewState"    # Z

    .prologue
    .line 66
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    iput-boolean p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->restoringViewState:Z

    .line 67
    return-void
.end method

.method public setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity<TV;TP;TVS;>;"
    .local p1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/MvpViewStateActivity;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 63
    return-void
.end method
