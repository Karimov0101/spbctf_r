.class public abstract Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;
.super Landroid/widget/FrameLayout;
.source "MvpFrameLayout.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Landroid/widget/FrameLayout;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback",
        "<TV;TP;>;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;"
    }
.end annotation


# instance fields
.field protected mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation
.end field

.field protected presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TP;"
        }
    .end annotation
.end field

.field private retainInstance:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->retainInstance:Z

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 49
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->retainInstance:Z

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 53
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->retainInstance:Z

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 58
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->retainInstance:Z

    .line 59
    return-void
.end method


# virtual methods
.method public abstract createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation
.end method

.method protected getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation

    .prologue
    .line 76
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegateImpl;-><init>(Landroid/view/View;Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;Z)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    return-object v0
.end method

.method public getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 118
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    return-object p0
.end method

.method public getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 110
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 84
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 85
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onAttachedToWindow()V

    .line 86
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 89
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 90
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onDetachedFromWindow()V

    .line 91
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 99
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 100
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "MissingSuperCall"
        }
    .end annotation

    .prologue
    .line 94
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-virtual {p0}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    move-result-object v0

    invoke-interface {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation

    .prologue
    .line 114
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    .local p1, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 115
    return-void
.end method

.method public final superOnRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 126
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 127
    return-void
.end method

.method public final superOnSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 122
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout;, "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpFrameLayout<TV;TP;>;"
    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method
