.class public interface abstract Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupDelegateCallback;
.super Ljava/lang/Object;
.source "ViewGroupDelegateCallback.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
        "<TV;TP;>;"
    }
.end annotation


# virtual methods
.method public abstract getContext()Landroid/content/Context;
.end method

.method public abstract superOnRestoreInstanceState(Landroid/os/Parcelable;)V
.end method

.method public abstract superOnSaveInstanceState()Landroid/os/Parcelable;
.end method
