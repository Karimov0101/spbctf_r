.class public interface abstract Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;
.super Ljava/lang/Object;
.source "MvpDelegateCallback.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation
.end method

.method public abstract getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public abstract getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation
.end method

.method public abstract setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TP;)V"
        }
    .end annotation
.end method
