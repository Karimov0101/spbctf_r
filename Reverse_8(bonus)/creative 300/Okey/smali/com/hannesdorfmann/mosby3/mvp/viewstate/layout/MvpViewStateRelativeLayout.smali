.class public abstract Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;
.super Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout;
.source "MvpViewStateRelativeLayout.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout",
        "<TV;TP;>;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback",
        "<TV;TP;TVS;>;"
    }
.end annotation


# instance fields
.field private restoringViewState:Z

.field protected viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TVS;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 42
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    invoke-direct {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout;-><init>(Landroid/content/Context;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 46
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    invoke-direct {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    .line 47
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 50
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 56
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/hannesdorfmann/mosby3/mvp/layout/MvpRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 38
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    .line 57
    return-void
.end method


# virtual methods
.method protected getMvpDelegate()Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
            "<TV;TP;>;"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    if-nez v0, :cond_0

    .line 62
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p0, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;-><init>(Landroid/view/View;Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;Z)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->mvpDelegate:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;

    return-object v0
.end method

.method public getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    return-object v0
.end method

.method public isRestoringViewState()Z
    .locals 1

    .prologue
    .line 81
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    return v0
.end method

.method public onViewStateInstanceRestored(Z)V
    .locals 0
    .param p1, "instanceStateRetained"    # Z

    .prologue
    .line 86
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    return-void
.end method

.method public setRestoringViewState(Z)V
    .locals 0
    .param p1, "retstoringViewState"    # Z

    .prologue
    .line 77
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    iput-boolean p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->restoringViewState:Z

    .line 78
    return-void
.end method

.method public setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TVS;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;, "Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout<TV;TP;TVS;>;"
    .local p1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/layout/MvpViewStateRelativeLayout;->viewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 74
    return-void
.end method
