.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;
.super Lcom/hannesdorfmann/mosby3/MosbySavedState;
.source "MosbyViewStateSavedState.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mosbyViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState$1;

    invoke-direct {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState$1;-><init>()V

    .line 37
    invoke-static {v0}, Landroid/support/v4/os/ParcelableCompat;->newCreator(Landroid/support/v4/os/ParcelableCompatCreatorCallbacks;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 36
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/MosbySavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    .line 60
    invoke-virtual {p1, p2}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->mosbyViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    .line 61
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;
    .param p2, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "viewState"    # Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/MosbySavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;)V

    .line 55
    iput-object p3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->mosbyViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    .line 56
    return-void
.end method


# virtual methods
.method public getRestoreableViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 70
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->mosbyViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 64
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/MosbySavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 65
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->mosbyViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 66
    return-void
.end method
