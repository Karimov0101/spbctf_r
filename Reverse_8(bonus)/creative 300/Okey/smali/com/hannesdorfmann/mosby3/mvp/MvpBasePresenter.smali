.class public Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.super Ljava/lang/Object;
.source "MvpBasePresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;"
    }
.end annotation


# instance fields
.field private viewRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<TV;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter<TV;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter<TV;>;"
    .local p1, "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    .line 78
    return-void
.end method

.method public detachView(Z)V
    .locals 1
    .param p1, "retainInstance"    # Z
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter<TV;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->clear()V

    .line 104
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    .line 106
    :cond_0
    return-void
.end method

.method public getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 88
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter<TV;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    goto :goto_0
.end method

.method public isViewAttached()Z
    .locals 1
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 97
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter<TV;>;"
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;->viewRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
