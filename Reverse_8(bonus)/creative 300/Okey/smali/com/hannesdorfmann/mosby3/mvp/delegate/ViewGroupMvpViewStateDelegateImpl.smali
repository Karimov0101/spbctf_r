.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;
.super Ljava/lang/Object;
.source "ViewGroupMvpViewStateDelegateImpl.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;VS::",
        "Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpDelegate",
        "<TV;TP;>;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "ViewGroupMvpDelegateImp"


# instance fields
.field private final activity:Landroid/app/Activity;

.field private applyViewState:Z

.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;"
        }
    .end annotation
.end field

.field private final isInEditMode:Z

.field private final keepPresenter:Z

.field private mosbyViewId:Ljava/lang/String;

.field private restoreableParcelableViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TVS;"
        }
    .end annotation
.end field

.field private viewStateFromMemoryRestored:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "keepPresenter"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback",
            "<TV;TP;TVS;>;Z)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback<TV;TP;TVS;>;"
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->restoreableParcelableViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 52
    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    .line 53
    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    .line 65
    if-nez p1, :cond_0

    .line 66
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "View is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 68
    :cond_0
    if-nez p2, :cond_1

    .line 69
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "MvpDelegateCallback is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_1
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 72
    iput-boolean p3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    .line 73
    invoke-virtual {p1}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    .line 74
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    if-nez v0, :cond_2

    .line 75
    invoke-interface {p2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_2
    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    goto :goto_0
.end method

.method private createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    .line 91
    .local v1, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v1, :cond_0

    .line 92
    new-instance v2, Ljava/lang/NullPointerException;

    const-string v3, "Presenter returned from createPresenter() is null."

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 94
    :cond_0
    iget-boolean v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    if-eqz v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 96
    .local v0, "context":Landroid/content/Context;
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 97
    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getActivity(Landroid/content/Context;)Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putPresenter(Landroid/app/Activity;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 99
    .end local v0    # "context":Landroid/content/Context;
    :cond_1
    return-object v1
.end method

.method private createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TVS;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v3, 0x0

    .line 103
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v0

    .line 104
    .local v0, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    if-eqz v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 107
    :cond_0
    iput-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    .line 108
    iput-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    .line 109
    return-object v0
.end method

.method private getContext()Landroid/content/Context;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 113
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    .local v0, "c":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 115
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Context returned from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is null"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onAttachedToWindow()V
    .locals 8

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 121
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    if-eqz v3, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    const/4 v0, 0x0

    .line 124
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    const/4 v2, 0x0

    .line 126
    .local v2, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 129
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 130
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_2

    .line 131
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new Presenter instance created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 134
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 131
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_2
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v2

    .line 138
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 139
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "new ViewState instance created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 142
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 139
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    .line 212
    .local v1, "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    if-nez v1, :cond_b

    .line 213
    new-instance v3, Ljava/lang/NullPointerException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MvpView returned from getMvpView() is null. Returned by "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 145
    .end local v1    # "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    :cond_4
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getPresenter(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 146
    .restart local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_6

    .line 149
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 150
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_5

    .line 151
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No Presenter instance found in cache, although MosbyView ID present. This was caused by process death, therefore new Presenter instance created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    :cond_5
    :goto_2
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getViewState(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    check-cast v2, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 165
    .restart local v2    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v2, :cond_a

    .line 167
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->restoreableParcelableViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    if-nez v3, :cond_7

    .line 169
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->createViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v2

    .line 170
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 171
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "No ViewState instance found in cache, although MosbyView ID present. This was caused by process death, therefore new ViewState instance created: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 156
    :cond_6
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_5

    .line 157
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Presenter instance reused from internal cache: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 160
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 157
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 177
    :cond_7
    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->restoreableParcelableViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 178
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    .line 179
    iput-boolean v7, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    .line 181
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    if-eqz v3, :cond_9

    .line 182
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 183
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The (internal) Mosby View id is null although restoreable view state (Parcelable) is not null. This should never happen. This seems to be a Mosby internal error. Please report this issue at https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 187
    :cond_8
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v4, v2}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putViewState(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/Object;)V

    .line 190
    :cond_9
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 191
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Parcelable ViewState instance reused from last SavedState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 194
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 191
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 199
    :cond_a
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    .line 200
    iput-boolean v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    .line 201
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 202
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ViewState instance reused from internal cache: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 205
    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 202
    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 217
    .restart local v1    # "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    :cond_b
    if-nez v0, :cond_c

    .line 218
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Oops, Presenter is null. This seems to be a Mosby internal bug. Please report this issue here: https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 222
    :cond_c
    if-nez v2, :cond_d

    .line 223
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Oops, ViewState is null. This seems to be a Mosby internal bug. Please report this issue here: https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 227
    :cond_d
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3, v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->setViewState(Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;)V

    .line 229
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    if-eqz v3, :cond_e

    .line 230
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3, v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 233
    :cond_e
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3, v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 234
    invoke-interface {v0, v1}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V

    .line 235
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_f

    .line 236
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "MvpView attached to Presenter. MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   Presenter: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    :cond_f
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->applyViewState:Z

    if-eqz v3, :cond_10

    .line 241
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    invoke-interface {v2, v1, v3}, Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;->apply(Lcom/hannesdorfmann/mosby3/mvp/MvpView;Z)V

    .line 242
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3, v7}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->setRestoringViewState(Z)V

    .line 243
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    iget-boolean v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    invoke-interface {v3, v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->onViewStateInstanceRestored(Z)V

    .line 244
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_0

    .line 245
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ViewState restored (from memory = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->viewStateFromMemoryRestored:Z

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ). MvpView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "   ViewState: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 253
    :cond_10
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->onNewViewStateInstance()V

    goto/16 :goto_0
.end method

.method public onDetachedFromWindow()V
    .locals 8

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 258
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    if-eqz v5, :cond_1

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 260
    :cond_1
    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v2

    .line 261
    .local v2, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v2, :cond_2

    .line 262
    new-instance v3, Ljava/lang/NullPointerException;

    const-string v4, "Presenter returned from delegateCallback.getPresenter() is null"

    invoke-direct {v3, v4}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 266
    :cond_2
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    if-eqz v5, :cond_8

    .line 268
    iget-boolean v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    .line 269
    invoke-static {v5, v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v5

    if-nez v5, :cond_5

    move v0, v3

    .line 271
    .local v0, "destroyedPermanently":Z
    :goto_1
    if-eqz v0, :cond_6

    .line 273
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_3

    .line 274
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 275
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " and removing presenter permanently from internal cache because the hosting Activity will be destroyed permanently"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 274
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_3
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 282
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v5}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 284
    :cond_4
    iput-object v7, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 285
    invoke-interface {v2, v4}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    goto :goto_0

    .end local v0    # "destroyedPermanently":Z
    :cond_5
    move v0, v4

    .line 269
    goto :goto_1

    .line 287
    .restart local v0    # "destroyedPermanently":Z
    :cond_6
    iget-boolean v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    iget-object v5, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    .line 288
    invoke-static {v4, v5}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v1

    .line 290
    .local v1, "detachedBecauseOrientationChange":Z
    if-eqz v1, :cond_0

    .line 292
    sget-boolean v4, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v4, :cond_7

    .line 293
    const-string v4, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 294
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " temporarily because of orientation change"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 293
    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :cond_7
    invoke-interface {v2, v3}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    goto/16 :goto_0

    .line 319
    .end local v0    # "destroyedPermanently":Z
    .end local v1    # "detachedBecauseOrientationChange":Z
    :cond_8
    sget-boolean v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->DEBUG:Z

    if-eqz v3, :cond_9

    .line 320
    const-string v3, "ViewGroupMvpDelegateImp"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Detaching View "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 321
    invoke-interface {v6}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " from Presenter "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " permanently"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 320
    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_9
    invoke-interface {v2, v4}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    .line 328
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v3, :cond_a

    .line 329
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 331
    :cond_a
    iput-object v7, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 364
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    if-eqz v1, :cond_0

    .line 375
    :goto_0
    return-void

    .line 366
    :cond_0
    instance-of v1, p1, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    if-nez v1, :cond_1

    .line 367
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v1, p1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->superOnRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0

    :cond_1
    move-object v0, p1

    .line 371
    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    .line 372
    .local v0, "savedState":Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;
    invoke-virtual {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->getMosbyViewId()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 373
    invoke-virtual {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->getRestoreableViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->restoreableParcelableViewState:Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    .line 374
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-virtual {v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->superOnRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl<TV;TP;TVS;>;"
    const/4 v2, 0x0

    .line 339
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->isInEditMode:Z

    if-eqz v3, :cond_1

    move-object v0, v2

    .line 356
    :cond_0
    :goto_0
    return-object v0

    .line 341
    :cond_1
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getViewState()Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;

    move-result-object v1

    .line 342
    .local v1, "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    if-nez v1, :cond_2

    .line 343
    new-instance v2, Ljava/lang/NullPointerException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ViewState returned from getViewState() is null for MvpView "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    .line 344
    invoke-interface {v4}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 347
    :cond_2
    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;

    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateCallback;->superOnSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 349
    .local v0, "superState":Landroid/os/Parcelable;
    iget-boolean v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->keepPresenter:Z

    if-eqz v3, :cond_0

    .line 350
    instance-of v3, v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    if-eqz v3, :cond_3

    .line 351
    new-instance v2, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    check-cast v1, Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;

    .end local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    invoke-direct {v2, v0, v3, v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;)V

    move-object v0, v2

    goto :goto_0

    .line 354
    .restart local v1    # "viewState":Lcom/hannesdorfmann/mosby3/mvp/viewstate/ViewState;, "TVS;"
    :cond_3
    new-instance v3, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;

    iget-object v4, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ViewGroupMvpViewStateDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-direct {v3, v0, v4, v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MosbyViewStateSavedState;-><init>(Landroid/os/Parcelable;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/viewstate/RestorableParcelableViewState;)V

    move-object v0, v3

    goto :goto_0
.end method
