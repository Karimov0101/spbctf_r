.class public Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;
.super Ljava/lang/Object;
.source "ActivityMvpDelegateImpl.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
        "P::",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<TV;>;>",
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegate;"
    }
.end annotation


# static fields
.field public static DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "ActivityMvpDelegateImpl"

.field protected static final KEY_MOSBY_VIEW_ID:Ljava/lang/String; = "com.hannesdorfmann.mosby3.activity.mvp.id"


# instance fields
.field protected activity:Landroid/app/Activity;

.field private delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
            "<TV;TP;>;"
        }
    .end annotation
.end field

.field protected keepPresenterInstance:Z

.field protected mosbyViewId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    sput-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;Z)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "keepPresenterInstance"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback",
            "<TV;TP;>;Z)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    .local p2, "delegateCallback":Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback<TV;TP;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 59
    if-nez p1, :cond_0

    .line 60
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Activity is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_0
    if-nez p2, :cond_1

    .line 64
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "MvpDelegateCallback is null!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_1
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 67
    iput-object p1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    .line 68
    iput-boolean p3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->keepPresenterInstance:Z

    .line 69
    return-void
.end method

.method private createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 90
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 91
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_0

    .line 92
    new-instance v1, Ljava/lang/NullPointerException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Presenter returned from createPresenter() is null. Activity is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 95
    :cond_0
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->keepPresenterInstance:Z

    if-eqz v1, :cond_1

    .line 96
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 97
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/hannesdorfmann/mosby3/PresenterManager;->putPresenter(Landroid/app/Activity;Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 99
    :cond_1
    return-object v0
.end method

.method private getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation

    .prologue
    .line 170
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    .line 171
    .local v0, "view":Lcom/hannesdorfmann/mosby3/mvp/MvpView;, "TV;"
    if-nez v0, :cond_0

    .line 172
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "View returned from getMvpView() is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 174
    :cond_0
    return-object v0
.end method

.method private getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TP;"
        }
    .end annotation

    .prologue
    .line 162
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 163
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-nez v0, :cond_0

    .line 164
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Presenter returned from getPresenter() is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 166
    :cond_0
    return-object v0
.end method

.method static retainPresenterInstance(ZLandroid/app/Activity;)Z
    .locals 1
    .param p0, "keepPresenterInstance"    # Z
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 77
    if-eqz p0, :cond_1

    invoke-virtual {p1}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-virtual {p1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onContentChanged()V
    .locals 0

    .prologue
    .line 221
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 104
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    const/4 v0, 0x0

    .line 106
    .local v0, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-eqz p1, :cond_3

    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->keepPresenterInstance:Z

    if-eqz v1, :cond_3

    .line 108
    const-string v1, "com.hannesdorfmann.mosby3.activity.mvp.id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 110
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_0

    .line 111
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MosbyView ID = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for MvpView: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 112
    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 111
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    :cond_0
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    .line 116
    invoke-static {v1, v2}, Lcom/hannesdorfmann/mosby3/PresenterManager;->getPresenter(Landroid/app/Activity;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    check-cast v0, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .restart local v0    # "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "TP;"
    if-eqz v0, :cond_2

    .line 120
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 121
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Reused presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    .line 122
    invoke-interface {v3}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 121
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_1
    :goto_0
    if-nez v0, :cond_4

    .line 149
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Oops, Presenter is null. This seems to be a Mosby internal bug. Please report this issue here: https://github.com/sockeqwe/mosby/issues"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 128
    :cond_2
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 129
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 130
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No presenter found although view Id was here: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Most likely this was caused by a process death. New Presenter created"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 135
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 130
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 142
    :cond_3
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->createViewIdAndCreatePresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    .line 143
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 144
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "New presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " for view "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 153
    :cond_4
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->delegateCallback:Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;

    invoke-interface {v1, v0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/MvpDelegateCallback;->setPresenter(Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V

    .line 154
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V

    .line 156
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_5

    .line 157
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " attached to Presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    :cond_5
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 178
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    iget-boolean v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->keepPresenterInstance:Z

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    invoke-static {v1, v2}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->retainPresenterInstance(ZLandroid/app/Activity;)Z

    move-result v0

    .line 179
    .local v0, "retainPresenterInstance":Z
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;->detachView(Z)V

    .line 180
    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/hannesdorfmann/mosby3/PresenterManager;->remove(Landroid/app/Activity;Ljava/lang/String;)V

    .line 184
    :cond_0
    sget-boolean v1, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 185
    if-eqz v0, :cond_2

    .line 186
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 187
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " destroyed temporarily. View detached from presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 189
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 186
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    :cond_1
    :goto_0
    return-void

    .line 191
    :cond_2
    const-string v1, "ActivityMvpDelegateImpl"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "View"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 192
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " destroyed permanently. View detached permanently from presenter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    .line 194
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 191
    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 201
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onPostCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 234
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onRestart()V
    .locals 0

    .prologue
    .line 217
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 205
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 224
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    iget-boolean v0, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->keepPresenterInstance:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 225
    const-string v0, "com.hannesdorfmann.mosby3.activity.mvp.id"

    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    sget-boolean v0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 227
    const-string v0, "ActivityMvpDelegateImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Saving MosbyViewId into Bundle. ViewId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for view "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 228
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;->getMvpView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 227
    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 209
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 213
    .local p0, "this":Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl;, "Lcom/hannesdorfmann/mosby3/mvp/delegate/ActivityMvpDelegateImpl<TV;TP;>;"
    return-void
.end method
