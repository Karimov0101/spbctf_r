.class Lcom/hannesdorfmann/mosby3/ActivityScopedCache;
.super Ljava/lang/Object;
.source "ActivityScopedCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    }
.end annotation


# instance fields
.field private final presenterMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/support/v4/util/ArrayMap;

    invoke-direct {v0}, Landroid/support/v4/util/ArrayMap;-><init>()V

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    .line 41
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 62
    return-void
.end method

.method public getPresenter(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<P:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TP;"
        }
    .end annotation

    .prologue
    .line 72
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .line 73
    .local v0, "holder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$000(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v1

    goto :goto_0
.end method

.method public getViewState(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<VS:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            ")TVS;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .line 85
    .local v0, "holder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {v0}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$100(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public putPresenter(Ljava/lang/String;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)V
    .locals 3
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2    # Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
            "<+",
            "Lcom/hannesdorfmann/mosby3/mvp/MvpView;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 98
    .local p2, "presenter":Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;, "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter<+Lcom/hannesdorfmann/mosby3/mvp/MvpView;>;"
    if-nez p1, :cond_0

    .line 99
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "ViewId is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    if-nez p2, :cond_1

    .line 103
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "Presenter is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .line 107
    .local v0, "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    if-nez v0, :cond_2

    .line 108
    new-instance v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .end local v0    # "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    invoke-direct {v0}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;-><init>()V

    .line 109
    .restart local v0    # "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    invoke-static {v0, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$002(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 110
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    :goto_0
    return-void

    .line 112
    :cond_2
    invoke-static {v0, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$002(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    goto :goto_0
.end method

.method public putViewState(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "viewState"    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 127
    if-nez p1, :cond_0

    .line 128
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "ViewId is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :cond_0
    if-nez p2, :cond_1

    .line 132
    new-instance v1, Ljava/lang/NullPointerException;

    const-string v2, "ViewState is null"

    invoke-direct {v1, v2}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .line 136
    .local v0, "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    if-nez v0, :cond_2

    .line 137
    new-instance v0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;

    .end local v0    # "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    invoke-direct {v0}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;-><init>()V

    .line 138
    .restart local v0    # "presenterHolder":Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;
    invoke-static {v0, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$102(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    iget-object v1, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_2
    invoke-static {v0, p2}, Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;->access$102(Lcom/hannesdorfmann/mosby3/ActivityScopedCache$PresenterHolder;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public remove(Ljava/lang/String;)V
    .locals 2
    .param p1, "viewId"    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 152
    if-nez p1, :cond_0

    .line 153
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "View Id is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/ActivityScopedCache;->presenterMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 157
    return-void
.end method
