.class public Lcom/hannesdorfmann/mosby3/MosbySavedState;
.super Landroid/support/v4/view/AbsSavedState;
.source "MosbySavedState.java"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/hannesdorfmann/mosby3/MosbySavedState;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mosbyViewId:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/hannesdorfmann/mosby3/MosbySavedState$1;

    invoke-direct {v0}, Lcom/hannesdorfmann/mosby3/MosbySavedState$1;-><init>()V

    .line 34
    invoke-static {v0}, Landroid/support/v4/os/ParcelableCompat;->newCreator(Landroid/support/v4/os/ParcelableCompatCreatorCallbacks;)Landroid/os/Parcelable$Creator;

    move-result-object v0

    sput-object v0, Lcom/hannesdorfmann/mosby3/MosbySavedState;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 33
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V
    .locals 1
    .param p1, "in"    # Landroid/os/Parcel;
    .param p2, "loader"    # Ljava/lang/ClassLoader;

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/AbsSavedState;-><init>(Landroid/os/Parcel;Ljava/lang/ClassLoader;)V

    .line 56
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/hannesdorfmann/mosby3/MosbySavedState;->mosbyViewId:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcelable;Ljava/lang/String;)V
    .locals 0
    .param p1, "superState"    # Landroid/os/Parcelable;
    .param p2, "mosbyViewId"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Landroid/support/v4/view/AbsSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 51
    iput-object p2, p0, Lcom/hannesdorfmann/mosby3/MosbySavedState;->mosbyViewId:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public getMosbyViewId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/MosbySavedState;->mosbyViewId:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "out"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/AbsSavedState;->writeToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-object v0, p0, Lcom/hannesdorfmann/mosby3/MosbySavedState;->mosbyViewId:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 62
    return-void
.end method
