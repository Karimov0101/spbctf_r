.class public final Lcom/safonov/speedreading/R$array;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "array"
.end annotation


# static fields
.field public static final accelerator_course_training_titles:I = 0x7f030000

.field public static final accelerator_settings_column_count_entry_values:I = 0x7f030001

.field public static final accelerator_settings_duration_entries:I = 0x7f030002

.field public static final accelerator_settings_duration_entry_values:I = 0x7f030003

.field public static final cup_timer_settings_duration_entries:I = 0x7f030004

.field public static final cup_timer_settings_duration_entry_values:I = 0x7f030005

.field public static final cup_timer_settings_type_entries:I = 0x7f030006

.field public static final cup_timer_settings_type_entry_values:I = 0x7f030007

.field public static final flash_words_settings_duration_entries:I = 0x7f030008

.field public static final flash_words_settings_duration_entry_values:I = 0x7f030009

.field public static final green_dot_settings_duration_entries:I = 0x7f03000a

.field public static final green_dot_settings_duration_entry_values:I = 0x7f03000b

.field public static final library_book_actions:I = 0x7f03000c

.field public static final line_of_sight_alphabet:I = 0x7f03000d

.field public static final line_of_sight_settings_board_size_entries:I = 0x7f03000e

.field public static final line_of_sight_settings_board_size_entry_values:I = 0x7f03000f

.field public static final line_of_sight_settings_field_type_entries:I = 0x7f030010

.field public static final line_of_sight_settings_field_type_entry_values:I = 0x7f030011

.field public static final math_settings_duration_entries:I = 0x7f030012

.field public static final math_settings_duration_entry_values:I = 0x7f030013

.field public static final motivators:I = 0x7f030014

.field public static final pass_course_training_icons:I = 0x7f030015

.field public static final pass_course_training_titles:I = 0x7f030016

.field public static final reader_reading_modes_dialog_items:I = 0x7f030017

.field public static final reader_settings_text_size_entries:I = 0x7f030018

.field public static final recommendation_services_description:I = 0x7f030019

.field public static final recommendation_services_link:I = 0x7f03001a

.field public static final recommendation_services_title:I = 0x7f03001b

.field public static final remember_words_array:I = 0x7f03001c

.field public static final rules_of_success_array:I = 0x7f03001d

.field public static final schulte_table_settings_column_count_entries:I = 0x7f03001e

.field public static final schulte_table_settings_row_count_entries:I = 0x7f03001f

.field public static final speed_reading_words:I = 0x7f030020

.field public static final true_colors_list:I = 0x7f030021

.field public static final true_colors_words:I = 0x7f030022

.field public static final word_pairs_non_equal_first_words:I = 0x7f030023

.field public static final word_pairs_non_equal_second_words:I = 0x7f030024

.field public static final word_pairs_words:I = 0x7f030025


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
