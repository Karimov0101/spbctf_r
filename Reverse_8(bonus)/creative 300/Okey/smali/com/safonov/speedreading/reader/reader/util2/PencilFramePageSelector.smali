.class public Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;
.super Ljava/lang/Object;
.source "PencilFramePageSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
    }
.end annotation


# static fields
.field private static final pattern:Ljava/util/regex/Pattern;

.field private static final regExp:Ljava/lang/String; = "\\b([\\w]+)\\b"


# instance fields
.field private staticLayout:Landroid/text/StaticLayout;

.field private text:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 87
    const-string v0, "\\b([\\w]+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->pattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/text/TextPaint;Ljava/lang/CharSequence;I)V
    .locals 8
    .param p1, "textPaint"    # Landroid/text/TextPaint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "pageWidth"    # I

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->text:Ljava/lang/CharSequence;

    .line 54
    new-instance v0, Landroid/text/StaticLayout;

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p2

    move-object v2, p1

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    .line 55
    return-void
.end method

.method private getWordCount(Ljava/lang/CharSequence;)I
    .locals 3
    .param p1, "charSequence"    # Ljava/lang/CharSequence;

    .prologue
    .line 90
    sget-object v2, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 92
    .local v0, "matcher":Ljava/util/regex/Matcher;
    const/4 v1, 0x0

    .line 93
    .local v1, "wordCount":I
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 94
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    :cond_0
    return v1
.end method


# virtual methods
.method public getFrameCoordinates(II)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
    .locals 8
    .param p1, "lineStartIndex"    # I
    .param p2, "lineCount"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 59
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v6

    if-le p1, v6, :cond_0

    .line 60
    const/4 v1, 0x0

    .line 83
    :goto_0
    return-object v1

    .line 63
    :cond_0
    add-int v6, p1, p2

    iget-object v7, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v7}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v7

    if-le v6, v7, :cond_1

    .line 64
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v0

    .line 69
    .local v0, "lineEndIndex":I
    :goto_1
    new-instance v1, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;

    invoke-direct {v1}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;-><init>()V

    .line 71
    .local v1, "pencilFrameDataSet":Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->setFrameTopY(I)V

    .line 72
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, v0}, Landroid/text/StaticLayout;->getLineTop(I)I

    move-result v6

    invoke-virtual {v1, v6}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->setFrameBottomY(I)V

    .line 74
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, p1}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v4

    .line 75
    .local v4, "textStartIndex":I
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->staticLayout:Landroid/text/StaticLayout;

    invoke-virtual {v6, v0}, Landroid/text/StaticLayout;->getLineStart(I)I

    move-result v3

    .line 77
    .local v3, "textEndIndex":I
    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->text:Ljava/lang/CharSequence;

    invoke-interface {v6, v4, v3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 79
    .local v2, "subText":Ljava/lang/CharSequence;
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->getWordCount(Ljava/lang/CharSequence;)I

    move-result v5

    .line 81
    .local v5, "wordCount":I
    invoke-virtual {v1, v5}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->setWordCount(I)V

    goto :goto_0

    .line 66
    .end local v0    # "lineEndIndex":I
    .end local v1    # "pencilFrameDataSet":Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
    .end local v2    # "subText":Ljava/lang/CharSequence;
    .end local v3    # "textEndIndex":I
    .end local v4    # "textStartIndex":I
    .end local v5    # "wordCount":I
    :cond_1
    add-int v0, p1, p2

    .restart local v0    # "lineEndIndex":I
    goto :goto_1
.end method
