.class public final enum Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;
.super Ljava/lang/Enum;
.source "FileExplorerFileType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum EPUB:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum FB2:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum FB2_ZIP:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum PARENT_FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

.field public static final enum TXT:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "TXT"

    invoke-direct {v0, v1, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->TXT:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "FB2"

    invoke-direct {v0, v1, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "FB2_ZIP"

    invoke-direct {v0, v1, v5}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2_ZIP:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "EPUB"

    invoke-direct {v0, v1, v6}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->EPUB:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "FOLDER"

    invoke-direct {v0, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    const-string v1, "PARENT_FOLDER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->PARENT_FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    .line 7
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->TXT:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2_ZIP:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->EPUB:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->PARENT_FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->$VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->$VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    return-object v0
.end method
