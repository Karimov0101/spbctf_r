.class public Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
.super Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;
.source "TxtDao.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "bookDescriptionDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 29
    return-void
.end method


# virtual methods
.method public addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
        }
    .end annotation

    .prologue
    .line 33
    iget-object v1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 34
    new-instance v1, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;

    invoke-direct {v1}, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;-><init>()V

    throw v1

    .line 37
    :cond_0
    new-instance v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 39
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    iget-object v1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v2

    .line 41
    .local v2, "id":J
    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 42
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 43
    const-string v1, "txt"

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 44
    invoke-static {p1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 48
    return-object v0
.end method

.method public getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 8
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;-><init>()V

    .line 55
    .local v0, "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;
    :try_start_0
    new-instance v6, Ljava/io/File;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getFilePath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getEncoding(Ljava/io/File;)Ljava/lang/String;

    move-result-object v5

    .line 56
    .local v5, "encoding":Ljava/lang/String;
    if-nez v5, :cond_0

    .line 57
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 58
    .local v3, "content":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->setTitle(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->setContent(Ljava/lang/CharSequence;)V

    .line 66
    :goto_0
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 67
    .local v1, "bookChapterList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    new-instance v2, Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-direct {v2}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;-><init>()V

    .line 70
    .local v2, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    invoke-virtual {v2, v1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->setBookChapterList(Ljava/util/List;)V

    .line 72
    return-object v2

    .line 61
    .end local v1    # "bookChapterList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v2    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .end local v3    # "content":Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getFilePath()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6, v5}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 62
    .restart local v3    # "content":Ljava/lang/String;
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->setTitle(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->setContent(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 73
    .end local v3    # "content":Ljava/lang/String;
    .end local v5    # "encoding":Ljava/lang/String;
    :catch_0
    move-exception v4

    .line 74
    .local v4, "e":Ljava/io/IOException;
    new-instance v6, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v6, v4}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v6
.end method

.method public removeBook(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 80
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->removeBookDescription(J)V

    .line 81
    return-void
.end method
