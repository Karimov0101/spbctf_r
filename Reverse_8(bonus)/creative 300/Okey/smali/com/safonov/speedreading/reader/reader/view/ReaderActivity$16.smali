.class Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "ReaderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 830
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 858
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$1300(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPageViewCenterClick(Z)V

    .line 859
    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v4, 0x0

    .line 834
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v2, v3

    .line 836
    .local v2, "x":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    .line 837
    .local v0, "pageWidth":I
    div-int/lit8 v1, v0, 0x3

    .line 839
    .local v1, "thirdPartOfPageWidth":I
    if-ge v2, v1, :cond_0

    .line 840
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$1000(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v3}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPageViewLeftClick()V

    move v3, v4

    .line 847
    :goto_0
    return v3

    .line 842
    :cond_0
    if-gt v1, v2, :cond_1

    sub-int v3, v0, v1

    if-gt v2, v3, :cond_1

    .line 843
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$1100(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPageViewCenterClick(Z)V

    .line 844
    const/4 v3, 0x1

    goto :goto_0

    .line 846
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$1200(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v3}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPageViewRightClick()V

    move v3, v4

    .line 847
    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 853
    const/4 v0, 0x1

    return v0
.end method
