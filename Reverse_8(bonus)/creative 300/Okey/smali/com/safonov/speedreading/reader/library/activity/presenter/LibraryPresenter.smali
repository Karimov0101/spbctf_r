.class public Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "LibraryPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;"
    }
.end annotation


# instance fields
.field private fragmentType:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    return-void
.end method

.method private setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V
    .locals 2
    .param p1, "fragmentType"    # Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    .prologue
    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->fragmentType:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    .line 23
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$1;->$SwitchMap$com$safonov$speedreading$reader$library$activity$presenter$LibraryPresenter$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 31
    :goto_0
    return-void

    .line 25
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->setLibraryFragment()V

    goto :goto_0

    .line 28
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->setFileExplorerFragment()V

    goto :goto_0

    .line 23
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public init()V
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    .line 19
    return-void
.end method

.method public onActionBarHomePressed()V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$1;->$SwitchMap$com$safonov$speedreading$reader$library$activity$presenter$LibraryPresenter$FragmentType:[I

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->fragmentType:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 79
    :goto_0
    return-void

    .line 70
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->finish()V

    goto :goto_0

    .line 73
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    goto :goto_0

    .line 76
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    goto :goto_0

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 35
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$1;->$SwitchMap$com$safonov$speedreading$reader$library$activity$presenter$LibraryPresenter$FragmentType:[I

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->fragmentType:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    return-void

    .line 37
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->finish()V

    goto :goto_0

    .line 40
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->onFileExplorerBackPressed()V

    goto :goto_0

    .line 43
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    goto :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 60
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;->setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 64
    :cond_0
    return-void
.end method

.method public requestToSetFileExplorerFragment()V
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    .line 56
    return-void
.end method

.method public requestToSetLibraryFragment()V
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;->setFragmentType(Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;)V

    .line 51
    return-void
.end method
