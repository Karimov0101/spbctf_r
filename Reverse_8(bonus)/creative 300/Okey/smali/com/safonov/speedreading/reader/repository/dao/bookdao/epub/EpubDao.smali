.class public Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
.super Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;
.source "EpubDao.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;
    }
.end annotation


# static fields
.field private static final EMPHASIS_TAG_1:Ljava/lang/String; = "em"

.field private static final EMPHASIS_TAG_2:Ljava/lang/String; = "i"

.field private static final EMPTY_LINE_TAG:Ljava/lang/String; = "br"

.field private static final HEAD_1_TAG:Ljava/lang/String; = "h1"

.field private static final HEAD_2_TAG:Ljava/lang/String; = "h2"

.field private static final HEAD_3_TAG:Ljava/lang/String; = "h3"

.field private static final HEAD_4_TAG:Ljava/lang/String; = "h4"

.field private static final HEAD_5_TAG:Ljava/lang/String; = "h5"

.field private static final HEAD_6_TAG:Ljava/lang/String; = "h6"

.field private static final PARAGRAPH_TAG_1:Ljava/lang/String; = "p"

.field private static final PARAGRAPH_TAG_2:Ljava/lang/String; = "div"

.field private static final STRIKE_THROUGH_TAG:Ljava/lang/String; = "strike"

.field private static final STRONG_TAG_1:Ljava/lang/String; = "strong"

.field private static final STRONG_TAG_2:Ljava/lang/String; = "b"

.field public static final SUB_FONT_HEIGHT:F = 0.5f

.field private static final SUB_TAG:Ljava/lang/String; = "sub"

.field private static final SUP_TAG:Ljava/lang/String; = "sup"

.field private static final UNDERLINE_TAG:Ljava/lang/String; = "u"

.field private static final headers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private static final xmlPattern:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 53
    const-string v0, ".+\\.x?html"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->xmlPattern:Ljava/util/regex/Pattern;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    .line 88
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h1"

    const/high16 v2, 0x3fc00000    # 1.5f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h2"

    const v2, 0x3fb33333    # 1.4f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h3"

    const v2, 0x3fa66666    # 1.3f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h4"

    const v2, 0x3f99999a    # 1.2f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h5"

    const v2, 0x3f8ccccd    # 1.1f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v1, "h6"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "bookDescriptionDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 97
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 98
    return-void
.end method

.method private getBookAuthor(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 181
    const-string v0, "dc:creator"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBookLanguage(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 177
    const-string v0, "dc:language"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBookNavigationPoints(Lorg/w3c/dom/Document;)Ljava/util/List;
    .locals 28
    .param p1, "docNcx"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Document;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 423
    const-string v24, "navPoint"

    move-object/from16 v0, p1

    move-object/from16 v1, v24

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v20

    .line 424
    .local v20, "navPoints":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v20 .. v20}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v21

    .line 426
    .local v21, "navPointsCount":I
    new-instance v5, Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 427
    .local v5, "bookChapterContentPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v7, Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 428
    .local v7, "bookChapterTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v6, Ljava/util/ArrayList;

    move/from16 v0, v21

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 430
    .local v6, "bookChapterId":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    move/from16 v0, v21

    if-ge v11, v0, :cond_5

    .line 431
    move-object/from16 v0, v20

    invoke-interface {v0, v11}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v18

    .line 432
    .local v18, "navPointChildNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v18 .. v18}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v19

    .line 434
    .local v19, "navPointChildNodesCount":I
    const/4 v9, 0x0

    .line 435
    .local v9, "contentPath":Ljava/lang/String;
    const/4 v10, 0x0

    .line 436
    .local v10, "contentTitle":Ljava/lang/String;
    const/4 v8, 0x0

    .line 438
    .local v8, "contentId":Ljava/lang/String;
    const/4 v13, 0x0

    .local v13, "j":I
    :goto_1
    move/from16 v0, v19

    if-ge v13, v0, :cond_4

    .line 439
    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v24

    const-string v25, "content"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_1

    .line 441
    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    check-cast v24, Lorg/w3c/dom/Element;

    const-string v25, "src"

    invoke-interface/range {v24 .. v25}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 442
    sget-object v24, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->xmlPattern:Ljava/util/regex/Pattern;

    move-object/from16 v0, v24

    invoke-virtual {v0, v9}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v15

    .line 444
    .local v15, "matcher":Ljava/util/regex/Matcher;
    const/16 v24, 0x23

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v23

    .line 445
    .local v23, "sharpIndex":I
    if-lez v23, :cond_0

    .line 446
    add-int/lit8 v24, v23, 0x1

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 449
    :cond_0
    invoke-virtual {v15}, Ljava/util/regex/Matcher;->find()Z

    move-result v24

    if-eqz v24, :cond_1

    .line 450
    invoke-virtual {v15}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v9

    .line 452
    const/16 v24, 0x2f

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v12

    .local v12, "index":I
    const/16 v24, -0x1

    move/from16 v0, v24

    if-le v12, v0, :cond_1

    .line 453
    add-int/lit8 v24, v12, 0x1

    move/from16 v0, v24

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    .line 458
    .end local v12    # "index":I
    .end local v15    # "matcher":Ljava/util/regex/Matcher;
    .end local v23    # "sharpIndex":I
    :cond_1
    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v24

    const-string v25, "navLabel"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_2

    .line 459
    move-object/from16 v0, v18

    invoke-interface {v0, v13}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v16

    .line 460
    .local v16, "navLabelChildNodes":Lorg/w3c/dom/NodeList;
    invoke-interface/range {v16 .. v16}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v17

    .line 462
    .local v17, "navLabelChildNodesCount":I
    const/4 v14, 0x0

    .local v14, "k":I
    :goto_2
    move/from16 v0, v17

    if-ge v14, v0, :cond_2

    .line 463
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v24

    const-string v25, "text"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_3

    .line 464
    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v24

    invoke-interface/range {v24 .. v24}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v10

    .line 438
    .end local v14    # "k":I
    .end local v16    # "navLabelChildNodes":Lorg/w3c/dom/NodeList;
    .end local v17    # "navLabelChildNodesCount":I
    :cond_2
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_1

    .line 462
    .restart local v14    # "k":I
    .restart local v16    # "navLabelChildNodes":Lorg/w3c/dom/NodeList;
    .restart local v17    # "navLabelChildNodesCount":I
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    .line 471
    .end local v14    # "k":I
    .end local v16    # "navLabelChildNodes":Lorg/w3c/dom/NodeList;
    .end local v17    # "navLabelChildNodesCount":I
    :cond_4
    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 472
    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    invoke-interface {v6, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 476
    .end local v8    # "contentId":Ljava/lang/String;
    .end local v9    # "contentPath":Ljava/lang/String;
    .end local v10    # "contentTitle":Ljava/lang/String;
    .end local v13    # "j":I
    .end local v18    # "navPointChildNodes":Lorg/w3c/dom/NodeList;
    .end local v19    # "navPointChildNodesCount":I
    :cond_5
    new-instance v22, Ljava/util/ArrayList;

    move-object/from16 v0, v22

    move/from16 v1, v21

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 477
    .local v22, "navigationPoints":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;>;"
    const/4 v11, 0x0

    :goto_3
    move/from16 v0, v21

    if-ge v11, v0, :cond_6

    .line 478
    new-instance v27, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-interface {v7, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    invoke-interface {v5, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Ljava/lang/String;

    invoke-interface {v6, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v26

    check-cast v26, Ljava/lang/String;

    move-object/from16 v0, v27

    move-object/from16 v1, p0

    move-object/from16 v2, v24

    move-object/from16 v3, v25

    move-object/from16 v4, v26

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 477
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 481
    :cond_6
    return-object v22
.end method

.method private getBookTitle(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 2
    .param p1, "document"    # Lorg/w3c/dom/Document;

    .prologue
    .line 173
    const-string v0, "dc:title"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getCoverPath(Lorg/w3c/dom/Document;)Ljava/lang/String;
    .locals 8
    .param p1, "document"    # Lorg/w3c/dom/Document;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, "coverId":Ljava/lang/String;
    const/4 v0, 0x0

    .line 188
    .local v0, "coverFilePath":Ljava/lang/String;
    const/4 v4, 0x0

    .line 191
    .local v4, "itemNodes":Lorg/w3c/dom/NodeList;
    const-string v6, "meta"

    invoke-interface {p1, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 193
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v3, v6, :cond_1

    .line 194
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    check-cast v5, Lorg/w3c/dom/Element;

    .line 197
    .local v5, "metaElement":Lorg/w3c/dom/Element;
    const-string v6, "name"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "cover"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 198
    const-string v6, "content"

    invoke-interface {v5, v6}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 193
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 202
    .end local v5    # "metaElement":Lorg/w3c/dom/Element;
    :cond_1
    if-nez v1, :cond_2

    .line 203
    const/4 v6, 0x0

    .line 216
    :goto_1
    return-object v6

    .line 207
    :cond_2
    const-string v6, "item"

    invoke-interface {p1, v6}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v4

    .line 208
    const/4 v3, 0x0

    :goto_2
    invoke-interface {v4}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    if-ge v3, v6, :cond_4

    .line 209
    invoke-interface {v4, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    check-cast v2, Lorg/w3c/dom/Element;

    .line 211
    .local v2, "element":Lorg/w3c/dom/Element;
    const-string v6, "id"

    invoke-interface {v2, v6}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 212
    const-string v6, "href"

    invoke-interface {v2, v6}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 208
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v2    # "element":Lorg/w3c/dom/Element;
    :cond_4
    move-object v6, v0

    .line 216
    goto :goto_1
.end method

.method private parseBody(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "body"    # Lorg/w3c/dom/Node;

    .prologue
    .line 491
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 492
    .local v0, "bodyChildNotes":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 494
    .local v1, "bodyChildNotesCount":I
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 496
    .local v2, "builder":Landroid/text/SpannableStringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v1, :cond_1

    .line 497
    invoke-interface {v0, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 499
    .local v3, "currentTag":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 500
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 496
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 504
    .end local v3    # "currentTag":Lorg/w3c/dom/Node;
    :cond_1
    return-object v2
.end method

.method private parseBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 25
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 227
    :try_start_0
    new-instance v21, Ljava/util/zip/ZipFile;

    move-object/from16 v0, v21

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    .line 228
    .local v21, "zipFile":Ljava/util/zip/ZipFile;
    invoke-virtual/range {v21 .. v21}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v19

    .line 229
    .local v19, "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v17

    .line 231
    .local v17, "navigationPoints":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;>;"
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v22

    :cond_0
    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->hasNext()Z

    move-result v23

    if-eqz v23, :cond_1

    invoke-interface/range {v22 .. v22}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/zip/ZipEntry;

    .line 232
    .local v20, "zipEntry":Ljava/util/zip/ZipEntry;
    new-instance v23, Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v24

    invoke-direct/range {v23 .. v24}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v23 .. v23}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    .line 235
    .local v12, "fileName":Ljava/lang/String;
    invoke-virtual {v12}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v23

    const-string v24, "toc.ncx"

    invoke-virtual/range {v23 .. v24}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v23

    if-eqz v23, :cond_0

    .line 236
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v10

    .line 237
    .local v10, "docNcx":Lorg/w3c/dom/Document;
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->getBookNavigationPoints(Lorg/w3c/dom/Document;)Ljava/util/List;

    move-result-object v17

    .line 242
    .end local v10    # "docNcx":Lorg/w3c/dom/Document;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v20    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_1
    new-instance v7, Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-direct {v7}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;-><init>()V

    .line 243
    .local v7, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    new-instance v6, Ljava/util/LinkedList;

    invoke-direct {v6}, Ljava/util/LinkedList;-><init>()V

    .line 245
    .local v6, "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v22

    if-ge v14, v0, :cond_7

    .line 246
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterId()Ljava/lang/String;

    move-result-object v3

    .line 247
    .local v3, "bookChapterId":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterPath()Ljava/lang/String;

    move-result-object v4

    .line 248
    .local v4, "bookChapterPath":Ljava/lang/String;
    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterTitle()Ljava/lang/String;

    move-result-object v5

    .line 250
    .local v5, "bookChapterTitle":Ljava/lang/String;
    invoke-interface/range {v19 .. v19}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :cond_2
    :goto_1
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v22

    if-eqz v22, :cond_3

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Ljava/util/zip/ZipEntry;

    .line 251
    .restart local v20    # "zipEntry":Ljava/util/zip/ZipEntry;
    new-instance v22, Ljava/io/File;

    invoke-virtual/range {v20 .. v20}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v22

    move-object/from16 v1, v24

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v12

    .line 253
    .restart local v12    # "fileName":Ljava/lang/String;
    invoke-virtual {v4, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 254
    if-nez v3, :cond_4

    .line 255
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v8

    .line 256
    .local v8, "chapterDocument":Lorg/w3c/dom/Document;
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseChapter(Lorg/w3c/dom/Document;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 258
    .local v2, "bookChapterContent":Ljava/lang/CharSequence;
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v22

    if-nez v22, :cond_3

    .line 259
    new-instance v22, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;

    move-object/from16 v0, v22

    invoke-direct {v0, v5, v2}, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object/from16 v0, v22

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    .end local v2    # "bookChapterContent":Ljava/lang/CharSequence;
    .end local v8    # "chapterDocument":Lorg/w3c/dom/Document;
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v20    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_3
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 262
    .restart local v12    # "fileName":Ljava/lang/String;
    .restart local v20    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_4
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .local v15, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 265
    .local v18, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v15, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    move-object/from16 v0, v18

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    add-int/lit8 v16, v14, 0x1

    .local v16, "j":I
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v22

    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_6

    .line 269
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterPath()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 270
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterId()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v22

    invoke-interface {v15, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;

    invoke-virtual/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->getBookChapterTitle()Ljava/lang/String;

    move-result-object v22

    move-object/from16 v0, v18

    move-object/from16 v1, v22

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v14, v14, 0x1

    .line 268
    :cond_5
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 276
    :cond_6
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v13

    .line 277
    .local v13, "html":Ljava/lang/String;
    move-object/from16 v0, v21

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v22

    invoke-static/range {v22 .. v22}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v8

    .line 279
    .restart local v8    # "chapterDocument":Lorg/w3c/dom/Document;
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v8, v13, v15, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseCombineChapter(Lorg/w3c/dom/Document;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    .line 280
    .local v9, "combineChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    invoke-interface {v6, v9}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    .line 288
    .end local v3    # "bookChapterId":Ljava/lang/String;
    .end local v4    # "bookChapterPath":Ljava/lang/String;
    .end local v5    # "bookChapterTitle":Ljava/lang/String;
    .end local v6    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v7    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .end local v8    # "chapterDocument":Lorg/w3c/dom/Document;
    .end local v9    # "combineChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v12    # "fileName":Ljava/lang/String;
    .end local v13    # "html":Ljava/lang/String;
    .end local v14    # "i":I
    .end local v15    # "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v16    # "j":I
    .end local v17    # "navigationPoints":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;>;"
    .end local v18    # "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v19    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .end local v20    # "zipEntry":Ljava/util/zip/ZipEntry;
    .end local v21    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v11

    .line 289
    .local v11, "e":Ljava/io/IOException;
    new-instance v22, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    move-object/from16 v0, v22

    invoke-direct {v0, v11}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v22

    .line 286
    .end local v11    # "e":Ljava/io/IOException;
    .restart local v6    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .restart local v7    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .restart local v14    # "i":I
    .restart local v17    # "navigationPoints":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;>;"
    .restart local v19    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .restart local v21    # "zipFile":Ljava/util/zip/ZipFile;
    :cond_7
    :try_start_1
    invoke-virtual {v7, v6}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->setBookChapterList(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 287
    return-object v7
.end method

.method private parseChapter(Lorg/w3c/dom/Document;)Ljava/lang/CharSequence;
    .locals 3
    .param p1, "bookDocument"    # Lorg/w3c/dom/Document;

    .prologue
    .line 485
    const-string v1, "body"

    invoke-interface {p1, v1}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    .line 487
    .local v0, "body":Lorg/w3c/dom/Node;
    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseBody(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v1

    return-object v1
.end method

.method private parseCombineChapter(Lorg/w3c/dom/Document;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 26
    .param p1, "combineChapterDocument"    # Lorg/w3c/dom/Document;
    .param p2, "html"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Document;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookChapter;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 295
    .local p3, "ids":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct/range {p0 .. p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseChapter(Lorg/w3c/dom/Document;)Ljava/lang/CharSequence;

    move-result-object v10

    .line 297
    .local v10, "combineChapter":Ljava/lang/CharSequence;
    const-string v6, "<body[\\s\\S]+</body>"

    .line 299
    .local v6, "bodyRegex":Ljava/lang/String;
    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 300
    .local v5, "bodyPattern":Ljava/util/regex/Pattern;
    move-object/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v17

    .line 302
    .local v17, "matcher":Ljava/util/regex/Matcher;
    move-object/from16 v3, p2

    .line 304
    .local v3, "body":Ljava/lang/String;
    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->find()Z

    move-result v24

    if-eqz v24, :cond_0

    .line 305
    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->start()I

    move-result v24

    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->end()I

    move-result v25

    move-object/from16 v0, p2

    move/from16 v1, v24

    move/from16 v2, v25

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 306
    const-string v24, "\n"

    const-string v25, ""

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 309
    :cond_0
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v9

    .line 310
    .local v9, "chaptersCount":I
    new-array v0, v9, [I

    move-object/from16 v22, v0

    .line 311
    .local v22, "startIndexes":[I
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 313
    .local v7, "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_0
    if-ge v14, v9, :cond_b

    .line 314
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 315
    .local v20, "request":Ljava/lang/StringBuilder;
    const-string v24, "id=\""

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 316
    move-object/from16 v0, p3

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v24

    check-cast v24, Ljava/lang/String;

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    const-string v24, "\""

    move-object/from16 v0, v20

    move-object/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-static/range {v24 .. v24}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v19

    .line 321
    .local v19, "pattern":Ljava/util/regex/Pattern;
    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v17

    .line 323
    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->find()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 324
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    .line 326
    .local v4, "bodyLength":I
    const/16 v21, 0x0

    .line 327
    .local v21, "startIndex":I
    move v13, v4

    .line 329
    .local v13, "endIndex":I
    const/4 v15, 0x0

    .line 331
    .local v15, "itsTag":Z
    invoke-virtual/range {v17 .. v17}, Ljava/util/regex/Matcher;->start()I

    move-result v16

    .local v16, "j":I
    :goto_1
    move/from16 v0, v16

    if-ge v0, v4, :cond_1

    .line 332
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x3e

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_3

    .line 333
    add-int/lit8 v21, v16, 0x1

    .line 338
    :cond_1
    move/from16 v16, v21

    :goto_2
    move/from16 v0, v16

    if-ge v0, v4, :cond_6

    .line 339
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_4

    .line 340
    const/4 v15, 0x1

    .line 338
    :cond_2
    :goto_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_2

    .line 331
    :cond_3
    add-int/lit8 v16, v16, 0x1

    goto :goto_1

    .line 341
    :cond_4
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x3e

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_5

    .line 342
    if-eqz v15, :cond_2

    .line 343
    const/4 v15, 0x0

    goto :goto_3

    .line 345
    :cond_5
    if-nez v15, :cond_2

    .line 346
    move/from16 v21, v16

    .line 352
    :cond_6
    move/from16 v16, v21

    :goto_4
    move/from16 v0, v16

    if-ge v0, v4, :cond_7

    .line 353
    move/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v24

    const/16 v25, 0x3c

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_9

    .line 354
    add-int/lit8 v13, v16, -0x1

    .line 359
    :cond_7
    add-int/lit8 v24, v13, 0x1

    move/from16 v0, v21

    move/from16 v1, v24

    invoke-virtual {v3, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v18

    .line 360
    .local v18, "partOfContent":Ljava/lang/String;
    add-int/lit8 v24, v13, 0x1

    move/from16 v0, v24

    invoke-virtual {v3, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 362
    invoke-static/range {v18 .. v18}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v12

    .line 363
    .local v12, "combinePattern":Ljava/util/regex/Pattern;
    invoke-virtual {v12, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v11

    .line 365
    .local v11, "combineMatcher":Ljava/util/regex/Matcher;
    if-lez v14, :cond_a

    .line 366
    add-int/lit8 v24, v14, -0x1

    aget v24, v22, v24

    move/from16 v0, v24

    invoke-virtual {v11, v0}, Ljava/util/regex/Matcher;->find(I)Z

    move-result v24

    if-eqz v24, :cond_8

    .line 367
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->start()I

    move-result v24

    aput v24, v22, v14

    .line 313
    .end local v4    # "bodyLength":I
    .end local v11    # "combineMatcher":Ljava/util/regex/Matcher;
    .end local v12    # "combinePattern":Ljava/util/regex/Pattern;
    .end local v13    # "endIndex":I
    .end local v15    # "itsTag":Z
    .end local v16    # "j":I
    .end local v18    # "partOfContent":Ljava/lang/String;
    .end local v21    # "startIndex":I
    :cond_8
    :goto_5
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 352
    .restart local v4    # "bodyLength":I
    .restart local v13    # "endIndex":I
    .restart local v15    # "itsTag":Z
    .restart local v16    # "j":I
    .restart local v21    # "startIndex":I
    :cond_9
    add-int/lit8 v16, v16, 0x1

    goto :goto_4

    .line 370
    .restart local v11    # "combineMatcher":Ljava/util/regex/Matcher;
    .restart local v12    # "combinePattern":Ljava/util/regex/Pattern;
    .restart local v18    # "partOfContent":Ljava/lang/String;
    :cond_a
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->find()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 371
    invoke-virtual {v11}, Ljava/util/regex/Matcher;->start()I

    move-result v24

    aput v24, v22, v14

    goto :goto_5

    .line 377
    .end local v4    # "bodyLength":I
    .end local v11    # "combineMatcher":Ljava/util/regex/Matcher;
    .end local v12    # "combinePattern":Ljava/util/regex/Pattern;
    .end local v13    # "endIndex":I
    .end local v15    # "itsTag":Z
    .end local v16    # "j":I
    .end local v18    # "partOfContent":Ljava/lang/String;
    .end local v19    # "pattern":Ljava/util/regex/Pattern;
    .end local v20    # "request":Ljava/lang/StringBuilder;
    .end local v21    # "startIndex":I
    :cond_b
    const/4 v14, 0x0

    :goto_6
    add-int/lit8 v24, v9, -0x1

    move/from16 v0, v24

    if-ge v14, v0, :cond_d

    .line 378
    move-object/from16 v0, p4

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/CharSequence;

    .line 379
    .local v23, "title":Ljava/lang/CharSequence;
    aget v24, v22, v14

    add-int/lit8 v25, v14, 0x1

    aget v25, v22, v25

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v10, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    .line 381
    .local v8, "chapter":Ljava/lang/CharSequence;
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_c

    .line 382
    new-instance v24, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object/from16 v0, v24

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 377
    :cond_c
    add-int/lit8 v14, v14, 0x1

    goto :goto_6

    .line 384
    .end local v8    # "chapter":Ljava/lang/CharSequence;
    .end local v23    # "title":Ljava/lang/CharSequence;
    :cond_d
    add-int/lit8 v24, v9, -0x1

    move-object/from16 v0, p4

    move/from16 v1, v24

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v23

    check-cast v23, Ljava/lang/CharSequence;

    .line 385
    .restart local v23    # "title":Ljava/lang/CharSequence;
    add-int/lit8 v24, v9, -0x1

    aget v24, v22, v24

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v25

    move/from16 v0, v24

    move/from16 v1, v25

    invoke-interface {v10, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    .line 387
    .restart local v8    # "chapter":Ljava/lang/CharSequence;
    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/String;->isEmpty()Z

    move-result v24

    if-nez v24, :cond_e

    .line 388
    new-instance v24, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;

    move-object/from16 v0, v24

    move-object/from16 v1, v23

    invoke-direct {v0, v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    move-object/from16 v0, v24

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_e
    return-object v7
.end method

.method private parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;
    .locals 14
    .param p1, "tag"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v9, 0x2

    const/16 v10, 0xa

    const/4 v8, 0x1

    const/16 v13, 0x21

    const/4 v7, 0x0

    .line 509
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v6

    const/4 v11, 0x3

    if-ne v6, v11, :cond_1

    .line 510
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 512
    .local v5, "tagNodeValue":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 662
    .end local v5    # "tagNodeValue":Ljava/lang/String;
    :goto_0
    return-object v5

    .line 515
    .restart local v5    # "tagNodeValue":Ljava/lang/String;
    :cond_0
    const-string v5, ""

    goto :goto_0

    .line 519
    .end local v5    # "tagNodeValue":Ljava/lang/String;
    :cond_1
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 521
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 522
    .local v3, "nestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 524
    .local v4, "nestedTagsCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_2

    .line 525
    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 524
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 528
    :cond_2
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v11

    const/4 v6, -0x1

    invoke-virtual {v11}, Ljava/lang/String;->hashCode()I

    move-result v12

    sparse-switch v12, :sswitch_data_0

    :cond_3
    :goto_2
    packed-switch v6, :pswitch_data_0

    :cond_4
    :goto_3
    move-object v5, v0

    .line 662
    goto :goto_0

    .line 528
    :sswitch_0
    const-string v12, "h1"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    move v6, v7

    goto :goto_2

    :sswitch_1
    const-string v12, "h2"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    move v6, v8

    goto :goto_2

    :sswitch_2
    const-string v12, "h3"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    move v6, v9

    goto :goto_2

    :sswitch_3
    const-string v12, "h4"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v6, 0x3

    goto :goto_2

    :sswitch_4
    const-string v12, "h5"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v6, 0x4

    goto :goto_2

    :sswitch_5
    const-string v12, "h6"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v6, 0x5

    goto :goto_2

    :sswitch_6
    const-string v12, "p"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v6, 0x6

    goto :goto_2

    :sswitch_7
    const-string v12, "div"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/4 v6, 0x7

    goto :goto_2

    :sswitch_8
    const-string v12, "em"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0x8

    goto :goto_2

    :sswitch_9
    const-string v12, "i"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0x9

    goto :goto_2

    :sswitch_a
    const-string v12, "strong"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    move v6, v10

    goto :goto_2

    :sswitch_b
    const-string v12, "b"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0xb

    goto :goto_2

    :sswitch_c
    const-string v12, "sub"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v12, "sup"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v12, "strike"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0xe

    goto/16 :goto_2

    :sswitch_f
    const-string v12, "u"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0xf

    goto/16 :goto_2

    :sswitch_10
    const-string v12, "br"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    const/16 v6, 0x10

    goto/16 :goto_2

    .line 530
    :pswitch_0
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v11, "h1"

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 531
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 530
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 532
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 533
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 532
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 534
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 535
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 534
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 537
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 538
    .local v1, "builderLength":I
    if-lt v1, v8, :cond_4

    .line 542
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-eq v6, v10, :cond_4

    .line 543
    const-string v6, "\n\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 549
    .end local v1    # "builderLength":I
    :pswitch_1
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v11, "h2"

    invoke-interface {v6, v11}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 550
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 549
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 551
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 552
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 551
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 553
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 554
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 553
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 556
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 557
    .restart local v1    # "builderLength":I
    if-lt v1, v8, :cond_4

    .line 561
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-eq v6, v10, :cond_4

    .line 562
    const-string v6, "\n\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 568
    .end local v1    # "builderLength":I
    :pswitch_2
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v10, "h3"

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 569
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 568
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 570
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 571
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 570
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 572
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 573
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 572
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 576
    :pswitch_3
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v10, "h4"

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 577
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 576
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 578
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 579
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 578
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 580
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 581
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 580
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 584
    :pswitch_4
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v10, "h5"

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 585
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 584
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 586
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 587
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 586
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 588
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 589
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 588
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 592
    :pswitch_5
    new-instance v9, Landroid/text/style/RelativeSizeSpan;

    sget-object v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->headers:Ljava/util/Map;

    const-string v10, "h6"

    invoke-interface {v6, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Float;

    invoke-virtual {v6}, Ljava/lang/Float;->floatValue()F

    move-result v6

    invoke-direct {v9, v6}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 593
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    .line 592
    invoke-virtual {v0, v9, v7, v6, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 594
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v9, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v6, v9}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 595
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v9

    .line 594
    invoke-virtual {v0, v6, v7, v9, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 596
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 597
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 596
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 600
    :pswitch_6
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 601
    .restart local v1    # "builderLength":I
    if-lt v1, v8, :cond_4

    .line 605
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-eq v6, v10, :cond_4

    .line 606
    const-string v6, "\n\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 611
    .end local v1    # "builderLength":I
    :pswitch_7
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 612
    .restart local v1    # "builderLength":I
    if-lt v1, v8, :cond_4

    .line 616
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v6

    if-eq v6, v10, :cond_4

    .line 617
    const-string v6, "\n\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 622
    .end local v1    # "builderLength":I
    :pswitch_8
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 623
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 622
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 626
    :pswitch_9
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v9}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 627
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 626
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 630
    :pswitch_a
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 631
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 630
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 634
    :pswitch_b
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 635
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 634
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 638
    :pswitch_c
    new-instance v6, Landroid/text/style/RelativeSizeSpan;

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct {v6, v8}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 639
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 638
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 640
    new-instance v6, Landroid/text/style/SubscriptSpan;

    invoke-direct {v6}, Landroid/text/style/SubscriptSpan;-><init>()V

    .line 641
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 640
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 644
    :pswitch_d
    new-instance v6, Landroid/text/style/RelativeSizeSpan;

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct {v6, v8}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 645
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 644
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 646
    new-instance v6, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v6}, Landroid/text/style/SuperscriptSpan;-><init>()V

    .line 647
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 646
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 650
    :pswitch_e
    new-instance v6, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v6}, Landroid/text/style/StrikethroughSpan;-><init>()V

    .line 651
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 650
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 654
    :pswitch_f
    new-instance v6, Landroid/text/style/UnderlineSpan;

    invoke-direct {v6}, Landroid/text/style/UnderlineSpan;-><init>()V

    .line 655
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 654
    invoke-virtual {v0, v6, v7, v8, v13}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_3

    .line 658
    :pswitch_10
    const-string v6, "\n"

    invoke-virtual {v0, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_3

    .line 528
    nop

    :sswitch_data_0
    .sparse-switch
        -0x352aa04e -> :sswitch_e
        -0x352a8969 -> :sswitch_a
        0x62 -> :sswitch_b
        0x69 -> :sswitch_9
        0x70 -> :sswitch_6
        0x75 -> :sswitch_f
        0xc50 -> :sswitch_10
        0xca8 -> :sswitch_8
        0xcc9 -> :sswitch_0
        0xcca -> :sswitch_1
        0xccb -> :sswitch_2
        0xccc -> :sswitch_3
        0xccd -> :sswitch_4
        0xcce -> :sswitch_5
        0x18491 -> :sswitch_7
        0x1be40 -> :sswitch_c
        0x1be4e -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method


# virtual methods
.method public addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 22
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
        }
    .end annotation

    .prologue
    .line 102
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 103
    new-instance v19, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;

    invoke-direct/range {v19 .. v19}, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;-><init>()V

    throw v19

    .line 106
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-object/from16 v19, v0

    invoke-interface/range {v19 .. v19}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v12

    .line 108
    .local v12, "id":J
    new-instance v4, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-direct {v4}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 110
    .local v4, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    const/16 v17, 0x0

    .line 112
    .local v17, "zipFile":Ljava/util/zip/ZipFile;
    :try_start_0
    new-instance v18, Ljava/util/zip/ZipFile;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .local v18, "zipFile":Ljava/util/zip/ZipFile;
    :try_start_1
    invoke-virtual/range {v18 .. v18}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v15

    .line 116
    .local v15, "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 117
    .local v2, "bookChaptersPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 119
    .local v7, "coverPath":Ljava/lang/String;
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_1
    :goto_0
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_2

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/zip/ZipEntry;

    .line 120
    .local v16, "zipEntry":Ljava/util/zip/ZipEntry;
    new-instance v20, Ljava/io/File;

    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 123
    .local v10, "fileName":Ljava/lang/String;
    invoke-virtual {v10}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v20

    const-string v21, "content.opf"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_1

    .line 124
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v5

    .line 126
    .local v5, "contentOpf":Lorg/w3c/dom/Document;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->getBookTitle(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->getBookLanguage(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 128
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->getBookAuthor(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 130
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->getCoverPath(Lorg/w3c/dom/Document;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 134
    .end local v5    # "contentOpf":Lorg/w3c/dom/Document;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v16    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_2
    if-eqz v7, :cond_4

    .line 135
    new-instance v19, Ljava/io/File;

    move-object/from16 v0, v19

    invoke-direct {v0, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    .line 136
    .local v6, "coverName":Ljava/lang/String;
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->libraryFilePath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 138
    .local v14, "imagePath":Ljava/lang/String;
    new-instance v11, Ljava/io/File;

    invoke-direct {v11, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 139
    .local v11, "imageFile":Ljava/io/File;
    invoke-virtual {v11}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/io/File;->mkdirs()Z

    .line 141
    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/zip/ZipEntry;

    .line 142
    .restart local v16    # "zipEntry":Ljava/util/zip/ZipEntry;
    new-instance v20, Ljava/io/File;

    invoke-virtual/range {v16 .. v16}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v20 .. v20}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v10

    .line 144
    .restart local v10    # "fileName":Ljava/lang/String;
    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 145
    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-static {v0, v11}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->copyFile(Ljava/io/InputStream;Ljava/io/File;)V

    .line 146
    invoke-virtual {v4, v14}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setCoverImagePath(Ljava/lang/String;)V

    .line 151
    .end local v6    # "coverName":Ljava/lang/String;
    .end local v10    # "fileName":Ljava/lang/String;
    .end local v11    # "imageFile":Ljava/io/File;
    .end local v14    # "imagePath":Ljava/lang/String;
    .end local v16    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_4
    invoke-virtual/range {v18 .. v18}, Ljava/util/zip/ZipFile;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 157
    invoke-virtual {v4, v12, v13}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 158
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 159
    invoke-static/range {p1 .. p1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 161
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->libraryFilePath:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    sget-object v20, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 163
    .local v8, "directoryPath":Ljava/lang/String;
    invoke-direct/range {p0 .. p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->parseBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v3

    .line 164
    .local v3, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    invoke-static {v4, v3, v8}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->saveBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/repository/entity/BookContent;Ljava/lang/String;)V

    .line 166
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 168
    return-object v4

    .line 153
    .end local v2    # "bookChaptersPaths":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .end local v7    # "coverPath":Ljava/lang/String;
    .end local v8    # "directoryPath":Ljava/lang/String;
    .end local v15    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .end local v18    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v9

    .line 154
    .local v9, "e":Ljava/io/IOException;
    :goto_1
    new-instance v19, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    move-object/from16 v0, v19

    invoke-direct {v0, v9}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v19

    .line 153
    .end local v9    # "e":Ljava/io/IOException;
    .end local v17    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v18    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_1
    move-exception v9

    move-object/from16 v17, v18

    .end local v18    # "zipFile":Ljava/util/zip/ZipFile;
    .restart local v17    # "zipFile":Ljava/util/zip/ZipFile;
    goto :goto_1
.end method

.method public getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 221
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->libraryFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 222
    .local v0, "directoryPath":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->readBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v1

    return-object v1
.end method

.method public removeBook(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 667
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->removeBookDescription(J)V

    .line 668
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;->libraryFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->removeDirectory(Ljava/io/File;)Z

    .line 669
    return-void
.end method
