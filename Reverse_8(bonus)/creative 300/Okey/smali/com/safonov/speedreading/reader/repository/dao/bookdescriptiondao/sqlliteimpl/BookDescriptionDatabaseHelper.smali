.class public Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDatabaseHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "BookDescriptionDatabaseHelper.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final BOOK_AUTHOR:Ljava/lang/String; = "author"

.field public static final BOOK_COVER_NAME:Ljava/lang/String; = "cover_name"

.field public static final BOOK_FAVORITE_FLAG:Ljava/lang/String; = "favorite"

.field public static final BOOK_FILE_PATH:Ljava/lang/String; = "file_path"

.field public static final BOOK_LANGUAGE:Ljava/lang/String; = "language"

.field public static final BOOK_OFFSET:Ljava/lang/String; = "book_offset"

.field public static final BOOK_PROGRESS:Ljava/lang/String; = "progress"

.field public static final BOOK_TABLE:Ljava/lang/String; = "books"

.field public static final BOOK_TABLE_COLUMNS:[Ljava/lang/String;

.field public static final BOOK_TITLE:Ljava/lang/String; = "title"

.field public static final BOOK_TYPE:Ljava/lang/String; = "type"

.field private static final DATABASE_NAME:Ljava/lang/String; = "books_database.db"

.field private static final DATABASE_VERSION:I = 0x3


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "author"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "language"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "cover_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "file_path"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "favorite"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "progress"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "book_offset"

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDatabaseHelper;->BOOK_TABLE_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 45
    const-string v0, "books_database.db"

    const/4 v1, 0x0

    const/4 v2, 0x3

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 46
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 50
    const-string v0, "CREATE TABLE books(_id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, author TEXT, language TEXT, cover_name TEXT, file_path TEXT NOT NULL, type TEXT NOT NULL, favorite INTEGER NOT NULL, progress INTEGER NOT NULL, book_offset INTEGER NOT NULL);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 64
    return-void
.end method

.method public onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 68
    const-string v0, "DROP TABLE IF EXISTS books"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 70
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 74
    const-string v0, "DROP TABLE IF EXISTS books"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDatabaseHelper;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 76
    return-void
.end method
