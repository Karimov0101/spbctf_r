.class public Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;
.super Ljava/lang/Object;
.source "BookDetailFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

.field private view2131296723:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    .line 26
    const v1, 0x7f09003c

    const-string v2, "field \'coverImageView\'"

    const-class v3, Landroid/widget/ImageView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->coverImageView:Landroid/widget/ImageView;

    .line 27
    const v1, 0x7f09020c

    const-string v2, "field \'titleTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->titleTextView:Landroid/widget/TextView;

    .line 28
    const v1, 0x7f09002e

    const-string v2, "field \'authorView\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorView:Landroid/view/View;

    .line 29
    const v1, 0x7f09002d

    const-string v2, "field \'authorTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorTextView:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f0900d4

    const-string v2, "field \'languageView\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageView:Landroid/view/View;

    .line 31
    const v1, 0x7f0900d3

    const-string v2, "field \'languageTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageTextView:Landroid/widget/TextView;

    .line 32
    const v1, 0x7f090090

    const-string v2, "field \'filePathTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->filePathTextView:Landroid/widget/TextView;

    .line 33
    const v1, 0x7f0901d3

    const-string v2, "method \'onStartReadingClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 34
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->view2131296723:Landroid/view/View;

    .line 35
    new-instance v1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    .line 47
    .local v0, "target":Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    .line 50
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->coverImageView:Landroid/widget/ImageView;

    .line 51
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->titleTextView:Landroid/widget/TextView;

    .line 52
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorView:Landroid/view/View;

    .line 53
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorTextView:Landroid/widget/TextView;

    .line 54
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageView:Landroid/view/View;

    .line 55
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageTextView:Landroid/widget/TextView;

    .line 56
    iput-object v2, v0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->filePathTextView:Landroid/widget/TextView;

    .line 58
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->view2131296723:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iput-object v2, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment_ViewBinding;->view2131296723:Landroid/view/View;

    .line 60
    return-void
.end method
