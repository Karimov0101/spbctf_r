.class Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "FileExplorerFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FileExplorerAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# static fields
.field private static final FILE_TYPE_EPUB:Ljava/lang/String; = "epub"

.field private static final FILE_TYPE_FB2:Ljava/lang/String; = "fb2"

.field private static final FILE_TYPE_TXT:Ljava/lang/String; = "txt"

.field private static final PARENT_FOLDER_NAME:Ljava/lang/String; = "..."


# instance fields
.field private fileExplorerFileWrapperList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;


# direct methods
.method private constructor <init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;)V
    .locals 1
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    .prologue
    .line 196
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 195
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->fileExplorerFileWrapperList:Ljava/util/List;

    .line 197
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    .line 198
    return-void
.end method

.method synthetic constructor <init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;
    .param p2, "x1"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;
    .param p3, "x2"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$1;

    .prologue
    .line 168
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;)V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 168
    check-cast p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->onBindViewHolder(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;I)V
    .locals 7
    .param p1, "holder"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/16 v4, 0x8

    const v6, 0x7f0e006f

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 222
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    .line 224
    .local v0, "fileExplorerFileWrapper":Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileNameTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->getFileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$4;->$SwitchMap$com$safonov$speedreading$reader$library$fileexplorer$model$FileExplorerFileType:[I

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->getType()Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 250
    :goto_0
    return-void

    .line 228
    :pswitch_0
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileCoverImageView:Landroid/widget/ImageView;

    const v2, 0x7f08008f

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 229
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileNameTextView:Landroid/widget/TextView;

    const-string v2, "..."

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 233
    :pswitch_1
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileCoverImageView:Landroid/widget/ImageView;

    const v2, 0x7f08008e

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 234
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileTypeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 237
    :pswitch_2
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileCoverImageView:Landroid/widget/ImageView;

    const v2, 0x7f080090

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 238
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileTypeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "txt"

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 242
    :pswitch_3
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileCoverImageView:Landroid/widget/ImageView;

    const v2, 0x7f08008d

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 243
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileTypeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "fb2"

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 246
    :pswitch_4
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileCoverImageView:Landroid/widget/ImageView;

    const v2, 0x7f08008c

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 247
    iget-object v1, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->fileTypeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "epub"

    aput-object v4, v3, v5

    invoke-virtual {v2, v6, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 226
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 168
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b008a

    const/4 v3, 0x0

    .line 209
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 211
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    invoke-direct {v1, p0, v0, v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;Landroid/view/View;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;)V

    return-object v1
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "data":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;>;"
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 202
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 203
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->notifyDataSetChanged()V

    .line 204
    return-void
.end method
