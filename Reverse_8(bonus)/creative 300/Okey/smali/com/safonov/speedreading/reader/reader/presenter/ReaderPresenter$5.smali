.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final FLASH_MODE_PROGRESS_DELAY:I = 0x64


# instance fields
.field private currentPage:Ljava/lang/CharSequence;

.field private flashModeCurrentPageDelay:I

.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 751
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getFlashModeReadingDelay(Ljava/lang/CharSequence;I)I
    .locals 3
    .param p1, "page"    # Ljava/lang/CharSequence;
    .param p2, "speed"    # I

    .prologue
    .line 759
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "[\\s\']"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v0, v1

    .line 760
    .local v0, "wordsCount":I
    const v1, 0xea60

    div-int/2addr v1, p2

    mul-int/2addr v1, v0

    return v1
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 765
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 833
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 770
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    if-nez v4, :cond_2

    .line 771
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    .line 774
    :cond_2
    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    .line 775
    .local v1, "oldDelay":I
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    sub-int v3, v1, v4

    .line 777
    .local v3, "remainder":I
    int-to-float v4, v3

    int-to-float v5, v1

    div-float v2, v4, v5

    .line 778
    .local v2, "ratio":F
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1600(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->getFlashModeReadingDelay(Ljava/lang/CharSequence;I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v2

    float-to-int v0, v4

    .line 780
    .local v0, "newRemainder":I
    sub-int v4, v1, v3

    add-int/2addr v4, v0

    iput v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    .line 781
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 782
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->initProgressViewProgress(I)V

    .line 785
    :cond_3
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4, v7}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 788
    .end local v0    # "newRemainder":I
    .end local v1    # "oldDelay":I
    .end local v2    # "ratio":F
    .end local v3    # "remainder":I
    :cond_4
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    if-nez v4, :cond_8

    .line 789
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v4

    iput-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    .line 791
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1600(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-direct {p0, v4, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->getFlashModeReadingDelay(Ljava/lang/CharSequence;I)I

    move-result v4

    iput v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    .line 792
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 793
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->currentPage:Ljava/lang/CharSequence;

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 794
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 795
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->initProgressViewProgress(I)V

    .line 803
    :cond_5
    :goto_1
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    add-int/lit8 v5, v5, 0x64

    invoke-static {v4, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I

    .line 804
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 805
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewProgress(I)V

    .line 808
    :cond_6
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    iget v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    if-lt v4, v5, :cond_a

    .line 809
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4, v7}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I

    .line 811
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 812
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 813
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_7

    .line 814
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$408(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    .line 815
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPageView(II)V

    .line 816
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationSeekBar(I)V

    .line 817
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v5

    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    .line 818
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v6

    invoke-interface {v4, v5, v6}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationPageView(II)V

    .line 822
    :cond_7
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 797
    :cond_8
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    iget v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    iget v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->flashModeCurrentPageDelay:I

    div-int/lit8 v6, v6, 0xa

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_5

    .line 798
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 799
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSecondaryTextColor()V

    goto/16 :goto_1

    .line 824
    :cond_9
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 825
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 826
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 827
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v5

    iget-object v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 831
    :cond_a
    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v4

    const-wide/16 v6, 0x64

    invoke-virtual {v4, p0, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
