.class final enum Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;
.super Ljava/lang/Enum;
.source "LibraryPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "FragmentType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

.field public static final enum BOOK_DETAIL:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

.field public static final enum FILE_EXPLORER:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

.field public static final enum LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    const-string v1, "LIBRARY"

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    const-string v1, "FILE_EXPLORER"

    invoke-direct {v0, v1, v3}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    new-instance v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    const-string v1, "BOOK_DETAIL"

    invoke-direct {v0, v1, v4}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    sget-object v1, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->LIBRARY:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->$VALUES:[Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 13
    const-class v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->$VALUES:[Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter$FragmentType;

    return-object v0
.end method
