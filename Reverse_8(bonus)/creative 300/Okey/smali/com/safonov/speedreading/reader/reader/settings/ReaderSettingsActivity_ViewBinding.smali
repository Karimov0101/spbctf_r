.class public Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity_ViewBinding;
.super Ljava/lang/Object;
.source "ReaderSettingsActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;Landroid/view/View;)V

    .line 22
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;

    .line 28
    const v2, 0x7f09020d

    const-string v3, "field \'toolbar\'"

    const-class v4, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 30
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 31
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 32
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e0134

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeKey:Ljava/lang/String;

    .line 33
    const v2, 0x7f0e0133

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeDefaultValue:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 39
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;

    .line 40
    .local v0, "target":Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;

    .line 43
    iput-object v1, v0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 44
    return-void
.end method
