.class public Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;
.super Landroid/os/AsyncTask;
.source "BookCollectionAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
        ">;>;"
    }
.end annotation


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private listener:Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;)V
    .locals 0
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 23
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;

    .line 24
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->doInBackground([Ljava/lang/Void;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/util/List;
    .locals 1
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/IBookController;->getBookDescriptionList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->onPostExecute(Ljava/util/List;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    .local p1, "bookDescriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookDescription;>;"
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;->onCollectionPostExecute(Ljava/util/List;)V

    .line 43
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;->onCollectionPreExecute()V

    .line 30
    return-void
.end method
