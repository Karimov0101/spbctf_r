.class public Lcom/safonov/speedreading/reader/repository/util/JsonUtil;
.super Ljava/lang/Object;
.source "JsonUtil.java"


# static fields
.field private static final BOOK_CONTENT_DIRECTORY_NAME:Ljava/lang/String; = "book_content"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static convertChaptersToJson(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Ljava/util/List;)Ljava/util/List;
    .locals 15
    .param p0, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookChapter;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/json/JSONObject;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 78
    .local p1, "chapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v11

    invoke-direct {v4, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 80
    .local v4, "jsonChapters":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;

    .line 81
    .local v0, "chapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 83
    .local v1, "chapterJsonObject":Lorg/json/JSONObject;
    new-instance v10, Lorg/json/JSONArray;

    invoke-direct {v10}, Lorg/json/JSONArray;-><init>()V

    .line 84
    .local v10, "titleSpanJsonArray":Lorg/json/JSONArray;
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 86
    .local v2, "chapterSpanJsonArray":Lorg/json/JSONArray;
    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;->getTitle()Ljava/lang/CharSequence;

    move-result-object v9

    .line 87
    .local v9, "title":Ljava/lang/CharSequence;
    if-nez v9, :cond_0

    .line 88
    const-string v11, "title"

    const-string v13, ""

    invoke-virtual {v1, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 89
    const-string v11, "title_spans"

    invoke-virtual {v1, v11, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 110
    :goto_1
    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;->getContent()Ljava/lang/CharSequence;

    move-result-object v3

    .line 111
    .local v3, "content":Ljava/lang/CharSequence;
    if-nez v3, :cond_3

    .line 112
    const-string v11, "content"

    const-string v13, ""

    invoke-virtual {v1, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 113
    const-string v11, "content_spans"

    invoke-virtual {v1, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 128
    :goto_2
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 91
    .end local v3    # "content":Ljava/lang/CharSequence;
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v11

    const-string v13, "fb2"

    invoke-virtual {v11, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    move-object v7, v9

    .line 92
    check-cast v7, Landroid/text/Spanned;

    .line 94
    .local v7, "spannedTitle":Landroid/text/Spanned;
    const-string v11, "title"

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 96
    const/4 v11, 0x0

    invoke-interface {v7}, Landroid/text/Spanned;->length()I

    move-result v13

    const-class v14, Ljava/lang/Object;

    invoke-interface {v7, v11, v13, v14}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    .line 99
    .local v8, "spans":[Ljava/lang/Object;
    array-length v13, v8

    const/4 v11, 0x0

    :goto_3
    if-ge v11, v13, :cond_1

    aget-object v5, v8, v11

    .line 100
    .local v5, "span":Ljava/lang/Object;
    invoke-static {v7, v5}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->convertSpanToJson(Landroid/text/Spanned;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v14

    invoke-virtual {v10, v14}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 99
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 103
    .end local v5    # "span":Ljava/lang/Object;
    :cond_1
    const-string v11, "title_spans"

    invoke-virtual {v1, v11, v10}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .line 105
    .end local v7    # "spannedTitle":Landroid/text/Spanned;
    .end local v8    # "spans":[Ljava/lang/Object;
    :cond_2
    const-string v11, "title"

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_1

    .restart local v3    # "content":Ljava/lang/CharSequence;
    :cond_3
    move-object v6, v3

    .line 115
    check-cast v6, Landroid/text/Spanned;

    .line 117
    .local v6, "spannedContent":Landroid/text/Spanned;
    const-string v11, "content"

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v1, v11, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 119
    const/4 v11, 0x0

    invoke-interface {v6}, Landroid/text/Spanned;->length()I

    move-result v13

    const-class v14, Ljava/lang/Object;

    invoke-interface {v6, v11, v13, v14}, Landroid/text/Spanned;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v8

    .line 121
    .restart local v8    # "spans":[Ljava/lang/Object;
    array-length v13, v8

    const/4 v11, 0x0

    :goto_4
    if-ge v11, v13, :cond_4

    aget-object v5, v8, v11

    .line 122
    .restart local v5    # "span":Ljava/lang/Object;
    invoke-static {v6, v5}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->convertSpanToJson(Landroid/text/Spanned;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v14

    invoke-virtual {v2, v14}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 121
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 125
    .end local v5    # "span":Ljava/lang/Object;
    :cond_4
    const-string v11, "content_spans"

    invoke-virtual {v1, v11, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_2

    .line 131
    .end local v0    # "chapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    .end local v1    # "chapterJsonObject":Lorg/json/JSONObject;
    .end local v2    # "chapterSpanJsonArray":Lorg/json/JSONArray;
    .end local v3    # "content":Ljava/lang/CharSequence;
    .end local v6    # "spannedContent":Landroid/text/Spanned;
    .end local v8    # "spans":[Ljava/lang/Object;
    .end local v9    # "title":Ljava/lang/CharSequence;
    .end local v10    # "titleSpanJsonArray":Lorg/json/JSONArray;
    :cond_5
    return-object v4
.end method

.method private static convertSpanToJson(Landroid/text/Spanned;Ljava/lang/Object;)Lorg/json/JSONObject;
    .locals 8
    .param p0, "spanned"    # Landroid/text/Spanned;
    .param p1, "span"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 137
    .local v1, "jsonObject":Lorg/json/JSONObject;
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    .line 139
    .local v3, "spanClass":Ljava/lang/Class;
    const-string v5, "span_class"

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 140
    const-string v5, "start_index"

    invoke-interface {p0, p1}, Landroid/text/Spanned;->getSpanStart(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 141
    const-string v5, "end_index"

    invoke-interface {p0, p1}, Landroid/text/Spanned;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 143
    const-class v5, Landroid/text/style/AlignmentSpan$Standard;

    if-ne v3, v5, :cond_1

    move-object v0, p1

    .line 144
    check-cast v0, Landroid/text/style/AlignmentSpan$Standard;

    .line 145
    .local v0, "alignmentSpan":Landroid/text/style/AlignmentSpan$Standard;
    const-string v5, "alignment"

    invoke-virtual {v0}, Landroid/text/style/AlignmentSpan$Standard;->getAlignment()Landroid/text/Layout$Alignment;

    move-result-object v6

    invoke-virtual {v6}, Landroid/text/Layout$Alignment;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 163
    .end local v0    # "alignmentSpan":Landroid/text/style/AlignmentSpan$Standard;
    .end local p1    # "span":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 146
    .restart local p1    # "span":Ljava/lang/Object;
    :cond_1
    const-class v5, Landroid/text/style/RelativeSizeSpan;

    if-ne v3, v5, :cond_2

    move-object v2, p1

    .line 147
    check-cast v2, Landroid/text/style/RelativeSizeSpan;

    .line 148
    .local v2, "relativeSizeSpan":Landroid/text/style/RelativeSizeSpan;
    const-string v5, "relative_size"

    invoke-virtual {v2}, Landroid/text/style/RelativeSizeSpan;->getSizeChange()F

    move-result v6

    float-to-double v6, v6

    invoke-virtual {v1, v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    goto :goto_0

    .line 149
    .end local v2    # "relativeSizeSpan":Landroid/text/style/RelativeSizeSpan;
    :cond_2
    const-class v5, Landroid/text/style/StyleSpan;

    if-ne v3, v5, :cond_0

    .line 150
    check-cast p1, Landroid/text/style/StyleSpan;

    .end local p1    # "span":Ljava/lang/Object;
    move-object v4, p1

    check-cast v4, Landroid/text/style/StyleSpan;

    .line 151
    .local v4, "styleSpan":Landroid/text/style/StyleSpan;
    const-string v5, "style_type"

    invoke-virtual {v4}, Landroid/text/style/StyleSpan;->getStyle()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    goto :goto_0
.end method

.method private static parseChapter(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    .locals 10
    .param p0, "chapter"    # Lorg/json/JSONObject;
    .param p1, "bookType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 200
    const/4 v8, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v8, :pswitch_data_0

    .line 235
    new-instance v8, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    const-string v9, "Unsupported epub_book formatToSeconds"

    invoke-direct {v8, v9}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 200
    :sswitch_0
    const-string v9, "fb2"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x0

    goto :goto_0

    :sswitch_1
    const-string v9, "epub"

    invoke-virtual {p1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x1

    goto :goto_0

    .line 202
    :pswitch_0
    new-instance v4, Landroid/text/SpannableString;

    const-string v8, "title"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v4, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 203
    .local v4, "spannedTitle":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/SpannableString;

    const-string v8, "content"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 205
    .local v3, "spannedContent":Landroid/text/SpannableString;
    const-string v8, "title_spans"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v6

    .line 206
    .local v6, "titleSpansJsonArray":Lorg/json/JSONArray;
    invoke-virtual {v6}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 208
    .local v7, "titleSpansJsonArrayLength":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_1

    .line 209
    invoke-virtual {v6, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    invoke-static {v4, v8}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->setSpan(Landroid/text/SpannableString;Lorg/json/JSONObject;)V

    .line 208
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 212
    :cond_1
    const-string v8, "content_spans"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 213
    .local v0, "contentSpansJsonArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 215
    .local v1, "contentSpansJsonArrayLength":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v1, :cond_2

    .line 216
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    invoke-static {v3, v8}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->setSpan(Landroid/text/SpannableString;Lorg/json/JSONObject;)V

    .line 215
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 219
    :cond_2
    new-instance v8, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;

    invoke-direct {v8, v4, v3}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 232
    .end local v4    # "spannedTitle":Landroid/text/SpannableString;
    .end local v6    # "titleSpansJsonArray":Lorg/json/JSONArray;
    .end local v7    # "titleSpansJsonArrayLength":I
    :goto_3
    return-object v8

    .line 222
    .end local v0    # "contentSpansJsonArray":Lorg/json/JSONArray;
    .end local v1    # "contentSpansJsonArrayLength":I
    .end local v2    # "i":I
    .end local v3    # "spannedContent":Landroid/text/SpannableString;
    :pswitch_1
    const-string v8, "title"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 223
    .local v5, "title":Ljava/lang/CharSequence;
    new-instance v3, Landroid/text/SpannableString;

    const-string v8, "content"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 225
    .restart local v3    # "spannedContent":Landroid/text/SpannableString;
    const-string v8, "content_spans"

    invoke-virtual {p0, v8}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 226
    .restart local v0    # "contentSpansJsonArray":Lorg/json/JSONArray;
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    .line 228
    .restart local v1    # "contentSpansJsonArrayLength":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_4
    if-ge v2, v1, :cond_3

    .line 229
    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/json/JSONObject;

    invoke-static {v3, v8}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->setSpan(Landroid/text/SpannableString;Lorg/json/JSONObject;)V

    .line 228
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 232
    :cond_3
    new-instance v8, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;

    invoke-direct {v8, v5, v3}, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 200
    nop

    :sswitch_data_0
    .sparse-switch
        0x18af6 -> :sswitch_0
        0x2f9c78 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static readBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 17
    .param p0, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .param p1, "bookIdDirectoryPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 168
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v3

    .line 169
    .local v3, "bookType":Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    sget-object v14, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 171
    .local v8, "chaptersDirectoryPath":Ljava/lang/String;
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 172
    .local v7, "chaptersDirectory":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v6

    .line 174
    .local v6, "chapterFiles":[Ljava/io/File;
    new-instance v1, Ljava/util/ArrayList;

    array-length v13, v6

    invoke-direct {v1, v13}, Ljava/util/ArrayList;-><init>(I)V

    .line 176
    .local v1, "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    array-length v14, v6

    const/4 v13, 0x0

    :goto_0
    if-ge v13, v14, :cond_1

    aget-object v5, v6, v13

    .line 177
    .local v5, "chapterFile":Ljava/io/File;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 178
    .local v11, "jsonStringBuilder":Ljava/lang/StringBuilder;
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v15, Ljava/io/InputStreamReader;

    new-instance v16, Ljava/io/FileInputStream;

    move-object/from16 v0, v16

    invoke-direct {v0, v5}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct/range {v15 .. v16}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v4, v15}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 181
    .local v4, "br":Ljava/io/BufferedReader;
    :goto_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v12

    .local v12, "line":Ljava/lang/String;
    if-eqz v12, :cond_0

    .line 182
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 194
    .end local v1    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v3    # "bookType":Ljava/lang/String;
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v5    # "chapterFile":Ljava/io/File;
    .end local v6    # "chapterFiles":[Ljava/io/File;
    .end local v7    # "chaptersDirectory":Ljava/io/File;
    .end local v8    # "chaptersDirectoryPath":Ljava/lang/String;
    .end local v11    # "jsonStringBuilder":Ljava/lang/StringBuilder;
    .end local v12    # "line":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 195
    .local v9, "e":Ljava/lang/Exception;
    :goto_2
    new-instance v13, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v13, v9}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v13

    .line 184
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v1    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .restart local v3    # "bookType":Ljava/lang/String;
    .restart local v4    # "br":Ljava/io/BufferedReader;
    .restart local v5    # "chapterFile":Ljava/io/File;
    .restart local v6    # "chapterFiles":[Ljava/io/File;
    .restart local v7    # "chaptersDirectory":Ljava/io/File;
    .restart local v8    # "chaptersDirectoryPath":Ljava/lang/String;
    .restart local v11    # "jsonStringBuilder":Ljava/lang/StringBuilder;
    .restart local v12    # "line":Ljava/lang/String;
    :cond_0
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 186
    new-instance v10, Lorg/json/JSONObject;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-direct {v10, v15}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 187
    .local v10, "jsonChapter":Lorg/json/JSONObject;
    invoke-static {v10, v3}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->parseChapter(Lorg/json/JSONObject;Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookChapter;

    move-result-object v15

    invoke-interface {v1, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v13, v13, 0x1

    goto :goto_0

    .line 190
    .end local v4    # "br":Ljava/io/BufferedReader;
    .end local v5    # "chapterFile":Ljava/io/File;
    .end local v10    # "jsonChapter":Lorg/json/JSONObject;
    .end local v11    # "jsonStringBuilder":Ljava/lang/StringBuilder;
    .end local v12    # "line":Ljava/lang/String;
    :cond_1
    new-instance v2, Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-direct {v2}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;-><init>()V

    .line 191
    .local v2, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    invoke-virtual {v2, v1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->setBookChapterList(Ljava/util/List;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 193
    return-object v2

    .line 194
    .end local v1    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v2    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .end local v3    # "bookType":Ljava/lang/String;
    .end local v6    # "chapterFiles":[Ljava/io/File;
    .end local v7    # "chaptersDirectory":Ljava/io/File;
    .end local v8    # "chaptersDirectoryPath":Ljava/lang/String;
    :catch_1
    move-exception v9

    goto :goto_2
.end method

.method public static saveBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/repository/entity/BookContent;Ljava/lang/String;)V
    .locals 6
    .param p0, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .param p1, "bookContent"    # Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .param p2, "bookIdDirectoryPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 44
    :try_start_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->getBookChapterList()Ljava/util/List;

    move-result-object v0

    .line 46
    .local v0, "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    invoke-static {p0, v0}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->convertChaptersToJson(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 48
    .local v2, "jsonChapters":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 49
    .local v3, "savePath":Ljava/lang/String;
    invoke-static {v2, v3}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->saveBookChapters(Ljava/util/List;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    return-void

    .line 50
    .end local v0    # "bookChapters":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    .end local v2    # "jsonChapters":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    .end local v3    # "savePath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Lorg/json/JSONException;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v4, v1}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method private static saveBookChapters(Ljava/util/List;Ljava/lang/String;)V
    .locals 9
    .param p1, "directoryPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/json/JSONObject;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 57
    .local p0, "jsonChapters":Ljava/util/List;, "Ljava/util/List<Lorg/json/JSONObject;>;"
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 58
    .local v0, "directory":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 60
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v5

    .line 62
    .local v5, "jsonChaptersCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_0

    .line 63
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 65
    .local v6, "savePath":Ljava/lang/String;
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 66
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 68
    new-instance v3, Ljava/io/FileWriter;

    invoke-direct {v3, v2}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    .line 69
    .local v3, "fileWriter":Ljava/io/FileWriter;
    invoke-interface {p0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/json/JSONObject;

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    .line 70
    invoke-virtual {v3}, Ljava/io/FileWriter;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 72
    .end local v0    # "directory":Ljava/io/File;
    .end local v2    # "file":Ljava/io/File;
    .end local v3    # "fileWriter":Ljava/io/FileWriter;
    .end local v4    # "i":I
    .end local v5    # "jsonChaptersCount":I
    .end local v6    # "savePath":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Ljava/io/IOException;
    new-instance v7, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v7, v1}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v7

    .line 75
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "directory":Ljava/io/File;
    .restart local v4    # "i":I
    .restart local v5    # "jsonChaptersCount":I
    :cond_0
    return-void
.end method

.method private static setSpan(Landroid/text/SpannableString;Lorg/json/JSONObject;)V
    .locals 9
    .param p0, "spannableString"    # Landroid/text/SpannableString;
    .param p1, "span"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/16 v8, 0x21

    .line 240
    const-string v6, "span_class"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 241
    .local v1, "className":Ljava/lang/String;
    const-string v6, "start_index"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 242
    .local v4, "startIndex":I
    const-string v6, "end_index"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 244
    .local v2, "endIndex":I
    const-class v6, Landroid/text/style/AlignmentSpan$Standard;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 245
    const-string v6, "alignment"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/text/Layout$Alignment;->valueOf(Ljava/lang/String;)Landroid/text/Layout$Alignment;

    move-result-object v0

    .line 247
    .local v0, "alignment":Landroid/text/Layout$Alignment;
    new-instance v6, Landroid/text/style/AlignmentSpan$Standard;

    invoke-direct {v6, v0}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 284
    .end local v0    # "alignment":Landroid/text/Layout$Alignment;
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    const-class v6, Landroid/text/style/RelativeSizeSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 252
    const-string v6, "relative_size"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    double-to-float v3, v6

    .line 254
    .local v3, "relativeSize":F
    new-instance v6, Landroid/text/style/RelativeSizeSpan;

    invoke-direct {v6, v3}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 258
    .end local v3    # "relativeSize":F
    :cond_2
    const-class v6, Landroid/text/style/StyleSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 259
    const-string v6, "style_type"

    invoke-virtual {p1, v6}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 261
    .local v5, "styleType":I
    new-instance v6, Landroid/text/style/StyleSpan;

    invoke-direct {v6, v5}, Landroid/text/style/StyleSpan;-><init>(I)V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 265
    .end local v5    # "styleType":I
    :cond_3
    const-class v6, Landroid/text/style/SubscriptSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 266
    new-instance v6, Landroid/text/style/SubscriptSpan;

    invoke-direct {v6}, Landroid/text/style/SubscriptSpan;-><init>()V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 270
    :cond_4
    const-class v6, Landroid/text/style/SuperscriptSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 271
    new-instance v6, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v6}, Landroid/text/style/SuperscriptSpan;-><init>()V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 275
    :cond_5
    const-class v6, Landroid/text/style/StrikethroughSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 276
    new-instance v6, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v6}, Landroid/text/style/StrikethroughSpan;-><init>()V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    .line 280
    :cond_6
    const-class v6, Landroid/text/style/UnderlineSpan;

    invoke-virtual {v6}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 281
    new-instance v6, Landroid/text/style/UnderlineSpan;

    invoke-direct {v6}, Landroid/text/style/UnderlineSpan;-><init>()V

    invoke-virtual {p0, v6, v4, v2, v8}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0
.end method
