.class public interface abstract Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;
.super Ljava/lang/Object;
.source "IFileExplorerPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
.implements Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;"
    }
.end annotation


# virtual methods
.method public abstract init()V
.end method

.method public abstract onItemClick(I)V
.end method
