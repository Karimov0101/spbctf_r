.class public interface abstract Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;
.super Ljava/lang/Object;
.source "ILibraryView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract dismissProgressDialog()V
.end method

.method public abstract removeItem(J)V
.end method

.method public abstract requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .param p1    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract requestToSetFileExplorerFragment()V
.end method

.method public abstract setItems(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setSpeedReadingBookInfo(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract showActionDialog(Ljava/lang/String;)V
.end method

.method public abstract showFormatDialog()V
.end method

.method public abstract showLoadingLibraryProgressDialog()V
.end method

.method public abstract showRemovingBookProgressDialog()V
.end method
