.class public Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;
.super Landroid/os/AsyncTask;
.source "BookContentLoaderAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
        "Ljava/lang/Void;",
        "Lcom/safonov/speedreading/reader/repository/entity/BookContent;",
        ">;"
    }
.end annotation


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;)V
    .locals 0
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 23
    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    .line 24
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 4
    .param p1, "bookDescriptions"    # [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    const/4 v1, 0x0

    .line 37
    :try_start_0
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/reader/repository/IBookController;->getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    :try_end_0
    .catch Lcom/safonov/speedreading/reader/repository/exception/BookParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 41
    :goto_0
    return-object v1

    .line 38
    :catch_0
    move-exception v0

    .line 39
    .local v0, "e":Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
    goto :goto_0

    .line 40
    .end local v0    # "e":Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
    :catch_1
    move-exception v0

    .line 41
    .local v0, "e":Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    check-cast p1, [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)V
    .locals 1
    .param p1, "bookContent"    # Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    .prologue
    .line 47
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 49
    if-nez p1, :cond_1

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;->onBookContentLoaderExecuteError()V

    .line 55
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;->onBookContentLoaderExecuteSuccess(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 16
    check-cast p1, Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->onPostExecute(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 28
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;->onBookContentLoaderPreExecute()V

    .line 32
    :cond_0
    return-void
.end method
