.class public interface abstract Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
.super Ljava/lang/Object;
.source "IBookDescriptionDao.java"


# virtual methods
.method public abstract addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J
.end method

.method public abstract findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
.end method

.method public abstract findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
.end method

.method public abstract getBookDescriptionList()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getNextItemId()J
.end method

.method public abstract removeBookDescription(J)V
.end method

.method public abstract updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method
