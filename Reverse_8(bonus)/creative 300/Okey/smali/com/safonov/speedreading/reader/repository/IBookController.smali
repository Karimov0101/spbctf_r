.class public interface abstract Lcom/safonov/speedreading/reader/repository/IBookController;
.super Ljava/lang/Object;
.source "IBookController.java"


# virtual methods
.method public abstract addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
        }
    .end annotation
.end method

.method public abstract findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
.end method

.method public abstract findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
.end method

.method public abstract getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
        }
    .end annotation
.end method

.method public abstract getBookDescriptionList()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation
.end method

.method public abstract removeBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method
