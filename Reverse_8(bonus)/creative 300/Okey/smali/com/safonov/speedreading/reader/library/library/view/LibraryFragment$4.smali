.class Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;
.super Ljava/lang/Object;
.source "LibraryFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->showActionDialog(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

.field final synthetic val$bookTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;Ljava/lang/String;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;->val$bookTitle:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 237
    if-nez p2, :cond_0

    .line 238
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 239
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 240
    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;->val$bookTitle:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 241
    const v2, 0x7f0e009d

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 243
    const v2, 0x7f0e009c

    new-instance v3, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4$1;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 252
    const v2, 0x7f0e009b

    new-instance v3, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4$2;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 260
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 261
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 263
    .end local v0    # "builder":Landroid/support/v7/app/AlertDialog$Builder;
    .end local v1    # "dialog":Landroid/support/v7/app/AlertDialog;
    :cond_0
    return-void
.end method
