.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->init(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 107
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v1

    aget-object v0, v0, v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->setRemainingTime(J)V

    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$208(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getRemainingTime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$302(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;J)J

    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showTimerModeDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$210(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showTimerModeCompletedDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "playedTime"    # J

    .prologue
    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setTimerView(J)V

    .line 117
    :cond_0
    return-void
.end method
