.class Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "FileExplorerFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field fileCoverImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09008e
    .end annotation
.end field

.field fileNameTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09008f
    .end annotation
.end field

.field fileTypeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090091
    .end annotation
.end field

.field private listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

.field final synthetic this$1:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;Landroid/view/View;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;)V
    .locals 1
    .param p1, "this$1"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->this$1:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

    .line 179
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 180
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 182
    iput-object p3, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    .line 183
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;->onItemClick(I)V

    .line 191
    :cond_0
    return-void
.end method
