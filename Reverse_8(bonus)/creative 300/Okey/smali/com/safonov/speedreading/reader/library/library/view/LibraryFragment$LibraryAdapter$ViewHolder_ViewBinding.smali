.class public Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "LibraryFragment$LibraryAdapter$ViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    .line 24
    const v0, 0x7f090041

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f09003f

    const-string v1, "field \'progressTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f090040

    const-string v1, "field \'bookReadButton\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookReadButton:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f09003e

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressBar:Landroid/widget/ProgressBar;

    .line 28
    const v0, 0x7f09003d

    const-string v1, "field \'bookImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookImageView:Landroid/widget/ImageView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    .line 35
    .local v0, "target":Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    .line 38
    iput-object v1, v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookReadButton:Landroid/widget/TextView;

    .line 41
    iput-object v1, v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressBar:Landroid/widget/ProgressBar;

    .line 42
    iput-object v1, v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookImageView:Landroid/widget/ImageView;

    .line 43
    return-void
.end method
