.class public Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
.super Ljava/lang/Object;
.source "SplittedBook.java"


# instance fields
.field private chapters:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;>;"
        }
    .end annotation
.end field

.field private final size:I

.field private titles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p1, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "chapters":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/CharSequence;>;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 19
    .local v4, "trimTitles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 20
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const-string v7, "[\\s]+"

    const-string v8, " "

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 21
    .local v0, "bufTitle":Ljava/lang/String;
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 22
    const-string v0, "..."

    .line 25
    :cond_0
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 28
    .end local v0    # "bufTitle":Ljava/lang/String;
    .end local v3    # "title":Ljava/lang/String;
    :cond_1
    iput-object v4, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->titles:Ljava/util/List;

    .line 29
    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->chapters:Ljava/util/List;

    .line 31
    const/4 v2, 0x0

    .line 32
    .local v2, "size":I
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    .line 33
    .local v1, "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    add-int/2addr v2, v6

    .line 34
    goto :goto_1

    .line 36
    .end local v1    # "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    :cond_2
    iput v2, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->size:I

    .line 37
    return-void
.end method


# virtual methods
.method public getPage(I)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "pageIndex"    # I

    .prologue
    .line 71
    move v2, p1

    .line 73
    .local v2, "offsetIndex":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->chapters:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 74
    .local v0, "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 76
    .local v1, "chapterPagesCount":I
    if-ge v2, v1, :cond_0

    .line 77
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/CharSequence;

    .line 83
    .end local v0    # "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    .end local v1    # "chapterPagesCount":I
    :goto_1
    return-object v3

    .line 79
    .restart local v0    # "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    .restart local v1    # "chapterPagesCount":I
    :cond_0
    sub-int/2addr v2, v1

    .line 81
    goto :goto_0

    .line 83
    .end local v0    # "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    .end local v1    # "chapterPagesCount":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getPageCount()I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->size:I

    return v0
.end method

.method public getPageIndexByTitle(I)I
    .locals 3
    .param p1, "titleIndex"    # I

    .prologue
    .line 57
    const/4 v1, 0x0

    .line 59
    .local v1, "pageIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 60
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->chapters:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_0
    return v1
.end method

.method public getTitle(I)Ljava/lang/String;
    .locals 5
    .param p1, "pageIndex"    # I

    .prologue
    .line 40
    const/4 v2, 0x0

    .line 41
    .local v2, "pageCount":I
    const/4 v1, 0x0

    .line 43
    .local v1, "lastTitleIndex":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->chapters:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 44
    .local v0, "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v2, v4

    .line 46
    if-lt p1, v2, :cond_0

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 49
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->titles:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 53
    .end local v0    # "chapter":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    :goto_1
    return-object v3

    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getTitles()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->titles:Ljava/util/List;

    return-object v0
.end method
