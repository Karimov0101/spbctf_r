.class public Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "LibraryPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;"
    }
.end annotation


# static fields
.field private static final SHOW_FORMAT_DIALOG_KEY:Ljava/lang/String; = "show_format_dialog"


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation
.end field

.field private removeBookId:J

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/IBookController;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 31
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 32
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 33
    return-void
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->itemList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)J
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    .prologue
    .line 23
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->removeBookId:J

    return-wide v0
.end method


# virtual methods
.method public init()V
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    new-instance v2, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;-><init>(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)V

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 55
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 56
    return-void
.end method

.method public onFormatDialogDismiss(Z)V
    .locals 3
    .param p1, "dontShowAgain"    # Z

    .prologue
    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "show_format_dialog"

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 118
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->requestToSetFileExplorerFragment()V

    .line 121
    :cond_0
    return-void

    .line 117
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemClick(J)V
    .locals 3
    .param p1, "bookId"    # J

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v1, p1, p2}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 63
    :cond_0
    return-void
.end method

.method public onItemLongClick(J)V
    .locals 3
    .param p1, "bookId"    # J

    .prologue
    .line 70
    iput-wide p1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->removeBookId:J

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v1, p1, p2}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->showActionDialog(Ljava/lang/String;)V

    .line 74
    :cond_0
    return-void
.end method

.method public requestToAddBook()V
    .locals 3

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "show_format_dialog"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->showFormatDialog()V

    .line 113
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->requestToSetFileExplorerFragment()V

    goto :goto_0
.end method

.method public requestToRemoveBook()V
    .locals 6

    .prologue
    .line 78
    new-instance v0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    new-instance v2, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;-><init>(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)V

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    iget-wide v4, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->removeBookId:J

    .line 93
    invoke-interface {v3, v4, v5}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 94
    return-void
.end method

.method public setSpeedReadingBookInfo(J)V
    .locals 3
    .param p1, "bookId"    # J

    .prologue
    .line 98
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v1, p1, p2}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    .line 99
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->setSpeedReadingBookInfo(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 100
    return-void
.end method
