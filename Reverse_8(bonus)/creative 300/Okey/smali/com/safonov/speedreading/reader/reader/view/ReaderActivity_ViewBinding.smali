.class public Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;
.super Ljava/lang/Object;
.source "ReaderActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

.field private view2131296526:Landroid/view/View;

.field private view2131296528:Landroid/view/View;

.field private view2131296529:Landroid/view/View;

.field private view2131296531:Landroid/view/View;

.field private view2131296532:Landroid/view/View;

.field private view2131296534:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;Landroid/view/View;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;Landroid/view/View;)V
    .locals 7
    .param p1, "target"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v6, 0x7f090111

    const v5, 0x7f09010e

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .line 45
    const v2, 0x7f09020d

    const-string v3, "field \'toolbar\'"

    const-class v4, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/Toolbar;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 46
    const v2, 0x7f090128

    const-string v3, "field \'pageTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pageTextView:Landroid/widget/TextView;

    .line 47
    const v2, 0x7f0901c7

    const-string v3, "field \'speedTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->speedTextView:Landroid/widget/TextView;

    .line 48
    const v2, 0x7f09014e

    const-string v3, "field \'progressBar\'"

    const-class v4, Landroid/widget/ProgressBar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 49
    const v2, 0x7f090067

    const-string v3, "field \'contentTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    .line 50
    const v2, 0x7f090118

    const-string v3, "field \'navigationView\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationView:Landroid/view/View;

    .line 51
    const v2, 0x7f090117

    const-string v3, "field \'navigationSeekBar\'"

    const-class v4, Landroid/support/v7/widget/AppCompatSeekBar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/AppCompatSeekBar;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;

    .line 52
    const-string v2, "field \'navigationPageTextView\' and method \'onNavigationPageViewClick\'"

    invoke-static {p2, v6, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 53
    .local v1, "view":Landroid/view/View;
    const-string v2, "field \'navigationPageTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {v1, v6, v2, v3}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationPageTextView:Landroid/widget/TextView;

    .line 54
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296529:Landroid/view/View;

    .line 55
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$1;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 61
    const-string v2, "field \'navigationChapterTitleTextView\' and method \'onNavigationChapterClick\'"

    invoke-static {p2, v5, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 62
    const-string v2, "field \'navigationChapterTitleTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {v1, v5, v2, v3}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationChapterTitleTextView:Landroid/widget/TextView;

    .line 63
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296526:Landroid/view/View;

    .line 64
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$2;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$2;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v2, 0x7f090135

    const-string v3, "field \'frameView\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameView:Landroid/view/View;

    .line 71
    const v2, 0x7f090134

    const-string v3, "field \'frameLayout\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameLayout:Landroid/view/View;

    .line 72
    const v2, 0x7f090112

    const-string v3, "field \'pencilFrameLayout\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pencilFrameLayout:Landroid/view/View;

    .line 73
    const v2, 0x7f090115

    const-string v3, "field \'lineCountTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->lineCountTextView:Landroid/widget/TextView;

    .line 74
    const v2, 0x7f090207

    const-string v3, "field \'timeTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->timeTextView:Landroid/widget/TextView;

    .line 75
    const v2, 0x7f090113

    const-string v3, "method \'onMib\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 76
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296531:Landroid/view/View;

    .line 77
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$3;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$3;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    const v2, 0x7f090114

    const-string v3, "method \'onPlb\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 84
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296532:Landroid/view/View;

    .line 85
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$4;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$4;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v2, 0x7f090116

    const-string v3, "method \'onNavigationPreviousPageClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 92
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296534:Landroid/view/View;

    .line 93
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$5;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$5;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    const v2, 0x7f090110

    const-string v3, "method \'onNavigationNextPageClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 100
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296528:Landroid/view/View;

    .line 101
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$6;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding$6;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 108
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 109
    .local v0, "context":Landroid/content/Context;
    const v2, 0x1060003

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->blackColor:I

    .line 110
    const v2, 0x1060005

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->greyColor:I

    .line 111
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 116
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .line 117
    .local v0, "target":Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 118
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->target:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .line 120
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 121
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pageTextView:Landroid/widget/TextView;

    .line 122
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->speedTextView:Landroid/widget/TextView;

    .line 123
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressBar:Landroid/widget/ProgressBar;

    .line 124
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    .line 125
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationView:Landroid/view/View;

    .line 126
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;

    .line 127
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationPageTextView:Landroid/widget/TextView;

    .line 128
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationChapterTitleTextView:Landroid/widget/TextView;

    .line 129
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameView:Landroid/view/View;

    .line 130
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameLayout:Landroid/view/View;

    .line 131
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pencilFrameLayout:Landroid/view/View;

    .line 132
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->lineCountTextView:Landroid/widget/TextView;

    .line 133
    iput-object v2, v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->timeTextView:Landroid/widget/TextView;

    .line 135
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296529:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296529:Landroid/view/View;

    .line 137
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296526:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296526:Landroid/view/View;

    .line 139
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296531:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296531:Landroid/view/View;

    .line 141
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296532:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296532:Landroid/view/View;

    .line 143
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296534:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 144
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296534:Landroid/view/View;

    .line 145
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296528:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity_ViewBinding;->view2131296528:Landroid/view/View;

    .line 147
    return-void
.end method
