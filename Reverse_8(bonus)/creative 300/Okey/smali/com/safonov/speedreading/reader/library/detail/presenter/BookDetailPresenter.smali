.class public Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "BookDetailPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public init(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    const/4 v3, 0x0

    .line 19
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getCoverImagePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 21
    .local v0, "bookCoverBitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 22
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setCoverView(Landroid/graphics/Bitmap;)V

    .line 25
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setTitleView(Ljava/lang/String;)V

    .line 27
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getAuthor()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 28
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-interface {v1, v3}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setAuthorViewVisibility(Z)V

    .line 33
    :goto_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-interface {v1, v3}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setLanguageViewVisibility(Z)V

    .line 39
    :goto_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setFilePathView(Ljava/lang/String;)V

    .line 41
    .end local v0    # "bookCoverBitmap":Landroid/graphics/Bitmap;
    :cond_1
    return-void

    .line 30
    .restart local v0    # "bookCoverBitmap":Landroid/graphics/Bitmap;
    :cond_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setAuthorView(Ljava/lang/String;)V

    goto :goto_0

    .line 36
    :cond_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;->setLanguageView(Ljava/lang/String;)V

    goto :goto_1
.end method
