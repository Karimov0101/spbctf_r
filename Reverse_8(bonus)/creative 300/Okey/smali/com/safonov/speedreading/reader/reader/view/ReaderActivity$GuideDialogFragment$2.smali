.class Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;
.super Ljava/lang/Object;
.source "ReaderActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

.field final synthetic val$dontShowAgainCheckBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;Landroid/widget/CheckBox;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    .prologue
    .line 678
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->val$dontShowAgainCheckBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 681
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->dismiss()V

    .line 682
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->access$600(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;)Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 683
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->access$600(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;)Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;

    move-result-object v1

    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;->val$dontShowAgainCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;->dismiss(Z)V

    .line 685
    :cond_0
    return-void

    .line 683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
