.class Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "LibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "LibraryAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;,
        Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private bookDescriptionList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation
.end field

.field private context:Landroid/content/Context;

.field private itemClickListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

.field sharedPreferences:Landroid/content/SharedPreferences;

.field speedReadingBookId:J


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "itemClickListener"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    .prologue
    .line 544
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 540
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    .line 545
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->context:Landroid/content/Context;

    .line 546
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->itemClickListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    .line 547
    const-string v0, "load_book"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 548
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "book_id"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->speedReadingBookId:J

    .line 549
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;
    .param p2, "x1"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;
    .param p3, "x2"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$1;

    .prologue
    .line 496
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;)V

    return-void
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 634
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 496
    check-cast p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->onBindViewHolder(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;I)V
    .locals 11
    .param p1, "holder"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 585
    iget-object v5, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    invoke-interface {v5, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 588
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 589
    .local v1, "bookTitle":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 590
    invoke-virtual {v1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v3

    .line 594
    .local v3, "firstLetter":C
    :goto_0
    invoke-static {v3}, Lcom/safonov/speedreading/reader/library/library/view/LibraryCoverColorHelper;->getBookCoverColor(C)I

    move-result v2

    .line 596
    .local v2, "coverColor":I
    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 597
    .local v4, "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v4, v7}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 598
    invoke-virtual {v4, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 601
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-ge v5, v8, :cond_0

    .line 607
    :cond_0
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 610
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v8

    const/4 v5, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v5, :pswitch_data_0

    .line 623
    :goto_2
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v5

    if-lez v5, :cond_2

    .line 624
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressTextView:Landroid/widget/TextView;

    iget-object v8, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->context:Landroid/content/Context;

    const v9, 0x7f0e009a

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v7, v6

    invoke-virtual {v8, v9, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 625
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 626
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressTextView:Landroid/widget/TextView;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 627
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v5, v6}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 628
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookReadButton:Landroid/widget/TextView;

    const/4 v6, 0x4

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 630
    :cond_2
    return-void

    .line 592
    .end local v2    # "coverColor":I
    .end local v3    # "firstLetter":C
    .end local v4    # "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    :cond_3
    const/16 v3, 0x2d

    .restart local v3    # "firstLetter":C
    goto :goto_0

    .line 610
    .restart local v2    # "coverColor":I
    .restart local v4    # "gradientDrawable":Landroid/graphics/drawable/GradientDrawable;
    :sswitch_0
    const-string v9, "epub"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v5, v6

    goto :goto_1

    :sswitch_1
    const-string v9, "txt"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move v5, v7

    goto :goto_1

    :sswitch_2
    const-string v9, "fb2"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v5, 0x2

    goto :goto_1

    :sswitch_3
    const-string v9, "fb2.zip"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v5, 0x3

    goto :goto_1

    .line 612
    :pswitch_0
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookImageView:Landroid/widget/ImageView;

    const v8, 0x7f08008c

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 615
    :pswitch_1
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookImageView:Landroid/widget/ImageView;

    const v8, 0x7f080090

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 619
    :pswitch_2
    iget-object v5, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->bookImageView:Landroid/widget/ImageView;

    const v8, 0x7f08008d

    invoke-virtual {v5, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2

    .line 610
    nop

    :sswitch_data_0
    .sparse-switch
        -0x42321fd7 -> :sswitch_3
        0x18af6 -> :sswitch_2
        0x1c270 -> :sswitch_1
        0x2f9c78 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 496
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 577
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0094

    const/4 v3, 0x0

    .line 578
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 580
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->itemClickListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    invoke-direct {v1, p0, v0, v2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;Landroid/view/View;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;)V

    return-object v1
.end method

.method public removeItem(J)V
    .locals 7
    .param p1, "bookId"    # J

    .prologue
    .line 561
    iget-wide v4, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->speedReadingBookId:J

    cmp-long v3, p1, v4

    if-eqz v3, :cond_2

    .line 562
    const/4 v1, 0x0

    .line 563
    .local v1, "index":I
    const/4 v2, 0x0

    .line 564
    .local v2, "removeIndex":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 565
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 566
    move v2, v1

    .line 568
    :cond_0
    add-int/lit8 v1, v1, 0x1

    .line 569
    goto :goto_0

    .line 570
    .end local v0    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 571
    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->notifyItemRemoved(I)V

    .line 573
    .end local v1    # "index":I
    .end local v2    # "removeIndex":I
    :cond_2
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 552
    .local p1, "bookDescriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookDescription;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 553
    .local v0, "description":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->speedReadingBookId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 554
    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->bookDescriptionList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 557
    .end local v0    # "description":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->notifyDataSetChanged()V

    .line 558
    return-void
.end method
