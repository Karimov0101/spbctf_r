.class public Lcom/safonov/speedreading/reader/repository/util/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# static fields
.field public static final EPUB:Ljava/lang/String; = "epub"

.field public static final FB2:Ljava/lang/String; = "fb2"

.field public static final FB2_ZIP:Ljava/lang/String; = "fb2.zip"

.field public static final TXT:Ljava/lang/String; = "txt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static copyFile(Ljava/io/File;Ljava/io/File;)V
    .locals 7
    .param p0, "source"    # Ljava/io/File;
    .param p1, "dest"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 185
    const/4 v1, 0x0

    .line 186
    .local v1, "is":Ljava/io/InputStream;
    const/4 v4, 0x0

    .line 188
    .local v4, "os":Ljava/io/OutputStream;
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 189
    .end local v1    # "is":Ljava/io/InputStream;
    .local v2, "is":Ljava/io/InputStream;
    :try_start_1
    new-instance v5, Ljava/io/FileOutputStream;

    invoke-direct {v5, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 190
    .end local v4    # "os":Ljava/io/OutputStream;
    .local v5, "os":Ljava/io/OutputStream;
    const/16 v6, 0x400

    :try_start_2
    new-array v0, v6, [B

    .line 192
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v3

    .local v3, "length":I
    if-lez v3, :cond_2

    .line 193
    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 196
    .end local v0    # "buffer":[B
    .end local v3    # "length":I
    :catchall_0
    move-exception v6

    move-object v4, v5

    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v4    # "os":Ljava/io/OutputStream;
    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    :goto_1
    if-eqz v1, :cond_0

    .line 197
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 199
    :cond_0
    if-eqz v4, :cond_1

    .line 200
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v6

    .line 196
    .end local v1    # "is":Ljava/io/InputStream;
    .end local v4    # "os":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "is":Ljava/io/InputStream;
    .restart local v3    # "length":I
    .restart local v5    # "os":Ljava/io/OutputStream;
    :cond_2
    if-eqz v2, :cond_3

    .line 197
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    .line 199
    :cond_3
    if-eqz v5, :cond_4

    .line 200
    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V

    .line 203
    :cond_4
    return-void

    .line 196
    .end local v0    # "buffer":[B
    .end local v2    # "is":Ljava/io/InputStream;
    .end local v3    # "length":I
    .end local v5    # "os":Ljava/io/OutputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    .restart local v4    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v6

    goto :goto_1

    .end local v1    # "is":Ljava/io/InputStream;
    .restart local v2    # "is":Ljava/io/InputStream;
    :catchall_2
    move-exception v6

    move-object v1, v2

    .end local v2    # "is":Ljava/io/InputStream;
    .restart local v1    # "is":Ljava/io/InputStream;
    goto :goto_1
.end method

.method public static copyFile(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 5
    .param p0, "inputStreamSource"    # Ljava/io/InputStream;
    .param p1, "dest"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 207
    const/4 v2, 0x0

    .line 209
    .local v2, "os":Ljava/io/OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210
    .end local v2    # "os":Ljava/io/OutputStream;
    .local v3, "os":Ljava/io/OutputStream;
    const/16 v4, 0x400

    :try_start_1
    new-array v0, v4, [B

    .line 212
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "length":I
    if-lez v1, :cond_2

    .line 213
    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4, v1}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 216
    .end local v0    # "buffer":[B
    .end local v1    # "length":I
    :catchall_0
    move-exception v4

    move-object v2, v3

    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :goto_1
    if-eqz p0, :cond_0

    .line 217
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 219
    :cond_0
    if-eqz v2, :cond_1

    .line 220
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_1
    throw v4

    .line 216
    .end local v2    # "os":Ljava/io/OutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "length":I
    .restart local v3    # "os":Ljava/io/OutputStream;
    :cond_2
    if-eqz p0, :cond_3

    .line 217
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 219
    :cond_3
    if-eqz v3, :cond_4

    .line 220
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V

    .line 223
    :cond_4
    return-void

    .line 216
    .end local v0    # "buffer":[B
    .end local v1    # "length":I
    .end local v3    # "os":Ljava/io/OutputStream;
    .restart local v2    # "os":Ljava/io/OutputStream;
    :catchall_1
    move-exception v4

    goto :goto_1
.end method

.method public static getEncoding(Ljava/io/File;)Ljava/lang/String;
    .locals 6
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 70
    const/4 v3, 0x0

    .line 71
    .local v3, "fis":Ljava/io/FileInputStream;
    new-instance v1, Lorg/mozilla/universalchardet/UniversalDetector;

    const/4 v5, 0x0

    invoke-direct {v1, v5}, Lorg/mozilla/universalchardet/UniversalDetector;-><init>(Lorg/mozilla/universalchardet/CharsetListener;)V

    .line 72
    .local v1, "detector":Lorg/mozilla/universalchardet/UniversalDetector;
    const/4 v2, 0x0

    .line 73
    .local v2, "encoding":Ljava/lang/String;
    const/16 v5, 0x1000

    new-array v0, v5, [B

    .line 74
    .local v0, "buf":[B
    new-instance v3, Ljava/io/FileInputStream;

    .end local v3    # "fis":Ljava/io/FileInputStream;
    invoke-direct {v3, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 76
    .restart local v3    # "fis":Ljava/io/FileInputStream;
    :goto_0
    invoke-virtual {v3, v0}, Ljava/io/FileInputStream;->read([B)I

    move-result v4

    .local v4, "nread":I
    if-lez v4, :cond_0

    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->isDone()Z

    move-result v5

    if-nez v5, :cond_0

    .line 77
    const/4 v5, 0x0

    invoke-virtual {v1, v0, v5, v4}, Lorg/mozilla/universalchardet/UniversalDetector;->handleData([BII)V

    goto :goto_0

    .line 79
    :cond_0
    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->dataEnd()V

    .line 80
    invoke-virtual {v1}, Lorg/mozilla/universalchardet/UniversalDetector;->getDetectedCharset()Ljava/lang/String;

    move-result-object v2

    .line 82
    return-object v2
.end method

.method public static getFileExtension(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 31
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x2e

    .line 47
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 48
    .local v1, "fileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 50
    .local v0, "extension":Ljava/lang/String;
    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 52
    .local v2, "i":I
    if-lez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_0

    .line 53
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 55
    const-string v4, "zip"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    const-string v4, "fb2.zip"

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 58
    .local v3, "zipIndex":I
    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "fb2.zip"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    if-ne v3, v4, :cond_0

    .line 59
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_0

    .line 60
    const-string v4, "fb2.zip"

    .line 66
    .end local v3    # "zipIndex":I
    :goto_0
    return-object v4

    :cond_0
    move-object v4, v0

    goto :goto_0
.end method

.method public static getFileName(Ljava/io/File;)Ljava/lang/String;
    .locals 3
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 36
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 37
    .local v0, "fileName":Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    .line 42
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    .line 43
    .local v0, "fileName":Ljava/lang/String;
    const/4 v1, 0x0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getFilesCollection(Ljava/lang/String;)Ljava/util/List;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 244
    .local v2, "root":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v3

    .line 245
    .local v3, "tempFiles":[Ljava/io/File;
    if-nez v3, :cond_1

    .line 246
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 258
    :cond_0
    return-object v1

    .line 248
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 250
    .local v1, "files":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    array-length v5, v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v5, :cond_0

    aget-object v0, v3, v4

    .line 251
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isFile()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 252
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 254
    :cond_2
    invoke-static {v0, v1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFilesRecursive(Ljava/io/File;Ljava/util/List;)Ljava/util/List;

    goto :goto_1
.end method

.method private static getFilesRecursive(Ljava/io/File;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "outFilesList":Ljava/util/List;, "Ljava/util/List<Ljava/io/File;>;"
    invoke-virtual {p0}, Ljava/io/File;->isFile()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 263
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    :cond_0
    return-object p1

    .line 268
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 269
    .local v1, "tempFiles":[Ljava/io/File;
    if-eqz v1, :cond_0

    .line 272
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v0, v1, v2

    .line 273
    .local v0, "file1":Ljava/io/File;
    invoke-static {v0, p1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFilesRecursive(Ljava/io/File;Ljava/util/List;)Ljava/util/List;

    .line 272
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getTextFromFile(Ljava/io/File;)Ljava/lang/String;
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 104
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 106
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTextFromFile(Ljava/io/File;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "file"    # Ljava/io/File;
    .param p1, "charset"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 86
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 88
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTextFromFile(Ljava/io/InputStream;)Ljava/lang/String;
    .locals 2
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 116
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 118
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTextFromFile(Ljava/io/InputStream;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .param p1, "charset"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 98
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, p0, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 100
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;
    .locals 4
    .param p0, "streamReader"    # Ljava/io/InputStreamReader;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 123
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    .local v2, "text":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 128
    .local v0, "br":Ljava/io/BufferedReader;
    :goto_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v1

    .local v1, "line":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 129
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 132
    :cond_0
    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    .line 134
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public static getTextFromFile(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 110
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 112
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTextFromFile(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "charset"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Ljava/io/InputStreamReader;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 94
    .local v0, "streamReader":Ljava/io/InputStreamReader;
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getTextFromFile(Ljava/io/InputStreamReader;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static removeDirectory(Ljava/io/File;)Z
    .locals 5
    .param p0, "directory"    # Ljava/io/File;

    .prologue
    .line 226
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 227
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 228
    .local v1, "files":[Ljava/io/File;
    if-eqz v1, :cond_1

    .line 229
    array-length v3, v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, v1, v2

    .line 230
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 231
    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->removeDirectory(Ljava/io/File;)Z

    .line 229
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 234
    :cond_0
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_1

    .line 239
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "files":[Ljava/io/File;
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    move-result v2

    return v2
.end method

.method public static unZip(Ljava/lang/String;Ljava/lang/String;)V
    .locals 16
    .param p0, "zipFilePath"    # Ljava/lang/String;
    .param p1, "targetDirectory"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 138
    new-instance v13, Ljava/util/zip/ZipFile;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    .line 140
    .local v13, "zipFile":Ljava/util/zip/ZipFile;
    const/16 v1, 0x1000

    .line 142
    .local v1, "BUFFER":I
    new-instance v10, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 143
    .local v10, "rootDirectory":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v14

    if-nez v14, :cond_1

    .line 144
    invoke-virtual {v10}, Ljava/io/File;->mkdir()Z

    .line 152
    :goto_0
    invoke-virtual {v13}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v14

    invoke-static {v14}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v11

    .line 154
    .local v11, "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :cond_0
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/util/zip/ZipEntry;

    .line 155
    .local v12, "zipEntry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v15

    if-nez v15, :cond_0

    .line 157
    new-instance v5, Ljava/io/File;

    invoke-virtual {v12}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-direct {v5, v0, v15}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v9

    .line 161
    .local v9, "parentPath":Ljava/lang/String;
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 162
    .local v8, "parentDirectories":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->mkdirs()Z

    .line 165
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 166
    .local v6, "fileOutputStream":Ljava/io/FileOutputStream;
    new-instance v2, Ljava/io/BufferedOutputStream;

    invoke-direct {v2, v6, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    .line 169
    .local v2, "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    new-array v4, v1, [B

    .line 171
    .local v4, "data":[B
    invoke-virtual {v13, v12}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v7

    .line 173
    .local v7, "inputStream":Ljava/io/InputStream;
    :goto_2
    const/4 v15, 0x0

    invoke-virtual {v7, v4, v15, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .local v3, "currentByte":I
    const/4 v15, -0x1

    if-eq v3, v15, :cond_2

    .line 174
    const/4 v15, 0x0

    invoke-virtual {v2, v4, v15, v3}, Ljava/io/BufferedOutputStream;->write([BII)V

    goto :goto_2

    .line 146
    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "currentByte":I
    .end local v4    # "data":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .end local v8    # "parentDirectories":Ljava/io/File;
    .end local v9    # "parentPath":Ljava/lang/String;
    .end local v11    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .end local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_1
    invoke-static {v10}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->removeDirectory(Ljava/io/File;)Z

    .line 147
    invoke-virtual {v10}, Ljava/io/File;->mkdir()Z

    goto :goto_0

    .line 177
    .restart local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .restart local v3    # "currentByte":I
    .restart local v4    # "data":[B
    .restart local v5    # "file":Ljava/io/File;
    .restart local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .restart local v7    # "inputStream":Ljava/io/InputStream;
    .restart local v8    # "parentDirectories":Ljava/io/File;
    .restart local v9    # "parentPath":Ljava/lang/String;
    .restart local v11    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .restart local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_2
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->flush()V

    .line 178
    invoke-virtual {v2}, Ljava/io/BufferedOutputStream;->close()V

    goto :goto_1

    .line 181
    .end local v2    # "bufferedOutputStream":Ljava/io/BufferedOutputStream;
    .end local v3    # "currentByte":I
    .end local v4    # "data":[B
    .end local v5    # "file":Ljava/io/File;
    .end local v6    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v7    # "inputStream":Ljava/io/InputStream;
    .end local v8    # "parentDirectories":Ljava/io/File;
    .end local v9    # "parentPath":Ljava/lang/String;
    .end local v12    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_3
    invoke-virtual {v13}, Ljava/util/zip/ZipFile;->close()V

    .line 182
    return-void
.end method
