.class public Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;
.super Ljava/lang/Object;
.source "FileExplorerFileWrapper.java"


# instance fields
.field private file:Ljava/io/File;

.field private type:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;


# direct methods
.method public constructor <init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V
    .locals 0
    .param p1, "file"    # Ljava/io/File;
    .param p2, "type"    # Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->file:Ljava/io/File;

    .line 16
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->type:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    .line 17
    return-void
.end method


# virtual methods
.method public getFile()Ljava/io/File;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->file:Ljava/io/File;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->type:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    return-object v0
.end method
