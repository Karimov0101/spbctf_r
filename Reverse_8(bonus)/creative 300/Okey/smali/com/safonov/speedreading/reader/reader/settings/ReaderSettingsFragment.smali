.class public Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "ReaderSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "s"    # Ljava/lang/String;

    .prologue
    .line 19
    const v0, 0x7f110006

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;->addPreferencesFromResource(I)V

    .line 20
    return-void
.end method
