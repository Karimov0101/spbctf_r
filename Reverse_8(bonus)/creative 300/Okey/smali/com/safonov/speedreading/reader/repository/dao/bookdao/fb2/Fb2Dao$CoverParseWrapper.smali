.class Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;
.super Ljava/lang/Object;
.source "Fb2Dao.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CoverParseWrapper"
.end annotation


# instance fields
.field private final binaryCover:Ljava/lang/String;

.field private final fileName:Ljava/lang/String;

.field final synthetic this$0:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "binaryCover"    # Ljava/lang/String;
    .param p3, "fileName"    # Ljava/lang/String;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->this$0:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->binaryCover:Ljava/lang/String;

    .line 170
    iput-object p3, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->fileName:Ljava/lang/String;

    .line 171
    return-void
.end method


# virtual methods
.method public getBinaryCover()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->binaryCover:Ljava/lang/String;

    return-object v0
.end method

.method public getFileName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->fileName:Ljava/lang/String;

    return-object v0
.end method
