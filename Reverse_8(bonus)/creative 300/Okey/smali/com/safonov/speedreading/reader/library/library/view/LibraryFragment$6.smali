.class Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;
.super Ljava/lang/Object;
.source "LibraryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->onRequestPermissionsResult(I[Ljava/lang/String;[I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 457
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 460
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.APPLICATION_DETAILS_SETTINGS"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "package:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    invoke-virtual {v3}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 461
    .local v0, "appSettingsIntent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->startActivity(Landroid/content/Intent;)V

    .line 462
    return-void
.end method
