.class public Lcom/safonov/speedreading/reader/repository/BookController;
.super Ljava/lang/Object;
.source "BookController.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/IBookController;


# instance fields
.field private bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

.field private epubDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;

.field private fb2Dao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;

.field private txtDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V
    .locals 0
    .param p1, "bookDescriptionDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "fb2Dao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "epubDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "txtDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/BookController;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    .line 38
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/BookController;->fb2Dao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;

    .line 39
    iput-object p3, p0, Lcom/safonov/speedreading/reader/repository/BookController;->epubDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;

    .line 40
    iput-object p4, p0, Lcom/safonov/speedreading/reader/repository/BookController;->txtDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;

    .line 41
    return-void
.end method


# virtual methods
.method public addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
        }
    .end annotation

    .prologue
    .line 48
    invoke-static {p1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 57
    new-instance v0, Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;-><init>()V

    throw v0

    .line 48
    :sswitch_0
    const-string v2, "fb2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "fb2.zip"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "epub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "txt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x3

    goto :goto_0

    .line 51
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->fb2Dao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;->addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    .line 55
    :goto_1
    return-object v0

    .line 53
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->epubDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;->addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    goto :goto_1

    .line 55
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->txtDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;->addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    goto :goto_1

    .line 48
    :sswitch_data_0
    .sparse-switch
        -0x42321fd7 -> :sswitch_1
        0x18af6 -> :sswitch_0
        0x1c270 -> :sswitch_3
        0x2f9c78 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 83
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    return-object v0
.end method

.method public findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    return-object v0
.end method

.method public getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 3
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 72
    new-instance v0, Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;-><init>()V

    throw v0

    .line 64
    :sswitch_0
    const-string v2, "fb2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "epub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "txt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 66
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->fb2Dao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;->getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v0

    .line 70
    :goto_1
    return-object v0

    .line 68
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->epubDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;->getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v0

    goto :goto_1

    .line 70
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->txtDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;->getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v0

    goto :goto_1

    .line 64
    :sswitch_data_0
    .sparse-switch
        0x18af6 -> :sswitch_0
        0x1c270 -> :sswitch_2
        0x2f9c78 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getBookDescriptionList()Ljava/util/List;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getBookDescriptionList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public removeBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 99
    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v1

    const/4 v0, -0x1

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 110
    :goto_1
    return-void

    .line 99
    :sswitch_0
    const-string v2, "fb2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "epub"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v2, "txt"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 101
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->fb2Dao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;->removeBook(J)V

    goto :goto_1

    .line 104
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->epubDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;->removeBook(J)V

    goto :goto_1

    .line 107
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->txtDao:Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;->removeBook(J)V

    goto :goto_1

    .line 99
    :sswitch_data_0
    .sparse-switch
        0x18af6 -> :sswitch_0
        0x1c270 -> :sswitch_2
        0x2f9c78 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/BookController;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 95
    return-void
.end method
