.class Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "LibraryFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field bookImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09003d
    .end annotation
.end field

.field bookReadButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090040
    .end annotation
.end field

.field private listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09003e
    .end annotation
.end field

.field progressTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09003f
    .end annotation
.end field

.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090041
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;Landroid/view/View;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "listener"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    .prologue
    .line 514
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    .line 515
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 516
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 517
    iput-object p3, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    .line 518
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 519
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 520
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 524
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->access$700(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;->onItemClick(J)V

    .line 527
    :cond_0
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 531
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    if-eqz v0, :cond_0

    .line 532
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->listener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;

    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->this$0:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->access$700(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ViewHolder;->getAdapterPosition()I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;->onItemLongClick(J)V

    .line 534
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
