.class public interface abstract Lcom/safonov/speedreading/reader/repository/dao/bookdao/IBookDao;
.super Ljava/lang/Object;
.source "IBookDao.java"


# virtual methods
.method public abstract addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
        }
    .end annotation
.end method

.method public abstract getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation
.end method

.method public abstract removeBook(J)V
.end method
