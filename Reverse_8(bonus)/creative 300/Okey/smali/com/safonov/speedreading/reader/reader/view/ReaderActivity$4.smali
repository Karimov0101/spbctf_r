.class Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;
.super Ljava/lang/Object;
.source "ReaderActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->showSelectPageDialog(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

.field final synthetic val$numberPicker:Landroid/widget/NumberPicker;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;Landroid/widget/NumberPicker;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 420
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;->val$numberPicker:Landroid/widget/NumberPicker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialogInterface"    # Landroid/content/DialogInterface;
    .param p2, "i"    # I

    .prologue
    .line 423
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;->val$numberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->clearFocus()V

    .line 424
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$200(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;->val$numberPicker:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onNavigationPageSelected(I)V

    .line 425
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 426
    return-void
.end method
