.class public Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "ReaderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GuideDialogFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;
    }
.end annotation


# static fields
.field private static final FRAGMENT_COUNT:I = 0x4


# instance fields
.field private dismissListener:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 617
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;)Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    .prologue
    .line 617
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->dismissListener:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 646
    const v6, 0x7f0b008b

    const/4 v7, 0x0

    invoke-virtual {p1, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 648
    .local v4, "view":Landroid/view/View;
    const v6, 0x7f090243

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/support/v4/view/ViewPager;

    .line 649
    .local v5, "viewPager":Landroid/support/v4/view/ViewPager;
    new-instance v6, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$1;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v7

    invoke-direct {v6, p0, v7}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$1;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;Landroid/support/v4/app/FragmentManager;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 669
    const v6, 0x7f0901dc

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/design/widget/TabLayout;

    .line 670
    .local v3, "tabLayout":Landroid/support/design/widget/TabLayout;
    invoke-virtual {v3, v5}, Landroid/support/design/widget/TabLayout;->setupWithViewPager(Landroid/support/v4/view/ViewPager;)V

    .line 671
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v6, 0x4

    if-ge v2, v6, :cond_0

    .line 672
    invoke-virtual {v3, v2}, Landroid/support/design/widget/TabLayout;->getTabAt(I)Landroid/support/design/widget/TabLayout$Tab;

    move-result-object v6

    add-int/lit8 v7, v2, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/support/design/widget/TabLayout$Tab;->setText(Ljava/lang/CharSequence;)Landroid/support/design/widget/TabLayout$Tab;

    .line 671
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 675
    :cond_0
    const v6, 0x7f09007f

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 677
    .local v1, "dontShowAgainCheckBox":Landroid/widget/CheckBox;
    const v6, 0x7f090053

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 678
    .local v0, "closeButton":Landroid/widget/Button;
    new-instance v6, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;

    invoke-direct {v6, p0, v1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$2;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 688
    return-object v4
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 630
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 632
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 633
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 635
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    check-cast v0, Landroid/view/WindowManager$LayoutParams;

    .end local v0    # "params":Landroid/view/ViewGroup$LayoutParams;
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 637
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 638
    return-void
.end method

.method public setDismissListener(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;)V
    .locals 0
    .param p1, "dismissListener"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;

    .prologue
    .line 625
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->dismissListener:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;

    .line 626
    return-void
.end method
