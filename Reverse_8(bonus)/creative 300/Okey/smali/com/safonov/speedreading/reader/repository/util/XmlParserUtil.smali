.class public Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;
.super Ljava/lang/Object;
.source "XmlParserUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getXmlFromFile(Ljava/io/File;)Lorg/w3c/dom/Document;
    .locals 5
    .param p0, "file"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 20
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 21
    .local v2, "documentBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 23
    .local v1, "documentBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v1, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 24
    .local v0, "document":Lorg/w3c/dom/Document;
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Element;->normalize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 26
    return-object v0

    .line 27
    .end local v0    # "document":Lorg/w3c/dom/Document;
    .end local v1    # "documentBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "documentBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_0
    move-exception v3

    .line 28
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v4, v3}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public static getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    .locals 5
    .param p0, "inputStream"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 34
    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v2

    .line 35
    .local v2, "documentBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    invoke-virtual {v2}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v1

    .line 37
    .local v1, "documentBuilder":Ljavax/xml/parsers/DocumentBuilder;
    invoke-virtual {v1, p0}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    .line 38
    .local v0, "document":Lorg/w3c/dom/Document;
    invoke-interface {v0}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Element;->normalize()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 39
    return-object v0

    .line 40
    .end local v0    # "document":Lorg/w3c/dom/Document;
    .end local v1    # "documentBuilder":Ljavax/xml/parsers/DocumentBuilder;
    .end local v2    # "documentBuilderFactory":Ljavax/xml/parsers/DocumentBuilderFactory;
    :catch_0
    move-exception v3

    .line 41
    .local v3, "e":Ljava/lang/Exception;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v4, v3}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method
