.class public Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;
.source "ReaderActivity.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/reader/view/IReaderView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$PencilFrameReadingGuideFragment;,
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FlashReadingGuideFragment;,
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FastReadingGuideFragment;,
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$ReadingGuideFragment;,
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;,
        Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$TimeConverterUtil;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpActivity",
        "<",
        "Lcom/safonov/speedreading/reader/reader/view/IReaderView;",
        "Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;",
        ">;",
        "Lcom/safonov/speedreading/reader/reader/view/IReaderView;"
    }
.end annotation


# static fields
.field public static final BOOK_DESCRIPTION_ID_PARAM:Ljava/lang/String; = "book_description_id"

.field public static final BOOK_ID:Ljava/lang/String; = "book_id"

.field public static final LOAD_BOOK:Ljava/lang/String; = "load_book"

.field private static final PURCHASE_REQUEST_CODE:I = 0x3ea

.field public static final READING_MODE_DEFAULT:I = 0x0

.field public static final READING_MODE_FAST:I = 0x1

.field public static final READING_MODE_FLASH:I = 0x2

.field public static final READING_MODE_PENCIL_FRAME:I = 0x3

.field private static final SETTINGS_REQUEST_CODE:I = 0x65

.field public static final SHOULD_LOAD_BOOK:Ljava/lang/String; = "should_load_book"

.field public static final TIMER_MODE_ACCELERATOR_COURSE:I = 0x2

.field public static final TIMER_MODE_NONE:I = 0x0

.field public static final TIMER_MODE_PARAM:Ljava/lang/String; = "timer_mode"

.field public static final TIMER_MODE_PASS_COURSE:I = 0x1


# instance fields
.field blackColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field contentTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090067
    .end annotation
.end field

.field frameLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090134
    .end annotation
.end field

.field frameView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090135
    .end annotation
.end field

.field private gestureDetector:Landroid/view/GestureDetector;

.field private gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

.field greyColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060005
    .end annotation
.end field

.field protected interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

.field lineCountTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090115
    .end annotation
.end field

.field private menu:Landroid/view/Menu;

.field navigationChapterTitleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09010e
    .end annotation
.end field

.field navigationPageTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090111
    .end annotation
.end field

.field navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090117
    .end annotation
.end field

.field navigationView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090118
    .end annotation
.end field

.field pageTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090128
    .end annotation
.end field

.field pencilFrameLayout:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090112
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014e
    .end annotation
.end field

.field private progressDialog:Landroid/app/ProgressDialog;

.field private seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field sharedPreferences:Landroid/content/SharedPreferences;

.field speedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901c7
    .end annotation
.end field

.field private textSize:Ljava/lang/String;

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090207
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;-><init>()V

    .line 830
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$16;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    .line 863
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$17;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$17;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->gestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->createPresenter()Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;
    .locals 12
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 96
    invoke-static {p0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v6

    .line 99
    .local v6, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v2, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;-><init>(Landroid/content/Context;)V

    .line 100
    .local v2, "readerPreferenceUtil":Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;
    new-instance v3, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;-><init>(Landroid/content/Context;)V

    .line 102
    .local v3, "readerTimerModeSaveUtil":Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;
    new-instance v8, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-direct {v8, p0, v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 103
    .local v8, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v7, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-direct {v7, p0, v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 104
    .local v7, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v9, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-direct {v9, p0, v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 106
    .local v9, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    const-string v0, "load_book"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "book_id"

    const-wide/16 v10, -0x1

    invoke-interface {v0, v1, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 109
    .local v4, "speedReadingBookId":J
    new-instance v0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    new-instance v1, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v1, v6, v8, v7, v9}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;J)V

    return-object v0
.end method

.method public dismissProgressDialog()V
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 808
    return-void
.end method

.method public hideActionBar()V
    .locals 1

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 235
    return-void
.end method

.method public initNavigationSeekBar(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 345
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->setMax(I)V

    .line 346
    return-void
.end method

.method public initProgressViewProgress(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 325
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 326
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v3, -0x1

    .line 765
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 766
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 767
    if-ne p2, v3, :cond_0

    .line 768
    const v0, 0x7f0e0134

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->textSize:Ljava/lang/String;

    .line 770
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->textSize:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 771
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$14;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$14;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 782
    :cond_0
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_1

    .line 783
    if-ne p2, v3, :cond_1

    .line 784
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPageViewRightClick()V

    .line 787
    :cond_1
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onBackPressed()V

    .line 205
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    .line 140
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreate(Landroid/os/Bundle;)V

    .line 141
    const v3, 0x7f0b0087

    invoke-virtual {p0, v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->setContentView(I)V

    .line 142
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 144
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v3

    const v4, 0x7f0e0134

    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f0e0133

    invoke-virtual {p0, v5}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->textSize:Ljava/lang/String;

    .line 146
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v3

    iput-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->unbinder:Lbutterknife/Unbinder;

    .line 148
    new-instance v3, Landroid/view/GestureDetector;

    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->gestureListener:Landroid/view/GestureDetector$SimpleOnGestureListener;

    invoke-direct {v3, p0, v4}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->gestureDetector:Landroid/view/GestureDetector;

    .line 150
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->textSize:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 151
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    new-instance v4, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$1;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$1;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 158
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;

    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->seekBarChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/AppCompatSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 160
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 161
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    const v4, 0x7f080066

    invoke-virtual {v3, v4}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 162
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 163
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 165
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "book_description_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v3, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 166
    .local v0, "bookDescriptionId":J
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "timer_mode"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 168
    .local v2, "readerMode":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v3, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v3, v2, v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->init(IJ)V

    .line 169
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0002

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 193
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->menu:Landroid/view/Menu;

    .line 194
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1
    .param p1, "parent"    # Landroid/view/View;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 695
    invoke-super {p0, p1, p2, p3, p4}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 885
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onDestroy()V

    .line 886
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 887
    return-void
.end method

.method public onMib()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090113
        }
    .end annotation

    .prologue
    .line 266
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPencilFrameMinusClick()V

    .line 267
    return-void
.end method

.method public onNavigationChapterClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09010e
        }
    .end annotation

    .prologue
    .line 365
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onNavigationChapterViewClick()V

    .line 366
    return-void
.end method

.method public onNavigationNextPageClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090110
        }
    .end annotation

    .prologue
    .line 380
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onNavigationNextPageViewClick()V

    .line 381
    return-void
.end method

.method public onNavigationPageViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090111
        }
    .end annotation

    .prologue
    .line 355
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onNavigationPageViewClick()V

    .line 356
    return-void
.end method

.method public onNavigationPreviousPageClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090116
        }
    .end annotation

    .prologue
    .line 375
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onNavigationPreviousPageViewClick()V

    .line 376
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 173
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 184
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 175
    :sswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->finish()V

    move v0, v1

    .line 176
    goto :goto_0

    .line 178
    :sswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v2, 0x65

    invoke-virtual {p0, v0, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v1

    .line 179
    goto :goto_0

    .line 181
    :sswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onShowReadingModeDialogClick()V

    move v0, v1

    .line 182
    goto :goto_0

    .line 173
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f09009c -> :sswitch_2
        0x7f09019d -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onPause()V

    .line 210
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPause()V

    .line 211
    return-void
.end method

.method public onPlb()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090114
        }
    .end annotation

    .prologue
    .line 271
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onPencilFramePlusClick()V

    .line 272
    return-void
.end method

.method public onRequestToGetTextViewData()V
    .locals 2

    .prologue
    .line 817
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    new-instance v1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 826
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onResume()V

    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onResume()V

    .line 225
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 215
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onStop()V

    .line 216
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->onStop()V

    .line 217
    return-void
.end method

.method public seNavigationViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 340
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 341
    return-void

    .line 340
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setActionBarSelectReadingModeVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 199
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f09009c

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 200
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 230
    return-void
.end method

.method public setAd()V
    .locals 0

    .prologue
    .line 705
    return-void
.end method

.method public setBookPurchaseActivity()V
    .locals 2

    .prologue
    .line 709
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 710
    return-void
.end method

.method public setCourseActivityResult(Z)V
    .locals 1
    .param p1, "completed"    # Z

    .prologue
    .line 791
    if-eqz p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->setResult(I)V

    .line 792
    return-void

    .line 791
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setNavigationChapterView(Ljava/lang/String;)V
    .locals 1
    .param p1, "chapterTitle"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationChapterTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    return-void
.end method

.method public setNavigationPageView(II)V
    .locals 5
    .param p1, "currentPage"    # I
    .param p2, "maxPage"    # I

    .prologue
    .line 360
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationPageTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0124

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    return-void
.end method

.method public setNavigationSeekBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 350
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->navigationSeekBar:Landroid/support/v7/widget/AppCompatSeekBar;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/AppCompatSeekBar;->setProgress(I)V

    .line 351
    return-void
.end method

.method public setPageView(II)V
    .locals 5
    .param p1, "currentPage"    # I
    .param p2, "maxPage"    # I

    .prologue
    .line 291
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pageTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0125

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    return-void
.end method

.method public setPencilFrameLineCountView(I)V
    .locals 5
    .param p1, "lineCount"    # I

    .prologue
    .line 261
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->lineCountTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0126

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    return-void
.end method

.method public setPencilFrameView(II)V
    .locals 3
    .param p1, "topY"    # I
    .param p2, "bottomY"    # I

    .prologue
    .line 253
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameLayout:Landroid/view/View;

    int-to-float v1, p1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getY()F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setY(F)V

    .line 254
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    sub-int v1, p2, p1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 255
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 256
    return-void
.end method

.method public setPencilFrameViewVisibility(Z)V
    .locals 4
    .param p1, "visible"    # Z

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 247
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->frameLayout:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 248
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->pencilFrameLayout:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 249
    return-void

    :cond_0
    move v0, v2

    .line 247
    goto :goto_0

    :cond_1
    move v1, v2

    .line 248
    goto :goto_1
.end method

.method public setPrimaryTextColor()V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->blackColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 282
    return-void
.end method

.method public setProgressViewProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 330
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 331
    return-void
.end method

.method public setProgressViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 335
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressBar:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 336
    return-void

    .line 335
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setSecondaryTextColor()V
    .locals 2

    .prologue
    .line 286
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->greyColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 287
    return-void
.end method

.method public setSpeedView(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    .line 296
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->speedTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0137

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 297
    return-void
.end method

.method public setSpeedViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 301
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->speedTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    return-void

    .line 301
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 276
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    return-void
.end method

.method public setTimerView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 315
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->timeTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0143

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$TimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 316
    return-void
.end method

.method public setTimerViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 320
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->timeTextView:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 321
    return-void

    .line 320
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public showActionBar()V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 240
    return-void
.end method

.method public showErrorMessage()V
    .locals 2

    .prologue
    .line 812
    const v0, 0x7f0e0110

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 813
    return-void
.end method

.method public showExitDialog()V
    .locals 4

    .prologue
    .line 491
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 492
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 493
    const v2, 0x7f0e010e

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 494
    const v2, 0x7f0e010d

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 496
    const v2, 0x7f0e010b

    new-instance v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$9;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$9;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 503
    const v2, 0x7f0e010c

    new-instance v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$10;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$10;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 512
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 513
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 514
    return-void
.end method

.method public showGuideDialog()V
    .locals 3

    .prologue
    .line 750
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;-><init>()V

    .line 751
    .local v0, "dialogFragment":Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;
    new-instance v1, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$13;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$13;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->setDismissListener(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$DismissListener;)V

    .line 757
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 758
    return-void
.end method

.method public showProgressDialog()V
    .locals 2

    .prologue
    .line 799
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressDialog:Landroid/app/ProgressDialog;

    .line 800
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0e010f

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 801
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 802
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 803
    return-void
.end method

.method public showPurchasePremiumDialog()V
    .locals 4

    .prologue
    .line 465
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 466
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 467
    const v2, 0x7f0e012b

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 468
    const v2, 0x7f0e0129

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 470
    const v2, 0x7f0e0128

    new-instance v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$7;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$7;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 477
    const v2, 0x7f0e012a

    new-instance v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$8;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$8;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 485
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 486
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 487
    return-void
.end method

.method public showSelectChapterDialog([Ljava/lang/String;)V
    .locals 3
    .param p1, "items"    # [Ljava/lang/String;

    .prologue
    .line 387
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 388
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e012c

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 389
    new-instance v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$2;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$2;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, p1, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 395
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 396
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 397
    return-void
.end method

.method public showSelectPageDialog(II)V
    .locals 7
    .param p1, "currentPage"    # I
    .param p2, "maxPage"    # I

    .prologue
    const/4 v6, -0x2

    .line 401
    new-instance v3, Landroid/widget/NumberPicker;

    invoke-direct {v3, p0}, Landroid/widget/NumberPicker;-><init>(Landroid/content/Context;)V

    .line 402
    .local v3, "numberPicker":Landroid/widget/NumberPicker;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 403
    invoke-virtual {v3, p2}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 404
    invoke-virtual {v3, p1}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 406
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 407
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v4, 0x7f0e012f

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 408
    new-instance v4, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$3;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$3;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 420
    const v4, 0x7f0e012e

    new-instance v5, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;

    invoke-direct {v5, p0, v3}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$4;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;Landroid/widget/NumberPicker;)V

    invoke-virtual {v0, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 429
    const v4, 0x7f0e012d

    new-instance v5, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$5;

    invoke-direct {v5, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$5;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v4, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 436
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, p0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 437
    .local v2, "frameLayout":Landroid/widget/FrameLayout;
    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x11

    invoke-direct {v4, v6, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v2, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 442
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 444
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 445
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 446
    return-void
.end method

.method public showSelectReadingModeDialog(I)V
    .locals 4
    .param p1, "selectedItemIndex"    # I

    .prologue
    .line 450
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 451
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e0130

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 452
    const v2, 0x7f030017

    new-instance v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$6;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$6;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v2, p1, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 459
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 460
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 461
    return-void
.end method

.method public showTimerModeCompletedDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V
    .locals 11
    .param p1, "timerModeModels"    # [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 569
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 570
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {v0, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 571
    const v5, 0x7f0e013d

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 572
    const v5, 0x7f0e013c

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 574
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 575
    .local v3, "linearLayout":Landroid/widget/LinearLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v5, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 578
    invoke-virtual {v3, v10}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 580
    array-length v7, p1

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v4, p1, v5

    .line 581
    .local v4, "timerModeModel":Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0b0096

    .line 582
    invoke-virtual {v8, v9, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/AppCompatCheckBox;

    .line 584
    .local v1, "checkBox":Landroid/support/v7/widget/AppCompatCheckBox;
    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getReadingMode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 598
    :goto_1
    invoke-virtual {v1, v10}, Landroid/support/v7/widget/AppCompatCheckBox;->setChecked(Z)V

    .line 599
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 580
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 586
    :pswitch_0
    const v8, 0x7f0e0115

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 589
    :pswitch_1
    const v8, 0x7f0e0118

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 592
    :pswitch_2
    const v8, 0x7f0e011a

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 595
    :pswitch_3
    const v8, 0x7f0e011e

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 602
    .end local v1    # "checkBox":Landroid/support/v7/widget/AppCompatCheckBox;
    .end local v4    # "timerModeModel":Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    :cond_0
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 604
    const v5, 0x7f0e013f

    new-instance v6, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$12;

    invoke-direct {v6, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$12;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 613
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v2

    .line 614
    .local v2, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 615
    return-void

    .line 584
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public showTimerModeDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V
    .locals 10
    .param p1, "timerModeModels"    # [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x0

    .line 518
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 519
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {v0, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 520
    const v5, 0x7f0e0142

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 521
    const v5, 0x7f0e0140

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 523
    new-instance v3, Landroid/widget/LinearLayout;

    invoke-direct {v3, p0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 524
    .local v3, "linearLayout":Landroid/widget/LinearLayout;
    new-instance v5, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v7, -0x1

    const/4 v8, -0x2

    invoke-direct {v5, v7, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 527
    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 529
    array-length v7, p1

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v4, p1, v5

    .line 530
    .local v4, "timerModeModel":Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0b0096

    .line 531
    invoke-virtual {v8, v9, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/AppCompatCheckBox;

    .line 533
    .local v1, "checkBox":Landroid/support/v7/widget/AppCompatCheckBox;
    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getReadingMode()I

    move-result v8

    packed-switch v8, :pswitch_data_0

    .line 547
    :goto_1
    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->isCompleted()Z

    move-result v8

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setChecked(Z)V

    .line 549
    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 529
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 535
    :pswitch_0
    const v8, 0x7f0e0115

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 538
    :pswitch_1
    const v8, 0x7f0e0118

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 541
    :pswitch_2
    const v8, 0x7f0e011a

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 544
    :pswitch_3
    const v8, 0x7f0e011e

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/AppCompatCheckBox;->setText(I)V

    goto :goto_1

    .line 552
    .end local v1    # "checkBox":Landroid/support/v7/widget/AppCompatCheckBox;
    .end local v4    # "timerModeModel":Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    :cond_0
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 554
    const v5, 0x7f0e0141

    new-instance v6, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$11;

    invoke-direct {v6, p0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$11;-><init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V

    invoke-virtual {v0, v5, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 563
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v2

    .line 564
    .local v2, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v2}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 565
    return-void

    .line 533
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
