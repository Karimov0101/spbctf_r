.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 703
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 706
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 743
    :cond_0
    :goto_0
    return-void

    .line 710
    :cond_1
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    move-result-object v1

    if-nez v1, :cond_2

    .line 711
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    new-instance v2, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v3

    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;-><init>(Ljava/lang/CharSequence;)V

    invoke-static {v1, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/WordSelector;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 714
    :cond_2
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->getPageWithNextSelectedWord()Landroid/text/Spanned;

    move-result-object v0

    .line 715
    .local v0, "worldSelectorPage":Landroid/text/Spanned;
    if-eqz v0, :cond_4

    .line 716
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 717
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 719
    :cond_3
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v1

    const v2, 0xea60

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1600(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    div-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 722
    :cond_4
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 723
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 724
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    .line 725
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$408(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    .line 726
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPageView(II)V

    .line 727
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationSeekBar(I)V

    .line 728
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    .line 729
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationPageView(II)V

    .line 733
    :cond_5
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/WordSelector;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 734
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0xc8

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 736
    :cond_6
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 737
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1, v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/WordSelector;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 738
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 739
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
