.class public Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
.super Ljava/lang/Object;
.source "TimerModeModel.java"


# instance fields
.field private final readingMode:I

.field private remainingTime:J


# direct methods
.method public constructor <init>(IJ)V
    .locals 0
    .param p1, "readingMode"    # I
    .param p2, "remainingTime"    # J

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->readingMode:I

    .line 16
    iput-wide p2, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->remainingTime:J

    .line 17
    return-void
.end method


# virtual methods
.method public getReadingMode()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->readingMode:I

    return v0
.end method

.method public getRemainingTime()J
    .locals 2

    .prologue
    .line 24
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->remainingTime:J

    return-wide v0
.end method

.method public isCompleted()Z
    .locals 4

    .prologue
    .line 32
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->remainingTime:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setRemainingTime(J)V
    .locals 1
    .param p1, "remainingTime"    # J

    .prologue
    .line 20
    iput-wide p1, p0, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->remainingTime:J

    .line 21
    return-void
.end method
