.class public Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;
.super Ljava/lang/Object;
.source "PageSplitter.java"


# instance fields
.field private final lineSpacingExtra:F

.field private final lineSpacingMultiplier:F

.field private final pageHeight:I

.field private final pageWidth:I

.field private final textPaint:Landroid/text/TextPaint;


# direct methods
.method public constructor <init>(Landroid/text/TextPaint;II)V
    .locals 1
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "pageWidth"    # I
    .param p3, "pageHeight"    # I

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->textPaint:Landroid/text/TextPaint;

    .line 27
    iput p2, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageWidth:I

    .line 28
    iput p3, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageHeight:I

    .line 30
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingMultiplier:F

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingExtra:F

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/text/TextPaint;IIFI)V
    .locals 1
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "pageWidth"    # I
    .param p3, "pageHeight"    # I
    .param p4, "lineSpacingMultiplier"    # F
    .param p5, "lineSpacingExtra"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->textPaint:Landroid/text/TextPaint;

    .line 36
    iput p2, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageWidth:I

    .line 37
    iput p3, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageHeight:I

    .line 38
    iput p4, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingMultiplier:F

    .line 39
    int-to-float v0, p5

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingExtra:F

    .line 40
    return-void
.end method


# virtual methods
.method public getSeparatedBook(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
    .locals 20
    .param p1, "bookContent"    # Lcom/safonov/speedreading/reader/repository/entity/BookContent;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 43
    new-instance v16, Ljava/util/ArrayList;

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 44
    .local v16, "titles":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->getTitles()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/CharSequence;

    .line 45
    .local v15, "title":Ljava/lang/CharSequence;
    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 48
    .end local v15    # "title":Ljava/lang/CharSequence;
    :cond_0
    new-instance v12, Ljava/util/LinkedList;

    invoke-direct {v12}, Ljava/util/LinkedList;-><init>()V

    .line 50
    .local v12, "pages":Ljava/util/List;, "Ljava/util/List<Ljava/util/List<Ljava/lang/CharSequence;>;>;"
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->getChaptersText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :goto_1
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    .line 51
    .local v2, "chapter":Ljava/lang/CharSequence;
    new-instance v9, Ljava/util/LinkedList;

    invoke-direct {v9}, Ljava/util/LinkedList;-><init>()V

    .line 53
    .local v9, "chapterPages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    const/16 v17, 0x0

    .line 55
    .local v17, "usedHeight":I
    const/4 v14, 0x0

    .line 56
    .local v14, "startIndex":I
    const/4 v10, 0x0

    .line 58
    .local v10, "endIndex":I
    new-instance v1, Landroid/text/StaticLayout;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->textPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageWidth:I

    sget-object v5, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingMultiplier:F

    move-object/from16 v0, p0

    iget v7, v0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->lineSpacingExtra:F

    const/4 v8, 0x1

    invoke-direct/range {v1 .. v8}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 59
    .local v1, "tempLayout":Landroid/text/StaticLayout;
    invoke-virtual {v1}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v3

    add-int/lit8 v11, v3, -0x1

    .line 61
    .local v11, "lastLineIndex":I
    if-nez v11, :cond_1

    .line 62
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_1
    const/4 v13, 0x0

    .line 66
    .local v13, "selectedLine":I
    :goto_2
    if-eq v13, v11, :cond_3

    .line 67
    move-object/from16 v0, p0

    iget v3, v0, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->pageHeight:I

    add-int v18, v17, v3

    .line 69
    .local v18, "verticalOffset":I
    move/from16 v0, v18

    invoke-virtual {v1, v0}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v13

    .line 70
    invoke-virtual {v1, v13}, Landroid/text/StaticLayout;->getLineBottom(I)I

    move-result v3

    move/from16 v0, v18

    if-le v3, v0, :cond_2

    .line 71
    add-int/lit8 v13, v13, -0x1

    .line 74
    :cond_2
    invoke-virtual {v1, v13}, Landroid/text/StaticLayout;->getLineBottom(I)I

    move-result v17

    .line 76
    invoke-virtual {v1, v13}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v10

    .line 77
    invoke-interface {v2, v14, v10}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    move v14, v10

    .line 79
    goto :goto_2

    .line 81
    .end local v18    # "verticalOffset":I
    :cond_3
    invoke-interface {v12, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 84
    .end local v1    # "tempLayout":Landroid/text/StaticLayout;
    .end local v2    # "chapter":Ljava/lang/CharSequence;
    .end local v9    # "chapterPages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    .end local v10    # "endIndex":I
    .end local v11    # "lastLineIndex":I
    .end local v13    # "selectedLine":I
    .end local v14    # "startIndex":I
    .end local v17    # "usedHeight":I
    :cond_4
    new-instance v3, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v12}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;-><init>(Ljava/util/List;Ljava/util/List;)V

    return-object v3
.end method
