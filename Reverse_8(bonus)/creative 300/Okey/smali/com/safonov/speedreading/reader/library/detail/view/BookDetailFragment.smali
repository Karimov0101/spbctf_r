.class public Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "BookDetailFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;",
        "Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;"
    }
.end annotation


# static fields
.field private static final BOOK_DESCRIPTION_PARAM:Ljava/lang/String; = "book_description"


# instance fields
.field authorTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09002d
    .end annotation
.end field

.field authorView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09002e
    .end annotation
.end field

.field private bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

.field coverImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09003c
    .end annotation
.end field

.field filePathTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090090
    .end annotation
.end field

.field languageTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d3
    .end annotation
.end field

.field languageView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d4
    .end annotation
.end field

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020c
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;
    .locals 3
    .param p0, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 46
    new-instance v1, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;-><init>()V

    .line 47
    .local v1, "fragment":Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "book_description"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 49
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->setArguments(Landroid/os/Bundle;)V

    .line 50
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->createPresenter()Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/library/detail/presenter/BookDetailPresenter;-><init>()V

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v1, -0x1

    .line 145
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 146
    packed-switch p1, :pswitch_data_0

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 148
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(I)V

    .line 150
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    .line 146
    nop

    :pswitch_data_0
    .packed-switch 0x7ce
        :pswitch_0
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 56
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "book_description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 59
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 78
    const v1, 0x7f0b0088

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 80
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->unbinder:Lbutterknife/Unbinder;

    .line 82
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 160
    return-void
.end method

.method public onStartReadingClick()V
    .locals 6
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901d3
        }
    .end annotation

    .prologue
    .line 128
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "timer_mode"

    const/4 v4, 0x0

    .line 129
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 132
    .local v1, "timerModeParam":I
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-class v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 133
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "book_description_id"

    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 134
    const-string v2, "timer_mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 136
    if-nez v1, :cond_0

    .line 137
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->startActivity(Landroid/content/Intent;)V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    const/16 v2, 0x7ce

    invoke-virtual {p0, v0, v2}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 87
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/detail/presenter/IBookDetailPresenter;->init(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 89
    return-void
.end method

.method public setAuthorView(Ljava/lang/String;)V
    .locals 1
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method

.method public setAuthorViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 108
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->authorView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 109
    return-void

    .line 108
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setCoverView(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->coverImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 94
    return-void
.end method

.method public setFilePathView(Ljava/lang/String;)V
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->filePathTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    return-void
.end method

.method public setLanguageView(Ljava/lang/String;)V
    .locals 1
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method public setLanguageViewVisibility(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 118
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->languageView:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 119
    return-void

    .line 118
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setTitleView(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method
