.class public Lcom/safonov/speedreading/reader/reader/util/TimerModeUtil;
.super Ljava/lang/Object;
.source "TimerModeUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getTimerModeModels(I)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    .locals 10
    .param p0, "timerMode"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const-wide/32 v8, 0x493e0

    const-wide/32 v6, 0xea60

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 16
    packed-switch p0, :pswitch_data_0

    .line 28
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported timer mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 18
    :pswitch_0
    new-array v0, v3, [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    new-instance v1, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    invoke-direct {v1, v2, v8, v9}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;-><init>(IJ)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    invoke-direct {v1, v3, v8, v9}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;-><init>(IJ)V

    aput-object v1, v0, v2

    .line 23
    :goto_0
    return-object v0

    :pswitch_1
    new-array v0, v3, [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    new-instance v1, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    invoke-direct {v1, v2, v6, v7}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;-><init>(IJ)V

    aput-object v1, v0, v4

    new-instance v1, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    invoke-direct {v1, v3, v6, v7}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;-><init>(IJ)V

    aput-object v1, v0, v2

    goto :goto_0

    .line 16
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
