.class public interface abstract Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;
.super Ljava/lang/Object;
.source "IFileExplorerView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;
.implements Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;


# virtual methods
.method public abstract closeFileExplorer()V
.end method

.method public abstract setActionBarTitle(Ljava/lang/String;)V
.end method

.method public abstract setData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;)V"
        }
    .end annotation
.end method
