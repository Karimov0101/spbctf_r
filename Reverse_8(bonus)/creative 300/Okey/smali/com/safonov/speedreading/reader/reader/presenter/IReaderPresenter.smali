.class public interface abstract Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;
.super Ljava/lang/Object;
.source "IReaderPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/reader/reader/view/IReaderView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract init(IJ)V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onGuideDialogClosed(Z)V
.end method

.method public abstract onNavigationChapterSelected(I)V
.end method

.method public abstract onNavigationChapterViewClick()V
.end method

.method public abstract onNavigationNextPageViewClick()V
.end method

.method public abstract onNavigationPageSelected(I)V
.end method

.method public abstract onNavigationPageViewClick()V
.end method

.method public abstract onNavigationPreviousPageViewClick()V
.end method

.method public abstract onNavigationSeekBarPageChanged(I)V
.end method

.method public abstract onNavigationSeekBarPageSelected()V
.end method

.method public abstract onPageViewCenterClick(Z)V
.end method

.method public abstract onPageViewLeftClick()V
.end method

.method public abstract onPageViewRightClick()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPencilFrameMinusClick()V
.end method

.method public abstract onPencilFramePlusClick()V
.end method

.method public abstract onReadingModeSelected(I)V
.end method

.method public abstract onResume()V
.end method

.method public abstract onShowReadingModeDialogClick()V
.end method

.method public abstract onStop()V
.end method

.method public abstract onTimerModeCompletedDialogClosed()V
.end method

.method public abstract onTimerModeDialogClosed()V
.end method

.method public abstract requestToSplitBook(Landroid/text/TextPaint;II)V
    .param p1    # Landroid/text/TextPaint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
