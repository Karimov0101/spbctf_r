.class public Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "ReaderSettingsActivity.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field private changedTextSize:Ljava/lang/String;

.field private sharedPreferences:Landroid/content/SharedPreferences;

.field private textSize:Ljava/lang/String;

.field textSizeDefaultValue:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0133
    .end annotation
.end field

.field textSizeKey:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0134
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public finish()V
    .locals 3

    .prologue
    .line 71
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSize:Ljava/lang/String;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->changedTextSize:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 72
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->setResult(I)V

    .line 80
    :goto_0
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    .line 81
    return-void

    .line 74
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 75
    .local v0, "data":Landroid/content/Intent;
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->changedTextSize:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 77
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 39
    const v0, 0x7f0b0095

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->setContentView(I)V

    .line 41
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->unbinder:Lbutterknife/Unbinder;

    .line 43
    invoke-static {p0}, Landroid/support/v7/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeKey:Ljava/lang/String;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeDefaultValue:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSize:Ljava/lang/String;

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->changedTextSize:Ljava/lang/String;

    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 48
    invoke-static {}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;->newInstance()Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e017a

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 54
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->unbinder:Lbutterknife/Unbinder;

    .line 107
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 60
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 65
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 62
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->finish()V

    .line 63
    const/4 v0, 0x1

    goto :goto_0

    .line 60
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onPause()V

    .line 86
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 87
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onResume()V

    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 93
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeKey:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->textSizeDefaultValue:Ljava/lang/String;

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/settings/ReaderSettingsActivity;->changedTextSize:Ljava/lang/String;

    .line 100
    :cond_0
    return-void
.end method
