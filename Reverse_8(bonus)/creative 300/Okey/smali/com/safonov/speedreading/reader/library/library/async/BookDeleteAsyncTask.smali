.class public Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;
.super Landroid/os/AsyncTask;
.source "BookDeleteAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;)V
    .locals 0
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 24
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    .line 25
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    check-cast p1, [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 37
    array-length v2, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v0, p1, v1

    .line 38
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v3, v0}, Lcom/safonov/speedreading/reader/repository/IBookController;->removeBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 37
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 40
    .end local v0    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    :cond_0
    const/4 v1, 0x0

    return-object v1
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "v"    # Ljava/lang/Void;

    .prologue
    .line 45
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;->oBookDeletePostExecute()V

    .line 49
    :cond_0
    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 29
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 30
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;->onBookDeletePreExecute()V

    .line 33
    :cond_0
    return-void
.end method
