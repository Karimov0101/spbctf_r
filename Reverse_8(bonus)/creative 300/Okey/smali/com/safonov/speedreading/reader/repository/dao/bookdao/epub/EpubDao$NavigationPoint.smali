.class Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;
.super Ljava/lang/Object;
.source "EpubDao.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NavigationPoint"
.end annotation


# instance fields
.field private bookChapterId:Ljava/lang/String;

.field private bookChapterPath:Ljava/lang/String;

.field private bookChapterTitle:Ljava/lang/String;

.field final synthetic this$0:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "bookChapterTitle"    # Ljava/lang/String;
    .param p3, "bookChapterPath"    # Ljava/lang/String;

    .prologue
    .line 398
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->this$0:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 399
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterTitle:Ljava/lang/String;

    .line 400
    iput-object p3, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterPath:Ljava/lang/String;

    .line 401
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p2, "bookChapterTitle"    # Ljava/lang/String;
    .param p3, "bookChapterPath"    # Ljava/lang/String;
    .param p4, "bookChapterId"    # Ljava/lang/String;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->this$0:Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 404
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterTitle:Ljava/lang/String;

    .line 405
    iput-object p3, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterPath:Ljava/lang/String;

    .line 406
    iput-object p4, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterId:Ljava/lang/String;

    .line 407
    return-void
.end method


# virtual methods
.method public getBookChapterId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterId:Ljava/lang/String;

    return-object v0
.end method

.method public getBookChapterPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterPath:Ljava/lang/String;

    return-object v0
.end method

.method public getBookChapterTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao$NavigationPoint;->bookChapterTitle:Ljava/lang/String;

    return-object v0
.end method
