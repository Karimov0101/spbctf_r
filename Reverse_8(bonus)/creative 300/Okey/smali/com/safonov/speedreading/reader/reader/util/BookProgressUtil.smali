.class public Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;
.super Ljava/lang/Object;
.source "BookProgressUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getBookOffset(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;I)J
    .locals 6
    .param p0, "splittedBook"    # Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "pageIndex"    # I

    .prologue
    .line 44
    if-nez p1, :cond_1

    const-wide/16 v0, 0x0

    .line 57
    :cond_0
    :goto_0
    return-wide v0

    .line 46
    :cond_1
    const-wide/16 v0, 0x0

    .line 47
    .local v0, "bookOffset":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, p1, :cond_2

    .line 48
    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->length()I

    move-result v3

    int-to-long v4, v3

    add-long/2addr v0, v4

    .line 47
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 53
    :cond_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPageCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge p1, v3, :cond_0

    .line 54
    const-wide/16 v4, 0x1

    add-long/2addr v0, v4

    goto :goto_0
.end method

.method public static getPageFromProgress(II)I
    .locals 2
    .param p0, "progress"    # I
    .param p1, "pageCount"    # I

    .prologue
    .line 79
    if-nez p0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 82
    :goto_0
    return v0

    :cond_0
    int-to-float v0, p1

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    int-to-float v1, p0

    mul-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public static getPageIndex(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;J)I
    .locals 7
    .param p0, "splittedBook"    # Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "bookOffset"    # J

    .prologue
    .line 19
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-nez v4, :cond_1

    const/4 v3, 0x0

    .line 34
    :cond_0
    return v3

    .line 21
    :cond_1
    const/4 v3, 0x0

    .line 22
    .local v3, "pageIndex":I
    const-wide/16 v0, 0x0

    .line 24
    .local v0, "bookLength":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPageCount()I

    move-result v4

    if-ge v2, v4, :cond_0

    .line 25
    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    .line 27
    cmp-long v4, v0, p1

    if-gtz v4, :cond_0

    .line 28
    add-int/lit8 v3, v3, 0x1

    .line 24
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static getProgress(II)I
    .locals 2
    .param p0, "pageIndex"    # I
    .param p1, "pageCount"    # I

    .prologue
    .line 67
    if-nez p0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p0, 0x1

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v0, v1

    int-to-float v1, p1

    div-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_0
.end method
