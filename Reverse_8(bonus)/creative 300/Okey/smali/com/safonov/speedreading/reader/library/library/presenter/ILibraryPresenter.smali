.class public interface abstract Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;
.super Ljava/lang/Object;
.source "ILibraryPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract init()V
.end method

.method public abstract onFormatDialogDismiss(Z)V
.end method

.method public abstract onItemClick(J)V
.end method

.method public abstract onItemLongClick(J)V
.end method

.method public abstract requestToAddBook()V
.end method

.method public abstract requestToRemoveBook()V
.end method

.method public abstract setSpeedReadingBookInfo(J)V
.end method
