.class public Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
.super Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;
.source "Fb2Dao.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;
    }
.end annotation


# static fields
.field private static final CITE_TAG:Ljava/lang/String; = "cite"

.field private static final EMPHASIS_TAG:Ljava/lang/String; = "emphasis"

.field private static final EMPTY_LINE_TAG:Ljava/lang/String; = "empty-line"

.field private static final EPIGRAPH_TAG:Ljava/lang/String; = "epigraph"

.field private static final PARAGRAPH_TAG:Ljava/lang/String; = "p"

.field private static final POEM_TAG:Ljava/lang/String; = "poem"

.field private static final SECTION_TAG:Ljava/lang/String; = "section"

.field private static final STANZA_TAG:Ljava/lang/String; = "stanza"

.field private static final STRIKE_THROUGH_TAG:Ljava/lang/String; = "strikethrough"

.field private static final STRONG_TAG:Ljava/lang/String; = "strong"

.field private static final SUBTITLE_FONT_HEIGHT:F = 1.2f

.field private static final SUBTITLE_TAG:Ljava/lang/String; = "subtitle"

.field private static final SUB_FONT_HEIGHT:F = 0.5f

.field private static final SUB_TAG:Ljava/lang/String; = "sub"

.field private static final SUP_TAG:Ljava/lang/String; = "sup"

.field private static final TEXT_AUTHOR_TAG:Ljava/lang/String; = "text-author"

.field private static final TITLE_FONT_HEIGHT:F = 1.5f

.field private static final TITLE_TAG:Ljava/lang/String; = "title"

.field private static final V_TAG:Ljava/lang/String; = "v"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "bookDescriptionDao"    # Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/BookDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 82
    return-void
.end method

.method private getBookAuthors(Lorg/w3c/dom/Element;)Ljava/util/List;
    .locals 16
    .param p1, "titleInfo"    # Lorg/w3c/dom/Element;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Element;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 270
    const-string v14, "author"

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 271
    .local v3, "authorTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 273
    .local v4, "authorTagsCount":I
    new-instance v13, Ljava/util/ArrayList;

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v14

    invoke-direct {v13, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 275
    .local v13, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v4, :cond_4

    .line 276
    invoke-interface {v3, v7}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    invoke-interface {v14}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v1

    .line 277
    .local v1, "authorNestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v1}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v2

    .line 279
    .local v2, "authorNestedTagsCount":I
    const/4 v6, 0x0

    .line 280
    .local v6, "firstName":Ljava/lang/String;
    const/4 v10, 0x0

    .line 281
    .local v10, "middleName":Ljava/lang/String;
    const/4 v9, 0x0

    .line 283
    .local v9, "lastName":Ljava/lang/String;
    const/4 v11, 0x0

    .line 285
    .local v11, "nickName":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "j":I
    :goto_1
    if-ge v8, v2, :cond_1

    .line 286
    invoke-interface {v1, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    .line 287
    .local v5, "currentAuthorNestedTag":Lorg/w3c/dom/Node;
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v12

    .line 289
    .local v12, "nodeName":Ljava/lang/String;
    const/4 v14, -0x1

    invoke-virtual {v12}, Ljava/lang/String;->hashCode()I

    move-result v15

    sparse-switch v15, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v14, :pswitch_data_0

    .line 285
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 289
    :sswitch_0
    const-string v15, "first-name"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v14, 0x0

    goto :goto_2

    :sswitch_1
    const-string v15, "middle-name"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v14, 0x1

    goto :goto_2

    :sswitch_2
    const-string v15, "last-name"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v14, 0x2

    goto :goto_2

    :sswitch_3
    const-string v15, "nickname"

    invoke-virtual {v12, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    const/4 v14, 0x3

    goto :goto_2

    .line 291
    :pswitch_0
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v6

    .line 292
    goto :goto_3

    .line 294
    :pswitch_1
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v10

    .line 295
    goto :goto_3

    .line 297
    :pswitch_2
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v9

    .line 298
    goto :goto_3

    .line 300
    :pswitch_3
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v11

    goto :goto_3

    .line 305
    .end local v5    # "currentAuthorNestedTag":Lorg/w3c/dom/Node;
    .end local v12    # "nodeName":Ljava/lang/String;
    :cond_1
    if-eqz v6, :cond_3

    .line 306
    if-eqz v10, :cond_2

    .line 307
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v15, 0x20

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v15, 0x20

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 309
    :cond_2
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v14, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const/16 v15, 0x20

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-interface {v13, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 312
    :cond_3
    invoke-interface {v13, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 315
    .end local v1    # "authorNestedTags":Lorg/w3c/dom/NodeList;
    .end local v2    # "authorNestedTagsCount":I
    .end local v6    # "firstName":Ljava/lang/String;
    .end local v8    # "j":I
    .end local v9    # "lastName":Ljava/lang/String;
    .end local v10    # "middleName":Ljava/lang/String;
    .end local v11    # "nickName":Ljava/lang/String;
    :cond_4
    return-object v13

    .line 289
    nop

    :sswitch_data_0
    .sparse-switch
        -0xc590878 -> :sswitch_0
        0x436a86e -> :sswitch_3
        0x165877c3 -> :sswitch_1
        0x753d3762 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private getBookLanguage(Lorg/w3c/dom/Element;)Ljava/lang/String;
    .locals 2
    .param p1, "titleInfo"    # Lorg/w3c/dom/Element;

    .prologue
    .line 266
    const-string v0, "lang"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getBookTitle(Lorg/w3c/dom/Element;)Ljava/lang/String;
    .locals 2
    .param p1, "titleInfo"    # Lorg/w3c/dom/Element;

    .prologue
    .line 262
    const-string v0, "book-title"

    invoke-interface {p1, v0}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getTextContent()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private parseBody(Lorg/w3c/dom/Node;)Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    .locals 10
    .param p1, "body"    # Lorg/w3c/dom/Node;

    .prologue
    .line 345
    new-instance v2, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;

    invoke-direct {v2}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;-><init>()V

    .line 347
    .local v2, "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 348
    .local v0, "bodyNestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 350
    .local v1, "bodyNestedTagsCount":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v1, :cond_1

    .line 351
    invoke-interface {v0, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v3

    .line 352
    .local v3, "currentTag":Lorg/w3c/dom/Node;
    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v4

    .line 354
    .local v4, "currentTagName":Ljava/lang/String;
    const/4 v8, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v8, :pswitch_data_0

    .line 350
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 354
    :sswitch_0
    const-string v9, "title"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x0

    goto :goto_1

    :sswitch_1
    const-string v9, "epigraph"

    invoke-virtual {v4, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    .line 356
    :pswitch_0
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 357
    .local v7, "title":Ljava/lang/CharSequence;
    invoke-virtual {v2, v7}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 360
    .end local v7    # "title":Ljava/lang/CharSequence;
    :pswitch_1
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 361
    .local v5, "epigraph":Ljava/lang/CharSequence;
    invoke-virtual {v2, v5}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->setContent(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 366
    .end local v3    # "currentTag":Lorg/w3c/dom/Node;
    .end local v4    # "currentTagName":Ljava/lang/String;
    .end local v5    # "epigraph":Ljava/lang/CharSequence;
    :cond_1
    return-object v2

    .line 354
    nop

    :sswitch_data_0
    .sparse-switch
        -0x2661c510 -> :sswitch_1
        0x6942258 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private parseBook(Lorg/w3c/dom/Document;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 11
    .param p1, "bookDocument"    # Lorg/w3c/dom/Document;

    .prologue
    .line 319
    new-instance v4, Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-direct {v4}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;-><init>()V

    .line 321
    .local v4, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    const-string v9, "body"

    invoke-interface {p1, v9}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v0

    .line 322
    .local v0, "bodyTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v0}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v1

    .line 324
    .local v1, "bodyTagsCount":I
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    .line 326
    .local v3, "bookChapterList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v1, :cond_2

    .line 327
    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    check-cast v9, Lorg/w3c/dom/Element;

    const-string v10, "section"

    invoke-interface {v9, v10}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 328
    .local v7, "sectionTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v8

    .line 330
    .local v8, "sectionTagsCount":I
    invoke-interface {v0, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseBody(Lorg/w3c/dom/Node;)Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    const/4 v6, 0x0

    .local v6, "j":I
    :goto_1
    if-ge v6, v8, :cond_1

    .line 333
    invoke-interface {v7, v6}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseSection(Lorg/w3c/dom/Node;)Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;

    move-result-object v2

    .line 334
    .local v2, "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->getBookChapter()Ljava/lang/CharSequence;

    move-result-object v9

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 335
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 326
    .end local v2    # "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 339
    .end local v6    # "j":I
    .end local v7    # "sectionTags":Lorg/w3c/dom/NodeList;
    .end local v8    # "sectionTagsCount":I
    :cond_2
    invoke-virtual {v4, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->setBookChapterList(Ljava/util/List;)V

    .line 341
    return-object v4
.end method

.method private parseCover(Lorg/w3c/dom/Document;)Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;
    .locals 14
    .param p1, "bookDocument"    # Lorg/w3c/dom/Document;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 184
    const-string v12, "coverpage"

    invoke-interface {p1, v12}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    .line 185
    .local v7, "coverPageTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v12

    if-nez v12, :cond_0

    .line 186
    const/4 v12, 0x0

    .line 237
    :goto_0
    return-object v12

    .line 188
    :cond_0
    const/4 v12, 0x0

    invoke-interface {v7, v12}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 189
    .local v5, "coverPageNestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 191
    .local v6, "coverPageNestedTagsCount":I
    const/4 v11, 0x0

    .line 193
    .local v11, "imageTag":Lorg/w3c/dom/Element;
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v6, :cond_1

    .line 194
    invoke-interface {v5, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v12

    const/4 v13, 0x1

    if-ne v12, v13, :cond_2

    .line 195
    invoke-interface {v5, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v11

    .end local v11    # "imageTag":Lorg/w3c/dom/Element;
    check-cast v11, Lorg/w3c/dom/Element;

    .line 200
    .restart local v11    # "imageTag":Lorg/w3c/dom/Element;
    :cond_1
    if-nez v11, :cond_3

    .line 201
    const/4 v12, 0x0

    goto :goto_0

    .line 193
    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 204
    :cond_3
    invoke-interface {v11}, Lorg/w3c/dom/Element;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v9

    .line 205
    .local v9, "imageAttributes":Lorg/w3c/dom/NamedNodeMap;
    invoke-interface {v9}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v10

    .line 207
    .local v10, "imageAttributesCount":I
    const/4 v4, 0x0

    .line 209
    .local v4, "coverId":Ljava/lang/String;
    const/4 v8, 0x0

    :goto_2
    if-ge v8, v10, :cond_4

    .line 210
    invoke-interface {v9, v8}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v12

    const-string v13, "href"

    invoke-virtual {v12, v13}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v12

    if-lez v12, :cond_5

    .line 211
    invoke-interface {v9, v8}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    invoke-interface {v12}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v4

    .line 216
    :cond_4
    if-nez v4, :cond_6

    .line 217
    const/4 v12, 0x0

    goto :goto_0

    .line 209
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 219
    :cond_6
    const/4 v12, 0x0

    invoke-virtual {v4, v12}, Ljava/lang/String;->charAt(I)C

    move-result v12

    const/16 v13, 0x23

    if-ne v12, v13, :cond_7

    .line 220
    const/4 v12, 0x1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v13

    invoke-virtual {v4, v12, v13}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 224
    :cond_7
    const-string v12, "binary"

    invoke-interface {p1, v12}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v2

    .line 225
    .local v2, "binaryTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v3

    .line 227
    .local v3, "binaryTagsCount":I
    const/4 v8, 0x0

    :goto_3
    if-ge v8, v3, :cond_9

    .line 228
    invoke-interface {v2, v8}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Element;

    .line 230
    .local v1, "binaryTag":Lorg/w3c/dom/Element;
    const-string v12, "id"

    invoke-interface {v1, v12}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    .local v0, "attribute":Ljava/lang/String;
    if-eqz v0, :cond_8

    .line 232
    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    .line 233
    new-instance v12, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;

    invoke-interface {v1}, Lorg/w3c/dom/Element;->getTextContent()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, p0, v13, v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 227
    :cond_8
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 237
    .end local v0    # "attribute":Ljava/lang/String;
    .end local v1    # "binaryTag":Lorg/w3c/dom/Element;
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_0
.end method

.method private parseSection(Lorg/w3c/dom/Node;)Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    .locals 10
    .param p1, "section"    # Lorg/w3c/dom/Node;

    .prologue
    .line 370
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v5

    .line 371
    .local v5, "sectionNestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v5}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v6

    .line 373
    .local v6, "sectionNestedTagsCount":I
    new-instance v0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;-><init>()V

    .line 374
    .local v0, "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 376
    .local v1, "builder":Landroid/text/SpannableStringBuilder;
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v6, :cond_1

    .line 377
    invoke-interface {v5, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    .line 378
    .local v2, "currentTag":Lorg/w3c/dom/Node;
    invoke-interface {v2}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v3

    .line 380
    .local v3, "currentTagName":Ljava/lang/String;
    const/4 v8, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_1
    packed-switch v8, :pswitch_data_0

    .line 392
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 376
    :goto_2
    :pswitch_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 380
    :sswitch_0
    const-string v9, "title"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x0

    goto :goto_1

    :sswitch_1
    const-string v9, "epigraph"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x1

    goto :goto_1

    :sswitch_2
    const-string v9, "section"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v8, 0x2

    goto :goto_1

    .line 382
    :pswitch_1
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v7

    .line 383
    .local v7, "title":Ljava/lang/CharSequence;
    invoke-virtual {v0, v7}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 386
    .end local v7    # "title":Ljava/lang/CharSequence;
    :pswitch_2
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_2

    .line 397
    .end local v2    # "currentTag":Lorg/w3c/dom/Node;
    .end local v3    # "currentTagName":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 398
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->setContent(Ljava/lang/CharSequence;)V

    .line 402
    :cond_2
    return-object v0

    .line 380
    :sswitch_data_0
    .sparse-switch
        -0x2661c510 -> :sswitch_1
        0x6942258 -> :sswitch_0
        0x756f7ee5 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method private parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;
    .locals 13
    .param p1, "tag"    # Lorg/w3c/dom/Node;

    .prologue
    const/4 v8, 0x2

    const/16 v9, 0xa

    const/16 v12, 0x21

    const/4 v6, 0x0

    const/4 v7, 0x1

    .line 406
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v5

    const/4 v10, 0x3

    if-ne v5, v10, :cond_2

    .line 409
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 410
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    .line 603
    :cond_0
    :goto_0
    return-object v0

    .line 412
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 416
    :cond_2
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 418
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v3

    .line 419
    .local v3, "nestedTags":Lorg/w3c/dom/NodeList;
    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    .line 421
    .local v4, "nestedTagsCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 422
    invoke-interface {v3, v2}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseTag(Lorg/w3c/dom/Node;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 421
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 425
    :cond_3
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v10

    const/4 v5, -0x1

    invoke-virtual {v10}, Ljava/lang/String;->hashCode()I

    move-result v11

    sparse-switch v11, :sswitch_data_0

    :cond_4
    :goto_2
    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 427
    :pswitch_0
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 428
    .local v1, "builderLength":I
    if-lt v1, v7, :cond_0

    .line 432
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const/high16 v8, 0x3fc00000    # 1.5f

    invoke-direct {v5, v8}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 433
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 432
    invoke-virtual {v0, v5, v6, v8, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 434
    new-instance v5, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v5, v8}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 435
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 434
    invoke-virtual {v0, v5, v6, v8, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 436
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 437
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 436
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 440
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 441
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    .line 425
    .end local v1    # "builderLength":I
    :sswitch_0
    const-string v11, "title"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v5, v6

    goto :goto_2

    :sswitch_1
    const-string v11, "subtitle"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v5, v7

    goto :goto_2

    :sswitch_2
    const-string v11, "p"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v5, v8

    goto :goto_2

    :sswitch_3
    const-string v11, "epigraph"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v5, 0x3

    goto :goto_2

    :sswitch_4
    const-string v11, "poem"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v5, 0x4

    goto :goto_2

    :sswitch_5
    const-string v11, "stanza"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v5, 0x5

    goto :goto_2

    :sswitch_6
    const-string v11, "v"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v5, 0x6

    goto/16 :goto_2

    :sswitch_7
    const-string v11, "cite"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/4 v5, 0x7

    goto/16 :goto_2

    :sswitch_8
    const-string v11, "text-author"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0x8

    goto/16 :goto_2

    :sswitch_9
    const-string v11, "emphasis"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0x9

    goto/16 :goto_2

    :sswitch_a
    const-string v11, "strong"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    move v5, v9

    goto/16 :goto_2

    :sswitch_b
    const-string v11, "sub"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0xb

    goto/16 :goto_2

    :sswitch_c
    const-string v11, "sup"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0xc

    goto/16 :goto_2

    :sswitch_d
    const-string v11, "strikethrough"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0xd

    goto/16 :goto_2

    :sswitch_e
    const-string v11, "empty-line"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    const/16 v5, 0xe

    goto/16 :goto_2

    .line 446
    :pswitch_1
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 447
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 451
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const v8, 0x3f99999a    # 1.2f

    invoke-direct {v5, v8}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 452
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 451
    invoke-virtual {v0, v5, v6, v8, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 453
    new-instance v5, Landroid/text/style/AlignmentSpan$Standard;

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-direct {v5, v8}, Landroid/text/style/AlignmentSpan$Standard;-><init>(Landroid/text/Layout$Alignment;)V

    .line 454
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v8

    .line 453
    invoke-virtual {v0, v5, v6, v8, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 455
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 456
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 455
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 459
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 460
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 465
    .end local v1    # "builderLength":I
    :pswitch_2
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 466
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 470
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 471
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 476
    .end local v1    # "builderLength":I
    :pswitch_3
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 477
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 481
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 482
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 487
    .end local v1    # "builderLength":I
    :pswitch_4
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 488
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 492
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 493
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 498
    .end local v1    # "builderLength":I
    :pswitch_5
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 499
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 503
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 504
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 503
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 506
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 507
    const-string v5, "\n\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 512
    .end local v1    # "builderLength":I
    :pswitch_6
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 513
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 517
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 518
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 523
    .end local v1    # "builderLength":I
    :pswitch_7
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 524
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 528
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 529
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 528
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 533
    .end local v1    # "builderLength":I
    :pswitch_8
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 534
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 538
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v5

    if-eq v5, v9, :cond_0

    .line 539
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 544
    .end local v1    # "builderLength":I
    :pswitch_9
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 545
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 549
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v8}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 550
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 549
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 554
    .end local v1    # "builderLength":I
    :pswitch_a
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 555
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 559
    new-instance v5, Landroid/text/style/StyleSpan;

    invoke-direct {v5, v7}, Landroid/text/style/StyleSpan;-><init>(I)V

    .line 560
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 559
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 564
    .end local v1    # "builderLength":I
    :pswitch_b
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 565
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 569
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-direct {v5, v7}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 570
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 569
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 571
    new-instance v5, Landroid/text/style/SubscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SubscriptSpan;-><init>()V

    .line 572
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 571
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 576
    .end local v1    # "builderLength":I
    :pswitch_c
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 577
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 581
    new-instance v5, Landroid/text/style/RelativeSizeSpan;

    const/high16 v7, 0x3f000000    # 0.5f

    invoke-direct {v5, v7}, Landroid/text/style/RelativeSizeSpan;-><init>(F)V

    .line 582
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 581
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 583
    new-instance v5, Landroid/text/style/SuperscriptSpan;

    invoke-direct {v5}, Landroid/text/style/SuperscriptSpan;-><init>()V

    .line 584
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 583
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 588
    .end local v1    # "builderLength":I
    :pswitch_d
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 589
    .restart local v1    # "builderLength":I
    if-lt v1, v7, :cond_0

    .line 593
    new-instance v5, Landroid/text/style/StrikethroughSpan;

    invoke-direct {v5}, Landroid/text/style/StrikethroughSpan;-><init>()V

    .line 594
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    .line 593
    invoke-virtual {v0, v5, v6, v7, v12}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto/16 :goto_0

    .line 599
    .end local v1    # "builderLength":I
    :pswitch_e
    const-string v5, "\n"

    invoke-virtual {v0, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto/16 :goto_0

    .line 425
    :sswitch_data_0
    .sparse-switch
        -0x7ad0b3e8 -> :sswitch_1
        -0x41e56b8c -> :sswitch_e
        -0x39f7812d -> :sswitch_d
        -0x3532460b -> :sswitch_5
        -0x352a8969 -> :sswitch_a
        -0x2661c510 -> :sswitch_3
        0x70 -> :sswitch_2
        0x76 -> :sswitch_6
        0x1be40 -> :sswitch_b
        0x1be4e -> :sswitch_c
        0x2e9957 -> :sswitch_7
        0x3496e7 -> :sswitch_4
        0x6942258 -> :sswitch_0
        0x46e4157c -> :sswitch_9
        0x635c8a2b -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private parseZippedBook(Ljava/lang/String;)Lorg/w3c/dom/Document;
    .locals 8
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 135
    .local v0, "bookDocument":Lorg/w3c/dom/Document;
    :try_start_0
    new-instance v4, Ljava/util/zip/ZipFile;

    invoke-direct {v4, p1}, Ljava/util/zip/ZipFile;-><init>(Ljava/lang/String;)V

    .line 136
    .local v4, "zipFile":Ljava/util/zip/ZipFile;
    invoke-virtual {v4}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v5

    invoke-static {v5}, Ljava/util/Collections;->list(Ljava/util/Enumeration;)Ljava/util/ArrayList;

    move-result-object v2

    .line 138
    .local v2, "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/zip/ZipEntry;

    .line 139
    .local v3, "zipEntry":Ljava/util/zip/ZipEntry;
    invoke-virtual {v3}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "fb2"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 140
    invoke-virtual {v4, v3}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v5

    invoke-static {v5}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/InputStream;)Lorg/w3c/dom/Document;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 147
    .end local v3    # "zipEntry":Ljava/util/zip/ZipEntry;
    :cond_1
    return-object v0

    .line 144
    .end local v2    # "zipEntries":Ljava/util/List;, "Ljava/util/List<+Ljava/util/zip/ZipEntry;>;"
    .end local v4    # "zipFile":Ljava/util/zip/ZipFile;
    :catch_0
    move-exception v1

    .line 145
    .local v1, "e":Ljava/io/IOException;
    new-instance v5, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v5, v1}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method

.method private saveCover(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "binaryCover"    # Ljava/lang/String;
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "directoryPath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 242
    const/4 v5, 0x0

    :try_start_0
    invoke-static {p1, v5}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v2

    .line 243
    .local v2, "imageByte":[B
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 245
    .local v4, "imagePath":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 246
    .local v3, "imageFile":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 247
    invoke-virtual {v3}, Ljava/io/File;->createNewFile()Z

    .line 249
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 250
    .local v1, "fileOutputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v1, v2}, Ljava/io/FileOutputStream;->write([B)V

    .line 251
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    return-object v4

    .line 254
    .end local v1    # "fileOutputStream":Ljava/io/FileOutputStream;
    .end local v2    # "imageByte":[B
    .end local v3    # "imageFile":Ljava/io/File;
    .end local v4    # "imagePath":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 255
    .local v0, "e":Ljava/io/IOException;
    new-instance v5, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;

    invoke-direct {v5, v0}, Lcom/safonov/speedreading/reader/repository/exception/BookParserException;-><init>(Ljava/lang/Throwable;)V

    throw v5
.end method


# virtual methods
.method public addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 17
    .param p1, "filePath"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;,
            Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
        }
    .end annotation

    .prologue
    .line 86
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-object/from16 v0, p1

    invoke-interface {v15, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v4

    .line 87
    .local v4, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    if-eqz v4, :cond_0

    .line 88
    new-instance v15, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;

    invoke-direct {v15}, Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;-><init>()V

    throw v15

    .line 91
    :cond_0
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v15}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v6

    .line 93
    .local v6, "bookId":J
    new-instance v4, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .end local v4    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-direct {v4}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 94
    .restart local v4    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v4, v6, v7}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 95
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 96
    const-string v15, "fb2"

    invoke-virtual {v4, v15}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 98
    const/4 v5, 0x0

    .line 99
    .local v5, "bookDocument":Lorg/w3c/dom/Document;
    invoke-static/range {p1 .. p1}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const-string v16, "fb2.zip"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 100
    invoke-direct/range {p0 .. p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseZippedBook(Ljava/lang/String;)Lorg/w3c/dom/Document;

    move-result-object v5

    .line 105
    :goto_0
    const-string v15, "description"

    invoke-interface {v5, v15}, Lorg/w3c/dom/Document;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v12

    check-cast v12, Lorg/w3c/dom/Element;

    .line 106
    .local v12, "description":Lorg/w3c/dom/Element;
    const-string v15, "title-info"

    invoke-interface {v12, v15}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v15

    const/16 v16, 0x0

    invoke-interface/range {v15 .. v16}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v14

    check-cast v14, Lorg/w3c/dom/Element;

    .line 108
    .local v14, "titleInfo":Lorg/w3c/dom/Element;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->getBookTitle(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v9

    .line 109
    .local v9, "bookTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->getBookLanguage(Lorg/w3c/dom/Element;)Ljava/lang/String;

    move-result-object v8

    .line 110
    .local v8, "bookLanguage":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->getBookAuthors(Lorg/w3c/dom/Element;)Ljava/util/List;

    move-result-object v2

    .line 112
    .local v2, "authors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-virtual {v4, v9}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 113
    invoke-virtual {v4, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 114
    const/4 v15, 0x0

    invoke-interface {v2, v15}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-virtual {v4, v15}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 116
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->libraryFilePath:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    sget-object v16, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 118
    .local v13, "saveDirectoryPath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseCover(Lorg/w3c/dom/Document;)Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;

    move-result-object v11

    .line 119
    .local v11, "coverParseWrapper":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;
    if-eqz v11, :cond_1

    .line 120
    invoke-virtual {v11}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->getBinaryCover()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v11}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;->getFileName()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1, v13}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->saveCover(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 121
    .local v10, "coverImagePath":Ljava/lang/String;
    invoke-virtual {v4, v10}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setCoverImagePath(Ljava/lang/String;)V

    .line 124
    .end local v10    # "coverImagePath":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->parseBook(Lorg/w3c/dom/Document;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v3

    .line 125
    .local v3, "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    invoke-static {v4, v3, v13}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->saveBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/repository/entity/BookContent;Ljava/lang/String;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v15, v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 129
    return-object v4

    .line 102
    .end local v2    # "authors":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v3    # "bookContent":Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .end local v8    # "bookLanguage":Ljava/lang/String;
    .end local v9    # "bookTitle":Ljava/lang/String;
    .end local v11    # "coverParseWrapper":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao$CoverParseWrapper;
    .end local v12    # "description":Lorg/w3c/dom/Element;
    .end local v13    # "saveDirectoryPath":Ljava/lang/String;
    .end local v14    # "titleInfo":Lorg/w3c/dom/Element;
    :cond_2
    new-instance v15, Ljava/io/File;

    move-object/from16 v0, p1

    invoke-direct {v15, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v15}, Lcom/safonov/speedreading/reader/repository/util/XmlParserUtil;->getXmlFromFile(Ljava/io/File;)Lorg/w3c/dom/Document;

    move-result-object v5

    goto/16 :goto_0
.end method

.method public getBookContent(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
        }
    .end annotation

    .prologue
    .line 153
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->libraryFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    .local v0, "directoryPath":Ljava/lang/String;
    invoke-static {p1, v0}, Lcom/safonov/speedreading/reader/repository/util/JsonUtil;->readBook(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    move-result-object v1

    return-object v1
.end method

.method public removeBook(J)V
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->bookDescriptionDao:Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->removeBookDescription(J)V

    .line 160
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;->libraryFilePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/safonov/speedreading/reader/repository/util/FileUtil;->removeDirectory(Ljava/io/File;)Z

    .line 161
    return-void
.end method
