.class public Lcom/safonov/speedreading/reader/repository/entity/BookContent;
.super Ljava/lang/Object;
.source "BookContent.java"


# instance fields
.field private bookChapterList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookChapter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBookChapterList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookChapter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    return-object v0
.end method

.method public getChaptersText()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    .local v1, "chapterTextList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/CharSequence;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;

    .line 27
    .local v0, "chapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;->getBookChapter()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 30
    .end local v0    # "chapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    :cond_0
    return-object v1
.end method

.method public getTitles()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 36
    .local v1, "titleList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;

    .line 37
    .local v0, "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookChapter;->getTitle()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 40
    .end local v0    # "bookChapter":Lcom/safonov/speedreading/reader/repository/entity/BookChapter;
    :cond_0
    return-object v1
.end method

.method public setBookChapterList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookChapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p1, "bookChapterList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookChapter;>;"
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookContent;->bookChapterList:Ljava/util/List;

    .line 20
    return-void
.end method
