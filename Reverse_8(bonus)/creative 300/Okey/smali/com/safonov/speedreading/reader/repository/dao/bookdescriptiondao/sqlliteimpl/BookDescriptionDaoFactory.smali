.class public abstract Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;
.super Ljava/lang/Object;
.source "BookDescriptionDaoFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 13
    new-instance v0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public abstract getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
.end method
