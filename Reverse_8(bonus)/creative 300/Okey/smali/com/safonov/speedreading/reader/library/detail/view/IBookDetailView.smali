.class public interface abstract Lcom/safonov/speedreading/reader/library/detail/view/IBookDetailView;
.super Ljava/lang/Object;
.source "IBookDetailView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setAuthorView(Ljava/lang/String;)V
.end method

.method public abstract setAuthorViewVisibility(Z)V
.end method

.method public abstract setCoverView(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setFilePathView(Ljava/lang/String;)V
.end method

.method public abstract setLanguageView(Ljava/lang/String;)V
.end method

.method public abstract setLanguageViewVisibility(Z)V
.end method

.method public abstract setTitleView(Ljava/lang/String;)V
.end method
