.class public Lcom/safonov/speedreading/reader/reader/util2/WordSelector;
.super Ljava/lang/Object;
.source "WordSelector.java"


# static fields
.field private static final pattern:Ljava/util/regex/Pattern;

.field private static final regExp:Ljava/lang/String; = "\\b([\\w]+)\\b"


# instance fields
.field private matcher:Ljava/util/regex/Matcher;

.field private page:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "\\b([\\w]+)\\b"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->pattern:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "page"    # Ljava/lang/CharSequence;

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->page:Ljava/lang/CharSequence;

    .line 26
    sget-object v0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->pattern:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->matcher:Ljava/util/regex/Matcher;

    .line 27
    return-void
.end method


# virtual methods
.method public getPageWithNextSelectedWord()Landroid/text/Spanned;
    .locals 5

    .prologue
    .line 30
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 33
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v2

    .line 34
    .local v2, "startIndex":I
    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->matcher:Ljava/util/regex/Matcher;

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v0

    .line 36
    .local v0, "endIndex":I
    new-instance v1, Landroid/text/SpannableString;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/util2/WordSelector;->page:Ljava/lang/CharSequence;

    invoke-direct {v1, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 37
    .local v1, "spannableString":Landroid/text/SpannableString;
    new-instance v3, Landroid/text/style/BackgroundColorSpan;

    const v4, -0xff0001

    invoke-direct {v3, v4}, Landroid/text/style/BackgroundColorSpan;-><init>(I)V

    const/16 v4, 0x21

    invoke-virtual {v1, v3, v2, v0, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 42
    .end local v0    # "endIndex":I
    .end local v1    # "spannableString":Landroid/text/SpannableString;
    .end local v2    # "startIndex":I
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
