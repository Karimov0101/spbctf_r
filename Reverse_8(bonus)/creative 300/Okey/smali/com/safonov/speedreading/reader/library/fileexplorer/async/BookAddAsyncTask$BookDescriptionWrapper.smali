.class Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;
.super Ljava/lang/Object;
.source "BookAddAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "BookDescriptionWrapper"
.end annotation


# instance fields
.field private bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

.field private status:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;
    .param p2, "status"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->status:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;
    .param p2, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .param p3, "status"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->this$0:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 42
    iput-object p3, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->status:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    .line 43
    return-void
.end method


# virtual methods
.method public getBookDescription()Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    return-object v0
.end method

.method public getStatus()Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->status:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    return-object v0
.end method
