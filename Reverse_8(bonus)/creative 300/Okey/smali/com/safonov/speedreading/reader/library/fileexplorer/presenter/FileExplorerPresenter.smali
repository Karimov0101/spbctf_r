.class public Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "FileExplorerPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;"
    }
.end annotation


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private fileExplorerFileWrapperList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

.field private final rootFile:Ljava/io/File;

.field private selectedFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;)V
    .locals 1
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 34
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->rootFile:Ljava/io/File;

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    .line 29
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 30
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    .line 31
    return-void
.end method

.method private setSelectedFile(Ljava/io/File;)V
    .locals 4
    .param p1, "file"    # Ljava/io/File;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->selectedFile:Ljava/io/File;

    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->rootFile:Ljava/io/File;

    invoke-virtual {p1, v0}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    sget-object v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->PARENT_FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    invoke-static {v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/util/FileExplorerUtils;->filterFiles([Ljava/io/File;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;->setData(Ljava/util/List;)V

    .line 68
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;->setActionBarTitle(Ljava/lang/String;)V

    .line 70
    :cond_1
    return-void
.end method


# virtual methods
.method public init()V
    .locals 1

    .prologue
    .line 40
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->setSelectedFile(Ljava/io/File;)V

    .line 41
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->selectedFile:Ljava/io/File;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->rootFile:Ljava/io/File;

    invoke-virtual {v0, v1}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;->closeFileExplorer()V

    .line 79
    :goto_0
    return-void

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->selectedFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->setSelectedFile(Ljava/io/File;)V

    goto :goto_0
.end method

.method public onItemClick(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 45
    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->fileExplorerFileWrapperList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    .line 46
    .local v0, "fileWrapper":Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;
    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter$1;->$SwitchMap$com$safonov$speedreading$reader$library$fileexplorer$model$FileExplorerFileType:[I

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->getType()Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 52
    new-instance v1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->getFile()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 55
    :goto_0
    return-void

    .line 49
    :pswitch_0
    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;->getFile()Ljava/io/File;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;->setSelectedFile(Ljava/io/File;)V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
