.class public interface abstract Lcom/safonov/speedreading/reader/reader/view/IReaderView;
.super Ljava/lang/Object;
.source "IReaderView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract dismissProgressDialog()V
.end method

.method public abstract finish()V
.end method

.method public abstract hideActionBar()V
.end method

.method public abstract initNavigationSeekBar(I)V
.end method

.method public abstract initProgressViewProgress(I)V
.end method

.method public abstract onRequestToGetTextViewData()V
.end method

.method public abstract seNavigationViewVisibility(Z)V
.end method

.method public abstract setActionBarSelectReadingModeVisibility(Z)V
.end method

.method public abstract setActionBarTitle(Ljava/lang/String;)V
.end method

.method public abstract setAd()V
.end method

.method public abstract setBookPurchaseActivity()V
.end method

.method public abstract setCourseActivityResult(Z)V
.end method

.method public abstract setNavigationChapterView(Ljava/lang/String;)V
.end method

.method public abstract setNavigationPageView(II)V
.end method

.method public abstract setNavigationSeekBar(I)V
.end method

.method public abstract setPageView(II)V
.end method

.method public abstract setPencilFrameLineCountView(I)V
.end method

.method public abstract setPencilFrameView(II)V
.end method

.method public abstract setPencilFrameViewVisibility(Z)V
.end method

.method public abstract setPrimaryTextColor()V
.end method

.method public abstract setProgressViewProgress(I)V
.end method

.method public abstract setProgressViewVisibility(Z)V
.end method

.method public abstract setSecondaryTextColor()V
.end method

.method public abstract setSpeedView(I)V
.end method

.method public abstract setSpeedViewVisibility(Z)V
.end method

.method public abstract setText(Ljava/lang/CharSequence;)V
.end method

.method public abstract setTimerView(J)V
.end method

.method public abstract setTimerViewVisibility(Z)V
.end method

.method public abstract showActionBar()V
.end method

.method public abstract showErrorMessage()V
.end method

.method public abstract showExitDialog()V
.end method

.method public abstract showGuideDialog()V
.end method

.method public abstract showProgressDialog()V
.end method

.method public abstract showPurchasePremiumDialog()V
.end method

.method public abstract showSelectChapterDialog([Ljava/lang/String;)V
.end method

.method public abstract showSelectPageDialog(II)V
.end method

.method public abstract showSelectReadingModeDialog(I)V
.end method

.method public abstract showTimerModeCompletedDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V
    .param p1    # [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract showTimerModeDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V
    .param p1    # [Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
