.class public Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;
.source "LibraryActivity.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpActivity",
        "<",
        "Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;",
        "Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;"
    }
.end annotation


# instance fields
.field private currentFragment:Landroid/support/v4/app/Fragment;

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;-><init>()V

    return-void
.end method

.method private setFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 77
    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 80
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 81
    return-void
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->createPresenter()Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 35
    new-instance v0, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/LibraryPresenter;-><init>()V

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->onBackPressed()V

    .line 71
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f0b0090

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->setContentView(I)V

    .line 47
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->unbinder:Lbutterknife/Unbinder;

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->init()V

    .line 55
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onDestroy()V

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 135
    return-void
.end method

.method public onFileExplorerBackPressed()V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;->onBackPressed()V

    .line 124
    return-void
.end method

.method public onFileExplorerBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 114
    return-void
.end method

.method public onFileExplorerClose()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->requestToSetLibraryFragment()V

    .line 129
    return-void
.end method

.method public onLibraryAddBookClick()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->requestToSetFileExplorerFragment()V

    .line 104
    return-void
.end method

.method public onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 109
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 64
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 61
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;->onActionBarHomePressed()V

    .line 62
    const/4 v0, 0x1

    goto :goto_0

    .line 59
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 119
    return-void
.end method

.method public setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 2
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 97
    invoke-static {p1}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->newInstance(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 98
    return-void
.end method

.method public setFileExplorerFragment()V
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->newInstance()Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 92
    return-void
.end method

.method public setLibraryFragment()V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0098

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 86
    invoke-static {}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->newInstance()Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 87
    return-void
.end method
