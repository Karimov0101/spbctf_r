.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 840
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 843
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 884
    :cond_0
    :goto_0
    return-void

    .line 845
    :cond_1
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    move-result-object v1

    if-nez v1, :cond_2

    .line 846
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    new-instance v2, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$2000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v5}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$2100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;-><init>(Landroid/text/TextPaint;Ljava/lang/CharSequence;I)V

    invoke-static {v1, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1102(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 849
    :cond_2
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget v2, v2, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget v3, v3, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    invoke-virtual {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;->getFrameCoordinates(II)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;

    move-result-object v0

    .line 850
    .local v0, "dataSet":Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
    if-nez v0, :cond_5

    .line 851
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iput v6, v1, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    .line 852
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1102(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 854
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 855
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 856
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_3

    .line 857
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$408(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    .line 859
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPageView(II)V

    .line 860
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationSeekBar(I)V

    .line 861
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    .line 862
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationPageView(II)V

    .line 864
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 868
    :cond_3
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 870
    :cond_4
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1, v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 872
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 873
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 877
    :cond_5
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget v2, v1, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget v3, v3, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    add-int/2addr v2, v3

    iput v2, v1, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    .line 878
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 879
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->getFrameTopY()I

    move-result v2

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->getFrameBottomY()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameView(II)V

    .line 882
    :cond_6
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;

    move-result-object v1

    const v2, 0xea60

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->getWordCount()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1600(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v3

    div-int/2addr v2, v3

    int-to-long v2, v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
