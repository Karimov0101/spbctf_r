.class public Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
.super Ljava/lang/Object;
.source "BookDescription.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private author:Ljava/lang/String;

.field private bookOffset:J

.field private coverImagePath:Ljava/lang/String;

.field private filePath:Ljava/lang/String;

.field private id:J

.field private isFavorite:Z

.field private language:Ljava/lang/String;

.field private progress:I

.field private title:Ljava/lang/String;

.field private type:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 143
    new-instance v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription$1;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription$1;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2
    .param p1, "in"    # Landroid/os/Parcel;

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->id:J

    .line 112
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->bookOffset:J

    .line 113
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->progress:I

    .line 114
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->title:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->author:Ljava/lang/String;

    .line 116
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->language:Ljava/lang/String;

    .line 117
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->type:Ljava/lang/String;

    .line 118
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->filePath:Ljava/lang/String;

    .line 119
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->isFavorite:Z

    .line 120
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->coverImagePath:Ljava/lang/String;

    .line 121
    return-void

    .line 119
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 125
    const/4 v0, 0x0

    return v0
.end method

.method public getAuthor()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->author:Ljava/lang/String;

    return-object v0
.end method

.method public getBookOffset()J
    .locals 2

    .prologue
    .line 37
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->bookOffset:J

    return-wide v0
.end method

.method public getCoverImagePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->coverImagePath:Ljava/lang/String;

    return-object v0
.end method

.method public getFilePath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->filePath:Ljava/lang/String;

    return-object v0
.end method

.method public getId()J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->id:J

    return-wide v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->language:Ljava/lang/String;

    return-object v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->progress:I

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->title:Ljava/lang/String;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->type:Ljava/lang/String;

    return-object v0
.end method

.method public isFavorite()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->isFavorite:Z

    return v0
.end method

.method public setAuthor(Ljava/lang/String;)V
    .locals 0
    .param p1, "author"    # Ljava/lang/String;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->author:Ljava/lang/String;

    .line 66
    return-void
.end method

.method public setBookOffset(J)V
    .locals 1
    .param p1, "bookOffset"    # J

    .prologue
    .line 41
    iput-wide p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->bookOffset:J

    .line 42
    return-void
.end method

.method public setCoverImagePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "coverImagePath"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->coverImagePath:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public setFavorite(Z)V
    .locals 0
    .param p1, "favorite"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->isFavorite:Z

    .line 98
    return-void
.end method

.method public setFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 89
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->filePath:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public setId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->id:J

    .line 34
    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 0
    .param p1, "language"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->language:Ljava/lang/String;

    .line 74
    return-void
.end method

.method public setProgress(I)V
    .locals 0
    .param p1, "progress"    # I

    .prologue
    .line 49
    iput p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->progress:I

    .line 50
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->title:Ljava/lang/String;

    .line 58
    return-void
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->type:Ljava/lang/String;

    .line 82
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 130
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->id:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 131
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->bookOffset:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 132
    iget v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->progress:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->title:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->author:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->language:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->type:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 137
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->filePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 138
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->isFavorite:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 139
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->coverImagePath:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 140
    return-void

    .line 138
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
