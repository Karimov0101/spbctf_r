.class public Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;
.super Ljava/lang/Object;
.source "Fb2BookChapter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/entity/BookChapter;


# instance fields
.field private content:Ljava/lang/CharSequence;

.field private title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "content"    # Ljava/lang/CharSequence;

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->title:Ljava/lang/CharSequence;

    .line 17
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->content:Ljava/lang/CharSequence;

    .line 18
    return-void
.end method


# virtual methods
.method public getBookChapter()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 40
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 42
    .local v0, "builder":Landroid/text/SpannableStringBuilder;
    iget-object v3, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->title:Ljava/lang/CharSequence;

    if-eqz v3, :cond_0

    .line 43
    iget-object v3, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 46
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->content:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1

    .line 47
    iget-object v3, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->content:Ljava/lang/CharSequence;

    invoke-virtual {v0, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 51
    :cond_1
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 52
    .local v1, "builderLength":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_2

    .line 53
    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v3

    const/16 v4, 0xa

    if-eq v3, v4, :cond_3

    .line 54
    const/4 v3, 0x0

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/text/SpannableStringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 58
    .end local v0    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_2
    return-object v0

    .line 52
    .restart local v0    # "builder":Landroid/text/SpannableStringBuilder;
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_0
.end method

.method public getContent()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->content:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/CharSequence;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->content:Ljava/lang/CharSequence;

    .line 36
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 26
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/Fb2BookChapter;->title:Ljava/lang/CharSequence;

    .line 27
    return-void
.end method
