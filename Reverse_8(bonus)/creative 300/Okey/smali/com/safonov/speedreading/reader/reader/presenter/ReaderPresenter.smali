.class public Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "ReaderPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/reader/reader/view/IReaderView;",
        ">;",
        "Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;"
    }
.end annotation


# static fields
.field private static final DEFAULT_PAGE_CHANGE_DELAY:I = 0xc8

.field private static final MAX_PREMIUM_SPEED:I = 0xbb8

.field private static final MAX_SPEED:I = 0x226

.field private static final MIN_SPEED:I = 0x64

.field private static final PENCIL_FRAME_SELECTED_LINE_COUNT_MAX:I = 0xa

.field private static final PENCIL_FRAME_SELECTED_LINE_COUNT_MIN:I = 0x1

.field private static final SPEED_STEP:I = 0x32


# instance fields
.field private bookContent:Lcom/safonov/speedreading/reader/repository/entity/BookContent;

.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

.field private bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

.field private fastReadingPaused:Z

.field private fastReadingThread:Ljava/lang/Runnable;

.field private fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

.field private flashModeCurrentTime:I

.field private flashReadingThread:Ljava/lang/Runnable;

.field private handler:Landroid/os/Handler;

.field private isFirstLaunch:Z

.field private navigationViewShowing:Z

.field private pageCount:I

.field private pageIndex:I

.field private pageWidth:I

.field pencilFrameLineIndex:I

.field private pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

.field private pencilFrameReadingThread:Ljava/lang/Runnable;

.field pencilFrameSelectedLineCount:I

.field private preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private previousAdsPage:I

.field private readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

.field private readingMode:I

.field private speed:I

.field private speedHasChanged:Z

.field private speedReadingBookId:J

.field private splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

.field private textPaint:Landroid/text/TextPaint;

.field private timerMode:I

.field private timerModeIndex:I

.field private timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

.field private timerRemindingTime:J

.field private timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;J)V
    .locals 2
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "readerPreferenceUtil"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "readerTimerModeSaveUtil"    # Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "speedReadingBookId"    # J

    .prologue
    const/4 v1, 0x1

    .line 58
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 48
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 373
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->previousAdsPage:I

    .line 375
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBookUtil()Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    .line 640
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isFirstLaunch:Z

    .line 687
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->handler:Landroid/os/Handler;

    .line 698
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 703
    new-instance v0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$4;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingThread:Ljava/lang/Runnable;

    .line 751
    new-instance v0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$5;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashReadingThread:Ljava/lang/Runnable;

    .line 840
    new-instance v0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$6;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameReadingThread:Ljava/lang/Runnable;

    .line 60
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 61
    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    .line 62
    iput-object p3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    .line 63
    iput-wide p4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedReadingBookId:J

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    return v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/WordSelector;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isFirstLaunch:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isFirstLaunch:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    return v0
.end method

.method static synthetic access$1500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    return-void
.end method

.method static synthetic access$1600(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    return v0
.end method

.method static synthetic access$1700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedHasChanged:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedHasChanged:Z

    return p1
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    return v0
.end method

.method static synthetic access$2000(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Landroid/text/TextPaint;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->textPaint:Landroid/text/TextPaint;

    return-object v0
.end method

.method static synthetic access$208(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    return v0
.end method

.method static synthetic access$210(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    return v0
.end method

.method static synthetic access$2100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageWidth:I

    return v0
.end method

.method static synthetic access$302(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;J)J
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # J

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    return-wide p1
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    return v0
.end method

.method static synthetic access$402(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    return p1
.end method

.method static synthetic access$408(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    return-object v0
.end method

.method static synthetic access$502(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    return-object p1
.end method

.method static synthetic access$602(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookContent:Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    return-object p1
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    return v0
.end method

.method static synthetic access$802(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    return p1
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    return v0
.end method

.method static synthetic access$902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    return p1
.end method

.method private updateSpeedReadingBookView()Z
    .locals 5

    .prologue
    const/16 v4, 0x1e

    .line 605
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedReadingBookId:J

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 607
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-static {v0, v1}, Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;->getProgress(II)I

    move-result v0

    if-le v0, v4, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/BookUtil;->isBookPurchased()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 608
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setBookPurchaseActivity()V

    .line 609
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-static {v4, v0}, Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;->getPageFromProgress(II)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 610
    const/4 v0, 0x0

    .line 622
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private updateViewsOnPageChange()V
    .locals 3

    .prologue
    .line 628
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 629
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 630
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 631
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPageView(II)V

    .line 633
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationSeekBar(I)V

    .line 634
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    .line 635
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationPageView(II)V

    .line 637
    :cond_0
    return-void
.end method


# virtual methods
.method public init(IJ)V
    .locals 8
    .param p1, "selectedTimerMode"    # I
    .param p2, "bookDescriptionId"    # J

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 80
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    .line 82
    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-nez v2, :cond_1

    .line 83
    iput v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    .line 142
    :goto_0
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->getSpeed()I

    move-result v2

    iput v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 143
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->getPencilSelectedLineCount()I

    move-result v2

    iput v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    .line 145
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v2, p2, p3}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .line 147
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-virtual {v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setActionBarTitle(Ljava/lang/String;)V

    .line 151
    :cond_0
    new-instance v2, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    new-instance v4, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    invoke-direct {v2, v3, v4}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;)V

    new-array v3, v7, [Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    iget-object v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    aput-object v4, v3, v6

    .line 176
    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 177
    return-void

    .line 85
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v2, v7}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setTimerViewVisibility(Z)V

    .line 89
    :cond_2
    iput v6, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    .line 90
    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/util/TimerModeUtil;->getTimerModeModels(I)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    .line 92
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->load(I)Lcom/safonov/speedreading/training/util/course/TimerModeSave;

    move-result-object v1

    .line 93
    .local v1, "save":Lcom/safonov/speedreading/training/util/course/TimerModeSave;
    if-eqz v1, :cond_3

    .line 94
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, v1, Lcom/safonov/speedreading/training/util/course/TimerModeSave;->remainingTimes:[J

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 95
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    aget-object v2, v2, v0

    iget-object v3, v1, Lcom/safonov/speedreading/training/util/course/TimerModeSave;->remainingTimes:[J

    aget-wide v4, v3, v0

    invoke-virtual {v2, v4, v5}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->setRemainingTime(J)V

    .line 96
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->isCompleted()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 97
    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 104
    .end local v0    # "i":I
    :cond_3
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getRemainingTime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    .line 106
    new-instance v2, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v2}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 107
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v3, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$1;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    goto/16 :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 679
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 680
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 681
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showExitDialog()V

    .line 683
    :cond_0
    return-void
.end method

.method public onGuideDialogClosed(Z)V
    .locals 2
    .param p1, "shouldShowAgain"    # Z

    .prologue
    .line 308
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->setShouldShowGuideDialog(Z)V

    .line 310
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-nez v0, :cond_1

    .line 311
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showSelectReadingModeDialog(I)V

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 317
    :cond_1
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 318
    :cond_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 319
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showTimerModeDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V

    goto :goto_0
.end method

.method public onNavigationChapterSelected(I)V
    .locals 2
    .param p1, "chapterIndex"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 546
    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    .line 547
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 549
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 550
    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    .line 552
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPageIndexByTitle(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 553
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 554
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 556
    :cond_0
    return-void
.end method

.method public onNavigationChapterViewClick()V
    .locals 2

    .prologue
    .line 536
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 537
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v0, v1, [Ljava/lang/String;

    .line 538
    .local v0, "items":[Ljava/lang/String;
    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitles()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "items":[Ljava/lang/String;
    check-cast v0, [Ljava/lang/String;

    .line 540
    .restart local v0    # "items":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v1, v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showSelectChapterDialog([Ljava/lang/String;)V

    .line 542
    .end local v0    # "items":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onNavigationNextPageViewClick()V
    .locals 2

    .prologue
    .line 483
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 484
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 485
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 487
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    .line 488
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 490
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 493
    :cond_0
    return-void
.end method

.method public onNavigationPageSelected(I)V
    .locals 1
    .param p1, "pageIndex"    # I

    .prologue
    const/4 v0, 0x0

    .line 569
    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 570
    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 571
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    .line 573
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 574
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 575
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 577
    :cond_0
    return-void
.end method

.method public onNavigationPageViewClick()V
    .locals 3

    .prologue
    .line 560
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 561
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showSelectPageDialog(II)V

    .line 565
    :cond_0
    return-void
.end method

.method public onNavigationPreviousPageViewClick()V
    .locals 1

    .prologue
    .line 497
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    if-lez v0, :cond_0

    .line 498
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 500
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    .line 501
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 503
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 505
    :cond_0
    return-void
.end method

.method public onNavigationSeekBarPageChanged(I)V
    .locals 3
    .param p1, "pageIndex"    # I

    .prologue
    .line 509
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 510
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 511
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    invoke-virtual {v1, p1}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getTitle(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationChapterView(Ljava/lang/String;)V

    .line 512
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setNavigationPageView(II)V

    .line 514
    :cond_0
    return-void
.end method

.method public onNavigationSeekBarPageSelected()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 519
    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashModeCurrentTime:I

    .line 520
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 522
    iput-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFramePageSelector:Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 523
    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameLineIndex:I

    .line 525
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPageView(II)V

    .line 528
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 529
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 532
    :cond_0
    return-void
.end method

.method public onPageViewCenterClick(Z)V
    .locals 6
    .param p1, "isDoubleClick"    # Z

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 412
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-nez v0, :cond_6

    .line 413
    if-eqz p1, :cond_2

    .line 414
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 415
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    if-eqz v0, :cond_3

    .line 416
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->hideActionBar()V

    .line 417
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->seNavigationViewVisibility(Z)V

    .line 419
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-ne v0, v3, :cond_1

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    invoke-virtual {v0, v4, v5}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 431
    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    .line 479
    :cond_2
    :goto_2
    return-void

    .line 423
    :cond_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showActionBar()V

    .line 424
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->seNavigationViewVisibility(Z)V

    .line 426
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_4

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-ne v0, v3, :cond_1

    .line 427
    :cond_4
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    goto :goto_0

    :cond_5
    move v0, v2

    .line 431
    goto :goto_1

    .line 435
    :cond_6
    if-eqz p1, :cond_d

    .line 436
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    if-nez v0, :cond_9

    .line 437
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_7

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-ne v0, v3, :cond_8

    .line 438
    :cond_7
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    .line 440
    :cond_8
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 443
    :cond_9
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    if-eqz v0, :cond_b

    .line 444
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 445
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->hideActionBar()V

    .line 446
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->seNavigationViewVisibility(Z)V

    .line 455
    :cond_a
    :goto_3
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    if-nez v0, :cond_c

    :goto_4
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    goto :goto_2

    .line 449
    :cond_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 450
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showActionBar()V

    .line 451
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->seNavigationViewVisibility(Z)V

    goto :goto_3

    :cond_c
    move v1, v2

    .line 455
    goto :goto_4

    .line 457
    :cond_d
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->navigationViewShowing:Z

    if-nez v0, :cond_2

    .line 458
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    if-nez v0, :cond_e

    move v2, v1

    :cond_e
    iput-boolean v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 459
    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    if-nez v0, :cond_13

    .line 460
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-ne v0, v1, :cond_11

    .line 461
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingThread:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 468
    :cond_f
    :goto_5
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_10

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-ne v0, v3, :cond_2

    .line 469
    :cond_10
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    goto/16 :goto_2

    .line 462
    :cond_11
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-ne v0, v3, :cond_12

    .line 463
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->flashReadingThread:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_5

    .line 464
    :cond_12
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_f

    .line 465
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameReadingThread:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_5

    .line 472
    :cond_13
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_14

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-ne v0, v3, :cond_2

    .line 473
    :cond_14
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    goto/16 :goto_2
.end method

.method public onPageViewLeftClick()V
    .locals 2

    .prologue
    const/16 v1, 0x64

    .line 356
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-nez v0, :cond_1

    .line 357
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    if-lez v0, :cond_0

    .line 358
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 359
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 362
    :cond_1
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    add-int/lit8 v0, v0, -0x32

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 363
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    if-ge v0, v1, :cond_2

    .line 364
    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 366
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedHasChanged:Z

    .line 367
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    goto :goto_0
.end method

.method public onPageViewRightClick()V
    .locals 4

    .prologue
    const/16 v3, 0xbb8

    const/16 v2, 0x226

    const/4 v1, 0x1

    .line 379
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-nez v0, :cond_1

    .line 380
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 381
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateSpeedReadingBookView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 382
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    .line 383
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->updateViewsOnPageChange()V

    .line 408
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    add-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 388
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 389
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    if-le v0, v3, :cond_2

    .line 390
    iput v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 403
    :cond_2
    :goto_1
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speedHasChanged:Z

    .line 404
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    goto :goto_0

    .line 393
    :cond_3
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    if-le v0, v2, :cond_2

    .line 394
    iput v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    .line 395
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 397
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showPurchasePremiumDialog()V

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 644
    iput-boolean v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 645
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    .line 648
    :cond_1
    return-void
.end method

.method public onPencilFrameMinusClick()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 584
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    .line 585
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    if-ge v0, v1, :cond_0

    .line 586
    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    .line 588
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 589
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameLineCountView(I)V

    .line 591
    :cond_1
    return-void
.end method

.method public onPencilFramePlusClick()V
    .locals 2

    .prologue
    const/16 v1, 0xa

    .line 595
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    .line 596
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    if-le v0, v1, :cond_0

    .line 597
    iput v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    .line 599
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 600
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameLineCountView(I)V

    .line 602
    :cond_1
    return-void
.end method

.method public onReadingModeSelected(I)V
    .locals 5
    .param p1, "readingModeIndex"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 253
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    .line 254
    packed-switch p1, :pswitch_data_0

    .line 304
    :cond_0
    :goto_0
    return-void

    .line 256
    :pswitch_0
    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 258
    iput-boolean v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingPaused:Z

    .line 260
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameViewVisibility(Z)V

    .line 263
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 264
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 265
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    .line 266
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewVisibility(Z)V

    goto :goto_0

    .line 270
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameViewVisibility(Z)V

    .line 273
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 274
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    .line 275
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    .line 276
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewVisibility(Z)V

    goto :goto_0

    .line 280
    :pswitch_2
    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->fastReadingWordSelector:Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 282
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameViewVisibility(Z)V

    .line 285
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPage(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setText(Ljava/lang/CharSequence;)V

    .line 286
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewVisibility(Z)V

    .line 287
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    .line 288
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    goto/16 :goto_0

    .line 292
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameViewVisibility(Z)V

    .line 294
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameLineCountView(I)V

    .line 296
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPrimaryTextColor()V

    .line 297
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    .line 298
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    .line 299
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewVisibility(Z)V

    goto/16 :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 675
    return-void
.end method

.method public onShowReadingModeDialogClick()V
    .locals 2

    .prologue
    .line 246
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showSelectReadingModeDialog(I)V

    .line 249
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 6

    .prologue
    .line 652
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->setSpeed(I)V

    .line 653
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->preferenceUtil:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pencilFrameSelectedLineCount:I

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->setPencilSelectedLineCount(I)V

    .line 655
    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 657
    :cond_0
    iget-wide v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_1

    .line 658
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    aget-object v2, v2, v3

    iget-wide v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerRemindingTime:J

    invoke-virtual {v2, v4, v5}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->setRemainingTime(J)V

    .line 661
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    array-length v2, v2

    new-array v1, v2, [J

    .line 662
    .local v1, "remainingTimes":[J
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 663
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getRemainingTime()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 662
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 665
    :cond_2
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerMode:I

    invoke-virtual {v2, v3, v1}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->save(I[J)V

    .line 668
    .end local v0    # "i":I
    .end local v1    # "remainingTimes":[J
    :cond_3
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    iget v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    iget v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageCount:I

    invoke-static {v3, v4}, Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;->getProgress(II)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setProgress(I)V

    .line 669
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->splittedBook:Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    iget v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageIndex:I

    invoke-static {v3, v4}, Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;->getBookOffset(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setBookOffset(J)V

    .line 671
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookDescription:Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/reader/repository/IBookController;->updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 672
    return-void
.end method

.method public onTimerModeCompletedDialogClosed()V
    .locals 2

    .prologue
    .line 345
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 346
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setCourseActivityResult(Z)V

    .line 347
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->finish()V

    .line 349
    :cond_0
    return-void
.end method

.method public onTimerModeDialogClosed()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 327
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getReadingMode()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    .line 329
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    if-eqz v0, :cond_1

    .line 330
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedView(I)V

    .line 331
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    .line 336
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeModels:[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    iget v4, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->timerModeIndex:I

    aget-object v1, v1, v4

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;->getRemainingTime()J

    move-result-wide v4

    invoke-interface {v0, v4, v5}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setTimerView(J)V

    .line 338
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_2

    move v1, v2

    :goto_1
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setProgressViewVisibility(Z)V

    .line 339
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->readingMode:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    :goto_2
    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setPencilFrameViewVisibility(Z)V

    .line 341
    :cond_0
    return-void

    .line 333
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v3}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setSpeedViewVisibility(Z)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 338
    goto :goto_1

    :cond_3
    move v2, v3

    .line 339
    goto :goto_2
.end method

.method public requestToSplitBook(Landroid/text/TextPaint;II)V
    .locals 4
    .param p1, "textPaint"    # Landroid/text/TextPaint;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "pageWidth"    # I
    .param p3, "pageHeight"    # I

    .prologue
    .line 189
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->textPaint:Landroid/text/TextPaint;

    .line 190
    iput p2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->pageWidth:I

    .line 193
    new-instance v0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;

    new-instance v1, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;

    invoke-direct {v1, p1, p2, p3}, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;-><init>(Landroid/text/TextPaint;II)V

    new-instance v2, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;-><init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;-><init>(Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->bookContent:Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    aput-object v3, v1, v2

    .line 241
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 242
    return-void
.end method
