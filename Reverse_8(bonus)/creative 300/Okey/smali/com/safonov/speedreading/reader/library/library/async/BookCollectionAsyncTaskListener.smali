.class public interface abstract Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;
.super Ljava/lang/Object;
.source "BookCollectionAsyncTaskListener.java"


# virtual methods
.method public abstract onCollectionPostExecute(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onCollectionPreExecute()V
.end method
