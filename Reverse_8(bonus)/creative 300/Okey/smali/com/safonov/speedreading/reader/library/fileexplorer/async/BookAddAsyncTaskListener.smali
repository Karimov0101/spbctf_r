.class public interface abstract Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;
.super Ljava/lang/Object;
.source "BookAddAsyncTaskListener.java"


# virtual methods
.method public abstract onBookAddFailed()V
.end method

.method public abstract onBookAddPreExecute()V
.end method

.method public abstract onBookAddedSuccess(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract onBookAlreadyExist(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method
