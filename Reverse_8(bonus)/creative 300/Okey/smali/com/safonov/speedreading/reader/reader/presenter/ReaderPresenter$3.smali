.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->requestToSplitBook(Landroid/text/TextPaint;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPostBookSeparate(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)V
    .locals 7
    .param p1, "book"    # Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, p1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$502(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    .line 204
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v1

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$700(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getBookOffset()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/safonov/speedreading/reader/reader/util/BookProgressUtil;->getPageIndex(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;J)I

    move-result v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$402(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I

    .line 205
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;->getPageCount()I

    move-result v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$802(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I

    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$902(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;I)I

    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1002(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/WordSelector;)Lcom/safonov/speedreading/reader/reader/util2/WordSelector;

    .line 209
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, v6}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1102(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;)Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;

    .line 211
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1200(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1300(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->shouldShowGuideDialog()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showGuideDialog()V

    .line 227
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, v2}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1202(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Z)Z

    .line 230
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 231
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->dismissProgressDialog()V

    .line 233
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x1

    :goto_1
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->setActionBarSelectReadingModeVisibility(Z)V

    .line 234
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->hideActionBar()V

    .line 236
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$800(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->initNavigationSeekBar(I)V

    .line 237
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->seNavigationViewVisibility(Z)V

    .line 239
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1500(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V

    .line 240
    return-void

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$1400(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)I

    move-result v0

    if-nez v0, :cond_4

    .line 218
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showSelectReadingModeDialog(I)V

    goto :goto_0

    .line 222
    :cond_4
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$100(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)[Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showTimerModeDialog([Lcom/safonov/speedreading/reader/reader/model/TimerModeModel;)V

    goto/16 :goto_0

    :cond_5
    move v1, v2

    .line 233
    goto :goto_1
.end method

.method public onPreBookSeparate()V
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$3;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showProgressDialog()V

    .line 199
    :cond_0
    return-void
.end method
