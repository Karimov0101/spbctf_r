.class public Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;
.super Landroid/os/AsyncTask;
.source "BookSeparatorAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/safonov/speedreading/reader/repository/entity/BookContent;",
        "Ljava/lang/Void;",
        "Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;",
        ">;"
    }
.end annotation


# instance fields
.field private listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

.field private pageSplitter:Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;)V
    .locals 0
    .param p1, "pageSplitter"    # Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->pageSplitter:Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;

    .line 21
    iput-object p2, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    .line 22
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;
    .locals 2
    .param p1, "bookContents"    # [Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->pageSplitter:Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/reader/reader/util2/PageSplitter;->getSeparatedBook(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p1, [Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->doInBackground([Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)V
    .locals 1
    .param p1, "splittedBook"    # Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;->onPostBookSeparate(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)V

    .line 43
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->onPostExecute(Lcom/safonov/speedreading/reader/reader/util2/SplittedBook;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTask;->listener:Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/async/BookSeparatorAsyncTaskListener;->onPreBookSeparate()V

    .line 30
    :cond_0
    return-void
.end method
