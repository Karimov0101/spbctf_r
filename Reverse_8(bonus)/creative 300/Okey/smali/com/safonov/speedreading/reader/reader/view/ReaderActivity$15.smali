.class Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;
.super Ljava/lang/Object;
.source "ReaderActivity.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->onRequestToGetTextViewData()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    .prologue
    .line 817
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 820
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v2, v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingRight()I

    move-result v3

    sub-int v1, v2, v3

    .line 821
    .local v1, "pageWidth":I
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v2, v2, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaddingBottom()I

    move-result v3

    sub-int v0, v2, v3

    .line 823
    .local v0, "pageHeight":I
    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-static {v2}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->access$900(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;

    iget-object v3, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$15;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    iget-object v3, v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;->contentTextView:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-interface {v2, v3, v1, v0}, Lcom/safonov/speedreading/reader/reader/presenter/IReaderPresenter;->requestToSplitBook(Landroid/text/TextPaint;II)V

    .line 824
    return-void
.end method
