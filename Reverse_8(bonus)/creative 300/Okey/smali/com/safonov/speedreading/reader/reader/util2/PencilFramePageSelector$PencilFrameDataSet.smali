.class public Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;
.super Ljava/lang/Object;
.source "PencilFramePageSelector.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PencilFrameDataSet"
.end annotation


# instance fields
.field private frameBottomY:I

.field private frameTopY:I

.field private wordCount:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFrameBottomY()I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->frameBottomY:I

    return v0
.end method

.method public getFrameTopY()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->frameTopY:I

    return v0
.end method

.method public getWordCount()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->wordCount:I

    return v0
.end method

.method public setFrameBottomY(I)V
    .locals 0
    .param p1, "frameBottomY"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->frameBottomY:I

    .line 38
    return-void
.end method

.method public setFrameTopY(I)V
    .locals 0
    .param p1, "frameTopY"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->frameTopY:I

    .line 30
    return-void
.end method

.method public setWordCount(I)V
    .locals 0
    .param p1, "wordCount"    # I

    .prologue
    .line 45
    iput p1, p0, Lcom/safonov/speedreading/reader/reader/util2/PencilFramePageSelector$PencilFrameDataSet;->wordCount:I

    .line 46
    return-void
.end method
