.class public Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;
.super Landroid/os/AsyncTask;
.source "BookAddAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;,
        Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;",
        ">;"
    }
.end annotation


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;)V
    .locals 0
    .param p1, "bookController"    # Lcom/safonov/speedreading/reader/repository/IBookController;
    .param p2, "listener"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    .line 21
    iput-object p2, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    .line 22
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;
    .locals 5
    .param p1, "params"    # [Ljava/lang/String;

    .prologue
    .line 56
    const/4 v3, 0x0

    aget-object v2, p1, v3

    .line 58
    .local v2, "filePath":Ljava/lang/String;
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v3, v2}, Lcom/safonov/speedreading/reader/repository/IBookController;->addBook(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    .line 59
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    sget-object v4, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->OK:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-direct {v3, p0, v0, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V
    :try_end_0
    .catch Lcom/safonov/speedreading/reader/repository/exception/BookParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 68
    .end local v0    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    :goto_0
    return-object v3

    .line 60
    :catch_0
    move-exception v1

    .line 61
    .local v1, "e":Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    sget-object v4, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_PARSER_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-direct {v3, p0, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V

    goto :goto_0

    .line 62
    .end local v1    # "e":Lcom/safonov/speedreading/reader/repository/exception/BookParserException;
    :catch_1
    move-exception v1

    .line 63
    .local v1, "e":Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
    iget-object v3, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

    invoke-interface {v3, v2}, Lcom/safonov/speedreading/reader/repository/IBookController;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    .line 64
    .restart local v0    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    sget-object v4, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_ALREADY_EXIST_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-direct {v3, p0, v0, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V

    goto :goto_0

    .line 65
    .end local v0    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .end local v1    # "e":Lcom/safonov/speedreading/reader/repository/exception/BookAlreadyExistException;
    :catch_2
    move-exception v1

    .line 66
    .local v1, "e":Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    sget-object v4, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_UNSUPPORTED_FORMAT_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-direct {v3, p0, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V

    goto :goto_0

    .line 67
    .end local v1    # "e":Lcom/safonov/speedreading/reader/repository/exception/BookUnsupportedFormatException;
    :catch_3
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    sget-object v4, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_PARSER_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-direct {v3, p0, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;)V

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->doInBackground([Ljava/lang/String;)Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;)V
    .locals 2
    .param p1, "bookDescriptionWrapper"    # Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    if-eqz v0, :cond_0

    .line 76
    sget-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$1;->$SwitchMap$com$safonov$speedreading$reader$library$fileexplorer$async$BookAddAsyncTask$Status:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->getStatus()Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;->onBookAddFailed()V

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 78
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->getBookDescription()Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;->onBookAddedSuccess(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    goto :goto_0

    .line 81
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;->getBookDescription()Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;->onBookAlreadyExist(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    goto :goto_0

    .line 76
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->onPostExecute(Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$BookDescriptionWrapper;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 26
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;->listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;->onBookAddPreExecute()V

    .line 28
    return-void
.end method
