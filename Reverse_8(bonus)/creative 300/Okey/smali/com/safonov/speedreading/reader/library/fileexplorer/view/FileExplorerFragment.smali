.class public Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "FileExplorerFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;,
        Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/IFileExplorerView;"
    }
.end annotation


# instance fields
.field private adapter:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

.field private fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

.field private parentView:Landroid/view/View;

.field private progressDialog:Landroid/app/ProgressDialog;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090160
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;)Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    return-object v0
.end method

.method public static newInstance()Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public closeFileExplorer()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;->onFileExplorerClose()V

    .line 161
    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->createPresenter()Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;
    .locals 6
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v4

    .line 51
    invoke-virtual {v4}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v0

    .line 53
    .local v0, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v2, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v2, v4, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 54
    .local v2, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v1, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 55
    .local v1, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v3, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 57
    .local v3, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    new-instance v4, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;

    new-instance v5, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v5, v0, v2, v1, v3}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    invoke-direct {v4, v5, p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/FileExplorerPresenter;-><init>(Lcom/safonov/speedreading/reader/repository/IBookController;Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;)V

    return-object v4
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 263
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 264
    instance-of v0, p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    if-eqz v0, :cond_0

    .line 265
    check-cast p1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    .line 270
    return-void

    .line 267
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement FileExplorerFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;->onBackPressed()V

    .line 113
    return-void
.end method

.method public onBookAddFailed()V
    .locals 3

    .prologue
    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    const v1, 0x7f0e006c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 130
    return-void
.end method

.method public onBookAddPreExecute()V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0e006b

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 123
    return-void
.end method

.method public onBookAddedSuccess(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 3
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    const v1, 0x7f0e006d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f0e0070

    new-instance v2, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$2;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$2;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 137
    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 142
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 143
    return-void
.end method

.method public onBookAlreadyExist(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 3
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->cancel()V

    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    const v1, 0x7f0e006e

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f0e0070

    new-instance v2, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$3;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$3;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 150
    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 155
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    .line 156
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 76
    const v0, 0x7f0b0089

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->unbinder:Lbutterknife/Unbinder;

    .line 80
    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

    new-instance v1, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$1;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$1;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;)V

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;-><init>(Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapterClickListener;Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$1;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->adapter:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/DividerItemDecoration;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Landroid/support/v7/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->adapter:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 91
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->parentView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 281
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 282
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 274
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    .line 276
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 96
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/presenter/IFileExplorerPresenter;->init()V

    .line 98
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;->setActionBarTitle(Ljava/lang/String;)V

    .line 103
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "fileExplorerFileWrapperList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;>;"
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->adapter:Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;

    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment$FileExplorerAdapter;->setData(Ljava/util/List;)V

    .line 108
    return-void
.end method
