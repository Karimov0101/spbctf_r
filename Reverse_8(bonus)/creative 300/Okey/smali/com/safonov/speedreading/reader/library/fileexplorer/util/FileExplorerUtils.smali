.class public Lcom/safonov/speedreading/reader/library/fileexplorer/util/FileExplorerUtils;
.super Ljava/lang/Object;
.source "FileExplorerUtils.java"


# static fields
.field private static final EPUB:Ljava/lang/String; = "epub"

.field private static final FB2:Ljava/lang/String; = "fb2"

.field private static final FB2_ZIP:Ljava/lang/String; = "fb2.zip"

.field private static final TXT:Ljava/lang/String; = "txt"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static filterFiles([Ljava/io/File;)Ljava/util/List;
    .locals 8
    .param p0, "files"    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/io/File;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 50
    if-nez p0, :cond_1

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 84
    :cond_0
    return-object v0

    .line 54
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    array-length v3, p0

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 56
    .local v0, "acceptedFiles":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;>;"
    array-length v6, p0

    move v5, v4

    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v1, p0, v5

    .line 57
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->isHidden()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 56
    :cond_2
    :goto_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_0

    .line 61
    :cond_3
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 62
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    sget-object v7, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FOLDER:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v3, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_4
    invoke-static {v1}, Lcom/safonov/speedreading/reader/library/fileexplorer/util/FileExplorerUtils;->getFileExtension(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    .line 65
    .local v2, "fileExtension":Ljava/lang/String;
    if-eqz v2, :cond_2

    .line 66
    const/4 v3, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    :cond_5
    :goto_2
    packed-switch v3, :pswitch_data_0

    goto :goto_1

    .line 68
    :pswitch_0
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    sget-object v7, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->EPUB:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v3, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    :sswitch_0
    const-string v7, "epub"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    move v3, v4

    goto :goto_2

    :sswitch_1
    const-string v7, "txt"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v3, 0x1

    goto :goto_2

    :sswitch_2
    const-string v7, "fb2"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v3, 0x2

    goto :goto_2

    :sswitch_3
    const-string v7, "fb2.zip"

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    const/4 v3, 0x3

    goto :goto_2

    .line 71
    :pswitch_1
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    sget-object v7, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->TXT:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v3, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 74
    :pswitch_2
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    sget-object v7, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v3, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 77
    :pswitch_3
    new-instance v3, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;

    sget-object v7, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;->FB2_ZIP:Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;

    invoke-direct {v3, v1, v7}, Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileWrapper;-><init>(Ljava/io/File;Lcom/safonov/speedreading/reader/library/fileexplorer/model/FileExplorerFileType;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    nop

    :sswitch_data_0
    .sparse-switch
        -0x42321fd7 -> :sswitch_3
        0x18af6 -> :sswitch_2
        0x1c270 -> :sswitch_1
        0x2f9c78 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static getFileExtension(Ljava/io/File;)Ljava/lang/String;
    .locals 1
    .param p0, "file"    # Ljava/io/File;

    .prologue
    .line 23
    invoke-virtual {p0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/safonov/speedreading/reader/library/fileexplorer/util/FileExplorerUtils;->getFileExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFileExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/16 v6, 0x2e

    .line 27
    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 28
    .local v1, "fileName":Ljava/lang/String;
    const/4 v0, 0x0

    .line 30
    .local v0, "extension":Ljava/lang/String;
    invoke-virtual {v1, v6}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    .line 32
    .local v2, "i":I
    if-lez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v2, v4, :cond_0

    .line 33
    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    .line 35
    const-string v4, "zip"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 36
    const-string v4, "fb2.zip"

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    .line 38
    .local v3, "zipIndex":I
    const/4 v4, 0x1

    if-le v3, v4, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    const-string v5, "fb2.zip"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    if-ne v3, v4, :cond_0

    .line 39
    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_0

    .line 40
    const-string v4, "fb2.zip"

    .line 46
    .end local v3    # "zipIndex":I
    :goto_0
    return-object v4

    :cond_0
    move-object v4, v0

    goto :goto_0
.end method
