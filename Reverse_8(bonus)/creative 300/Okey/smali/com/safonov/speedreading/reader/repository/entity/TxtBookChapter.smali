.class public Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;
.super Ljava/lang/Object;
.source "TxtBookChapter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/entity/BookChapter;


# instance fields
.field private content:Ljava/lang/CharSequence;

.field private title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBookChapter()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->content:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getContent()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->content:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/CharSequence;

    .prologue
    .line 16
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->content:Ljava/lang/CharSequence;

    .line 17
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/TxtBookChapter;->title:Ljava/lang/CharSequence;

    .line 13
    return-void
.end method
