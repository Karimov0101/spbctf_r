.class public Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;
.super Ljava/lang/Object;
.source "ReaderPreferenceUtil.java"


# instance fields
.field private context:Landroid/content/Context;

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    .line 19
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 20
    return-void
.end method


# virtual methods
.method public getPencilSelectedLineCount()I
    .locals 4

    .prologue
    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0127

    .line 48
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    .line 49
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 47
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getSpeed()I
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0132

    .line 28
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    .line 29
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a000c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 27
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public setPencilSelectedLineCount(I)V
    .locals 3
    .param p1, "selectedLineCount"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0127

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 44
    return-void
.end method

.method public setShouldShowGuideDialog(Z)V
    .locals 3
    .param p1, "shouldShow"    # Z

    .prologue
    .line 33
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0131

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 34
    return-void
.end method

.method public setSpeed(I)V
    .locals 3
    .param p1, "speed"    # I

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0132

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 24
    return-void
.end method

.method public shouldShowGuideDialog()Z
    .locals 4

    .prologue
    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    const v2, 0x7f0e0131

    .line 38
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPreferenceUtil;->context:Landroid/content/Context;

    .line 39
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f050006

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v2

    .line 37
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
