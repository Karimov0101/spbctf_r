.class Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;
.super Ljava/lang/Object;
.source "LibraryPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/library/async/BookCollectionAsyncTaskListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    .prologue
    .line 39
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCollectionPostExecute(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    .local p1, "bookDescriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookDescription;>;"
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-static {v0, p1}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->access$002(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;Ljava/util/List;)Ljava/util/List;

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->setItems(Ljava/util/List;)V

    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->dismissProgressDialog()V

    .line 54
    :cond_0
    return-void
.end method

.method public onCollectionPreExecute()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$1;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->showLoadingLibraryProgressDialog()V

    .line 45
    :cond_0
    return-void
.end method
