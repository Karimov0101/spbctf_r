.class public interface abstract Lcom/safonov/speedreading/reader/library/activity/presenter/ILibraryPresenter;
.super Ljava/lang/Object;
.source "ILibraryPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract init()V
.end method

.method public abstract onActionBarHomePressed()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract requestToSetFileExplorerFragment()V
.end method

.method public abstract requestToSetLibraryFragment()V
.end method
