.class Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$1;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "ReaderActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;Landroid/support/v4/app/FragmentManager;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;
    .param p2, "x0"    # Landroid/support/v4/app/FragmentManager;

    .prologue
    .line 649
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment$1;->this$0:Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$GuideDialogFragment;

    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 665
    const/4 v0, 0x4

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 653
    packed-switch p1, :pswitch_data_0

    .line 659
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 654
    :pswitch_0
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$ReadingGuideFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$ReadingGuideFragment;-><init>()V

    goto :goto_0

    .line 655
    :pswitch_1
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FastReadingGuideFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FastReadingGuideFragment;-><init>()V

    goto :goto_0

    .line 656
    :pswitch_2
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FlashReadingGuideFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$FlashReadingGuideFragment;-><init>()V

    goto :goto_0

    .line 657
    :pswitch_3
    new-instance v0, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$PencilFrameReadingGuideFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity$PencilFrameReadingGuideFragment;-><init>()V

    goto :goto_0

    .line 653
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
