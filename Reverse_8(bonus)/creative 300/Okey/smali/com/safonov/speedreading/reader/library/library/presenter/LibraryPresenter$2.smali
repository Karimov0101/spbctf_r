.class Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;
.super Ljava/lang/Object;
.source "LibraryPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/library/async/BookDeleteAsyncTaskListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->requestToRemoveBook()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public oBookDeletePostExecute()V
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->access$100(Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;)J

    move-result-wide v2

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->removeItem(J)V

    .line 90
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->dismissProgressDialog()V

    .line 92
    :cond_0
    return-void
.end method

.method public onBookDeletePreExecute()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter$2;->this$0:Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;->showRemovingBookProgressDialog()V

    .line 84
    :cond_0
    return-void
.end method
