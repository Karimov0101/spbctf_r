.class final enum Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;
.super Ljava/lang/Enum;
.source "BookAddAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "Status"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

.field public static final enum BOOK_ALREADY_EXIST_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

.field public static final enum BOOK_PARSER_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

.field public static final enum BOOK_UNSUPPORTED_FORMAT_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

.field public static final enum OK:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->OK:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    const-string v1, "BOOK_PARSER_EXCEPTION"

    invoke-direct {v0, v1, v3}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_PARSER_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    const-string v1, "BOOK_ALREADY_EXIST_EXCEPTION"

    invoke-direct {v0, v1, v4}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_ALREADY_EXIST_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    new-instance v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    const-string v1, "BOOK_UNSUPPORTED_FORMAT_EXCEPTION"

    invoke-direct {v0, v1, v5}, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_UNSUPPORTED_FORMAT_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->OK:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    aput-object v1, v0, v2

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_PARSER_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    aput-object v1, v0, v3

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_ALREADY_EXIST_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    aput-object v1, v0, v4

    sget-object v1, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->BOOK_UNSUPPORTED_FORMAT_EXCEPTION:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    aput-object v1, v0, v5

    sput-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->$VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 30
    const-class v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->$VALUES:[Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTask$Status;

    return-object v0
.end method
