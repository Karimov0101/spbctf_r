.class Lcom/safonov/speedreading/reader/library/library/view/LibraryCoverColorHelper;
.super Ljava/lang/Object;
.source "LibraryCoverColorHelper.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static getBookCoverColor(C)I
    .locals 2
    .param p0, "firstLetter"    # C

    .prologue
    .line 8
    invoke-static {p0}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v0

    .line 10
    .local v0, "c":C
    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    .line 11
    const v1, -0x1a8c8d

    .line 52
    :goto_0
    return v1

    .line 13
    :cond_0
    const/16 v1, 0x41

    if-eq v0, v1, :cond_1

    const/16 v1, 0x42

    if-eq v0, v1, :cond_1

    const/16 v1, 0x410

    if-eq v0, v1, :cond_1

    const/16 v1, 0x411

    if-eq v0, v1, :cond_1

    const/16 v1, 0x412

    if-ne v0, v1, :cond_2

    .line 14
    :cond_1
    const v1, -0xf9d6e

    goto :goto_0

    .line 16
    :cond_2
    const/16 v1, 0x43

    if-eq v0, v1, :cond_3

    const/16 v1, 0x44

    if-eq v0, v1, :cond_3

    const/16 v1, 0x413

    if-eq v0, v1, :cond_3

    const/16 v1, 0x414

    if-eq v0, v1, :cond_3

    const/16 v1, 0x415

    if-ne v0, v1, :cond_4

    .line 17
    :cond_3
    const v1, -0x459738

    goto :goto_0

    .line 19
    :cond_4
    const/16 v1, 0x45

    if-eq v0, v1, :cond_5

    const/16 v1, 0x46

    if-eq v0, v1, :cond_5

    const/16 v1, 0x401

    if-eq v0, v1, :cond_5

    const/16 v1, 0x416

    if-eq v0, v1, :cond_5

    const/16 v1, 0x417

    if-ne v0, v1, :cond_6

    .line 20
    :cond_5
    const v1, -0x6a8a33

    goto :goto_0

    .line 22
    :cond_6
    const/16 v1, 0x47

    if-eq v0, v1, :cond_7

    const/16 v1, 0x48

    if-eq v0, v1, :cond_7

    const/16 v1, 0x418

    if-eq v0, v1, :cond_7

    const/16 v1, 0x419

    if-eq v0, v1, :cond_7

    const/16 v1, 0x41a

    if-ne v0, v1, :cond_8

    .line 23
    :cond_7
    const v1, -0x867935

    goto :goto_0

    .line 25
    :cond_8
    const/16 v1, 0x49

    if-eq v0, v1, :cond_9

    const/16 v1, 0x4a

    if-eq v0, v1, :cond_9

    const/16 v1, 0x41b

    if-eq v0, v1, :cond_9

    const/16 v1, 0x41c

    if-eq v0, v1, :cond_9

    const/16 v1, 0x41d

    if-ne v0, v1, :cond_a

    .line 26
    :cond_9
    const v1, -0x9b4a0a

    goto :goto_0

    .line 28
    :cond_a
    const/16 v1, 0x4b

    if-eq v0, v1, :cond_b

    const/16 v1, 0x4c

    if-eq v0, v1, :cond_b

    const/16 v1, 0x41e

    if-eq v0, v1, :cond_b

    const/16 v1, 0x41f

    if-eq v0, v1, :cond_b

    const/16 v1, 0x420

    if-ne v0, v1, :cond_c

    .line 29
    :cond_b
    const v1, -0xb03c09

    goto/16 :goto_0

    .line 31
    :cond_c
    const/16 v1, 0x4d

    if-eq v0, v1, :cond_d

    const/16 v1, 0x4e

    if-eq v0, v1, :cond_d

    const/16 v1, 0x421

    if-eq v0, v1, :cond_d

    const/16 v1, 0x422

    if-eq v0, v1, :cond_d

    const/16 v1, 0x423

    if-ne v0, v1, :cond_e

    .line 32
    :cond_d
    const v1, -0xb22f1f

    goto/16 :goto_0

    .line 34
    :cond_e
    const/16 v1, 0x4f

    if-eq v0, v1, :cond_f

    const/16 v1, 0x50

    if-eq v0, v1, :cond_f

    const/16 v1, 0x424

    if-eq v0, v1, :cond_f

    const/16 v1, 0x425

    if-eq v0, v1, :cond_f

    const/16 v1, 0x426

    if-ne v0, v1, :cond_10

    .line 35
    :cond_f
    const v1, -0xb24954

    goto/16 :goto_0

    .line 37
    :cond_10
    const/16 v1, 0x51

    if-eq v0, v1, :cond_11

    const/16 v1, 0x52

    if-eq v0, v1, :cond_11

    const/16 v1, 0x427

    if-eq v0, v1, :cond_11

    const/16 v1, 0x428

    if-eq v0, v1, :cond_11

    const/16 v1, 0x429

    if-ne v0, v1, :cond_12

    .line 38
    :cond_11
    const v1, -0x7e387c

    goto/16 :goto_0

    .line 40
    :cond_12
    const/16 v1, 0x53

    if-eq v0, v1, :cond_13

    const/16 v1, 0x54

    if-eq v0, v1, :cond_13

    const/16 v1, 0x42a

    if-eq v0, v1, :cond_13

    const/16 v1, 0x42b

    if-eq v0, v1, :cond_13

    const/16 v1, 0x42c

    if-ne v0, v1, :cond_14

    .line 41
    :cond_13
    const v1, -0x512a7f

    goto/16 :goto_0

    .line 43
    :cond_14
    const/16 v1, 0x55

    if-eq v0, v1, :cond_15

    const/16 v1, 0x56

    if-eq v0, v1, :cond_15

    const/16 v1, 0x42d

    if-eq v0, v1, :cond_15

    const/16 v1, 0x42e

    if-eq v0, v1, :cond_15

    const/16 v1, 0x42f

    if-ne v0, v1, :cond_16

    .line 44
    :cond_15
    const/16 v1, -0x759b

    goto/16 :goto_0

    .line 46
    :cond_16
    const/16 v1, 0x57

    if-eq v0, v1, :cond_17

    const/16 v1, 0x58

    if-ne v0, v1, :cond_18

    .line 47
    :cond_17
    const v1, -0x2b1ea9

    goto/16 :goto_0

    .line 49
    :cond_18
    const/16 v1, 0x59

    if-eq v0, v1, :cond_19

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_1a

    .line 50
    :cond_19
    const/16 v1, -0x2ab1

    goto/16 :goto_0

    .line 52
    :cond_1a
    const/16 v1, -0x48b3

    goto/16 :goto_0
.end method
