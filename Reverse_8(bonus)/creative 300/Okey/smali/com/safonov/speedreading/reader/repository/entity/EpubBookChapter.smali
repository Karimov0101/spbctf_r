.class public Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;
.super Ljava/lang/Object;
.source "EpubBookChapter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/entity/BookChapter;


# instance fields
.field private content:Ljava/lang/CharSequence;

.field private title:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;
    .param p2, "content"    # Ljava/lang/CharSequence;

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->title:Ljava/lang/CharSequence;

    .line 15
    iput-object p2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    .line 16
    return-void
.end method


# virtual methods
.method public getBookChapter()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 38
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    if-eqz v2, :cond_2

    .line 39
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 40
    .local v0, "contentLength":I
    add-int/lit8 v1, v0, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 41
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    invoke-interface {v2, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0xa

    if-eq v2, v3, :cond_0

    .line 42
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    const/4 v3, 0x0

    add-int/lit8 v4, v1, 0x1

    invoke-interface {v2, v3, v4}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 48
    .end local v0    # "contentLength":I
    .end local v1    # "i":I
    :goto_1
    return-object v2

    .line 40
    .restart local v0    # "contentLength":I
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 46
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    goto :goto_1

    .line 48
    .end local v0    # "contentLength":I
    .end local v1    # "i":I
    :cond_2
    const-string v2, ""

    goto :goto_1
.end method

.method public getContent()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->title:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public setContent(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "content"    # Ljava/lang/CharSequence;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->content:Ljava/lang/CharSequence;

    .line 34
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/CharSequence;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/entity/EpubBookChapter;->title:Ljava/lang/CharSequence;

    .line 25
    return-void
.end method
