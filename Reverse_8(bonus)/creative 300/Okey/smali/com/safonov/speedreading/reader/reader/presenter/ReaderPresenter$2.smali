.class Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;
.super Ljava/lang/Object;
.source "ReaderPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/reader/async/BookContentLoaderAsyncTaskListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->init(IJ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBookContentLoaderExecuteError()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->dismissProgressDialog()V

    .line 172
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showErrorMessage()V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->finish()V

    .line 175
    :cond_0
    return-void
.end method

.method public onBookContentLoaderExecuteSuccess(Lcom/safonov/speedreading/reader/repository/entity/BookContent;)V
    .locals 1
    .param p1, "content"    # Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-static {v0, p1}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->access$602(Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;Lcom/safonov/speedreading/reader/repository/entity/BookContent;)Lcom/safonov/speedreading/reader/repository/entity/BookContent;

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->dismissProgressDialog()V

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->onRequestToGetTextViewData()V

    .line 166
    :cond_0
    return-void
.end method

.method public onBookContentLoaderPreExecute()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter$2;->this$0:Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/reader/presenter/ReaderPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/reader/reader/view/IReaderView;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/reader/view/IReaderView;->showProgressDialog()V

    .line 157
    :cond_0
    return-void
.end method
