.class public Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "LibraryFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;",
        "Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;",
        ">;",
        "Lcom/safonov/speedreading/reader/library/library/view/ILibraryView;"
    }
.end annotation


# static fields
.field public static final BOOK_ID:Ljava/lang/String; = "book_id"

.field private static final READ_STORAGE_PERMISSION_REQUEST_CODE:I = 0xa

.field public static final SHOULD_LOAD_BOOK:Ljava/lang/String; = "should_load_book"


# instance fields
.field private adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

.field emptyView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090084
    .end annotation
.end field

.field filePath:Ljava/lang/String;

.field private formatDialog:Landroid/support/v7/app/AlertDialog;

.field private fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

.field private interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

.field private isEmpty:Z

.field private parentView:Landroid/view/View;

.field path:Ljava/lang/String;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private progressDialog:Landroid/app/ProgressDialog;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090160
    .end annotation
.end field

.field sharedPreferences:Landroid/content/SharedPreferences;

.field speedReadingBookId:J

.field speedReadingBookProgress:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b6
    .end annotation
.end field

.field speedReadingBookProgressTv:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b7
    .end annotation
.end field

.field speedReadingBookView:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d8
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 101
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 152
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->isEmpty:Z

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Landroid/support/v7/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->formatDialog:Landroid/support/v7/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    return-object v0
.end method

.method private addSpeedReadingBook(Ljava/lang/String;)V
    .locals 12
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 320
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v4/app/FragmentActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v8

    iget-object v8, v8, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "books"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->path:Ljava/lang/String;

    .line 322
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    invoke-static {v7}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v7

    .line 323
    invoke-virtual {v7}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v0

    .line 325
    .local v0, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    invoke-interface {v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v2

    .line 326
    .local v2, "bookId":J
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->path:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "fb2"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    .line 328
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    invoke-interface {v0, v7}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v6

    .line 330
    .local v6, "speedReading":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    new-instance v6, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .end local v6    # "speedReading":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-direct {v6}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 331
    .restart local v6    # "speedReading":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v6, v2, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 333
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    const-string v8, "book_id"

    invoke-interface {v7, v8, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 334
    iput-wide v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    .line 336
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 337
    const-string v7, "fb2"

    invoke-virtual {v6, v7}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 339
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e018b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 340
    .local v4, "bookTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e018a

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 342
    .local v1, "bookLanguage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0185

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 344
    invoke-virtual {v6, v4}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 345
    invoke-virtual {v6, v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 347
    invoke-interface {v0, v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 349
    new-instance v5, Ljava/io/File;

    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    invoke-direct {v5, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 351
    .local v5, "file":Ljava/io/File;
    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v7

    if-nez v7, :cond_0

    .line 352
    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 353
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    .line 354
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->copyFile(Ljava/lang/String;)V

    .line 357
    :cond_0
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgress:Landroid/widget/ProgressBar;

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 358
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgress:Landroid/widget/ProgressBar;

    invoke-virtual {v6}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 359
    iget-object v7, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgressTv:Landroid/widget/TextView;

    const v8, 0x7f0e009a

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 360
    return-void
.end method

.method private copyFile(Ljava/lang/String;)V
    .locals 9
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 363
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/FragmentActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 365
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 366
    .local v3, "in":Ljava/io/InputStream;
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->filePath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 367
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v6, 0x400

    new-array v1, v6, [B

    .line 368
    .local v1, "buffer":[B
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 369
    .local v5, "read":I
    :goto_0
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 370
    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 371
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    goto :goto_0

    .line 373
    :cond_0
    iget-object v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "should_load_book"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    .end local v1    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "out":Ljava/io/OutputStream;
    .end local v5    # "read":I
    :goto_1
    return-void

    .line 374
    :catch_0
    move-exception v2

    .line 375
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    goto :goto_1
.end method

.method private isStoragePermissionGranted()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 435
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_0

    .line 436
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "android.permission.READ_EXTERNAL_STORAGE"

    invoke-static {v2, v3}, Landroid/support/v4/content/ContextCompat;->checkSelfPermission(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    .line 438
    .local v0, "result":I
    if-nez v0, :cond_1

    .line 440
    .end local v0    # "result":I
    :cond_0
    :goto_0
    return v1

    .line 438
    .restart local v0    # "result":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static newInstance()Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;-><init>()V

    return-object v0
.end method

.method private requestPermission()V
    .locals 3

    .prologue
    .line 445
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_EXTERNAL_STORAGE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->requestPermissions([Ljava/lang/String;I)V

    .line 446
    return-void
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->createPresenter()Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v5

    invoke-virtual {v5}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v1

    .line 87
    .local v1, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v3, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 88
    .local v3, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v2, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v2, v5, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 89
    .local v2, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 91
    .local v4, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    new-instance v0, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v0, v1, v3, v2, v4}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    .line 93
    .local v0, "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    new-instance v5, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v0}, Lcom/safonov/speedreading/reader/library/library/presenter/LibraryPresenter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/IBookController;)V

    return-object v5
.end method

.method public dismissProgressDialog()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 293
    :cond_0
    return-void
.end method

.method public onAddBookClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090096
        }
    .end annotation

    .prologue
    .line 193
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->isStoragePermissionGranted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;->requestToAddBook()V

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    invoke-direct {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->requestPermission()V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 480
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 481
    instance-of v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    if-eqz v0, :cond_0

    .line 482
    check-cast p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 487
    return-void

    .line 484
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement LibraryFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 119
    const v0, 0x7f0b0093

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->parentView:Landroid/view/View;

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->parentView:Landroid/view/View;

    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->unbinder:Lbutterknife/Unbinder;

    .line 123
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "load_book"

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "book_id"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    .line 126
    new-instance v0, Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    const v1, 0x7f0e0091

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdUnitId(Ljava/lang/String;)V

    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 132
    :cond_0
    new-instance v0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$1;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)V

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter$ItemClickListener;Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$1;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    .line 144
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Landroid/support/v7/widget/GridLayoutManager;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v5}, Landroid/support/v7/widget/GridLayoutManager;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/safonov/speedreading/GridSpacingItemDecoration;

    const/high16 v2, 0x41400000    # 12.0f

    .line 146
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    .line 145
    invoke-static {v4, v2, v3}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v1, v5, v2, v4}, Lcom/safonov/speedreading/GridSpacingItemDecoration;-><init>(IIZ)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->parentView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 472
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 473
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 474
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 491
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 492
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 493
    return-void
.end method

.method public onRequestPermissionsResult(I[Ljava/lang/String;[I)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "permissions"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "grantResults"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 450
    packed-switch p1, :pswitch_data_0

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 452
    :pswitch_0
    array-length v0, p3

    if-lez v0, :cond_0

    .line 453
    aget v0, p3, v2

    if-eqz v0, :cond_0

    .line 457
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->parentView:Landroid/view/View;

    const v1, 0x7f0e00a4

    invoke-static {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->make(Landroid/view/View;II)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    const v1, 0x7f0e00a5

    new-instance v2, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$6;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/design/widget/Snackbar;->setAction(ILandroid/view/View$OnClickListener;)Landroid/support/design/widget/Snackbar;

    move-result-object v0

    .line 463
    invoke-virtual {v0}, Landroid/support/design/widget/Snackbar;->show()V

    goto :goto_0

    .line 450
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
    .end packed-switch
.end method

.method public onSpeedReadingBookClick()V
    .locals 10
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900d8
        }
    .end annotation

    .prologue
    .line 382
    iget-wide v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 383
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v6

    .line 384
    invoke-virtual {v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v2

    .line 386
    .local v2, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 387
    .local v4, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v3, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 388
    .local v3, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v5, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 390
    .local v5, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    new-instance v0, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v0, v2, v4, v3, v5}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    .line 391
    .local v0, "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    iget-wide v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    invoke-virtual {v0, v6, v7}, Lcom/safonov/speedreading/reader/repository/BookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 393
    .local v1, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    if-eqz v1, :cond_1

    .line 394
    iget-object v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 405
    .end local v0    # "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .end local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    .end local v3    # "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    .end local v4    # "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    .end local v5    # "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    :cond_0
    :goto_0
    return-void

    .line 397
    .restart local v0    # "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    .restart local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .restart local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    .restart local v3    # "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    .restart local v4    # "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    .restart local v5    # "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    :cond_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    const-string v7, "ru"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 398
    const-string v6, "SpeedReading.json"

    invoke-direct {p0, v6}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->addSpeedReadingBook(Ljava/lang/String;)V

    .line 401
    :goto_1
    iget-wide v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    invoke-virtual {v0, v6, v7}, Lcom/safonov/speedreading/reader/repository/BookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 402
    iget-object v6, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    goto :goto_0

    .line 400
    :cond_2
    const-string v6, "SpeedReading_en.json"

    invoke-direct {p0, v6}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->addSpeedReadingBook(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 156
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;->init()V

    .line 158
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;

    iget-wide v2, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/reader/library/library/presenter/ILibraryPresenter;->setSpeedReadingBookInfo(J)V

    .line 160
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->isEmpty:Z

    .line 165
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookView:Landroid/support/constraint/ConstraintLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public removeItem(J)V
    .locals 3
    .param p1, "bookId"    # J

    .prologue
    .line 182
    iget-wide v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookId:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {v0, p1, p2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->removeItem(J)V

    .line 185
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->getItemCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 187
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->emptyView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 189
    :cond_1
    return-void
.end method

.method public requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 2
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 414
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 429
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 418
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->show()V

    .line 419
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$5;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$5;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    goto :goto_0

    .line 426
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    goto :goto_0
.end method

.method public requestToSetFileExplorerFragment()V
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->fragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryAddBookClick()V

    .line 410
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "bookDescriptionList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookDescription;>;"
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 169
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->setItems(Ljava/util/List;)V

    .line 171
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->adapter:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$LibraryAdapter;->getItemCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->isEmpty:Z

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->emptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 176
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->emptyView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setSpeedReadingBookInfo(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 5
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 306
    if-eqz p1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgress:Landroid/widget/ProgressBar;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 308
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgress:Landroid/widget/ProgressBar;

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 309
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->speedReadingBookProgressTv:Landroid/widget/TextView;

    const v1, 0x7f0e009a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 317
    :goto_0
    return-void

    .line 312
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 313
    const-string v0, "SpeedReading.json"

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->addSpeedReadingBook(Ljava/lang/String;)V

    goto :goto_0

    .line 315
    :cond_1
    const-string v0, "SpeedReading_en.json"

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->addSpeedReadingBook(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showActionDialog(Ljava/lang/String;)V
    .locals 4
    .param p1, "bookTitle"    # Ljava/lang/String;

    .prologue
    .line 233
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 234
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f03000c

    new-instance v3, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$4;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setItems(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 266
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 267
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 268
    return-void
.end method

.method public showFormatDialog()V
    .locals 7

    .prologue
    .line 204
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 206
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0b0091

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 208
    .local v3, "dialogView":Landroid/view/View;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 209
    invoke-virtual {v0, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 211
    const v4, 0x7f090069

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    .line 212
    .local v2, "continueButton":Landroid/widget/Button;
    new-instance v4, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$2;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$2;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;)V

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    const v4, 0x7f09007f

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 220
    .local v1, "checkBox":Landroid/widget/CheckBox;
    new-instance v4, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$3;

    invoke-direct {v4, p0, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment$3;-><init>(Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 227
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->formatDialog:Landroid/support/v7/app/AlertDialog;

    .line 228
    iget-object v4, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->formatDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v4}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 229
    return-void
.end method

.method public showLoadingLibraryProgressDialog()V
    .locals 2

    .prologue
    .line 275
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 276
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0e009e

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 277
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 278
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 279
    return-void
.end method

.method public showRemovingBookProgressDialog()V
    .locals 2

    .prologue
    .line 283
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 284
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    const v1, 0x7f0e0099

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 285
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 286
    iget-object v0, p0, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 287
    return-void
.end method
