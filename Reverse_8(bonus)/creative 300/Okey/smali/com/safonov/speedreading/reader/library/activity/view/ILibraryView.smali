.class public interface abstract Lcom/safonov/speedreading/reader/library/activity/view/ILibraryView;
.super Ljava/lang/Object;
.source "ILibraryView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;
.implements Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;


# virtual methods
.method public abstract finish()V
.end method

.method public abstract onFileExplorerBackPressed()V
.end method

.method public abstract setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract setFileExplorerFragment()V
.end method

.method public abstract setLibraryFragment()V
.end method
