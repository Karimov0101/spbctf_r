.class Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;
.super Ljava/lang/Object;
.source "SqlLiteBookDescriptionDao.java"

# interfaces
.implements Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;


# instance fields
.field private database:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "database"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    .line 24
    return-void
.end method

.method private getBookDescription(Landroid/database/Cursor;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 6
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x1

    .line 114
    new-instance v0, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    invoke-direct {v0}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 116
    .local v0, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v0, v4, v5}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 118
    const-string v3, "title"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 119
    const-string v3, "author"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 120
    const-string v3, "language"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 121
    const-string v3, "cover_name"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setCoverImagePath(Ljava/lang/String;)V

    .line 123
    const-string v3, "file_path"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 124
    const-string v3, "type"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 125
    const-string v3, "progress"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setProgress(I)V

    .line 126
    const-string v3, "book_offset"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setBookOffset(J)V

    .line 128
    const-string v3, "favorite"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 129
    .local v1, "favorite":I
    if-ne v1, v2, :cond_0

    .line 131
    .local v2, "favoriteFlag":Z
    :goto_0
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFavorite(Z)V

    .line 133
    return-object v0

    .line 129
    .end local v2    # "favoriteFlag":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getValues(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Landroid/content/ContentValues;
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 27
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 28
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    const-string v1, "author"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getAuthor()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-string v1, "language"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 31
    const-string v1, "cover_name"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getCoverImagePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    const-string v1, "file_path"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v1, "type"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 34
    const-string v1, "favorite"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->isFavorite()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 35
    const-string v1, "progress"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getProgress()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 36
    const-string v1, "book_offset"

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getBookOffset()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 37
    return-object v0
.end method


# virtual methods
.method public addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J
    .locals 4
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "books"

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->getValues(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    return-wide v0
.end method

.method public findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 75
    iget-object v2, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT * FROM books WHERE _id=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 76
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 78
    .local v1, "result":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 79
    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->getBookDescription(Landroid/database/Cursor;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 80
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 83
    :cond_0
    return-object v1
.end method

.method public findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .locals 6
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 60
    const-string v3, "\'"

    const-string v4, "\'\'"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 62
    .local v2, "sqlFilePath":Ljava/lang/String;
    iget-object v3, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT * FROM books WHERE file_path=\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    .local v0, "cursor":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 65
    .local v1, "result":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 66
    invoke-direct {p0, v0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->getBookDescription(Landroid/database/Cursor;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 67
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 70
    :cond_0
    return-object v1
.end method

.method public getBookDescriptionList()Ljava/util/List;
    .locals 10
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/reader/repository/entity/BookDescription;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "books"

    sget-object v2, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDatabaseHelper;->BOOK_TABLE_COLUMNS:[Ljava/lang/String;

    const-string v7, "title"

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 101
    .local v9, "cursor":Landroid/database/Cursor;
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    .local v8, "bookDescriptions":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/reader/repository/entity/BookDescription;>;"
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    invoke-direct {p0, v9}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->getBookDescription(Landroid/database/Cursor;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    :cond_1
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 110
    return-object v8
.end method

.method public getNextItemId()J
    .locals 6

    .prologue
    .line 47
    iget-object v1, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "SELECT * FROM SQLITE_SEQUENCE WHERE name=\'books\'"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 48
    .local v0, "cursor":Landroid/database/Cursor;
    const-wide/16 v2, 0x0

    .line 50
    .local v2, "result":J
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 51
    const-string v1, "seq"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 52
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 55
    :cond_0
    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    return-wide v4
.end method

.method public removeBookDescription(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "books"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 94
    return-void
.end method

.method public updateBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 6
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->database:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "books"

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SqlLiteBookDescriptionDao;->getValues(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Landroid/content/ContentValues;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id=\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getId()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 89
    return-void
.end method
