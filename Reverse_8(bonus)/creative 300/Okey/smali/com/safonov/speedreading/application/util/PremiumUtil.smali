.class public Lcom/safonov/speedreading/application/util/PremiumUtil;
.super Ljava/lang/Object;
.source "PremiumUtil.java"


# static fields
.field private static final PREMIUM_USER_KEY:Ljava/lang/String; = "premium_user"

.field public static final PREMIUM_USER_SKU:Ljava/lang/String; = "com.speedreading.alexander.speedreading.premiumsub"

.field public static final PREMIUM_USER_SKU_HALF_YEAR:Ljava/lang/String; = "com.speedreading.alexander.speedreading.premium.subscription.halfyear"

.field public static final PREMIUM_USER_SKU_MONTH:Ljava/lang/String; = "com.speedreading.alexander.speedreading.premium.subscription.month"

.field public static final PREMIUM_USER_SKU_UNSUB:Ljava/lang/String; = "com.speedreading.alexander.speedreading.premium"

.field public static final PREMIUM_USER_SKU_YEAR:Ljava/lang/String; = "com.speedreading.alexander.speedreading.premium.subscription.year"


# instance fields
.field private premiumUser:Z

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "premium_user"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->premiumUser:Z

    .line 35
    return-void
.end method


# virtual methods
.method public isPremiumUser()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->premiumUser:Z

    return v0
.end method

.method public setPremiumUser(Z)V
    .locals 2
    .param p1, "premiumUser"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->premiumUser:Z

    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/PremiumUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "premium_user"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 27
    return-void
.end method
