.class public Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;
.super Ljava/lang/Object;
.source "AdvertisementDialog.java"


# static fields
.field private static final ADVERTISEMENT_DIALOG_SHARED_PREFERENCES:Ljava/lang/String; = "advertisement_russian_dialog"

.field private static final DIALOG_LAUNCH_TIMES:Ljava/lang/String; = "launch_times"

.field private static final NEEDED_LAUNCH_TIMES:I = 0x4

.field private static final SHOULD_SHOW_KEY:Ljava/lang/String; = "should_show"

.field private static final appId:Ljava/lang/String; = "com.greenkeycompany.exam"


# instance fields
.field private final context:Landroid/content/Context;

.field private dialog:Landroid/support/v7/app/AlertDialog;

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    .line 55
    const-string v0, "advertisement_russian_dialog"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)Landroid/support/v7/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method private getCurrentLocale()Ljava/util/Locale;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .prologue
    .line 60
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x18

    if-lt v0, v1, :cond_0

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLocales()Landroid/os/LocaleList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/LocaleList;->get(I)Ljava/util/Locale;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    goto :goto_0
.end method

.method private isRussianLocale()Z
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->getCurrentLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ua"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 68
    :goto_0
    return v0

    .line 69
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isShouldShow()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    iget-object v3, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "should_show"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v1

    .line 41
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v4, "launch_times"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 42
    .local v0, "dialogLaunchCount":I
    const/4 v3, 0x4

    if-ge v0, v3, :cond_2

    .line 43
    iget-object v2, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "launch_times"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 47
    :cond_2
    invoke-direct {p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->isRussianLocale()Z

    move-result v3

    if-eqz v3, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public show()V
    .locals 7

    .prologue
    .line 78
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v4, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    iget-object v4, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->context:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0b001e

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 82
    .local v2, "rateDialog":Landroid/view/View;
    const v4, 0x7f0901a3

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 83
    .local v3, "showView":Landroid/widget/Button;
    new-instance v4, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$1;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$1;-><init>(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)V

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v4, 0x7f090054

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 96
    .local v1, "closeButton":Landroid/widget/Button;
    new-instance v4, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$2;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$2;-><init>(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)V

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    new-instance v4, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$3;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog$3;-><init>(Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;)V

    invoke-virtual {v0, v4}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 110
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 112
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v4

    iput-object v4, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    .line 113
    iget-object v4, p0, Lcom/safonov/speedreading/application/ratedialog/AdvertisementDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v4}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 114
    return-void
.end method
