.class public Lcom/safonov/speedreading/application/util/BookUtil;
.super Ljava/lang/Object;
.source "BookUtil.java"


# static fields
.field private static final BOOK_KEY:Ljava/lang/String; = "speed_reading_book"

.field public static final BOOK_SKU:Ljava/lang/String; = "com.speedreading.alexander.speedreading.book"


# instance fields
.field private bookPurchased:Z

.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/BookUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/BookUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "speed_reading_book"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/safonov/speedreading/application/util/BookUtil;->bookPurchased:Z

    .line 29
    return-void
.end method


# virtual methods
.method public isBookPurchased()Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/safonov/speedreading/application/util/BookUtil;->bookPurchased:Z

    return v0
.end method

.method public setBookPurchased(Z)V
    .locals 2
    .param p1, "bookPurchased"    # Z

    .prologue
    .line 19
    iput-boolean p1, p0, Lcom/safonov/speedreading/application/util/BookUtil;->bookPurchased:Z

    .line 20
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/BookUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "speed_reading_book"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 21
    return-void
.end method
