.class public Lcom/safonov/speedreading/application/App;
.super Landroid/app/Application;
.source "App.java"


# static fields
.field private static instance:Lcom/safonov/speedreading/application/App;


# instance fields
.field private final billing:Lorg/solovyev/android/checkout/Billing;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

.field private concentrationConfiguration:Lio/realm/RealmConfiguration;

.field private cupTimerConfiguration:Lio/realm/RealmConfiguration;

.field private evenNumbersConfiguration:Lio/realm/RealmConfiguration;

.field private flashWordsConfiguration:Lio/realm/RealmConfiguration;

.field private greenDotConfiguration:Lio/realm/RealmConfiguration;

.field private lineOfSightConfiguration:Lio/realm/RealmConfiguration;

.field private mathConfiguration:Lio/realm/RealmConfiguration;

.field private passCourseConfiguration:Lio/realm/RealmConfiguration;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private rememberNumberConfiguration:Lio/realm/RealmConfiguration;

.field private rememberWordsConfiguration:Lio/realm/RealmConfiguration;

.field private schulteTableConfiguration:Lio/realm/RealmConfiguration;

.field private speedReadingConfiguration:Lio/realm/RealmConfiguration;

.field private trueColorsConfiguration:Lio/realm/RealmConfiguration;

.field private wordBlockConfiguration:Lio/realm/RealmConfiguration;

.field private wordPairsConfiguration:Lio/realm/RealmConfiguration;

.field private wordsColumnsConfiguration:Lio/realm/RealmConfiguration;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Application;-><init>()V

    .line 70
    new-instance v0, Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lcom/safonov/speedreading/application/App$1;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/application/App$1;-><init>(Lcom/safonov/speedreading/application/App;)V

    invoke-direct {v0, p0, v1}, Lorg/solovyev/android/checkout/Billing;-><init>(Landroid/content/Context;Lorg/solovyev/android/checkout/Billing$Configuration;)V

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->billing:Lorg/solovyev/android/checkout/Billing;

    return-void
.end method

.method public static get()Lcom/safonov/speedreading/application/App;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/safonov/speedreading/application/App;->instance:Lcom/safonov/speedreading/application/App;

    return-object v0
.end method


# virtual methods
.method public getBilling()Lorg/solovyev/android/checkout/Billing;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/application/App;->billing:Lorg/solovyev/android/checkout/Billing;

    return-object v0
.end method

.method public getBookUtil()Lcom/safonov/speedreading/application/util/BookUtil;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/safonov/speedreading/application/App;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    return-object v0
.end method

.method public getConcentrationRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 465
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 480
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 466
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 467
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 468
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 470
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 471
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 478
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 472
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 473
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 474
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 476
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 477
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getCupTimerRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 487
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 502
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 488
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 489
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 490
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 492
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 493
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 500
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 494
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 495
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 496
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 498
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 499
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getEvenNumbersRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 399
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 414
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 400
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 401
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 402
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 404
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 405
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 412
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 406
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 407
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 408
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 410
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 411
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getFlashWordsRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 267
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 282
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 268
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 269
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 270
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 272
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 273
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 280
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 274
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 275
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 276
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 278
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 279
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getGreenDotRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 421
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 436
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 422
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 423
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 424
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 426
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 427
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 434
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 428
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 429
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 430
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 432
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 433
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getLineOfSightRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 333
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 348
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 334
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 335
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 336
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 338
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 339
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 346
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 340
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 341
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 342
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 344
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 345
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getMathRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 443
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 458
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 444
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 445
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 446
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 448
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 449
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 456
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 450
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 451
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 452
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 454
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 455
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getPassCourseConfiguration()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 201
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 216
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 202
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 203
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 204
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 206
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 207
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 214
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 208
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 209
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 210
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 212
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 213
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/safonov/speedreading/application/App;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-object v0
.end method

.method public getRememberNumberRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 311
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 326
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 312
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 313
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 314
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 316
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 317
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 324
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 318
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 319
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 320
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 322
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 323
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getRememberWordsRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 509
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 524
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 510
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 511
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 512
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 514
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 515
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 522
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 516
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 517
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 518
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 520
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 521
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getSchulteTableRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 289
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 304
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 290
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 291
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 292
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 294
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 295
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 302
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 296
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 297
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 298
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 300
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 301
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getSpeedReadingRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 355
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 370
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 356
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 357
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 358
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 360
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 361
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 368
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 362
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 363
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 364
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 366
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 367
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getTrueColorsRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 531
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 546
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 532
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 533
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 534
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 536
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 537
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 544
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 538
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 539
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 540
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 542
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 543
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getWordBlockRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 245
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 260
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 246
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 247
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 248
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 250
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 251
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 258
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 252
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 253
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 254
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 256
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 257
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getWordPairsRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 377
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 392
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 378
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 379
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 380
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 382
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 383
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 390
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 384
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 385
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 386
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 388
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 389
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public getWordsColumnsRealm()Lio/realm/Realm;
    .locals 4

    .prologue
    .line 223
    :try_start_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;
    :try_end_0
    .catch Lio/realm/exceptions/RealmMigrationNeededException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lio/realm/exceptions/RealmFileException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 238
    .local v2, "realm":Lio/realm/Realm;
    :goto_0
    return-object v2

    .line 224
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_0
    move-exception v1

    .line 225
    .local v1, "me":Lio/realm/exceptions/RealmMigrationNeededException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 226
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 228
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 229
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .line 236
    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0

    .line 230
    .end local v1    # "me":Lio/realm/exceptions/RealmMigrationNeededException;
    .end local v2    # "realm":Lio/realm/Realm;
    :catch_1
    move-exception v0

    .line 231
    .local v0, "ex":Lio/realm/exceptions/RealmFileException;
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->isClosed()Z

    move-result v3

    if-nez v3, :cond_1

    .line 232
    invoke-static {}, Lio/realm/Realm;->getDefaultInstance()Lio/realm/Realm;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/Realm;->close()V

    .line 234
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->deleteRealm(Lio/realm/RealmConfiguration;)Z

    .line 235
    iget-object v3, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    invoke-static {v3}, Lio/realm/Realm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/Realm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/Realm;
    goto :goto_0
.end method

.method public onCreate()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x5

    const-wide/16 v6, 0x1

    const/4 v4, 0x0

    .line 91
    invoke-super {p0}, Landroid/app/Application;->onCreate()V

    .line 92
    sput-object p0, Lcom/safonov/speedreading/application/App;->instance:Lcom/safonov/speedreading/application/App;

    .line 94
    new-instance v0, Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/application/util/PremiumUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 95
    new-instance v0, Lcom/safonov/speedreading/application/util/BookUtil;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/application/util/BookUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    .line 97
    invoke-static {p0}, Lio/realm/Realm;->init(Landroid/content/Context;)V

    .line 98
    const-string v0, "ca-app-pub-1214906094509332~3275388209"

    invoke-static {p0, v0}, Lcom/google/android/gms/ads/MobileAds;->initialize(Landroid/content/Context;Ljava/lang/String;)V

    .line 100
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "passcourse.realm"

    .line 101
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 102
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 103
    invoke-virtual {v0, v8, v9}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 104
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->passCourseConfiguration:Lio/realm/RealmConfiguration;

    .line 107
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "wordscolumns.realm"

    .line 108
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 109
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 110
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->wordsColumnsConfiguration:Lio/realm/RealmConfiguration;

    .line 112
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "wordblock.realm"

    .line 113
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 114
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v6, v7}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 116
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 117
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->wordBlockConfiguration:Lio/realm/RealmConfiguration;

    .line 119
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "flashwords.realm"

    .line 120
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 121
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 122
    invoke-virtual {v0, v6, v7}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->flashWordsConfiguration:Lio/realm/RealmConfiguration;

    .line 126
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "schultetable.realm"

    .line 127
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 128
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->schulteTableConfiguration:Lio/realm/RealmConfiguration;

    .line 131
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "remembernumber.realm"

    .line 132
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 133
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 134
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->rememberNumberConfiguration:Lio/realm/RealmConfiguration;

    .line 136
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "lineofsight.realm"

    .line 137
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 138
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 139
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->lineOfSightConfiguration:Lio/realm/RealmConfiguration;

    .line 141
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "speedreading.realm"

    .line 142
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 143
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 144
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->speedReadingConfiguration:Lio/realm/RealmConfiguration;

    .line 146
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "wordpairs.realm"

    .line 147
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 148
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 149
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->wordPairsConfiguration:Lio/realm/RealmConfiguration;

    .line 151
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "evennumbers.realm"

    .line 152
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 153
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 154
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->evenNumbersConfiguration:Lio/realm/RealmConfiguration;

    .line 156
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "greendot.realm"

    .line 157
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 158
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 159
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->greenDotConfiguration:Lio/realm/RealmConfiguration;

    .line 161
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "math.realm"

    .line 162
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 163
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    const-wide/16 v2, 0x2

    .line 164
    invoke-virtual {v0, v2, v3}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 165
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 166
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->mathConfiguration:Lio/realm/RealmConfiguration;

    .line 168
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "concentration.realm"

    .line 169
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 170
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 171
    invoke-virtual {v0, v8, v9}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 172
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 173
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->concentrationConfiguration:Lio/realm/RealmConfiguration;

    .line 175
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "cuptimer.realm"

    .line 176
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 177
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    const-wide/16 v2, 0x4

    .line 178
    invoke-virtual {v0, v2, v3}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 179
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 180
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->cupTimerConfiguration:Lio/realm/RealmConfiguration;

    .line 182
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "rememberwords.realm"

    .line 183
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 184
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 185
    invoke-virtual {v0, v6, v7}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 186
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 187
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->rememberWordsConfiguration:Lio/realm/RealmConfiguration;

    .line 189
    new-instance v0, Lio/realm/RealmConfiguration$Builder;

    invoke-direct {v0}, Lio/realm/RealmConfiguration$Builder;-><init>()V

    const-string v1, "truecolors.realm"

    .line 190
    invoke-virtual {v0, v1}, Lio/realm/RealmConfiguration$Builder;->name(Ljava/lang/String;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmModule;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmModule;-><init>()V

    new-array v2, v4, [Ljava/lang/Object;

    .line 191
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmConfiguration$Builder;->modules(Ljava/lang/Object;[Ljava/lang/Object;)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 192
    invoke-virtual {v0, v6, v7}, Lio/realm/RealmConfiguration$Builder;->schemaVersion(J)Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->deleteRealmIfMigrationNeeded()Lio/realm/RealmConfiguration$Builder;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lio/realm/RealmConfiguration$Builder;->build()Lio/realm/RealmConfiguration;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/App;->trueColorsConfiguration:Lio/realm/RealmConfiguration;

    .line 195
    return-void
.end method
