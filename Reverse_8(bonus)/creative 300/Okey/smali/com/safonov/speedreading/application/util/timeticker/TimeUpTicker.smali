.class public Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;
.super Ljava/lang/Object;
.source "TimeUpTicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_DELAY:J = 0x64L


# instance fields
.field private currentPlayedTime:J

.field private handler:Landroid/os/Handler;

.field private listener:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->handler:Landroid/os/Handler;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;)J
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    .prologue
    .line 14
    iget-wide v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->currentPlayedTime:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;J)J
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;
    .param p1, "x1"    # J

    .prologue
    .line 14
    iput-wide p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->currentPlayedTime:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;)Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancel()J
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->timer:Ljava/util/Timer;

    .line 65
    :cond_0
    iget-wide v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->currentPlayedTime:J

    return-wide v0
.end method

.method public start()V
    .locals 4

    .prologue
    .line 34
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->start(JJ)V

    .line 35
    return-void
.end method

.method public start(J)V
    .locals 3
    .param p1, "playedTime"    # J

    .prologue
    .line 38
    const-wide/16 v0, 0x64

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->start(JJ)V

    .line 39
    return-void
.end method

.method public start(JJ)V
    .locals 7
    .param p1, "playedTime"    # J
    .param p3, "delay"    # J

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->currentPlayedTime:J

    .line 44
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->timer:Ljava/util/Timer;

    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$1;

    invoke-direct {v1, p0, p3, p4}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$1;-><init>(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;J)V

    move-wide v2, p3

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    .line 57
    return-void
.end method
