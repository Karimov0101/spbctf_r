.class public Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;
.super Ljava/lang/Object;
.source "TimeDownTicker.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;
    }
.end annotation


# static fields
.field private static final DEFAULT_DELAY:J = 0x64L


# instance fields
.field private handler:Landroid/os/Handler;

.field private listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

.field private timeLeft:J

.field private timer:Ljava/util/Timer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->handler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;)J
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .prologue
    .line 13
    iget-wide v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timeLeft:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;J)J
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;
    .param p1, "x1"    # J

    .prologue
    .line 13
    iput-wide p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timeLeft:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;)Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;)Ljava/util/Timer;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$202(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;Ljava/util/Timer;)Ljava/util/Timer;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;
    .param p1, "x1"    # Ljava/util/Timer;

    .prologue
    .line 13
    iput-object p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    return-object p1
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancel()J
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    .line 91
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    .line 94
    :cond_0
    iget-wide v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timeLeft:J

    return-wide v0
.end method

.method public setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    .line 24
    return-void
.end method

.method public start(J)V
    .locals 3
    .param p1, "timeLeft"    # J

    .prologue
    .line 38
    const-wide/16 v0, 0x64

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(JJ)V

    .line 39
    return-void
.end method

.method public start(JJ)V
    .locals 7
    .param p1, "timeLeft"    # J
    .param p3, "delay"    # J

    .prologue
    .line 42
    iput-wide p1, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timeLeft:J

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;->onStart()V

    .line 48
    :cond_0
    iget-wide v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timeLeft:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->listener:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;->onEnd()V

    .line 86
    :cond_1
    :goto_0
    return-void

    .line 55
    :cond_2
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    .line 56
    iget-object v0, p0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->timer:Ljava/util/Timer;

    new-instance v1, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$1;

    invoke-direct {v1, p0, p3, p4}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$1;-><init>(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;J)V

    move-wide v2, p3

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;JJ)V

    goto :goto_0
.end method
