.class public Lcom/safonov/speedreading/application/ratedialog/RateDialog;
.super Ljava/lang/Object;
.source "RateDialog.java"


# static fields
.field private static final DIALOG_LAUNCH_TIMES:Ljava/lang/String; = "dialog_launch_times"

.field private static final DONT_SHOW_AGAIN_KEY:Ljava/lang/String; = "dont_show_again"

.field private static final RATE_APP_DIALOG_SHARED_PREFERENCES:Ljava/lang/String; = "rate_app_dialog"


# instance fields
.field private final context:Landroid/content/Context;

.field private dialog:Landroid/support/v7/app/AlertDialog;

.field private final neededLaunchTimes:I

.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->context:Landroid/content/Context;

    .line 33
    const-string v0, "rate_app_dialog"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 34
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->neededLaunchTimes:I

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/RateDialog;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/RateDialog;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)Landroid/support/v7/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/application/ratedialog/RateDialog;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method public isShouldShow()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v2, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "dont_show_again"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 47
    :goto_0
    return v1

    .line 42
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "dialog_launch_times"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v0, v2, 0x1

    .line 43
    .local v0, "dialogLaunchCount":I
    iget v2, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->neededLaunchTimes:I

    if-ge v0, v2, :cond_1

    .line 44
    iget-object v2, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "dialog_launch_times"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 47
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public show()V
    .locals 8

    .prologue
    .line 54
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->context:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->context:Landroid/content/Context;

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f0b0086

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 58
    .local v2, "rateDialog":Landroid/view/View;
    const v5, 0x7f090158

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    .line 59
    .local v3, "rateNowButton":Landroid/widget/Button;
    new-instance v5, Lcom/safonov/speedreading/application/ratedialog/RateDialog$1;

    invoke-direct {v5, p0}, Lcom/safonov/speedreading/application/ratedialog/RateDialog$1;-><init>(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)V

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const v5, 0x7f090157

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 70
    .local v1, "dontShowAgainButton":Landroid/widget/Button;
    new-instance v5, Lcom/safonov/speedreading/application/ratedialog/RateDialog$2;

    invoke-direct {v5, p0}, Lcom/safonov/speedreading/application/ratedialog/RateDialog$2;-><init>(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v5, 0x7f090159

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    .line 80
    .local v4, "remindMeLaterButton":Landroid/widget/Button;
    new-instance v5, Lcom/safonov/speedreading/application/ratedialog/RateDialog$3;

    invoke-direct {v5, p0}, Lcom/safonov/speedreading/application/ratedialog/RateDialog$3;-><init>(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    new-instance v5, Lcom/safonov/speedreading/application/ratedialog/RateDialog$4;

    invoke-direct {v5, p0}, Lcom/safonov/speedreading/application/ratedialog/RateDialog$4;-><init>(Lcom/safonov/speedreading/application/ratedialog/RateDialog;)V

    invoke-virtual {v0, v5}, Landroid/support/v7/app/AlertDialog$Builder;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 95
    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 97
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    .line 98
    iget-object v5, p0, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->dialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v5}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 99
    return-void
.end method
