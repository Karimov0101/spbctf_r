.class public abstract Lcom/safonov/speedreading/application/realm/RealmUtil;
.super Ljava/lang/Object;
.source "RealmUtil.java"


# instance fields
.field protected realm:Lio/realm/Realm;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/safonov/speedreading/application/realm/RealmUtil;->realm:Lio/realm/Realm;

    .line 18
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/application/realm/RealmUtil;->realm:Lio/realm/Realm;

    invoke-virtual {v0}, Lio/realm/Realm;->close()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/application/realm/RealmUtil;->realm:Lio/realm/Realm;

    .line 28
    return-void
.end method

.method protected getNextId(Ljava/lang/Class;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmObject;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 21
    .local p1, "realmObjectClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmObject;>;"
    iget-object v1, p0, Lcom/safonov/speedreading/application/realm/RealmUtil;->realm:Lio/realm/Realm;

    invoke-virtual {v1, p1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    invoke-virtual {v1, v2}, Lio/realm/RealmQuery;->max(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 22
    .local v0, "currentId":Ljava/lang/Number;
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
