.class public Lcom/safonov/speedreading/BottomNavigationViewHelper;
.super Ljava/lang/Object;
.source "BottomNavigationViewHelper.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static removeShiftMode(Landroid/support/design/widget/BottomNavigationView;)V
    .locals 7
    .param p0, "view"    # Landroid/support/design/widget/BottomNavigationView;

    .prologue
    const/4 v5, 0x0

    .line 12
    invoke-virtual {p0, v5}, Landroid/support/design/widget/BottomNavigationView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/design/internal/BottomNavigationMenuView;

    .line 14
    .local v3, "menuView":Landroid/support/design/internal/BottomNavigationMenuView;
    :try_start_0
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    const-string v6, "mShiftingMode"

    invoke-virtual {v5, v6}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v4

    .line 15
    .local v4, "shiftingMode":Ljava/lang/reflect/Field;
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 16
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Field;->setBoolean(Ljava/lang/Object;Z)V

    .line 17
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 18
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {v3}, Landroid/support/design/internal/BottomNavigationMenuView;->getChildCount()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 19
    invoke-virtual {v3, v1}, Landroid/support/design/internal/BottomNavigationMenuView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/design/internal/BottomNavigationItemView;

    .line 20
    .local v2, "item":Landroid/support/design/internal/BottomNavigationItemView;
    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Landroid/support/design/internal/BottomNavigationItemView;->setShiftingMode(Z)V

    .line 22
    invoke-virtual {v2}, Landroid/support/design/internal/BottomNavigationItemView;->getItemData()Landroid/support/v7/view/menu/MenuItemImpl;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/view/menu/MenuItemImpl;->isChecked()Z

    move-result v5

    invoke-virtual {v2, v5}, Landroid/support/design/internal/BottomNavigationItemView;->setChecked(Z)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 18
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 24
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/support/design/internal/BottomNavigationItemView;
    .end local v4    # "shiftingMode":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 25
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v5, "ERROR NO SUCH FIELD"

    const-string v6, "Unable to get shift mode field"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :cond_0
    :goto_1
    return-void

    .line 26
    :catch_1
    move-exception v0

    .line 27
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v5, "ERROR ILLEGAL ALG"

    const-string v6, "Unable to change value of shift mode"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
