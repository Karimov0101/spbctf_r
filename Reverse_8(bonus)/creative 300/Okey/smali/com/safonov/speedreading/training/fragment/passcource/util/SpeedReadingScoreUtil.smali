.class public Lcom/safonov/speedreading/training/fragment/passcource/util/SpeedReadingScoreUtil;
.super Ljava/lang/Object;
.source "SpeedReadingScoreUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseScore(I)I
    .locals 1
    .param p0, "maxSpeed"    # I

    .prologue
    .line 9
    const/16 v0, 0xbb8

    if-le p0, v0, :cond_0

    const/16 v0, 0x56

    .line 71
    :goto_0
    return v0

    .line 10
    :cond_0
    const/16 v0, 0xb86

    if-le p0, v0, :cond_1

    const/16 v0, 0x54

    goto :goto_0

    .line 11
    :cond_1
    const/16 v0, 0xb54

    if-le p0, v0, :cond_2

    const/16 v0, 0x52

    goto :goto_0

    .line 12
    :cond_2
    const/16 v0, 0xb22

    if-le p0, v0, :cond_3

    const/16 v0, 0x50

    goto :goto_0

    .line 13
    :cond_3
    const/16 v0, 0xaf0

    if-le p0, v0, :cond_4

    const/16 v0, 0x4e

    goto :goto_0

    .line 14
    :cond_4
    const/16 v0, 0xabe

    if-le p0, v0, :cond_5

    const/16 v0, 0x4c

    goto :goto_0

    .line 15
    :cond_5
    const/16 v0, 0xa8c

    if-le p0, v0, :cond_6

    const/16 v0, 0x4a

    goto :goto_0

    .line 16
    :cond_6
    const/16 v0, 0xa5a

    if-le p0, v0, :cond_7

    const/16 v0, 0x48

    goto :goto_0

    .line 17
    :cond_7
    const/16 v0, 0xa28

    if-le p0, v0, :cond_8

    const/16 v0, 0x46

    goto :goto_0

    .line 18
    :cond_8
    const/16 v0, 0x9f6

    if-le p0, v0, :cond_9

    const/16 v0, 0x44

    goto :goto_0

    .line 19
    :cond_9
    const/16 v0, 0x9c4

    if-le p0, v0, :cond_a

    const/16 v0, 0x42

    goto :goto_0

    .line 20
    :cond_a
    const/16 v0, 0x992

    if-le p0, v0, :cond_b

    const/16 v0, 0x41

    goto :goto_0

    .line 21
    :cond_b
    const/16 v0, 0x960

    if-le p0, v0, :cond_c

    const/16 v0, 0x40

    goto :goto_0

    .line 22
    :cond_c
    const/16 v0, 0x92e

    if-le p0, v0, :cond_d

    const/16 v0, 0x3f

    goto :goto_0

    .line 23
    :cond_d
    const/16 v0, 0x8fc

    if-le p0, v0, :cond_e

    const/16 v0, 0x3e

    goto :goto_0

    .line 24
    :cond_e
    const/16 v0, 0x8ca

    if-le p0, v0, :cond_f

    const/16 v0, 0x3d

    goto :goto_0

    .line 25
    :cond_f
    const/16 v0, 0x898

    if-le p0, v0, :cond_10

    const/16 v0, 0x3c

    goto :goto_0

    .line 26
    :cond_10
    const/16 v0, 0x866

    if-le p0, v0, :cond_11

    const/16 v0, 0x3b

    goto :goto_0

    .line 27
    :cond_11
    const/16 v0, 0x834

    if-le p0, v0, :cond_12

    const/16 v0, 0x3a

    goto :goto_0

    .line 28
    :cond_12
    const/16 v0, 0x802

    if-le p0, v0, :cond_13

    const/16 v0, 0x39

    goto/16 :goto_0

    .line 29
    :cond_13
    const/16 v0, 0x7d0

    if-le p0, v0, :cond_14

    const/16 v0, 0x38

    goto/16 :goto_0

    .line 30
    :cond_14
    const/16 v0, 0x79e

    if-le p0, v0, :cond_15

    const/16 v0, 0x37

    goto/16 :goto_0

    .line 31
    :cond_15
    const/16 v0, 0x76c

    if-le p0, v0, :cond_16

    const/16 v0, 0x36

    goto/16 :goto_0

    .line 32
    :cond_16
    const/16 v0, 0x73a

    if-le p0, v0, :cond_17

    const/16 v0, 0x35

    goto/16 :goto_0

    .line 34
    :cond_17
    const/16 v0, 0x708

    if-le p0, v0, :cond_18

    const/16 v0, 0x34

    goto/16 :goto_0

    .line 35
    :cond_18
    const/16 v0, 0x6d6

    if-le p0, v0, :cond_19

    const/16 v0, 0x32

    goto/16 :goto_0

    .line 36
    :cond_19
    const/16 v0, 0x6a4

    if-le p0, v0, :cond_1a

    const/16 v0, 0x30

    goto/16 :goto_0

    .line 37
    :cond_1a
    const/16 v0, 0x672

    if-le p0, v0, :cond_1b

    const/16 v0, 0x2e

    goto/16 :goto_0

    .line 38
    :cond_1b
    const/16 v0, 0x640

    if-le p0, v0, :cond_1c

    const/16 v0, 0x2c

    goto/16 :goto_0

    .line 39
    :cond_1c
    const/16 v0, 0x60e

    if-le p0, v0, :cond_1d

    const/16 v0, 0x2a

    goto/16 :goto_0

    .line 40
    :cond_1d
    const/16 v0, 0x5dc

    if-le p0, v0, :cond_1e

    const/16 v0, 0x28

    goto/16 :goto_0

    .line 41
    :cond_1e
    const/16 v0, 0x5aa

    if-le p0, v0, :cond_1f

    const/16 v0, 0x26

    goto/16 :goto_0

    .line 42
    :cond_1f
    const/16 v0, 0x578

    if-le p0, v0, :cond_20

    const/16 v0, 0x24

    goto/16 :goto_0

    .line 43
    :cond_20
    const/16 v0, 0x546

    if-le p0, v0, :cond_21

    const/16 v0, 0x22

    goto/16 :goto_0

    .line 44
    :cond_21
    const/16 v0, 0x514

    if-le p0, v0, :cond_22

    const/16 v0, 0x20

    goto/16 :goto_0

    .line 45
    :cond_22
    const/16 v0, 0x4e2

    if-le p0, v0, :cond_23

    const/16 v0, 0x1e

    goto/16 :goto_0

    .line 46
    :cond_23
    const/16 v0, 0x4b0

    if-le p0, v0, :cond_24

    const/16 v0, 0x1c

    goto/16 :goto_0

    .line 47
    :cond_24
    const/16 v0, 0x47e

    if-le p0, v0, :cond_25

    const/16 v0, 0x1a

    goto/16 :goto_0

    .line 48
    :cond_25
    const/16 v0, 0x44c

    if-le p0, v0, :cond_26

    const/16 v0, 0x18

    goto/16 :goto_0

    .line 49
    :cond_26
    const/16 v0, 0x41a

    if-le p0, v0, :cond_27

    const/16 v0, 0x16

    goto/16 :goto_0

    .line 50
    :cond_27
    const/16 v0, 0x3e8

    if-le p0, v0, :cond_28

    const/16 v0, 0x14

    goto/16 :goto_0

    .line 52
    :cond_28
    const/16 v0, 0x3b6

    if-le p0, v0, :cond_29

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 53
    :cond_29
    const/16 v0, 0x384

    if-le p0, v0, :cond_2a

    const/16 v0, 0x11

    goto/16 :goto_0

    .line 54
    :cond_2a
    const/16 v0, 0x352

    if-le p0, v0, :cond_2b

    const/16 v0, 0x10

    goto/16 :goto_0

    .line 55
    :cond_2b
    const/16 v0, 0x320

    if-le p0, v0, :cond_2c

    const/16 v0, 0xf

    goto/16 :goto_0

    .line 56
    :cond_2c
    const/16 v0, 0x2ee

    if-le p0, v0, :cond_2d

    const/16 v0, 0xe

    goto/16 :goto_0

    .line 57
    :cond_2d
    const/16 v0, 0x2bc

    if-le p0, v0, :cond_2e

    const/16 v0, 0xd

    goto/16 :goto_0

    .line 58
    :cond_2e
    const/16 v0, 0x28a

    if-le p0, v0, :cond_2f

    const/16 v0, 0xc

    goto/16 :goto_0

    .line 59
    :cond_2f
    const/16 v0, 0x258

    if-le p0, v0, :cond_30

    const/16 v0, 0xb

    goto/16 :goto_0

    .line 60
    :cond_30
    const/16 v0, 0x226

    if-le p0, v0, :cond_31

    const/16 v0, 0xa

    goto/16 :goto_0

    .line 61
    :cond_31
    const/16 v0, 0x1f4

    if-le p0, v0, :cond_32

    const/16 v0, 0x9

    goto/16 :goto_0

    .line 62
    :cond_32
    const/16 v0, 0x1c2

    if-le p0, v0, :cond_33

    const/16 v0, 0x8

    goto/16 :goto_0

    .line 63
    :cond_33
    const/16 v0, 0x190

    if-le p0, v0, :cond_34

    const/4 v0, 0x7

    goto/16 :goto_0

    .line 64
    :cond_34
    const/16 v0, 0x15e

    if-le p0, v0, :cond_35

    const/4 v0, 0x6

    goto/16 :goto_0

    .line 65
    :cond_35
    const/16 v0, 0x12c

    if-le p0, v0, :cond_36

    const/4 v0, 0x5

    goto/16 :goto_0

    .line 66
    :cond_36
    const/16 v0, 0xfa

    if-le p0, v0, :cond_37

    const/4 v0, 0x4

    goto/16 :goto_0

    .line 67
    :cond_37
    const/16 v0, 0xc8

    if-le p0, v0, :cond_38

    const/4 v0, 0x3

    goto/16 :goto_0

    .line 68
    :cond_38
    const/16 v0, 0x96

    if-le p0, v0, :cond_39

    const/4 v0, 0x2

    goto/16 :goto_0

    .line 69
    :cond_39
    const/16 v0, 0x64

    if-le p0, v0, :cond_3a

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 71
    :cond_3a
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
