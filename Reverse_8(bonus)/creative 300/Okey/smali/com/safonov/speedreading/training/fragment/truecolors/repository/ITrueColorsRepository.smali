.class public interface abstract Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;
.super Ljava/lang/Object;
.source "ITrueColorsRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;ILcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigShowTime(II)V
.end method
