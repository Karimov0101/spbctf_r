.class public interface abstract Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;
.super Ljava/lang/Object;
.source "IMathRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;ILcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigComplexity(II)V
.end method
