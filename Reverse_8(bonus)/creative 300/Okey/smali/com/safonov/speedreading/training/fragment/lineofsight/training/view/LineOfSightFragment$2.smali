.class Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "LineOfSightFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCanceled:Z

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 132
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;->isCanceled:Z

    .line 135
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 139
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 140
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;->isCanceled:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->onPreShowAnimationCompleted()V

    .line 143
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 126
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 127
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;->isCanceled:Z

    .line 128
    return-void
.end method
