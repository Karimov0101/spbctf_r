.class public Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "MathResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-interface {v6, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v3

    .line 28
    .local v3, "result":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getConfig()Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getConfig()Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getResultList(I)Ljava/util/List;

    move-result-object v4

    .line 32
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;>;"
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getScore()I

    move-result v5

    .line 33
    .local v5, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getScore()I

    move-result v1

    .line 35
    .local v1, "bestScore":I
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getId()I

    move-result v6

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    const/4 v2, 0x1

    .line 37
    .local v2, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->isViewAttached()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;

    invoke-interface {v6, v5}, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;->updateScoreView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;->updateBestScoreView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;

    invoke-interface {v6, v2}, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;->setNewBestScoreViewVisibility(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;->setChartViewData(Ljava/util/List;)V

    .line 43
    :cond_0
    return-void

    .line 35
    .end local v2    # "isNewBest":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
