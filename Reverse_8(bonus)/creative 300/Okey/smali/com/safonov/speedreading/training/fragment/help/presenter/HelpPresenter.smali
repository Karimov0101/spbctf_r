.class public Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "HelpPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public requestToLoadContent(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 3
    .param p1, "helpFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const v2, 0x7f0b001b

    .line 16
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 17
    sget-object v0, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter$1;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 20
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 23
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 27
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b00a2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 30
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b0099

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 33
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b0044

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 36
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b00a9

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 39
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b00b8

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 42
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b0035

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 45
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b003d

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 48
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b004b

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto :goto_0

    .line 51
    :pswitch_a
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b0020

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 54
    :pswitch_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b0024

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 57
    :pswitch_c
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b009d

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 60
    :pswitch_d
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;

    const v1, 0x7f0b00b2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 17
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method
