.class Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;
.super Ljava/lang/Object;
.source "WordColumnsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 82
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;Z)Z

    .line 83
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 85
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;-><init>()V

    .line 86
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->setUnixTime(J)V

    .line 88
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->val$configId:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->updateConfigSpeed(II)V

    .line 89
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->addResult(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;ILcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;)V

    .line 97
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;Z)Z

    .line 70
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 71
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "playedTime"    # J

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->updateTimeView(J)V

    .line 78
    :cond_0
    return-void
.end method
