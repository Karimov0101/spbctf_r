.class public Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordPairsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;"
    }
.end annotation


# instance fields
.field private bestResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

.field private config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

.field private currentPlayTime:J

.field private isTrueAnswer:Z

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;

.field private repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

.field private score:I

.field private timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

.field private trueAnswerCount:I


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;)V
    .locals 0
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->model:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;

    .line 33
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    .prologue
    .line 19
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 128
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 130
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->currentPlayTime:J

    .line 131
    return-void
.end method

.method public init(I)V
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getTrainingDuration()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->currentPlayTime:J

    .line 46
    new-instance v0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;I)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 120
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 121
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->initBoard(II)V

    .line 123
    :cond_0
    return-void
.end method

.method public onItemTouchDown(I)V
    .locals 2
    .param p1, "itemIndex"    # I

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->areWordsEqual()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isTrueAnswer:Z

    .line 188
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isTrueAnswer:Z

    if-eqz v0, :cond_3

    .line 189
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    .line 190
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    .line 197
    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->updateScore(I)V

    .line 198
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isTrueAnswer:Z

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->itemTouchDown(IZ)V

    .line 200
    :cond_1
    return-void

    .line 186
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 192
    :cond_3
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    if-lez v0, :cond_0

    .line 193
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->score:I

    goto :goto_1
.end method

.method public onItemTouchUp(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isTrueAnswer:Z

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->itemTouchUp(IZ)V

    .line 207
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 208
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    .line 210
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->model:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 211
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 212
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v2

    .line 210
    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;->createWordPairs(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    .line 214
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->setBoardItems(Ljava/util/List;)V

    .line 217
    :cond_0
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->setBoardItemsEnable(Z)V

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->currentPlayTime:J

    .line 165
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->model:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 171
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 172
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v2

    .line 170
    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;->createWordPairs(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    .line 174
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->setBoardItems(Ljava/util/List;)V

    .line 175
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->setBoardItemsEnable(Z)V

    .line 177
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    .line 179
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->currentPlayTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 181
    :cond_0
    return-void
.end method

.method public startTraining()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 137
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->trueAnswerCount:I

    .line 139
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->updateProgressBar(I)V

    .line 141
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getTrainingDuration()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->initProgressBar(I)V

    .line 143
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->updateRecord(I)V

    .line 144
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->updateScore(I)V

    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->model:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 147
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 148
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v2

    .line 146
    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;->createWordPairs(II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    .line 150
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->setBoardItems(Ljava/util/List;)V

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->currentPlayTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 154
    :cond_0
    return-void

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v1

    goto :goto_0
.end method
