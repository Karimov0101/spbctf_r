.class Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;
.super Ljava/lang/Object;
.source "RememberWordsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->updateConfigWordsCount(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$wordsCount:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;->val$configId:I

    iput p3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;->val$wordsCount:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 69
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;->val$configId:I

    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .line 73
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    if-eqz v0, :cond_0

    .line 74
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;->val$wordsCount:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->setStartWordsCount(I)V

    .line 76
    :cond_0
    return-void
.end method
