.class public interface abstract Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
.super Ljava/lang/Object;
.source "ISpeedReadingRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;ILcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResultByAverageSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getBestResultByMaxSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigSpeed(II)V
.end method
