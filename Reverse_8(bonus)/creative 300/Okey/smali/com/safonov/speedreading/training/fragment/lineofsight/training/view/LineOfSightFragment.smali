.class public Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "LineOfSightFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field public static final FIELD_TYPE_0_ITEM_COUNT:I = 0x4

.field public static final FIELD_TYPE_1_ITEM_COUNT:I = 0x8

.field private static final ITEM_TEXT_SIZE_SP:I = 0x1e

.field private static final PRE_SHOW_DELAY_DURATION:I = 0x7d0

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field backgroundWhiteColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001f
    .end annotation
.end field

.field boardItemHeight:I
    .annotation build Lbutterknife/BindDimen;
        value = 0x7f07008a
    .end annotation
.end field

.field boardWidth:I
    .annotation build Lbutterknife/BindDimen;
        value = 0x7f07008b
    .end annotation
.end field

.field checkButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900dd
    .end annotation
.end field

.field private checkedItemIndexes:[I

.field private configId:I

.field greenColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field gridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900de
    .end annotation
.end field

.field private itemCount:I

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private lineOfSightModel:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;

.field private lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

.field private preShowAnimator:Landroid/animation/ValueAnimator;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900e0
    .end annotation
.end field

.field textColorBlack:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;

.field white:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060081
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 63
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)[I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkedItemIndexes:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method private initGridLayout(II)V
    .locals 9
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I

    .prologue
    const/4 v8, 0x1

    .line 184
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v6}, Landroid/support/v7/widget/GridLayout;->removeAllViews()V

    .line 185
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v6, p1}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 186
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v6, p2}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 188
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->boardItemHeight:I

    .line 189
    .local v2, "itemHeight":I
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->boardWidth:I

    div-int v3, v6, p2

    .line 191
    .local v3, "itemWidth":I
    new-instance v6, Ljava/util/ArrayList;

    mul-int v7, p1, p2

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemList:Ljava/util/List;

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 194
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, p2, :cond_0

    .line 195
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 196
    .local v1, "item":Landroid/widget/TextView;
    new-instance v5, Landroid/support/v7/widget/GridLayout$LayoutParams;

    .line 197
    invoke-static {v0, v8}, Landroid/support/v7/widget/GridLayout;->spec(II)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v6

    .line 198
    invoke-static {v4, v8}, Landroid/support/v7/widget/GridLayout;->spec(II)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/support/v7/widget/GridLayout$LayoutParams;-><init>(Landroid/support/v7/widget/GridLayout$Spec;Landroid/support/v7/widget/GridLayout$Spec;)V

    .line 200
    .local v5, "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 201
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iput v2, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 202
    invoke-virtual {v1}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    iput v3, v6, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 204
    const/16 v6, 0x11

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setGravity(I)V

    .line 205
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->textColorBlack:I

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 206
    const/4 v6, 0x2

    const/high16 v7, 0x41f00000    # 30.0f

    invoke-virtual {v1, v6, v7}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 207
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->backgroundWhiteColor:I

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 209
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemList:Ljava/util/List;

    invoke-interface {v6, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v6, v1}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 194
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 193
    .end local v1    # "item":Landroid/widget/TextView;
    .end local v5    # "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    .end local v4    # "j":I
    :cond_1
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 69
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;-><init>()V

    .line 70
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 71
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->setArguments(Landroid/os/Bundle;)V

    .line 73
    return-object v1
.end method


# virtual methods
.method public cancelPreShowAnimation()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 238
    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 57
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getLineOfSightRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 58
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightModel:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;

    .line 59
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightModel:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V

    return-object v1
.end method

.method public initBoardView(III)V
    .locals 7
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I
    .param p3, "fieldType"    # I

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 250
    mul-int v0, p1, p2

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemCount:I

    .line 251
    if-nez p3, :cond_1

    .line 252
    new-array v0, v6, [I

    div-int/lit8 v1, p2, 0x2

    aput v1, v0, v2

    div-int/lit8 v1, p1, 0x2

    mul-int/2addr v1, p2

    aput v1, v0, v3

    div-int/lit8 v1, p1, 0x2

    mul-int/2addr v1, p2

    add-int/2addr v1, p2

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v4

    mul-int v1, p2, p1

    div-int/lit8 v2, p2, 0x2

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v5

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkedItemIndexes:[I

    .line 271
    :cond_0
    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->initGridLayout(II)V

    .line 272
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->startTraining()V

    .line 273
    return-void

    .line 258
    :cond_1
    if-ne p3, v3, :cond_0

    .line 259
    const/16 v0, 0x8

    new-array v0, v0, [I

    aput v2, v0, v2

    div-int/lit8 v1, p2, 0x2

    aput v1, v0, v3

    add-int/lit8 v1, p2, -0x1

    aput v1, v0, v4

    div-int/lit8 v1, p1, 0x2

    mul-int/2addr v1, p2

    aput v1, v0, v5

    div-int/lit8 v1, p1, 0x2

    mul-int/2addr v1, p2

    add-int/2addr v1, p2

    add-int/lit8 v1, v1, -0x1

    aput v1, v0, v6

    const/4 v1, 0x5

    mul-int v2, p1, p2

    sub-int/2addr v2, p2

    aput v2, v0, v1

    const/4 v1, 0x6

    mul-int v2, p2, p1

    div-int/lit8 v3, p2, 0x2

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    const/4 v1, 0x7

    mul-int v2, p1, p2

    add-int/lit8 v2, v2, -0x1

    aput v2, v0, v1

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkedItemIndexes:[I

    goto :goto_0
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 223
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 308
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 309
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 310
    check-cast p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

    .line 315
    return-void

    .line 312
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement LineOfSightTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCheckButtonClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900dd
        }
    .end annotation

    .prologue
    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->onCheckButtonPressed()V

    .line 218
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->configId:I

    .line 82
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 102
    const v1, 0x7f0b0045

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 104
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->unbinder:Lbutterknife/Unbinder;

    .line 106
    new-array v1, v5, [I

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->backgroundWhiteColor:I

    aput v2, v1, v3

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->greenColor:I

    aput v2, v1, v4

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    .line 107
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Landroid/animation/ArgbEvaluator;

    invoke-direct {v2}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 108
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 109
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v4}, Landroid/animation/ValueAnimator;->setRepeatCount(I)V

    .line 110
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v5}, Landroid/animation/ValueAnimator;->setRepeatMode(I)V

    .line 111
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 120
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$2;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 146
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 157
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 159
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    .line 161
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->close()V

    .line 162
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 164
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->lineOfSightModel:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;

    .line 166
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->cancelTraining()V

    .line 167
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 168
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->unbinder:Lbutterknife/Unbinder;

    .line 175
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 319
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 320
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

    .line 321
    return-void
.end method

.method public onLineOfSightTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 301
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;->onLineOfSightTrainingCompleted(I)V

    .line 302
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 151
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->init(I)V

    .line 153
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->pauseTraining()V

    .line 292
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 296
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;->resumeTraining()V

    .line 297
    return-void
.end method

.method public setCheckButtonEnabled(Z)V
    .locals 1
    .param p1, "isEnabled"    # Z

    .prologue
    .line 242
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkButton:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 243
    return-void
.end method

.method public setCheckedItemsData(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 284
    .local p1, "checkedItemsData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkedItemIndexes:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 285
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemList:Ljava/util/List;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkedItemIndexes:[I

    aget v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    :cond_0
    return-void
.end method

.method public setItemsData(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 277
    .local p1, "itemsData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemCount:I

    if-ge v0, v1, :cond_0

    .line 278
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 280
    :cond_0
    return-void
.end method

.method public startPreShowAnimation()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->preShowAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 233
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 228
    return-void
.end method
