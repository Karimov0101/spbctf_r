.class public interface abstract Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;
.super Ljava/lang/Object;
.source "IDescriptionPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract requestToLoadContent(Lcom/safonov/speedreading/training/FragmentType;)V
.end method

.method public abstract setDontShowAgain(Lcom/safonov/speedreading/training/FragmentType;Z)V
.end method
