.class Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;
.super Ljava/lang/Object;
.source "WordBlockPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 211
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->setWordBlockItemText(Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
