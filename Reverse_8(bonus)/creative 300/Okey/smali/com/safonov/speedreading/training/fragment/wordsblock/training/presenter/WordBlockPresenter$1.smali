.class Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;
.super Ljava/lang/Object;
.source "WordBlockPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 51
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 5

    .prologue
    .line 67
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;Z)Z

    .line 68
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 70
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;-><init>()V

    .line 71
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;->setUnixTime(J)V

    .line 73
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->val$configId:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->updateConfig(III)V

    .line 74
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->addResult(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;ILcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V

    .line 82
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;Z)Z

    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 56
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "playedTime"    # J

    .prologue
    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->updateTimeView(J)V

    .line 63
    :cond_0
    return-void
.end method
