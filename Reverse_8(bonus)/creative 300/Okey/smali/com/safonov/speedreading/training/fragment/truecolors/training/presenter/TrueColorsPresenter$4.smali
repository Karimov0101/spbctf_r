.class Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;
.super Ljava/lang/Object;
.source "TrueColorsPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    .prologue
    .line 157
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 160
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->hideAnswerImage()V

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    if-gez v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    const/4 v1, 0x0

    iput v1, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCount:I

    const/16 v1, 0x32

    if-ne v0, v1, :cond_1

    .line 167
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->finishTraining()V

    .line 178
    :goto_0
    return-void

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getAnswers()Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    .line 172
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget-object v1, v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->currentColor:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget-object v2, v2, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->showLevel(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/util/List;)V

    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget v1, v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->updateScoreView(I)V

    .line 175
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget v1, v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->updateProgressBar(I)V

    .line 176
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->timerStart()V

    .line 177
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->enableButtons()V

    goto :goto_0
.end method
