.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;
.super Ljava/lang/Object;
.source "IWordColumnsView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract deselectItem(I)V
.end method

.method public abstract initGridLayout(II)V
.end method

.method public abstract onTrainingCompleted(I)V
.end method

.method public abstract selectItem(I)V
.end method

.method public abstract setItems([Ljava/lang/String;)V
    .param p1    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract updateSpeedView(I)V
.end method

.method public abstract updateTimeView(J)V
.end method
