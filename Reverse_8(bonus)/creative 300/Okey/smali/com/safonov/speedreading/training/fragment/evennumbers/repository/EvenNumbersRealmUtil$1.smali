.class Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;
.super Ljava/lang/Object;
.source "EvenNumbersRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 72
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$configId:I

    .line 73
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 76
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setId(I)V

    .line 77
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;)V

    .line 79
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 80
    return-void
.end method
