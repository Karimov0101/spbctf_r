.class Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "GreenDotPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->startTraining(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCanceled:Z

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->val$configId:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->isCanceled:Z

    .line 56
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 61
    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->isCanceled:Z

    if-nez v1, :cond_0

    .line 63
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;-><init>()V

    .line 64
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->setUnixTime(J)V

    .line 66
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2$1;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->addResult(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;ILcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V

    .line 75
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 49
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$2;->isCanceled:Z

    .line 50
    return-void
.end method
