.class public Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "EvenNumbersPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 7
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-interface {v5, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v2

    .line 28
    .local v2, "currentResult":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getId()I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v4

    .line 31
    .local v4, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v1

    .line 33
    .local v1, "bestScore":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getId()I

    move-result v5

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getId()I

    move-result v6

    if-ne v5, v6, :cond_1

    const/4 v3, 0x1

    .line 35
    .local v3, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->isViewAttached()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;

    invoke-interface {v5, v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;->updateScoreView(I)V

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;

    invoke-interface {v5, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;->updateBestScoreView(I)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;

    invoke-interface {v5, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;->setNewBestScoreViewVisibility(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/passcource/util/EvenNumbersScoreUtil;->getPassCourseScore(I)I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 41
    :cond_0
    return-void

    .line 33
    .end local v3    # "isNewBest":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
