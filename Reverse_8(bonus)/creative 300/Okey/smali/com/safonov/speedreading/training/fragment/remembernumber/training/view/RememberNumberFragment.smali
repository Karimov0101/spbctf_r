.class public Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "RememberNumberFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final CARD_TEXT_SIZE_SP:I = 0x28

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field acceptGreen:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field buttons:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f090162,
            0x7f090163,
            0x7f090164,
            0x7f090165,
            0x7f090166,
            0x7f090167,
            0x7f090168,
            0x7f090169,
            0x7f09016a,
            0x7f090161,
            0x7f09016b
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final cardLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

.field cardMargin:I
    .annotation build Lbutterknife/BindDimen;
        value = 0x7f0700a3
    .end annotation
.end field

.field cardPadding:I
    .annotation build Lbutterknife/BindDimen;
        value = 0x7f0700a4
    .end annotation
.end field

.field private cards:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field cardsLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09016c
    .end annotation
.end field

.field colorAccent:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06002d
    .end annotation
.end field

.field private configId:I

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014e
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015f
    .end annotation
.end field

.field reject_red:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006d
    .end annotation
.end field

.field private rememberNumberModel:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;

.field private rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090188
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 54
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 162
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    .line 55
    return-void
.end method

.method private createCard()Landroid/widget/TextView;
    .locals 5

    .prologue
    .line 195
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 197
    .local v0, "card":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 198
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardPadding:I

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardPadding:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardPadding:I

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardPadding:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 199
    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 200
    const/4 v1, 0x2

    const/high16 v2, 0x42200000    # 40.0f

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 202
    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;
    .locals 5
    .param p0, "configId"    # I

    .prologue
    .line 61
    const-string v2, "configId"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;-><init>()V

    .line 63
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 49
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getRememberNumberRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberModel:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;

    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberModel:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;)V

    return-object v1
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "maxProgress"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 143
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 297
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 298
    check-cast p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

    .line 303
    return-void

    .line 300
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement RememberNumberTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onBackspaceButtonClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09016b
        }
    .end annotation

    .prologue
    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->onBackspaceButtonPressed()V

    .line 128
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->configId:I

    .line 75
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    const v1, 0x7f0b009a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->unbinder:Lbutterknife/Unbinder;

    .line 105
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 270
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 271
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberModel:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;

    .line 273
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->close()V

    .line 274
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    .line 276
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->cancelTraining()V

    .line 277
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 278
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 283
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->unbinder:Lbutterknife/Unbinder;

    .line 285
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 307
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

    .line 309
    return-void
.end method

.method public onNumberButtonClick(Landroid/widget/Button;)V
    .locals 2
    .param p1, "button"    # Landroid/widget/Button;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090162,
            0x7f090163,
            0x7f090164,
            0x7f090165,
            0x7f090166,
            0x7f090167,
            0x7f090168,
            0x7f090169,
            0x7f09016a,
            0x7f090161
        }
    .end annotation

    .prologue
    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->onNumberButtonPressed(Ljava/lang/String;)V

    .line 123
    return-void
.end method

.method public onRememberNumberTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 289
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;->onRememberNumberTrainingCompleted(I)V

    .line 290
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 110
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 111
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->startTraining(I)V

    .line 112
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->pauseTraining()V

    .line 261
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;->resumeTraining()V

    .line 266
    return-void
.end method

.method public sedDefaultCardValueAt(I)V
    .locals 2
    .param p1, "cardIndex"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "_"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    return-void
.end method

.method public setBlinkCardsTextColor()V
    .locals 3

    .prologue
    .line 253
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 254
    .local v0, "card":Landroid/widget/TextView;
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->colorAccent:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 256
    .end local v0    # "card":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setButtonsEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 152
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 153
    .local v0, "button":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 155
    .end local v0    # "button":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public setCardAnswerBackgroundAt(IZ)V
    .locals 2
    .param p1, "cardIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 238
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    const v1, 0x7f0800e5

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 241
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 242
    return-void

    .line 238
    :cond_0
    const v1, 0x7f0800e4

    goto :goto_0
.end method

.method public setCardValueAt(ILjava/lang/String;)V
    .locals 1
    .param p1, "cardIndex"    # I
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    return-void
.end method

.method public setCardViews(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const/4 v5, 0x0

    .line 170
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardLayoutParams:Landroid/widget/LinearLayout$LayoutParams;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardMargin:I

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardMargin:I

    invoke-virtual {v2, v3, v5, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 172
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 173
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_2

    .line 174
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->createCard()Landroid/widget/TextView;

    move-result-object v0

    .line 176
    .local v0, "card":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 177
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 180
    .end local v0    # "card":Landroid/widget/TextView;
    .end local v1    # "i":I
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    if-ne p1, v2, :cond_1

    .line 181
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->createCard()Landroid/widget/TextView;

    move-result-object v0

    .line 183
    .restart local v0    # "card":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 187
    .end local v0    # "card":Landroid/widget/TextView;
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_2

    .line 188
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    add-int/lit8 v3, p1, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 189
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardsLayout:Landroid/widget/LinearLayout;

    add-int/lit8 v3, p1, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 192
    :cond_2
    return-void
.end method

.method public setCardsDefaultBackground()V
    .locals 3

    .prologue
    .line 231
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 232
    .local v0, "card":Landroid/widget/TextView;
    const v2, 0x7f0800e3

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_0

    .line 234
    .end local v0    # "card":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setCardsDefaultTextColor()V
    .locals 3

    .prologue
    .line 246
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 247
    .local v0, "card":Landroid/widget/TextView;
    const/high16 v2, -0x1000000

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 249
    .end local v0    # "card":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setCardsDefaultValues()V
    .locals 3

    .prologue
    .line 219
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 220
    .local v0, "card":Landroid/widget/TextView;
    const-string v2, "_"

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 222
    .end local v0    # "card":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setCardsValues([Ljava/lang/String;)V
    .locals 3
    .param p1, "values"    # [Ljava/lang/String;

    .prologue
    .line 207
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 208
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cards:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method

.method public updateBestScoreView(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e014b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 148
    return-void
.end method

.method public updateScoreView(I)V
    .locals 5
    .param p1, "score"    # I

    .prologue
    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->scoreTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0151

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    return-void
.end method
