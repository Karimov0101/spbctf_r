.class public Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "ConcentrationRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 26
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;

    .prologue
    .line 150
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 152
    .local v0, "configs":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;>;"
    invoke-virtual {v0}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 153
    invoke-virtual {v0}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 154
    const-string v3, "concentration config r"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :goto_0
    return-void

    .line 158
    :cond_0
    const-string v2, "concentration config r"

    const-string v3, "\u041d\u0435\u0442 \u043a\u043e\u043d\u0444\u0438\u0433\u0430"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    const-class v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v1

    .line 162
    .local v1, "nextId":I
    invoke-virtual {p1, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setId(I)V

    .line 164
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$6;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$7;

    invoke-direct {v4, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;

    .prologue
    .line 181
    const-class v6, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 183
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 184
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 186
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 187
    aget-object v0, p1, v3

    .line 189
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "circlesCount"

    .line 190
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 191
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 192
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "circlesRadius"

    .line 193
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 194
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 195
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "grayTime"

    .line 196
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getGrayTime()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 197
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 198
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "circlesSpeed"

    .line 199
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeed()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 200
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 202
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 204
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 186
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 207
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setId(I)V

    .line 208
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    aput v4, v1, v3

    .line 211
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 215
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 216
    if-eqz p2, :cond_2

    .line 217
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 236
    :cond_2
    :goto_2
    return-void

    .line 223
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$9;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$9;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ILcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;

    .prologue
    .line 124
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 126
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;ILcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$5;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 146
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 3
    .param p1, "configId"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 48
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 50
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 47
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 59
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 58
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 65
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 32
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 31
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 39
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 38
    return-object v0
.end method

.method public updateConfig(IIII)V
    .locals 7
    .param p1, "configId"    # I
    .param p2, "circlesCount"    # I
    .param p3, "circlesRadius"    # I
    .param p4, "circlesSpeed"    # I

    .prologue
    .line 87
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$2;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;IIII)V

    invoke-virtual {v6, v0}, Lio/realm/Realm;->executeTransaction(Lio/realm/Realm$Transaction;)V

    .line 102
    return-void
.end method

.method public updateConfigComplexity(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "complexity"    # I

    .prologue
    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 83
    return-void
.end method

.method public updateConfigCustom(IIII)V
    .locals 7
    .param p1, "configId"    # I
    .param p2, "circlesCount"    # I
    .param p3, "circlesRadius"    # I
    .param p4, "circlesSpeed"    # I

    .prologue
    .line 106
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->realm:Lio/realm/Realm;

    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$3;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;IIII)V

    invoke-virtual {v6, v0}, Lio/realm/Realm;->executeTransaction(Lio/realm/Realm$Transaction;)V

    .line 120
    return-void
.end method
