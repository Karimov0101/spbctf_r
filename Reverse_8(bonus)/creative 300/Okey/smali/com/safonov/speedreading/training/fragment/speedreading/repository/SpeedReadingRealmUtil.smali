.class public Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "SpeedReadingRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;

    .prologue
    .line 120
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "trainingShowCount"

    .line 121
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getTrainingShowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 122
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "minWordShowCount"

    .line 124
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinWordShowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 125
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 126
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "maxWordShowCount"

    .line 127
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMaxWordShowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 128
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 129
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "minSpeed"

    .line 130
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinSpeed()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 131
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 132
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "speedStep"

    .line 133
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getSpeedStep()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 134
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 137
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 138
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 159
    :goto_0
    return-void

    .line 142
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 144
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setId(I)V

    .line 146
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$5;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;

    .prologue
    .line 164
    const-class v6, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 166
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 167
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 169
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 170
    aget-object v0, p1, v3

    .line 172
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "trainingShowCount"

    .line 173
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getTrainingShowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 174
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 175
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "minWordShowCount"

    .line 176
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinWordShowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 177
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 178
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "maxWordShowCount"

    .line 179
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMaxWordShowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 180
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 181
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "minSpeed"

    .line 182
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinSpeed()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 183
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 184
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "speedStep"

    .line 185
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getSpeedStep()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 186
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 188
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 190
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 169
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 193
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setId(I)V

    .line 194
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    aput v4, v1, v3

    .line 197
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 201
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 202
    if-eqz p2, :cond_2

    .line 203
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 222
    :cond_2
    :goto_2
    return-void

    .line 209
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$7;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;ILcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;

    .prologue
    .line 93
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 95
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;ILcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$3;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 115
    return-void
.end method

.method public getBestResultByAverageSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "averageSpeed"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 55
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .line 52
    return-object v0
.end method

.method public getBestResultByMaxSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "maxSpeed"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 45
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .line 42
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .locals 3
    .param p1, "speedReadingConfigId"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 63
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .line 62
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 70
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 69
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .line 35
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method public updateConfigSpeed(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "speed"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 87
    return-void
.end method
