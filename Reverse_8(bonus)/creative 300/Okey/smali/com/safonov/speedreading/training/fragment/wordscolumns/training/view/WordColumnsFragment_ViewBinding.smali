.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "WordColumnsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

.field private view2131296682:Landroid/view/View;

.field private view2131296712:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    .line 31
    const v2, 0x7f090022

    const-string v3, "field \'adView\'"

    const-class v4, Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/ads/AdView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 32
    const v2, 0x7f090207

    const-string v3, "field \'timeTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->timeTextView:Landroid/widget/TextView;

    .line 33
    const v2, 0x7f0901c7

    const-string v3, "field \'speedTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->speedTextView:Landroid/widget/TextView;

    .line 34
    const v2, 0x7f0900a6

    const-string v3, "field \'gridLayout\'"

    const-class v4, Landroid/support/v7/widget/GridLayout;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/GridLayout;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 35
    const v2, 0x7f0901aa

    const-string v3, "method \'onSpeedDownViewClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 36
    .local v1, "view":Landroid/view/View;
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 37
    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 43
    const v2, 0x7f0901c8

    const-string v3, "method \'onSpeedUpViewClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 44
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 45
    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding$2;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 52
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    .local v0, "context":Landroid/content/Context;
    const v2, 0x1060004

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemTextColor:I

    .line 54
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    .line 60
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 61
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    .line 63
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 64
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->timeTextView:Landroid/widget/TextView;

    .line 65
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->speedTextView:Landroid/widget/TextView;

    .line 66
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 68
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 70
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 72
    return-void
.end method
