.class public Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SpeedReadingPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "result_id"


# instance fields
.field averageSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090030
    .end annotation
.end field

.field bestAverageSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090033
    .end annotation
.end field

.field bestMaxSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090034
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field maxSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090103
    .end annotation
.end field

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field private resultId:I

.field private speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 42
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 48
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;-><init>()V

    .line 49
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getSpeedReadingRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 38
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 142
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 143
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 148
    return-void

    .line 145
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->resultId:I

    .line 61
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const v1, 0x7f0b00ab

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 80
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->close()V

    .line 160
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 161
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 162
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 166
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 167
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 169
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 152
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 154
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 135
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;->initTrainingResults(I)V

    .line 87
    return-void
.end method

.method public setNewBestAverageSpeedViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 118
    if-nez p1, :cond_0

    .line 119
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->averageSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 121
    :cond_0
    return-void
.end method

.method public setNewBestMaxSpeedViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 101
    if-nez p1, :cond_0

    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->maxSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 104
    :cond_0
    return-void
.end method

.method public updateAverageSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->averageSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method

.method public updateBestAverageSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->bestAverageSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method public updateBestMaxSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->bestMaxSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method public updateMaxSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->maxSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    return-void
.end method
