.class public Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
.super Lio/realm/RealmObject;
.source "TrueColorsConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/TrueColorsConfigRealmProxyInterface;


# static fields
.field public static final FIELD_SHOW_TIME:Ljava/lang/String; = "showTime"


# instance fields
.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private showTime:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getShowTime()I
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->realmGet$showTime()I

    move-result v0

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->id:I

    return v0
.end method

.method public realmGet$showTime()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->showTime:I

    return v0
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->id:I

    return-void
.end method

.method public realmSet$showTime(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->showTime:I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->realmSet$id(I)V

    .line 34
    return-void
.end method

.method public setShowTime(I)V
    .locals 0
    .param p1, "showTime"    # I

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->realmSet$showTime(I)V

    .line 26
    return-void
.end method
