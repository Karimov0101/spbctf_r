.class public Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "EvenNumbersResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-interface {v6, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v2

    .line 28
    .local v2, "currentResult":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getResultList(I)Ljava/util/List;

    move-result-object v4

    .line 32
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;>;"
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v5

    .line 33
    .local v5, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v1

    .line 35
    .local v1, "bestScore":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getId()I

    move-result v6

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    const/4 v3, 0x1

    .line 37
    .local v3, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->isViewAttached()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;

    invoke-interface {v6, v5}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;->updateScoreView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;->updateBestScoreView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;

    invoke-interface {v6, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;->setNewBestScoreViewVisibility(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;->setChartViewData(Ljava/util/List;)V

    .line 43
    :cond_0
    return-void

    .line 35
    .end local v3    # "isNewBest":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
