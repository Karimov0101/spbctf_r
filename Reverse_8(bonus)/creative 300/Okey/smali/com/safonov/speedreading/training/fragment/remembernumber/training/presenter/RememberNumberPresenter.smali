.class public Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "RememberNumberPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;"
    }
.end annotation


# static fields
.field private static final BLINK_DURATION:I = 0xc8

.field private static final PRE_SHOW_DURATION:I = 0x2ee

.field private static final SHOW_NUMBER_DURATION:I = 0x1f4


# instance fields
.field private bestResult:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

.field private blinkRunnable:Ljava/lang/Runnable;

.field private cardIndex:I

.field private complexity:I

.field private config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

.field private configId:I

.field private handler:Landroid/os/Handler;

.field private isPaused:Z

.field private model:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;

.field private numberHasShown:Z

.field private postShowNumberRunnable:Ljava/lang/Runnable;

.field private preShowNumberRunnable:Ljava/lang/Runnable;

.field private repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

.field private score:I

.field private showCount:I

.field private showNumberRunnable:Ljava/lang/Runnable;

.field private trueAnswer:[Ljava/lang/String;

.field private trueAnswerCount:I

.field private userAnswer:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;)V
    .locals 1
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 37
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    .line 169
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->preShowNumberRunnable:Ljava/lang/Runnable;

    .line 206
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->blinkRunnable:Ljava/lang/Runnable;

    .line 221
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showNumberRunnable:Ljava/lang/Runnable;

    .line 240
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$4;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->postShowNumberRunnable:Ljava/lang/Runnable;

    .line 42
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->model:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;

    .line 43
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    .line 44
    return-void
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->numberHasShown:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isPaused:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showNumberRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$102(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isPaused:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->userAnswer:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswer:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
    .param p1, "x1"    # [Ljava/lang/String;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswer:[Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->model:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->postShowNumberRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->configId:I

    return v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    return v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->blinkRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->preShowNumberRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showNumberRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 77
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->postShowNumberRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->blinkRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    .line 82
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->model:Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;

    .line 83
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->userAnswer:[Ljava/lang/String;

    .line 84
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswer:[Ljava/lang/String;

    .line 85
    return-void
.end method

.method public onBackspaceButtonPressed()V
    .locals 2

    .prologue
    .line 158
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    if-lez v0, :cond_0

    .line 159
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    .line 161
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->sedDefaultCardValueAt(I)V

    .line 165
    :cond_0
    return-void
.end method

.method public onNumberButtonPressed(Ljava/lang/String;)V
    .locals 6
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 102
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->userAnswer:[Ljava/lang/String;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    aput-object p1, v2, v3

    .line 104
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    invoke-interface {v2, v3, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardValueAt(ILjava/lang/String;)V

    .line 106
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    .line 108
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    if-ne v2, v3, :cond_7

    .line 109
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showCount:I

    .line 110
    iput v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->cardIndex:I

    .line 112
    const/4 v1, 0x1

    .line 113
    .local v1, "isTrueAnswer":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    if-ge v0, v2, :cond_1

    .line 114
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->userAnswer:[Ljava/lang/String;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswer:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    .line 116
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardAnswerBackgroundAt(IZ)V

    .line 113
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    const/4 v1, 0x0

    .line 119
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    .line 120
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v2, v0, v4}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardAnswerBackgroundAt(IZ)V

    goto :goto_1

    .line 124
    :cond_1
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    if-gez v2, :cond_2

    .line 125
    iput v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    .line 128
    :cond_2
    if-eqz v1, :cond_8

    .line 129
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    .line 134
    :goto_2
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityDown()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 135
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMinComplexity()I

    move-result v3

    if-le v2, v3, :cond_3

    .line 136
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    .line 138
    :cond_3
    iput v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    .line 141
    :cond_4
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityUp()I

    move-result v3

    if-ne v2, v3, :cond_6

    .line 142
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMaxComplexity()I

    move-result v3

    if-ge v2, v3, :cond_5

    .line 143
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    .line 145
    :cond_5
    iput v4, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    .line 148
    :cond_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->score:I

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->updateScoreView(I)V

    .line 149
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v2, v4}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setButtonsEnabled(Z)V

    .line 150
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->showCount:I

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->updateProgressBar(I)V

    .line 152
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->preShowNumberRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2ee

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    .end local v0    # "i":I
    .end local v1    # "isTrueAnswer":Z
    :cond_7
    return-void

    .line 131
    .restart local v0    # "i":I
    .restart local v1    # "isTrueAnswer":Z
    :cond_8
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->trueAnswerCount:I

    goto :goto_2
.end method

.method public pauseTraining()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isPaused:Z

    .line 90
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 94
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isPaused:Z

    .line 95
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->numberHasShown:Z

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->preShowNumberRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 98
    :cond_0
    return-void
.end method

.method public startTraining(I)V
    .locals 4
    .param p1, "configId"    # I

    .prologue
    const/4 v2, 0x0

    .line 52
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->configId:I

    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 56
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getComplexity()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->updateScoreView(I)V

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->updateBestScoreView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getTrainingShowCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->initProgressBar(I)V

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->updateProgressBar(I)V

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->complexity:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardViews(I)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultBackground()V

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultValues()V

    .line 67
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultTextColor()V

    .line 68
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setButtonsEnabled(Z)V

    .line 70
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->preShowNumberRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    return-void

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v1

    goto :goto_0
.end method
