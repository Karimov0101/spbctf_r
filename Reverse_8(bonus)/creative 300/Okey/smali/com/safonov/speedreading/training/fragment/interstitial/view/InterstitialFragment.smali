.class public Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "InterstitialFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/interstitial/view/IInterstitialView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/interstitial/view/IInterstitialView;",
        "Lcom/safonov/speedreading/training/fragment/interstitial/presenter/IInterstitialPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/interstitial/view/IInterstitialView;"
    }
.end annotation


# static fields
.field private static final COURSE_TYPE_PARAM:Ljava/lang/String; = "course_type"

.field private static final TRAINING_INDEX_PARAM:Ljava/lang/String; = "training_index"


# instance fields
.field private courseType:Lcom/safonov/speedreading/training/TrainingType;

.field private fragmentListener:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

.field pager:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090223
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09012f
    .end annotation
.end field

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090160
    .end annotation
.end field

.field titleTv:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900bc
    .end annotation
.end field

.field topIv:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900bb
    .end annotation
.end field

.field trainersLeft:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090133
    .end annotation
.end field

.field private trainingIndex:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    return-object v0
.end method

.method public static newInstance(Lcom/safonov/speedreading/training/TrainingType;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
    .locals 3
    .param p0, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "trainingIndex"    # I

    .prologue
    .line 59
    new-instance v1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;-><init>()V

    .line 60
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 61
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_index"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    const-string v2, "course_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 63
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/interstitial/presenter/IInterstitialPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/interstitial/presenter/IInterstitialPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    new-instance v0, Lcom/safonov/speedreading/training/fragment/interstitial/presenter/InterstitialPresenter;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/interstitial/presenter/InterstitialPresenter;-><init>()V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 236
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 237
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    if-eqz v0, :cond_0

    .line 238
    check-cast p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    .line 243
    return-void

    .line 240
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement InterstitialFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_index"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "course_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/TrainingType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->courseType:Lcom/safonov/speedreading/training/TrainingType;

    .line 74
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const v9, 0x7f0b0042

    const/4 v10, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v9, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 91
    .local v8, "view":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v9

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->unbinder:Lbutterknife/Unbinder;

    .line 93
    const/4 v7, 0x0

    .line 94
    .local v7, "titles":[Ljava/lang/String;
    sget-object v9, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$3;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->courseType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v10}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v10

    aget v9, v9, v10

    packed-switch v9, :pswitch_data_0

    .line 107
    :goto_0
    if-eqz v7, :cond_0

    .line 108
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f030015

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "icons":[Ljava/lang/String;
    const v9, 0x7f0b006f

    const/4 v10, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 112
    .local v5, "page":Landroid/view/View;
    const v9, 0x7f090154

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 113
    .local v3, "advantageTitleTV":Landroid/widget/TextView;
    move-object/from16 v0, p0

    iget v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    aget-object v9, v7, v9

    invoke-virtual {v3, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    const v9, 0x7f090153

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 116
    .local v2, "advantageImageView":Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    aget-object v10, v4, v10

    const-string v11, "drawable"

    .line 117
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v4/app/FragmentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v12

    .line 116
    invoke-virtual {v9, v10, v11, v12}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 118
    .local v6, "resID":I
    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 120
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainersLeft:Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e00ef

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    array-length v14, v7

    move-object/from16 v0, p0

    iget v15, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    sub-int/2addr v14, v15

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->progressBar:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 122
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->progressBar:Landroid/widget/ProgressBar;

    array-length v10, v7

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 124
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->pager:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 126
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v10, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getContext()Landroid/content/Context;

    move-result-object v11

    invoke-direct {v10, v11}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 127
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v10, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;

    move-object/from16 v0, p0

    iget v11, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainingIndex:I

    invoke-direct {v10, v7, v11}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;-><init>([Ljava/lang/String;I)V

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 128
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v10, 0x2

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setOverScrollMode(I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 132
    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    .end local v4    # "icons":[Ljava/lang/String;
    .end local v5    # "page":Landroid/view/View;
    .end local v6    # "resID":I
    :cond_0
    return-object v8

    .line 96
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->topIv:Landroid/widget/ImageView;

    const v10, 0x7f0800ff

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 97
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->titleTv:Landroid/widget/TextView;

    const v10, 0x7f0e001f

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const/high16 v10, 0x7f030000

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 99
    goto/16 :goto_0

    .line 101
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->topIv:Landroid/widget/ImageView;

    const v10, 0x7f080100

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 102
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->titleTv:Landroid/widget/TextView;

    const v10, 0x7f0e0035

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 103
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f030016

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_0

    .line 94
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 142
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 143
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 144
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 247
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 248
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    .line 249
    return-void
.end method

.method public onRestartClick()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090130
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 149
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e0093

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 150
    const v2, 0x7f0e0095

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 152
    const v2, 0x7f0e0094

    new-instance v3, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 160
    const v2, 0x7f0e00f2

    new-instance v3, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$2;-><init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 167
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 168
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 169
    return-void
.end method

.method public onStartButtonClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900ba
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;->onInterstitialFragmentListenerCompleted()V

    .line 138
    return-void
.end method
