.class final enum Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
.super Ljava/lang/Enum;
.source "FlashWordsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "BoardType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

.field public static final enum FIRST:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

.field public static final enum SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

.field public static final enum THIRD:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;


# instance fields
.field private final type:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 149
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    const-string v1, "FIRST"

    invoke-direct {v0, v1, v4, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->FIRST:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 150
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    const-string v1, "SECOND"

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 151
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    const-string v1, "THIRD"

    invoke-direct {v0, v1, v3, v5}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->THIRD:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 148
    new-array v0, v5, [Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    sget-object v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->FIRST:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->THIRD:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->$VALUES:[Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 155
    iput p3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->type:I

    .line 156
    return-void
.end method

.method public static crete(I)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 1
    .param p0, "type"    # I

    .prologue
    .line 173
    packed-switch p0, :pswitch_data_0

    .line 179
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 174
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->FIRST:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 175
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 176
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->THIRD:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 173
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static getNext(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 3
    .param p0, "boardType"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 194
    sget-object v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$4;->$SwitchMap$com$safonov$speedreading$training$fragment$flashword$training$presenter$FlashWordsPresenter$BoardType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 198
    :goto_0
    :pswitch_0
    return-object v0

    .line 195
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 196
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->THIRD:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public static getPrevious(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 3
    .param p0, "boardType"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 184
    sget-object v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$4;->$SwitchMap$com$safonov$speedreading$training$fragment$flashword$training$presenter$FlashWordsPresenter$BoardType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 188
    :goto_0
    :pswitch_0
    return-object v0

    .line 186
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->FIRST:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 187
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->SECOND:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    goto :goto_0

    .line 184
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 148
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 1

    .prologue
    .line 148
    sget-object v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->$VALUES:[Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    return-object v0
.end method


# virtual methods
.method public getBoardPartCount()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->type:I

    packed-switch v0, :pswitch_data_0

    .line 165
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->type:I

    :goto_0
    return v0

    .line 160
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 161
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 162
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 159
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->type:I

    return v0
.end method
