.class public Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SchulteTablePassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestTimeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090038
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field newBestTimeView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011b
    .end annotation
.end field

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field private schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090207
    .end annotation
.end field

.field private trainingResultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 45
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;-><init>()V

    .line 52
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getSchulteTableRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 41
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 124
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 125
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 130
    return-void

    .line 127
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->trainingResultId:I

    .line 64
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 77
    const v1, 0x7f0b00a4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 81
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;->close()V

    .line 142
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 144
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 145
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 152
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 135
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 136
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 116
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 117
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->trainingResultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;->initTrainingResults(I)V

    .line 88
    return-void
.end method

.method public setNewBestTimeViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 102
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->newBestTimeView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    return-void

    .line 102
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestTimeView(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->bestTimeTextView:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method

.method public updateTimeView(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->timeTextView:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-void
.end method
