.class public interface abstract Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;
.super Ljava/lang/Object;
.source "IRememberNumberPassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setNewBestScoreViewVisibility(Z)V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updatePassCoursePointsView(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
