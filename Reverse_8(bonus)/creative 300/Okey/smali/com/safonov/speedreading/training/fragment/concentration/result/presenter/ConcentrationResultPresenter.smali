.class public Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "ConcentrationResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/IConcentrationResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/IConcentrationResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    invoke-interface {v6, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v3

    .line 28
    .local v3, "result":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getConfig()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getConfig()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getResultList(I)Ljava/util/List;

    move-result-object v4

    .line 32
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;>;"
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getScore()I

    move-result v5

    .line 33
    .local v5, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getScore()I

    move-result v1

    .line 35
    .local v1, "bestScore":I
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getId()I

    move-result v6

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    const/4 v2, 0x1

    .line 37
    .local v2, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->isViewAttached()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;

    invoke-interface {v6, v5}, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;->updateScoreView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;->updateBestScoreView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;

    invoke-interface {v6, v2}, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;->setNewBestScoreViewVisibility(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/result/presenter/ConcentrationResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/concentration/result/view/IConcentrationResultView;->setChartViewData(Ljava/util/List;)V

    .line 43
    :cond_0
    return-void

    .line 35
    .end local v2    # "isNewBest":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
