.class public Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "TrueColorsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;"
    }
.end annotation


# static fields
.field public static final ANSWER_DURATION:I = 0x12c

.field public static final DEFAULT_INCORRECT_ANSWER_CORRECTION:I = 0x3e8

.field public static final DEFAULT_LEVEL_DURATION:I = 0x1388

.field public static final PRE_SHOW_DURATION:I = 0x0

.field public static final SHOW_COUNT:I = 0x32


# instance fields
.field answerColors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;"
        }
    .end annotation
.end field

.field private bestResult:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

.field colors:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;"
        }
    .end annotation
.end field

.field private config:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

.field private configId:I

.field currentColor:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

.field private handler:Landroid/os/Handler;

.field private repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

.field score:I

.field private showCorrectAnswer:Ljava/lang/Runnable;

.field showCount:I

.field private showIncorrectAnswer:Ljava/lang/Runnable;

.field private showWords:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V
    .locals 1
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 116
    iput v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    .line 117
    iput v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCount:I

    .line 119
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    .line 133
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    .line 145
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    .line 157
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$4;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showWords:Ljava/lang/Runnable;

    .line 37
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    .line 38
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showWords:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 80
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showWords:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    .line 84
    return-void
.end method

.method public finishTraining()V
    .locals 4

    .prologue
    .line 102
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;-><init>()V

    .line 103
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->setScore(I)V

    .line 105
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->addResult(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;ILcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V

    .line 114
    return-void
.end method

.method public getAnswers()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 70
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->colors:Ljava/util/List;

    invoke-static {v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 71
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->colors:Ljava/util/List;

    const/4 v2, 0x4

    invoke-interface {v1, v3, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 72
    .local v0, "answers":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;>;"
    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->currentColor:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    .line 73
    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 74
    return-object v0
.end method

.method public onNumberButtonPressed(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/lang/String;)V
    .locals 4
    .param p1, "rightAnswer"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;
    .param p2, "answerText"    # Ljava/lang/String;

    .prologue
    const-wide/16 v2, 0x0

    .line 123
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCount:I

    .line 125
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->getColorName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public pauseTraining()V
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->timerPause()V

    .line 89
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->disableButtons()V

    .line 90
    return-void
.end method

.method public resumeTraining()V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->timerStart()V

    .line 95
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->enableButtons()V

    .line 96
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getAnswers()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    .line 97
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->currentColor:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->showLevel(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/util/List;)V

    .line 98
    return-void
.end method

.method public startTraining(I)V
    .locals 4
    .param p1, "configId"    # I

    .prologue
    const/4 v2, 0x0

    .line 51
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->configId:I

    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->config:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->updateScoreView(I)V

    .line 56
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->updateBestScoreView(I)V

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    const/16 v1, 0x32

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->initProgressBar(I)V

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->updateProgressBar(I)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->getColors()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->colors:Ljava/util/List;

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getAnswers()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->currentColor:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->answerColors:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->showLevel(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/util/List;)V

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    const-wide/16 v2, 0x1388

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->initCountDownTimer(J)V

    .line 67
    return-void

    .line 56
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getScore()I

    move-result v1

    goto :goto_0
.end method
