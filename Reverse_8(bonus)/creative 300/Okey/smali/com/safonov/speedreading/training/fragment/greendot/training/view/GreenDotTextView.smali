.class public Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;
.super Landroid/support/v7/widget/AppCompatTextView;
.source "GreenDotTextView.java"


# static fields
.field private static final MAX_SWEEP_ANGLE:F = 360.0f

.field private static final START_ANGLE:F = -90.0f


# instance fields
.field private final MAX_PROGRESS:I

.field private dotRect:Landroid/graphics/RectF;

.field private frameColor:I

.field private greenDotFrameSize:F

.field private greenDotHeight:F

.field private greenDotWidth:F

.field private mainColor:I

.field private paint:Landroid/graphics/Paint;

.field private progress:I

.field private shadowColor:I

.field private sweepAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 41
    invoke-direct {p0, p1}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;)V

    .line 19
    const/16 v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    .line 20
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->MAX_PROGRESS:I

    .line 22
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    .line 24
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    .line 25
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->sweepAngle:F

    .line 32
    const v0, -0xffff01

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    .line 33
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    .line 35
    const v0, -0xff0100

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    .line 42
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->init(Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 46
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 19
    const/16 v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    .line 20
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->MAX_PROGRESS:I

    .line 22
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    .line 24
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    .line 25
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->sweepAngle:F

    .line 32
    const v0, -0xffff01

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    .line 33
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    .line 35
    const v0, -0xff0100

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->init(Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/AppCompatTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 19
    const/16 v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    .line 20
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->MAX_PROGRESS:I

    .line 22
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    .line 24
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    .line 25
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->sweepAngle:F

    .line 32
    const v0, -0xffff01

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    .line 33
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    .line 35
    const v0, -0xff0100

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    .line 52
    invoke-direct {p0, p2, p3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->init(Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method private calcSweepAngleFromProgress(I)F
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 94
    if-nez p1, :cond_0

    .line 95
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    .line 96
    :cond_0
    const/16 v0, 0x64

    if-ne p1, v0, :cond_1

    .line 97
    const/high16 v0, 0x43b40000    # 360.0f

    goto :goto_0

    .line 100
    :cond_1
    const v0, 0x40666666    # 3.6f

    int-to-float v1, p1

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method private drawFrame(Landroid/graphics/Canvas;)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v8, 0x0

    .line 151
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getWidth()I

    move-result v5

    .line 152
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getHeight()I

    move-result v3

    .line 154
    .local v3, "height":I
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setColor(I)V

    .line 155
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    sget-object v7, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 156
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 164
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v8, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 165
    .local v0, "a":Landroid/graphics/PointF;
    new-instance v1, Landroid/graphics/PointF;

    iget v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    invoke-direct {v1, v8, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 166
    .local v1, "b":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    iget v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    invoke-direct {v2, v6, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 168
    .local v2, "c":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 169
    .local v4, "path":Landroid/graphics/Path;
    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 170
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 171
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 172
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 173
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 174
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 178
    new-instance v0, Landroid/graphics/PointF;

    .end local v0    # "a":Landroid/graphics/PointF;
    int-to-float v6, v5

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    sub-float/2addr v6, v7

    invoke-direct {v0, v6, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 179
    .restart local v0    # "a":Landroid/graphics/PointF;
    new-instance v1, Landroid/graphics/PointF;

    .end local v1    # "b":Landroid/graphics/PointF;
    int-to-float v6, v5

    invoke-direct {v1, v6, v8}, Landroid/graphics/PointF;-><init>(FF)V

    .line 180
    .restart local v1    # "b":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    .end local v2    # "c":Landroid/graphics/PointF;
    int-to-float v6, v5

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    invoke-direct {v2, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 182
    .restart local v2    # "c":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "path":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 183
    .restart local v4    # "path":Landroid/graphics/Path;
    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 184
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 185
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 186
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 187
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 188
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 192
    new-instance v0, Landroid/graphics/PointF;

    .end local v0    # "a":Landroid/graphics/PointF;
    int-to-float v6, v3

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    sub-float/2addr v6, v7

    invoke-direct {v0, v8, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 193
    .restart local v0    # "a":Landroid/graphics/PointF;
    new-instance v1, Landroid/graphics/PointF;

    .end local v1    # "b":Landroid/graphics/PointF;
    int-to-float v6, v3

    invoke-direct {v1, v8, v6}, Landroid/graphics/PointF;-><init>(FF)V

    .line 194
    .restart local v1    # "b":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    .end local v2    # "c":Landroid/graphics/PointF;
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    int-to-float v7, v3

    invoke-direct {v2, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 196
    .restart local v2    # "c":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "path":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 197
    .restart local v4    # "path":Landroid/graphics/Path;
    sget-object v6, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 198
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 199
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 200
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 201
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 202
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 206
    new-instance v0, Landroid/graphics/PointF;

    .end local v0    # "a":Landroid/graphics/PointF;
    int-to-float v6, v5

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    sub-float/2addr v6, v7

    int-to-float v7, v3

    invoke-direct {v0, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 207
    .restart local v0    # "a":Landroid/graphics/PointF;
    new-instance v1, Landroid/graphics/PointF;

    .end local v1    # "b":Landroid/graphics/PointF;
    int-to-float v6, v5

    int-to-float v7, v3

    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    sub-float/2addr v7, v8

    invoke-direct {v1, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 208
    .restart local v1    # "b":Landroid/graphics/PointF;
    new-instance v2, Landroid/graphics/PointF;

    .end local v2    # "c":Landroid/graphics/PointF;
    int-to-float v6, v5

    int-to-float v7, v3

    invoke-direct {v2, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    .line 210
    .restart local v2    # "c":Landroid/graphics/PointF;
    new-instance v4, Landroid/graphics/Path;

    .end local v4    # "path":Landroid/graphics/Path;
    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    .line 211
    .restart local v4    # "path":Landroid/graphics/Path;
    sget-object v6, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    invoke-virtual {v4, v6}, Landroid/graphics/Path;->setFillType(Landroid/graphics/Path$FillType;)V

    .line 212
    iget v6, v1, Landroid/graphics/PointF;->x:F

    iget v7, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->moveTo(FF)V

    .line 213
    iget v6, v2, Landroid/graphics/PointF;->x:F

    iget v7, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 214
    iget v6, v0, Landroid/graphics/PointF;->x:F

    iget v7, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v4, v6, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 215
    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    .line 216
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v4, v6}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 217
    return-void
.end method

.method private drawMainOval(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 220
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 221
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 223
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->dotRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 224
    return-void
.end method

.method private drawShadowShape(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 228
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 230
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->dotRect:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->sweepAngle:F

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 231
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 56
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->paint:Landroid/graphics/Paint;

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/safonov/speedreading/R$styleable;->GreenDotTextView:[I

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 61
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x5

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    .line 64
    const/4 v1, 0x2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    .line 68
    const/4 v1, 0x3

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    .line 72
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotFrameSize:F

    .line 76
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->frameColor:I

    .line 80
    const/4 v1, 0x4

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->shadowColor:I

    .line 84
    const/4 v1, 0x6

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->mainColor:I

    .line 88
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 90
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->setProgress(I)V

    .line 91
    return-void
.end method

.method private initDotMeasurments()V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 122
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingLeft()I

    move-result v4

    .line 123
    .local v4, "paddingLeft":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingTop()I

    move-result v6

    .line 124
    .local v6, "paddingTop":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingRight()I

    move-result v5

    .line 125
    .local v5, "paddingRight":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingBottom()I

    move-result v3

    .line 128
    .local v3, "paddingBottom":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getWidth()I

    move-result v10

    sub-int/2addr v10, v4

    sub-int v9, v10, v5

    .line 129
    .local v9, "width":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getHeight()I

    move-result v10

    sub-int/2addr v10, v3

    sub-int v1, v10, v6

    .line 131
    .local v1, "height":I
    div-int/lit8 v10, v9, 0x2

    add-int/2addr v10, v4

    int-to-float v10, v10

    iget v11, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    div-float/2addr v11, v12

    sub-float v2, v10, v11

    .line 132
    .local v2, "left":F
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotWidth:F

    add-float v7, v2, v10

    .line 133
    .local v7, "right":F
    div-int/lit8 v10, v1, 0x2

    add-int/2addr v10, v6

    int-to-float v10, v10

    iget v11, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    div-float/2addr v11, v12

    sub-float v8, v10, v11

    .line 134
    .local v8, "top":F
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->greenDotHeight:F

    add-float v0, v8, v10

    .line 136
    .local v0, "bottom":F
    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v2, v8, v7, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v10, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->dotRect:Landroid/graphics/RectF;

    .line 137
    return-void
.end method


# virtual methods
.method public getProgress()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/support/v7/widget/AppCompatTextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 142
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->initDotMeasurments()V

    .line 146
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->drawMainOval(Landroid/graphics/Canvas;)V

    .line 147
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->drawShadowShape(Landroid/graphics/Canvas;)V

    .line 148
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    const/16 v0, 0x64

    .line 104
    if-gtz p1, :cond_0

    .line 105
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    .line 112
    :goto_0
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->calcSweepAngleFromProgress(I)F

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->sweepAngle:F

    .line 113
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->invalidate()V

    .line 114
    return-void

    .line 106
    :cond_0
    if-lt p1, v0, :cond_1

    .line 107
    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    goto :goto_0

    .line 109
    :cond_1
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->progress:I

    goto :goto_0
.end method
