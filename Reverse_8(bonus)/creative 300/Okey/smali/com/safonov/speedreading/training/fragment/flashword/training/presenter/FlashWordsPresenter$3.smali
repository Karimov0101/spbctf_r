.class Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;
.super Ljava/lang/Object;
.source "FlashWordsPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 284
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 287
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    :goto_0
    return-void

    .line 289
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->setItemsText(ILjava/util/List;)V

    .line 292
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$608(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I

    .line 293
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getBoardPartCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    .line 294
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$602(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)I

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
