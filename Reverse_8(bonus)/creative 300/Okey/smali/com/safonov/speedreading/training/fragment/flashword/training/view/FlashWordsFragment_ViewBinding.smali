.class public Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "FlashWordsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

.field private view2131296360:Landroid/view/View;

.field private view2131296531:Landroid/view/View;

.field private view2131296532:Landroid/view/View;

.field private view2131296682:Landroid/view/View;

.field private view2131296712:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v4, 0x7f090068

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

    .line 35
    const v1, 0x7f090022

    const-string v2, "field \'adView\'"

    const-class v3, Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 36
    const v1, 0x7f0901c7

    const-string v2, "field \'speedTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->speedTextView:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f090207

    const-string v2, "field \'timeTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->timeTextView:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f0900d7

    const-string v2, "field \'levelTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->levelTextView:Landroid/widget/TextView;

    .line 39
    const-string v1, "field \'contentView\' and method \'onContentViewClick\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'contentView\'"

    const-class v2, Landroid/view/ViewGroup;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    .line 41
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296360:Landroid/view/View;

    .line 42
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const v1, 0x7f0901aa

    const-string v2, "method \'onSpeedDownViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 49
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    const v1, 0x7f0901c8

    const-string v2, "method \'onSpeedUpViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 57
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 58
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const v1, 0x7f090113

    const-string v2, "method \'onMinusViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296531:Landroid/view/View;

    .line 66
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const v1, 0x7f090114

    const-string v2, "method \'onPlusViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 73
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296532:Landroid/view/View;

    .line 74
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$5;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

    .line 86
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 87
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->speedTextView:Landroid/widget/TextView;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->timeTextView:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->levelTextView:Landroid/widget/TextView;

    .line 93
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296360:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296360:Landroid/view/View;

    .line 97
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 99
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 101
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296531:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296531:Landroid/view/View;

    .line 103
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296532:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 104
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment_ViewBinding;->view2131296532:Landroid/view/View;

    .line 105
    return-void
.end method
