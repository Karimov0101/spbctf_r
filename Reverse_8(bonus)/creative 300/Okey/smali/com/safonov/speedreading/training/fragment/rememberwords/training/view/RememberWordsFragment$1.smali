.class Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;
.super Ljava/lang/Object;
.source "RememberWordsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 181
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->words:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 183
    const/4 v1, 0x0

    .local v1, "countRow":I
    const/4 v0, 0x0

    .line 184
    .local v0, "count":I
    const/4 v2, 0x1

    .line 186
    .local v2, "isNeedWordsForRequest":Z
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->words:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 187
    .local v3, "word":Ljava/lang/String;
    if-nez v1, :cond_1

    .line 188
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsRequest:Ljava/util/List;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, v7, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    invoke-static {v6, v7}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$000(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Landroid/widget/TableLayout;)Landroid/widget/TableRow;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    :cond_1
    if-eqz v2, :cond_2

    .line 192
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsRequest:Ljava/util/List;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, v7, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsRequest:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    invoke-static {v6, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$100(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/TextView;

    .line 196
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 197
    add-int/lit8 v0, v0, 0x1

    .line 199
    if-ne v1, v8, :cond_3

    .line 200
    const/4 v1, 0x0

    .line 202
    :cond_3
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$200(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I

    move-result v4

    if-ne v0, v4, :cond_4

    .line 203
    const/4 v2, 0x0

    .line 206
    :cond_4
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerList:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$200(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I

    move-result v4

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v6}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$200(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I

    move-result v6

    add-int/2addr v4, v6

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_0

    .line 210
    .end local v3    # "word":Ljava/lang/String;
    :cond_5
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerList:Ljava/util/List;

    invoke-static {v4}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 211
    const/4 v0, 0x0

    .line 212
    const/4 v1, 0x0

    .line 214
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 215
    .restart local v3    # "word":Ljava/lang/String;
    if-nez v1, :cond_7

    .line 216
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsAnswer:Ljava/util/List;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, v7, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->tableLayout:Landroid/widget/TableLayout;

    invoke-static {v6, v7}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$000(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Landroid/widget/TableLayout;)Landroid/widget/TableRow;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_7
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsAnswer:Ljava/util/List;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v7, v7, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsAnswer:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TableRow;

    invoke-static {v6, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$300(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/Button;

    .line 221
    add-int/lit8 v1, v1, 0x1

    .line 222
    add-int/lit8 v0, v0, 0x1

    .line 224
    if-ne v1, v8, :cond_6

    .line 225
    const/4 v1, 0x0

    goto :goto_0

    .line 229
    .end local v3    # "word":Ljava/lang/String;
    :cond_8
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$500(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Landroid/os/Handler;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v5}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$400(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 230
    return-void
.end method
