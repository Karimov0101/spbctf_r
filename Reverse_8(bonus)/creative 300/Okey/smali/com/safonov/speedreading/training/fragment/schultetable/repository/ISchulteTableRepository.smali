.class public interface abstract Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
.super Ljava/lang/Object;
.source "ISchulteTableRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;ILcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;"
        }
    .end annotation
.end method
