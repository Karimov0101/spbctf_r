.class public Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
.super Lio/realm/RealmObject;
.source "WordPairsResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/WordPairsResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->score:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->config:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->score:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V

    .line 59
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmSet$id(I)V

    .line 33
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmSet$score(I)V

    .line 41
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 50
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->realmSet$unixTime(J)V

    .line 51
    return-void
.end method
