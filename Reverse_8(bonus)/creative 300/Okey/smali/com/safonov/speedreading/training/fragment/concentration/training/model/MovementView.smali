.class public Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
.super Landroid/view/SurfaceView;
.source "MovementView.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field public static final CIRCLES_GRAY_RUN_TIME:I = 0x7d0

.field public static final CIRCLES_RUN_TIME:I = 0xfa0

.field public static final FPS:I = 0x3c

.field public static final ONE_TICK:I = 0xa

.field public static final SHOW_ANSWER:I = 0x5dc

.field public static final SHOW_ANSWER_DELAY:I = 0x96


# instance fields
.field private answersCount:I

.field private circlePaintBlue:Landroid/graphics/Paint;

.field private circlePaintGrey:Landroid/graphics/Paint;

.field private circlePaintRed:Landroid/graphics/Paint;

.field private circles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;",
            ">;"
        }
    .end annotation
.end field

.field private colorGreen:I

.field private colorRed:I

.field private config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

.field private correctAnswers:I

.field private correctGameCount:I

.field private correction:Z

.field private currentGameCount:I

.field private currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

.field private handler:Landroid/os/Handler;

.field private height:I

.field private inCorrectGameCount:I

.field private isAllGrey:Z

.field private isAnswered:Z

.field private isAuto:Z

.field private isLocked:Z

.field private isRun:Z

.field private repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

.field private restartLevel:Ljava/lang/Runnable;

.field private score:I

.field private showCorrectAnswer:Ljava/lang/Runnable;

.field private showInCorrectAnswer:Ljava/lang/Runnable;

.field private startNextLevel:Ljava/lang/Runnable;

.field private thread:Ljava/lang/Thread;

.field private time:J

.field private timerAll:Landroid/os/CountDownTimer;

.field private timerGray:Landroid/os/CountDownTimer;

.field private view:Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

.field private width:I

.field private wrongAnswers:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IIIZ)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "circlesCount"    # I
    .param p3, "circlesSpeed"    # I
    .param p4, "circlesRadius"    # I
    .param p5, "isPause"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 123
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorGreen:I

    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06006d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorRed:I

    .line 51
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    .line 52
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    .line 53
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    .line 54
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 55
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 56
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    .line 60
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    .line 61
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintBlue:Landroid/graphics/Paint;

    .line 62
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintRed:Landroid/graphics/Paint;

    .line 67
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 68
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isLocked:Z

    .line 69
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 70
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    .line 72
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    .line 81
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->score:I

    .line 375
    iput-boolean v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    .line 452
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showInCorrectAnswer:Ljava/lang/Runnable;

    .line 477
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->restartLevel:Ljava/lang/Runnable;

    .line 504
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$5;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$5;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startNextLevel:Ljava/lang/Runnable;

    .line 531
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$6;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$6;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showCorrectAnswer:Ljava/lang/Runnable;

    .line 125
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    const v1, -0x777778

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintBlue:Landroid/graphics/Paint;

    const v1, -0xffff01

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintRed:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorRed:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    div-int/lit8 v1, p2, 0x2

    invoke-direct {v0, p2, v1, p4, p3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;-><init>(IIII)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v0

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v2

    div-int/lit8 v2, v2, 0x5

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleRadius(I)V

    .line 135
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    .line 138
    :cond_0
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAuto:Z

    .line 139
    if-eqz p5, :cond_1

    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 140
    :goto_0
    return-void

    .line 139
    :cond_1
    iput-boolean v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;ILcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;Z)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
    .param p3, "configId"    # I
    .param p4, "view"    # Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;
    .param p5, "isPause"    # Z

    .prologue
    const/16 v8, 0xc

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 86
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f06001a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorGreen:I

    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f06006d

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorRed:I

    .line 51
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    .line 52
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    .line 53
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    .line 54
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 55
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 56
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    .line 60
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    .line 61
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintBlue:Landroid/graphics/Paint;

    .line 62
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintRed:Landroid/graphics/Paint;

    .line 67
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 68
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isLocked:Z

    .line 69
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 70
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    .line 72
    new-instance v1, Ljava/lang/Thread;

    invoke-direct {v1, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    .line 73
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    .line 81
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->score:I

    .line 375
    iput-boolean v7, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    .line 452
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showInCorrectAnswer:Ljava/lang/Runnable;

    .line 477
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->restartLevel:Ljava/lang/Runnable;

    .line 504
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$5;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$5;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startNextLevel:Ljava/lang/Runnable;

    .line 531
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$6;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$6;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showCorrectAnswer:Ljava/lang/Runnable;

    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 90
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    .line 91
    invoke-interface {p2, p3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    const v3, -0x777778

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintBlue:Landroid/graphics/Paint;

    const v3, -0xffff01

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintRed:Landroid/graphics/Paint;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorRed:I

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 97
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesCount()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesCount()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v5

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeed()I

    move-result v6

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;-><init>(IIII)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    .line 99
    const-string v1, "count"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    invoke-interface {p2, p3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v0

    .line 103
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    const/16 v1, 0x14

    invoke-interface {p4, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->initProgressBar(I)V

    .line 104
    invoke-interface {p4, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->updateScoreView(I)V

    .line 105
    invoke-interface {p4, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->updateProgressBar(I)V

    .line 106
    if-nez v0, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {p4, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->updateBestScoreView(I)V

    .line 108
    iput-object p4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->view:Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    .line 110
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v1

    if-lt v1, v8, :cond_2

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    if-eqz v1, :cond_2

    .line 111
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v4

    div-int/lit8 v4, v4, 0x5

    sub-int/2addr v3, v4

    invoke-virtual {v1, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleRadius(I)V

    .line 112
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    .line 118
    :cond_0
    :goto_1
    iput-boolean v7, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAuto:Z

    .line 119
    if-eqz p5, :cond_3

    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 120
    :goto_2
    return-void

    .line 106
    :cond_1
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->getScore()I

    move-result v1

    goto :goto_0

    .line 114
    :cond_2
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v1

    if-ge v1, v8, :cond_0

    .line 115
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleRadius(I)V

    goto :goto_1

    .line 119
    :cond_3
    iput-boolean v7, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    goto :goto_2
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showAllGrey()V

    return-void
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$102(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    return p1
.end method

.method static synthetic access$1102(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isLocked:Z

    return p1
.end method

.method static synthetic access$1200(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    return v0
.end method

.method static synthetic access$1208(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    return v0
.end method

.method static synthetic access$1300(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getNextLevel(Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startNextLevel:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$202(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    return p1
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->colorGreen:I

    return v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->drawToScreen()V

    return-void
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAuto:Z

    return v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    return v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->view:Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    return-object v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->restartLevel:Ljava/lang/Runnable;

    return-object v0
.end method

.method private checkTouchCircles(DD)V
    .locals 5
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 430
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 431
    .local v0, "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isPointIn(DD)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getAnswerPaint()Landroid/graphics/Paint;

    move-result-object v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isPointIn(DD)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getAnswerPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    const v3, -0x777778

    if-ne v2, v3, :cond_4

    .line 432
    :cond_2
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    .line 433
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Paint;->getColor()I

    move-result v2

    const v3, -0xffff01

    if-ne v2, v3, :cond_3

    .line 434
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPaint()Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setAnswerPaint(Landroid/graphics/Paint;)V

    .line 435
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    .line 436
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getScore()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setScore(I)V

    goto :goto_0

    .line 438
    :cond_3
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintRed:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setAnswerPaint(Landroid/graphics/Paint;)V

    .line 439
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    goto :goto_0

    .line 442
    :cond_4
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getAnswerPaint()Landroid/graphics/Paint;

    move-result-object v2

    if-nez v2, :cond_0

    .line 443
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setAnswerPaint(Landroid/graphics/Paint;)V

    goto :goto_0

    .line 448
    .end local v0    # "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_5
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 449
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    .line 450
    return-void
.end method

.method private createCircle(IILandroid/graphics/Paint;Ljava/util/Random;)Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    .locals 6
    .param p1, "radius"    # I
    .param p2, "speed"    # I
    .param p3, "paint"    # Landroid/graphics/Paint;
    .param p4, "random"    # Ljava/util/Random;

    .prologue
    .line 330
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;-><init>()V

    .line 332
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setRadius(I)V

    .line 333
    new-instance v1, Ljavax/vecmath/Vector2d;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->width:I

    invoke-virtual {p4, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/2addr v2, p1

    int-to-double v2, v2

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->height:I

    invoke-virtual {p4, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    add-int/2addr v4, p1

    int-to-double v4, v4

    invoke-direct {v1, v2, v3, v4, v5}, Ljavax/vecmath/Vector2d;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setPosition(Ljavax/vecmath/Vector2d;)V

    .line 334
    new-instance v1, Ljavax/vecmath/Vector2d;

    int-to-double v2, p2

    int-to-double v4, p2

    invoke-direct {v1, v2, v3, v4, v5}, Ljavax/vecmath/Vector2d;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setSpeed(Ljavax/vecmath/Vector2d;)V

    .line 335
    invoke-virtual {v0, p3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setPaint(Landroid/graphics/Paint;)V

    .line 337
    return-object v0
.end method

.method private drawToScreen()V
    .locals 3

    .prologue
    .line 247
    const/4 v0, 0x0

    .line 249
    .local v0, "canvas":Landroid/graphics/Canvas;
    :try_start_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 252
    if-eqz v0, :cond_0

    .line 253
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->draw(Landroid/graphics/Canvas;)V

    .line 254
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    .line 258
    :cond_0
    return-void

    .line 252
    :catchall_0
    move-exception v1

    if-eqz v0, :cond_1

    .line 253
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->draw(Landroid/graphics/Canvas;)V

    .line 254
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    invoke-interface {v2, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_1
    throw v1
.end method

.method private getNextLevel(Z)V
    .locals 7
    .param p1, "isCorrectAnswered"    # Z

    .prologue
    const/4 v2, 0x3

    const/16 v6, 0xc

    const/4 v5, 0x0

    .line 378
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAuto:Z

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->view:Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentGameCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->updateProgressBar(I)V

    .line 380
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->view:Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getScore()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->updateScoreView(I)V

    .line 382
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v0

    if-ge v0, v6, :cond_2

    .line 383
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleSpeed()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleSpeed(I)V

    .line 389
    :goto_0
    if-eqz p1, :cond_3

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    if-ne v0, v2, :cond_3

    .line 390
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeed()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleSpeed(I)V

    .line 391
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleCount(I)V

    .line 392
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCountTrue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleCountTrue(I)V

    .line 393
    iput v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 394
    iput v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 395
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleSpeed()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->updateConfig(IIII)V

    .line 405
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v0

    if-lt v0, v6, :cond_4

    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    if-eqz v0, :cond_4

    .line 406
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v2

    div-int/lit8 v2, v2, 0x5

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleRadius(I)V

    .line 407
    iput-boolean v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correction:Z

    .line 413
    :cond_1
    :goto_2
    return-void

    .line 386
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeed()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleSpeed(I)V

    goto :goto_0

    .line 396
    :cond_3
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v0

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 397
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeed()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleSpeed(I)V

    .line 398
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleCount(I)V

    .line 399
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCountTrue()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleCountTrue(I)V

    .line 400
    iput v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 401
    iput v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 402
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleSpeed()I

    move-result v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->updateConfig(IIII)V

    goto/16 :goto_1

    .line 409
    :cond_4
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v0

    if-ge v0, v6, :cond_1

    .line 410
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesRadius()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->setCircleRadius(I)V

    goto :goto_2
.end method

.method private initCircles(Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;)Ljava/util/List;
    .locals 11
    .param p1, "level"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 292
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 293
    .local v5, "result":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;>;"
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 295
    .local v4, "random":Ljava/util/Random;
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCountTrue()I

    move-result v0

    .line 296
    .local v0, "bufferBlue":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCount()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 297
    if-lez v0, :cond_0

    .line 298
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v8

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleSpeed()I

    move-result v9

    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintBlue:Landroid/graphics/Paint;

    invoke-direct {p0, v8, v9, v10, v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->createCircle(IILandroid/graphics/Paint;Ljava/util/Random;)Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    add-int/lit8 v0, v0, -0x1

    .line 296
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 302
    :cond_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleRadius()I

    move-result v8

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleSpeed()I

    move-result v9

    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    invoke-direct {p0, v8, v9, v10, v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->createCircle(IILandroid/graphics/Paint;Ljava/util/Random;)Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    move-result-object v8

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 306
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    .line 308
    .local v7, "size":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_5

    .line 309
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 311
    .local v1, "firstCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    if-ge v3, v7, :cond_4

    .line 313
    if-ne v2, v3, :cond_3

    .line 311
    :cond_2
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 316
    :cond_3
    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 318
    .local v6, "secondCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v1, v6}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isCollisionWithCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 319
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->width:I

    iget v9, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->height:I

    invoke-virtual {v1, v6, v8, v9}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setPosOutCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;II)V

    goto :goto_4

    .line 323
    .end local v6    # "secondCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_4
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->width:I

    iget v9, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->height:I

    invoke-virtual {v1, v8, v9}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->collisionWithWalls(II)Z

    .line 308
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 326
    .end local v1    # "firstCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    .end local v3    # "j":I
    :cond_5
    return-object v5
.end method

.method private showAllGrey()V
    .locals 2

    .prologue
    .line 212
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 213
    const-wide/16 v0, 0x7d0

    invoke-direct {p0, v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startTimerStop(J)V

    .line 214
    return-void
.end method

.method private startTimerStop(J)V
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 217
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$2;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$2;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;JJ)V

    .line 225
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$2;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerGray:Landroid/os/CountDownTimer;

    .line 226
    return-void
.end method

.method private startTimerToAllGrey(J)V
    .locals 7
    .param p1, "time"    # J

    .prologue
    .line 200
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$1;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$1;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;JJ)V

    .line 208
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$1;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerAll:Landroid/os/CountDownTimer;

    .line 209
    return-void
.end method


# virtual methods
.method public cancelGame()V
    .locals 4

    .prologue
    .line 264
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 266
    const/4 v1, 0x1

    .line 267
    .local v1, "retry":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 269
    :try_start_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 270
    const/4 v1, 0x0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    .line 272
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 276
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 277
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showInCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 278
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->restartLevel:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 279
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startNextLevel:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 281
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerAll:Landroid/os/CountDownTimer;

    if-eqz v2, :cond_1

    .line 282
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerAll:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 283
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerGray:Landroid/os/CountDownTimer;

    if-eqz v2, :cond_2

    .line 284
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->timerGray:Landroid/os/CountDownTimer;

    invoke-virtual {v2}, Landroid/os/CountDownTimer;->cancel()V

    .line 285
    :cond_2
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 144
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->draw(Landroid/graphics/Canvas;)V

    .line 146
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06002e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 147
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 148
    .local v0, "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    iget-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circlePaintGrey:Landroid/graphics/Paint;

    invoke-virtual {v0, p1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 151
    :cond_0
    iget-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    if-eqz v2, :cond_1

    .line 152
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->drawAnswered(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 154
    :cond_1
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 157
    .end local v0    # "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const-wide/16 v8, 0x96

    const/4 v6, 0x0

    .line 342
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    .line 343
    iget-boolean v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    if-nez v4, :cond_1

    iget-boolean v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isLocked:Z

    if-nez v4, :cond_1

    .line 344
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->performClick()Z

    .line 345
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    float-to-double v0, v4

    .line 346
    .local v0, "x":D
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-double v2, v4

    .line 348
    .local v2, "y":D
    invoke-direct {p0, v0, v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->checkTouchCircles(DD)V

    .line 349
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->drawToScreen()V

    .line 351
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCountTrue()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 352
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isLocked:Z

    .line 353
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getCircleCountTrue()I

    move-result v5

    if-ne v4, v5, :cond_0

    .line 354
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 355
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 356
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 359
    :cond_0
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    if-lez v4, :cond_1

    .line 360
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->handler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->showInCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v4, v5, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 361
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctGameCount:I

    .line 362
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->inCorrectGameCount:I

    .line 367
    .end local v0    # "x":D
    .end local v2    # "y":D
    :cond_1
    return v6
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 372
    invoke-super {p0}, Landroid/view/SurfaceView;->performClick()Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 6

    .prologue
    .line 556
    :goto_0
    iget-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    if-eqz v2, :cond_1

    .line 557
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 559
    .local v0, "cTime":J
    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->time:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x10

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    .line 560
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->updatePhysics()V

    .line 561
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->drawToScreen()V

    .line 564
    :cond_0
    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->time:J

    goto :goto_0

    .line 566
    .end local v0    # "cTime":J
    :cond_1
    return-void
.end method

.method public startLevel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 417
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->initCircles(Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    .line 419
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 420
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->wrongAnswers:I

    .line 421
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->correctAnswers:I

    .line 422
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->answersCount:I

    .line 423
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAnswered:Z

    .line 424
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 426
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startThread()V

    .line 427
    return-void
.end method

.method public startThread()V
    .locals 2

    .prologue
    .line 193
    const-wide/16 v0, 0xfa0

    invoke-direct {p0, v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startTimerToAllGrey(J)V

    .line 195
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    .line 196
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 197
    return-void
.end method

.method public stopGame()V
    .locals 3

    .prologue
    .line 229
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isAllGrey:Z

    .line 230
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->isRun:Z

    .line 232
    const/4 v1, 0x1

    .line 234
    .local v1, "retry":Z
    :goto_0
    if-eqz v1, :cond_0

    .line 236
    :try_start_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->thread:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 237
    const/4 v1, 0x0

    goto :goto_0

    .line 238
    :catch_0
    move-exception v0

    .line 239
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 243
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->drawToScreen()V

    .line 244
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 261
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 182
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v0

    .line 183
    .local v0, "surfaceFrame":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->width:I

    .line 184
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->height:I

    .line 186
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->currentLevel:Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->initCircles(Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    .line 188
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->drawToScreen()V

    .line 189
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startThread()V

    .line 190
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 288
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->cancelGame()V

    .line 289
    return-void
.end method

.method public updatePhysics()V
    .locals 8

    .prologue
    .line 160
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v5

    .line 162
    .local v5, "size":I
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 163
    .local v0, "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setNextPos()V

    goto :goto_0

    .line 166
    .end local v0    # "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v5, :cond_3

    .line 167
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 169
    .local v1, "firstCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    add-int/lit8 v3, v2, 0x1

    .local v3, "j":I
    :goto_2
    if-ge v3, v5, :cond_2

    .line 170
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->circles:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 172
    .local v4, "secondCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v1, v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isCollisionWithCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 173
    invoke-virtual {v1, v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->collisionWithCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)V

    .line 169
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 177
    .end local v4    # "secondCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_2
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->width:I

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->height:I

    invoke-virtual {v1, v6, v7}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->collisionWithWalls(II)Z

    .line 166
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 179
    .end local v1    # "firstCircle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    .end local v3    # "j":I
    :cond_3
    return-void
.end method
