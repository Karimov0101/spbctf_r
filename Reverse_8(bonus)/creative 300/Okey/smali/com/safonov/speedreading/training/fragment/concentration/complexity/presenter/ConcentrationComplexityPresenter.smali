.class public Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "ConcentrationComplexityPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;"
    }
.end annotation


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

.field private configId:I

.field private repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    .line 21
    return-void
.end method


# virtual methods
.method public bridge synthetic attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->attachView(Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;)V

    return-void
.end method

.method public attachView(Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;)V
    .locals 0
    .param p1, "view"    # Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;

    .prologue
    .line 26
    return-void
.end method

.method public detachView(Z)V
    .locals 0
    .param p1, "retainInstance"    # Z

    .prologue
    .line 31
    return-void
.end method

.method public initConfig(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->configId:I

    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 42
    return-void
.end method

.method public setComplexity(I)V
    .locals 2
    .param p1, "complexity"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;->configId:I

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->updateConfigComplexity(II)V

    .line 36
    return-void
.end method
