.class public Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
.super Lio/realm/RealmObject;
.source "SchulteTableResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/SchulteTableResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_TIME:Ljava/lang/String; = "time"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private time:J

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getTime()J
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmGet$time()J

    move-result-wide v0

    return-wide v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->id:I

    return v0
.end method

.method public realmGet$time()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->time:J

    return-wide v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->id:I

    return-void
.end method

.method public realmSet$time(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->time:J

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .prologue
    .line 56
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;)V

    .line 57
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmSet$id(I)V

    .line 31
    return-void
.end method

.method public setTime(J)V
    .locals 1
    .param p1, "time"    # J

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmSet$time(J)V

    .line 39
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 48
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->realmSet$unixTime(J)V

    .line 49
    return-void
.end method
