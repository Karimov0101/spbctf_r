.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
.super Ljava/lang/Object;
.source "ILineOfSightRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ILcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;"
        }
    .end annotation
.end method
