.class Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "InterstitialFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "InterstitialAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private selectedIndex:I

.field private titles:[Ljava/lang/String;


# direct methods
.method public constructor <init>([Ljava/lang/String;I)V
    .locals 0
    .param p1, "titles"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "selectedIndex"    # I

    .prologue
    .line 186
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 187
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->titles:[Ljava/lang/String;

    .line 188
    iput p2, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->selectedIndex:I

    .line 189
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->titles:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 171
    check-cast p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->onBindViewHolder(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;I)V
    .locals 6
    .param p1, "holder"    # Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x0

    .line 201
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->getItemCount()I

    move-result v4

    add-int/lit8 v2, v4, -0x1

    .line 203
    .local v2, "lastIndex":I
    if-nez p2, :cond_1

    .line 204
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->selectedIndex:I

    if-nez v4, :cond_0

    const v0, 0x7f0800fa

    .line 208
    .local v0, "firstItemResource":I
    :goto_0
    iget-object v4, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;->itemTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v0, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 223
    .end local v0    # "firstItemResource":I
    :goto_1
    iget-object v4, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;->itemTextView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->titles:[Ljava/lang/String;

    aget-object v5, v5, p2

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    return-void

    .line 204
    :cond_0
    const v0, 0x7f0800f9

    goto :goto_0

    .line 209
    :cond_1
    if-ne p2, v2, :cond_3

    .line 210
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->selectedIndex:I

    if-ne v4, v2, :cond_2

    const v3, 0x7f0800fd

    .line 214
    .local v3, "lastItemResource":I
    :goto_2
    iget-object v4, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;->itemTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v3, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    .line 210
    .end local v3    # "lastItemResource":I
    :cond_2
    const v3, 0x7f0800fc

    goto :goto_2

    .line 216
    :cond_3
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->selectedIndex:I

    if-ne v4, p2, :cond_4

    const v1, 0x7f0800fe

    .line 220
    .local v1, "itemResource":I
    :goto_3
    iget-object v4, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;->itemTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v1, v5, v5, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_1

    .line 216
    .end local v1    # "itemResource":I
    :cond_4
    const v1, 0x7f0800fb

    goto :goto_3
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 193
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0043

    const/4 v3, 0x0

    .line 194
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 196
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment$InterstitialAdapter;Landroid/view/View;)V

    return-object v1
.end method
