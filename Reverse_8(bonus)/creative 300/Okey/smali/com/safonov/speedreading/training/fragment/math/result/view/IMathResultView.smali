.class public interface abstract Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;
.super Ljava/lang/Object;
.source "IMathResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setNewBestScoreViewVisibility(Z)V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
