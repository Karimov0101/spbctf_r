.class Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;
.super Ljava/lang/Object;
.source "WordPairsGenerator.java"


# static fields
.field private static random:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->random:Ljava/util/Random;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static containsInArray([II)Z
    .locals 4
    .param p0, "array"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "item"    # I

    .prologue
    const/4 v1, 0x0

    .line 90
    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p0, v2

    .line 91
    .local v0, "arrayItem":I
    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    .line 93
    .end local v0    # "arrayItem":I
    :cond_0
    return v1

    .line 90
    .restart local v0    # "arrayItem":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method static createWordPairs([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;II)Ljava/util/List;
    .locals 15
    .param p0, "words"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "firstWords"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "secondWords"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "wordPairsCount"    # I
    .param p4, "differentWordPairsCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            "II)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 24
    move/from16 v0, p4

    move/from16 v1, p3

    if-le v0, v1, :cond_0

    .line 25
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "differentWordPairsCount must be no more than wordPairsCount"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 27
    :cond_0
    array-length v13, p0

    sub-int v14, p3, p4

    if-ge v13, v14, :cond_1

    .line 28
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "words array must not be no less than wordPairsCount - differentWordPairsCount"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 30
    :cond_1
    move-object/from16 v0, p1

    array-length v13, v0

    move/from16 v0, p4

    if-ge v13, v0, :cond_2

    .line 31
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "firstWords array must be more than differentWordPairsCount"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 33
    :cond_2
    move-object/from16 v0, p2

    array-length v13, v0

    move/from16 v0, p4

    if-ge v13, v0, :cond_3

    .line 34
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "secondWords array must be more than differentWordPairsCount"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 36
    :cond_3
    move-object/from16 v0, p1

    array-length v13, v0

    move-object/from16 v0, p2

    array-length v14, v0

    if-eq v13, v14, :cond_4

    .line 37
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "firstWords array must have same length as secondWords"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 40
    :cond_4
    array-length v9, p0

    .line 41
    .local v9, "length":I
    sub-int v5, p3, p4

    .line 42
    .local v5, "equalPairsCount":I
    invoke-static {v9, v5}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->getUniqueIndexes(II)[I

    move-result-object v6

    .line 44
    .local v6, "equalWordPairsIndexes":[I
    move-object/from16 v0, p1

    array-length v4, v0

    .line 45
    .local v4, "differentWordsCount":I
    invoke-static {v4, v4}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->getUniqueIndexes(II)[I

    move-result-object v3

    .line 47
    .local v3, "differentWordIndexes":[I
    new-instance v12, Ljava/util/ArrayList;

    move/from16 v0, p3

    invoke-direct {v12, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    .local v12, "wordPairs":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;>;"
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v5, :cond_5

    .line 49
    aget v2, v6, v8

    .line 51
    .local v2, "currentIndex":I
    aget-object v11, p0, v2

    .line 53
    .local v11, "word":Ljava/lang/String;
    new-instance v13, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;

    invoke-direct {v13, v11, v11}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 56
    .end local v2    # "currentIndex":I
    .end local v11    # "word":Ljava/lang/String;
    :cond_5
    const/4 v8, 0x0

    :goto_1
    move/from16 v0, p4

    if-ge v8, v0, :cond_6

    .line 57
    aget v2, v3, v8

    .line 59
    .restart local v2    # "currentIndex":I
    aget-object v7, p1, v2

    .line 60
    .local v7, "firstWord":Ljava/lang/String;
    aget-object v10, p2, v2

    .line 62
    .local v10, "secondWord":Ljava/lang/String;
    new-instance v13, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;

    invoke-direct {v13, v7, v10}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 65
    .end local v2    # "currentIndex":I
    .end local v7    # "firstWord":Ljava/lang/String;
    .end local v10    # "secondWord":Ljava/lang/String;
    :cond_6
    invoke-static {v12}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 67
    return-object v12
.end method

.method private static getUniqueIndexes(II)[I
    .locals 4
    .param p0, "length"    # I
    .param p1, "uniqueIndexCount"    # I

    .prologue
    .line 71
    new-array v2, p1, [I

    .line 73
    .local v2, "result":[I
    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 75
    const/4 v0, 0x0

    .line 76
    .local v0, "index":I
    :cond_0
    :goto_0
    if-ge v0, p1, :cond_1

    .line 78
    sget-object v3, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->random:Ljava/util/Random;

    invoke-virtual {v3, p0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 79
    .local v1, "randomIndex":I
    invoke-static {v2, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->containsInArray([II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 80
    aput v1, v2, v0

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    .end local v1    # "randomIndex":I
    :cond_1
    return-object v2
.end method
