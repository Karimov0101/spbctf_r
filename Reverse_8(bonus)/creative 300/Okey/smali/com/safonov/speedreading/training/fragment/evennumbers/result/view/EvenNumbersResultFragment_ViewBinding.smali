.class public Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment_ViewBinding;
.super Ljava/lang/Object;
.source "EvenNumbersResultFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;

    .line 26
    const v2, 0x7f090175

    const-string v3, "field \'scoreTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->scoreTextView:Landroid/widget/TextView;

    .line 27
    const v2, 0x7f090036

    const-string v3, "field \'bestScoreTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    .line 28
    const v2, 0x7f09011a

    const-string v3, "field \'newBestScoreView\'"

    const-class v4, Landroid/widget/ImageView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    .line 29
    const v2, 0x7f09008a

    const-string v3, "field \'lineChart\'"

    const-class v4, Lcom/github/mikephil/charting/charts/LineChart;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/github/mikephil/charting/charts/LineChart;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    .line 31
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 32
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 33
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f060070

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartLineColor:I

    .line 34
    const v2, 0x7f06006e

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemColor:I

    .line 35
    const v2, 0x7f0a000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemCircleRadiusDp:I

    .line 36
    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemValueTextSizeDp:I

    .line 37
    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartLineWidthDp:I

    .line 38
    const v2, 0x7f0e01ac

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->scoreTitle:Ljava/lang/String;

    .line 39
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;

    .line 45
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 46
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;

    .line 48
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->scoreTextView:Landroid/widget/TextView;

    .line 49
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    .line 50
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    .line 51
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    .line 52
    return-void
.end method
