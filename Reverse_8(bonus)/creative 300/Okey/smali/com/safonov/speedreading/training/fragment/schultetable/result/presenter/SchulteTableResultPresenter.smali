.class public Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SchulteTableResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-interface {v4, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v2

    .line 28
    .local v2, "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getConfig()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getId()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getId()I

    move-result v4

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getId()I

    move-result v5

    if-ne v4, v5, :cond_1

    const/4 v1, 0x1

    .line 32
    .local v1, "isNewBest":Z
    :goto_0
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getConfig()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v5

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getId()I

    move-result v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getResultList(I)Ljava/util/List;

    move-result-object v3

    .line 34
    .local v3, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->isViewAttached()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 35
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;->updateTimeView(J)V

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;->updateBestTimeView(J)V

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;

    invoke-interface {v4, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;->setNewBestTimeViewVisibility(Z)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;

    invoke-static {v3}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;->setData(Ljava/util/List;)V

    .line 40
    :cond_0
    return-void

    .line 30
    .end local v1    # "isNewBest":Z
    .end local v3    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
