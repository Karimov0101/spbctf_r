.class public Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SchulteTableFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final BIG_BOARD_ITEM_TEXT_SIZE_SP:I = 0x1a

.field public static final NON_INITIALIZATION_VALUE:Ljava/lang/String; = "-"

.field private static final SMALL_BOARD_ITEM_TEXT_SIZE_SP:I = 0x24

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field backgroundWhiteColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001f
    .end annotation
.end field

.field bestTimeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09017f
    .end annotation
.end field

.field private configId:I

.field greenColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field gridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090180
    .end annotation
.end field

.field private itemCount:I

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field itemMargin:I
    .annotation build Lbutterknife/BindDimen;
        value = 0x7f0700a6
    .end annotation
.end field

.field private itemOnTouchListener:Landroid/view/View$OnTouchListener;

.field nextItemTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090181
    .end annotation
.end field

.field redColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006d
    .end annotation
.end field

.field private schulteTableModel:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/SchulteTableModel;

.field private schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

.field statisticsPanel:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090182
    .end annotation
.end field

.field tableEdgeColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060074
    .end annotation
.end field

.field textColorBlack:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field textColorWhite:I
    .annotation build Lbutterknife/BindColor;
        value = 0x106000b
    .end annotation
.end field

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090187
    .end annotation
.end field

.field private trainingFragmentListener:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 205
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getItemIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method private getItemIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 198
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemCount:I

    if-ge v0, v1, :cond_1

    .line 199
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 203
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 198
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private initGridLayout(IIZ)V
    .locals 12
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I
    .param p3, "isFullScreen"    # Z

    .prologue
    .line 139
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9}, Landroid/support/v7/widget/GridLayout;->removeAllViews()V

    .line 140
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    iget v10, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->tableEdgeColor:I

    invoke-virtual {v9, v10}, Landroid/support/v7/widget/GridLayout;->setBackgroundColor(I)V

    .line 141
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9, p1}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 142
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9, p2}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 144
    if-eqz p3, :cond_3

    .line 145
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9}, Landroid/support/v7/widget/GridLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    const/4 v10, 0x0

    iput-object v10, v9, Landroid/support/constraint/ConstraintLayout$LayoutParams;->dimensionRatio:Ljava/lang/String;

    .line 151
    :goto_0
    const/4 v9, 0x6

    if-gt p1, v9, :cond_0

    const/4 v9, 0x6

    if-le p2, v9, :cond_4

    .line 152
    :cond_0
    const/16 v7, 0x1a

    .line 157
    .local v7, "textSizeSp":I
    :goto_1
    new-instance v9, Ljava/util/ArrayList;

    mul-int v10, p1, p2

    invoke-direct {v9, v10}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    .line 159
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, p1, :cond_6

    .line 160
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_3
    if-ge v3, p2, :cond_5

    .line 161
    new-instance v2, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v2, v9}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 163
    .local v2, "item":Landroid/widget/TextView;
    new-instance v4, Landroid/support/v7/widget/GridLayout$LayoutParams;

    const/high16 v9, -0x80000000

    const/high16 v10, 0x3f800000    # 1.0f

    .line 164
    invoke-static {v9, v10}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v9

    const/high16 v10, -0x80000000

    const/high16 v11, 0x3f800000    # 1.0f

    .line 165
    invoke-static {v10, v11}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v10

    invoke-direct {v4, v9, v10}, Landroid/support/v7/widget/GridLayout$LayoutParams;-><init>(Landroid/support/v7/widget/GridLayout$Spec;Landroid/support/v7/widget/GridLayout$Spec;)V

    .line 167
    .local v4, "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemMargin:I

    .line 168
    .local v8, "topMargin":I
    const/4 v0, 0x0

    .line 169
    .local v0, "bottomMargin":I
    iget v5, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemMargin:I

    .line 170
    .local v5, "leftMargin":I
    const/4 v6, 0x0

    .line 172
    .local v6, "rightMargin":I
    add-int/lit8 v9, p1, -0x1

    if-ne v1, v9, :cond_1

    .line 173
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemMargin:I

    .line 175
    :cond_1
    add-int/lit8 v9, p2, -0x1

    if-ne v3, v9, :cond_2

    .line 176
    iget v6, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemMargin:I

    .line 179
    :cond_2
    invoke-virtual {v4, v5, v8, v6, v0}, Landroid/support/v7/widget/GridLayout$LayoutParams;->setMargins(IIII)V

    .line 182
    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 183
    const/16 v9, 0x11

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setGravity(I)V

    .line 184
    iget v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorBlack:I

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setTextColor(I)V

    .line 185
    const/4 v9, 0x2

    int-to-float v10, v7

    invoke-virtual {v2, v9, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 186
    iget v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->backgroundWhiteColor:I

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 187
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v9}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 189
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9, v2}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 160
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 147
    .end local v0    # "bottomMargin":I
    .end local v1    # "i":I
    .end local v2    # "item":Landroid/widget/TextView;
    .end local v3    # "j":I
    .end local v4    # "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    .end local v5    # "leftMargin":I
    .end local v6    # "rightMargin":I
    .end local v7    # "textSizeSp":I
    .end local v8    # "topMargin":I
    :cond_3
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v9}, Landroid/support/v7/widget/GridLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/support/constraint/ConstraintLayout$LayoutParams;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ":"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, Landroid/support/constraint/ConstraintLayout$LayoutParams;->dimensionRatio:Ljava/lang/String;

    goto/16 :goto_0

    .line 154
    :cond_4
    const/16 v7, 0x24

    .restart local v7    # "textSizeSp":I
    goto/16 :goto_1

    .line 159
    .restart local v1    # "i":I
    .restart local v3    # "j":I
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    .line 194
    .end local v3    # "j":I
    :cond_6
    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v9, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    invoke-interface {v9}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;->startTraining()V

    .line 195
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 71
    new-instance v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;-><init>()V

    .line 72
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 73
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 74
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->setArguments(Landroid/os/Bundle;)V

    .line 75
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getSchulteTableRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 51
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/model/SchulteTableModel;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/model/SchulteTableModel;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->schulteTableModel:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/SchulteTableModel;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->schulteTableModel:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/SchulteTableModel;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V

    return-object v0
.end method

.method public initBoard(IIZ)V
    .locals 2
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I
    .param p3, "isFullscreen"    # Z

    .prologue
    .line 127
    mul-int v0, p1, p2

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemCount:I

    .line 128
    if-eqz p3, :cond_0

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->statisticsPanel:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 131
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->initGridLayout(IIZ)V

    .line 132
    return-void
.end method

.method public itemTouchDown(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 254
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 255
    .local v0, "itemView":Landroid/widget/TextView;
    if-eqz p2, :cond_0

    .line 256
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->greenColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 257
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->redColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 260
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public itemTouchUp(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 266
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 267
    .local v0, "itemView":Landroid/widget/TextView;
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->backgroundWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 268
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorBlack:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 269
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 293
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 294
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 295
    check-cast p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->trainingFragmentListener:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

    .line 300
    return-void

    .line 297
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SchulteTableTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->configId:I

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 102
    const v1, 0x7f0b00a3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 103
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->unbinder:Lbutterknife/Unbinder;

    .line 104
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 273
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 274
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;->cancelTraining()V

    .line 275
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;->close()V

    .line 276
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 280
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 281
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 282
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 304
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->trainingFragmentListener:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

    .line 306
    return-void
.end method

.method public onSchulteTableTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->trainingFragmentListener:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;->onSchulteTableTrainingCompleted(I)V

    .line 287
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;->init(I)V

    .line 111
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;->pauseTraining()V

    .line 116
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;->resumeTraining()V

    .line 121
    return-void
.end method

.method public setBoardItems(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 223
    .local p1, "itemsData":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemCount:I

    if-ge v0, v1, :cond_0

    .line 224
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    :cond_0
    return-void
.end method

.method public setBoardItemsEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 230
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 231
    .local v0, "itemView":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 233
    .end local v0    # "itemView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public updateBestTimeView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 242
    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    const-string v0, "-"

    .line 244
    .local v0, "bestTime":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->bestTimeTextView:Landroid/widget/TextView;

    const v2, 0x7f0e0168

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 245
    return-void

    .line 242
    .end local v0    # "bestTime":Ljava/lang/String;
    :cond_0
    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public updateCurrentTimeView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 237
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->timeTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0178

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 238
    return-void
.end method

.method public updateNextItemView(Ljava/lang/String;)V
    .locals 4
    .param p1, "nextItem"    # Ljava/lang/String;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->nextItemTextView:Landroid/widget/TextView;

    const v1, 0x7f0e016a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 250
    return-void
.end method
