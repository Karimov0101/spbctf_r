.class public interface abstract Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;
.super Ljava/lang/Object;
.source "IFlashWordRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;ILcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfig(II)V
.end method
