.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;
.super Ljava/lang/Object;
.source "IWordPairsPassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setNewBestScoreViewVisibility(Z)V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updatePassCoursePointsView(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
