.class public Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment_ViewBinding;
.super Ljava/lang/Object;
.source "HelpFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;

    .line 21
    const v0, 0x7f0900aa

    const-string v1, "field \'contentHolderScrollView\'"

    const-class v2, Landroid/widget/ScrollView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    .line 22
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;

    .line 28
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;

    .line 31
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    .line 32
    return-void
.end method
