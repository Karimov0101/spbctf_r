.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
.super Ljava/lang/Object;
.source "IWordPairsRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            ">;"
        }
    .end annotation
.end method
