.class Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;
.super Ljava/lang/Object;
.source "TrueColorsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;ILcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;ILcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 73
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$configId:I

    .line 74
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 77
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->setId(I)V

    .line 78
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->setConfig(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V

    .line 80
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 81
    return-void
.end method
