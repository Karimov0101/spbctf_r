.class Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;
.super Ljava/lang/Object;
.source "EvenNumbersPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 64
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;-><init>()V

    .line 65
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setScore(I)V

    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setUnixTime(J)V

    .line 68
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->addResult(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V

    .line 76
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "playedTime"    # J

    .prologue
    .line 57
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getTrainingDuration()I

    move-result v1

    int-to-long v2, v1

    sub-long/2addr v2, p1

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->updateProgressBar(I)V

    .line 60
    :cond_0
    return-void
.end method
