.class public Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
.super Lio/realm/RealmObject;
.source "LineOfSightResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/LineOfSightResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_FOUND_MISTAKE_COUNT:Ljava/lang/String; = "foundMistakeCount"

.field public static final FIELD_MISTAKE_COUNT:Ljava/lang/String; = "mistakeCount"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

.field private foundMistakeCount:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private mistakeCount:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v0

    return-object v0
.end method

.method public getFoundMistakeCount()I
    .locals 1

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v0

    return v0
.end method

.method public getFoundMistakesAccuracy()F
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 56
    :cond_0
    const/4 v0, 0x0

    .line 66
    :goto_0
    return v0

    .line 58
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    .line 62
    :cond_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v1

    if-le v0, v1, :cond_3

    .line 63
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$foundMistakeCount()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0

    .line 66
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getMistakeCount()I
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$mistakeCount()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    return-object v0
.end method

.method public realmGet$foundMistakeCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->foundMistakeCount:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->id:I

    return v0
.end method

.method public realmGet$mistakeCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->mistakeCount:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    return-void
.end method

.method public realmSet$foundMistakeCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->foundMistakeCount:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->id:I

    return-void
.end method

.method public realmSet$mistakeCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->mistakeCount:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    .line 85
    return-void
.end method

.method public setFoundMistakeCount(I)V
    .locals 0
    .param p1, "foundMistakeCount"    # I

    .prologue
    .line 51
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmSet$foundMistakeCount(I)V

    .line 52
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmSet$id(I)V

    .line 36
    return-void
.end method

.method public setMistakeCount(I)V
    .locals 0
    .param p1, "mistakeCount"    # I

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmSet$mistakeCount(I)V

    .line 44
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 76
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->realmSet$unixTime(J)V

    .line 77
    return-void
.end method
