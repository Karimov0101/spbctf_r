.class public interface abstract Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;
.super Ljava/lang/Object;
.source "IMathPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract compleateTraining()V
.end method

.method public abstract onNumberButtonPressed(I)V
.end method

.method public abstract pauseGame()V
.end method

.method public abstract shuffleArray()V
.end method

.method public abstract startTraining(I)V
.end method
