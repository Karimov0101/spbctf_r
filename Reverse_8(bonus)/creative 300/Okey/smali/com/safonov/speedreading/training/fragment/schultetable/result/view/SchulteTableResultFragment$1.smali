.class Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment$1;
.super Ljava/lang/Object;
.source "SchulteTableResultFragment.java"

# interfaces
.implements Lcom/github/mikephil/charting/formatter/IValueFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->setData(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFormattedValue(FLcom/github/mikephil/charting/data/Entry;ILcom/github/mikephil/charting/utils/ViewPortHandler;)Ljava/lang/String;
    .locals 2
    .param p1, "value"    # F
    .param p2, "entry"    # Lcom/github/mikephil/charting/data/Entry;
    .param p3, "dataSetIndex"    # I
    .param p4, "viewPortHandler"    # Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .prologue
    .line 140
    float-to-long v0, p1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
