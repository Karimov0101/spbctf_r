.class public Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "FlashWordsResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .line 21
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 6
    .param p1, "resultId"    # I

    .prologue
    .line 25
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v0

    .line 26
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->getResultList()Ljava/util/List;

    move-result-object v1

    .line 28
    .local v1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;->getConfig()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getTrainingDuration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;->updateTrainingDurationView(J)V

    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;->setChartViewData(Ljava/util/List;)V

    .line 32
    :cond_0
    return-void
.end method
