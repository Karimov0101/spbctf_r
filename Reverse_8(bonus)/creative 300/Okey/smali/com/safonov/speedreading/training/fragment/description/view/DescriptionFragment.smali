.class public Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "DescriptionFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;",
        "Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;"
    }
.end annotation


# static fields
.field private static final TRAINING_FRAGMENT_KEY:Ljava/lang/String; = "training_fragment_key"


# instance fields
.field contentHolderScrollView:Landroid/widget/ScrollView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090074
    .end annotation
.end field

.field private fragmentListener:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

.field private fragmentType:Lcom/safonov/speedreading/training/FragmentType;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 39
    return-void
.end method

.method public static newInstance(Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;
    .locals 3
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    .line 42
    new-instance v1, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;-><init>()V

    .line 43
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 44
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_fragment_key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 45
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    new-instance v0, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 104
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

    if-eqz v0, :cond_0

    .line 105
    check-cast p1, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

    .line 110
    return-void

    .line 107
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must DescriptionFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_fragment_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->fragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 55
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    const v1, 0x7f0b00b0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 66
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->unbinder:Lbutterknife/Unbinder;

    .line 68
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 90
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 91
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

    .line 116
    return-void
.end method

.method public onStartTrainingClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090075
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->fragmentListener:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;->onDescriptionFragmentStartClick()V

    .line 74
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->fragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;->requestToLoadContent(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 85
    return-void
.end method

.method public setContentLayout(I)V
    .locals 3
    .param p1, "layout"    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 97
    return-void
.end method
