.class public Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "MathPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;"
    }
.end annotation


# static fields
.field public static final INCORRECT_MAX_COUNT:I = 0x3


# instance fields
.field private bestResult:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

.field private complexity:I

.field private config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

.field private configId:I

.field private currentCorrectAnswer:I

.field private currentExpressionId:I

.field private currentScoreBuff:F

.field private handler:Landroid/os/Handler;

.field private incorrectCount:I

.field private model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

.field private postShowCorrectAnswer:Ljava/lang/Runnable;

.field private postShowIncorrectAnswer:Ljava/lang/Runnable;

.field private repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

.field private result:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

.field private score:F

.field private showCorrectAnswer:Ljava/lang/Runnable;

.field private showCount:I

.field private showExpressionRunnable:Ljava/lang/Runnable;

.field private showIncorrectAnswer:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V
    .locals 1
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 24
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    .line 139
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    .line 149
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$3;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowCorrectAnswer:Ljava/lang/Runnable;

    .line 159
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    .line 169
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowIncorrectAnswer:Ljava/lang/Runnable;

    .line 186
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showExpressionRunnable:Ljava/lang/Runnable;

    .line 43
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    .line 44
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)F
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentScoreBuff:F

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowCorrectAnswer:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)F
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showExpressionRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentCorrectAnswer:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowIncorrectAnswer:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->incorrectCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    return v0
.end method

.method static synthetic access$808(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    return v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    return v0
.end method


# virtual methods
.method public compleateTraining()V
    .locals 4

    .prologue
    .line 75
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 76
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 77
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 78
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 79
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showExpressionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 81
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    .line 83
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;-><init>()V

    .line 84
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->setScore(I)V

    .line 86
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->addResult(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;ILcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V

    .line 94
    return-void
.end method

.method public onNumberButtonPressed(I)V
    .locals 12
    .param p1, "value"    # I

    .prologue
    const-wide/16 v10, 0xa

    const/4 v8, 0x0

    .line 97
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getInstance()Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    move-result-object v1

    .line 98
    .local v1, "model":Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    invoke-virtual {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getExpression(II)Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    move-result-object v0

    .line 100
    .local v0, "expression":Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCount:I

    .line 102
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getCorrectAnswer()I

    move-result v2

    if-ne v2, p1, :cond_1

    .line 103
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    add-double/2addr v2, v4

    const v4, 0x453b8000    # 3000.0f

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getDuration()J

    move-result-wide v6

    long-to-float v5, v6

    div-float/2addr v4, v5

    float-to-double v4, v4

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentScoreBuff:F

    .line 104
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentScoreBuff:F

    add-float/2addr v2, v3

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    .line 105
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->timerPause()V

    .line 106
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 116
    :goto_0
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    cmpg-float v2, v2, v8

    if-gez v2, :cond_0

    .line 117
    iput v8, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    .line 119
    :cond_0
    return-void

    .line 109
    :cond_1
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->incorrectCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->incorrectCount:I

    .line 110
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->score:F

    .line 111
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getCorrectAnswer()I

    move-result v2

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentCorrectAnswer:I

    .line 112
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->timerPause()V

    .line 113
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v2, v3, v10, v11}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public pauseGame()V
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->refreshExpressionTextView()V

    .line 130
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setCorrectAnswerGone()V

    .line 131
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowCorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->postShowIncorrectAnswer:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showExpressionRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    return-void
.end method

.method public shuffleArray()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->shuffleExpressions()V

    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->showExpressionRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 125
    return-void
.end method

.method public startTraining(I)V
    .locals 6
    .param p1, "configId"    # I

    .prologue
    const/4 v4, 0x0

    .line 48
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->configId:I

    .line 49
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 50
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .line 51
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getComplexity()I

    move-result v2

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    .line 52
    iput v4, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->incorrectCount:I

    .line 54
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getInstance()Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    .line 55
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->shuffleExpressions()V

    .line 57
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    iget v5, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    invoke-virtual {v2, v3, v5}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getExpression(II)Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    move-result-object v1

    .line 58
    .local v1, "expression":Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->model:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->currentExpressionId:I

    iget v5, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->complexity:I

    invoke-virtual {v2, v3, v5}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getAllAnswers(II)[I

    move-result-object v0

    .line 60
    .local v0, "answers":[I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2, v4}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->updateScoreView(I)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    if-nez v3, :cond_0

    move v3, v4

    :goto_0
    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->updateBestScoreView(I)V

    .line 63
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2, v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setButtonsText([I)V

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setExpressionText(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    const/16 v3, 0x32

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->initProgressBar(I)V

    .line 67
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2, v4}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->updateProgressBar(I)V

    .line 69
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getDuration()J

    move-result-wide v4

    invoke-interface {v2, v4, v5}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->initCountDownTimer(J)V

    .line 70
    return-void

    .line 61
    :cond_0
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getScore()I

    move-result v3

    goto :goto_0
.end method
