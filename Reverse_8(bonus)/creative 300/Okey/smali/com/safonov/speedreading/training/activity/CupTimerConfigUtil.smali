.class public Lcom/safonov/speedreading/training/activity/CupTimerConfigUtil;
.super Ljava/lang/Object;
.source "CupTimerConfigUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getUserConfig(JI)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .locals 2
    .param p0, "duration"    # J
    .param p2, "type"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 15
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;-><init>()V

    .line 17
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    invoke-virtual {v0, p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->setDuration(J)V

    .line 18
    invoke-virtual {v0, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->setType(I)V

    .line 20
    return-object v0
.end method
