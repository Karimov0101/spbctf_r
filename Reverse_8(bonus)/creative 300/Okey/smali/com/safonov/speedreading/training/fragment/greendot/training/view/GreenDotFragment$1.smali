.class Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;
.super Ljava/lang/Object;
.source "GreenDotFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 91
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v2, v2, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v3, v3, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v3, v3, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingRight()I

    move-result v3

    sub-int v1, v2, v3

    .line 92
    .local v1, "width":I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v2, v2, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v3, v3, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v3, v3, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaddingBottom()I

    move-result v3

    sub-int v0, v2, v3

    .line 94
    .local v0, "height":I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v2, v2, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v4, v4, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotText:Ljava/lang/String;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    iget-object v5, v5, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    invoke-static {v3, v4, v5, v1, v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->access$000(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;Ljava/lang/String;Landroid/text/TextPaint;II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->setText(Ljava/lang/CharSequence;)V

    .line 96
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->access$200(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->access$100(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;->startTraining(I)V

    .line 97
    return-void
.end method
