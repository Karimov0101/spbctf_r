.class public Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;
.super Ljava/lang/Object;
.source "MathModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/training/model/IMathModel;


# static fields
.field public static final EXPRESSIONS_COUNT:I = 0x32

.field private static instance:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;


# instance fields
.field private expressionsFirstComplexity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;",
            ">;"
        }
    .end annotation
.end field

.field private expressionsFourthComplexity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;",
            ">;"
        }
    .end annotation
.end field

.field private expressionsSecondComplexity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;",
            ">;"
        }
    .end annotation
.end field

.field private expressionsThirdComplexity:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->initExpressions()V

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;
    .locals 2

    .prologue
    .line 30
    sget-object v0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->instance:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    if-nez v0, :cond_1

    .line 31
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    monitor-enter v1

    .line 32
    :try_start_0
    sget-object v0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->instance:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->instance:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    .line 34
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 36
    :cond_1
    sget-object v0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->instance:Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    return-object v0

    .line 34
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private initExpressions()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x9

    const/4 v7, 0x6

    const/4 v6, 0x4

    const/4 v5, 0x3

    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "2 + ? = 5"

    new-array v3, v5, [I

    fill-array-data v3, :array_0

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 + 9 = ?"

    const/16 v3, 0xe

    new-array v4, v5, [I

    fill-array-data v4, :array_1

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "18 - 7 = ?"

    const/16 v3, 0xb

    new-array v4, v5, [I

    fill-array-data v4, :array_2

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 * ? = 45"

    new-array v3, v5, [I

    fill-array-data v3, :array_3

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? / 4 = 2"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_4

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 + 2 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_5

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "6 / 3 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_6

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * ? = 12"

    new-array v3, v5, [I

    fill-array-data v3, :array_7

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 - ? = 4"

    new-array v3, v5, [I

    fill-array-data v3, :array_8

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 + 8 = ?"

    const/16 v3, 0xd

    new-array v4, v5, [I

    fill-array-data v4, :array_9

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "9 - ? = 5"

    new-array v3, v5, [I

    fill-array-data v3, :array_a

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 * ? = 20"

    new-array v3, v5, [I

    fill-array-data v3, :array_b

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "9 / ? = 3"

    new-array v3, v5, [I

    fill-array-data v3, :array_c

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 + ? = 7"

    new-array v3, v5, [I

    fill-array-data v3, :array_d

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? * 2 = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_e

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 / ? = 4"

    const/4 v3, 0x1

    new-array v4, v5, [I

    fill-array-data v4, :array_f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "6 * ? = 24"

    new-array v3, v5, [I

    fill-array-data v3, :array_10

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 111
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "12 - ? = 7"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_11

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "15 / 5 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_12

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 * ? = 49"

    const/4 v3, 0x7

    new-array v4, v5, [I

    fill-array-data v4, :array_13

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "9 - ? = 3"

    new-array v3, v5, [I

    fill-array-data v3, :array_14

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? * 3 = 9"

    new-array v3, v5, [I

    fill-array-data v3, :array_15

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 + ? = 16"

    new-array v3, v5, [I

    fill-array-data v3, :array_16

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 + 4 = ?"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_17

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "18 - ? = 9"

    new-array v3, v5, [I

    fill-array-data v3, :array_18

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 119
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * ? = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_19

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "3 * 2 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_1a

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "12 + 5 = ?"

    const/16 v3, 0x11

    new-array v4, v5, [I

    fill-array-data v4, :array_1b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 * ? = 25"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_1c

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "6 * ? = 24"

    new-array v3, v5, [I

    fill-array-data v3, :array_1d

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 4 = 13"

    new-array v3, v5, [I

    fill-array-data v3, :array_1e

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 * ? = 32"

    new-array v3, v5, [I

    fill-array-data v3, :array_1f

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "17 - 8 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_20

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 * ? = 42"

    new-array v3, v5, [I

    fill-array-data v3, :array_21

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * 8 = ?"

    const/16 v3, 0x20

    new-array v4, v5, [I

    fill-array-data v4, :array_22

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 7 = 9"

    const/16 v3, 0x10

    new-array v4, v5, [I

    fill-array-data v4, :array_23

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "2 * 7 = ?"

    const/16 v3, 0xe

    new-array v4, v5, [I

    fill-array-data v4, :array_24

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "15 - ? = 6"

    new-array v3, v5, [I

    fill-array-data v3, :array_25

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "9 * ? = 36"

    new-array v3, v5, [I

    fill-array-data v3, :array_26

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 * ? = 16"

    new-array v3, v5, [I

    fill-array-data v3, :array_27

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * ? = 24"

    new-array v3, v5, [I

    fill-array-data v3, :array_28

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "16 / ? = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_29

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "18 / ? = 9"

    new-array v3, v5, [I

    fill-array-data v3, :array_2a

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "36 / ? = 9"

    new-array v3, v5, [I

    fill-array-data v3, :array_2b

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "48 / ? = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_2c

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "56 / ? = 7"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_2d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "14 - 8 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_2e

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "20 / ? = 4"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_2f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 142
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "16 / ? = 4"

    new-array v3, v5, [I

    fill-array-data v3, :array_30

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "2 - ? = 0"

    new-array v3, v5, [I

    fill-array-data v3, :array_31

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 144
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 1 = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_32

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 + 3 = ?"

    const/16 v3, 0xa

    new-array v4, v5, [I

    fill-array-data v4, :array_33

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 + ? = 10"

    new-array v3, v5, [I

    fill-array-data v3, :array_34

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 - ? = 2"

    new-array v3, v5, [I

    fill-array-data v3, :array_35

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "9 + 3 = ?"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_36

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 2 = 12"

    const/16 v3, 0xa

    new-array v4, v5, [I

    fill-array-data v4, :array_37

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 - 3 = ?"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_38

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 1 = 11"

    const/16 v3, 0xa

    new-array v4, v5, [I

    fill-array-data v4, :array_39

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 - 1 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_3a

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 * 7 = ?"

    const/16 v3, 0x38

    new-array v4, v5, [I

    fill-array-data v4, :array_3b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "2 * 3 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_3c

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 * 8 = ?"

    const/16 v3, 0x28

    new-array v4, v5, [I

    fill-array-data v4, :array_3d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "30 / 5 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_3e

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "8 * 6 = ?"

    const/16 v3, 0x30

    new-array v4, v5, [I

    fill-array-data v4, :array_3f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "7 * 6 = ?"

    const/16 v3, 0x2a

    new-array v4, v5, [I

    fill-array-data v4, :array_40

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "21 / 7 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_41

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 160
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * 2 = ?"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_42

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "4 * 7 = ?"

    const/16 v3, 0x1c

    new-array v4, v5, [I

    fill-array-data v4, :array_43

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "15 / 3 = ?"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_44

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "48 / 6 = ?"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_45

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "24 / 4 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_46

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "28 / 4 = ?"

    const/4 v3, 0x7

    new-array v4, v5, [I

    fill-array-data v4, :array_47

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "32 / 8 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_48

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "5 * 7 = ?"

    const/16 v3, 0x23

    new-array v4, v5, [I

    fill-array-data v4, :array_49

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "21 / 3 = ?"

    const/4 v3, 0x7

    new-array v4, v5, [I

    fill-array-data v4, :array_4a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "72 / 9 = ?"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_4b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "20 / 5 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_4c

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "45 / 9 = ?"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_4d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "42 / 7 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_4e

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "28 / 7 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_4f

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    .line 177
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "12 + ? = 25"

    const/16 v3, 0xd

    new-array v4, v5, [I

    fill-array-data v4, :array_50

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "19 - 10 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_51

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "32 + 7 = ?"

    const/16 v3, 0x27

    new-array v4, v5, [I

    fill-array-data v4, :array_52

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 180
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "12 * ? = 36"

    new-array v3, v5, [I

    fill-array-data v3, :array_53

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 181
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 12 = 24"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_54

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "48 / ? = 12"

    new-array v3, v5, [I

    fill-array-data v3, :array_55

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "14 + 19 = ?"

    const/16 v3, 0x21

    new-array v4, v5, [I

    fill-array-data v4, :array_56

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 184
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "93 - 47 = ?"

    const/16 v3, 0x2e

    new-array v4, v5, [I

    fill-array-data v4, :array_57

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 185
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "23 + 64 = ?"

    const/16 v3, 0x57

    new-array v4, v5, [I

    fill-array-data v4, :array_58

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 186
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 72 = 94"

    const/16 v3, 0x16

    new-array v4, v5, [I

    fill-array-data v4, :array_59

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "72 / ? = 9"

    const/16 v3, 0x8

    new-array v4, v5, [I

    fill-array-data v4, :array_5a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "26 + ? = 74"

    const/16 v3, 0x30

    new-array v4, v5, [I

    fill-array-data v4, :array_5b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "68 / ? = 34"

    new-array v3, v5, [I

    fill-array-data v3, :array_5c

    invoke-direct {v1, v2, v9, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 190
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? / 6 = 11"

    const/16 v3, 0x42

    new-array v4, v5, [I

    fill-array-data v4, :array_5d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "95 - 19 = ?"

    const/16 v3, 0x4c

    new-array v4, v5, [I

    fill-array-data v4, :array_5e

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "75 / ? = 15"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_5f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "56 + ? = 74"

    const/16 v3, 0x12

    new-array v4, v5, [I

    fill-array-data v4, :array_60

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? / 2 = 32"

    const/16 v3, 0x40

    new-array v4, v5, [I

    fill-array-data v4, :array_61

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 92 = 97"

    const/4 v3, 0x5

    new-array v4, v5, [I

    fill-array-data v4, :array_62

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "88 / ? = 2"

    const/16 v3, 0x2c

    new-array v4, v5, [I

    fill-array-data v4, :array_63

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "18 * ? = 54"

    new-array v3, v5, [I

    fill-array-data v3, :array_64

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "56 + 28 = ?"

    const/16 v3, 0x54

    new-array v4, v5, [I

    fill-array-data v4, :array_65

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "84 - ? = 55"

    const/16 v3, 0x1d

    new-array v4, v5, [I

    fill-array-data v4, :array_66

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "24 + 49 = ?"

    const/16 v3, 0x49

    new-array v4, v5, [I

    fill-array-data v4, :array_67

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "69 / ? = 23"

    new-array v3, v5, [I

    fill-array-data v3, :array_68

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "19 * 4 = ?"

    const/16 v3, 0x4c

    new-array v4, v5, [I

    fill-array-data v4, :array_69

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 203
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "28 * 3 = ?"

    const/16 v3, 0x54

    new-array v4, v5, [I

    fill-array-data v4, :array_6a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "33 * 3 = ?"

    const/16 v3, 0x63

    new-array v4, v5, [I

    fill-array-data v4, :array_6b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "76 - 24 = ?"

    const/16 v3, 0x34

    new-array v4, v5, [I

    fill-array-data v4, :array_6c

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "89 - 56 = ?"

    const/16 v3, 0x21

    new-array v4, v5, [I

    fill-array-data v4, :array_6d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "27 * 3 = ?"

    const/16 v3, 0x51

    new-array v4, v5, [I

    fill-array-data v4, :array_6e

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "75 - 38 = ?"

    const/16 v3, 0x25

    new-array v4, v5, [I

    fill-array-data v4, :array_6f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "47 + 39 = ?"

    const/16 v3, 0x56

    new-array v4, v5, [I

    fill-array-data v4, :array_70

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "25 * 3 = ?"

    const/16 v3, 0x4b

    new-array v4, v5, [I

    fill-array-data v4, :array_71

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "43 + 28 = ?"

    const/16 v3, 0x47

    new-array v4, v5, [I

    fill-array-data v4, :array_72

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "16 * 6 = ?"

    const/16 v3, 0x60

    new-array v4, v5, [I

    fill-array-data v4, :array_73

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "84 / ? = 28"

    new-array v3, v5, [I

    fill-array-data v3, :array_74

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "99 / ? = 3"

    const/16 v3, 0x21

    new-array v4, v5, [I

    fill-array-data v4, :array_75

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "24 + ? = 76"

    const/16 v3, 0x34

    new-array v4, v5, [I

    fill-array-data v4, :array_76

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 216
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "33 + ? = 89"

    const/16 v3, 0x38

    new-array v4, v5, [I

    fill-array-data v4, :array_77

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "81 / ? = 27"

    new-array v3, v5, [I

    fill-array-data v3, :array_78

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 218
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "37 + ? = 75"

    const/16 v3, 0x26

    new-array v4, v5, [I

    fill-array-data v4, :array_79

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "86 - ? = 39"

    const/16 v3, 0x2f

    new-array v4, v5, [I

    fill-array-data v4, :array_7a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 220
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "75 / ? = 3"

    const/16 v3, 0x19

    new-array v4, v5, [I

    fill-array-data v4, :array_7b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "71 - 43 = ?"

    const/16 v3, 0x1c

    new-array v4, v5, [I

    fill-array-data v4, :array_7c

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "96 / ? = 6"

    const/16 v3, 0x10

    new-array v4, v5, [I

    fill-array-data v4, :array_7d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "33 - ? = 19"

    const/16 v3, 0xe

    new-array v4, v5, [I

    fill-array-data v4, :array_7e

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "72 / ? = 8"

    new-array v3, v5, [I

    fill-array-data v3, :array_7f

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "76 / 19 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_80

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "73 - 49 = ?"

    const/16 v3, 0x18

    new-array v4, v5, [I

    fill-array-data v4, :array_81

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "50 + 20 = ?"

    const/16 v3, 0x46

    new-array v4, v5, [I

    fill-array-data v4, :array_82

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 228
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "68 + ? = 97"

    const/16 v3, 0x1d

    new-array v4, v5, [I

    fill-array-data v4, :array_83

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 17 = 36"

    const/16 v3, 0x35

    new-array v4, v5, [I

    fill-array-data v4, :array_84

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "25 + 27 = ?"

    const/16 v3, 0x34

    new-array v4, v5, [I

    fill-array-data v4, :array_85

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "37 + 36 = ?"

    const/16 v3, 0x49

    new-array v4, v5, [I

    fill-array-data v4, :array_86

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 32 = 32"

    const/16 v3, 0x40

    new-array v4, v5, [I

    fill-array-data v4, :array_87

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "45 + 40 = ?"

    const/16 v3, 0x55

    new-array v4, v5, [I

    fill-array-data v4, :array_88

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 234
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "48 - 45 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_89

    invoke-direct {v1, v2, v5, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 235
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "14 + ? = 43"

    const/16 v3, 0x1d

    new-array v4, v5, [I

    fill-array-data v4, :array_8a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 236
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 78 = 14"

    const/16 v3, 0x5c

    new-array v4, v5, [I

    fill-array-data v4, :array_8b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    .line 240
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "102 + ? = 245"

    const/16 v3, 0x8f

    new-array v4, v5, [I

    fill-array-data v4, :array_8c

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 241
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "189 - 90 = ?"

    const/16 v3, 0x63

    new-array v4, v5, [I

    fill-array-data v4, :array_8d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 242
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "321 + 48 = ?"

    const/16 v3, 0x171

    new-array v4, v5, [I

    fill-array-data v4, :array_8e

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "12 * ? = 144"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_8f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 244
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "31 * ? = 186"

    new-array v3, v5, [I

    fill-array-data v3, :array_90

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 245
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "963 - 624 = ?"

    const/16 v3, 0x153

    new-array v4, v5, [I

    fill-array-data v4, :array_91

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 246
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "436 / 4 = ?"

    const/16 v3, 0x6d

    new-array v4, v5, [I

    fill-array-data v4, :array_92

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 247
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "264 + ? = 580"

    const/16 v3, 0x13c

    new-array v4, v5, [I

    fill-array-data v4, :array_93

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 248
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 328 = 301"

    const/16 v3, 0x275

    new-array v4, v5, [I

    fill-array-data v4, :array_94

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 249
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "54 * ? = 486"

    new-array v3, v5, [I

    fill-array-data v3, :array_95

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 250
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "668 - ? = 455"

    const/16 v3, 0xd5

    new-array v4, v5, [I

    fill-array-data v4, :array_96

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 251
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? * 14 = 322"

    const/16 v3, 0x17

    new-array v4, v5, [I

    fill-array-data v4, :array_97

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 252
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "368 - 234 = ?"

    const/16 v3, 0x86

    new-array v4, v5, [I

    fill-array-data v4, :array_98

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 253
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "24 * 13 = ?"

    const/16 v3, 0x138

    new-array v4, v5, [I

    fill-array-data v4, :array_99

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 254
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 368 = 121"

    const/16 v3, 0x1e9

    new-array v4, v5, [I

    fill-array-data v4, :array_9a

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 255
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "794 - 236 = ?"

    const/16 v3, 0x22e

    new-array v4, v5, [I

    fill-array-data v4, :array_9b

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 256
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "36 * 9 = ?"

    const/16 v3, 0x144

    new-array v4, v5, [I

    fill-array-data v4, :array_9c

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 257
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "235 - ? = 79"

    const/16 v3, 0x9c

    new-array v4, v5, [I

    fill-array-data v4, :array_9d

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 258
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? * 6 = 288"

    const/16 v3, 0x30

    new-array v4, v5, [I

    fill-array-data v4, :array_9e

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 259
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "245 - ? = 102"

    const/16 v3, 0x8f

    new-array v4, v5, [I

    fill-array-data v4, :array_9f

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 260
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "99 + ? = 189"

    const/16 v3, 0x5a

    new-array v4, v5, [I

    fill-array-data v4, :array_a0

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 261
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "369 - 321 = ?"

    const/16 v3, 0x30

    new-array v4, v5, [I

    fill-array-data v4, :array_a1

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "144 / 12 = ?"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_a2

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 263
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "186 / 31 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_a3

    invoke-direct {v1, v2, v7, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 264
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "624 + 339 = ?"

    const/16 v3, 0x3c3

    new-array v4, v5, [I

    fill-array-data v4, :array_a4

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 265
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "109 * ? = 436"

    new-array v3, v5, [I

    fill-array-data v3, :array_a5

    invoke-direct {v1, v2, v6, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 316 = 264"

    const/16 v3, 0x244

    new-array v4, v5, [I

    fill-array-data v4, :array_a6

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 267
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 328 = 629"

    const/16 v3, 0x12d

    new-array v4, v5, [I

    fill-array-data v4, :array_a7

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 268
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "486 / 54 = ?"

    new-array v3, v5, [I

    fill-array-data v3, :array_a8

    invoke-direct {v1, v2, v8, v3}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 269
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "213 + 455 = ?"

    const/16 v3, 0x29c

    new-array v4, v5, [I

    fill-array-data v4, :array_a9

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "322 / 14 = ?"

    const/16 v3, 0x17

    new-array v4, v5, [I

    fill-array-data v4, :array_aa

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 234 = 368"

    const/16 v3, 0x86

    new-array v4, v5, [I

    fill-array-data v4, :array_ab

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "312 / 13 = ?"

    const/16 v3, 0x18

    new-array v4, v5, [I

    fill-array-data v4, :array_ac

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 273
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "121 + ? = 489"

    const/16 v3, 0x170

    new-array v4, v5, [I

    fill-array-data v4, :array_ad

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "558 + ? = 794"

    const/16 v3, 0xec

    new-array v4, v5, [I

    fill-array-data v4, :array_ae

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "324 / ? = 9"

    const/16 v3, 0x24

    new-array v4, v5, [I

    fill-array-data v4, :array_af

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 276
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 156 = 235"

    const/16 v3, 0x4f

    new-array v4, v5, [I

    fill-array-data v4, :array_b0

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "288 / 6 = ?"

    const/16 v3, 0x30

    new-array v4, v5, [I

    fill-array-data v4, :array_b1

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 278
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "395 + ? = 862"

    const/16 v3, 0x1d3

    new-array v4, v5, [I

    fill-array-data v4, :array_b2

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 279
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "862 - 395 = ?"

    const/16 v3, 0x1d3

    new-array v4, v5, [I

    fill-array-data v4, :array_b3

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 280
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "389 + 536 = ?"

    const/16 v3, 0x39d

    new-array v4, v5, [I

    fill-array-data v4, :array_b4

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 294 = 449"

    const/16 v3, 0x2e7

    new-array v4, v5, [I

    fill-array-data v4, :array_b5

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 282
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "15 * ? = 180"

    const/16 v3, 0xc

    new-array v4, v5, [I

    fill-array-data v4, :array_b6

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 283
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "472 - 247 = ?"

    const/16 v3, 0xe1

    new-array v4, v5, [I

    fill-array-data v4, :array_b7

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "257 + ? = 885"

    const/16 v3, 0x274

    new-array v4, v5, [I

    fill-array-data v4, :array_b8

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? * 24 = 624"

    const/16 v3, 0x1a

    new-array v4, v5, [I

    fill-array-data v4, :array_b9

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "745 - 625 = ?"

    const/16 v3, 0x78

    new-array v4, v5, [I

    fill-array-data v4, :array_ba

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "426 + ? = 973"

    const/16 v3, 0x223

    new-array v4, v5, [I

    fill-array-data v4, :array_bb

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 288
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "32 * ? = 512"

    const/16 v3, 0x10

    new-array v4, v5, [I

    fill-array-data v4, :array_bc

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 289
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 367 = 52"

    const/16 v3, 0x1a3

    new-array v4, v5, [I

    fill-array-data v4, :array_bd

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 290
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "862 - 137 = ?"

    const/16 v3, 0x2d5

    new-array v4, v5, [I

    fill-array-data v4, :array_be

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 103 = 690"

    const/16 v3, 0x319

    new-array v4, v5, [I

    fill-array-data v4, :array_bf

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 292
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "560 + 200 = ?"

    const/16 v3, 0x2f8

    new-array v4, v5, [I

    fill-array-data v4, :array_c0

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "728 - ? = 592"

    const/16 v3, 0x88

    new-array v4, v5, [I

    fill-array-data v4, :array_c1

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? + 435 = 563"

    const/16 v3, 0x80

    new-array v4, v5, [I

    fill-array-data v4, :array_c2

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 295
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "805 - 194 = ?"

    const/16 v3, 0x263

    new-array v4, v5, [I

    fill-array-data v4, :array_c3

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "? - 137 = 419"

    const/16 v3, 0x22c

    new-array v4, v5, [I

    fill-array-data v4, :array_c4

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "866 - 104 = ?"

    const/16 v3, 0x2fa

    new-array v4, v5, [I

    fill-array-data v4, :array_c5

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 298
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "405 + 200 = ?"

    const/16 v3, 0x25d

    new-array v4, v5, [I

    fill-array-data v4, :array_c6

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    const-string v2, "473 + ? = 954"

    const/16 v3, 0x1e1

    new-array v4, v5, [I

    fill-array-data v4, :array_c7

    invoke-direct {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;-><init>(Ljava/lang/String;I[I)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 301
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    .line 303
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 304
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 305
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 307
    return-void

    .line 94
    :array_0
    .array-data 4
        0x2
        0x4
        0x1
    .end array-data

    .line 95
    :array_1
    .array-data 4
        0xd
        0xb
        0x10
    .end array-data

    .line 96
    :array_2
    .array-data 4
        0xd
        0xc
        0xa
    .end array-data

    .line 97
    :array_3
    .array-data 4
        0x7
        0xa
        0x8
    .end array-data

    .line 98
    :array_4
    .array-data 4
        0x6
        0xa
        0xc
    .end array-data

    .line 99
    :array_5
    .array-data 4
        0xa
        0x8
        0xb
    .end array-data

    .line 100
    :array_6
    .array-data 4
        0x3
        0x4
        0x6
    .end array-data

    .line 101
    :array_7
    .array-data 4
        0x4
        0x6
        0x2
    .end array-data

    .line 102
    :array_8
    .array-data 4
        0x4
        0x2
        0x5
    .end array-data

    .line 103
    :array_9
    .array-data 4
        0xc
        0xb
        0xe
    .end array-data

    .line 104
    :array_a
    .array-data 4
        0x3
        0x5
        0x6
    .end array-data

    .line 105
    :array_b
    .array-data 4
        0x3
        0x6
        0x5
    .end array-data

    .line 106
    :array_c
    .array-data 4
        0x2
        0x4
        0x6
    .end array-data

    .line 107
    :array_d
    .array-data 4
        0x4
        0x2
        0x5
    .end array-data

    .line 108
    :array_e
    .array-data 4
        0x6
        0x2
        0x3
    .end array-data

    .line 109
    :array_f
    .array-data 4
        0x4
        0x2
        0x6
    .end array-data

    .line 110
    :array_10
    .array-data 4
        0x6
        0x3
        0x5
    .end array-data

    .line 111
    :array_11
    .array-data 4
        0x6
        0x4
        0x7
    .end array-data

    .line 112
    :array_12
    .array-data 4
        0x4
        0x2
        0x5
    .end array-data

    .line 113
    :array_13
    .array-data 4
        0x6
        0x8
        0x5
    .end array-data

    .line 114
    :array_14
    .array-data 4
        0x5
        0x7
        0x4
    .end array-data

    .line 115
    :array_15
    .array-data 4
        0x4
        0x6
        0x5
    .end array-data

    .line 116
    :array_16
    .array-data 4
        0x7
        0x8
        0x6
    .end array-data

    .line 117
    :array_17
    .array-data 4
        0x10
        0x12
        0xe
    .end array-data

    .line 118
    :array_18
    .array-data 4
        0x7
        0x8
        0xa
    .end array-data

    .line 119
    :array_19
    .array-data 4
        0x3
        0x5
        0x4
    .end array-data

    .line 120
    :array_1a
    .array-data 4
        0x8
        0x9
        0xc
    .end array-data

    .line 121
    :array_1b
    .array-data 4
        0x10
        0x12
        0xf
    .end array-data

    .line 122
    :array_1c
    .array-data 4
        0x6
        0x4
        0x3
    .end array-data

    .line 123
    :array_1d
    .array-data 4
        0x3
        0x7
        0x6
    .end array-data

    .line 124
    :array_1e
    .array-data 4
        0x7
        0x8
        0xa
    .end array-data

    .line 125
    :array_1f
    .array-data 4
        0x6
        0x3
        0x7
    .end array-data

    .line 126
    :array_20
    .array-data 4
        0x7
        0x8
        0x6
    .end array-data

    .line 127
    :array_21
    .array-data 4
        0x7
        0x8
        0x5
    .end array-data

    .line 128
    :array_22
    .array-data 4
        0x28
        0x18
        0x10
    .end array-data

    .line 129
    :array_23
    .array-data 4
        0xf
        0x1a
        0x11
    .end array-data

    .line 130
    :array_24
    .array-data 4
        0x15
        0x1c
        0x23
    .end array-data

    .line 131
    :array_25
    .array-data 4
        0x7
        0x8
        0xa
    .end array-data

    .line 132
    :array_26
    .array-data 4
        0x3
        0x6
        0x5
    .end array-data

    .line 133
    :array_27
    .array-data 4
        0x3
        0x5
        0x4
    .end array-data

    .line 134
    :array_28
    .array-data 4
        0x7
        0x8
        0x4
    .end array-data

    .line 135
    :array_29
    .array-data 4
        0x3
        0x6
        0x4
    .end array-data

    .line 136
    :array_2a
    .array-data 4
        0x4
        0x9
        0x3
    .end array-data

    .line 137
    :array_2b
    .array-data 4
        0x3
        0x6
        0x5
    .end array-data

    .line 138
    :array_2c
    .array-data 4
        0x7
        0x8
        0x4
    .end array-data

    .line 139
    :array_2d
    .array-data 4
        0x7
        0x6
        0x9
    .end array-data

    .line 140
    :array_2e
    .array-data 4
        0x7
        0x8
        0x4
    .end array-data

    .line 141
    :array_2f
    .array-data 4
        0x6
        0x3
        0x4
    .end array-data

    .line 142
    :array_30
    .array-data 4
        0x3
        0x5
        0x6
    .end array-data

    .line 143
    :array_31
    .array-data 4
        0x4
        0x3
        0xc
    .end array-data

    .line 144
    :array_32
    .array-data 4
        0x8
        0x6
        0xa
    .end array-data

    .line 145
    :array_33
    .array-data 4
        0x9
        0xb
        0xc
    .end array-data

    .line 146
    :array_34
    .array-data 4
        0x3
        0x4
        0x1
    .end array-data

    .line 147
    :array_35
    .array-data 4
        0x4
        0x3
        0x1
    .end array-data

    .line 148
    :array_36
    .array-data 4
        0xe
        0xa
        0x10
    .end array-data

    .line 149
    :array_37
    .array-data 4
        0xe
        0xc
        0x9
    .end array-data

    .line 150
    :array_38
    .array-data 4
        0x4
        0x8
        0x6
    .end array-data

    .line 151
    :array_39
    .array-data 4
        0x9
        0xb
        0xc
    .end array-data

    .line 152
    :array_3a
    .array-data 4
        0x4
        0x2
        0x5
    .end array-data

    .line 153
    :array_3b
    .array-data 4
        0x36
        0x30
        0x2e
    .end array-data

    .line 154
    :array_3c
    .array-data 4
        0x8
        0x7
        0xc
    .end array-data

    .line 155
    :array_3d
    .array-data 4
        0x23
        0x1e
        0x2d
    .end array-data

    .line 156
    :array_3e
    .array-data 4
        0x4
        0x8
        0x5
    .end array-data

    .line 157
    :array_3f
    .array-data 4
        0x36
        0x2a
        0x3a
    .end array-data

    .line 158
    :array_40
    .array-data 4
        0x31
        0x30
        0x34
    .end array-data

    .line 159
    :array_41
    .array-data 4
        0x4
        0x2
        0x7
    .end array-data

    .line 160
    :array_42
    .array-data 4
        0x4
        0xc
        0x6
    .end array-data

    .line 161
    :array_43
    .array-data 4
        0x18
        0x15
        0x22
    .end array-data

    .line 162
    :array_44
    .array-data 4
        0x7
        0x3
        0xf
    .end array-data

    .line 163
    :array_45
    .array-data 4
        0x6
        0xc
        0x2
    .end array-data

    .line 164
    :array_46
    .array-data 4
        0x7
        0x8
        0x4
    .end array-data

    .line 165
    :array_47
    .array-data 4
        0x8
        0x6
        0x4
    .end array-data

    .line 166
    :array_48
    .array-data 4
        0x6
        0x3
        0x2
    .end array-data

    .line 167
    :array_49
    .array-data 4
        0x2d
        0x28
        0x1e
    .end array-data

    .line 168
    :array_4a
    .array-data 4
        0x8
        0x6
        0x4
    .end array-data

    .line 169
    :array_4b
    .array-data 4
        0x6
        0xc
        0x4
    .end array-data

    .line 170
    :array_4c
    .array-data 4
        0x3
        0x5
        0x6
    .end array-data

    .line 171
    :array_4d
    .array-data 4
        0xf
        0x7
        0x19
    .end array-data

    .line 172
    :array_4e
    .array-data 4
        0x8
        0x7
        0x4
    .end array-data

    .line 173
    :array_4f
    .array-data 4
        0x6
        0x8
        0x7
    .end array-data

    .line 177
    :array_50
    .array-data 4
        0xe
        0x17
        0x18
    .end array-data

    .line 178
    :array_51
    .array-data 4
        0x8
        0x13
        0x7
    .end array-data

    .line 179
    :array_52
    .array-data 4
        0x25
        0x2f
        0x31
    .end array-data

    .line 180
    :array_53
    .array-data 4
        0x2
        0x4
        0x5
    .end array-data

    .line 181
    :array_54
    .array-data 4
        0xb
        0xd
        0xe
    .end array-data

    .line 182
    :array_55
    .array-data 4
        0x6
        0x3
        0x7
    .end array-data

    .line 183
    :array_56
    .array-data 4
        0x20
        0x22
        0x2b
    .end array-data

    .line 184
    :array_57
    .array-data 4
        0x38
        0x2f
        0x39
    .end array-data

    .line 185
    :array_58
    .array-data 4
        0x61
        0x58
        0x62
    .end array-data

    .line 186
    :array_59
    .array-data 4
        0xc
        0x18
        0xe
    .end array-data

    .line 187
    :array_5a
    .array-data 4
        0x6
        0x7
        0x9
    .end array-data

    .line 188
    :array_5b
    .array-data 4
        0x2f
        0x3a
        0x2e
    .end array-data

    .line 189
    :array_5c
    .array-data 4
        0x3
        0x4
        0x6
    .end array-data

    .line 190
    :array_5d
    .array-data 4
        0x4c
        0x11
        0x41
    .end array-data

    .line 191
    :array_5e
    .array-data 4
        0x42
        0x4b
        0x56
    .end array-data

    .line 192
    :array_5f
    .array-data 4
        0xf
        0xa
        0x7
    .end array-data

    .line 193
    :array_60
    .array-data 4
        0x11
        0x10
        0xf
    .end array-data

    .line 194
    :array_61
    .array-data 4
        0x36
        0x44
        0x7c
    .end array-data

    .line 195
    :array_62
    .array-data 4
        0xc
        0xf
        0x7d
    .end array-data

    .line 196
    :array_63
    .array-data 4
        0x58
        0x22
        0x36
    .end array-data

    .line 197
    :array_64
    .array-data 4
        0x5
        0x6
        0x4
    .end array-data

    .line 198
    :array_65
    .array-data 4
        0x4a
        0x4c
        0x53
    .end array-data

    .line 199
    :array_66
    .array-data 4
        0x12
        0x13
        0x27
    .end array-data

    .line 200
    :array_67
    .array-data 4
        0x3f
        0x4c
        0x42
    .end array-data

    .line 201
    :array_68
    .array-data 4
        0xd
        0x6
        0x4
    .end array-data

    .line 202
    :array_69
    .array-data 4
        0x42
        0x4e
        0x44
    .end array-data

    .line 203
    :array_6a
    .array-data 4
        0x56
        0x58
        0x4c
    .end array-data

    .line 204
    :array_6b
    .array-data 4
        0xb
        0x60
        0x59
    .end array-data

    .line 205
    :array_6c
    .array-data 4
        0x2a
        0x3e
        0x3f
    .end array-data

    .line 206
    :array_6d
    .array-data 4
        0x17
        0xd
        0x1a
    .end array-data

    .line 207
    :array_6e
    .array-data 4
        0x47
        0x3d
        0x5b
    .end array-data

    .line 208
    :array_6f
    .array-data 4
        0x1b
        0x2f
        0x1c
    .end array-data

    .line 209
    :array_70
    .array-data 4
        0x4c
        0x58
        0x4e
    .end array-data

    .line 210
    :array_71
    .array-data 4
        0x55
        0x46
        0x41
    .end array-data

    .line 211
    :array_72
    .array-data 4
        0x51
        0x48
        0x52
    .end array-data

    .line 212
    :array_73
    .array-data 4
        0x56
        0x61
        0x88
    .end array-data

    .line 213
    :array_74
    .array-data 4
        0x4
        0x6
        0x2
    .end array-data

    .line 214
    :array_75
    .array-data 4
        0x22
        0x24
        0x17
    .end array-data

    .line 215
    :array_76
    .array-data 4
        0x36
        0x2a
        0x58
    .end array-data

    .line 216
    :array_77
    .array-data 4
        0x39
        0x42
        0x2e
    .end array-data

    .line 217
    :array_78
    .array-data 4
        0x6
        0x7
        0x4
    .end array-data

    .line 218
    :array_79
    .array-data 4
        0x30
        0x24
        0x2c
    .end array-data

    .line 219
    :array_7a
    .array-data 4
        0x25
        0x39
        0x43
    .end array-data

    .line 220
    :array_7b
    .array-data 4
        0x23
        0x14
        0x1e
    .end array-data

    .line 221
    :array_7c
    .array-data 4
        0x12
        0x26
        0x22
    .end array-data

    .line 222
    :array_7d
    .array-data 4
        0x12
        0x1a
        0x18
    .end array-data

    .line 223
    :array_7e
    .array-data 4
        0x18
        0x1a
        0x10
    .end array-data

    .line 224
    :array_7f
    .array-data 4
        0x8
        0x7
        0x6
    .end array-data

    .line 225
    :array_80
    .array-data 4
        0x6
        0x8
        0x3
    .end array-data

    .line 226
    :array_81
    .array-data 4
        0x22
        0xe
        0x10
    .end array-data

    .line 227
    :array_82
    .array-data 4
        0x3c
        0x50
        0x5a
    .end array-data

    .line 228
    :array_83
    .array-data 4
        0x1b
        0x27
        0x25
    .end array-data

    .line 229
    :array_84
    .array-data 4
        0x3f
        0x40
        0x39
    .end array-data

    .line 230
    :array_85
    .array-data 4
        0x3e
        0x3d
        0x33
    .end array-data

    .line 231
    :array_86
    .array-data 4
        0x3f
        0x43
        0x1
    .end array-data

    .line 232
    :array_87
    .array-data 4
        0x36
        0x4a
        0x56
    .end array-data

    .line 233
    :array_88
    .array-data 4
        0x5f
        0x4b
        0x5
    .end array-data

    .line 234
    :array_89
    .array-data 4
        0x5d
        0x4
        0xd
    .end array-data

    .line 235
    :array_8a
    .array-data 4
        0x13
        0x27
        0x18
    .end array-data

    .line 236
    :array_8b
    .array-data 4
        0x62
        0x52
        0x58
    .end array-data

    .line 240
    :array_8c
    .array-data 4
        0x99
        0x85
        0x7b
    .end array-data

    .line 241
    :array_8d
    .array-data 4
        0x59
        0x6d
        0x4f
    .end array-data

    .line 242
    :array_8e
    .array-data 4
        0x17b
        0x185
        0x16d
    .end array-data

    .line 243
    :array_8f
    .array-data 4
        0xb
        0xd
        0xe
    .end array-data

    .line 244
    :array_90
    .array-data 4
        0x8
        0xc
        0x4
    .end array-data

    .line 245
    :array_91
    .array-data 4
        0x149
        0x147
        0x151
    .end array-data

    .line 246
    :array_92
    .array-data 4
        0x77
        0x6b
        0x75
    .end array-data

    .line 247
    :array_93
    .array-data 4
        0x146
        0x13d
        0x13e
    .end array-data

    .line 248
    :array_94
    .array-data 4
        0x27f
        0x273
        0x211
    .end array-data

    .line 249
    :array_95
    .array-data 4
        0x8
        0x7
        0x6
    .end array-data

    .line 250
    :array_96
    .array-data 4
        0xdf
        0xe9
        0xf3
    .end array-data

    .line 251
    :array_97
    .array-data 4
        0x18
        0x2b
        0x21
    .end array-data

    .line 252
    :array_98
    .array-data 4
        0x90
        0x9a
        0x88
    .end array-data

    .line 253
    :array_99
    .array-data 4
        0x142
        0x13a
        0x144
    .end array-data

    .line 254
    :array_9a
    .array-data 4
        0x1f3
        0x1df
        0x1e7
    .end array-data

    .line 255
    :array_9b
    .array-data 4
        0x21a
        0x1e8
        0x18e
    .end array-data

    .line 256
    :array_9c
    .array-data 4
        0x13a
        0x126
        0x1a8
    .end array-data

    .line 257
    :array_9d
    .array-data 4
        0x88
        0x92
        0xa6
    .end array-data

    .line 258
    :array_9e
    .array-data 4
        0x1a
        0x3a
        0x1c
    .end array-data

    .line 259
    :array_9f
    .array-data 4
        0x67
        0x5d
        0x99
    .end array-data

    .line 260
    :array_a0
    .array-data 4
        0x50
        0x5c
        0x52
    .end array-data

    .line 261
    :array_a1
    .array-data 4
        0x3a
        0x2f
        0x2e
    .end array-data

    .line 262
    :array_a2
    .array-data 4
        0xe
        0xb
        0xd
    .end array-data

    .line 263
    :array_a3
    .array-data 4
        0x8
        0xc
        0x4
    .end array-data

    .line 264
    :array_a4
    .array-data 4
        0x3a5
        0x37d
        0x3cd
    .end array-data

    .line 265
    :array_a5
    .array-data 4
        0x6
        0x7
        0x3
    .end array-data

    .line 266
    :array_a6
    .array-data 4
        0x2a8
        0x24e
        0x23a
    .end array-data

    .line 267
    :array_a7
    .array-data 4
        0x123
        0xc9
        0x187
    .end array-data

    .line 268
    :array_a8
    .array-data 4
        0x7
        0x8
        0x6
    .end array-data

    .line 269
    :array_a9
    .array-data 4
        0x27e
        0x1d4
        0x242
    .end array-data

    .line 270
    :array_aa
    .array-data 4
        0x18
        0x13
        0x21
    .end array-data

    .line 271
    :array_ab
    .array-data 4
        0x5e
        0xc2
        0x9a
    .end array-data

    .line 272
    :array_ac
    .array-data 4
        0x1c
        0x1a
        0xe
    .end array-data

    .line 273
    :array_ad
    .array-data 4
        0x148
        0x12a
        0x184
    .end array-data

    .line 274
    :array_ae
    .array-data 4
        0xd8
        0xb1
        0xf6
    .end array-data

    .line 275
    :array_af
    .array-data 4
        0x1a
        0x22
        0x1c
    .end array-data

    .line 276
    :array_b0
    .array-data 4
        0x45
        0x81
        0x43
    .end array-data

    .line 277
    :array_b1
    .array-data 4
        0x3a
        0x2e
        0x35
    .end array-data

    .line 278
    :array_b2
    .array-data 4
        0x197
        0x18d
        0x1bf
    .end array-data

    .line 279
    :array_b3
    .array-data 4
        0x197
        0x18d
        0x1bf
    .end array-data

    .line 280
    :array_b4
    .array-data 4
        0x3a7
        0x38a
        0x380
    .end array-data

    .line 281
    :array_b5
    .array-data 4
        0x2d3
        0x32d
        0x31a
    .end array-data

    .line 282
    :array_b6
    .array-data 4
        0x10
        0x16
        0xe
    .end array-data

    .line 283
    :array_b7
    .array-data 4
        0xcd
        0x73
        0xf5
    .end array-data

    .line 284
    :array_b8
    .array-data 4
        0x24c
        0x26a
        0x256
    .end array-data

    .line 285
    :array_b9
    .array-data 4
        0x1c
        0x18
        0x24
    .end array-data

    .line 286
    :array_ba
    .array-data 4
        0x82
        0x8c
        0x68
    .end array-data

    .line 287
    :array_bb
    .array-data 4
        0x219
        0x1b5
        0x254
    .end array-data

    .line 288
    :array_bc
    .array-data 4
        0x12
        0x1c
        0x1a
    .end array-data

    .line 289
    :array_bd
    .array-data 4
        0x211
        0x199
        0x1ac
    .end array-data

    .line 290
    :array_be
    .array-data 4
        0x27b
        0x2f3
        0x20d
    .end array-data

    .line 291
    :array_bf
    .array-data 4
        0x2e7
        0x2b5
        0x283
    .end array-data

    .line 292
    :array_c0
    .array-data 4
        0x35c
        0x294
        0x3c0
    .end array-data

    .line 293
    :array_c1
    .array-data 4
        0xb0
        0xf6
        0x146
    .end array-data

    .line 294
    :array_c2
    .array-data 4
        0x94
        0xee
        0x6c
    .end array-data

    .line 295
    :array_c3
    .array-data 4
        0x2c7
        0x26d
        0x32b
    .end array-data

    .line 296
    :array_c4
    .array-data 4
        0x218
        0x1dc
        0x204
    .end array-data

    .line 297
    :array_c5
    .array-data 4
        0x2c8
        0x354
        0x2b4
    .end array-data

    .line 298
    :array_c6
    .array-data 4
        0x267
        0x1f9
        0xcd
    .end array-data

    .line 299
    :array_c7
    .array-data 4
        0x187
        0x1eb
        0x209
    .end array-data
.end method

.method private shuffleArray([I)V
    .locals 5
    .param p1, "ar"    # [I

    .prologue
    .line 80
    new-instance v3, Ljava/util/Random;

    invoke-direct {v3}, Ljava/util/Random;-><init>()V

    .line 81
    .local v3, "rnd":Ljava/util/Random;
    array-length v4, p1

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_0

    .line 83
    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    .line 85
    .local v2, "index":I
    aget v0, p1, v2

    .line 86
    .local v0, "a":I
    aget v4, p1, v1

    aput v4, p1, v2

    .line 87
    aput v0, p1, v1

    .line 81
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 89
    .end local v0    # "a":I
    .end local v2    # "index":I
    :cond_0
    return-void
.end method


# virtual methods
.method public getAllAnswers(II)[I
    .locals 6
    .param p1, "id"    # I
    .param p2, "complexity"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getExpression(II)Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    move-result-object v1

    .line 58
    .local v1, "expression":Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getCorrectAnswer()I

    move-result v0

    .line 59
    .local v0, "correctAnswer":I
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getIncorrectAnswers()[I

    move-result-object v3

    .line 61
    .local v3, "incorrectAnswers":[I
    const/4 v5, 0x4

    new-array v4, v5, [I

    .line 62
    .local v4, "result":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v5, v3

    if-ge v2, v5, :cond_0

    .line 63
    aget v5, v3, v2

    aput v5, v4, v2

    .line 62
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 65
    :cond_0
    const/4 v5, 0x3

    aput v0, v4, v5

    .line 66
    invoke-direct {p0, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->shuffleArray([I)V

    .line 67
    return-object v4
.end method

.method public getExpression(II)Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    .locals 1
    .param p1, "id"    # I
    .param p2, "complexity"    # I

    .prologue
    .line 41
    packed-switch p2, :pswitch_data_0

    .line 51
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 43
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    goto :goto_0

    .line 47
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    goto :goto_0

    .line 49
    :pswitch_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    goto :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public shuffleExpressions()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFirstComplexity:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 73
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsSecondComplexity:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 74
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsThirdComplexity:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->expressionsFourthComplexity:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 76
    return-void
.end method
