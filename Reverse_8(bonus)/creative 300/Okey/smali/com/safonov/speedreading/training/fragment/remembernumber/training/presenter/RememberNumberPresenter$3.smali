.class Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;
.super Ljava/lang/Object;
.source "RememberNumberPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 221
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    :goto_0
    return-void

    .line 228
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$1102(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;[Ljava/lang/String;)[Ljava/lang/String;

    .line 229
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$1300(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;->createNumber(I)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$1202(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;[Ljava/lang/String;)[Ljava/lang/String;

    .line 231
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultTextColor()V

    .line 233
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$1200(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsValues([Ljava/lang/String;)V

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$1400(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
