.class public Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;
.super Ljava/lang/Object;
.source "CourseSaveUtil.java"


# static fields
.field private static final ACCELERATOR_COURSE_TRAININGS_JSON:Ljava/lang/String; = "accelerator_course_json"

.field private static final COURSE_SAVE_PREFERENCES:Ljava/lang/String; = "course_save_preferences"

.field private static final PASS_COURSE_1_TRAININGS_JSON:Ljava/lang/String; = "pass_course_1_json"


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "course_save_preferences"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 23
    return-void
.end method


# virtual methods
.method public load(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;
    .locals 4
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 52
    sget-object v2, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 60
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "This course type is unsupported"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 54
    :pswitch_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "pass_course_1_json"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "json":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 64
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    const-class v2, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;

    .line 67
    :cond_0
    return-object v1

    .line 57
    .end local v0    # "json":Ljava/lang/String;
    :pswitch_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "accelerator_course_json"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    .restart local v0    # "json":Ljava/lang/String;
    goto :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reset(Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 3
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 72
    sget-object v0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This course type is unsupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pass_course_1_json"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 82
    :goto_0
    return-void

    .line 77
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accelerator_course_json"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public save(Lcom/safonov/speedreading/training/TrainingType;I[I)V
    .locals 4
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "trainingsCompletedCount"    # I
    .param p3, "resultIds"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 29
    new-instance v1, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;-><init>()V

    .line 31
    .local v1, "save":Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;
    iput p2, v1, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;->trainingCompletedCount:I

    .line 32
    iput-object p3, v1, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;->trainingResultIds:[I

    .line 34
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 36
    .local v0, "json":Ljava/lang/String;
    sget-object v2, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 44
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This course type is unsupported"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 38
    :pswitch_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pass_course_1_json"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 46
    :goto_0
    return-void

    .line 41
    :pswitch_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "accelerator_course_json"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 36
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
