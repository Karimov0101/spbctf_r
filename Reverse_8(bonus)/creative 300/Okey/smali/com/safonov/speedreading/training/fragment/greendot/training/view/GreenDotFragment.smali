.class public Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "GreenDotFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;",
        "Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field private completeListener:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

.field private configId:I

.field private greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field greenDotText:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e008e
    .end annotation
.end field

.field greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900a5
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 46
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;Ljava/lang/String;Landroid/text/TextPaint;II)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/text/TextPaint;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->trimGreenDotText(Ljava/lang/String;Landroid/text/TextPaint;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .prologue
    .line 32
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->configId:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 52
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;-><init>()V

    .line 53
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->setArguments(Landroid/os/Bundle;)V

    .line 56
    return-object v1
.end method

.method private trimGreenDotText(Ljava/lang/String;Landroid/text/TextPaint;II)Ljava/lang/String;
    .locals 11
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "textPaint"    # Landroid/text/TextPaint;
    .param p3, "viewWidth"    # I
    .param p4, "viewHeight"    # I

    .prologue
    const/16 v1, 0x20

    .line 102
    const/high16 v5, 0x3f800000    # 1.0f

    .line 103
    .local v5, "lineSpacingMultiplier":F
    const/4 v6, 0x0

    .line 105
    .local v6, "lineSpacingExtra":F
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 107
    .local v10, "temp":Ljava/lang/StringBuilder;
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 109
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 111
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 112
    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 113
    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 115
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/4 v7, 0x1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    .line 116
    .local v0, "tempLayout":Landroid/text/StaticLayout;
    invoke-virtual {v0, p4}, Landroid/text/StaticLayout;->getLineForVertical(I)I

    move-result v9

    .line 119
    .local v9, "lastVisibleLine":I
    invoke-virtual {v0, v9}, Landroid/text/StaticLayout;->getLineBottom(I)I

    move-result v1

    if-le v1, p4, :cond_0

    .line 120
    add-int/lit8 v1, v9, -0x1

    invoke-virtual {v0, v1}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v8

    .line 125
    .local v8, "lastCharacterIndex":I
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v10, v1, v8}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 122
    .end local v8    # "lastCharacterIndex":I
    :cond_0
    invoke-virtual {v0, v9}, Landroid/text/StaticLayout;->getLineEnd(I)I

    move-result v8

    .restart local v8    # "lastCharacterIndex":I
    goto :goto_0
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 41
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getGreenDotRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 42
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V

    return-object v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 169
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 170
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 171
    check-cast p1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

    .line 176
    return-void

    .line 173
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 61
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->configId:I

    .line 65
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    const v1, 0x7f0b003e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 79
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->unbinder:Lbutterknife/Unbinder;

    .line 81
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 135
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->close()V

    .line 137
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 139
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;->cancelTraining()V

    .line 140
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 141
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->unbinder:Lbutterknife/Unbinder;

    .line 148
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

    .line 182
    return-void
.end method

.method public onGreenDotTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;->onGreenDotTrainingCompleted(I)V

    .line 163
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 86
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->setProgress(I)V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->post(Ljava/lang/Runnable;)Z

    .line 99
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;->pauseTraining()V

    .line 153
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;->resumeTraining()V

    .line 158
    return-void
.end method

.method public setGreenDotViewProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;->setProgress(I)V

    .line 131
    return-void
.end method
