.class public Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "GreenDotPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/IGreenDotPresenter;"
    }
.end annotation


# static fields
.field public static final MAX_PROGRESS:I = 0x64


# instance fields
.field private isAfterPause:Z

.field private progress:I

.field private progressBeforePause:I

.field private repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

.field private timeBeforePause:J

.field private timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V
    .locals 3
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 17
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 25
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    .line 27
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progressBeforePause:I

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeBeforePause:J

    .line 29
    iput-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isAfterPause:Z

    .line 18
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 13
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isAfterPause:Z

    return v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 13
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isAfterPause:Z

    return p1
.end method

.method static synthetic access$102(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    return p1
.end method

.method static synthetic access$108(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 13
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 13
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progressBeforePause:I

    return v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 78
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    .line 82
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .line 83
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeBeforePause:J

    .line 88
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progressBeforePause:I

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progress:I

    .line 91
    return-void
.end method

.method public resumeTraining()V
    .locals 8

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isAfterPause:Z

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeBeforePause:J

    iget-wide v4, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeBeforePause:J

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->progressBeforePause:I

    rsub-int/lit8 v1, v1, 0x64

    int-to-long v6, v1

    div-long/2addr v4, v6

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(JJ)V

    .line 97
    return-void
.end method

.method public startTraining(I)V
    .locals 8
    .param p1, "configId"    # I

    .prologue
    .line 33
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v0

    .line 35
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    new-instance v1, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 36
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;I)V

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 72
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x64

    div-long/2addr v4, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(JJ)V

    .line 73
    return-void
.end method
