.class Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PrepareTrainingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCanceled:Z

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    .prologue
    .line 48
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;->isCanceled:Z

    .line 62
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 67
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;->isCanceled:Z

    if-nez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->access$000(Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;)Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;->onPrepareFragmentCompleted()V

    .line 70
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;->isCanceled:Z

    .line 56
    return-void
.end method
