.class public interface abstract Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;
.super Ljava/lang/Object;
.source "ITrueColorsPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract finishTraining()V
.end method

.method public abstract onNumberButtonPressed(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/lang/String;)V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract startTraining(I)V
.end method
