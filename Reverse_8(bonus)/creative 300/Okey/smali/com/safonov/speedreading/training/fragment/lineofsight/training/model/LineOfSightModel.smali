.class public Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;
.super Ljava/lang/Object;
.source "LineOfSightModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;


# static fields
.field private static final CENTER_VALUE:Ljava/lang/String; = "\u2022"

.field private static final MAX_PROBABILITY:I = 0x64


# instance fields
.field private alphabet:[Ljava/lang/String;

.field private random:Ljava/util/Random;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    .line 24
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f03000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public getCheckedItems(IZ)Ljava/util/List;
    .locals 7
    .param p1, "checkedItemCount"    # I
    .param p2, "areAllEqual"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 45
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 46
    .local v0, "checkedItemList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget-object v2, v4, v5

    .line 47
    .local v2, "letter":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 48
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :cond_0
    if-nez p2, :cond_1

    .line 52
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    invoke-virtual {v4, p1}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 53
    .local v3, "replacedIndex":I
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-interface {v0, v3, v4}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 56
    .end local v3    # "replacedIndex":I
    :cond_1
    return-object v0
.end method

.method public getItems(II)Ljava/util/List;
    .locals 7
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    mul-int v2, p1, p2

    .line 30
    .local v2, "itemCount":I
    div-int/lit8 v0, v2, 0x2

    .line 32
    .local v0, "center":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 33
    .local v3, "itemList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 34
    if-ne v1, v0, :cond_0

    .line 35
    const-string v4, "\u2022"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 33
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 37
    :cond_0
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->alphabet:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 40
    :cond_1
    return-object v3
.end method

.method public nextIsMistake(I)Z
    .locals 2
    .param p1, "mistakeProbability"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/LineOfSightModel;->random:Ljava/util/Random;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    if-gt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
