.class public Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SpeedReadingResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "result_id"


# instance fields
.field averageSpeedItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006f
    .end annotation
.end field

.field averageSpeedLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060071
    .end annotation
.end field

.field averageSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090030
    .end annotation
.end field

.field averageSpeedTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0193
    .end annotation
.end field

.field bestAverageSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090033
    .end annotation
.end field

.field bestMaxSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090034
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901ba
    .end annotation
.end field

.field maxSpeedItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field maxSpeedLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field maxSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090103
    .end annotation
.end field

.field maxSpeedTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0196
    .end annotation
.end field

.field private resultId:I

.field private speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 55
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 61
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;-><init>()V

    .line 62
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 64
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 50
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getSpeedReadingRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->resultId:I

    .line 74
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    const v1, 0x7f0b00ac

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 91
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 93
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 215
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 216
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->close()V

    .line 217
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 218
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 219
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 226
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;->initTrainingResults(I)V

    .line 100
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 150
    .local p1, "speedReadingResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v7, "maxSpeedEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 153
    .local v1, "averageSpeedEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_0

    .line 154
    new-instance v11, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v12, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v10}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v10

    int-to-float v10, v10

    invoke-direct {v11, v12, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v7, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v11, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v12, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v10}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v10

    int-to-float v10, v10

    invoke-direct {v11, v12, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v1, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 158
    :cond_0
    new-instance v6, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->maxSpeedTitle:Ljava/lang/String;

    invoke-direct {v6, v7, v10}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 159
    .local v6, "maxSpeedDataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->maxSpeedLineColor:I

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 160
    sget-object v10, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 161
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartLineWidthDp:I

    int-to-float v10, v10

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 162
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v10, v10

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 163
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 164
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->maxSpeedItemColor:I

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 165
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v10, v10

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 166
    const/4 v10, 0x0

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 167
    new-instance v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment$1;

    invoke-direct {v10, p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;)V

    invoke-virtual {v6, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 174
    new-instance v0, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->averageSpeedTitle:Ljava/lang/String;

    invoke-direct {v0, v1, v10}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 175
    .local v0, "averageSpeedDataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->averageSpeedLineColor:I

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 176
    sget-object v10, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 177
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartLineWidthDp:I

    int-to-float v10, v10

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 178
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v10, v10

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 179
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 180
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->averageSpeedItemColor:I

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 181
    iget v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v10, v10

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 182
    const/4 v10, 0x0

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 183
    new-instance v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment$2;

    invoke-direct {v10, p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment$2;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;)V

    invoke-virtual {v0, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 190
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 191
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    new-instance v2, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v2, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 196
    .local v2, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v9

    .line 197
    .local v9, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v10, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v9, v10}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 198
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v9, v10}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 200
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 201
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v10, 0x0

    invoke-virtual {v5, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 202
    const/4 v10, 0x1

    invoke-virtual {v5, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 204
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v8

    .line 205
    .local v8, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 206
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 208
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 209
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10, v2}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 210
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 211
    return-void
.end method

.method public setNewBestAverageSpeedViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 131
    if-nez p1, :cond_0

    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->averageSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 134
    :cond_0
    return-void
.end method

.method public setNewBestMaxSpeedViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    const/4 v1, 0x0

    .line 114
    if-nez p1, :cond_0

    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->maxSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 117
    :cond_0
    return-void
.end method

.method public updateAverageSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->averageSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 122
    return-void
.end method

.method public updateBestAverageSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->bestAverageSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    return-void
.end method

.method public updateBestMaxSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->bestMaxSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method public updateMaxSpeedView(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->maxSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method
