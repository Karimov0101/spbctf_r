.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;
.super Ljava/lang/Object;
.source "ILineOfSightPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract init(I)V
.end method

.method public abstract onCheckButtonPressed()V
.end method

.method public abstract onPreShowAnimationCompleted()V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract startTraining()V
.end method
