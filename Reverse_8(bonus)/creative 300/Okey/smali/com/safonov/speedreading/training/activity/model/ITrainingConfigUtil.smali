.class public interface abstract Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;
.super Ljava/lang/Object;
.source "ITrainingConfigUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;,
        Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;
    }
.end annotation


# virtual methods
.method public abstract requestToGetCourseConfigList(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract requestToGetTrainingConfig(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
