.class public interface abstract Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
.super Ljava/lang/Object;
.source "IRememberNumberRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ILcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigComplexity(II)V
.end method
