.class public Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "PassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final PASS_COURSE_RESULT_ARRAY_PARAM:Ljava/lang/String; = "pass_course_result_array"


# instance fields
.field chartItem1Color:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItem2Color:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006f
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLine1Color:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLine2Color:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060071
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field private evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field private greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field layout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090131
    .end annotation
.end field

.field private lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

.field private passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

.field private progressDialog:Landroid/app/ProgressDialog;

.field private rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

.field private resultIds:[I

.field private schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

.field private speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;

.field private wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 112
    return-void
.end method

.method public static newInstance([I)Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
    .locals 3
    .param p0, "resultIds"    # [I

    .prologue
    .line 118
    new-instance v1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;-><init>()V

    .line 119
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 120
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "pass_course_result_array"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 121
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 122
    return-object v1
.end method


# virtual methods
.method public addEvenNumbersResultView(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/List;)V
    .locals 13
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;>;"
    const v12, 0x7f09020c

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 591
    if-eqz p1, :cond_0

    .line 592
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b0068

    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    .line 593
    invoke-virtual {v7, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 595
    .local v4, "resultHolderView":Landroid/view/View;
    invoke-static {v4, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 596
    .local v6, "titleTextView":Landroid/widget/TextView;
    const v7, 0x7f0e0063

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 598
    const v7, 0x7f090066

    invoke-static {v4, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 599
    .local v0, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b0066

    .line 600
    invoke-virtual {v7, v8, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 602
    .local v2, "itemView":Landroid/view/View;
    invoke-static {v2, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 603
    .local v1, "itemTitleTextView":Landroid/widget/TextView;
    const v7, 0x7f090188

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 604
    .local v5, "scoreTextView":Landroid/widget/TextView;
    const v7, 0x7f090132

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 606
    .local v3, "passCourseScoreTextView":Landroid/widget/TextView;
    const v7, 0x7f0e00f0

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 607
    const v7, 0x7f0e006a

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 608
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v7

    invoke-static {v7}, Lcom/safonov/speedreading/training/fragment/passcource/util/EvenNumbersScoreUtil;->getPassCourseScore(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 609
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 658
    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 660
    .end local v0    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v1    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v2    # "itemView":Landroid/view/View;
    .end local v3    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v4    # "resultHolderView":Landroid/view/View;
    .end local v5    # "scoreTextView":Landroid/widget/TextView;
    .end local v6    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public addGreenDotResultView(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;)V
    .locals 18
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    .prologue
    .line 664
    if-eqz p1, :cond_0

    .line 665
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v14

    const v15, 0x7f0b0068

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    .line 666
    invoke-virtual/range {v14 .. v17}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 668
    .local v12, "resultHolderView":Landroid/view/View;
    const v14, 0x7f09020c

    invoke-static {v12, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 669
    .local v13, "titleTextView":Landroid/widget/TextView;
    const v14, 0x7f0e007f

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    .line 671
    const v14, 0x7f090066

    invoke-static {v12, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 672
    .local v7, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v14

    invoke-static {v14}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v14

    const v15, 0x7f0b0067

    const/16 v16, 0x0

    .line 673
    move/from16 v0, v16

    invoke-virtual {v14, v15, v7, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 675
    .local v9, "itemView":Landroid/view/View;
    const v14, 0x7f09020c

    invoke-static {v9, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 676
    .local v8, "itemTitleTextView":Landroid/widget/TextView;
    const v14, 0x7f090081

    invoke-static {v9, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 677
    .local v3, "durationTextView":Landroid/widget/TextView;
    const v14, 0x7f090132

    invoke-static {v9, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 679
    .local v11, "passCourseScoreTextView":Landroid/widget/TextView;
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->getConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v14

    invoke-virtual {v14}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v4

    .line 680
    .local v4, "duration":J
    new-instance v2, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;-><init>()V

    .line 681
    .local v2, "converterUtil":Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;
    invoke-virtual {v2, v4, v5}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v6

    .line 683
    .local v6, "formattedDuration":Ljava/lang/String;
    const v14, 0x7f0e00f0

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    const/16 v17, 0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v8, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 684
    const v14, 0x7f0e0088

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v6, v15, v16

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v15}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v3, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 685
    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/fragment/passcource/util/GreenDotScoreUtil;->getPassCourseScore(J)I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 686
    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 688
    const v14, 0x7f0900db

    invoke-static {v12, v14}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Lcom/github/mikephil/charting/charts/LineChart;

    .line 689
    .local v10, "lineChart":Lcom/github/mikephil/charting/charts/LineChart;
    const/16 v14, 0x8

    invoke-virtual {v10, v14}, Lcom/github/mikephil/charting/charts/LineChart;->setVisibility(I)V

    .line 691
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v14, v12}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 693
    .end local v2    # "converterUtil":Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;
    .end local v3    # "durationTextView":Landroid/widget/TextView;
    .end local v4    # "duration":J
    .end local v6    # "formattedDuration":Ljava/lang/String;
    .end local v7    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v8    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v9    # "itemView":Landroid/view/View;
    .end local v10    # "lineChart":Lcom/github/mikephil/charting/charts/LineChart;
    .end local v11    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v12    # "resultHolderView":Landroid/view/View;
    .end local v13    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public addLineOfSightResultView(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/List;)V
    .locals 18
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 345
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;>;"
    if-eqz p1, :cond_0

    .line 346
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    const v13, 0x7f0b0068

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    const/4 v15, 0x0

    .line 347
    invoke-virtual {v12, v13, v14, v15}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 349
    .local v10, "resultHolderView":Landroid/view/View;
    const v12, 0x7f09020c

    invoke-static {v10, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 350
    .local v11, "titleTextView":Landroid/widget/TextView;
    const v12, 0x7f0e00a6

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 352
    const v12, 0x7f090066

    invoke-static {v10, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 353
    .local v4, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-static {v12}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v12

    const v13, 0x7f0b0069

    const/4 v14, 0x0

    .line 354
    invoke-virtual {v12, v13, v4, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 356
    .local v6, "itemView":Landroid/view/View;
    const v12, 0x7f09020c

    invoke-static {v6, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 357
    .local v5, "itemTitleTextView":Landroid/widget/TextView;
    const v12, 0x7f090108

    invoke-static {v6, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 358
    .local v7, "mistakesTextView":Landroid/widget/TextView;
    const v12, 0x7f09009d

    invoke-static {v6, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 359
    .local v3, "foundMistakesTextView":Landroid/widget/TextView;
    const v12, 0x7f090006

    invoke-static {v6, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 360
    .local v2, "accuracyTextView":Landroid/widget/TextView;
    const v12, 0x7f090132

    invoke-static {v6, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 362
    .local v9, "passCourseScoreTextView":Landroid/widget/TextView;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v12

    invoke-static {v12}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v8

    .line 363
    .local v8, "numberFormat":Ljava/text/NumberFormat;
    const/4 v12, 0x1

    invoke-virtual {v8, v12}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 365
    const v12, 0x7f0e00f0

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 366
    const v12, 0x7f0e00ac

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 367
    const v12, 0x7f0e00ab

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v15

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v3, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 368
    const v12, 0x7f0e00aa

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakesAccuracy()F

    move-result v15

    float-to-double v0, v15

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v8, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v13, v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 369
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v13

    invoke-static {v12, v13}, Lcom/safonov/speedreading/training/fragment/passcource/util/LineOfSightScoreUtil;->getPassCourseScore(II)I

    move-result v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v9, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 371
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 419
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v12, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 421
    .end local v2    # "accuracyTextView":Landroid/widget/TextView;
    .end local v3    # "foundMistakesTextView":Landroid/widget/TextView;
    .end local v4    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v5    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v6    # "itemView":Landroid/view/View;
    .end local v7    # "mistakesTextView":Landroid/widget/TextView;
    .end local v8    # "numberFormat":Ljava/text/NumberFormat;
    .end local v9    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v10    # "resultHolderView":Landroid/view/View;
    .end local v11    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public addPassCourseResultView(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/List;)V
    .locals 22
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 697
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;>;"
    if-eqz p1, :cond_1

    .line 698
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v18

    const v19, 0x7f0b0068

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    .line 699
    invoke-virtual/range {v18 .. v21}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    .line 701
    .local v14, "resultHolderView":Landroid/view/View;
    const v18, 0x7f09020c

    move/from16 v0, v18

    invoke-static {v14, v0}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 702
    .local v16, "titleTextView":Landroid/widget/TextView;
    const v18, 0x7f0e00df

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 704
    const v18, 0x7f090066

    move/from16 v0, v18

    invoke-static {v14, v0}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 705
    .local v8, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v18

    const v19, 0x7f0b006a

    const/16 v20, 0x0

    .line 706
    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v0, v1, v8, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v10

    .line 708
    .local v10, "itemView":Landroid/view/View;
    const v18, 0x7f09020c

    move/from16 v0, v18

    invoke-static {v10, v0}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 709
    .local v9, "itemTitleTextView":Landroid/widget/TextView;
    const v18, 0x7f090132

    move/from16 v0, v18

    invoke-static {v10, v0}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 710
    .local v13, "passCourseScoreTextView":Landroid/widget/TextView;
    const v18, 0x7f0e00ed

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(I)V

    .line 711
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->getScore()I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v18 .. v18}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 712
    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 715
    const v18, 0x7f0900db

    move/from16 v0, v18

    invoke-static {v14, v0}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Lcom/github/mikephil/charting/charts/LineChart;

    .line 716
    .local v12, "lineChart":Lcom/github/mikephil/charting/charts/LineChart;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/github/mikephil/charting/charts/LineChart;->setVisibility(I)V

    .line 717
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 719
    .local v3, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v18

    move/from16 v0, v18

    if-ge v7, v0, :cond_0

    .line 720
    new-instance v19, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v0, v7

    move/from16 v20, v0

    move-object/from16 v0, p2

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual/range {v18 .. v18}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->getScore()I

    move-result v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    move-object/from16 v0, v19

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 719
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 723
    :cond_0
    new-instance v5, Lcom/github/mikephil/charting/data/LineDataSet;

    const v18, 0x7f0e00ee

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-direct {v5, v3, v0}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 724
    .local v5, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartLine1Color:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 725
    sget-object v18, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 726
    move-object/from16 v0, p0

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartLineWidthDp:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 727
    move-object/from16 v0, p0

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItemValueTextSizeDp:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 728
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 729
    move-object/from16 v0, p0

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItem1Color:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 730
    move-object/from16 v0, p0

    iget v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItemCircleRadiusDp:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 731
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 732
    new-instance v18, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$1;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;)V

    move-object/from16 v0, v18

    invoke-virtual {v5, v0}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 739
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 740
    .local v6, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    new-instance v4, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v4, v6}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 744
    .local v4, "data":Lcom/github/mikephil/charting/data/LineData;
    invoke-virtual {v12}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v17

    .line 745
    .local v17, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v18, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual/range {v17 .. v18}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 746
    const/high16 v18, 0x3f800000    # 1.0f

    invoke-virtual/range {v17 .. v18}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 748
    invoke-virtual {v12}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v11

    .line 749
    .local v11, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 750
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v11, v0}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 752
    invoke-virtual {v12}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v15

    .line 753
    .local v15, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 754
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 756
    const/16 v18, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 757
    invoke-virtual {v12, v4}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 758
    invoke-virtual {v12}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 760
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v14}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 762
    .end local v3    # "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    .end local v4    # "data":Lcom/github/mikephil/charting/data/LineData;
    .end local v5    # "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    .end local v6    # "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    .end local v7    # "i":I
    .end local v8    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v9    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v10    # "itemView":Landroid/view/View;
    .end local v11    # "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    .end local v12    # "lineChart":Lcom/github/mikephil/charting/charts/LineChart;
    .end local v13    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v14    # "resultHolderView":Landroid/view/View;
    .end local v15    # "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    .end local v16    # "titleTextView":Landroid/widget/TextView;
    .end local v17    # "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    :cond_1
    return-void
.end method

.method public addRememberNumberResultView(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/List;)V
    .locals 13
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;>;"
    const v12, 0x7f09020c

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 273
    if-eqz p1, :cond_0

    .line 274
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b0068

    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    .line 275
    invoke-virtual {v7, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 277
    .local v4, "resultHolderView":Landroid/view/View;
    invoke-static {v4, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 278
    .local v6, "titleTextView":Landroid/widget/TextView;
    const v7, 0x7f0e014a

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 280
    const v7, 0x7f090066

    invoke-static {v4, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 281
    .local v0, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b006b

    .line 282
    invoke-virtual {v7, v8, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 284
    .local v2, "itemView":Landroid/view/View;
    invoke-static {v2, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 285
    .local v1, "itemTitleTextView":Landroid/widget/TextView;
    const v7, 0x7f090188

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 286
    .local v5, "scoreTextView":Landroid/widget/TextView;
    const v7, 0x7f090132

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 288
    .local v3, "passCourseScoreTextView":Landroid/widget/TextView;
    const v7, 0x7f0e00f0

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    const v7, 0x7f0e0151

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 290
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v7

    invoke-static {v7}, Lcom/safonov/speedreading/training/fragment/passcource/util/RememberNumberScoreUtil;->getPassCourseScore(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 339
    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 341
    .end local v0    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v1    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v2    # "itemView":Landroid/view/View;
    .end local v3    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v4    # "resultHolderView":Landroid/view/View;
    .end local v5    # "scoreTextView":Landroid/widget/TextView;
    .end local v6    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public addSchulteTableResultView(Ljava/util/List;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 164
    .local p1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f0b0068

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    const/4 v7, 0x0

    .line 165
    invoke-virtual {v4, v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 167
    .local v2, "resultHolderView":Landroid/view/View;
    const v4, 0x7f09020c

    invoke-static {v2, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 168
    .local v3, "titleTextView":Landroid/widget/TextView;
    const v4, 0x7f0e0167

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 170
    const v4, 0x7f090066

    invoke-static {v2, v4}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 172
    .local v0, "contentHolderLayout":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;)V

    .line 173
    .local v1, "recyclerView":Landroid/support/v7/widget/RecyclerView;
    new-instance v4, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 174
    new-instance v4, Landroid/support/v7/widget/DividerItemDecoration;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct {v4, v5, v6}, Landroid/support/v7/widget/DividerItemDecoration;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->addItemDecoration(Landroid/support/v7/widget/RecyclerView$ItemDecoration;)V

    .line 175
    new-instance v4, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;

    invoke-direct {v4, p0, p1}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;Ljava/util/List;)V

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 176
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 226
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 227
    return-void
.end method

.method public addSpeedReadingResultView(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/List;)V
    .locals 12
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 425
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;>;"
    if-eqz p1, :cond_0

    .line 426
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0b0068

    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    .line 427
    invoke-virtual {v8, v9, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 429
    .local v6, "resultHolderView":Landroid/view/View;
    const v8, 0x7f09020c

    invoke-static {v6, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 430
    .local v7, "titleTextView":Landroid/widget/TextView;
    const v8, 0x7f0e0183

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(I)V

    .line 432
    const v8, 0x7f090066

    invoke-static {v6, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 433
    .local v1, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    const v9, 0x7f0b006d

    const/4 v10, 0x0

    .line 434
    invoke-virtual {v8, v9, v1, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 436
    .local v3, "itemView":Landroid/view/View;
    const v8, 0x7f09020c

    invoke-static {v3, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 437
    .local v2, "itemTitleTextView":Landroid/widget/TextView;
    const v8, 0x7f090103

    invoke-static {v3, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 438
    .local v4, "maxSpeedTextView":Landroid/widget/TextView;
    const v8, 0x7f090030

    invoke-static {v3, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 439
    .local v0, "averageSpeedTextView":Landroid/widget/TextView;
    const v8, 0x7f090132

    invoke-static {v3, v8}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 441
    .local v5, "passCourseScoreTextView":Landroid/widget/TextView;
    const v8, 0x7f0e00f0

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 442
    const v8, 0x7f0e0192

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 443
    const v8, 0x7f0e0191

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {p0, v8, v9}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 444
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v8

    invoke-static {v8}, Lcom/safonov/speedreading/training/fragment/passcource/util/SpeedReadingScoreUtil;->getPassCourseScore(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 445
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 513
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v8, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 515
    .end local v0    # "averageSpeedTextView":Landroid/widget/TextView;
    .end local v1    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v2    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v3    # "itemView":Landroid/view/View;
    .end local v4    # "maxSpeedTextView":Landroid/widget/TextView;
    .end local v5    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v6    # "resultHolderView":Landroid/view/View;
    .end local v7    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public addWordPairsResultView(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/List;)V
    .locals 13
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "chartResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;>;"
    const v12, 0x7f09020c

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 519
    if-eqz p1, :cond_0

    .line 520
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b0068

    iget-object v9, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    .line 521
    invoke-virtual {v7, v8, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 523
    .local v4, "resultHolderView":Landroid/view/View;
    invoke-static {v4, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 524
    .local v6, "titleTextView":Landroid/widget/TextView;
    const v7, 0x7f0e01a6

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 526
    const v7, 0x7f090066

    invoke-static {v4, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 527
    .local v0, "itemHolderLayout":Landroid/widget/LinearLayout;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f0b006e

    .line 528
    invoke-virtual {v7, v8, v0, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 530
    .local v2, "itemView":Landroid/view/View;
    invoke-static {v2, v12}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 531
    .local v1, "itemTitleTextView":Landroid/widget/TextView;
    const v7, 0x7f090188

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 532
    .local v5, "scoreTextView":Landroid/widget/TextView;
    const v7, 0x7f090132

    invoke-static {v2, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 534
    .local v3, "passCourseScoreTextView":Landroid/widget/TextView;
    const v7, 0x7f0e00f0

    new-array v8, v11, [Ljava/lang/Object;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 535
    const v7, 0x7f0e01ad

    new-array v8, v11, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {p0, v7, v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 536
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v7

    invoke-static {v7}, Lcom/safonov/speedreading/training/fragment/passcource/util/WordPairsScoreUtil;->getPassCourseScore(I)I

    move-result v7

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 537
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 585
    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 587
    .end local v0    # "itemHolderLayout":Landroid/widget/LinearLayout;
    .end local v1    # "itemTitleTextView":Landroid/widget/TextView;
    .end local v2    # "itemView":Landroid/view/View;
    .end local v3    # "passCourseScoreTextView":Landroid/widget/TextView;
    .end local v4    # "resultHolderView":Landroid/view/View;
    .end local v5    # "scoreTextView":Landroid/widget/TextView;
    .end local v6    # "titleTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;
    .locals 10
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/application/App;

    .line 91
    .local v9, "app":Lcom/safonov/speedreading/application/App;
    new-instance v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getPassCourseConfiguration()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    .line 92
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getSchulteTableRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 93
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getRememberNumberRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    .line 94
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getLineOfSightRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 95
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getEvenNumbersRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 96
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getWordPairsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .line 97
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getSpeedReadingRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 98
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v9}, Lcom/safonov/speedreading/application/App;->getGreenDotRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 100
    new-instance v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-direct/range {v0 .. v8}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V

    return-object v0
.end method

.method public dismissProgressDialog()V
    .locals 1

    .prologue
    .line 774
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->progressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 775
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "pass_course_result_array"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->resultIds:[I

    .line 131
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 140
    const v1, 0x7f0b0065

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 142
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 144
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 779
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 781
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->close()V

    .line 782
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;->close()V

    .line 783
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->close()V

    .line 784
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->close()V

    .line 785
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->close()V

    .line 786
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->close()V

    .line 787
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->close()V

    .line 788
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->close()V

    .line 789
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 793
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 794
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 795
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 149
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->resultIds:[I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;->init([I)V

    .line 151
    return-void
.end method

.method public showProgressDialog()V
    .locals 2

    .prologue
    .line 768
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->progressDialog:Landroid/app/ProgressDialog;

    .line 769
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->progressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 770
    return-void
.end method
