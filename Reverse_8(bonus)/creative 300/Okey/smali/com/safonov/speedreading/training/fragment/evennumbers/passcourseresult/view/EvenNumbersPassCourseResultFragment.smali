.class public Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "EvenNumbersPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/IEvenNumbersPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090036
    .end annotation
.end field

.field private evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field newBestScoreView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011a
    .end annotation
.end field

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field private resultId:I

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090175
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 44
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;-><init>()V

    .line 51
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getEvenNumbersRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/EvenNumbersPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 121
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 122
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 127
    return-void

    .line 124
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->resultId:I

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const v1, 0x7f0b0037

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 77
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 78
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 138
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->close()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 140
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 147
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 132
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 133
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 114
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/presenter/IEvenNumbersPassCourseResultPresenter;->initTrainingResults(I)V

    .line 85
    return-void
.end method

.method public setNewBestScoreViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 99
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 100
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method
