.class public Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
.super Lio/realm/RealmObject;
.source "LineOfSightConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/LineOfSightConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COLUMN_COUNT:Ljava/lang/String; = "columnCount"

.field public static final FIELD_MISTAKE_PROBABILITY:Ljava/lang/String; = "mistakeProbability"

.field public static final FIELD_ROW_COUNT:Ljava/lang/String; = "rowCount"

.field public static final FIELD_SHOW_COUNT:Ljava/lang/String; = "showCount"

.field public static final FIELD_SHOW_DELAY:Ljava/lang/String; = "showDelay"

.field public static final FIELD_TYPE:Ljava/lang/String; = "fieldType"

.field public static final FIELD_TYPE_LARGE:I = 0x1

.field public static final FIELD_TYPE_SMALL:I


# instance fields
.field private columnCount:I

.field private fieldType:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private mistakeProbability:I

.field private rowCount:I

.field private showCount:I

.field private showDelay:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$columnCount()I

    move-result v0

    return v0
.end method

.method public getFieldType()I
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$fieldType()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getMistakeProbability()I
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$mistakeProbability()I

    move-result v0

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$rowCount()I

    move-result v0

    return v0
.end method

.method public getShowCount()I
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$showCount()I

    move-result v0

    return v0
.end method

.method public getShowDelay()J
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmGet$showDelay()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$columnCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->columnCount:I

    return v0
.end method

.method public realmGet$fieldType()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->fieldType:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->id:I

    return v0
.end method

.method public realmGet$mistakeProbability()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->mistakeProbability:I

    return v0
.end method

.method public realmGet$rowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->rowCount:I

    return v0
.end method

.method public realmGet$showCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->showCount:I

    return v0
.end method

.method public realmGet$showDelay()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->showDelay:J

    return-wide v0
.end method

.method public realmSet$columnCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->columnCount:I

    return-void
.end method

.method public realmSet$fieldType(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->fieldType:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->id:I

    return-void
.end method

.method public realmSet$mistakeProbability(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->mistakeProbability:I

    return-void
.end method

.method public realmSet$rowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->rowCount:I

    return-void
.end method

.method public realmSet$showCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->showCount:I

    return-void
.end method

.method public realmSet$showDelay(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->showDelay:J

    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$columnCount(I)V

    .line 59
    return-void
.end method

.method public setFieldType(I)V
    .locals 0
    .param p1, "fieldType"    # I

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$fieldType(I)V

    .line 67
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$id(I)V

    .line 43
    return-void
.end method

.method public setMistakeProbability(I)V
    .locals 0
    .param p1, "mistakeProbability"    # I

    .prologue
    .line 90
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$mistakeProbability(I)V

    .line 91
    return-void
.end method

.method public setRowCount(I)V
    .locals 0
    .param p1, "rowCount"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$rowCount(I)V

    .line 51
    return-void
.end method

.method public setShowCount(I)V
    .locals 0
    .param p1, "showCount"    # I

    .prologue
    .line 74
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$showCount(I)V

    .line 75
    return-void
.end method

.method public setShowDelay(J)V
    .locals 1
    .param p1, "showDelay"    # J

    .prologue
    .line 82
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->realmSet$showDelay(J)V

    .line 83
    return-void
.end method
