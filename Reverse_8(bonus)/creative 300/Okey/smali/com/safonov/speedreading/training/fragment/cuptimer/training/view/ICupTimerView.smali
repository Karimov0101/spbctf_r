.class public interface abstract Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;
.super Ljava/lang/Object;
.source "ICupTimerView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;


# virtual methods
.method public abstract hideImage()V
.end method

.method public abstract hideNotification()V
.end method

.method public abstract hideProgressBar()V
.end method

.method public abstract setImage(I)V
.end method

.method public abstract setMaxProgress(J)V
.end method

.method public abstract setProgress(I)V
.end method

.method public abstract showImage()V
.end method

.method public abstract showNotification(I)V
.end method
