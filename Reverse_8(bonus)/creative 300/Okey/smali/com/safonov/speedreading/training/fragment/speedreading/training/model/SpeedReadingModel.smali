.class public Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;
.super Ljava/lang/Object;
.source "SpeedReadingModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;


# instance fields
.field private words:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;->words:[Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public createItems(I)Ljava/util/List;
    .locals 6
    .param p1, "itemsCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 27
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;->words:[Ljava/lang/String;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    invoke-static {v3, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;->generateUniqueIndexes(II)[I

    move-result-object v1

    .line 28
    .local v1, "indexes":[I
    array-length v4, v1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v4, :cond_0

    aget v0, v1, v3

    .line 29
    .local v0, "index":I
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;->words:[Ljava/lang/String;

    aget-object v5, v5, v0

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 28
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 32
    .end local v0    # "index":I
    :cond_0
    return-object v2
.end method
