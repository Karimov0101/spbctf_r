.class public Lcom/safonov/speedreading/training/fragment/passcource/util/GreenDotScoreUtil;
.super Ljava/lang/Object;
.source "GreenDotScoreUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseScore(J)I
    .locals 2
    .param p0, "duration"    # J

    .prologue
    .line 9
    long-to-int v0, p0

    sparse-switch v0, :sswitch_data_0

    .line 15
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 10
    :sswitch_0
    const/16 v0, 0xa

    goto :goto_0

    .line 11
    :sswitch_1
    const/16 v0, 0x14

    goto :goto_0

    .line 12
    :sswitch_2
    const/16 v0, 0x1e

    goto :goto_0

    .line 13
    :sswitch_3
    const/16 v0, 0x28

    goto :goto_0

    .line 14
    :sswitch_4
    const/16 v0, 0x3c

    goto :goto_0

    .line 9
    nop

    :sswitch_data_0
    .sparse-switch
        0x1388 -> :sswitch_0
        0x2710 -> :sswitch_1
        0x3a98 -> :sswitch_2
        0x4e20 -> :sswitch_3
        0x7530 -> :sswitch_4
    .end sparse-switch
.end method
