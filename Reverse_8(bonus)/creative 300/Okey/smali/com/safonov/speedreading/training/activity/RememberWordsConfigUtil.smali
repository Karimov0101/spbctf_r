.class public Lcom/safonov/speedreading/training/activity/RememberWordsConfigUtil;
.super Ljava/lang/Object;
.source "RememberWordsConfigUtil.java"


# static fields
.field public static final DEFAULT_SHOW_WORDS_COUNT:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .locals 2

    .prologue
    .line 15
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;-><init>()V

    .line 17
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->setStartWordsCount(I)V

    .line 19
    return-object v0
.end method
