.class public Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
.super Lio/realm/RealmObject;
.source "RememberWordsResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/RememberWordsResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->score:I

    return v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->score:I

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V

    .line 45
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmSet$id(I)V

    .line 24
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->realmSet$score(I)V

    .line 37
    return-void
.end method
