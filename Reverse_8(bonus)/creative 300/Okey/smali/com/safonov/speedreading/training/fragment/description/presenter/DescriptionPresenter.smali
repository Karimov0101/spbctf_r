.class public Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "DescriptionPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/description/presenter/IDescriptionPresenter;"
    }
.end annotation


# instance fields
.field private trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 21
    new-instance v0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    invoke-direct {v0, p1}, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    .line 22
    return-void
.end method


# virtual methods
.method public requestToLoadContent(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 3
    .param p1, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const v2, 0x7f0b001b

    .line 26
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    sget-object v0, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter$1;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 29
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 32
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 35
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 39
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b00a2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 42
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b0099

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 45
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b0044

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 48
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b00a9

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 51
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b00b8

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 54
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b0035

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 57
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b003d

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto :goto_0

    .line 60
    :pswitch_a
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b004b

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 63
    :pswitch_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b0020

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 66
    :pswitch_c
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b0024

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 69
    :pswitch_d
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b009d

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 72
    :pswitch_e
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;

    const v1, 0x7f0b00b2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/description/view/IDescriptionView;->setContentLayout(I)V

    goto/16 :goto_0

    .line 27
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public setDontShowAgain(Lcom/safonov/speedreading/training/FragmentType;Z)V
    .locals 2
    .param p1, "type"    # Lcom/safonov/speedreading/training/FragmentType;
    .param p2, "dontShowAgaing"    # Z

    .prologue
    .line 80
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/description/presenter/DescriptionPresenter;->trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->setShowDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;Z)V

    .line 81
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
