.class Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;
.super Ljava/lang/Object;
.source "NumberWrapperGenerator.java"


# static fields
.field private static final random:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static createNumberWrappers(III)Ljava/util/List;
    .locals 6
    .param p0, "digitPerNumber"    # I
    .param p1, "numbersCount"    # I
    .param p2, "evenNumbersCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 17
    if-le p2, p1, :cond_0

    .line 18
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "evenNumbersCount must be no greater than numbersCount"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 20
    :cond_0
    if-ge p0, v4, :cond_1

    .line 21
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "digitPerNumber must be greater than 1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 24
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 26
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sub-int v2, p1, p2

    if-ge v0, v2, :cond_2

    .line 27
    new-instance v2, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;

    invoke-static {p0, v5}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->getRandomNumber(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v5}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 26
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 30
    :cond_2
    sub-int v0, p1, p2

    :goto_1
    if-ge v0, p1, :cond_3

    .line 31
    new-instance v2, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;

    invoke-static {p0, v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->getRandomNumber(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 34
    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 36
    return-object v1
.end method

.method private static getRandomNumber(IZ)Ljava/lang/String;
    .locals 6
    .param p0, "length"    # I
    .param p1, "isEvenNumber"    # Z

    .prologue
    const/16 v5, 0xa

    .line 40
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 42
    .local v0, "builder":Ljava/lang/StringBuilder;
    sget-object v4, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 43
    .local v1, "firstDigit":I
    :goto_0
    if-nez v1, :cond_0

    .line 44
    sget-object v4, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    goto :goto_0

    .line 46
    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    const/4 v2, 0x1

    .local v2, "i":I
    :goto_1
    add-int/lit8 v4, p0, -0x1

    if-ge v2, v4, :cond_1

    .line 49
    sget-object v4, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 48
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 52
    :cond_1
    sget-object v4, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 53
    .local v3, "lastDigit":I
    :goto_2
    rem-int/lit8 v4, v3, 0x2

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_3
    if-eq v4, p1, :cond_3

    .line 54
    sget-object v4, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->random:Ljava/util/Random;

    invoke-virtual {v4, v5}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    goto :goto_2

    .line 53
    :cond_2
    const/4 v4, 0x0

    goto :goto_3

    .line 56
    :cond_3
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 58
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method
