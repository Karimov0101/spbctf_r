.class public Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;
.super Ljava/lang/Object;
.source "FlashWordsPassCourseResultFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

.field private view2131296542:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v4, 0x7f09011e

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

    .line 27
    const v1, 0x7f090081

    const-string v2, "field \'durationTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    .line 28
    const-string v1, "field \'nextButton\' and method \'onNextClick\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'nextButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    .line 30
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->view2131296542:Landroid/view/View;

    .line 31
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

    .line 43
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 44
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

    .line 46
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    .line 47
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    .line 49
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->view2131296542:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment_ViewBinding;->view2131296542:Landroid/view/View;

    .line 51
    return-void
.end method
