.class public Lcom/safonov/speedreading/training/activity/WordPairsConfigUtil;
.super Ljava/lang/Object;
.source "WordPairsConfigUtil.java"


# static fields
.field private static final DEFAULT_COLUMN_COUNT:I = 0x3

.field private static final DEFAULT_DIFFERENT_WORD_PAIRS_COUNT:I = 0x5

.field private static final DEFAULT_ROW_COUNT:I = 0x6

.field private static final DEFAULT_TRAINING_TIME:I = 0x1d4c0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;-><init>()V

    .line 21
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setRowCount(I)V

    .line 22
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setColumnCount(I)V

    .line 23
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setDifferentWordPairsCount(I)V

    .line 24
    const v1, 0x1d4c0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setTrainingDuration(I)V

    .line 26
    return-object v0
.end method
