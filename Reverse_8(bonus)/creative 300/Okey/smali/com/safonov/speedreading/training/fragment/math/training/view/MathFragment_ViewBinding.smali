.class public Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;
.super Ljava/lang/Object;
.source "MathFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

.field private view2131296504:Landroid/view/View;

.field private view2131296505:Landroid/view/View;

.field private view2131296506:Landroid/view/View;

.field private view2131296507:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;Landroid/view/View;)V
    .locals 9
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v8, 0x7f0900fb

    const v7, 0x7f0900fa

    const v6, 0x7f0900f9

    const v5, 0x7f0900f8

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    .line 33
    const v1, 0x7f0900fd

    const-string v2, "field \'expressionTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f090100

    const-string v2, "field \'scoreTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->scoreTextView:Landroid/widget/TextView;

    .line 35
    const v1, 0x7f0900ff

    const-string v2, "field \'recordTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->recordTextView:Landroid/widget/TextView;

    .line 36
    const v1, 0x7f0900fe

    const-string v2, "field \'progressBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 37
    const v1, 0x7f090102

    const-string v2, "field \'timerBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 38
    const v1, 0x7f0900f4

    const-string v2, "field \'mathPointsTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    .line 39
    const v1, 0x7f0900f3

    const-string v2, "field \'correctAnswerTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->correctAnswerTextView:Landroid/widget/TextView;

    .line 40
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v5, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296504:Landroid/view/View;

    .line 42
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 48
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v6, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 49
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296505:Landroid/view/View;

    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v7, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 57
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296506:Landroid/view/View;

    .line 58
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v8, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 65
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296507:Landroid/view/View;

    .line 66
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    const/4 v1, 0x4

    new-array v2, v1, [Landroid/widget/Button;

    const/4 v3, 0x0

    const-string v1, "field \'buttons\'"

    const-class v4, Landroid/widget/Button;

    .line 73
    invoke-static {p2, v5, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v2, v3

    const/4 v3, 0x1

    const-string v1, "field \'buttons\'"

    const-class v4, Landroid/widget/Button;

    .line 74
    invoke-static {p2, v6, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v2, v3

    const/4 v3, 0x2

    const-string v1, "field \'buttons\'"

    const-class v4, Landroid/widget/Button;

    .line 75
    invoke-static {p2, v7, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v2, v3

    const/4 v3, 0x3

    const-string v1, "field \'buttons\'"

    const-class v4, Landroid/widget/Button;

    .line 76
    invoke-static {p2, v8, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aput-object v1, v2, v3

    .line 72
    invoke-static {v2}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->buttons:Ljava/util/List;

    .line 77
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 82
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    .line 83
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 84
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    .line 86
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    .line 87
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->scoreTextView:Landroid/widget/TextView;

    .line 88
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->recordTextView:Landroid/widget/TextView;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->correctAnswerTextView:Landroid/widget/TextView;

    .line 93
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->buttons:Ljava/util/List;

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296504:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296504:Landroid/view/View;

    .line 97
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296505:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296505:Landroid/view/View;

    .line 99
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296506:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296506:Landroid/view/View;

    .line 101
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296507:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment_ViewBinding;->view2131296507:Landroid/view/View;

    .line 103
    return-void
.end method
