.class Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;
.super Ljava/lang/Object;
.source "WordPairsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;-><init>()V

    .line 63
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setScore(I)V

    .line 64
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setUnixTime(J)V

    .line 66
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->addResult(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V

    .line 72
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "playedTime"    # J

    .prologue
    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getTrainingDuration()I

    move-result v1

    int-to-long v2, v1

    sub-long/2addr v2, p1

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;->updateProgressBar(I)V

    .line 58
    :cond_0
    return-void
.end method
