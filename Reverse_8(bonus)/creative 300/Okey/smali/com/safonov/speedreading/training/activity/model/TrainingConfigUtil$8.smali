.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnMultiTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetAcceleratorCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 383
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted([I)V
    .locals 8
    .param p1, "ids"    # [I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 386
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v5, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v4, v5, v7, v7, v6}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v2

    .line 387
    .local v2, "fragmentTypeListWithDescription":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v5, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v4, v5, v6, v7, v6}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 389
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 390
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    sget-object v4, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 391
    aget v4, p1, v6

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 392
    invoke-virtual {v0, v6}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 394
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    aput-object v5, v4, v6

    .line 395
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    aput-object v5, v4, v6

    .line 397
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_0
    array-length v4, p1

    if-ge v3, v4, :cond_0

    .line 398
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .end local v0    # "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    invoke-direct {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 399
    .restart local v0    # "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    sget-object v4, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 400
    aget v4, p1, v3

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 401
    invoke-virtual {v0, v6}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 403
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    aput-object v5, v4, v3

    .line 397
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 406
    :cond_0
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 407
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 409
    :cond_1
    return-void
.end method
