.class public Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "PassCourseRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 22
    return-void
.end method


# virtual methods
.method public addResult(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;

    .prologue
    .line 50
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 52
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->setId(I)V

    .line 54
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil$2;

    invoke-direct {v3, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 67
    return-void
.end method

.method public getBestResult()Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 3
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 36
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    const/4 v1, 0x0

    .line 37
    invoke-virtual {v0, v1}, Lio/realm/RealmResults;->first(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 34
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 27
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 28
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 26
    return-object v0
.end method

.method public getResultList()Ljava/util/List;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 43
    return-object v0
.end method
