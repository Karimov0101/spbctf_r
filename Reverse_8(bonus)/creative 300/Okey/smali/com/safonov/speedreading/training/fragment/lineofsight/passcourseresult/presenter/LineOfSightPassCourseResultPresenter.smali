.class public Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "LineOfSightPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private final repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 4
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v1

    .line 30
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v2

    .line 31
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v3

    .line 29
    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/passcource/util/LineOfSightScoreUtil;->getPassCourseScore(II)I

    move-result v0

    .line 33
    .local v0, "passCoursePoints":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;->updateMistakesView(I)V

    .line 35
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;->updateFoundMistakesView(I)V

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakesAccuracy()F

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;->updateFoundMistakesPercentView(F)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;

    invoke-interface {v2, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 41
    :cond_0
    return-void
.end method
