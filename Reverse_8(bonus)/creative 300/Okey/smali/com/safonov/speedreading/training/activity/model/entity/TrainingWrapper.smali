.class public Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
.super Ljava/lang/Object;
.source "TrainingWrapper.java"


# instance fields
.field private final configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

.field private final fragmentTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/FragmentType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V
    .locals 0
    .param p1, "configWrapper"    # Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/FragmentType;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 21
    .local p2, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .line 23
    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->fragmentTypeList:Ljava/util/List;

    .line 24
    return-void
.end method


# virtual methods
.method public getConfigWrapper()Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    return-object v0
.end method

.method public getFragmentTypeList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/FragmentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->fragmentTypeList:Ljava/util/List;

    return-object v0
.end method
