.class public Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "MathComplexityPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;"
    }
.end annotation


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

.field private configId:I

.field private repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    .line 22
    return-void
.end method


# virtual methods
.method public bridge synthetic attachView(Lcom/hannesdorfmann/mosby3/mvp/MvpView;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->attachView(Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;)V

    return-void
.end method

.method public attachView(Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;)V
    .locals 0
    .param p1, "view"    # Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;

    .prologue
    .line 27
    return-void
.end method

.method public detachView(Z)V
    .locals 0
    .param p1, "retainInstance"    # Z

    .prologue
    .line 32
    return-void
.end method

.method public initConfig(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->configId:I

    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 43
    return-void
.end method

.method public setComplexity(I)V
    .locals 2
    .param p1, "complexity"    # I

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->repository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;->configId:I

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->updateConfigComplexity(II)V

    .line 37
    return-void
.end method
