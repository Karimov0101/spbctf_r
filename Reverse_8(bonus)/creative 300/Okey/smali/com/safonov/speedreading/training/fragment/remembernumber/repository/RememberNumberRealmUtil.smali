.class public Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "RememberNumberRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;

    .prologue
    .line 111
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "trainingShowCount"

    .line 112
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getTrainingShowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 113
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 114
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "minComplexity"

    .line 115
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMinComplexity()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 116
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 117
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "maxComplexity"

    .line 118
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMaxComplexity()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 119
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 120
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "answersToComplexityUp"

    .line 121
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityUp()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 122
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 123
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "answersToComplexityDown"

    .line 124
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityDown()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 125
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 128
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 129
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 151
    :goto_0
    return-void

    .line 134
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 136
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setId(I)V

    .line 138
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$5;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;

    .prologue
    .line 156
    const-class v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 158
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 159
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 162
    aget-object v0, p1, v3

    .line 164
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "trainingShowCount"

    .line 165
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getTrainingShowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 166
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 167
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "minComplexity"

    .line 168
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMinComplexity()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 169
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 170
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "maxComplexity"

    .line 171
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getMaxComplexity()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 172
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 173
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "answersToComplexityUp"

    .line 174
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityUp()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 175
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 176
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "answersToComplexityDown"

    .line 177
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getAnswersToComplexityDown()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 178
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 180
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 182
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 161
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 185
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setId(I)V

    .line 186
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 187
    aput v4, v1, v3

    .line 189
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 193
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 194
    if-eqz p2, :cond_2

    .line 195
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 214
    :cond_2
    :goto_2
    return-void

    .line 201
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$6;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$7;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ILcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;

    .prologue
    .line 67
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 69
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;ILcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 89
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 45
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 42
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 53
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .line 35
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 28
    return-object v0
.end method

.method public updateConfigComplexity(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "complexity"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 106
    return-void
.end method
