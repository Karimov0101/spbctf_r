.class public interface abstract Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;
.super Ljava/lang/Object;
.source "IFlashWordsResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateTrainingDurationView(J)V
.end method
