.class Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;
.super Landroid/os/CountDownTimer;
.source "CupTimerPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startMainTimer(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

.field final synthetic val$time:J


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;JJJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 131
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    iput-wide p6, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->val$time:J

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->cancelTraining()V

    .line 140
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->saveResult()V

    .line 141
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->val$time:J

    sub-long/2addr v2, p1

    long-to-int v1, v2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->setProgress(I)V

    .line 135
    return-void
.end method
