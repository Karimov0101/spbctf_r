.class public Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "FlashWordsPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .line 18
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 4
    .param p1, "resultId"    # I

    .prologue
    .line 22
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v0

    .line 24
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;->getConfig()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getTrainingDuration()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;->updateTrainingDurationView(J)V

    .line 27
    :cond_0
    return-void
.end method
