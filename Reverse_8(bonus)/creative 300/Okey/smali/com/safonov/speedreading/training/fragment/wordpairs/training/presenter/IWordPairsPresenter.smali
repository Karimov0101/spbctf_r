.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;
.super Ljava/lang/Object;
.source "IWordPairsPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract init(I)V
.end method

.method public abstract onItemTouchDown(I)V
.end method

.method public abstract onItemTouchUp(I)V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract startTraining()V
.end method
