.class public Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "LineOfSightPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field foundMistakesPercentTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090006
    .end annotation
.end field

.field foundMistakesTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09009d
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field private lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

.field mistakesTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090108
    .end annotation
.end field

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field private numberFormat:Ljava/text/NumberFormat;

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field private resultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 49
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 55
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;-><init>()V

    .line 56
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 43
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getLineOfSightRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 45
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/LineOfSightPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V

    return-object v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 134
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 135
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 140
    return-void

    .line 137
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->resultId:I

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 81
    const v1, 0x7f0b0046

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 83
    .local v0, "view":Landroid/view/View;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->numberFormat:Ljava/text/NumberFormat;

    .line 84
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->numberFormat:Ljava/text/NumberFormat;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 86
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 88
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->close()V

    .line 153
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 155
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->numberFormat:Ljava/text/NumberFormat;

    .line 156
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 157
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 163
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 164
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 145
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 146
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 127
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/presenter/ILineOfSightPassCourseResultPresenter;->initTrainingResults(I)V

    .line 95
    return-void
.end method

.method public updateFoundMistakesPercentView(F)V
    .locals 4
    .param p1, "foundMistakesPercent"    # F

    .prologue
    .line 111
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->foundMistakesPercentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->numberFormat:Ljava/text/NumberFormat;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 112
    return-void
.end method

.method public updateFoundMistakesView(I)V
    .locals 2
    .param p1, "mistakesCount"    # I

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->foundMistakesTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method

.method public updateMistakesView(I)V
    .locals 2
    .param p1, "mistakesCount"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->mistakesTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    return-void
.end method
