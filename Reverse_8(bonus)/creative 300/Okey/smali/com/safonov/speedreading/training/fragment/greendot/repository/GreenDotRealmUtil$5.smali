.class Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil$5;
.super Ljava/lang/Object;
.source "GreenDotRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnMultiTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field final synthetic val$haveToSaveConfigList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .prologue
    .line 148
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil$5;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil$5;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil$5;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lio/realm/Realm;->copyToRealm(Ljava/lang/Iterable;)Ljava/util/List;

    .line 152
    return-void
.end method
