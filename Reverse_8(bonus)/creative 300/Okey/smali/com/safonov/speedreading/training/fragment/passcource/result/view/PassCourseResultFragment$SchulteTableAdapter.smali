.class Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "PassCourseResultFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SchulteTableAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    .local p2, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 245
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->itemList:Ljava/util/List;

    .line 246
    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->itemList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 229
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->onBindViewHolder(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;I)V
    .locals 10
    .param p1, "holder"    # Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 258
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->itemList:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .line 260
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    iget-object v1, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;->timeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    const v3, 0x7f0e0178

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    iget-object v1, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;->titTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    const v3, 0x7f0e00f0

    new-array v4, v9, [Ljava/lang/Object;

    add-int/lit8 v5, p2, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v1, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/passcource/util/SchulteTableScoreUtil;->getPassCourseScore(J)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 263
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b006c

    const/4 v3, 0x0

    .line 251
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 253
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment$SchulteTableAdapter;Landroid/view/View;)V

    return-object v1
.end method
