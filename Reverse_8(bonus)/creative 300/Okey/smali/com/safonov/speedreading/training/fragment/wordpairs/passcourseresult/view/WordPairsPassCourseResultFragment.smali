.class public Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "WordPairsPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090036
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field newBestScoreView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011a
    .end annotation
.end field

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090188
    .end annotation
.end field

.field private trainingResultId:I

.field private unbinder:Lbutterknife/Unbinder;

.field private wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 44
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;
    .locals 3
    .param p0, "trainingResultId"    # I

    .prologue
    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;-><init>()V

    .line 51
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getWordPairsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 123
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 124
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 129
    return-void

    .line 126
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->trainingResultId:I

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const v1, 0x7f0b00ba

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 80
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 140
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->close()V

    .line 141
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .line 143
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 144
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 148
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 151
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 133
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 135
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 116
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->trainingResultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;->initTrainingResults(I)V

    .line 87
    return-void
.end method

.method public setNewBestScoreViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 101
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 102
    return-void

    .line 101
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 91
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method
