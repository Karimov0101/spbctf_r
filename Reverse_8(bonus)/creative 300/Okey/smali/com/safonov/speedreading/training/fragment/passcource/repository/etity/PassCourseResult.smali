.class public Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
.super Lio/realm/RealmObject;
.source "PassCourseResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/PassCourseResultRealmProxyInterface;


# static fields
.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I

.field private type:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmGet$type()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->score:I

    return v0
.end method

.method public realmGet$type()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->type:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->score:I

    return-void
.end method

.method public realmSet$type(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->type:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->unixTime:J

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmSet$id(I)V

    .line 39
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmSet$score(I)V

    .line 29
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmSet$type(I)V

    .line 47
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->realmSet$unixTime(J)V

    .line 57
    return-void
.end method
