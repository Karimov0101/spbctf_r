.class public Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "AcceleratorCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;"
    }
.end annotation


# static fields
.field private static final COURSE_RESULT_ARRAY_ID_PARAM:Ljava/lang/String; = "course_result_id_array"


# instance fields
.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090081
    .end annotation
.end field

.field private flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

.field private resultIds:[I

.field private unbinder:Lbutterknife/Unbinder;

.field private wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

.field private wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance([I)Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;
    .locals 3
    .param p0, "resultIds"    # [I

    .prologue
    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;-><init>()V

    .line 51
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "course_result_id_array"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 53
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getWordsColumnsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getWordBlockRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    .line 41
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getFlashWordsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    .line 43
    new-instance v0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "course_result_id_array"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->resultIds:[I

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    const v1, 0x7f0b001c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 74
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 76
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 92
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;->close()V

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->close()V

    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;->close()V

    .line 96
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 102
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 81
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 82
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->resultIds:[I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;->initTrainingResults([I)V

    .line 83
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 5
    .param p1, "duration"    # J

    .prologue
    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0023

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToMinutes(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    return-void
.end method
