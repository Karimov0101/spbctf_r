.class Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;
.super Ljava/lang/Object;
.source "PassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->init([I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

.field final synthetic val$evenNumbersResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

.field final synthetic val$greenDotResult:Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

.field final synthetic val$lineOfSight1Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

.field final synthetic val$lineOfSight2Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

.field final synthetic val$lineOfSight3Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

.field final synthetic val$rememberNumber1Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

.field final synthetic val$rememberNumber2Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

.field final synthetic val$schulteTable1ResultList:Ljava/util/List;

.field final synthetic val$schulteTable2ResultList:Ljava/util/List;

.field final synthetic val$speedReading1Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

.field final synthetic val$speedReading2Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

.field final synthetic val$wordPairsResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;Ljava/util/List;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/List;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    .prologue
    .line 137
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$schulteTable1ResultList:Ljava/util/List;

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$rememberNumber1Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    iput-object p4, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight1Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    iput-object p5, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$speedReading1Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    iput-object p6, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$wordPairsResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    iput-object p7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$evenNumbersResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    iput-object p8, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$schulteTable2ResultList:Ljava/util/List;

    iput-object p9, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$rememberNumber2Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    iput-object p10, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight2Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    iput-object p11, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight3Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    iput-object p12, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$speedReading2Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    iput-object p13, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$greenDotResult:Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v4, 0x0

    .line 140
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;)Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v1

    .line 141
    .local v1, "passCourseResult":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;)Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    move-result-object v2

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;->getResultList()Ljava/util/List;

    move-result-object v0

    .line 143
    .local v0, "chartList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 144
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$schulteTable1ResultList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addSchulteTableResultView(Ljava/util/List;Ljava/util/List;)V

    .line 145
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$rememberNumber1Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addRememberNumberResultView(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/List;)V

    .line 146
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight1Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addLineOfSightResultView(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/List;)V

    .line 147
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$speedReading1Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addSpeedReadingResultView(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/List;)V

    .line 148
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$wordPairsResult:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addWordPairsResultView(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/List;)V

    .line 149
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$evenNumbersResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addEvenNumbersResultView(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/List;)V

    .line 151
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$schulteTable2ResultList:Ljava/util/List;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addSchulteTableResultView(Ljava/util/List;Ljava/util/List;)V

    .line 152
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$rememberNumber2Result:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addRememberNumberResultView(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/List;)V

    .line 153
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight2Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addLineOfSightResultView(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/List;)V

    .line 154
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$lineOfSight3Result:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addLineOfSightResultView(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/List;)V

    .line 155
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$speedReading2Result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-interface {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addSpeedReadingResultView(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/List;)V

    .line 156
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->val$greenDotResult:Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addGreenDotResultView(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;)V

    .line 158
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    invoke-static {v0}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->addPassCourseResultView(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/List;)V

    .line 160
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->dismissProgressDialog()V

    .line 162
    :cond_0
    return-void
.end method
