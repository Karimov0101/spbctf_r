.class public Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SpeedReadingResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/ISpeedReadingResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 14
    .param p1, "resultId"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v12, 0x0

    .line 27
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-interface {v10, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v5

    .line 28
    .local v5, "currentResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v13

    invoke-virtual {v13}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v13

    invoke-interface {v10, v13}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getBestResultByMaxSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v4

    .line 29
    .local v4, "bestMaxSpeedResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v13

    invoke-virtual {v13}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v13

    invoke-interface {v10, v13}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getBestResultByAverageSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v2

    .line 31
    .local v2, "bestAverageSpeedResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v8

    .line 32
    .local v8, "maxSpeed":I
    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v3

    .line 33
    .local v3, "bestMaxSpeed":I
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v10

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v13

    if-ne v10, v13, :cond_3

    move v7, v11

    .line 35
    .local v7, "isNewBestMaxSpeed":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->isViewAttached()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v8}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->updateMaxSpeedView(I)V

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->updateBestMaxSpeedView(I)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v7}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->setNewBestMaxSpeedViewVisibility(Z)V

    .line 41
    :cond_0
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v0

    .line 42
    .local v0, "averageSpeed":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v1

    .line 43
    .local v1, "bestAverageSpeed":I
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v10

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v13

    if-ne v10, v13, :cond_4

    move v6, v11

    .line 45
    .local v6, "isNewBestAverageSpeed":Z
    :goto_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->isViewAttached()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->updateAverageSpeedView(I)V

    .line 47
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->updateBestAverageSpeedView(I)V

    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-interface {v10, v6}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->setNewBestAverageSpeedViewVisibility(Z)V

    .line 50
    :cond_1
    iget-object v10, p0, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v11

    invoke-virtual {v11}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v11

    invoke-interface {v10, v11}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getResultList(I)Ljava/util/List;

    move-result-object v9

    .line 52
    .local v9, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->isViewAttached()Z

    move-result v10

    if-eqz v10, :cond_2

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/result/presenter/SpeedReadingResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;

    invoke-static {v9}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v11

    invoke-interface {v10, v11}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;->setChartViewData(Ljava/util/List;)V

    .line 55
    :cond_2
    return-void

    .end local v0    # "averageSpeed":I
    .end local v1    # "bestAverageSpeed":I
    .end local v6    # "isNewBestAverageSpeed":Z
    .end local v7    # "isNewBestMaxSpeed":Z
    .end local v9    # "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;>;"
    :cond_3
    move v7, v12

    .line 33
    goto :goto_0

    .restart local v0    # "averageSpeed":I
    .restart local v1    # "bestAverageSpeed":I
    .restart local v7    # "isNewBestMaxSpeed":Z
    :cond_4
    move v6, v12

    .line 43
    goto :goto_1
.end method
