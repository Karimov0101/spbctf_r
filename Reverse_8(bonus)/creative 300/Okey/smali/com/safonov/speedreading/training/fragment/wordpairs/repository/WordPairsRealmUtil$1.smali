.class Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;
.super Ljava/lang/Object;
.source "WordPairsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 72
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$configId:I

    .line 73
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 76
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setId(I)V

    .line 77
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V

    .line 79
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 80
    return-void
.end method
