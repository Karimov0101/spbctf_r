.class public Lcom/safonov/speedreading/training/activity/SpeedReadingConfigUtil;
.super Ljava/lang/Object;
.source "SpeedReadingConfigUtil.java"


# static fields
.field private static final DEFAULT_MAX_SHOW_WORD_COUNT:I = 0x14

.field private static final DEFAULT_MIN_SHOW_WORD_COUNT:I = 0xc

.field private static final DEFAULT_MIN_SPEED:I = 0x64

.field private static final DEFAULT_SPEED:I = 0xc8

.field private static final DEFAULT_SPEED_STEP:I = 0x32

.field private static final DEFAULT_TRAINING_SHOW_COUNT:I = 0x14


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .locals 3

    .prologue
    const/16 v2, 0x14

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;-><init>()V

    .line 42
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setTrainingShowCount(I)V

    .line 44
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setMinWordShowCount(I)V

    .line 45
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setMaxWordShowCount(I)V

    .line 47
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setMinSpeed(I)V

    .line 48
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setSpeed(I)V

    .line 49
    const/16 v1, 0x32

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setSpeedStep(I)V

    .line 51
    return-object v0
.end method
