.class public Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
.super Lio/realm/RealmObject;
.source "CupTimerResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/CupTimerResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->id:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->id:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V

    .line 48
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmSet$id(I)V

    .line 30
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 39
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->realmSet$unixTime(J)V

    .line 40
    return-void
.end method
