.class public Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "MathComplexityFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field complexityButton1:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900ef
    .end annotation
.end field

.field complexityButton2:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f0
    .end annotation
.end field

.field complexityButton3:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f1
    .end annotation
.end field

.field private config:Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

.field private configId:I

.field private fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

.field private mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 55
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;-><init>()V

    .line 56
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;
    .locals 3

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 50
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getMathRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/MathComplexityPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V

    return-object v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 118
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 119
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    if-eqz v0, :cond_0

    .line 120
    check-cast p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    .line 125
    return-void

    .line 122
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->configId:I

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    const v1, 0x7f0b004a

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 78
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->unbinder:Lbutterknife/Unbinder;

    .line 80
    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 85
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 86
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;->initConfig(I)V

    .line 87
    return-void
.end method

.method public setComplexityFirst(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900ef
        }
    .end annotation

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;->setComplexity(I)V

    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;->onMathComplexityStartClick()V

    .line 96
    return-void
.end method

.method public setComplexityFourth(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900f2
        }
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;->setComplexity(I)V

    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;->onMathComplexityStartClick()V

    .line 114
    return-void
.end method

.method public setComplexitySecond(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900f0
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;->setComplexity(I)V

    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;->onMathComplexityStartClick()V

    .line 102
    return-void
.end method

.method public setComplexityThird(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900f1
        }
    .end annotation

    .prologue
    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;->setComplexity(I)V

    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;->onMathComplexityStartClick()V

    .line 108
    return-void
.end method
