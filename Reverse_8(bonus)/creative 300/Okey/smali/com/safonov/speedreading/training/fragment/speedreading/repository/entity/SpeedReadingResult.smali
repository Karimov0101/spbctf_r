.class public Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
.super Lio/realm/RealmObject;
.source "SpeedReadingResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/SpeedReadingResultRealmProxyInterface;


# static fields
.field public static final FIELD_AVERAGE_SPEED:Ljava/lang/String; = "averageSpeed"

.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_MAX_SPEED:Ljava/lang/String; = "maxSpeed"


# instance fields
.field private averageSpeed:I

.field private config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private maxSpeed:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getAverageSpeed()I
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmGet$averageSpeed()I

    move-result v0

    return v0
.end method

.method public getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getMaxSpeed()I
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmGet$maxSpeed()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$averageSpeed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->averageSpeed:I

    return v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->id:I

    return v0
.end method

.method public realmGet$maxSpeed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->maxSpeed:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$averageSpeed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->averageSpeed:I

    return-void
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->id:I

    return-void
.end method

.method public realmSet$maxSpeed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->maxSpeed:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->unixTime:J

    return-void
.end method

.method public setAverageSpeed(I)V
    .locals 0
    .param p1, "averageSpeed"    # I

    .prologue
    .line 50
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmSet$averageSpeed(I)V

    .line 51
    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V

    .line 69
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmSet$id(I)V

    .line 30
    return-void
.end method

.method public setMaxSpeed(I)V
    .locals 0
    .param p1, "maxSpeed"    # I

    .prologue
    .line 42
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmSet$maxSpeed(I)V

    .line 43
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 60
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->realmSet$unixTime(J)V

    .line 61
    return-void
.end method
