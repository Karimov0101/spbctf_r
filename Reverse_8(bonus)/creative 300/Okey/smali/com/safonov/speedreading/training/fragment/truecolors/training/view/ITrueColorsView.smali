.class public interface abstract Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;
.super Ljava/lang/Object;
.source "ITrueColorsView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;


# virtual methods
.method public abstract disableButtons()V
.end method

.method public abstract enableButtons()V
.end method

.method public abstract getColors()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getProgress()J
.end method

.method public abstract hideAnswerImage()V
.end method

.method public abstract initCountDownTimer(J)V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract setAnswerImage(Z)V
.end method

.method public abstract showLevel(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract timerPause()V
.end method

.method public abstract timerStart()V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
