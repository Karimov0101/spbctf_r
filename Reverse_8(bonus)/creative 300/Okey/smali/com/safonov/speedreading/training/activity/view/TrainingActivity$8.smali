.class Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;
.super Lcom/google/android/gms/ads/AdListener;
.source "TrainingActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setInterstitialFragment(Lcom/safonov/speedreading/training/TrainingType;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

.field final synthetic val$courseType:Lcom/safonov/speedreading/training/TrainingType;

.field final synthetic val$trainingIndex:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Lcom/safonov/speedreading/training/TrainingType;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 894
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->this$0:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->val$courseType:Lcom/safonov/speedreading/training/TrainingType;

    iput p3, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->val$trainingIndex:I

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClosed()V
    .locals 3

    .prologue
    .line 897
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->this$0:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->access$700(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/google/android/gms/ads/InterstitialAd;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 898
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->this$0:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->val$courseType:Lcom/safonov/speedreading/training/TrainingType;

    iget v2, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;->val$trainingIndex:I

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->newInstance(Lcom/safonov/speedreading/training/TrainingType;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->access$800(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Landroid/support/v4/app/Fragment;)V

    .line 899
    return-void
.end method
