.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
.super Lio/realm/RealmObject;
.source "WordsColumnsConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/WordsColumnsConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COLUMN_COUNT:Ljava/lang/String; = "columnCount"

.field public static final FIELD_ROW_COUNT:Ljava/lang/String; = "rowCount"

.field public static final FIELD_TRAINING_DURATION:Ljava/lang/String; = "trainingDuration"

.field public static final FIELD_WORDS_PER_ITEM:Ljava/lang/String; = "wordsPerItem"


# instance fields
.field private columnCount:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private rowCount:I

.field private speed:I

.field private trainingDuration:J

.field private wordsPerItem:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$columnCount()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getItemCount()I
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$rowCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$columnCount()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$rowCount()I

    move-result v0

    return v0
.end method

.method public getSpeed()I
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$speed()I

    move-result v0

    return v0
.end method

.method public getTrainingDuration()J
    .locals 2

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$trainingDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getWordsPerItem()I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmGet$wordsPerItem()I

    move-result v0

    return v0
.end method

.method public realmGet$columnCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->columnCount:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->id:I

    return v0
.end method

.method public realmGet$rowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->rowCount:I

    return v0
.end method

.method public realmGet$speed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->speed:I

    return v0
.end method

.method public realmGet$trainingDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->trainingDuration:J

    return-wide v0
.end method

.method public realmGet$wordsPerItem()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->wordsPerItem:I

    return v0
.end method

.method public realmSet$columnCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->columnCount:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->id:I

    return-void
.end method

.method public realmSet$rowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->rowCount:I

    return-void
.end method

.method public realmSet$speed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->speed:I

    return-void
.end method

.method public realmSet$trainingDuration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->trainingDuration:J

    return-void
.end method

.method public realmSet$wordsPerItem(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->wordsPerItem:I

    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$columnCount(I)V

    .line 54
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$id(I)V

    .line 38
    return-void
.end method

.method public setRowCount(I)V
    .locals 0
    .param p1, "rowCount"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$rowCount(I)V

    .line 46
    return-void
.end method

.method public setSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 73
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$speed(I)V

    .line 74
    return-void
.end method

.method public setTrainingDuration(J)V
    .locals 1
    .param p1, "trainingDuration"    # J

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$trainingDuration(J)V

    .line 82
    return-void
.end method

.method public setWordsPerItem(I)V
    .locals 0
    .param p1, "wordsPerItem"    # I

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->realmSet$wordsPerItem(I)V

    .line 66
    return-void
.end method
