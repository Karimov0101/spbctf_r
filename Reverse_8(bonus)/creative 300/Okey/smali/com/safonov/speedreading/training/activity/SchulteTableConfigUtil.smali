.class public Lcom/safonov/speedreading/training/activity/SchulteTableConfigUtil;
.super Ljava/lang/Object;
.source "SchulteTableConfigUtil.java"


# static fields
.field private static final PASS_COURSE_TYPE_1_COLUMN_COUNT:I = 0x5

.field private static final PASS_COURSE_TYPE_1_FULLSCREEN:Z = false

.field private static final PASS_COURSE_TYPE_1_ROW_COUNT:I = 0x5

.field private static final PASS_COURSE_TYPE_2_COLUMN_COUNT:I = 0x4

.field private static final PASS_COURSE_TYPE_2_FULLSCREEN:Z = true

.field private static final PASS_COURSE_TYPE_2_ROW_COUNT:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseType1Config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v1, 0x5

    .line 30
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;-><init>()V

    .line 32
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setRowCount(I)V

    .line 33
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setColumnCount(I)V

    .line 34
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setFullscreen(Z)V

    .line 36
    return-object v0
.end method

.method public static getPassCourseType2Config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 45
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;-><init>()V

    .line 47
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setRowCount(I)V

    .line 48
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setColumnCount(I)V

    .line 49
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setFullscreen(Z)V

    .line 51
    return-object v0
.end method

.method public static getUserConfig(IIZ)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    .locals 1
    .param p0, "rowCount"    # I
    .param p1, "columnCount"    # I
    .param p2, "fullscreen"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 15
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;-><init>()V

    .line 17
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    invoke-virtual {v0, p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setRowCount(I)V

    .line 18
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setColumnCount(I)V

    .line 19
    invoke-virtual {v0, p2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->setFullscreen(Z)V

    .line 21
    return-object v0
.end method
