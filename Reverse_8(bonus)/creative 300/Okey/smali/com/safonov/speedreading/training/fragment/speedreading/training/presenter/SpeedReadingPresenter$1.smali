.class Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;
.super Ljava/lang/Object;
.source "SpeedReadingPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getPreviousViewIndex(I)I
    .locals 1
    .param p1, "currentViewIndex"    # I

    .prologue
    .line 134
    if-nez p1, :cond_1

    .line 135
    const/16 v0, 0x8

    .line 142
    .local v0, "previousViewIndex":I
    :cond_0
    :goto_0
    return v0

    .line 137
    .end local v0    # "previousViewIndex":I
    :cond_1
    add-int/lit8 v0, p1, -0x1

    .line 138
    .restart local v0    # "previousViewIndex":I
    if-gez v0, :cond_0

    .line 139
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 151
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->getPreviousViewIndex(I)I

    move-result v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setWordItem(ILjava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v2

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setWordItem(ILjava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_2

    .line 155
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$308(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    .line 159
    :goto_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$108(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    .line 160
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 157
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0, v4}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$302(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;I)I

    goto :goto_1

    .line 162
    :cond_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->getPreviousViewIndex(I)I

    move-result v1

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setWordItem(ILjava/lang/String;)V

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0, v4}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$302(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;I)I

    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0, v4}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$102(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;I)I

    .line 167
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)V

    goto/16 :goto_0
.end method
