.class Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;
.super Ljava/lang/Object;
.source "SpeedReadingRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;ILcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;ILcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 98
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$configId:I

    .line 99
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 100
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .line 102
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->setId(I)V

    .line 103
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->setConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;)V

    .line 105
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 106
    return-void
.end method
