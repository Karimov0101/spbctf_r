.class public Lcom/safonov/speedreading/training/fragment/passcource/util/LineOfSightScoreUtil;
.super Ljava/lang/Object;
.source "LineOfSightScoreUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseScore(II)I
    .locals 2
    .param p0, "mistakeCount"    # I
    .param p1, "foundMistakeCount"    # I

    .prologue
    .line 9
    sub-int v1, p0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 10
    .local v0, "difference":I
    packed-switch v0, :pswitch_data_0

    .line 21
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 11
    :pswitch_0
    const/16 v1, 0x3c

    goto :goto_0

    .line 12
    :pswitch_1
    const/16 v1, 0x30

    goto :goto_0

    .line 13
    :pswitch_2
    const/16 v1, 0x2a

    goto :goto_0

    .line 14
    :pswitch_3
    const/16 v1, 0x1e

    goto :goto_0

    .line 15
    :pswitch_4
    const/16 v1, 0x12

    goto :goto_0

    .line 16
    :pswitch_5
    const/16 v1, 0xc

    goto :goto_0

    .line 17
    :pswitch_6
    const/4 v1, 0x6

    goto :goto_0

    .line 18
    :pswitch_7
    const/4 v1, 0x5

    goto :goto_0

    .line 19
    :pswitch_8
    const/4 v1, 0x3

    goto :goto_0

    .line 20
    :pswitch_9
    const/4 v1, 0x1

    goto :goto_0

    .line 10
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
