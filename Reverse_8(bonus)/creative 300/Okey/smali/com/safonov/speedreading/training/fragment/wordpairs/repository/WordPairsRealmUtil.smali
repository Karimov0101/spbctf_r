.class public Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "WordPairsRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;

    .prologue
    .line 94
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "rowCount"

    .line 95
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 96
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 97
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "columnCount"

    .line 98
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 99
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "trainingDuration"

    .line 101
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getTrainingDuration()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 103
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "differentWordPairsCount"

    .line 104
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 105
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 107
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 108
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 129
    :goto_0
    return-void

    .line 112
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 114
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setId(I)V

    .line 116
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$3;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$4;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;

    .prologue
    .line 134
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 136
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 137
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 139
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 140
    aget-object v0, p1, v3

    .line 142
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "rowCount"

    .line 143
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getRowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 144
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 145
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "columnCount"

    .line 146
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getColumnCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 147
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 148
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "trainingDuration"

    .line 149
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getTrainingDuration()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 150
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 151
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "differentWordPairsCount"

    .line 152
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getDifferentWordPairsCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 153
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 155
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 157
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 139
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 160
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->setId(I)V

    .line 161
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    aput v4, v1, v3

    .line 164
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 168
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 169
    if-eqz p2, :cond_2

    .line 170
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 189
    :cond_2
    :goto_2
    return-void

    .line 176
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$6;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;

    .prologue
    .line 67
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 69
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 89
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    .locals 3
    .param p1, "wordPairsConfigId"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 45
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .line 42
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .line 52
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .line 35
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "wordPairsConfigId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 28
    return-object v0
.end method
