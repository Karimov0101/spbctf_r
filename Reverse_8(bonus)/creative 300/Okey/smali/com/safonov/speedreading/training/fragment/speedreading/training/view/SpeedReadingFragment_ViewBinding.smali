.class public Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SpeedReadingFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

.field private view2131296683:Landroid/view/View;

.field private view2131296684:Landroid/view/View;

.field private view2131296685:Landroid/view/View;

.field private view2131296686:Landroid/view/View;

.field private view2131296687:Landroid/view/View;

.field private view2131296688:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;Landroid/view/View;)V
    .locals 12
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    .line 40
    const v2, 0x7f0901bc

    const-string v3, "field \'statisticsView\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->statisticsView:Landroid/view/View;

    .line 41
    const v2, 0x7f0901bb

    const-string v3, "field \'speedTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedTextView:Landroid/widget/TextView;

    .line 42
    const v2, 0x7f0901b9

    const-string v3, "field \'progressBar\'"

    const-class v4, Landroid/widget/ProgressBar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 43
    const v2, 0x7f0901c6

    const-string v3, "field \'wordsView\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsView:Landroid/view/View;

    .line 44
    const v2, 0x7f0901b1

    const-string v3, "field \'answerGridLayout\'"

    const-class v4, Landroid/support/v7/widget/GridLayout;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/GridLayout;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerGridLayout:Landroid/support/v7/widget/GridLayout;

    .line 45
    const v2, 0x7f0901b3

    const-string v3, "field \'answerSpeedTitleTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTitleTextView:Landroid/widget/TextView;

    .line 46
    const v2, 0x7f0901b2

    const-string v3, "field \'answerSpeedTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    .line 47
    const v2, 0x7f0901ab

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 48
    .local v1, "view":Landroid/view/View;
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296683:Landroid/view/View;

    .line 49
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v2, 0x7f0901ac

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 56
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296684:Landroid/view/View;

    .line 57
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$2;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v2, 0x7f0901ad

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 64
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296685:Landroid/view/View;

    .line 65
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$3;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v2, 0x7f0901ae

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 72
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296686:Landroid/view/View;

    .line 73
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$4;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const v2, 0x7f0901af

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 80
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296687:Landroid/view/View;

    .line 81
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$5;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v2, 0x7f0901b0

    const-string v3, "method \'onAnswerButtonClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 88
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296688:Landroid/view/View;

    .line 89
    new-instance v2, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$6;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding$6;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const/16 v2, 0x9

    new-array v3, v2, [Landroid/widget/TextView;

    const v2, 0x7f0901bd

    const-string v4, "field \'wordsTextViewList\'"

    const-class v5, Landroid/widget/TextView;

    .line 96
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v7

    const v2, 0x7f0901be

    const-string v4, "field \'wordsTextViewList\'"

    const-class v5, Landroid/widget/TextView;

    .line 97
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v8

    const v2, 0x7f0901bf

    const-string v4, "field \'wordsTextViewList\'"

    const-class v5, Landroid/widget/TextView;

    .line 98
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v9

    const v2, 0x7f0901c0

    const-string v4, "field \'wordsTextViewList\'"

    const-class v5, Landroid/widget/TextView;

    .line 99
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v10

    const v2, 0x7f0901c1

    const-string v4, "field \'wordsTextViewList\'"

    const-class v5, Landroid/widget/TextView;

    .line 100
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v11

    const/4 v4, 0x5

    const v2, 0x7f0901c2

    const-string v5, "field \'wordsTextViewList\'"

    const-class v6, Landroid/widget/TextView;

    .line 101
    invoke-static {p2, v2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    const/4 v4, 0x6

    const v2, 0x7f0901c3

    const-string v5, "field \'wordsTextViewList\'"

    const-class v6, Landroid/widget/TextView;

    .line 102
    invoke-static {p2, v2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    const/4 v4, 0x7

    const v2, 0x7f0901c4

    const-string v5, "field \'wordsTextViewList\'"

    const-class v6, Landroid/widget/TextView;

    .line 103
    invoke-static {p2, v2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    const/16 v4, 0x8

    const v2, 0x7f0901c5

    const-string v5, "field \'wordsTextViewList\'"

    const-class v6, Landroid/widget/TextView;

    .line 104
    invoke-static {p2, v2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    aput-object v2, v3, v4

    .line 95
    invoke-static {v3}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsTextViewList:Ljava/util/List;

    .line 105
    const/4 v2, 0x6

    new-array v3, v2, [Landroid/widget/Button;

    const v2, 0x7f0901ab

    const-string v4, "field \'answerButtons\'"

    const-class v5, Landroid/widget/Button;

    .line 106
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v7

    const v2, 0x7f0901ac

    const-string v4, "field \'answerButtons\'"

    const-class v5, Landroid/widget/Button;

    .line 107
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v8

    const v2, 0x7f0901ad

    const-string v4, "field \'answerButtons\'"

    const-class v5, Landroid/widget/Button;

    .line 108
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v9

    const v2, 0x7f0901ae

    const-string v4, "field \'answerButtons\'"

    const-class v5, Landroid/widget/Button;

    .line 109
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v10

    const v2, 0x7f0901af

    const-string v4, "field \'answerButtons\'"

    const-class v5, Landroid/widget/Button;

    .line 110
    invoke-static {p2, v2, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v11

    const/4 v4, 0x5

    const v2, 0x7f0901b0

    const-string v5, "field \'answerButtons\'"

    const-class v6, Landroid/widget/Button;

    .line 111
    invoke-static {p2, v2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    aput-object v2, v3, v4

    .line 105
    invoke-static {v3}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerButtons:Ljava/util/List;

    .line 113
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 114
    .local v0, "context":Landroid/content/Context;
    const v2, 0x1060003

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->textColorBlack:I

    .line 115
    const v2, 0x7f06001a

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->greenColor:I

    .line 116
    const v2, 0x7f06006d

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->redColor:I

    .line 117
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    .line 123
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 124
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    .line 126
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->statisticsView:Landroid/view/View;

    .line 127
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedTextView:Landroid/widget/TextView;

    .line 128
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 129
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsView:Landroid/view/View;

    .line 130
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerGridLayout:Landroid/support/v7/widget/GridLayout;

    .line 131
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTitleTextView:Landroid/widget/TextView;

    .line 132
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    .line 133
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsTextViewList:Ljava/util/List;

    .line 134
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerButtons:Ljava/util/List;

    .line 136
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296683:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 137
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296683:Landroid/view/View;

    .line 138
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296684:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296684:Landroid/view/View;

    .line 140
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296685:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296685:Landroid/view/View;

    .line 142
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296686:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296686:Landroid/view/View;

    .line 144
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296687:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 145
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296687:Landroid/view/View;

    .line 146
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296688:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 147
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment_ViewBinding;->view2131296688:Landroid/view/View;

    .line 148
    return-void
.end method
