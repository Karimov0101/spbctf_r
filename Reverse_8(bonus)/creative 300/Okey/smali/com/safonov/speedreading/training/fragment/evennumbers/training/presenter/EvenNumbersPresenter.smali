.class public Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "EvenNumbersPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;"
    }
.end annotation


# instance fields
.field private bestResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

.field private config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

.field private currentPlayTime:J

.field private isTrueAnswer:Z

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;

.field private repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

.field private score:I

.field private timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

.field private trueAnswersCount:I


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V
    .locals 0
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->model:Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;

    .line 26
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    .prologue
    .line 19
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 109
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->currentPlayTime:J

    .line 110
    return-void
.end method

.method public init(I)V
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->repository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getTrainingDuration()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->currentPlayTime:J

    .line 48
    new-instance v0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;I)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 79
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->initBoard(II)V

    .line 82
    :cond_0
    return-void
.end method

.method public onItemTouchDown(I)V
    .locals 2
    .param p1, "itemIndex"    # I

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->isEvenNumber()Z

    move-result v0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isTrueAnswer:Z

    .line 142
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isTrueAnswer:Z

    if-eqz v0, :cond_2

    .line 143
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    .line 144
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    .line 151
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->updateScore(I)V

    .line 152
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isTrueAnswer:Z

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->itemTouchDown(IZ)V

    .line 154
    :cond_1
    return-void

    .line 146
    :cond_2
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    if-lez v0, :cond_0

    .line 147
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->score:I

    goto :goto_0
.end method

.method public onItemTouchUp(I)V
    .locals 4
    .param p1, "itemIndex"    # I

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isTrueAnswer:Z

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->itemTouchUp(IZ)V

    .line 161
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 162
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->model:Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 165
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getDigitsPerNumber()I

    move-result v3

    .line 164
    invoke-interface {v0, v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;->createItems(III)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    .line 167
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->setBoardItems(Ljava/util/List;)V

    .line 170
    :cond_0
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->setBoardItemsEnable(Z)V

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->currentPlayTime:J

    .line 120
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 124
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->model:Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 128
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getDigitsPerNumber()I

    move-result v3

    .line 127
    invoke-interface {v0, v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;->createItems(III)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    .line 130
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->setBoardItems(Ljava/util/List;)V

    .line 131
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->setBoardItemsEnable(Z)V

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->currentPlayTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 135
    :cond_0
    return-void
.end method

.method public startTraining()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->updateProgressBar(I)V

    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getTrainingDuration()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->initProgressBar(I)V

    .line 90
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->updateScore(I)V

    .line 91
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->updateRecord(I)V

    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->model:Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v1

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v3

    mul-int/2addr v1, v3

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 94
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getDigitsPerNumber()I

    move-result v4

    .line 93
    invoke-interface {v0, v1, v3, v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;->createItems(III)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    .line 96
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;->setBoardItems(Ljava/util/List;)V

    .line 98
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->trueAnswersCount:I

    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->timeDownTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->currentPlayTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 102
    :cond_0
    return-void

    .line 91
    :cond_1
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->bestResult:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v1

    goto :goto_0
.end method
