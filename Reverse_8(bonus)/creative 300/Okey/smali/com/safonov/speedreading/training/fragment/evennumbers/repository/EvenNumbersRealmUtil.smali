.class public Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "EvenNumbersRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;

    .prologue
    .line 94
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "rowCount"

    .line 95
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 96
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 97
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "columnCount"

    .line 98
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 99
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "digitsPerNumber"

    .line 101
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getDigitsPerNumber()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 103
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "evenNumberCount"

    .line 104
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 105
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 106
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "trainingDuration"

    .line 107
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getTrainingDuration()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 108
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 110
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 111
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 132
    :goto_0
    return-void

    .line 115
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 117
    .local v0, "nextId":I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;

    invoke-direct {v3, p0, p1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;I)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$4;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;

    .prologue
    .line 137
    const-class v6, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 139
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 140
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 142
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 143
    aget-object v0, p1, v3

    .line 145
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "rowCount"

    .line 146
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getRowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 147
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 148
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "columnCount"

    .line 149
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getColumnCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 150
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 151
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "digitsPerNumber"

    .line 152
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getDigitsPerNumber()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 153
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 154
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "evenNumberCount"

    .line 155
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getEvenNumberCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 156
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 157
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "trainingDuration"

    .line 158
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getTrainingDuration()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 159
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 161
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 163
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 142
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 166
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setId(I)V

    .line 167
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    aput v4, v1, v3

    .line 170
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 174
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 175
    if-eqz p2, :cond_2

    .line 176
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 195
    :cond_2
    :goto_2
    return-void

    .line 182
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$5;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$6;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;

    .prologue
    .line 67
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 69
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 89
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    .locals 3
    .param p1, "evenNumbersConfigId"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 45
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .line 42
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 53
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .line 52
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 59
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .line 35
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "evenNumbersConfigId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 28
    return-object v0
.end method
