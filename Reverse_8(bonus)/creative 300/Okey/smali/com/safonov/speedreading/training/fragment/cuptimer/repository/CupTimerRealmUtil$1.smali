.class Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;
.super Ljava/lang/Object;
.source "CupTimerRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .prologue
    .line 58
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 61
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$configId:I

    .line 62
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 63
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .line 65
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->setId(I)V

    .line 66
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->setConfig(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V

    .line 68
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;->val$result:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 69
    return-void
.end method
