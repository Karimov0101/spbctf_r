.class public interface abstract Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;
.super Ljava/lang/Object;
.source "IRememberNumberView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;


# virtual methods
.method public abstract initProgressBar(I)V
.end method

.method public abstract sedDefaultCardValueAt(I)V
.end method

.method public abstract setBlinkCardsTextColor()V
.end method

.method public abstract setButtonsEnabled(Z)V
.end method

.method public abstract setCardAnswerBackgroundAt(IZ)V
.end method

.method public abstract setCardValueAt(ILjava/lang/String;)V
.end method

.method public abstract setCardViews(I)V
.end method

.method public abstract setCardsDefaultBackground()V
.end method

.method public abstract setCardsDefaultTextColor()V
.end method

.method public abstract setCardsDefaultValues()V
.end method

.method public abstract setCardsValues([Ljava/lang/String;)V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
