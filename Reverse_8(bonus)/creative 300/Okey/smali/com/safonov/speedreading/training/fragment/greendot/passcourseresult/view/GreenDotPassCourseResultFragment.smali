.class public Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "GreenDotPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900a2
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field private greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field passCourseScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090132
    .end annotation
.end field

.field private resultId:I

.field private timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 87
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 44
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;-><init>()V

    .line 51
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 52
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getGreenDotRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 113
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 114
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 119
    return-void

    .line 116
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->resultId:I

    .line 63
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    const v1, 0x7f0b003f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 78
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 130
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->close()V

    .line 133
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 135
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 136
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 142
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 143
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 125
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 106
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 83
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;->initTrainingResults(I)V

    .line 85
    return-void
.end method

.method public updatePassCoursePointsView(I)V
    .locals 2
    .param p1, "points"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->passCourseScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 97
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 3
    .param p1, "duration"    # J

    .prologue
    .line 91
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-virtual {v1, p1, p2}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    return-void
.end method
