.class public Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/ConcentrationPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "ConcentrationPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/IConcentrationPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/IConcentrationPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/ConcentrationPresenter;->repository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    .line 20
    return-void
.end method
