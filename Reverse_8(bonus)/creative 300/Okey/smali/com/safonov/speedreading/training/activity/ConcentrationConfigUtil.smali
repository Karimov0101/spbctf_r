.class public Lcom/safonov/speedreading/training/activity/ConcentrationConfigUtil;
.super Ljava/lang/Object;
.source "ConcentrationConfigUtil.java"


# static fields
.field public static final DEFAULT_CIRCLES_COUNT:I = 0x4

.field public static final DEFAULT_GRAY_DURATION:I = 0x7d0

.field public static final DEFAULT_RADIUS:I = 0x46

.field public static final DEFAULT_SPEED:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 5

    .prologue
    const/16 v4, 0x46

    const/4 v3, 0x4

    const/4 v2, 0x3

    .line 16
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;-><init>()V

    .line 18
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesCount(I)V

    .line 19
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesSpeed(I)V

    .line 20
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesRadius(I)V

    .line 21
    const/16 v1, 0x7d0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setGrayTime(I)V

    .line 23
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesSpeedCustom(I)V

    .line 24
    invoke-virtual {v0, v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesCountCustom(I)V

    .line 25
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->setCirclesSizeCustom(I)V

    .line 27
    return-object v0
.end method
