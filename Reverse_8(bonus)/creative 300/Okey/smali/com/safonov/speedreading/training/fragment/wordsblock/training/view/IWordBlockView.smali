.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;
.super Ljava/lang/Object;
.source "IWordBlockView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract onTrainingCompleted(I)V
.end method

.method public abstract setWordBlockItemText(Ljava/lang/String;)V
.end method

.method public abstract updateSpeedView(I)V
.end method

.method public abstract updateTimeView(J)V
.end method

.method public abstract updateWordCountView(I)V
.end method
