.class public interface abstract Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;
.super Ljava/lang/Object;
.source "ISchulteTableView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;


# virtual methods
.method public abstract initBoard(IIZ)V
.end method

.method public abstract itemTouchDown(IZ)V
.end method

.method public abstract itemTouchUp(IZ)V
.end method

.method public abstract setBoardItems(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setBoardItemsEnable(Z)V
.end method

.method public abstract updateBestTimeView(J)V
.end method

.method public abstract updateCurrentTimeView(J)V
.end method

.method public abstract updateNextItemView(Ljava/lang/String;)V
.end method
