.class public Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;
.super Ljava/lang/Object;
.source "IndexesUtil.java"


# static fields
.field private static final random:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;->random:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static containsInArray([II)Z
    .locals 4
    .param p0, "array"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p1, "checkedItem"    # I

    .prologue
    const/4 v1, 0x0

    .line 35
    array-length v3, p0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p0, v2

    .line 36
    .local v0, "item":I
    if-ne p1, v0, :cond_1

    const/4 v1, 0x1

    .line 38
    .end local v0    # "item":I
    :cond_0
    return v1

    .line 35
    .restart local v0    # "item":I
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public static generateUniqueIndexes(II)[I
    .locals 5
    .param p0, "maxIndexValue"    # I
    .param p1, "uniqueIndexesCount"    # I

    .prologue
    .line 18
    new-array v2, p1, [I

    .line 20
    .local v2, "result":[I
    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 22
    const/4 v0, 0x0

    .line 23
    .local v0, "index":I
    :cond_0
    :goto_0
    if-ge v0, p1, :cond_1

    .line 24
    sget-object v3, Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;->random:Ljava/util/Random;

    add-int/lit8 v4, p0, 0x1

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    .line 25
    .local v1, "randomIndex":I
    invoke-static {v2, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;->containsInArray([II)Z

    move-result v3

    if-nez v3, :cond_0

    .line 26
    aput v1, v2, v0

    .line 27
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 31
    .end local v1    # "randomIndex":I
    :cond_1
    return-object v2
.end method
