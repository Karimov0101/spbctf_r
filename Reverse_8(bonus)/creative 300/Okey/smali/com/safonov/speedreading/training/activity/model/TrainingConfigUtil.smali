.class public Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;


# instance fields
.field private acceleratorRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

.field private concentrationRepository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

.field private cupTimerRepository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

.field private evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

.field private flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

.field private greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

.field private lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

.field private mathRepository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

.field private rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

.field private rememberWordsRepository:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;

.field private schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

.field private speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

.field private trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

.field private trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

.field private trueColorsRepository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

.field private wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

.field private wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V
    .locals 1
    .param p1, "trainingSettingsUtil"    # Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;
    .param p2, "trainingDescriptionUtil"    # Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;
    .param p3, "acceleratorRepository"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;
    .param p4, "wordBlockRepository"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;
    .param p5, "flashWordRepository"    # Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;
    .param p6, "schulteTableRepository"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
    .param p7, "rememberNumberRepository"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
    .param p8, "lineOfSightRepository"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
    .param p9, "speedReadingRepository"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
    .param p10, "evenNumberRepository"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
    .param p11, "wordPairsRepository"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
    .param p12, "greenDotRepository"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;
    .param p13, "mathRepository"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;
    .param p14, "concentrationRepository"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
    .param p15, "cupTimerRepository"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;
    .param p16, "rememberWordsRepository"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;
    .param p17, "trueColorsRepository"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    .prologue
    .line 106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    .line 109
    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    .line 111
    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->acceleratorRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    .line 112
    iput-object p4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    .line 113
    iput-object p5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .line 115
    iput-object p6, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    .line 116
    iput-object p7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    .line 117
    iput-object p8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    .line 118
    iput-object p9, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 119
    iput-object p10, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    .line 120
    iput-object p11, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    .line 121
    iput-object p12, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .line 122
    iput-object p13, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->mathRepository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    .line 123
    iput-object p14, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->concentrationRepository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    .line 124
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->cupTimerRepository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    .line 125
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->rememberWordsRepository:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;

    .line 126
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trueColorsRepository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;
    .param p1, "x1"    # Lcom/safonov/speedreading/training/TrainingType;
    .param p2, "x2"    # Z
    .param p3, "x3"    # Z
    .param p4, "x4"    # Z

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->getTrainingFragmentTypes(Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;
    .param p1, "x1"    # [Ljava/lang/Object;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->itemsAreNonNull([Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private getTrainingFragmentTypes(Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;
    .locals 3
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "shouldShowDescription"    # Z
    .param p3, "shouldShowInterstitial"    # Z
    .param p4, "shouldShowComplexity"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/TrainingType;",
            "ZZZ)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/FragmentType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 136
    .local v1, "trainingStack":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    if-eqz p3, :cond_0

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->INTERSTITIAL:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_0
    if-eqz p2, :cond_1

    .line 139
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceDescriptionFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    .line 140
    .local v0, "descriptionFragmentType":Lcom/safonov/speedreading/training/FragmentType;
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingDescriptionUtil:Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    invoke-virtual {v2, v0}, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->shouldShowDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 141
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    .end local v0    # "descriptionFragmentType":Lcom/safonov/speedreading/training/FragmentType;
    :cond_1
    if-eqz p4, :cond_2

    .line 146
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceComplexityFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_2
    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceTrainingFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    return-object v1
.end method

.method private itemsAreNonNull([Ljava/lang/Object;)Z
    .locals 4
    .param p1, "array"    # [Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 156
    array-length v3, p1

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v0, p1, v2

    .line 157
    .local v0, "object":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 161
    .end local v0    # "object":Ljava/lang/Object;
    :goto_1
    return v1

    .line 156
    .restart local v0    # "object":Ljava/lang/Object;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 161
    .end local v0    # "object":Ljava/lang/Object;
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private requestToGetAcceleratorConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 487
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getWordsColumnsDuration()J

    move-result-wide v2

    .line 488
    .local v2, "trainingDuration":J
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getWordsColumnsColumnCount()I

    move-result v0

    .line 490
    .local v0, "columnCount":I
    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getUserConfig(IJ)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v1

    .line 492
    .local v1, "config":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->acceleratorRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$11;

    invoke-direct {v5, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$11;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v4, v1, v5}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;)V

    .line 503
    return-void
.end method

.method private requestToGetAcceleratorCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 10
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 372
    const/16 v4, 0x9

    new-array v2, v4, [Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .line 375
    .local v2, "trainingWrapperArray":[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    const/4 v4, 0x5

    new-array v0, v4, [Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 376
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseType1Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v4

    aput-object v4, v0, v6

    .line 377
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseType2Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v4

    aput-object v4, v0, v7

    .line 378
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseType3Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v4

    aput-object v4, v0, v8

    .line 379
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseType4Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v4

    aput-object v4, v0, v9

    const/4 v4, 0x4

    .line 380
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseType5Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v5

    aput-object v5, v0, v4

    .line 383
    .local v0, "configs":[Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->acceleratorRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;

    invoke-direct {v5, p0, v2, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$8;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v4, v0, v5}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnMultiTransactionCallback;)V

    .line 413
    new-array v1, v9, [Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .line 414
    invoke-static {}, Lcom/safonov/speedreading/training/activity/FlashWordsConfigUtil;->getAcceleratorCourse1Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v4

    aput-object v4, v1, v6

    .line 415
    invoke-static {}, Lcom/safonov/speedreading/training/activity/FlashWordsConfigUtil;->getAcceleratorCourse2Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v4

    aput-object v4, v1, v7

    .line 416
    invoke-static {}, Lcom/safonov/speedreading/training/activity/FlashWordsConfigUtil;->getAcceleratorCourse3Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v4

    aput-object v4, v1, v8

    .line 419
    .local v1, "flashWordsConfigs":[Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;

    invoke-direct {v5, p0, v2, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v4, v1, v5}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnMultiTransactionCallback;)V

    .line 446
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordBlockConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v3

    .line 448
    .local v3, "wordBlockConfig":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;

    invoke-direct {v5, p0, v2, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v4, v3, v5}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V

    .line 461
    return-void
.end method

.method private requestToGetConcentrationConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 692
    invoke-static {}, Lcom/safonov/speedreading/training/activity/ConcentrationConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    .line 694
    .local v0, "concentrationConfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->concentrationRepository:Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$22;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$22;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V

    .line 706
    return-void
.end method

.method private requestToGetCupTimerConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 709
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getCupTimerDuration()J

    move-result-wide v2

    .line 710
    .local v2, "duration":J
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getCupTimerType()I

    move-result v1

    .line 712
    .local v1, "type":I
    invoke-static {v2, v3, v1}, Lcom/safonov/speedreading/training/activity/CupTimerConfigUtil;->getUserConfig(JI)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v0

    .line 714
    .local v0, "cupTimerConfig":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->cupTimerRepository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;

    invoke-direct {v5, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v4, v0, v5}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V

    .line 725
    return-void
.end method

.method private requestToGetEvenNumbersConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 638
    invoke-static {}, Lcom/safonov/speedreading/training/activity/EvenNumbersConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v0

    .line 640
    .local v0, "evenNumbersConfig":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$19;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$19;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V

    .line 651
    return-void
.end method

.method private requestToGetFlashWordsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 524
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getFlashWordsBoardType()I

    move-result v0

    .line 525
    .local v0, "boardType":I
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getFlashWordsDuration()J

    move-result-wide v2

    .line 527
    .local v2, "duration":J
    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/training/activity/FlashWordsConfigUtil;->getUserConfig(IJ)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v1

    .line 529
    .local v1, "config":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;

    invoke-direct {v5, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v4, v1, v5}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;)V

    .line 540
    return-void
.end method

.method private requestToGetLineOfSightConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 583
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getLineOfSightRowCount()I

    move-result v2

    .line 584
    .local v2, "rowCount":I
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getLineOfSightFieldType()I

    move-result v1

    .line 586
    .local v1, "fieldType":I
    invoke-static {v2, v1}, Lcom/safonov/speedreading/training/activity/LineOfSightConfigUtil;->getUserConfig(II)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v0

    .line 588
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    new-instance v4, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$16;

    invoke-direct {v4, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$16;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v3, v0, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V

    .line 599
    return-void
.end method

.method private requestToGetMathConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 674
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getMathDuration()J

    move-result-wide v0

    .line 676
    .local v0, "duration":J
    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/MathConfigUtil;->getDefaultConfig(J)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v2

    .line 678
    .local v2, "mathConfig":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->mathRepository:Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;

    new-instance v4, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$21;

    invoke-direct {v4, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$21;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v3, v2, v4}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V

    .line 689
    return-void
.end method

.method private requestToGetPassCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 14
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 180
    const/16 v0, 0x14

    new-array v4, v0, [Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .line 183
    .local v4, "trainingWrappers":[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    invoke-static {}, Lcom/safonov/speedreading/training/activity/SchulteTableConfigUtil;->getPassCourseType1Config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v2

    .line 184
    .local v2, "passCourseSchulteTableConfig1":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    invoke-static {}, Lcom/safonov/speedreading/training/activity/SchulteTableConfigUtil;->getPassCourseType2Config()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v3

    .line 186
    .local v3, "passCourseSchulteTableConfig2":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    const/4 v0, 0x2

    new-array v10, v0, [Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    const/4 v0, 0x0

    aput-object v2, v10, v0

    const/4 v0, 0x1

    aput-object v3, v10, v0

    .line 191
    .local v10, "schulteTableConfigs":[Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    iget-object v13, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    new-instance v0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v13, v10, v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnMultiTransactionCallback;)V

    .line 230
    invoke-static {}, Lcom/safonov/speedreading/training/activity/RememberNumberConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v9

    .line 232
    .local v9, "rememberNumberConfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v9, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V

    .line 253
    const/4 v0, 0x3

    new-array v8, v0, [Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    const/4 v0, 0x0

    .line 254
    invoke-static {}, Lcom/safonov/speedreading/training/activity/LineOfSightConfigUtil;->getPassCourseType1Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    aput-object v1, v8, v0

    const/4 v0, 0x1

    .line 255
    invoke-static {}, Lcom/safonov/speedreading/training/activity/LineOfSightConfigUtil;->getPassCourseType2Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    aput-object v1, v8, v0

    const/4 v0, 0x2

    .line 256
    invoke-static {}, Lcom/safonov/speedreading/training/activity/LineOfSightConfigUtil;->getPassCourseType3Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    aput-object v1, v8, v0

    .line 259
    .local v8, "lineOfSightConfigs":[Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v8, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;)V

    .line 295
    invoke-static {}, Lcom/safonov/speedreading/training/activity/SpeedReadingConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v11

    .line 297
    .local v11, "speedReadingConfig":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$4;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$4;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v11, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V

    .line 319
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordPairsConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v12

    .line 321
    .local v12, "wordPairsConfig":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$5;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$5;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v12, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V

    .line 336
    invoke-static {}, Lcom/safonov/speedreading/training/activity/EvenNumbersConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v6

    .line 338
    .local v6, "evenNumbersConfig":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$6;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$6;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v6, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V

    .line 353
    invoke-static {}, Lcom/safonov/speedreading/training/activity/GreenDotConfigUtil;->getPassCourseConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v7

    .line 355
    .local v7, "greenDotConfig":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$7;

    invoke-direct {v1, p0, v4, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$7;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    invoke-interface {v0, v7, v1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V

    .line 368
    return-void
.end method

.method private requestToGetRememberNumberConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 565
    invoke-static {}, Lcom/safonov/speedreading/training/activity/RememberNumberConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    .line 567
    .local v0, "rememberNumberConfig":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$15;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$15;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V

    .line 579
    return-void
.end method

.method private requestToGetRememberWordsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 729
    invoke-static {}, Lcom/safonov/speedreading/training/activity/RememberWordsConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v0

    .line 731
    .local v0, "rememberWordsConfig":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->rememberWordsRepository:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$24;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$24;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V

    .line 743
    return-void
.end method

.method private requestToGetSchulteTableConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 6
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 544
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getSchulteTableRowCount()I

    move-result v3

    .line 545
    .local v3, "rowCount":I
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getSchulteTableColumnCount()I

    move-result v0

    .line 546
    .local v0, "columnCount":I
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->isSchulteTableFullscreen()Z

    move-result v2

    .line 548
    .local v2, "isFullscreen":Z
    invoke-static {v3, v0, v2}, Lcom/safonov/speedreading/training/activity/SchulteTableConfigUtil;->getUserConfig(IIZ)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v1

    .line 550
    .local v1, "config":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;

    invoke-direct {v5, p0, v2, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;ZLcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v4, v1, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;)V

    .line 561
    return-void
.end method

.method private requestToGetSpeedReadingConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 603
    invoke-static {}, Lcom/safonov/speedreading/training/activity/SpeedReadingConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v0

    .line 605
    .local v0, "speedReadingConfig":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$17;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$17;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V

    .line 617
    return-void
.end method

.method private requestToGetTrueColorsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 746
    invoke-static {}, Lcom/safonov/speedreading/training/activity/TrueColorsConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v0

    .line 748
    .local v0, "trueColorsConfig":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trueColorsRepository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$25;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$25;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V

    .line 760
    return-void
.end method

.method private requestToGetWordBlockConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 507
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordBlockConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v0

    .line 509
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$12;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$12;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V

    .line 520
    return-void
.end method

.method private requestToGetWordPairsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 621
    invoke-static {}, Lcom/safonov/speedreading/training/activity/WordPairsConfigUtil;->getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v0

    .line 623
    .local v0, "wordPairsConfig":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    new-instance v2, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$18;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$18;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v1, v0, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V

    .line 634
    return-void
.end method

.method private requestToGreenDotConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 5
    .param p1, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 655
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->trainingSettingsUtil:Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;->getGreenDotDuration()J

    move-result-wide v0

    .line 657
    .local v0, "duration":J
    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/GreenDotConfigUtil;->getUserConfig(J)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v2

    .line 659
    .local v2, "greenDotConfig":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    new-instance v4, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$20;

    invoke-direct {v4, p0, p1}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$20;-><init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    invoke-interface {v3, v2, v4}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V

    .line 670
    return-void
.end method


# virtual methods
.method public requestToGetCourseConfigList(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 2
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    .prologue
    .line 166
    sget-object v0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$26;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 176
    :goto_0
    return-void

    .line 168
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetPassCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    goto :goto_0

    .line 171
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetAcceleratorCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    goto :goto_0

    .line 166
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public requestToGetTrainingConfig(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 2
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "listener"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    .prologue
    .line 465
    sget-object v0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$26;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 483
    :goto_0
    return-void

    .line 466
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetAcceleratorConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 467
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetWordBlockConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 468
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetFlashWordsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 470
    :pswitch_3
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetSchulteTableConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 471
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetRememberNumberConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 472
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetLineOfSightConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 473
    :pswitch_6
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetSpeedReadingConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 474
    :pswitch_7
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetWordPairsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 475
    :pswitch_8
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetEvenNumbersConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 476
    :pswitch_9
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGreenDotConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 477
    :pswitch_a
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetMathConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 478
    :pswitch_b
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetConcentrationConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 479
    :pswitch_c
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetCupTimerConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 480
    :pswitch_d
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetRememberWordsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 481
    :pswitch_e
    invoke-direct {p0, p2}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetTrueColorsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0

    .line 465
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method
