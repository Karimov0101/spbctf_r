.class public Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
.super Lio/realm/RealmObject;
.source "TrueColorsResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/TrueColorsResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->config:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->score:I

    return v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->config:Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->score:I

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V

    .line 47
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmSet$id(I)V

    .line 26
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->realmSet$score(I)V

    .line 39
    return-void
.end method
