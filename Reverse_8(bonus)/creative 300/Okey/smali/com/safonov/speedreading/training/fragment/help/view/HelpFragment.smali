.class public Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "HelpFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;",
        "Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/help/view/IHelpView;"
    }
.end annotation


# static fields
.field private static final TRAINING_FRAGMENT_KEY:Ljava/lang/String; = "training_fragment_key"


# instance fields
.field contentHolderScrollView:Landroid/widget/ScrollView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900aa
    .end annotation
.end field

.field private fragmentType:Lcom/safonov/speedreading/training/FragmentType;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 40
    return-void
.end method

.method public static newInstance(Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;
    .locals 3
    .param p0, "helpFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    .line 43
    new-instance v1, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;-><init>()V

    .line 44
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_fragment_key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 46
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->setArguments(Landroid/os/Bundle;)V

    .line 47
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 32
    new-instance v0, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/help/presenter/HelpPresenter;-><init>()V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_fragment_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->fragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 56
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    const v1, 0x7f0b0041

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 67
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->unbinder:Lbutterknife/Unbinder;

    .line 69
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 82
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->unbinder:Lbutterknife/Unbinder;

    .line 89
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 74
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->fragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/help/presenter/IHelpPresenter;->requestToLoadContent(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 76
    return-void
.end method

.method public setContentLayout(I)V
    .locals 3
    .param p1, "layout"    # I
        .annotation build Landroid/support/annotation/LayoutRes;
        .end annotation
    .end param

    .prologue
    .line 93
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    const/4 v2, 0x1

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 94
    return-void
.end method
