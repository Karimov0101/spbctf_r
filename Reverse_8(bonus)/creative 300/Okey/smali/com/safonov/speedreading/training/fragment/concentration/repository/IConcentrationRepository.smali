.class public interface abstract Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;
.super Ljava/lang/Object;
.source "IConcentrationRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ILcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfig(IIII)V
.end method

.method public abstract updateConfigComplexity(II)V
.end method

.method public abstract updateConfigCustom(IIII)V
.end method
