.class public Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
.super Lio/realm/RealmObject;
.source "ConcentrationResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/ConcentrationResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->score:I

    return v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->score:I

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .prologue
    .line 34
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    .line 35
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmSet$id(I)V

    .line 42
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->realmSet$score(I)V

    .line 27
    return-void
.end method
