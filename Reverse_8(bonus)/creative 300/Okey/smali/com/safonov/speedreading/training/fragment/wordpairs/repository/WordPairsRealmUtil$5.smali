.class Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;
.super Ljava/lang/Object;
.source "WordPairsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnMultiTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

.field final synthetic val$haveToSaveConfigList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .prologue
    .line 176
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 179
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil$5;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lio/realm/Realm;->copyToRealm(Ljava/lang/Iterable;)Ljava/util/List;

    .line 180
    return-void
.end method
