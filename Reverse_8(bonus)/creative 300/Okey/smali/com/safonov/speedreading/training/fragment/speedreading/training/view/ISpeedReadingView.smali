.class public interface abstract Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;
.super Ljava/lang/Object;
.source "ISpeedReadingView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;


# virtual methods
.method public abstract hideAnswerView()V
.end method

.method public abstract hideWordsView()V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract setAnswerButtonsEnabled(Z)V
.end method

.method public abstract setAnswerItems(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setWordItem(ILjava/lang/String;)V
.end method

.method public abstract showAnswerView()V
.end method

.method public abstract showWordsView()V
.end method

.method public abstract updateAnswerSpeed(I)V
.end method

.method public abstract updateAnswerSpeedWithAnimation(IZ)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateSpeed(I)V
.end method
