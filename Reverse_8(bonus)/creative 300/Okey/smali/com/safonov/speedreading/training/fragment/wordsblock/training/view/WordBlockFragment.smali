.class public Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "WordBlockFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "training_config_id"


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field private configId:I

.field itemTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900ce
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

.field speedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901c7
    .end annotation
.end field

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090207
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;

.field wordCountTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090246
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 66
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;-><init>()V

    .line 52
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 53
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 42
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getWordBlockRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    .line 44
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 205
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 206
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 207
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

    .line 212
    return-void

    .line 209
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement MultiBlockWordTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onContentViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090068
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->switchPauseStatus()V

    .line 144
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->configId:I

    .line 64
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    const v1, 0x7f0b00b5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->unbinder:Lbutterknife/Unbinder;

    .line 84
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 103
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 185
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->close()V

    .line 186
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 190
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 191
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->cancelTraining()V

    .line 192
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 193
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 194
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 216
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 217
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

    .line 218
    return-void
.end method

.method public onMinusViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090113
        }
    .end annotation

    .prologue
    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->wordCountMinus()V

    .line 165
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 125
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onPause()V

    .line 126
    return-void
.end method

.method public onPlusViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090114
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->wordCountPlus()V

    .line 160
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onResume()V

    .line 131
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 132
    return-void
.end method

.method public onSpeedDownViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901aa
        }
    .end annotation

    .prologue
    .line 148
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->speedDown()V

    .line 149
    return-void
.end method

.method public onSpeedUpViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901c8
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->speedUp()V

    .line 155
    return-void
.end method

.method public onTrainingCompleted(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 198
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;->onWordBlockTrainingCompleted(I)V

    .line 199
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->init(I)V

    .line 110
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;->pauseTraining()V

    .line 115
    return-void
.end method

.method public resumeAnimations()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public setWordBlockItemText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 138
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->itemTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    return-void
.end method

.method public updateSpeedView(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->speedTextView:Landroid/widget/TextView;

    const v1, 0x7f0e01a4

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 170
    return-void
.end method

.method public updateTimeView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->timeTextView:Landroid/widget/TextView;

    const v1, 0x7f0e01a5

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    return-void
.end method

.method public updateWordCountView(I)V
    .locals 5
    .param p1, "wordCount"    # I

    .prologue
    .line 179
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->wordCountTextView:Landroid/widget/TextView;

    const v1, 0x7f0e01af

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 180
    return-void
.end method
