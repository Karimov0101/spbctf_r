.class public interface abstract Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;
.super Ljava/lang/Object;
.source "IFlashWordsPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract init(I)V
.end method

.method public abstract levelDown()V
.end method

.method public abstract levelUp()V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract speedDown()V
.end method

.method public abstract speedUp()V
.end method

.method public abstract startTraining()V
.end method

.method public abstract switchTrainingPause()V
.end method
