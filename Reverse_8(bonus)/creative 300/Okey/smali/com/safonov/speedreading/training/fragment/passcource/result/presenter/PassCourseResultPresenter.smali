.class public Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "PassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/IPassCourseResultPresenter;"
    }
.end annotation


# static fields
.field private static final PASS_COURSE_1_TYPE:I = 0x1


# instance fields
.field private evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

.field private greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

.field private lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

.field private passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

.field private rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

.field private schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

.field private speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

.field private wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V
    .locals 0
    .param p1, "passCourseRepository"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "schulteTableRepository"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "rememberNumberRepository"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p4, "lineOfSightRepository"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p5, "speedReadingRepository"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p6, "evenNumberRepository"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p7, "wordPairsRepository"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p8, "greenDotRepository"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    .line 66
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    .line 67
    iput-object p4, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    .line 68
    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    .line 69
    iput-object p5, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 70
    iput-object p6, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    .line 71
    iput-object p7, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    .line 72
    iput-object p8, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .line 73
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;)Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    return-object v0
.end method

.method private varargs getLineOfSightPassCourseScore([Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;)I
    .locals 6
    .param p1, "results"    # [Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 184
    const/4 v0, 0x0

    .line 185
    .local v0, "passCourseScore":I
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, p1, v2

    .line 186
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v4

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v5

    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/fragment/passcource/util/LineOfSightScoreUtil;->getPassCourseScore(II)I

    move-result v4

    add-int/2addr v0, v4

    .line 185
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    :cond_1
    return v0
.end method

.method private varargs getRememberPassCourseScore([Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;)I
    .locals 5
    .param p1, "results"    # [Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 202
    const/4 v0, 0x0

    .line 203
    .local v0, "passCourseScore":I
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, p1, v2

    .line 204
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    if-eqz v1, :cond_0

    .line 205
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v4

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/passcource/util/RememberNumberScoreUtil;->getPassCourseScore(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 203
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 207
    .end local v1    # "result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_1
    return v0
.end method

.method private getSchulteTablePassCourseScore(Ljava/util/List;)I
    .locals 6
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 175
    .local p1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    const/4 v0, 0x0

    .line 176
    .local v0, "passCourseScore":I
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .line 177
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    if-eqz v1, :cond_0

    .line 178
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/fragment/passcource/util/SchulteTableScoreUtil;->getPassCourseScore(J)I

    move-result v3

    add-int/2addr v0, v3

    goto :goto_0

    .line 180
    .end local v1    # "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    :cond_1
    return v0
.end method

.method private getShulteTableResultList([I)Ljava/util/List;
    .locals 5
    .param p1, "resultIds"    # [I
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 167
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 168
    .local v1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v0, p1, v2

    .line 169
    .local v0, "id":I
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->schulteTableRepository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-interface {v4, v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 171
    .end local v0    # "id":I
    :cond_0
    return-object v1
.end method

.method private varargs getSpeedReadingPassCourseScore([Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;)I
    .locals 5
    .param p1, "results"    # [Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 193
    const/4 v0, 0x0

    .line 194
    .local v0, "passCourseScore":I
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, p1, v2

    .line 195
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    if-eqz v1, :cond_0

    .line 196
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v4

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/passcource/util/SpeedReadingScoreUtil;->getPassCourseScore(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 194
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 198
    .end local v1    # "result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    :cond_1
    return v0
.end method


# virtual methods
.method public init([I)V
    .locals 21
    .param p1, "resultIds"    # [I

    .prologue
    .line 79
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;->showProgressDialog()V

    .line 83
    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v18

    .line 87
    .local v18, "schulteTable1ResultIds":[I
    const/16 v2, 0xa

    const/16 v3, 0xf

    move-object/from16 v0, p1

    invoke-static {v0, v2, v3}, Ljava/util/Arrays;->copyOfRange([III)[I

    move-result-object v19

    .line 92
    .local v19, "schulteTable2ResultIds":[I
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getShulteTableResultList([I)Ljava/util/List;

    move-result-object v4

    .line 93
    .local v4, "schulteTable1ResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    const/4 v3, 0x5

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v5

    .line 94
    .local v5, "rememberNumber1Result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    const/4 v3, 0x6

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v6

    .line 95
    .local v6, "lineOfSight1Result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    const/4 v3, 0x7

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v7

    .line 96
    .local v7, "speedReading1Result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->wordPairsRepository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    const/16 v3, 0x8

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v8

    .line 97
    .local v8, "wordPairsResult":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->evenNumberRepository:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    const/16 v3, 0x9

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v9

    .line 99
    .local v9, "evenNumbersResult":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getShulteTableResultList([I)Ljava/util/List;

    move-result-object v10

    .line 100
    .local v10, "schulteTable2ResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->rememberNumberRepository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    const/16 v3, 0xf

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v11

    .line 101
    .local v11, "rememberNumber2Result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    const/16 v3, 0x10

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v12

    .line 102
    .local v12, "lineOfSight2Result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->lineOfSightRepository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    const/16 v3, 0x11

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v13

    .line 103
    .local v13, "lineOfSight3Result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->speedReadingRepository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    const/16 v3, 0x12

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v14

    .line 104
    .local v14, "speedReading2Result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->greenDotRepository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    const/16 v3, 0x13

    aget v3, p1, v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v15

    .line 106
    .local v15, "greenDotResult":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    const/16 v17, 0x0

    .line 108
    .local v17, "passCourseScore":I
    if-eqz v4, :cond_1

    .line 109
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getSchulteTablePassCourseScore(Ljava/util/List;)I

    move-result v2

    add-int v17, v17, v2

    .line 111
    :cond_1
    if-eqz v10, :cond_2

    .line 112
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getSchulteTablePassCourseScore(Ljava/util/List;)I

    move-result v2

    add-int v17, v17, v2

    .line 114
    :cond_2
    if-eqz v5, :cond_3

    if-eqz v11, :cond_3

    .line 115
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    const/4 v3, 0x0

    aput-object v5, v2, v3

    const/4 v3, 0x1

    aput-object v11, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getRememberPassCourseScore([Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;)I

    move-result v2

    add-int v17, v17, v2

    .line 117
    :cond_3
    if-eqz v7, :cond_4

    if-eqz v14, :cond_4

    .line 118
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    const/4 v3, 0x0

    aput-object v7, v2, v3

    const/4 v3, 0x1

    aput-object v14, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getSpeedReadingPassCourseScore([Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;)I

    move-result v2

    add-int v17, v17, v2

    .line 120
    :cond_4
    if-eqz v6, :cond_5

    if-eqz v12, :cond_5

    if-eqz v13, :cond_5

    .line 121
    const/4 v2, 0x3

    new-array v2, v2, [Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    const/4 v3, 0x1

    aput-object v12, v2, v3

    const/4 v3, 0x2

    aput-object v13, v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->getLineOfSightPassCourseScore([Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;)I

    move-result v2

    add-int v17, v17, v2

    .line 123
    :cond_5
    if-eqz v8, :cond_6

    .line 124
    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v2

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/passcource/util/WordPairsScoreUtil;->getPassCourseScore(I)I

    move-result v2

    add-int v17, v17, v2

    .line 126
    :cond_6
    if-eqz v9, :cond_7

    .line 127
    invoke-virtual {v9}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v2

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/passcource/util/EvenNumbersScoreUtil;->getPassCourseScore(I)I

    move-result v2

    add-int v17, v17, v2

    .line 129
    :cond_7
    if-eqz v15, :cond_8

    .line 130
    invoke-virtual {v15}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->getConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/passcource/util/GreenDotScoreUtil;->getPassCourseScore(J)I

    move-result v2

    add-int v17, v17, v2

    .line 132
    :cond_8
    new-instance v16, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-direct/range {v16 .. v16}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;-><init>()V

    .line 133
    .local v16, "passCourseResult":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-virtual/range {v16 .. v17}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->setScore(I)V

    .line 134
    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->setType(I)V

    .line 135
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->setUnixTime(J)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    move-object/from16 v20, v0

    new-instance v2, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v15}, Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/result/presenter/PassCourseResultPresenter;Ljava/util/List;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/List;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;)V

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;->addResult(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;)V

    .line 164
    return-void
.end method
