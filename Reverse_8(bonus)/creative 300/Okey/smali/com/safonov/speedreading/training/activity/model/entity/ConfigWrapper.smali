.class public Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
.super Ljava/lang/Object;
.source "ConfigWrapper.java"


# instance fields
.field private configId:I

.field private fullscreen:Z

.field private trainingType:Lcom/safonov/speedreading/training/TrainingType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V
    .locals 0
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "fullscreen"    # Z

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    .line 24
    iput p2, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->configId:I

    .line 25
    iput-boolean p3, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->fullscreen:Z

    .line 26
    return-void
.end method


# virtual methods
.method public getConfigId()I
    .locals 1

    .prologue
    .line 37
    iget v0, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->configId:I

    return v0
.end method

.method public getTrainingType()Lcom/safonov/speedreading/training/TrainingType;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    return-object v0
.end method

.method public isFullscreen()Z
    .locals 1

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->fullscreen:Z

    return v0
.end method

.method public setConfigId(I)V
    .locals 0
    .param p1, "configId"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->configId:I

    .line 42
    return-void
.end method

.method public setFullscreen(Z)V
    .locals 0
    .param p1, "fullscreen"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->fullscreen:Z

    .line 50
    return-void
.end method

.method public setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 0
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    .line 34
    return-void
.end method
