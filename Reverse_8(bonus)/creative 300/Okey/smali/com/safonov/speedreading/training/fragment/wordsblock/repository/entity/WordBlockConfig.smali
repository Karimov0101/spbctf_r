.class public Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
.super Lio/realm/RealmObject;
.source "WordBlockConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/WordBlockConfigRealmProxyInterface;


# static fields
.field public static final FIELD_TRAINING_DURATION:Ljava/lang/String; = "trainingDuration"


# instance fields
.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private speed:I

.field private trainingDuration:J

.field private wordCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getSpeed()I
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmGet$speed()I

    move-result v0

    return v0
.end method

.method public getTrainingDuration()J
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmGet$trainingDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getWordCount()I
    .locals 1

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmGet$wordCount()I

    move-result v0

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->id:I

    return v0
.end method

.method public realmGet$speed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->speed:I

    return v0
.end method

.method public realmGet$trainingDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->trainingDuration:J

    return-wide v0
.end method

.method public realmGet$wordCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->wordCount:I

    return v0
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->id:I

    return-void
.end method

.method public realmSet$speed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->speed:I

    return-void
.end method

.method public realmSet$trainingDuration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->trainingDuration:J

    return-void
.end method

.method public realmSet$wordCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->wordCount:I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmSet$id(I)V

    .line 33
    return-void
.end method

.method public setSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 40
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmSet$speed(I)V

    .line 41
    return-void
.end method

.method public setTrainingDuration(J)V
    .locals 1
    .param p1, "trainingDuration"    # J

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmSet$trainingDuration(J)V

    .line 57
    return-void
.end method

.method public setWordCount(I)V
    .locals 0
    .param p1, "wordCount"    # I

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->realmSet$wordCount(I)V

    .line 49
    return-void
.end method
