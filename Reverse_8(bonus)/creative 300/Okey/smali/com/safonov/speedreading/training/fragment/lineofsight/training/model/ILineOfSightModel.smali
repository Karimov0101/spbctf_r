.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;
.super Ljava/lang/Object;
.source "ILineOfSightModel.java"


# virtual methods
.method public abstract getCheckedItems(IZ)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getItems(II)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end method

.method public abstract nextIsMistake(I)Z
.end method
