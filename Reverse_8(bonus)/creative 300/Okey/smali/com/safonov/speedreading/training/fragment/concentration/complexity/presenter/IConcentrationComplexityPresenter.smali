.class public interface abstract Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;
.super Ljava/lang/Object;
.source "IConcentrationComplexityPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract initConfig(I)V
.end method

.method public abstract setComplexity(I)V
.end method
