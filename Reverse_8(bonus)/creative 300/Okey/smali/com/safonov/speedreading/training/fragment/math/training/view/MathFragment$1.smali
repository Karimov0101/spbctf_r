.class Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;
.super Landroid/os/CountDownTimer;
.source "MathFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->initCountDownTimer(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

.field final synthetic val$seconds:J


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;JJJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 192
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    iput-wide p6, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;->val$seconds:J

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->access$000(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;->compleateTraining()V

    .line 201
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 195
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerBar:Landroid/widget/ProgressBar;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;->val$seconds:J

    sub-long/2addr v2, p1

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 196
    return-void
.end method
