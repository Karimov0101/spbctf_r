.class public interface abstract Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;
.super Ljava/lang/Object;
.source "IGreenDotRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;ILcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;",
            ">;"
        }
    .end annotation
.end method
