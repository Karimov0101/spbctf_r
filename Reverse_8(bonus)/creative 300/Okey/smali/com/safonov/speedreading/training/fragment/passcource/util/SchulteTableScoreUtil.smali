.class public Lcom/safonov/speedreading/training/fragment/passcource/util/SchulteTableScoreUtil;
.super Ljava/lang/Object;
.source "SchulteTableScoreUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseScore(J)I
    .locals 2
    .param p0, "time"    # J

    .prologue
    .line 12
    const-wide/16 v0, 0x1f40

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const/16 v0, 0x6f

    .line 74
    :goto_0
    return v0

    .line 13
    :cond_0
    const-wide/16 v0, 0x1fa4

    cmp-long v0, p0, v0

    if-gez v0, :cond_1

    const/16 v0, 0x6d

    goto :goto_0

    .line 14
    :cond_1
    const-wide/16 v0, 0x2008

    cmp-long v0, p0, v0

    if-gez v0, :cond_2

    const/16 v0, 0x6b

    goto :goto_0

    .line 15
    :cond_2
    const-wide/16 v0, 0x206c

    cmp-long v0, p0, v0

    if-gez v0, :cond_3

    const/16 v0, 0x69

    goto :goto_0

    .line 16
    :cond_3
    const-wide/16 v0, 0x20d0

    cmp-long v0, p0, v0

    if-gez v0, :cond_4

    const/16 v0, 0x67

    goto :goto_0

    .line 17
    :cond_4
    const-wide/16 v0, 0x2134

    cmp-long v0, p0, v0

    if-gez v0, :cond_5

    const/16 v0, 0x65

    goto :goto_0

    .line 18
    :cond_5
    const-wide/16 v0, 0x2198

    cmp-long v0, p0, v0

    if-gez v0, :cond_6

    const/16 v0, 0x63

    goto :goto_0

    .line 19
    :cond_6
    const-wide/16 v0, 0x21fc

    cmp-long v0, p0, v0

    if-gez v0, :cond_7

    const/16 v0, 0x61

    goto :goto_0

    .line 20
    :cond_7
    const-wide/16 v0, 0x2260

    cmp-long v0, p0, v0

    if-gez v0, :cond_8

    const/16 v0, 0x5f

    goto :goto_0

    .line 21
    :cond_8
    const-wide/16 v0, 0x22c4

    cmp-long v0, p0, v0

    if-gez v0, :cond_9

    const/16 v0, 0x5d

    goto :goto_0

    .line 22
    :cond_9
    const-wide/16 v0, 0x2328

    cmp-long v0, p0, v0

    if-gez v0, :cond_a

    const/16 v0, 0x5b

    goto :goto_0

    .line 23
    :cond_a
    const-wide/16 v0, 0x238c

    cmp-long v0, p0, v0

    if-gez v0, :cond_b

    const/16 v0, 0x59

    goto :goto_0

    .line 24
    :cond_b
    const-wide/16 v0, 0x23f0

    cmp-long v0, p0, v0

    if-gez v0, :cond_c

    const/16 v0, 0x57

    goto :goto_0

    .line 25
    :cond_c
    const-wide/16 v0, 0x2454

    cmp-long v0, p0, v0

    if-gez v0, :cond_d

    const/16 v0, 0x55

    goto :goto_0

    .line 26
    :cond_d
    const-wide/16 v0, 0x24b8

    cmp-long v0, p0, v0

    if-gez v0, :cond_e

    const/16 v0, 0x53

    goto :goto_0

    .line 27
    :cond_e
    const-wide/16 v0, 0x251c

    cmp-long v0, p0, v0

    if-gez v0, :cond_f

    const/16 v0, 0x51

    goto/16 :goto_0

    .line 28
    :cond_f
    const-wide/16 v0, 0x2580

    cmp-long v0, p0, v0

    if-gez v0, :cond_10

    const/16 v0, 0x4f

    goto/16 :goto_0

    .line 29
    :cond_10
    const-wide/16 v0, 0x25e4

    cmp-long v0, p0, v0

    if-gez v0, :cond_11

    const/16 v0, 0x4d

    goto/16 :goto_0

    .line 30
    :cond_11
    const-wide/16 v0, 0x2648

    cmp-long v0, p0, v0

    if-gez v0, :cond_12

    const/16 v0, 0x4b

    goto/16 :goto_0

    .line 31
    :cond_12
    const-wide/16 v0, 0x26ac

    cmp-long v0, p0, v0

    if-gez v0, :cond_13

    const/16 v0, 0x49

    goto/16 :goto_0

    .line 32
    :cond_13
    const-wide/16 v0, 0x2710

    cmp-long v0, p0, v0

    if-gez v0, :cond_14

    const/16 v0, 0x47

    goto/16 :goto_0

    .line 33
    :cond_14
    const-wide/16 v0, 0x2774

    cmp-long v0, p0, v0

    if-gez v0, :cond_15

    const/16 v0, 0x45

    goto/16 :goto_0

    .line 34
    :cond_15
    const-wide/16 v0, 0x27d8

    cmp-long v0, p0, v0

    if-gez v0, :cond_16

    const/16 v0, 0x43

    goto/16 :goto_0

    .line 35
    :cond_16
    const-wide/16 v0, 0x283c

    cmp-long v0, p0, v0

    if-gez v0, :cond_17

    const/16 v0, 0x41

    goto/16 :goto_0

    .line 36
    :cond_17
    const-wide/16 v0, 0x28a0

    cmp-long v0, p0, v0

    if-gez v0, :cond_18

    const/16 v0, 0x3f

    goto/16 :goto_0

    .line 37
    :cond_18
    const-wide/16 v0, 0x2904

    cmp-long v0, p0, v0

    if-gez v0, :cond_19

    const/16 v0, 0x3d

    goto/16 :goto_0

    .line 38
    :cond_19
    const-wide/16 v0, 0x2968

    cmp-long v0, p0, v0

    if-gez v0, :cond_1a

    const/16 v0, 0x3b

    goto/16 :goto_0

    .line 39
    :cond_1a
    const-wide/16 v0, 0x29cc

    cmp-long v0, p0, v0

    if-gez v0, :cond_1b

    const/16 v0, 0x39

    goto/16 :goto_0

    .line 40
    :cond_1b
    const-wide/16 v0, 0x2a30

    cmp-long v0, p0, v0

    if-gez v0, :cond_1c

    const/16 v0, 0x37

    goto/16 :goto_0

    .line 41
    :cond_1c
    const-wide/16 v0, 0x2a94

    cmp-long v0, p0, v0

    if-gez v0, :cond_1d

    const/16 v0, 0x35

    goto/16 :goto_0

    .line 42
    :cond_1d
    const-wide/16 v0, 0x2af8

    cmp-long v0, p0, v0

    if-gez v0, :cond_1e

    const/16 v0, 0x34

    goto/16 :goto_0

    .line 43
    :cond_1e
    const-wide/16 v0, 0x2b5c

    cmp-long v0, p0, v0

    if-gez v0, :cond_1f

    const/16 v0, 0x32

    goto/16 :goto_0

    .line 44
    :cond_1f
    const-wide/16 v0, 0x2bc0

    cmp-long v0, p0, v0

    if-gez v0, :cond_20

    const/16 v0, 0x30

    goto/16 :goto_0

    .line 45
    :cond_20
    const-wide/16 v0, 0x2c24

    cmp-long v0, p0, v0

    if-gez v0, :cond_21

    const/16 v0, 0x2e

    goto/16 :goto_0

    .line 46
    :cond_21
    const-wide/16 v0, 0x2c88

    cmp-long v0, p0, v0

    if-gez v0, :cond_22

    const/16 v0, 0x2c

    goto/16 :goto_0

    .line 47
    :cond_22
    const-wide/16 v0, 0x2cec

    cmp-long v0, p0, v0

    if-gez v0, :cond_23

    const/16 v0, 0x2a

    goto/16 :goto_0

    .line 48
    :cond_23
    const-wide/16 v0, 0x2d50

    cmp-long v0, p0, v0

    if-gez v0, :cond_24

    const/16 v0, 0x28

    goto/16 :goto_0

    .line 49
    :cond_24
    const-wide/16 v0, 0x2db4

    cmp-long v0, p0, v0

    if-gez v0, :cond_25

    const/16 v0, 0x26

    goto/16 :goto_0

    .line 50
    :cond_25
    const-wide/16 v0, 0x2e18

    cmp-long v0, p0, v0

    if-gez v0, :cond_26

    const/16 v0, 0x24

    goto/16 :goto_0

    .line 51
    :cond_26
    const-wide/16 v0, 0x2e7c

    cmp-long v0, p0, v0

    if-gez v0, :cond_27

    const/16 v0, 0x22

    goto/16 :goto_0

    .line 52
    :cond_27
    const-wide/16 v0, 0x2ee0

    cmp-long v0, p0, v0

    if-gez v0, :cond_28

    const/16 v0, 0x20

    goto/16 :goto_0

    .line 53
    :cond_28
    const-wide/16 v0, 0x30d4

    cmp-long v0, p0, v0

    if-gez v0, :cond_29

    const/16 v0, 0x1e

    goto/16 :goto_0

    .line 54
    :cond_29
    const-wide/16 v0, 0x32c8

    cmp-long v0, p0, v0

    if-gez v0, :cond_2a

    const/16 v0, 0x1c

    goto/16 :goto_0

    .line 55
    :cond_2a
    const-wide/16 v0, 0x34bc

    cmp-long v0, p0, v0

    if-gez v0, :cond_2b

    const/16 v0, 0x1a

    goto/16 :goto_0

    .line 56
    :cond_2b
    const-wide/16 v0, 0x36b0

    cmp-long v0, p0, v0

    if-gez v0, :cond_2c

    const/16 v0, 0x18

    goto/16 :goto_0

    .line 57
    :cond_2c
    const-wide/16 v0, 0x38a4

    cmp-long v0, p0, v0

    if-gez v0, :cond_2d

    const/16 v0, 0x16

    goto/16 :goto_0

    .line 58
    :cond_2d
    const-wide/16 v0, 0x3a98

    cmp-long v0, p0, v0

    if-gez v0, :cond_2e

    const/16 v0, 0x14

    goto/16 :goto_0

    .line 59
    :cond_2e
    const-wide/16 v0, 0x3e80

    cmp-long v0, p0, v0

    if-gez v0, :cond_2f

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 60
    :cond_2f
    const-wide/16 v0, 0x4268

    cmp-long v0, p0, v0

    if-gez v0, :cond_30

    const/16 v0, 0x10

    goto/16 :goto_0

    .line 61
    :cond_30
    const-wide/16 v0, 0x4650

    cmp-long v0, p0, v0

    if-gez v0, :cond_31

    const/16 v0, 0xe

    goto/16 :goto_0

    .line 62
    :cond_31
    const-wide/16 v0, 0x4a38

    cmp-long v0, p0, v0

    if-gez v0, :cond_32

    const/16 v0, 0xc

    goto/16 :goto_0

    .line 63
    :cond_32
    const-wide/16 v0, 0x4e20

    cmp-long v0, p0, v0

    if-gez v0, :cond_33

    const/16 v0, 0xa

    goto/16 :goto_0

    .line 64
    :cond_33
    const-wide/16 v0, 0x5208

    cmp-long v0, p0, v0

    if-gez v0, :cond_34

    const/16 v0, 0x9

    goto/16 :goto_0

    .line 65
    :cond_34
    const-wide/16 v0, 0x55f0

    cmp-long v0, p0, v0

    if-gez v0, :cond_35

    const/16 v0, 0x8

    goto/16 :goto_0

    .line 66
    :cond_35
    const-wide/16 v0, 0x59d8

    cmp-long v0, p0, v0

    if-gez v0, :cond_36

    const/4 v0, 0x7

    goto/16 :goto_0

    .line 67
    :cond_36
    const-wide/16 v0, 0x5dc0

    cmp-long v0, p0, v0

    if-gez v0, :cond_37

    const/4 v0, 0x6

    goto/16 :goto_0

    .line 68
    :cond_37
    const-wide/16 v0, 0x61a8

    cmp-long v0, p0, v0

    if-gez v0, :cond_38

    const/4 v0, 0x5

    goto/16 :goto_0

    .line 69
    :cond_38
    const-wide/16 v0, 0x6978

    cmp-long v0, p0, v0

    if-gez v0, :cond_39

    const/4 v0, 0x4

    goto/16 :goto_0

    .line 70
    :cond_39
    const-wide/16 v0, 0x7530

    cmp-long v0, p0, v0

    if-gez v0, :cond_3a

    const/4 v0, 0x3

    goto/16 :goto_0

    .line 71
    :cond_3a
    const-wide/32 v0, 0x88b8

    cmp-long v0, p0, v0

    if-gez v0, :cond_3b

    const/4 v0, 0x2

    goto/16 :goto_0

    .line 72
    :cond_3b
    const-wide/32 v0, 0x9c40

    cmp-long v0, p0, v0

    if-gez v0, :cond_3c

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 74
    :cond_3c
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
