.class public Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "LineOfSightRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V
    .locals 6
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;

    .prologue
    .line 83
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "rowCount"

    .line 84
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getRowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 85
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "columnCount"

    .line 87
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getColumnCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 88
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "fieldType"

    .line 90
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getFieldType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 91
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 92
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "showCount"

    .line 93
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowCount()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 94
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 95
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "showDelay"

    .line 96
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowDelay()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 97
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 98
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "mistakeProbability"

    .line 99
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getMistakeProbability()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 100
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 102
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 103
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 124
    :goto_0
    return-void

    .line 107
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 109
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setId(I)V

    .line 111
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$3;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$4;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;)V
    .locals 10
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;

    .prologue
    .line 128
    const-class v6, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 130
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 131
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 133
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 134
    aget-object v0, p1, v3

    .line 136
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "rowCount"

    .line 137
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getRowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 138
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 139
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "columnCount"

    .line 140
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getColumnCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 141
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 142
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "fieldType"

    .line 143
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getFieldType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 144
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 145
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "showCount"

    .line 146
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 147
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 148
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "showDelay"

    .line 149
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowDelay()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 150
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 151
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "mistakeProbability"

    .line 152
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getMistakeProbability()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 153
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 155
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 157
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 133
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 160
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setId(I)V

    .line 161
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    aput v4, v1, v3

    .line 164
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 168
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 169
    if-eqz p2, :cond_2

    .line 170
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 189
    :cond_2
    :goto_2
    return-void

    .line 176
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$5;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$6;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ILcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;

    .prologue
    .line 56
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 58
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;ILcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 78
    return-void
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 3
    .param p1, "lineOfSightConfigId"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 43
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .line 42
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 49
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    .locals 3
    .param p1, "lineOfSightResultId"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 36
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .line 35
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "lineOfSightConfigId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 28
    return-object v0
.end method
