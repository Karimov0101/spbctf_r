.class public interface abstract Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;
.super Ljava/lang/Object;
.source "IConcentrationView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;


# virtual methods
.method public abstract hidePointsTextView()V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract setPointsTextViewCorrect(I)V
.end method

.method public abstract setVisibleStatistics(Z)V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
