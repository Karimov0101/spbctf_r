.class public Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "WordPairsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    .line 25
    const v1, 0x7f090188

    const-string v2, "field \'scoreTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->scoreTextView:Landroid/widget/TextView;

    .line 26
    const v1, 0x7f09015f

    const-string v2, "field \'recordTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->recordTextView:Landroid/widget/TextView;

    .line 27
    const v1, 0x7f09014e

    const-string v2, "field \'progressBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 28
    const v1, 0x7f090247

    const-string v2, "field \'gridLayout\'"

    const-class v3, Landroid/support/v7/widget/GridLayout;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/GridLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 30
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 31
    .local v0, "context":Landroid/content/Context;
    const v1, 0x7f06002e

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->backgroundWhiteColor:I

    .line 32
    const v1, 0x106000b

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorWhite:I

    .line 33
    const v1, 0x1060003

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorBlack:I

    .line 34
    const v1, 0x7f06001a

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->greenColor:I

    .line 35
    const v1, 0x7f06006d

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v1

    iput v1, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->redColor:I

    .line 36
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    .line 42
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 43
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    .line 45
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->scoreTextView:Landroid/widget/TextView;

    .line 46
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->recordTextView:Landroid/widget/TextView;

    .line 47
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 48
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 49
    return-void
.end method
