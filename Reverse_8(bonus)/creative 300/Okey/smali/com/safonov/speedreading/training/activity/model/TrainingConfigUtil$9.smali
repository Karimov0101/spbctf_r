.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnMultiTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetAcceleratorCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 419
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted([I)V
    .locals 8
    .param p1, "ids"    # [I

    .prologue
    const/4 v7, 0x0

    .line 422
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v5, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    const/4 v6, 0x1

    invoke-static {v4, v5, v7, v6, v7}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 424
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    const/4 v3, 0x0

    .line 426
    .local v3, "idIndex":I
    const/4 v2, 0x5

    .line 427
    .local v2, "i":I
    :goto_0
    const/4 v4, 0x7

    if-gt v2, v4, :cond_0

    .line 430
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 431
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    sget-object v4, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 432
    invoke-virtual {v0, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 433
    aget v4, p1, v3

    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 434
    add-int/lit8 v3, v3, 0x1

    .line 436
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    aput-object v5, v4, v2

    .line 428
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 439
    .end local v0    # "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    :cond_0
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 440
    iget-object v4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$9;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v4, v5}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 442
    :cond_1
    return-void
.end method
