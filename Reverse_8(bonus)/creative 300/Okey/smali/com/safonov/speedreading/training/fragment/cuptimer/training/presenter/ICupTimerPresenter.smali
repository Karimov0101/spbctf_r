.class public interface abstract Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;
.super Ljava/lang/Object;
.source "ICupTimerPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract saveResult()V
.end method

.method public abstract startTraining(I)V
.end method
