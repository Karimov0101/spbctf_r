.class public interface abstract Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;
.super Ljava/lang/Object;
.source "IPassCourseRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository$OnTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult()Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            ">;"
        }
    .end annotation
.end method
