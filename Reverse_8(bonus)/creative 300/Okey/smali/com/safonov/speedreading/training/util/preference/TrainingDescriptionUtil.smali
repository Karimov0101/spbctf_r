.class public Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;
.super Ljava/lang/Object;
.source "TrainingDescriptionUtil.java"


# static fields
.field private static final ACCELERATOR_SHOW_HELP_KEY:Ljava/lang/String; = "accelerator_show_help"

.field private static final CONCENTRATION_SHOW_HELP_KEY:Ljava/lang/String; = "concentration_show_help"

.field private static final CUP_TIMER_SHOW_HELP_KEY:Ljava/lang/String; = "cup_timer_show_help"

.field private static final EVEN_NUMBERS_SHOW_HELP_KEY:Ljava/lang/String; = "even_numbers_show_help"

.field private static final GREEN_DOT_SHOW_HELP_KEY:Ljava/lang/String; = "green_dot_show_help"

.field private static final LINE_OF_SIGHT_SHOW_HELP_KEY:Ljava/lang/String; = "line_of_sight_show_help"

.field private static final MATHEMATICS_SHOW_HELP_KEY:Ljava/lang/String; = "mathematics_show_help"

.field private static final PASS_COURSE_SHOW_HELP_KEY:Ljava/lang/String; = "pass_course_show_help"

.field private static final REMEMBER_NUMBER_SHOW_HELP_KEY:Ljava/lang/String; = "remember_number_show_help"

.field public static final REMEMBER_WORDS_HELP_KEY:Ljava/lang/String; = "remember_words_show_help"

.field private static final SCHULTE_TABLE_SHOW_HELP_KEY:Ljava/lang/String; = "schulte_table_show_help"

.field private static final SPEED_READING_SHOW_HELP_KEY:Ljava/lang/String; = "speed_reading_show_help"

.field public static final TRUE_COLORS_HELP_KEY:Ljava/lang/String; = "true_colors_show_help"

.field private static final WORD_BLOCK_SHOW_HELP_KEY:Ljava/lang/String; = "word_block_show_help"

.field private static final WORD_PAIRS_SHOW_HELP_KEY:Ljava/lang/String; = "word_pairs_show_help"


# instance fields
.field private sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 38
    return-void
.end method


# virtual methods
.method public setShowDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;Z)V
    .locals 2
    .param p1, "descriptionFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;
    .param p2, "shouldShow"    # Z

    .prologue
    .line 80
    sget-object v0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil$1;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported fragment type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pass_course_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 134
    :goto_0
    return-void

    .line 85
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accelerator_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 88
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "word_block_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 92
    :pswitch_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accelerator_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 96
    :pswitch_4
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "schulte_table_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 99
    :pswitch_5
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "remember_number_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 102
    :pswitch_6
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "line_of_sight_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 105
    :pswitch_7
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "speed_reading_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 108
    :pswitch_8
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "word_pairs_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 111
    :pswitch_9
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "even_numbers_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 114
    :pswitch_a
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "green_dot_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 117
    :pswitch_b
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "mathematics_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 120
    :pswitch_c
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "concentration_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 123
    :pswitch_d
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "cup_timer_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 126
    :pswitch_e
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "remember_words_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 129
    :pswitch_f
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "true_colors_show_help"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public shouldShowDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 3
    .param p1, "descriptionFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v2, 0x1

    .line 41
    sget-object v0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil$1;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 75
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported fragment type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "pass_course_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 73
    :goto_0
    return v0

    .line 45
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "accelerator_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 47
    :pswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "word_block_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 48
    :pswitch_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "accelerator_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 51
    :pswitch_4
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "schulte_table_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 53
    :pswitch_5
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "remember_number_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 55
    :pswitch_6
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "line_of_sight_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 57
    :pswitch_7
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "speed_reading_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 59
    :pswitch_8
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "word_pairs_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 61
    :pswitch_9
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "even_numbers_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 63
    :pswitch_a
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "green_dot_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 65
    :pswitch_b
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "mathematics_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 67
    :pswitch_c
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "concentration_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 69
    :pswitch_d
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "cup_timer_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 71
    :pswitch_e
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "remember_words_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0

    .line 73
    :pswitch_f
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v1, "true_colors_show_help"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto/16 :goto_0

    .line 41
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method
