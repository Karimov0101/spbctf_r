.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;
.super Ljava/lang/Object;
.source "IWordBlockRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;ILcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfig(III)V
.end method
