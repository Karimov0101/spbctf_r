.class public interface abstract Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;
.super Ljava/lang/Object;
.source "IRememberWordsRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigWordsCount(II)V
.end method
