.class public Lcom/safonov/speedreading/training/activity/LineOfSightConfigUtil;
.super Ljava/lang/Object;
.source "LineOfSightConfigUtil.java"


# static fields
.field private static final DEFAULT_COLUMN_COUNT:I = 0x9

.field private static final DEFAULT_MISTAKE_PROBABILITY:I = 0x23

.field private static final DEFAULT_ROW_COUNT:I = 0x5

.field private static final DEFAULT_SHOW_COUNT:I = 0x28

.field private static final DEFAULT_SHOW_DELAY:I = 0x3e8

.field private static final PASS_COURSE_TYPE_1_FILED_TYPE:I = 0x0

.field private static final PASS_COURSE_TYPE_2_FILED_TYPE:I = 0x1

.field private static final PASS_COURSE_TYPE_3_COLUMN_COUNT:I = 0x9

.field private static final PASS_COURSE_TYPE_3_FILED_TYPE:I = 0x1

.field private static final PASS_COURSE_TYPE_3_ROW_COUNT:I = 0x9


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseType1Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 4

    .prologue
    .line 52
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;-><init>()V

    .line 54
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setRowCount(I)V

    .line 55
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setColumnCount(I)V

    .line 57
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowCount(I)V

    .line 58
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowDelay(J)V

    .line 59
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setMistakeProbability(I)V

    .line 61
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setFieldType(I)V

    .line 63
    return-object v0
.end method

.method public static getPassCourseType2Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 4

    .prologue
    .line 67
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;-><init>()V

    .line 69
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setRowCount(I)V

    .line 70
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setColumnCount(I)V

    .line 72
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowCount(I)V

    .line 73
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowDelay(J)V

    .line 74
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setMistakeProbability(I)V

    .line 76
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setFieldType(I)V

    .line 78
    return-object v0
.end method

.method public static getPassCourseType3Config()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 4

    .prologue
    const/16 v1, 0x9

    .line 82
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;-><init>()V

    .line 84
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setRowCount(I)V

    .line 85
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setColumnCount(I)V

    .line 87
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowCount(I)V

    .line 88
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowDelay(J)V

    .line 89
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setMistakeProbability(I)V

    .line 91
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setFieldType(I)V

    .line 93
    return-object v0
.end method

.method public static getUserConfig(II)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 4
    .param p0, "rowColumn"    # I
    .param p1, "fieldType"    # I

    .prologue
    .line 30
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;-><init>()V

    .line 32
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    invoke-virtual {v0, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setRowCount(I)V

    .line 33
    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setColumnCount(I)V

    .line 35
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowCount(I)V

    .line 36
    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setShowDelay(J)V

    .line 37
    const/16 v1, 0x23

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setMistakeProbability(I)V

    .line 39
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->setFieldType(I)V

    .line 41
    return-object v0
.end method
