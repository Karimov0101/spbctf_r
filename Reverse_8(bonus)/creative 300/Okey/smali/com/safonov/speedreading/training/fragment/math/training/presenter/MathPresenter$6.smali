.class Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;
.super Ljava/lang/Object;
.source "MathPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 186
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 189
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v2

    const/16 v3, 0x32

    if-ge v2, v3, :cond_1

    .line 190
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$808(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    .line 191
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$1000(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getExpression(II)Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;

    move-result-object v1

    .line 192
    .local v1, "expression1":Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$1000(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/math/training/model/MathModel;->getAllAnswers(II)[I

    move-result-object v0

    .line 194
    .local v0, "answers":[I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 195
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v2, v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setButtonsText([I)V

    .line 196
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setExpressionText(Ljava/lang/String;)V

    .line 197
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$1100(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->updateScoreView(I)V

    .line 198
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->updateProgressBar(I)V

    .line 204
    .end local v0    # "answers":[I
    .end local v1    # "expression1":Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$6;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->compleateTraining()V

    goto :goto_0
.end method
