.class public Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "RememberWordsFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/IRememberWordsView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/IRememberWordsView;",
        "Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/IRememerWordsPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/IRememberWordsView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;",
        "Landroid/view/View$OnClickListener;"
    }
.end annotation


# static fields
.field public static final DEFAULT_DURATION:I = 0x0

.field public static final DEFAULT_ELEVATION:I = 0x3

.field public static final DEFAULT_LEVELS_COUNT:I = 0x14

.field public static final DEFAULT_MARGIN:I = 0x6

.field public static final SHOW_WORD_DURATION:I = 0xbb8

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field private answerCount:I

.field answerLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09002a
    .end annotation
.end field

.field answerList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field answersContainer:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090029
    .end annotation
.end field

.field config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

.field private configId:I

.field private correctCount:I

.field private correctLevelsCount:I

.field private handler:Landroid/os/Handler;

.field private incorrectCount:I

.field private incorrectLevelsCount:I

.field private pointTextViewRunnable:Ljava/lang/Runnable;

.field pointsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090139
    .end annotation
.end field

.field private preShowWordRunnable:Ljava/lang/Runnable;

.field private progress:I

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090097
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090098
    .end annotation
.end field

.field private rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

.field requestList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private restartRunnable:Ljava/lang/Runnable;

.field rowsAnswer:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TableRow;",
            ">;"
        }
    .end annotation
.end field

.field rowsRequest:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TableRow;",
            ">;"
        }
    .end annotation
.end field

.field score:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090099
    .end annotation
.end field

.field private scoreVal:I

.field scrollView:Landroid/widget/HorizontalScrollView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900ae
    .end annotation
.end field

.field private showAnswerRunnable:Ljava/lang/Runnable;

.field private showWordRunnable:Ljava/lang/Runnable;

.field statisticsLayout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09009a
    .end annotation
.end field

.field tableLayout:Landroid/widget/TableLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901dd
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;

.field words:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private wordsCount:I

.field wordsLayout:Landroid/widget/TableLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090248
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 110
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerList:Ljava/util/List;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsRequest:Ljava/util/List;

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsAnswer:Ljava/util/List;

    .line 69
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctCount:I

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    .line 178
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->preShowWordRunnable:Ljava/lang/Runnable;

    .line 287
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerCount:I

    .line 288
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    .line 289
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectLevelsCount:I

    .line 369
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$3;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointTextViewRunnable:Ljava/lang/Runnable;

    .line 375
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->restartRunnable:Ljava/lang/Runnable;

    .line 400
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showWordRunnable:Ljava/lang/Runnable;

    .line 411
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$6;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$6;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showAnswerRunnable:Ljava/lang/Runnable;

    .line 111
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Landroid/widget/TableLayout;)Landroid/widget/TableRow;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # Landroid/widget/TableLayout;

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createTableRow(Landroid/widget/TableLayout;)Landroid/widget/TableRow;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/widget/TableRow;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createTextViewForRequest(Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1000(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->preShowWordRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showAnswerRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Landroid/widget/TableRow;

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createButtonForTableRow(Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/Button;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showWordRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$602(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    return p1
.end method

.method static synthetic access$702(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerCount:I

    return p1
.end method

.method static synthetic access$802(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p1, "x1"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctCount:I

    return p1
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 50
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    return v0
.end method

.method private createButton(Ljava/lang/String;)Landroid/widget/Button;
    .locals 7
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    const v6, 0x10301b3

    .line 489
    const/4 v3, 0x6

    invoke-virtual {p0, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->dpToPx(I)I

    move-result v0

    .line 491
    .local v0, "defaultIndenting":I
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v1, v3, v4, v5}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    .line 493
    .local v1, "params":Landroid/widget/TableRow$LayoutParams;
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 495
    new-instance v2, Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 496
    .local v2, "result":Landroid/widget/Button;
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 497
    invoke-virtual {v2, p1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 498
    const/4 v3, 0x2

    const/high16 v4, 0x41400000    # 12.0f

    invoke-virtual {v2, v3, v4}, Landroid/widget/Button;->setTextSize(IF)V

    .line 500
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_0

    .line 501
    invoke-virtual {v2, v6}, Landroid/widget/Button;->setTextAppearance(I)V

    .line 507
    :goto_0
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setAllCaps(Z)V

    .line 508
    const v3, 0x7f0800e9

    invoke-direct {p0, v2, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setBackground(Landroid/view/View;I)V

    .line 509
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setDefaultElevation(Landroid/view/View;)V

    .line 511
    div-int/lit8 v3, v0, 0x2

    div-int/lit8 v4, v0, 0x2

    invoke-virtual {v2, v3, v0, v4, v0}, Landroid/widget/Button;->setPadding(IIII)V

    .line 512
    invoke-virtual {v2, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    return-object v2

    .line 504
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Landroid/widget/Button;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private createButtonForTableRow(Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/Button;
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "row"    # Landroid/widget/TableRow;

    .prologue
    .line 481
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createButton(Ljava/lang/String;)Landroid/widget/Button;

    move-result-object v1

    .line 482
    .local v1, "button":Landroid/widget/Button;
    invoke-virtual {p2, v1}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 483
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f01000a

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 484
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p2, v0}, Landroid/widget/TableRow;->setAnimation(Landroid/view/animation/Animation;)V

    .line 485
    return-object v1
.end method

.method private createTableRow(Landroid/widget/TableLayout;)Landroid/widget/TableRow;
    .locals 4
    .param p1, "layout"    # Landroid/widget/TableLayout;

    .prologue
    .line 518
    new-instance v0, Landroid/widget/TableRow;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TableRow;-><init>(Landroid/content/Context;)V

    .line 519
    .local v0, "tableRow":Landroid/widget/TableRow;
    new-instance v1, Landroid/widget/TableRow$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/TableRow$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TableRow;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 520
    invoke-virtual {p1, v0}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    .line 521
    return-object v0
.end method

.method private createTextViewForAnswer(Ljava/lang/String;Z)Landroid/widget/TextView;
    .locals 6
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "isTrueAnswer"    # Z

    .prologue
    const/4 v5, -0x2

    .line 452
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->dpToPx(I)I

    move-result v1

    .line 454
    .local v1, "defaultIndenting":I
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 458
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 460
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 461
    .local v3, "result":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 462
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    const/4 v4, 0x2

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 465
    if-eqz p2, :cond_0

    .line 466
    const v4, 0x7f0800e8

    invoke-direct {p0, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setBackground(Landroid/view/View;I)V

    .line 470
    :goto_0
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setDefaultElevation(Landroid/view/View;)V

    .line 472
    invoke-virtual {v3, v1, v1, v1, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 474
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f01000b

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 475
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 477
    return-object v3

    .line 468
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_0
    const v4, 0x7f0800ea

    invoke-direct {p0, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setBackground(Landroid/view/View;I)V

    goto :goto_0
.end method

.method private createTextViewForRequest(Ljava/lang/String;Landroid/widget/TableRow;)Landroid/widget/TextView;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "row"    # Landroid/widget/TableRow;

    .prologue
    const/16 v7, 0x11

    .line 421
    const/4 v4, 0x6

    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->dpToPx(I)I

    move-result v1

    .line 423
    .local v1, "defaultIndenting":I
    new-instance v2, Landroid/widget/TableRow$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-direct {v2, v4, v5, v6}, Landroid/widget/TableRow$LayoutParams;-><init>(IIF)V

    .line 424
    .local v2, "params":Landroid/widget/TableRow$LayoutParams;
    invoke-virtual {v2, v1, v1, v1, v1}, Landroid/widget/TableRow$LayoutParams;->setMargins(IIII)V

    .line 426
    new-instance v3, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 427
    .local v3, "result":Landroid/widget/TextView;
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 428
    invoke-virtual {v3, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 429
    const/4 v4, 0x2

    const/high16 v5, 0x41600000    # 14.0f

    invoke-virtual {v3, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 431
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v7, :cond_0

    .line 432
    const/4 v4, 0x4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextAlignment(I)V

    .line 438
    :goto_0
    const v4, 0x7f0800e9

    invoke-direct {p0, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setBackground(Landroid/view/View;I)V

    .line 439
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setDefaultElevation(Landroid/view/View;)V

    .line 441
    mul-int/lit8 v4, v1, 0x2

    mul-int/lit8 v5, v1, 0x2

    invoke-virtual {v3, v1, v4, v1, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 443
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const v5, 0x7f01000a

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 444
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p2, v0}, Landroid/widget/TableRow;->startAnimation(Landroid/view/animation/Animation;)V

    .line 446
    invoke-virtual {p2, v3}, Landroid/widget/TableRow;->addView(Landroid/view/View;)V

    .line 448
    return-object v3

    .line 435
    .end local v0    # "anim":Landroid/view/animation/Animation;
    :cond_0
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setGravity(I)V

    goto :goto_0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .locals 5
    .param p0, "configId"    # I

    .prologue
    .line 117
    const-string v2, "configId"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    new-instance v1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;-><init>()V

    .line 119
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 120
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 122
    return-object v1
.end method

.method private setBackground(Landroid/view/View;I)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "bacgroundId"    # I

    .prologue
    .line 525
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 527
    .local v0, "sdk":I
    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 528
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 533
    :goto_0
    return-void

    .line 531
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setDefaultElevation(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 536
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 537
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->dpToPx(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setElevation(F)V

    .line 539
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/IRememerWordsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/IRememerWordsPresenter;
    .locals 7
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 101
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 102
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getRememberWordsRealm()Lio/realm/Realm;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    .line 103
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    invoke-virtual {v2, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->getConfig(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .line 104
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->config:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->getStartWordsCount()I

    move-result v2

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    .line 105
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    invoke-virtual {v2, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    move-result-object v1

    .line 106
    .local v1, "bestResult":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->recordTextView:Landroid/widget/TextView;

    const v5, 0x7f0e014b

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/Object;

    if-nez v1, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-virtual {p0, v5, v6}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    new-instance v2, Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/RememberWordsPresenter;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    invoke-direct {v2, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/RememberWordsPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;)V

    return-object v2

    .line 106
    :cond_0
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->getScore()I

    move-result v2

    goto :goto_0
.end method

.method public dpToPx(I)I
    .locals 2
    .param p1, "dp"    # I

    .prologue
    .line 542
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 543
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v0, v1, Landroid/util/DisplayMetrics;->density:F

    .line 545
    .local v0, "density":F
    int-to-float v1, p1

    mul-float/2addr v1, v0

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    return v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 253
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 254
    check-cast p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

    .line 259
    return-void

    .line 256
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement RememberNumberTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const v5, 0x7f0e0151

    const/4 v7, 0x3

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 293
    move-object v2, p1

    check-cast v2, Landroid/widget/Button;

    invoke-virtual {v2}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "word":Ljava/lang/String;
    invoke-virtual {p1, v6}, Landroid/view/View;->setEnabled(Z)V

    .line 297
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerCount:I

    .line 299
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 300
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answersContainer:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createTextViewForAnswer(Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 302
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctCount:I

    .line 303
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    .line 305
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->score:Landroid/widget/TextView;

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v5, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 306
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setPointsTextViewCorrect()V

    .line 322
    :goto_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scrollView:Landroid/widget/HorizontalScrollView;

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scrollView:Landroid/widget/HorizontalScrollView;

    const/16 v3, 0x42

    invoke-virtual {v2, v3}, Landroid/widget/HorizontalScrollView;->fullScroll(I)Z

    .line 326
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_1

    .line 327
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointTextViewRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1f4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 330
    :cond_1
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerCount:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v2, v3, :cond_3

    .line 331
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctCount:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_6

    .line 332
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    .line 333
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    .line 334
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    if-ne v2, v7, :cond_2

    .line 335
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    .line 336
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->updateConfigWordsCount(II)V

    .line 337
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    .line 350
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->tableLayout:Landroid/widget/TableLayout;

    invoke-virtual {v2}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 352
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    .line 353
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    const/16 v3, 0x14

    if-ne v2, v3, :cond_7

    .line 354
    new-instance v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;-><init>()V

    .line 355
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->setScore(I)V

    .line 356
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    new-instance v4, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$2;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$2;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V

    invoke-virtual {v2, v0, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V

    .line 367
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    :cond_3
    :goto_2
    return-void

    .line 309
    :cond_4
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answersContainer:Landroid/widget/LinearLayout;

    invoke-direct {p0, v1, v6}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->createTextViewForAnswer(Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 311
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    .line 312
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    .line 314
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    .line 315
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    if-gez v2, :cond_5

    .line 316
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    .line 318
    :cond_5
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->score:Landroid/widget/TextView;

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {p0, v5, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 319
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->setPointsTextViewIncorrect()V

    goto/16 :goto_0

    .line 341
    :cond_6
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->correctLevelsCount:I

    .line 342
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    .line 343
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    if-ne v2, v7, :cond_2

    .line 344
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    .line 345
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsCount:I

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->updateConfigWordsCount(II)V

    .line 346
    iput v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->incorrectCount:I

    goto :goto_1

    .line 365
    :cond_7
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->restartRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0xbb8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 129
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030020

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "wordsArr":[Ljava/lang/String;
    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->words:Ljava/util/List;

    .line 132
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "config_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->configId:I

    .line 135
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    .line 143
    const v1, 0x7f0b009e

    invoke-virtual {p1, v1, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 145
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 147
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progressBar:Landroid/widget/ProgressBar;

    const/16 v2, 0x14

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 148
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progressBar:Landroid/widget/ProgressBar;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progress:I

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 150
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->score:Landroid/widget/TextView;

    const v2, 0x7f0e0151

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scoreVal:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {p0, v2, v3}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->preShowWordRunnable:Ljava/lang/Runnable;

    const-wide/16 v4, 0x0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 155
    return-object v0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 270
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 271
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->preShowWordRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 272
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showWordRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 273
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->showAnswerRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 274
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->restartRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 275
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointTextViewRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 277
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->handler:Landroid/os/Handler;

    .line 278
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 279
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 264
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 265
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 266
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 283
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 284
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

    .line 285
    return-void
.end method

.method public onRememberWordsTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 235
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;->onRememberWordsTrainingCompleted(I)V

    .line 236
    return-void
.end method

.method public pauseAnimations()V
    .locals 0

    .prologue
    .line 241
    return-void
.end method

.method public resumeAnimations()V
    .locals 0

    .prologue
    .line 246
    return-void
.end method

.method public setPointsTextViewCorrect()V
    .locals 4

    .prologue
    .line 161
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    const-string v2, "+1"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1060015

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f010011

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 166
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 167
    return-void
.end method

.method public setPointsTextViewIncorrect()V
    .locals 4

    .prologue
    .line 170
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    const-string v2, "-1"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1060017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f010011

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 176
    return-void
.end method
