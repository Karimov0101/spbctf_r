.class public Lcom/safonov/speedreading/training/activity/GreenDotConfigUtil;
.super Ljava/lang/Object;
.source "GreenDotConfigUtil.java"


# static fields
.field private static final PASS_COURSE_DURATION:J = 0x493e0L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;-><init>()V

    .line 28
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    const-wide/32 v2, 0x493e0

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->setDuration(J)V

    .line 30
    return-object v0
.end method

.method public static getUserConfig(J)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    .locals 2
    .param p0, "duration"    # J
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 16
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;-><init>()V

    .line 18
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    invoke-virtual {v0, p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->setDuration(J)V

    .line 20
    return-object v0
.end method
