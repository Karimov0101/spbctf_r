.class public Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
.super Lio/realm/RealmObject;
.source "GreenDotConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/GreenDotConfigRealmProxyInterface;


# static fields
.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"


# instance fields
.field private duration:J

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->realmGet$duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public realmGet$duration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->duration:J

    return-wide v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->id:I

    return v0
.end method

.method public realmSet$duration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->duration:J

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->id:I

    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->realmSet$duration(J)V

    .line 36
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->realmSet$id(I)V

    .line 28
    return-void
.end method
