.class public Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "TrueColorsRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 25
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;

    .prologue
    .line 112
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "showTime"

    .line 113
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getShowTime()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 114
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 116
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 117
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 139
    :goto_0
    return-void

    .line 122
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 124
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->setId(I)V

    .line 126
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$5;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;

    .prologue
    .line 144
    const-class v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 146
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 147
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 149
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 150
    aget-object v0, p1, v3

    .line 152
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "showTime"

    .line 153
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getShowTime()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 154
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 156
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 157
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 149
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 159
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->setId(I)V

    .line 160
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    aput v4, v1, v3

    .line 163
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 167
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 168
    if-eqz p2, :cond_2

    .line 169
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 187
    :cond_2
    :goto_2
    return-void

    .line 174
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$6;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$7;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;ILcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;

    .prologue
    .line 68
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 70
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;ILcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 90
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .line 43
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 55
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .line 54
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 61
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .line 36
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public updateConfigShowTime(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "showTime"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 107
    return-void
.end method
