.class public Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;
.super Ljava/lang/Object;
.source "RememberNumberFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

.field private view2131296609:Landroid/view/View;

.field private view2131296610:Landroid/view/View;

.field private view2131296611:Landroid/view/View;

.field private view2131296612:Landroid/view/View;

.field private view2131296613:Landroid/view/View;

.field private view2131296614:Landroid/view/View;

.field private view2131296615:Landroid/view/View;

.field private view2131296616:Landroid/view/View;

.field private view2131296617:Landroid/view/View;

.field private view2131296618:Landroid/view/View;

.field private view2131296619:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;Landroid/view/View;)V
    .locals 11
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v10, 0x7f090165

    const v9, 0x7f090164

    const v8, 0x7f090163

    const v6, 0x7f090162

    const v7, 0x7f090161

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

    .line 51
    const v3, 0x7f090188

    const-string v4, "field \'scoreTextView\'"

    const-class v5, Landroid/widget/TextView;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->scoreTextView:Landroid/widget/TextView;

    .line 52
    const v3, 0x7f09015f

    const-string v4, "field \'recordTextView\'"

    const-class v5, Landroid/widget/TextView;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->recordTextView:Landroid/widget/TextView;

    .line 53
    const v3, 0x7f09014e

    const-string v4, "field \'progressBar\'"

    const-class v5, Landroid/widget/ProgressBar;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 54
    const v3, 0x7f09016c

    const-string v4, "field \'cardsLayout\'"

    const-class v5, Landroid/widget/LinearLayout;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardsLayout:Landroid/widget/LinearLayout;

    .line 55
    const-string v3, "method \'onNumberButtonClick\'"

    invoke-static {p2, v6, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 56
    .local v2, "view":Landroid/view/View;
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296610:Landroid/view/View;

    .line 57
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$1;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const-string v3, "method \'onNumberButtonClick\'"

    invoke-static {p2, v8, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 64
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296611:Landroid/view/View;

    .line 65
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$2;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const-string v3, "method \'onNumberButtonClick\'"

    invoke-static {p2, v9, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 72
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296612:Landroid/view/View;

    .line 73
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$3;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    const-string v3, "method \'onNumberButtonClick\'"

    invoke-static {p2, v10, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 80
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296613:Landroid/view/View;

    .line 81
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const v3, 0x7f090166

    const-string v4, "method \'onNumberButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 88
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296614:Landroid/view/View;

    .line 89
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$5;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    const v3, 0x7f090167

    const-string v4, "method \'onNumberButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 96
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296615:Landroid/view/View;

    .line 97
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$6;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$6;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    const v3, 0x7f090168

    const-string v4, "method \'onNumberButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 104
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296616:Landroid/view/View;

    .line 105
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$7;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$7;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    const v3, 0x7f090169

    const-string v4, "method \'onNumberButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 112
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296617:Landroid/view/View;

    .line 113
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$8;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$8;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    const v3, 0x7f09016a

    const-string v4, "method \'onNumberButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 120
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296618:Landroid/view/View;

    .line 121
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$9;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$9;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const-string v3, "method \'onNumberButtonClick\'"

    invoke-static {p2, v7, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 128
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296609:Landroid/view/View;

    .line 129
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$10;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$10;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    const v3, 0x7f09016b

    const-string v4, "method \'onBackspaceButtonClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 136
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296619:Landroid/view/View;

    .line 137
    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$11;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding$11;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 143
    const/16 v3, 0xb

    new-array v3, v3, [Landroid/view/View;

    const/4 v4, 0x0

    const-string v5, "field \'buttons\'"

    .line 144
    invoke-static {p2, v6, v5}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "field \'buttons\'"

    .line 145
    invoke-static {p2, v8, v5}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "field \'buttons\'"

    .line 146
    invoke-static {p2, v9, v5}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "field \'buttons\'"

    .line 147
    invoke-static {p2, v10, v5}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const v5, 0x7f090166

    const-string v6, "field \'buttons\'"

    .line 148
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const v5, 0x7f090167

    const-string v6, "field \'buttons\'"

    .line 149
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const v5, 0x7f090168

    const-string v6, "field \'buttons\'"

    .line 150
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const v5, 0x7f090169

    const-string v6, "field \'buttons\'"

    .line 151
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x8

    const v5, 0x7f09016a

    const-string v6, "field \'buttons\'"

    .line 152
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0x9

    const-string v5, "field \'buttons\'"

    .line 153
    invoke-static {p2, v7, v5}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    const/16 v4, 0xa

    const v5, 0x7f09016b

    const-string v6, "field \'buttons\'"

    .line 154
    invoke-static {p2, v5, v6}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v5

    aput-object v5, v3, v4

    .line 143
    invoke-static {v3}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->buttons:Ljava/util/List;

    .line 156
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 157
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 158
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f06001a

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->acceptGreen:I

    .line 159
    const v3, 0x7f06006d

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->reject_red:I

    .line 160
    const v3, 0x7f06002d

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->colorAccent:I

    .line 161
    const v3, 0x7f0700a3

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardMargin:I

    .line 162
    const v3, 0x7f0700a4

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardPadding:I

    .line 163
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

    .line 169
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

    .line 172
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->scoreTextView:Landroid/widget/TextView;

    .line 173
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->recordTextView:Landroid/widget/TextView;

    .line 174
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 175
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->cardsLayout:Landroid/widget/LinearLayout;

    .line 176
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->buttons:Ljava/util/List;

    .line 178
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296610:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 179
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296610:Landroid/view/View;

    .line 180
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296611:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 181
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296611:Landroid/view/View;

    .line 182
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296612:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296612:Landroid/view/View;

    .line 184
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296613:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296613:Landroid/view/View;

    .line 186
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296614:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 187
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296614:Landroid/view/View;

    .line 188
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296615:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296615:Landroid/view/View;

    .line 190
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296616:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 191
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296616:Landroid/view/View;

    .line 192
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296617:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296617:Landroid/view/View;

    .line 194
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296618:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 195
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296618:Landroid/view/View;

    .line 196
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296609:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 197
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296609:Landroid/view/View;

    .line 198
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296619:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 199
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment_ViewBinding;->view2131296619:Landroid/view/View;

    .line 200
    return-void
.end method
