.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;
.super Ljava/lang/Object;
.source "IWordColumnsPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract init(I)V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract speedDown()V
.end method

.method public abstract speedUp()V
.end method

.method public abstract startTraining()V
.end method

.method public abstract switchTrainingPauseStatus()V
.end method
