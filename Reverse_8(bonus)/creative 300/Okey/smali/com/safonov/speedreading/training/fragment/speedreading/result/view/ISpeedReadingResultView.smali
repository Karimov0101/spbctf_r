.class public interface abstract Lcom/safonov/speedreading/training/fragment/speedreading/result/view/ISpeedReadingResultView;
.super Ljava/lang/Object;
.source "ISpeedReadingResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setNewBestAverageSpeedViewVisibility(Z)V
.end method

.method public abstract setNewBestMaxSpeedViewVisibility(Z)V
.end method

.method public abstract updateAverageSpeedView(I)V
.end method

.method public abstract updateBestAverageSpeedView(I)V
.end method

.method public abstract updateBestMaxSpeedView(I)V
.end method

.method public abstract updateMaxSpeedView(I)V
.end method
