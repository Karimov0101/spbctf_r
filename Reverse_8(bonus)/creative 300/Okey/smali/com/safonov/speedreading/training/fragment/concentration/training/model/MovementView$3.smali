.class Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;
.super Ljava/lang/Object;
.source "MovementView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 452
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const v4, -0xffff01

    .line 456
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$102(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z

    .line 457
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$202(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z

    .line 459
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 460
    .local v1, "correctPaint":Landroid/graphics/Paint;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$300(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 462
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$400(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .line 463
    .local v0, "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getAnswerPaint()Landroid/graphics/Paint;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Paint;->getColor()I

    move-result v3

    if-eq v3, v4, :cond_0

    .line 464
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setAnswerPaint(Landroid/graphics/Paint;)V

    goto :goto_0

    .line 468
    .end local v0    # "circle":Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$500(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V

    .line 470
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$600(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 471
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$800(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$700(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->setPointsTextViewCorrect(I)V

    .line 473
    :cond_2
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1000(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$3;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$900(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x5dc

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 474
    return-void
.end method
