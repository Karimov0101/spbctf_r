.class public interface abstract Lcom/safonov/speedreading/training/activity/view/ITrainingView;
.super Ljava/lang/Object;
.source "ITrainingView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragmentListner;
.implements Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;
.implements Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;
.implements Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;
.implements Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragmentListener;
.implements Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragmentListener;
.implements Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;


# virtual methods
.method public abstract dismissPauseDialog()V
.end method

.method public abstract finish()V
.end method

.method public abstract hideActionBar()V
.end method

.method public abstract pauseFragmentAnimations()V
.end method

.method public abstract restartCourse()V
.end method

.method public abstract resumeFragmentAnimations()V
.end method

.method public abstract setAcceleratorCourseResultFragment([I)V
.end method

.method public abstract setArrowActionBar()V
.end method

.method public abstract setArrowTrainingToolbar(Z)V
.end method

.method public abstract setCloseActionBar()V
.end method

.method public abstract setConcentrationComplexityFragment(I)V
.end method

.method public abstract setConcentrationFragment(I)V
.end method

.method public abstract setConcentrationResultFragment(I)V
.end method

.method public abstract setCupTimerFragment(I)V
.end method

.method public abstract setCupTimerResultFragment(I)V
.end method

.method public abstract setCupTimerSettingsFragment()V
.end method

.method public abstract setDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .param p1    # Lcom/safonov/speedreading/training/FragmentType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setEvenNumbersFragment(I)V
.end method

.method public abstract setEvenNumbersPassCourseResultFragment(I)V
.end method

.method public abstract setEvenNumbersResultFragment(I)V
.end method

.method public abstract setFlashWordsFragment(I)V
.end method

.method public abstract setFlashWordsPassCourseResultFragment(I)V
.end method

.method public abstract setFlashWordsResultFragment(I)V
.end method

.method public abstract setFlashWordsSettingsFragment()V
.end method

.method public abstract setGreenDotFragment(I)V
.end method

.method public abstract setGreenDotPassCourseResultFragment(I)V
.end method

.method public abstract setGreenDotResultFragment(I)V
.end method

.method public abstract setGreenDotSettingsFragment()V
.end method

.method public abstract setHelpFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .param p1    # Lcom/safonov/speedreading/training/FragmentType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setInterstitialFragment(Lcom/safonov/speedreading/training/TrainingType;I)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract setLineOfSightFragment(I)V
.end method

.method public abstract setLineOfSightPassCourseResultFragment(I)V
.end method

.method public abstract setLineOfSightResultFragment(I)V
.end method

.method public abstract setLineOfSightSettingsFragment()V
.end method

.method public abstract setMathComplexityFragment(I)V
.end method

.method public abstract setMathFragment(I)V
.end method

.method public abstract setMathResultFragment(I)V
.end method

.method public abstract setMathSettingsFragment()V
.end method

.method public abstract setPassCourseResultFragment([I)V
.end method

.method public abstract setPrepareFragment()V
.end method

.method public abstract setRememberNumberFragment(I)V
.end method

.method public abstract setRememberNumberPassCourseResultFragment(I)V
.end method

.method public abstract setRememberNumberResultFragment(I)V
.end method

.method public abstract setRememberWordsFragment(I)V
.end method

.method public abstract setRememberWordsResultFragment(I)V
.end method

.method public abstract setSchulteTableFragment(I)V
.end method

.method public abstract setSchulteTablePassCourseResultFragment(I)V
.end method

.method public abstract setSchulteTableResultFragment(I)V
.end method

.method public abstract setSchulteTableSettingsFragment()V
.end method

.method public abstract setSpeedReadingFragment(I)V
.end method

.method public abstract setSpeedReadingPassCourseResultFragment(I)V
.end method

.method public abstract setSpeedReadingResultFragment(I)V
.end method

.method public abstract setToolbarTitle(I)V
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
.end method

.method public abstract setTrueColorsFragment(I)V
.end method

.method public abstract setTrueColorsResultFragment(I)V
.end method

.method public abstract setWordPairsFragment(I)V
.end method

.method public abstract setWordPairsPassCourseResultFragment(I)V
.end method

.method public abstract setWordPairsResultFragment(I)V
.end method

.method public abstract setWordsBlockCourseResultFragment(I)V
.end method

.method public abstract setWordsBlockFragment(I)V
.end method

.method public abstract setWordsBlockResultFragment(I)V
.end method

.method public abstract setWordsBlockSettingsFragment()V
.end method

.method public abstract setWordsColumnsCourseResultFragment(I)V
.end method

.method public abstract setWordsColumnsFragment(I)V
.end method

.method public abstract setWordsColumnsResultFragment(I)V
.end method

.method public abstract setWordsColumnsSettingsFragment()V
.end method

.method public abstract showActionBar()V
.end method

.method public abstract showPauseDialog(ZZZ)V
.end method

.method public abstract startCourseReaderActivity(Lcom/safonov/speedreading/training/TrainingType;)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method
