.class Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "WordPairsPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCanceled:Z

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    .prologue
    .line 57
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->val$configId:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 70
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->isCanceled:Z

    .line 71
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 76
    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->isCanceled:Z

    if-nez v1, :cond_0

    .line 78
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;-><init>()V

    .line 79
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setScore(I)V

    .line 80
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->setUnixTime(J)V

    .line 82
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->addResult(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ILcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository$OnSingleTransactionCallback;)V

    .line 89
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter$2;->isCanceled:Z

    .line 65
    return-void
.end method
