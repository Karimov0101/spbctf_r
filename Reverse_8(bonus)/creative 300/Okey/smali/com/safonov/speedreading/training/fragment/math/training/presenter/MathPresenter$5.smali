.class Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;
.super Ljava/lang/Object;
.source "MathPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 172
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->refreshExpressionTextView()V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->timerStart()V

    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setButtonsEnabled(Z)V

    .line 175
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setCorrectAnswerGone()V

    .line 177
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->compleateTraining()V

    .line 183
    :goto_0
    return-void

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
