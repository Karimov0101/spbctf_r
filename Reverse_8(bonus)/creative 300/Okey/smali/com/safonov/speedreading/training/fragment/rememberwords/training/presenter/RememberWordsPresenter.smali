.class public Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/RememberWordsPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "RememberWordsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/IRememerWordsPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/IRememberWordsView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/IRememerWordsPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/presenter/RememberWordsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;

    .line 20
    return-void
.end method
