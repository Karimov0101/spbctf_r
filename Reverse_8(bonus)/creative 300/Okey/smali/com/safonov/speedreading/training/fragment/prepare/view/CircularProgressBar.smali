.class public Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;
.super Landroid/view/View;
.source "CircularProgressBar.java"


# instance fields
.field private backLineColor:I

.field private drawCircle:Landroid/graphics/RectF;

.field private frontLineColor:I

.field private lineWidth:F

.field private maxProgress:I

.field private maxSweepAngle:F

.field private paint:Landroid/graphics/Paint;

.field private progress:I

.field private final startAngle:F

.field private sweepAngle:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 16
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    .line 17
    const v0, -0x777778

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->backLineColor:I

    .line 19
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    .line 21
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    .line 22
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxProgress:I

    .line 24
    const/high16 v0, -0x3d4c0000    # -90.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->startAngle:F

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    .line 27
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxSweepAngle:F

    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->init(Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 16
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    .line 17
    const v0, -0x777778

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->backLineColor:I

    .line 19
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    .line 21
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    .line 22
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxProgress:I

    .line 24
    const/high16 v0, -0x3d4c0000    # -90.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->startAngle:F

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    .line 27
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxSweepAngle:F

    .line 38
    invoke-direct {p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->init(Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 16
    const/high16 v0, -0x10000

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    .line 17
    const v0, -0x777778

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->backLineColor:I

    .line 19
    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    .line 21
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    .line 22
    const/16 v0, 0x64

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxProgress:I

    .line 24
    const/high16 v0, -0x3d4c0000    # -90.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->startAngle:F

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    .line 27
    const/high16 v0, 0x43b40000    # 360.0f

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxSweepAngle:F

    .line 43
    invoke-direct {p0, p2, p3}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->init(Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method private calcSweepAngleFromProgress(I)F
    .locals 2
    .param p1, "progress"    # I

    .prologue
    .line 138
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxSweepAngle:F

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxProgress:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    int-to-float v1, p1

    mul-float/2addr v0, v1

    return v0
.end method

.method private drawArc(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 111
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 116
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->drawCircle:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 117
    return-void
.end method

.method private drawDefaultArc(Landroid/graphics/Canvas;)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->backLineColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 106
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->drawCircle:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxSweepAngle:F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 107
    return-void
.end method

.method private init(Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "attrs"    # Landroid/util/AttributeSet;
    .param p2, "defStyle"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/safonov/speedreading/R$styleable;->CircularProgressBar:[I

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 51
    .local v0, "a":Landroid/content/res/TypedArray;
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    .line 55
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->backLineColor:I

    .line 59
    const/4 v1, 0x2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    .line 63
    const/4 v1, 0x3

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    .line 67
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->calcSweepAngleFromProgress(I)F

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    .line 69
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 71
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v4}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->paint:Landroid/graphics/Paint;

    .line 72
    return-void
.end method

.method private initMeasure()V
    .locals 13

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getPaddingLeft()I

    move-result v4

    .line 87
    .local v4, "paddingLeft":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getPaddingRight()I

    move-result v5

    .line 88
    .local v5, "paddingRight":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getPaddingBottom()I

    move-result v3

    .line 89
    .local v3, "paddingBottom":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getPaddingTop()I

    move-result v6

    .line 91
    .local v6, "paddingTop":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getWidth()I

    move-result v7

    sub-int/2addr v7, v4

    sub-int v1, v7, v5

    .line 92
    .local v1, "contentWidth":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->getHeight()I

    move-result v7

    sub-int/2addr v7, v6

    sub-int v0, v7, v3

    .line 94
    .local v0, "contentHeight":I
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    int-to-float v2, v7

    .line 96
    .local v2, "diameter":F
    new-instance v7, Landroid/graphics/RectF;

    int-to-float v8, v4

    iget v9, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    add-float/2addr v8, v9

    int-to-float v9, v6

    iget v10, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    add-float/2addr v9, v10

    int-to-float v10, v4

    add-float/2addr v10, v2

    iget v11, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    sub-float/2addr v10, v11

    int-to-float v11, v6

    add-float/2addr v11, v2

    iget v12, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    sub-float/2addr v11, v12

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v7, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->drawCircle:Landroid/graphics/RectF;

    .line 97
    return-void
.end method


# virtual methods
.method public getFrontLineColor()I
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    return v0
.end method

.method public getLineWidth()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 78
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->initMeasure()V

    .line 80
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->drawDefaultArc(Landroid/graphics/Canvas;)V

    .line 81
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->drawArc(Landroid/graphics/Canvas;)V

    .line 82
    return-void
.end method

.method public setFrontLineColor(I)V
    .locals 0
    .param p1, "frontLineColor"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->frontLineColor:I

    .line 125
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->invalidate()V

    .line 126
    return-void
.end method

.method public setLineWidth(F)V
    .locals 0
    .param p1, "lineWidth"    # F

    .prologue
    .line 133
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->lineWidth:F

    .line 134
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->invalidate()V

    .line 135
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 142
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->maxProgress:I

    if-gt p1, v0, :cond_0

    .line 143
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->progress:I

    .line 144
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->calcSweepAngleFromProgress(I)F

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->sweepAngle:F

    .line 145
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;->invalidate()V

    .line 147
    :cond_0
    return-void
.end method
