.class public Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;
.super Ljava/lang/Object;
.source "NumberWrapper.java"


# instance fields
.field private isEvenNumber:Z

.field private number:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "number"    # Ljava/lang/String;
    .param p2, "isEvenNumber"    # Z

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->number:Ljava/lang/String;

    .line 14
    iput-boolean p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->isEvenNumber:Z

    .line 15
    return-void
.end method


# virtual methods
.method public getNumber()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->number:Ljava/lang/String;

    return-object v0
.end method

.method public isEvenNumber()Z
    .locals 1

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->isEvenNumber:Z

    return v0
.end method
