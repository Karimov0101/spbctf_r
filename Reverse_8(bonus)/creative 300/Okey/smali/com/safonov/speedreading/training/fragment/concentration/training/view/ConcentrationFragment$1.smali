.class Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;
.super Ljava/lang/Object;
.source "ConcentrationFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

.field final synthetic val$config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

.field final synthetic val$count:Landroid/widget/NumberPicker;

.field final synthetic val$size:Landroid/widget/NumberPicker;

.field final synthetic val$speed:Landroid/widget/NumberPicker;

.field final synthetic val$speedValues:[Ljava/lang/String;

.field final synthetic val$valueSet:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;Landroid/widget/NumberPicker;[Ljava/lang/String;Landroid/widget/NumberPicker;[Ljava/lang/String;Landroid/widget/NumberPicker;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$count:Landroid/widget/NumberPicker;

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$speedValues:[Ljava/lang/String;

    iput-object p4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$speed:Landroid/widget/NumberPicker;

    iput-object p5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$valueSet:[Ljava/lang/String;

    iput-object p6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$size:Landroid/widget/NumberPicker;

    iput-object p7, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$count:Landroid/widget/NumberPicker;

    invoke-virtual {v0}, Landroid/widget/NumberPicker;->getValue()I

    move-result v2

    .line 158
    .local v2, "starCount":I
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$speedValues:[Ljava/lang/String;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$speed:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 159
    .local v3, "speedStr":I
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$valueSet:[Ljava/lang/String;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$size:Landroid/widget/NumberPicker;

    invoke-virtual {v1}, Landroid/widget/NumberPicker;->getValue()I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 161
    .local v4, "sizeStr":I
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->access$000(Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;)Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->val$config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v1

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->updateConfigCustom(IIII)V

    .line 163
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    iget-boolean v5, v5, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;-><init>(Landroid/content/Context;IIIZ)V

    iput-object v0, v6, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesContatiner:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    iget-object v1, v1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 165
    return-void
.end method
