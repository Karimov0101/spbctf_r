.class public Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment_ViewBinding;
.super Ljava/lang/Object;
.source "PassCourseResultFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    .line 24
    const v2, 0x7f090131

    const-string v3, "field \'layout\'"

    const-class v4, Landroid/widget/LinearLayout;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    .line 26
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 27
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 28
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f060070

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartLine1Color:I

    .line 29
    const v2, 0x7f06006e

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItem1Color:I

    .line 30
    const v2, 0x7f060071

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartLine2Color:I

    .line 31
    const v2, 0x7f06006f

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItem2Color:I

    .line 32
    const v2, 0x7f0a000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItemCircleRadiusDp:I

    .line 33
    const v2, 0x7f0a000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartItemValueTextSizeDp:I

    .line 34
    const v2, 0x7f0a000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->chartLineWidthDp:I

    .line 35
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    .line 41
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    .line 44
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->layout:Landroid/widget/LinearLayout;

    .line 45
    return-void
.end method
