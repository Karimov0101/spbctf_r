.class public Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "AcceleratorCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/IAcceleratorCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

.field private wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

.field private wordsColumnsRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V
    .locals 0
    .param p1, "wordsColumnsRepository"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "wordBlockRepository"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "flashWordRepository"    # Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->wordsColumnsRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    .line 28
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    .line 29
    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .line 30
    return-void
.end method


# virtual methods
.method public initTrainingResults([I)V
    .locals 6
    .param p1, "resultIds"    # [I

    .prologue
    .line 34
    const-wide/16 v0, 0x0

    .line 35
    .local v0, "duration":J
    const/4 v2, 0x0

    .line 36
    .local v2, "i":I
    :goto_0
    const/4 v3, 0x4

    if-gt v2, v3, :cond_0

    .line 38
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->wordsColumnsRepository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    aget v4, p1, v2

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getTrainingDuration()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 37
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :cond_0
    const/4 v2, 0x5

    .line 42
    :goto_1
    const/4 v3, 0x7

    if-gt v2, v3, :cond_1

    .line 44
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->flashWordRepository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    aget v4, p1, v2

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;->getConfig()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getTrainingDuration()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 43
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 47
    :cond_1
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->wordBlockRepository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    const/16 v4, 0x8

    aget v4, p1, v4

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getTrainingDuration()J

    move-result-wide v4

    add-long/2addr v0, v4

    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->isViewAttached()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 50
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/presenter/AcceleratorCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;

    invoke-interface {v3, v0, v1}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/IAcceleratorCourseResultView;->updateTrainingDurationView(J)V

    .line 52
    :cond_2
    return-void
.end method
