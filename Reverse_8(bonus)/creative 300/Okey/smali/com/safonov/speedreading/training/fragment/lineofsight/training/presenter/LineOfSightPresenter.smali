.class public Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "LineOfSightPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/ILineOfSightPresenter;"
    }
.end annotation


# static fields
.field private static final CENTER_VALUE:Ljava/lang/String; = "\u2022"


# instance fields
.field private checkedItemCount:I

.field private config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

.field private configId:I

.field private foundMistakes:I

.field private handler:Landroid/os/Handler;

.field private isPaused:Z

.field private isPreShowAnimationCompleted:Z

.field private mistakes:I

.field private model:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

.field private preShowItemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

.field private showIndex:I

.field private showRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V
    .locals 1
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    .line 87
    new-instance v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showRunnable:Ljava/lang/Runnable;

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->model:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    .line 33
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPaused:Z

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showIndex:I

    return v0
.end method

.method static synthetic access$108(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showIndex:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->model:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->mistakes:I

    return v0
.end method

.method static synthetic access$408(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->mistakes:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->mistakes:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->checkedItemCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->foundMistakes:I

    return v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->configId:I

    return v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    return-object v0
.end method


# virtual methods
.method public cancelTraining()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPaused:Z

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 131
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->preShowItemList:Ljava/util/List;

    .line 132
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showRunnable:Ljava/lang/Runnable;

    .line 133
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    .line 134
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->model:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    .line 135
    return-void
.end method

.method public init(I)V
    .locals 5
    .param p1, "configId"    # I

    .prologue
    .line 52
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->configId:I

    .line 53
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .line 54
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getFieldType()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x4

    :goto_0
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->checkedItemCount:I

    .line 56
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getFieldType()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->preShowItemList:Ljava/util/List;

    .line 57
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->checkedItemCount:I

    if-ge v0, v1, :cond_1

    .line 58
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->preShowItemList:Ljava/util/List;

    const-string v2, "\u2022"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 57
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 54
    .end local v0    # "i":I
    :cond_0
    const/16 v1, 0x8

    goto :goto_0

    .line 61
    .restart local v0    # "i":I
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getRowCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getColumnCount()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getFieldType()I

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->initBoardView(III)V

    .line 64
    :cond_2
    return-void
.end method

.method public onCheckButtonPressed()V
    .locals 1

    .prologue
    .line 164
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->foundMistakes:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->foundMistakes:I

    .line 165
    return-void
.end method

.method public onPreShowAnimationCompleted()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 81
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPreShowAnimationCompleted:Z

    .line 83
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->setCheckButtonEnabled(Z)V

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showRunnable:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowDelay()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 85
    return-void
.end method

.method public pauseTraining()V
    .locals 1

    .prologue
    .line 141
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPreShowAnimationCompleted:Z

    if-eqz v0, :cond_1

    .line 142
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPaused:Z

    .line 148
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->cancelPreShowAnimation()V

    goto :goto_0
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 152
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPreShowAnimationCompleted:Z

    if-eqz v0, :cond_1

    .line 153
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isPaused:Z

    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->showRunnable:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowDelay()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->startPreShowAnimation()V

    goto :goto_0
.end method

.method public startTraining()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->setCheckButtonEnabled(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->model:Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getRowCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getColumnCount()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;->getItems(II)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->setItemsData(Ljava/util/List;)V

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->preShowItemList:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->setCheckedItemsData(Ljava/util/List;)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->startPreShowAnimation()V

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->config:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->initProgressBar(I)V

    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->updateProgressBar(I)V

    .line 75
    return-void
.end method
