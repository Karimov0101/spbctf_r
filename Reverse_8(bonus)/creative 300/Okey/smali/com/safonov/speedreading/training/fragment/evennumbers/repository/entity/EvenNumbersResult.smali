.class public Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
.super Lio/realm/RealmObject;
.source "EvenNumbersResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/EvenNumbersResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .locals 1

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->score:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->score:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .prologue
    .line 59
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;)V

    .line 60
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmSet$id(I)V

    .line 29
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmSet$score(I)V

    .line 42
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 51
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->realmSet$unixTime(J)V

    .line 52
    return-void
.end method
