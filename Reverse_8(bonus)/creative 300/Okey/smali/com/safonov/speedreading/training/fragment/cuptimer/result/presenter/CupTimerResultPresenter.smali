.class public Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "CupTimerResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    move-result-object v0

    .line 28
    .local v0, "cupTimerResult":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;->getResultList()Ljava/util/List;

    move-result-object v1

    .line 30
    .local v1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->getConfig()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v4

    const-wide/16 v6, 0x2

    mul-long/2addr v4, v6

    invoke-interface {v2, v4, v5}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;->updateTrainingDurationView(J)V

    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;->setChartViewData(Ljava/util/List;)V

    .line 34
    :cond_0
    return-void
.end method
