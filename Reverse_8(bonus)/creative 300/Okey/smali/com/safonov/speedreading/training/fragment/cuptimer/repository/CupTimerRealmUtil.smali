.class public Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "CupTimerRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V
    .locals 6
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;

    .prologue
    .line 84
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "duration"

    .line 85
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 86
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v2

    .line 87
    invoke-virtual {v2}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "type"

    .line 88
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getType()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 89
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 91
    .local v0, "configs":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;>;"
    invoke-virtual {v0}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    invoke-virtual {v0}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 113
    :goto_0
    return-void

    .line 96
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v1

    .line 98
    .local v1, "nextId":I
    invoke-virtual {p1, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->setId(I)V

    .line 100
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$3;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$4;

    invoke-direct {v4, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;)V
    .locals 10
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;

    .prologue
    .line 119
    const-class v6, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 121
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 122
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 124
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 125
    aget-object v0, p1, v3

    .line 127
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "duration"

    .line 128
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 129
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v6

    .line 130
    invoke-virtual {v6}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "type"

    .line 131
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getType()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 132
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 134
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 136
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 124
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 139
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->setId(I)V

    .line 140
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    aput v4, v1, v3

    .line 143
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 147
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 148
    if-eqz p2, :cond_2

    .line 149
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 168
    :cond_2
    :goto_2
    return-void

    .line 155
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$5;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$6;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;

    .prologue
    .line 56
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 58
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 78
    return-void
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 41
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 42
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .line 41
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 48
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 48
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 29
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    .line 28
    return-object v0
.end method

.method public getResultList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 35
    return-object v0
.end method
