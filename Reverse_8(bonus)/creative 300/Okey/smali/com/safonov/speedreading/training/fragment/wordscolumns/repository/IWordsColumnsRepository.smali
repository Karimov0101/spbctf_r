.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;
.super Ljava/lang/Object;
.source "IWordsColumnsRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;ILcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;",
            ">;"
        }
    .end annotation
.end method

.method public abstract updateConfigSpeed(II)V
.end method
