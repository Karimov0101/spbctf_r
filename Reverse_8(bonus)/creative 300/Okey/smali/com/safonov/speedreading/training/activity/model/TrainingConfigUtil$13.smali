.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetFlashWordsConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 529
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 532
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {v0, v2, p1, v5}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V

    .line 533
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v3, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4, v5, v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 535
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    if-eqz v2, :cond_0

    .line 536
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$13;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    new-instance v3, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v3, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;->onConfigResponse(Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 538
    :cond_0
    return-void
.end method
