.class public Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "TrueColorsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

.field private view2131296814:Landroid/view/View;

.field private view2131296815:Landroid/view/View;

.field private view2131296816:Landroid/view/View;

.field private view2131296817:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;Landroid/view/View;)V
    .locals 9
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v8, 0x7f090231

    const v7, 0x7f090230

    const v6, 0x7f09022f

    const v5, 0x7f09022e

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    .line 33
    const v1, 0x7f090239

    const-string v2, "field \'scoreTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->scoreTextView:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f090237

    const-string v2, "field \'recordTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->recordTextView:Landroid/widget/TextView;

    .line 35
    const v1, 0x7f090235

    const-string v2, "field \'progressBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 36
    const v1, 0x7f0900d1

    const-string v2, "field \'answerImageView\'"

    const-class v3, Landroid/widget/ImageView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    .line 37
    const v1, 0x7f09023d

    const-string v2, "field \'timerBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 38
    const v1, 0x7f090236

    const-string v2, "field \'questionTV\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->questionTV:Landroid/widget/TextView;

    .line 39
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v5, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296814:Landroid/view/View;

    .line 41
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v6, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296815:Landroid/view/View;

    .line 49
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v7, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296816:Landroid/view/View;

    .line 57
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const-string v1, "method \'onNumberButtonClick\'"

    invoke-static {p2, v8, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 64
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296817:Landroid/view/View;

    .line 65
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const/4 v1, 0x4

    new-array v2, v1, [Landroid/widget/TextView;

    const/4 v3, 0x0

    const-string v1, "field \'answerButtons\'"

    const-class v4, Landroid/widget/TextView;

    .line 72
    invoke-static {p2, v5, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    const/4 v3, 0x1

    const-string v1, "field \'answerButtons\'"

    const-class v4, Landroid/widget/TextView;

    .line 73
    invoke-static {p2, v6, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    const/4 v3, 0x2

    const-string v1, "field \'answerButtons\'"

    const-class v4, Landroid/widget/TextView;

    .line 74
    invoke-static {p2, v7, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    const/4 v3, 0x3

    const-string v1, "field \'answerButtons\'"

    const-class v4, Landroid/widget/TextView;

    .line 75
    invoke-static {p2, v8, v1, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aput-object v1, v2, v3

    .line 71
    invoke-static {v2}, Lbutterknife/internal/Utils;->listOf([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    .line 76
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    .line 82
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    .line 85
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->scoreTextView:Landroid/widget/TextView;

    .line 86
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->recordTextView:Landroid/widget/TextView;

    .line 87
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 88
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->questionTV:Landroid/widget/TextView;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296814:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296814:Landroid/view/View;

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296815:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 96
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296815:Landroid/view/View;

    .line 97
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296816:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296816:Landroid/view/View;

    .line 99
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296817:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment_ViewBinding;->view2131296817:Landroid/view/View;

    .line 101
    return-void
.end method
