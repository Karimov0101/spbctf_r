.class public Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SchulteTablePresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/ISchulteTablePresenter;"
    }
.end annotation


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

.field private configId:I

.field private currentPlayedTime:J

.field private isTrueAnswer:Z

.field private itemCount:I

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private model:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;

.field private nextItemValue:Ljava/lang/String;

.field private repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

.field private timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V
    .locals 1
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    const-string v0, "1"

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->model:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;

    .line 33
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    .line 34
    return-void
.end method


# virtual methods
.method public cancelTraining()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->cancel()J

    .line 79
    return-void
.end method

.method public init(I)V
    .locals 5
    .param p1, "configId"    # I

    .prologue
    .line 41
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->configId:I

    .line 42
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .line 44
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getColumnCount()I

    move-result v2

    mul-int/2addr v1, v2

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->itemCount:I

    .line 46
    new-instance v1, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;)V

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;-><init>(Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    .line 55
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v0

    .line 56
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getRowCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getColumnCount()I

    move-result v3

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen()Z

    move-result v4

    invoke-interface {v1, v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->initBoard(IIZ)V

    .line 59
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->config:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen()Z

    move-result v1

    if-nez v1, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    if-nez v0, :cond_1

    const-wide/16 v2, 0x0

    :goto_0
    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->updateBestTimeView(J)V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->updateNextItemView(Ljava/lang/String;)V

    .line 64
    :cond_0
    return-void

    .line 60
    :cond_1
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v2

    goto :goto_0
.end method

.method public onItemTouchDown(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isTrueAnswer:Z

    .line 106
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isTrueAnswer:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->model:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->itemCount:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;->getNextNumberItem(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isTrueAnswer:Z

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->itemTouchDown(IZ)V

    .line 113
    :cond_1
    return-void
.end method

.method public onItemTouchUp(I)V
    .locals 4
    .param p1, "itemIndex"    # I

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-boolean v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isTrueAnswer:Z

    invoke-interface {v1, p1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->itemTouchUp(IZ)V

    .line 120
    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isTrueAnswer:Z

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 122
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->setBoardItemsEnable(Z)V

    .line 124
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->cancel()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->currentPlayedTime:J

    .line 126
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;-><init>()V

    .line 127
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->setTime(J)V

    .line 128
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->setUnixTime(J)V

    .line 130
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->addResult(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;ILcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;)V

    .line 143
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    :cond_0
    :goto_0
    return-void

    .line 139
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->nextItemValue:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->updateNextItemView(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->setBoardItemsEnable(Z)V

    .line 89
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->currentPlayedTime:J

    .line 90
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->setBoardItemsEnable(Z)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->start(J)V

    .line 99
    return-void
.end method

.method public startTraining()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker;->start()V

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->model:Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->itemCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/model/ISchulteTableModel;->getRandomNumbers(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->items:Ljava/util/List;

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->items:Ljava/util/List;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->setBoardItems(Ljava/util/List;)V

    .line 74
    :cond_0
    return-void
.end method
