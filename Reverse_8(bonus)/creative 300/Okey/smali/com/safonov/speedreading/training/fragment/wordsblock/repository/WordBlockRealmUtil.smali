.class public Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "WordBlockRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V
    .locals 6
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;

    .prologue
    .line 99
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "trainingDuration"

    .line 100
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getTrainingDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 101
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 104
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 126
    :goto_0
    return-void

    .line 109
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 111
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->setId(I)V

    .line 113
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$5;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;)V
    .locals 10
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;

    .prologue
    .line 130
    const-class v6, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 132
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 133
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 135
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 136
    aget-object v0, p1, v3

    .line 138
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "trainingDuration"

    .line 139
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getTrainingDuration()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 140
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 142
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 144
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 135
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 147
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->setId(I)V

    .line 148
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    aput v4, v1, v3

    .line 151
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 155
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 156
    if-eqz p2, :cond_2

    .line 157
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 176
    :cond_2
    :goto_2
    return-void

    .line 163
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$6;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$7;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;ILcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;

    .prologue
    .line 73
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 75
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$2;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;ILcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$3;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 95
    return-void
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .line 43
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 50
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    .line 29
    return-object v0
.end method

.method public getResultList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 36
    return-object v0
.end method

.method public updateConfig(III)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "speed"    # I
    .param p3, "wordCount"    # I

    .prologue
    .line 56
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$1;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;III)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 69
    return-void
.end method
