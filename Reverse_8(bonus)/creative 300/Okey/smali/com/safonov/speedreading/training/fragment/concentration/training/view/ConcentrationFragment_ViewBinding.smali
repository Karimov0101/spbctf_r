.class public Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment_ViewBinding;
.super Ljava/lang/Object;
.source "ConcentrationFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    .line 23
    const v0, 0x7f09005f

    const-string v1, "field \'scoreTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->scoreTextView:Landroid/widget/TextView;

    .line 24
    const v0, 0x7f09005d

    const-string v1, "field \'recordTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->recordTextView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f09005c

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 26
    const v0, 0x7f09004d

    const-string v1, "field \'circlesContatiner\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesContatiner:Landroid/widget/LinearLayout;

    .line 27
    const v0, 0x7f090061

    const-string v1, "field \'layout\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->layout:Landroid/widget/LinearLayout;

    .line 28
    const v0, 0x7f09005b

    const-string v1, "field \'concentrationPointsTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    .line 29
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    .line 35
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 36
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    .line 38
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->scoreTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->recordTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 41
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesContatiner:Landroid/widget/LinearLayout;

    .line 42
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->layout:Landroid/widget/LinearLayout;

    .line 43
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    .line 44
    return-void
.end method
