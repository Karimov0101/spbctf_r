.class Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;
.super Ljava/lang/Object;
.source "FlashWordsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 69
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 85
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;Z)Z

    .line 86
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 87
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 89
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;-><init>()V

    .line 90
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;->setUnixTime(J)V

    .line 92
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->val$configId:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->updateConfig(II)V

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->addResult(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;ILcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository$OnSingleTransactionCallback;)V

    .line 101
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;Z)Z

    .line 73
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 74
    return-void
.end method

.method public onTick(J)V
    .locals 1
    .param p1, "playedTime"    # J

    .prologue
    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateTimeView(J)V

    .line 81
    :cond_0
    return-void
.end method
