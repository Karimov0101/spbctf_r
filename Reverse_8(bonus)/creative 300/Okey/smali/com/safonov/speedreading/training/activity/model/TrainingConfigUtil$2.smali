.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetPassCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 232
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 9
    .param p1, "id"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 235
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    sget-object v5, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {v0, v5, p1, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V

    .line 237
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v6, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v5, v6, v8, v8, v7}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v2

    .line 238
    .local v2, "fragmentTypeListWithDescription":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v6, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v5, v6, v7, v8, v7}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 240
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    new-instance v4, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v4, v0, v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 241
    .local v4, "trainingWrapperWithDescription":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    new-instance v3, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v3, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 243
    .local v3, "trainingWrapper":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/4 v6, 0x5

    aput-object v4, v5, v6

    .line 244
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/16 v6, 0xf

    aput-object v3, v5, v6

    .line 246
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v6, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v5, v6}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 247
    iget-object v5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v6, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$2;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 249
    :cond_0
    return-void
.end method
