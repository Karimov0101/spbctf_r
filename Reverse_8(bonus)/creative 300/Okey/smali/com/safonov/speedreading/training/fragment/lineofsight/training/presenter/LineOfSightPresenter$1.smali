.class Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;
.super Ljava/lang/Object;
.source "LineOfSightPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    .prologue
    .line 87
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 90
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 95
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowCount()I

    move-result v3

    if-ge v2, v3, :cond_4

    .line 96
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->updateProgressBar(I)V

    .line 98
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getMistakeProbability()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;->nextIsMistake(I)Z

    move-result v0

    .line 99
    .local v0, "nextIsMistake":Z
    if-eqz v0, :cond_2

    .line 100
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$408(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    .line 102
    :cond_2
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;

    move-result-object v4

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v5

    if-nez v0, :cond_3

    const/4 v3, 0x1

    :goto_1
    invoke-interface {v4, v5, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/model/ILineOfSightModel;->getCheckedItems(IZ)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;->setCheckedItemsData(Ljava/util/List;)V

    .line 104
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$108(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    .line 105
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getShowDelay()J

    move-result-wide v4

    invoke-virtual {v2, p0, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 102
    :cond_3
    const/4 v3, 0x0

    goto :goto_1

    .line 108
    .end local v0    # "nextIsMistake":Z
    :cond_4
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;-><init>()V

    .line 109
    .local v1, "result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->setMistakeCount(I)V

    .line 110
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->setFoundMistakeCount(I)V

    .line 111
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->setUnixTime(J)V

    .line 113
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter;)I

    move-result v3

    new-instance v4, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1$1;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/presenter/LineOfSightPresenter$1;)V

    invoke-interface {v2, v1, v3, v4}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->addResult(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ILcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnSingleTransactionCallback;)V

    goto/16 :goto_0
.end method
