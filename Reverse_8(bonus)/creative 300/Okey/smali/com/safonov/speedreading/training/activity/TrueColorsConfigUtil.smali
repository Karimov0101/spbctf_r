.class public Lcom/safonov/speedreading/training/activity/TrueColorsConfigUtil;
.super Ljava/lang/Object;
.source "TrueColorsConfigUtil.java"


# static fields
.field private static final DEFAULT_TRAINING_SHOW_TIME:I = 0x2710


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    .locals 2

    .prologue
    .line 14
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;-><init>()V

    .line 16
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;
    const/16 v1, 0x2710

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->setShowTime(I)V

    .line 18
    return-object v0
.end method
