.class public Lcom/safonov/speedreading/training/activity/view/TrainingActivity_ViewBinding;
.super Ljava/lang/Object;
.source "TrainingActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Landroid/view/View;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity_ViewBinding;->target:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .line 26
    const v0, 0x7f09020d

    const-string v1, "field \'toolbar\'"

    const-class v2, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    iput-object v0, p1, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 27
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity_ViewBinding;->target:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .line 33
    .local v0, "target":Lcom/safonov/speedreading/training/activity/view/TrainingActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 34
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity_ViewBinding;->target:Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .line 36
    iput-object v1, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 37
    return-void
.end method
