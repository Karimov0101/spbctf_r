.class public Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "TrueColorsResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    .line 24
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "resultId"    # I

    .prologue
    .line 28
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    invoke-interface {v6, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v3

    .line 29
    .local v3, "result":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v0

    .line 31
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;->getResultList(I)Ljava/util/List;

    move-result-object v4

    .line 33
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;>;"
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getScore()I

    move-result v5

    .line 34
    .local v5, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getScore()I

    move-result v1

    .line 36
    .local v1, "bestScore":I
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getId()I

    move-result v6

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    const/4 v2, 0x1

    .line 38
    .local v2, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->isViewAttached()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;

    invoke-interface {v6, v5}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;->updateScoreView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;->updateBestScoreView(I)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;

    invoke-interface {v6, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;->setNewBestScoreViewVisibility(Z)V

    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;->setChartViewData(Ljava/util/List;)V

    .line 44
    :cond_0
    return-void

    .line 36
    .end local v2    # "isNewBest":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
