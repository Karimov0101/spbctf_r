.class Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;
.super Ljava/lang/Object;
.source "MovementView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .prologue
    .line 477
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 480
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1102(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)Z

    .line 482
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$600(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 483
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$800(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;

    move-result-object v1

    invoke-interface {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;->hidePointsTextView()V

    .line 484
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1208(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I

    .line 485
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1300(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;Z)V

    .line 486
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1200(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)I

    move-result v1

    const/16 v2, 0x14

    if-lt v1, v2, :cond_0

    .line 487
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;-><init>()V

    .line 488
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1400(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->getScore()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->setScore(I)V

    .line 490
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1600(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->access$1500(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getId()I

    move-result v2

    new-instance v3, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4$1;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;->addResult(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ILcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V

    .line 501
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :goto_0
    return-void

    .line 500
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startLevel()V

    goto :goto_0
.end method
