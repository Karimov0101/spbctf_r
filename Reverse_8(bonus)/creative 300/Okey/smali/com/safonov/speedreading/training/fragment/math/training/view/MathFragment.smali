.class public Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "MathFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;",
        "Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field public static final COUNT_DOWN_INTERVAL:I = 0xa

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field buttons:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f0900f8,
            0x7f0900f9,
            0x7f0900fa,
            0x7f0900fb
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field private configId:I

.field correctAnswerTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f3
    .end annotation
.end field

.field private expressionId:I

.field expressionTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900fd
    .end annotation
.end field

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field mathPointsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f4
    .end annotation
.end field

.field private mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900fe
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900ff
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090100
    .end annotation
.end field

.field timerBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090102
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 74
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;-><init>()V

    .line 75
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 76
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 77
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->setArguments(Landroid/os/Bundle;)V

    .line 78
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;
    .locals 3

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 66
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getMathRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    .line 67
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V

    return-object v1
.end method

.method public initCountDownTimer(J)V
    .locals 9
    .param p1, "seconds"    # J

    .prologue
    .line 190
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerBar:Landroid/widget/ProgressBar;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 191
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 192
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;JJJ)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 203
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 204
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "maxProgress"    # I

    .prologue
    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 164
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 134
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 135
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 136
    check-cast p1, Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

    .line 141
    return-void

    .line 138
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 84
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 85
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->configId:I

    .line 87
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 94
    const v1, 0x7f0b004c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 96
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->unbinder:Lbutterknife/Unbinder;

    .line 98
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->close()V

    .line 115
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    .line 116
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 119
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 125
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->unbinder:Lbutterknife/Unbinder;

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 128
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 145
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

    .line 147
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 149
    :cond_0
    return-void
.end method

.method public onMathTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;->onMathTrainingCompleted(I)V

    .line 154
    return-void
.end method

.method public onNumberButtonClick(Landroid/widget/Button;)V
    .locals 2
    .param p1, "button"    # Landroid/widget/Button;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900f8,
            0x7f0900f9,
            0x7f0900fa,
            0x7f0900fb
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    invoke-virtual {p1}, Landroid/widget/Button;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;->onNumberButtonPressed(I)V

    .line 109
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 274
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 275
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;->startTraining(I)V

    .line 276
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;->pauseGame()V

    .line 282
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->setButtonsEnabled(Z)V

    .line 283
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerPause()V

    .line 284
    return-void
.end method

.method public refreshExpressionTextView()V
    .locals 4

    .prologue
    .line 233
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x1060009

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 234
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 235
    const/4 v0, 0x0

    .line 236
    .local v0, "anim":Landroid/view/animation/Animation;
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/IMathPresenter;->shuffleArray()V

    .line 289
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->setButtonsEnabled(Z)V

    .line 290
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->timerStart()V

    .line 291
    return-void
.end method

.method public setButtonsEnabled(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 266
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->buttons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 267
    .local v0, "button":Landroid/widget/Button;
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 269
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    return-void
.end method

.method public setButtonsText([I)V
    .locals 3
    .param p1, "numbers"    # [I

    .prologue
    .line 178
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->buttons:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method public setCorrectAnswer()V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06001a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 229
    return-void
.end method

.method public setCorrectAnswerGone()V
    .locals 2

    .prologue
    .line 240
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->correctAnswerTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 241
    return-void
.end method

.method public setExpressionText(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 185
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 186
    return-void
.end method

.method public setIncorrectAnswer(I)V
    .locals 5
    .param p1, "correctAnswer"    # I

    .prologue
    const/4 v4, 0x0

    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->expressionTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f06006d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 220
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->correctAnswerTextView:Landroid/widget/TextView;

    const v1, 0x7f0e00c6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 221
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->correctAnswerTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    return-void
.end method

.method public setPointsTextViewCorrect(F)V
    .locals 7
    .param p1, "points"    # F

    .prologue
    const/4 v6, 0x0

    .line 245
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 246
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v1, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 247
    .local v1, "currentLocale":Ljava/util/Locale;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    const-string v3, "+%.2g"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v1, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 248
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 249
    const/4 v0, 0x0

    .line 250
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f010011

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 251
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 252
    return-void
.end method

.method public setPointsTextViewIncorrect()V
    .locals 4

    .prologue
    .line 256
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 257
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    const-string v2, "-1"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f06006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 259
    const/4 v0, 0x0

    .line 260
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010011

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 261
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mathPointsTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 262
    return-void
.end method

.method public timerPause()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 209
    return-void
.end method

.method public timerStart()V
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 214
    return-void
.end method

.method public updateBestScoreView(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e014b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 169
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    return-void
.end method
