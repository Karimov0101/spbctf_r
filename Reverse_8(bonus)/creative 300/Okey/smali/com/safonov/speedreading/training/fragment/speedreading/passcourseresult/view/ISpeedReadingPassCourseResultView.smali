.class public interface abstract Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;
.super Ljava/lang/Object;
.source "ISpeedReadingPassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setNewBestAverageSpeedViewVisibility(Z)V
.end method

.method public abstract setNewBestMaxSpeedViewVisibility(Z)V
.end method

.method public abstract updateAverageSpeedView(I)V
.end method

.method public abstract updateBestAverageSpeedView(I)V
.end method

.method public abstract updateBestMaxSpeedView(I)V
.end method

.method public abstract updateMaxSpeedView(I)V
.end method

.method public abstract updatePassCoursePointsView(I)V
.end method
