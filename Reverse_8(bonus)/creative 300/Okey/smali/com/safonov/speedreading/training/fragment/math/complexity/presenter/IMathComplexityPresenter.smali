.class public interface abstract Lcom/safonov/speedreading/training/fragment/math/complexity/presenter/IMathComplexityPresenter;
.super Ljava/lang/Object;
.source "IMathComplexityPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/complexity/view/IMathComplexityFragment;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract initConfig(I)V
.end method

.method public abstract setComplexity(I)V
.end method
