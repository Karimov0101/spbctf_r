.class public Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "MathResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;",
        "Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/math/result/view/IMathResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900eb
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f5
    .end annotation
.end field

.field private mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

.field newBestScoreView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011d
    .end annotation
.end field

.field private resultId:I

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900f6
    .end annotation
.end field

.field scoreTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0150
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 82
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;-><init>()V

    .line 83
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 84
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 86
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 52
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getMathRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    .line 54
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/MathResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->resultId:I

    .line 66
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    const v1, 0x7f0b004d

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 76
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 78
    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 91
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/result/presenter/IMathResultPresenter;->initTrainingResults(I)V

    .line 93
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "wordPairsResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;>;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 131
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;->getScore()I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 134
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->scoreTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 135
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 136
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 137
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 138
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 139
    new-instance v8, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 145
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 146
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 147
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 148
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 150
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 151
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 155
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 156
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 157
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 159
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 160
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v5, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 161
    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 163
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 164
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v6, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 165
    invoke-virtual {v6, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 167
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 168
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 169
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 170
    return-void
.end method

.method public setNewBestScoreViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 115
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 116
    return-void

    .line 115
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 111
    return-void
.end method
