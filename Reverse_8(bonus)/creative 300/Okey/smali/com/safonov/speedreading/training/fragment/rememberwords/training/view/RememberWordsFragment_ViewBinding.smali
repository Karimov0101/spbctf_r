.class public Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "RememberWordsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .line 25
    const v0, 0x7f090099

    const-string v1, "field \'score\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->score:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f090097

    const-string v1, "field \'progressBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 27
    const v0, 0x7f09009a

    const-string v1, "field \'statisticsLayout\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->statisticsLayout:Landroid/widget/LinearLayout;

    .line 28
    const v0, 0x7f090248

    const-string v1, "field \'wordsLayout\'"

    const-class v2, Landroid/widget/TableLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    .line 29
    const v0, 0x7f0901dd

    const-string v1, "field \'tableLayout\'"

    const-class v2, Landroid/widget/TableLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TableLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->tableLayout:Landroid/widget/TableLayout;

    .line 30
    const v0, 0x7f09002a

    const-string v1, "field \'answerLayout\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerLayout:Landroid/widget/LinearLayout;

    .line 31
    const v0, 0x7f090029

    const-string v1, "field \'answersContainer\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answersContainer:Landroid/widget/LinearLayout;

    .line 32
    const v0, 0x7f0900ae

    const-string v1, "field \'scrollView\'"

    const-class v2, Landroid/widget/HorizontalScrollView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scrollView:Landroid/widget/HorizontalScrollView;

    .line 33
    const v0, 0x7f090098

    const-string v1, "field \'recordTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->recordTextView:Landroid/widget/TextView;

    .line 34
    const v0, 0x7f090139

    const-string v1, "field \'pointsTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    .line 35
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .line 41
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 42
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .line 44
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->score:Landroid/widget/TextView;

    .line 45
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 46
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->statisticsLayout:Landroid/widget/LinearLayout;

    .line 47
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    .line 48
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->tableLayout:Landroid/widget/TableLayout;

    .line 49
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerLayout:Landroid/widget/LinearLayout;

    .line 50
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answersContainer:Landroid/widget/LinearLayout;

    .line 51
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->scrollView:Landroid/widget/HorizontalScrollView;

    .line 52
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->recordTextView:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->pointsTextView:Landroid/widget/TextView;

    .line 54
    return-void
.end method
