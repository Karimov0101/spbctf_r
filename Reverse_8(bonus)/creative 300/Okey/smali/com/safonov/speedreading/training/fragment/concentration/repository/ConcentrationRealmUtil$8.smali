.class Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;
.super Ljava/lang/Object;
.source "ConcentrationRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnMultiTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

.field final synthetic val$haveToSaveConfigList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    .prologue
    .line 223
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$8;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lio/realm/Realm;->copyToRealm(Ljava/lang/Iterable;)Ljava/util/List;

    .line 227
    return-void
.end method
