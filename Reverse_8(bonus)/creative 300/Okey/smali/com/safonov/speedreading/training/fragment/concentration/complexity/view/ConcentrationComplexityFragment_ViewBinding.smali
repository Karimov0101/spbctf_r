.class public Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;
.super Ljava/lang/Object;
.source "ConcentrationComplexityFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

.field private view2131296345:Landroid/view/View;

.field private view2131296346:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

    .line 28
    const v1, 0x7f090059

    const-string v2, "method \'setComplexityFirst\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 29
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296345:Landroid/view/View;

    .line 30
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 36
    const v1, 0x7f09005a

    const-string v2, "method \'setComplexitySecond\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 37
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296346:Landroid/view/View;

    .line 38
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296345:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296345:Landroid/view/View;

    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296346:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 56
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment_ViewBinding;->view2131296346:Landroid/view/View;

    .line 57
    return-void
.end method
