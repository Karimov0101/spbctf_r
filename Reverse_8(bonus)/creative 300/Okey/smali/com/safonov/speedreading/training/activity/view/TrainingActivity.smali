.class public Lcom/safonov/speedreading/training/activity/view/TrainingActivity;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;
.source "TrainingActivity.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/view/ITrainingView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpActivity",
        "<",
        "Lcom/safonov/speedreading/training/activity/view/ITrainingView;",
        "Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/activity/view/ITrainingView;"
    }
.end annotation


# static fields
.field public static final COURSE_REQUEST_CODE:I = 0x7ce

.field public static final TRAINING_TYPE_PARAM:Ljava/lang/String; = "training_type"


# instance fields
.field private concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

.field private cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

.field private currentFragment:Landroid/support/v4/app/Fragment;

.field private evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field private flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

.field private fragmentManager:Landroid/support/v4/app/FragmentManager;

.field private greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field private interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

.field private lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

.field private mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

.field private menu:Landroid/view/Menu;

.field private pauseDialog:Landroid/support/v7/app/AlertDialog;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private readingCompleted:Z

.field private rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

.field private rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

.field private schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

.field private speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private trainingType:Lcom/safonov/speedreading/training/TrainingType;

.field private trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;

.field private wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

.field private wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

.field private wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;-><init>()V

    .line 135
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)Lcom/google/android/gms/ads/InterstitialAd;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/view/TrainingActivity;
    .param p1, "x1"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragmentStateLoss(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private setCurrentFragment(Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 505
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 506
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 507
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 508
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 509
    return-void
.end method

.method private setCurrentFragmentStateLoss(Landroid/support/v4/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 512
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 513
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 514
    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 515
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 516
    return-void
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 115
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->createPresenter()Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;
    .locals 20
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 140
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v19

    .line 142
    .local v19, "app":Lcom/safonov/speedreading/application/App;
    new-instance v2, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;-><init>(Landroid/content/Context;)V

    .line 143
    .local v2, "trainingSettingsUtil":Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;
    new-instance v3, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;-><init>(Landroid/content/Context;)V

    .line 145
    .local v3, "trainingDescriptionUtil":Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;
    new-instance v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getWordsColumnsRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    .line 146
    new-instance v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getWordBlockRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    .line 148
    new-instance v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getFlashWordsRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    .line 150
    new-instance v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getSchulteTableRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 151
    new-instance v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getRememberNumberRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    .line 152
    new-instance v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getLineOfSightRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 153
    new-instance v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getEvenNumbersRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 154
    new-instance v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getWordPairsRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .line 155
    new-instance v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getSpeedReadingRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 156
    new-instance v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getGreenDotRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 157
    new-instance v4, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getMathRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    .line 158
    new-instance v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getConcentrationRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    .line 159
    new-instance v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getCupTimerRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .line 160
    new-instance v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getRememberWordsRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    .line 161
    new-instance v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual/range {v19 .. v19}, Lcom/safonov/speedreading/application/App;->getTrueColorsRealm()Lio/realm/Realm;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;-><init>(Lio/realm/Realm;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .line 163
    new-instance v1, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    move-object/from16 v18, v0

    invoke-direct/range {v1 .. v18}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;-><init>(Lcom/safonov/speedreading/training/util/preference/TrainingSettingsUtil;Lcom/safonov/speedreading/training/util/preference/TrainingDescriptionUtil;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V

    .line 184
    .local v1, "trainingConfigUtil":Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;
    new-instance v4, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    new-instance v5, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    move-object/from16 v0, p0

    invoke-direct {v5, v0}, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;-><init>(Landroid/content/Context;)V

    new-instance v6, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;-><init>(Landroid/content/Context;)V

    invoke-direct {v4, v1, v5, v6}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;-><init>(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;)V

    return-object v4
.end method

.method public dismissPauseDialog()V
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog;->dismiss()V

    .line 476
    return-void
.end method

.method public hideActionBar()V
    .locals 3

    .prologue
    const/16 v2, 0x400

    .line 366
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 367
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/view/Window;->setFlags(II)V

    .line 373
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->hide()V

    .line 374
    return-void

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 316
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 317
    packed-switch p1, :pswitch_data_0

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 319
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 320
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->readingCompleted:Z

    goto :goto_0

    .line 317
    :pswitch_data_0
    .packed-switch 0x7ce
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onBackPressed()V

    .line 250
    return-void
.end method

.method public onConcentrationComplexityStartClick()V
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextFragment()V

    .line 834
    return-void
.end method

.method public onConcentrationTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 838
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->CONCENTRATION:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 839
    return-void
.end method

.method public onCourseResultNextClick()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextCourseTraining()V

    .line 501
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreate(Landroid/os/Bundle;)V

    .line 203
    const v0, 0x7f0b00af

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setContentView(I)V

    .line 205
    new-instance v0, Lcom/google/android/gms/ads/InterstitialAd;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    .line 206
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    const v1, 0x7f0e0091

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdUnitId(Ljava/lang/String;)V

    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 211
    :cond_0
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->unbinder:Lbutterknife/Unbinder;

    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 215
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 217
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->fragmentManager:Landroid/support/v4/app/FragmentManager;

    .line 219
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/TrainingType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    .line 220
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 274
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 275
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0c0003

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 276
    const/4 v0, 0x1

    return v0
.end method

.method public onCupTimerTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 818
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 819
    return-void
.end method

.method public onDescriptionFragmentStartClick()V
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextFragment()V

    .line 486
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 918
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onDestroy()V

    .line 919
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 921
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordsColumnsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;->close()V

    .line 922
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordBlockRealmUtil:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/WordBlockRealmUtil;->close()V

    .line 923
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->flashWordRealmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;->close()V

    .line 925
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;->close()V

    .line 926
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberNumberRealmUtil:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/RememberNumberRealmUtil;->close()V

    .line 927
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->close()V

    .line 928
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->close()V

    .line 929
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->close()V

    .line 930
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->close()V

    .line 931
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->close()V

    .line 932
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->close()V

    .line 933
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->close()V

    .line 934
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->close()V

    .line 935
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->rememberWordsRealmUtil:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->close()V

    .line 936
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->close()V

    .line 937
    return-void
.end method

.method public onEvenNumbersTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 739
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 740
    return-void
.end method

.method public onFlashWordsTrainingCompleted(I)V
    .locals 2
    .param p1, "resultId"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 601
    return-void
.end method

.method public onGreenDotTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 765
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->GREEN_DOT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 766
    return-void
.end method

.method public onInterstitialFragmentListenerCompleted()V
    .locals 1

    .prologue
    .line 490
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextFragment()V

    .line 491
    return-void
.end method

.method public onLineOfSightTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 697
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 698
    return-void
.end method

.method public onMathComplexityStartClick()V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextFragment()V

    .line 942
    return-void
.end method

.method public onMathTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 791
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->MATHEMATICS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 792
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 254
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 268
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 256
    :sswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onActionBarRestartPressed()V

    move v0, v1

    .line 257
    goto :goto_0

    .line 259
    :sswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onActionBarHelpPressed()V

    move v0, v1

    .line 260
    goto :goto_0

    .line 262
    :sswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onActionBarSettingsPressed()V

    move v0, v1

    .line 263
    goto :goto_0

    .line 265
    :sswitch_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onActionBarHomePressed()V

    move v0, v1

    .line 266
    goto :goto_0

    .line 254
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_3
        0x7f0900a9 -> :sswitch_1
        0x7f090174 -> :sswitch_0
        0x7f09019d -> :sswitch_2
    .end sparse-switch
.end method

.method public onPassCourseRestarted()V
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->restartCourse()V

    .line 496
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onPause()V

    .line 238
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onPause()V

    .line 239
    return-void
.end method

.method protected onPostResume()V
    .locals 1

    .prologue
    .line 328
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onPostResume()V

    .line 329
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->readingCompleted:Z

    if-eqz v0, :cond_0

    .line 330
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->readingCompleted:Z

    .line 331
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onReadingCompleted()V

    .line 333
    :cond_0
    return-void
.end method

.method public onPrepareFragmentCompleted()V
    .locals 1

    .prologue
    .line 480
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToSetNextFragment()V

    .line 481
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 281
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    .line 282
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 283
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onRememberNumberTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 648
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 649
    return-void
.end method

.method public onRememberWordsTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 845
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 846
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 243
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onResume()V

    .line 244
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onResume()V

    .line 245
    return-void
.end method

.method public onSchulteTableTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 626
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 627
    return-void
.end method

.method public onSpeedReadingTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 670
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->SPEED_READING:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 671
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onStop()V

    .line 232
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onStop()V

    .line 233
    return-void
.end method

.method public onTrueColorsTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 870
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->TRUE_COLORS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 871
    return-void
.end method

.method public onWordBlockTrainingCompleted(I)V
    .locals 2
    .param p1, "resultId"    # I

    .prologue
    .line 574
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 575
    return-void
.end method

.method public onWordColumnsTrainingCompleted(I)V
    .locals 2
    .param p1, "resultId"    # I

    .prologue
    .line 547
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 548
    return-void
.end method

.method public onWordPairsTrainingCompleted(I)V
    .locals 2
    .param p1, "trainingResultId"    # I

    .prologue
    .line 718
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORD_PAIRS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 719
    return-void
.end method

.method public pauseFragmentAnimations()V
    .locals 2

    .prologue
    .line 388
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v1, v1, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;

    if-eqz v1, :cond_0

    .line 389
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;

    .line 391
    .local v0, "animatedFragment":Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;
    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;->pauseAnimations()V

    .line 393
    .end local v0    # "animatedFragment":Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;
    :cond_0
    return-void
.end method

.method public restartCourse()V
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;->restartCourse()V

    .line 225
    return-void
.end method

.method public resumeFragmentAnimations()V
    .locals 2

    .prologue
    .line 397
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    instance-of v1, v1, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;

    if-eqz v1, :cond_0

    .line 398
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;

    .line 400
    .local v0, "animatedFragment":Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;
    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;->resumeAnimations()V

    .line 402
    .end local v0    # "animatedFragment":Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;
    :cond_0
    return-void
.end method

.method public setAcceleratorCourseResultFragment([I)V
    .locals 1
    .param p1, "resultIds"    # [I

    .prologue
    .line 542
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->newInstance([I)Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 543
    return-void
.end method

.method public setArrowActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 350
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f090174

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 351
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f0900a9

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 352
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f09019d

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 353
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 354
    return-void
.end method

.method public setArrowTrainingToolbar(Z)V
    .locals 3
    .param p1, "isSettingsSupported"    # Z

    .prologue
    const/4 v2, 0x1

    .line 358
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f090174

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 359
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f0900a9

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 360
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f09019d

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 361
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 362
    return-void
.end method

.method public setCloseActionBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 342
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f090174

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 343
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f0900a9

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 344
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->menu:Landroid/view/Menu;

    const v1, 0x7f09019d

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 345
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080067

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 346
    return-void
.end method

.method public setConcentrationComplexityFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 808
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 809
    return-void
.end method

.method public setConcentrationFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 798
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 799
    return-void
.end method

.method public setConcentrationResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 803
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/concentration/result/view/ConcentrationResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/concentration/result/view/ConcentrationResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 804
    return-void
.end method

.method public setCupTimerFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 813
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 814
    return-void
.end method

.method public setCupTimerResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 823
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 824
    return-void
.end method

.method public setCupTimerSettingsFragment()V
    .locals 1

    .prologue
    .line 828
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/cuptimer/settings/CupTimerSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/cuptimer/settings/CupTimerSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 829
    return-void
.end method

.method public setDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 1
    .param p1, "descriptionFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 913
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->newInstance(Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 914
    return-void
.end method

.method public setEvenNumbersFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 724
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 725
    return-void
.end method

.method public setEvenNumbersPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 734
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/passcourseresult/view/EvenNumbersPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 735
    return-void
.end method

.method public setEvenNumbersResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 729
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 730
    return-void
.end method

.method public setFlashWordsFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 580
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 581
    return-void
.end method

.method public setFlashWordsPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 590
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 591
    return-void
.end method

.method public setFlashWordsResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 585
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 586
    return-void
.end method

.method public setFlashWordsSettingsFragment()V
    .locals 1

    .prologue
    .line 595
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/flashword/settings/FlashWordsSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/flashword/settings/FlashWordsSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 596
    return-void
.end method

.method public setGreenDotFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 745
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 746
    return-void
.end method

.method public setGreenDotPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 755
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/GreenDotPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 756
    return-void
.end method

.method public setGreenDotResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 750
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 751
    return-void
.end method

.method public setGreenDotSettingsFragment()V
    .locals 1

    .prologue
    .line 760
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/greendot/settings/GreenDotSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/greendot/settings/GreenDotSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 761
    return-void
.end method

.method public setHelpFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 1
    .param p1, "helpFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 908
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;->newInstance(Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/fragment/help/view/HelpFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 909
    return-void
.end method

.method public setInterstitialFragment(Lcom/safonov/speedreading/training/TrainingType;I)V
    .locals 2
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "trainingIndex"    # I

    .prologue
    .line 887
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 888
    invoke-static {p1, p2}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->newInstance(Lcom/safonov/speedreading/training/TrainingType;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 904
    :goto_0
    return-void

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 893
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->show()V

    .line 894
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$8;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;Lcom/safonov/speedreading/training/TrainingType;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    goto :goto_0

    .line 902
    :cond_1
    invoke-static {p1, p2}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->newInstance(Lcom/safonov/speedreading/training/TrainingType;I)Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method

.method public setLineOfSightFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 677
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 678
    return-void
.end method

.method public setLineOfSightPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 687
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/LineOfSightPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 688
    return-void
.end method

.method public setLineOfSightResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 682
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 683
    return-void
.end method

.method public setLineOfSightSettingsFragment()V
    .locals 1

    .prologue
    .line 692
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/lineofsight/settings/LineOfSightSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/lineofsight/settings/LineOfSightSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 693
    return-void
.end method

.method public setMathComplexityFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 781
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 782
    return-void
.end method

.method public setMathFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 771
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/math/training/view/MathFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 772
    return-void
.end method

.method public setMathResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 776
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/math/result/view/MathResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 777
    return-void
.end method

.method public setMathSettingsFragment()V
    .locals 1

    .prologue
    .line 786
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/math/settings/MathSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/math/settings/MathSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 787
    return-void
.end method

.method public setPassCourseResultFragment([I)V
    .locals 1
    .param p1, "resultIds"    # [I

    .prologue
    .line 877
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;->newInstance([I)Lcom/safonov/speedreading/training/fragment/passcource/result/view/PassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 878
    return-void
.end method

.method public setPrepareFragment()V
    .locals 1

    .prologue
    .line 882
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 883
    return-void
.end method

.method public setRememberNumberFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 633
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/RememberNumberFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 634
    return-void
.end method

.method public setRememberNumberPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 643
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/RememberNumberPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/RememberNumberPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 644
    return-void
.end method

.method public setRememberNumberResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 638
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/result/view/RememberNumberResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/remembernumber/result/view/RememberNumberResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 639
    return-void
.end method

.method public setRememberWordsFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 850
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 851
    return-void
.end method

.method public setRememberWordsResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 855
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/result/view/RememberWordsResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/rememberwords/result/view/RememberWordsResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 856
    return-void
.end method

.method public setSchulteTableFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 606
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 607
    return-void
.end method

.method public setSchulteTablePassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 616
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/SchulteTablePassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 617
    return-void
.end method

.method public setSchulteTableResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 611
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 612
    return-void
.end method

.method public setSchulteTableSettingsFragment()V
    .locals 1

    .prologue
    .line 621
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/schultetable/settings/SchulteTableSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/schultetable/settings/SchulteTableSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 622
    return-void
.end method

.method public setSpeedReadingFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 655
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 656
    return-void
.end method

.method public setSpeedReadingPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 665
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/SpeedReadingPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 666
    return-void
.end method

.method public setSpeedReadingResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 660
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/result/view/SpeedReadingResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 661
    return-void
.end method

.method public setToolbarTitle(I)V
    .locals 1
    .param p1, "titleRes"    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 338
    return-void
.end method

.method public setTrueColorsFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 860
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 861
    return-void
.end method

.method public setTrueColorsResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 865
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 866
    return-void
.end method

.method public setWordPairsFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 703
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 704
    return-void
.end method

.method public setWordPairsPassCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 713
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/WordPairsPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 714
    return-void
.end method

.method public setWordPairsResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 708
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/WordPairsResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/WordPairsResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 709
    return-void
.end method

.method public setWordsBlockCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 564
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/view/WordBlockPassCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/view/WordBlockPassCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 565
    return-void
.end method

.method public setWordsBlockFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 554
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 555
    return-void
.end method

.method public setWordsBlockResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 559
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/result/view/WordBlockResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordsblock/result/view/WordBlockResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 560
    return-void
.end method

.method public setWordsBlockSettingsFragment()V
    .locals 0

    .prologue
    .line 570
    return-void
.end method

.method public setWordsColumnsCourseResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 532
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/passcourseresult/view/WordColumnsCourseResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/passcourseresult/view/WordColumnsCourseResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 533
    return-void
.end method

.method public setWordsColumnsFragment(I)V
    .locals 1
    .param p1, "configId"    # I

    .prologue
    .line 522
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 523
    return-void
.end method

.method public setWordsColumnsResultFragment(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 527
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/result/view/WordColumnsResultFragment;->newInstance(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/result/view/WordColumnsResultFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 528
    return-void
.end method

.method public setWordsColumnsSettingsFragment()V
    .locals 1

    .prologue
    .line 537
    invoke-static {}, Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;->newInstance()Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->setCurrentFragment(Landroid/support/v4/app/Fragment;)V

    .line 538
    return-void
.end method

.method public showActionBar()V
    .locals 2

    .prologue
    .line 378
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x400

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 383
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/ActionBar;->show()V

    .line 384
    return-void

    .line 381
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public showPauseDialog(ZZZ)V
    .locals 12
    .param p1, "showRestart"    # Z
    .param p2, "showHelp"    # Z
    .param p3, "showSettings"    # Z

    .prologue
    const/4 v8, 0x0

    const/16 v9, 0x8

    .line 408
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 410
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v7

    const v10, 0x7f0b00b1

    const/4 v11, 0x0

    invoke-virtual {v7, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 411
    .local v6, "view":Landroid/view/View;
    invoke-virtual {v0, v6}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 413
    const v7, 0x7f090224

    invoke-static {v6, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 414
    .local v1, "continueView":Landroid/view/View;
    new-instance v7, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$1;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$1;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 421
    const v7, 0x7f090227

    invoke-static {v6, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    .line 422
    .local v4, "restartView":Landroid/view/View;
    if-eqz p1, :cond_0

    move v7, v8

    :goto_0
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 423
    new-instance v7, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$2;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$2;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v4, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    const v7, 0x7f090228

    invoke-static {v6, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v5

    .line 431
    .local v5, "settingsView":Landroid/view/View;
    if-eqz p3, :cond_1

    move v7, v8

    :goto_1
    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    .line 432
    new-instance v7, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$3;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$3;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v5, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 439
    const v7, 0x7f090226

    invoke-static {v6, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    .line 440
    .local v3, "helpView":Landroid/view/View;
    if-eqz p2, :cond_2

    :goto_2
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 441
    new-instance v7, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$4;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$4;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 448
    const v7, 0x7f090225

    invoke-static {v6, v7}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 449
    .local v2, "exitView":Landroid/view/View;
    new-instance v7, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$5;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$5;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v2, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 456
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    .line 457
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v7}, Landroid/support/v7/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v7

    invoke-virtual {v7, v9}, Landroid/view/Window;->addFlags(I)V

    .line 458
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    new-instance v8, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$6;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$6;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v7, v8}, Landroid/support/v7/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 464
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    new-instance v8, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$7;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$7;-><init>(Lcom/safonov/speedreading/training/activity/view/TrainingActivity;)V

    invoke-virtual {v7, v8}, Landroid/support/v7/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 470
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->pauseDialog:Landroid/support/v7/app/AlertDialog;

    invoke-virtual {v7}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 471
    return-void

    .end local v2    # "exitView":Landroid/view/View;
    .end local v3    # "helpView":Landroid/view/View;
    .end local v5    # "settingsView":Landroid/view/View;
    :cond_0
    move v7, v9

    .line 422
    goto :goto_0

    .restart local v5    # "settingsView":Landroid/view/View;
    :cond_1
    move v7, v9

    .line 431
    goto :goto_1

    .restart local v3    # "helpView":Landroid/view/View;
    :cond_2
    move v8, v9

    .line 440
    goto :goto_2
.end method

.method public startCourseReaderActivity(Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 5
    .param p1, "courseType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/16 v4, 0x7ce

    .line 290
    sget-object v1, Lcom/safonov/speedreading/training/activity/view/TrainingActivity$9;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 307
    :goto_0
    return-void

    .line 292
    :pswitch_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "timer_mode"

    const/4 v3, 0x1

    .line 293
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 295
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0, v4}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 300
    .end local v0    # "intent":Landroid/content/Intent;
    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/safonov/speedreading/reader/library/activity/view/LibraryActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "timer_mode"

    const/4 v3, 0x2

    .line 301
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 303
    .restart local v0    # "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0, v4}, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
