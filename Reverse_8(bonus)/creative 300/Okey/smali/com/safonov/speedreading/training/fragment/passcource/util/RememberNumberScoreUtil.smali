.class public Lcom/safonov/speedreading/training/fragment/passcource/util/RememberNumberScoreUtil;
.super Ljava/lang/Object;
.source "RememberNumberScoreUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPassCourseScore(I)I
    .locals 6
    .param p0, "score"    # I

    .prologue
    const/16 v0, 0x2c

    const/16 v1, 0x2b

    const/16 v2, 0x2a

    const/16 v3, 0x29

    const/16 v4, 0x28

    .line 11
    const/16 v5, 0x78

    if-le p0, v5, :cond_1

    const/16 v0, 0x62

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 12
    :cond_1
    const/16 v5, 0x77

    if-le p0, v5, :cond_2

    const/16 v0, 0x60

    goto :goto_0

    .line 13
    :cond_2
    const/16 v5, 0x76

    if-le p0, v5, :cond_3

    const/16 v0, 0x5e

    goto :goto_0

    .line 14
    :cond_3
    const/16 v5, 0x75

    if-le p0, v5, :cond_4

    const/16 v0, 0x5c

    goto :goto_0

    .line 15
    :cond_4
    const/16 v5, 0x74

    if-le p0, v5, :cond_5

    const/16 v0, 0x5a

    goto :goto_0

    .line 16
    :cond_5
    const/16 v5, 0x73

    if-le p0, v5, :cond_6

    const/16 v0, 0x58

    goto :goto_0

    .line 17
    :cond_6
    const/16 v5, 0x72

    if-le p0, v5, :cond_7

    const/16 v0, 0x56

    goto :goto_0

    .line 18
    :cond_7
    const/16 v5, 0x71

    if-le p0, v5, :cond_8

    const/16 v0, 0x54

    goto :goto_0

    .line 19
    :cond_8
    const/16 v5, 0x70

    if-le p0, v5, :cond_9

    const/16 v0, 0x52

    goto :goto_0

    .line 20
    :cond_9
    const/16 v5, 0x6f

    if-le p0, v5, :cond_a

    const/16 v0, 0x50

    goto :goto_0

    .line 21
    :cond_a
    const/16 v5, 0x6e

    if-le p0, v5, :cond_b

    const/16 v0, 0x4e

    goto :goto_0

    .line 22
    :cond_b
    const/16 v5, 0x6d

    if-le p0, v5, :cond_c

    const/16 v0, 0x4c

    goto :goto_0

    .line 23
    :cond_c
    const/16 v5, 0x6c

    if-le p0, v5, :cond_d

    const/16 v0, 0x4a

    goto :goto_0

    .line 24
    :cond_d
    const/16 v5, 0x6b

    if-le p0, v5, :cond_e

    const/16 v0, 0x48

    goto :goto_0

    .line 25
    :cond_e
    const/16 v5, 0x6a

    if-le p0, v5, :cond_f

    const/16 v0, 0x46

    goto :goto_0

    .line 26
    :cond_f
    const/16 v5, 0x69

    if-le p0, v5, :cond_10

    const/16 v0, 0x44

    goto :goto_0

    .line 27
    :cond_10
    const/16 v5, 0x68

    if-le p0, v5, :cond_11

    const/16 v0, 0x42

    goto :goto_0

    .line 28
    :cond_11
    const/16 v5, 0x67

    if-le p0, v5, :cond_12

    const/16 v0, 0x40

    goto :goto_0

    .line 29
    :cond_12
    const/16 v5, 0x66

    if-le p0, v5, :cond_13

    const/16 v0, 0x3f

    goto :goto_0

    .line 30
    :cond_13
    const/16 v5, 0x65

    if-le p0, v5, :cond_14

    const/16 v0, 0x3e

    goto/16 :goto_0

    .line 31
    :cond_14
    const/16 v5, 0x64

    if-le p0, v5, :cond_15

    const/16 v0, 0x3d

    goto/16 :goto_0

    .line 32
    :cond_15
    const/16 v5, 0x63

    if-le p0, v5, :cond_16

    const/16 v0, 0x3c

    goto/16 :goto_0

    .line 33
    :cond_16
    const/16 v5, 0x62

    if-le p0, v5, :cond_17

    const/16 v0, 0x3b

    goto/16 :goto_0

    .line 34
    :cond_17
    const/16 v5, 0x61

    if-le p0, v5, :cond_18

    const/16 v0, 0x3a

    goto/16 :goto_0

    .line 35
    :cond_18
    const/16 v5, 0x60

    if-le p0, v5, :cond_19

    const/16 v0, 0x39

    goto/16 :goto_0

    .line 36
    :cond_19
    const/16 v5, 0x5f

    if-le p0, v5, :cond_1a

    const/16 v0, 0x38

    goto/16 :goto_0

    .line 37
    :cond_1a
    const/16 v5, 0x5e

    if-le p0, v5, :cond_1b

    const/16 v0, 0x37

    goto/16 :goto_0

    .line 38
    :cond_1b
    const/16 v5, 0x5d

    if-le p0, v5, :cond_1c

    const/16 v0, 0x36

    goto/16 :goto_0

    .line 39
    :cond_1c
    const/16 v5, 0x5c

    if-le p0, v5, :cond_1d

    const/16 v0, 0x35

    goto/16 :goto_0

    .line 40
    :cond_1d
    const/16 v5, 0x5b

    if-le p0, v5, :cond_1e

    const/16 v0, 0x34

    goto/16 :goto_0

    .line 41
    :cond_1e
    const/16 v5, 0x5a

    if-le p0, v5, :cond_1f

    const/16 v0, 0x33

    goto/16 :goto_0

    .line 42
    :cond_1f
    const/16 v5, 0x59

    if-le p0, v5, :cond_20

    const/16 v0, 0x32

    goto/16 :goto_0

    .line 43
    :cond_20
    const/16 v5, 0x58

    if-le p0, v5, :cond_21

    const/16 v0, 0x31

    goto/16 :goto_0

    .line 44
    :cond_21
    const/16 v5, 0x57

    if-le p0, v5, :cond_22

    const/16 v0, 0x30

    goto/16 :goto_0

    .line 45
    :cond_22
    const/16 v5, 0x56

    if-le p0, v5, :cond_23

    const/16 v0, 0x2f

    goto/16 :goto_0

    .line 46
    :cond_23
    const/16 v5, 0x55

    if-le p0, v5, :cond_24

    const/16 v0, 0x2e

    goto/16 :goto_0

    .line 47
    :cond_24
    const/16 v5, 0x54

    if-le p0, v5, :cond_25

    const/16 v0, 0x2d

    goto/16 :goto_0

    .line 48
    :cond_25
    const/16 v5, 0x53

    if-gt p0, v5, :cond_0

    .line 49
    const/16 v5, 0x52

    if-le p0, v5, :cond_26

    move v0, v1

    goto/16 :goto_0

    .line 50
    :cond_26
    const/16 v5, 0x51

    if-le p0, v5, :cond_27

    move v0, v2

    goto/16 :goto_0

    .line 51
    :cond_27
    const/16 v5, 0x50

    if-le p0, v5, :cond_28

    move v0, v3

    goto/16 :goto_0

    .line 52
    :cond_28
    const/16 v5, 0x4f

    if-le p0, v5, :cond_29

    move v0, v4

    goto/16 :goto_0

    .line 53
    :cond_29
    const/16 v5, 0x4e

    if-le p0, v5, :cond_2a

    const/16 v0, 0x27

    goto/16 :goto_0

    .line 54
    :cond_2a
    const/16 v5, 0x4d

    if-le p0, v5, :cond_2b

    const/16 v0, 0x26

    goto/16 :goto_0

    .line 55
    :cond_2b
    const/16 v5, 0x4c

    if-le p0, v5, :cond_2c

    const/16 v0, 0x25

    goto/16 :goto_0

    .line 56
    :cond_2c
    const/16 v5, 0x4b

    if-le p0, v5, :cond_2d

    const/16 v0, 0x24

    goto/16 :goto_0

    .line 57
    :cond_2d
    const/16 v5, 0x4a

    if-le p0, v5, :cond_2e

    const/16 v0, 0x23

    goto/16 :goto_0

    .line 58
    :cond_2e
    const/16 v5, 0x49

    if-le p0, v5, :cond_2f

    const/16 v0, 0x22

    goto/16 :goto_0

    .line 59
    :cond_2f
    const/16 v5, 0x48

    if-le p0, v5, :cond_30

    const/16 v0, 0x21

    goto/16 :goto_0

    .line 60
    :cond_30
    const/16 v5, 0x47

    if-le p0, v5, :cond_31

    const/16 v0, 0x20

    goto/16 :goto_0

    .line 61
    :cond_31
    const/16 v5, 0x46

    if-le p0, v5, :cond_32

    const/16 v0, 0x1f

    goto/16 :goto_0

    .line 62
    :cond_32
    const/16 v5, 0x45

    if-le p0, v5, :cond_33

    const/16 v0, 0x1e

    goto/16 :goto_0

    .line 63
    :cond_33
    const/16 v5, 0x44

    if-le p0, v5, :cond_34

    const/16 v0, 0x1d

    goto/16 :goto_0

    .line 64
    :cond_34
    const/16 v5, 0x43

    if-le p0, v5, :cond_35

    const/16 v0, 0x1c

    goto/16 :goto_0

    .line 65
    :cond_35
    const/16 v5, 0x42

    if-le p0, v5, :cond_36

    const/16 v0, 0x1b

    goto/16 :goto_0

    .line 66
    :cond_36
    const/16 v5, 0x41

    if-le p0, v5, :cond_37

    const/16 v0, 0x1a

    goto/16 :goto_0

    .line 67
    :cond_37
    const/16 v5, 0x40

    if-le p0, v5, :cond_38

    const/16 v0, 0x19

    goto/16 :goto_0

    .line 68
    :cond_38
    const/16 v5, 0x3f

    if-le p0, v5, :cond_39

    const/16 v0, 0x18

    goto/16 :goto_0

    .line 69
    :cond_39
    const/16 v5, 0x3e

    if-le p0, v5, :cond_3a

    const/16 v0, 0x17

    goto/16 :goto_0

    .line 70
    :cond_3a
    const/16 v5, 0x3d

    if-le p0, v5, :cond_3b

    const/16 v0, 0x16

    goto/16 :goto_0

    .line 71
    :cond_3b
    const/16 v5, 0x3c

    if-le p0, v5, :cond_3c

    const/16 v0, 0x15

    goto/16 :goto_0

    .line 72
    :cond_3c
    const/16 v5, 0x3b

    if-le p0, v5, :cond_3d

    const/16 v0, 0x14

    goto/16 :goto_0

    .line 73
    :cond_3d
    const/16 v5, 0x3a

    if-le p0, v5, :cond_3e

    const/16 v0, 0x13

    goto/16 :goto_0

    .line 74
    :cond_3e
    const/16 v5, 0x39

    if-le p0, v5, :cond_3f

    const/16 v0, 0x12

    goto/16 :goto_0

    .line 75
    :cond_3f
    const/16 v5, 0x38

    if-le p0, v5, :cond_40

    const/16 v0, 0x11

    goto/16 :goto_0

    .line 76
    :cond_40
    const/16 v5, 0x37

    if-le p0, v5, :cond_41

    const/16 v0, 0x10

    goto/16 :goto_0

    .line 77
    :cond_41
    const/16 v5, 0x36

    if-le p0, v5, :cond_42

    const/16 v0, 0xf

    goto/16 :goto_0

    .line 78
    :cond_42
    const/16 v5, 0x35

    if-le p0, v5, :cond_43

    const/16 v0, 0xe

    goto/16 :goto_0

    .line 79
    :cond_43
    const/16 v5, 0x34

    if-le p0, v5, :cond_44

    const/16 v0, 0xd

    goto/16 :goto_0

    .line 80
    :cond_44
    const/16 v5, 0x33

    if-le p0, v5, :cond_45

    const/16 v0, 0xc

    goto/16 :goto_0

    .line 81
    :cond_45
    const/16 v5, 0x32

    if-le p0, v5, :cond_46

    const/16 v0, 0xb

    goto/16 :goto_0

    .line 82
    :cond_46
    const/16 v5, 0x31

    if-le p0, v5, :cond_47

    const/16 v0, 0xa

    goto/16 :goto_0

    .line 83
    :cond_47
    const/16 v5, 0x30

    if-le p0, v5, :cond_48

    const/16 v0, 0x9

    goto/16 :goto_0

    .line 84
    :cond_48
    const/16 v5, 0x2f

    if-le p0, v5, :cond_49

    const/16 v0, 0x8

    goto/16 :goto_0

    .line 85
    :cond_49
    const/16 v5, 0x2e

    if-le p0, v5, :cond_4a

    const/4 v0, 0x7

    goto/16 :goto_0

    .line 86
    :cond_4a
    const/16 v5, 0x2d

    if-le p0, v5, :cond_4b

    const/4 v0, 0x6

    goto/16 :goto_0

    .line 87
    :cond_4b
    if-le p0, v0, :cond_4c

    const/4 v0, 0x5

    goto/16 :goto_0

    .line 88
    :cond_4c
    if-le p0, v1, :cond_4d

    const/4 v0, 0x4

    goto/16 :goto_0

    .line 89
    :cond_4d
    if-le p0, v2, :cond_4e

    const/4 v0, 0x3

    goto/16 :goto_0

    .line 90
    :cond_4e
    if-le p0, v3, :cond_4f

    const/4 v0, 0x2

    goto/16 :goto_0

    .line 91
    :cond_4f
    if-le p0, v4, :cond_50

    const/4 v0, 0x1

    goto/16 :goto_0

    .line 93
    :cond_50
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
