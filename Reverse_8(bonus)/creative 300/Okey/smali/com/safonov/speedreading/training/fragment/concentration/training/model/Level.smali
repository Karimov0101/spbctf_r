.class public Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;
.super Ljava/lang/Object;
.source "Level.java"


# instance fields
.field private circleCount:I

.field private circleCountTrue:I

.field private circleRadius:I

.field private circleSpeed:I

.field private score:I


# direct methods
.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "circleCount"    # I
    .param p2, "circleCountTrue"    # I
    .param p3, "circleRadius"    # I
    .param p4, "circleSpeed"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCount:I

    .line 24
    iput p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCountTrue:I

    .line 25
    iput p3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleRadius:I

    .line 26
    iput p4, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleSpeed:I

    .line 27
    return-void
.end method


# virtual methods
.method public getCircleCount()I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCount:I

    return v0
.end method

.method public getCircleCountTrue()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCountTrue:I

    return v0
.end method

.method public getCircleRadius()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleRadius:I

    return v0
.end method

.method public getCircleSpeed()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleSpeed:I

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 15
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->score:I

    return v0
.end method

.method public setCircleCount(I)V
    .locals 0
    .param p1, "circleCount"    # I

    .prologue
    .line 34
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCount:I

    .line 35
    return-void
.end method

.method public setCircleCountTrue(I)V
    .locals 0
    .param p1, "circleCountTrue"    # I

    .prologue
    .line 42
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleCountTrue:I

    .line 43
    return-void
.end method

.method public setCircleRadius(I)V
    .locals 0
    .param p1, "circleRadius"    # I

    .prologue
    .line 50
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleRadius:I

    .line 51
    return-void
.end method

.method public setCircleSpeed(I)V
    .locals 0
    .param p1, "circleSpeed"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->circleSpeed:I

    .line 59
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 19
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/Level;->score:I

    .line 20
    return-void
.end method
