.class public Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
.super Lio/realm/RealmObject;
.source "RememberNumberConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/RememberNumberConfigRealmProxyInterface;


# static fields
.field public static final FIELD_ANSWERS_TO_COMPLEXITY_DOWN:Ljava/lang/String; = "answersToComplexityDown"

.field public static final FIELD_ANSWERS_TO_COMPLEXITY_UP:Ljava/lang/String; = "answersToComplexityUp"

.field public static final FIELD_COMPLEXITY:Ljava/lang/String; = "complexity"

.field public static final FIELD_MAX_COMPLEXITY:Ljava/lang/String; = "maxComplexity"

.field public static final FIELD_MIN_COMPLEXITY:Ljava/lang/String; = "minComplexity"

.field public static final FIELD_TRAINING_SHOW_COUNT:Ljava/lang/String; = "trainingShowCount"


# instance fields
.field private answersToComplexityDown:I

.field private answersToComplexityUp:I

.field private complexity:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private maxComplexity:I

.field private minComplexity:I

.field private trainingShowCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getAnswersToComplexityDown()I
    .locals 1

    .prologue
    .line 82
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$answersToComplexityDown()I

    move-result v0

    return v0
.end method

.method public getAnswersToComplexityUp()I
    .locals 1

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$answersToComplexityUp()I

    move-result v0

    return v0
.end method

.method public getComplexity()I
    .locals 1

    .prologue
    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$complexity()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getMaxComplexity()I
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$maxComplexity()I

    move-result v0

    return v0
.end method

.method public getMinComplexity()I
    .locals 1

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$minComplexity()I

    move-result v0

    return v0
.end method

.method public getTrainingShowCount()I
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmGet$trainingShowCount()I

    move-result v0

    return v0
.end method

.method public realmGet$answersToComplexityDown()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->answersToComplexityDown:I

    return v0
.end method

.method public realmGet$answersToComplexityUp()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->answersToComplexityUp:I

    return v0
.end method

.method public realmGet$complexity()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->complexity:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->id:I

    return v0
.end method

.method public realmGet$maxComplexity()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->maxComplexity:I

    return v0
.end method

.method public realmGet$minComplexity()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->minComplexity:I

    return v0
.end method

.method public realmGet$trainingShowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->trainingShowCount:I

    return v0
.end method

.method public realmSet$answersToComplexityDown(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->answersToComplexityDown:I

    return-void
.end method

.method public realmSet$answersToComplexityUp(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->answersToComplexityUp:I

    return-void
.end method

.method public realmSet$complexity(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->complexity:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->id:I

    return-void
.end method

.method public realmSet$maxComplexity(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->maxComplexity:I

    return-void
.end method

.method public realmSet$minComplexity(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->minComplexity:I

    return-void
.end method

.method public realmSet$trainingShowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->trainingShowCount:I

    return-void
.end method

.method public setAnswersToComplexityDown(I)V
    .locals 0
    .param p1, "answersToComplexityDown"    # I

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$answersToComplexityDown(I)V

    .line 87
    return-void
.end method

.method public setAnswersToComplexityUp(I)V
    .locals 0
    .param p1, "answersToComplexityUp"    # I

    .prologue
    .line 78
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$answersToComplexityUp(I)V

    .line 79
    return-void
.end method

.method public setComplexity(I)V
    .locals 0
    .param p1, "complexity"    # I

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$complexity(I)V

    .line 71
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$id(I)V

    .line 39
    return-void
.end method

.method public setMaxComplexity(I)V
    .locals 0
    .param p1, "maxComplexity"    # I

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$maxComplexity(I)V

    .line 63
    return-void
.end method

.method public setMinComplexity(I)V
    .locals 0
    .param p1, "minComplexity"    # I

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$minComplexity(I)V

    .line 55
    return-void
.end method

.method public setTrainingShowCount(I)V
    .locals 0
    .param p1, "trainingShowCount"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->realmSet$trainingShowCount(I)V

    .line 47
    return-void
.end method
