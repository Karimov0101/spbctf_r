.class public interface abstract Lcom/safonov/speedreading/training/fragment/passcource/result/view/IPassCourseResultView;
.super Ljava/lang/Object;
.source "IPassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract addEvenNumbersResultView(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addGreenDotResultView(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;)V
.end method

.method public abstract addLineOfSightResultView(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addPassCourseResultView(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addRememberNumberResultView(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addSchulteTableResultView(Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addSpeedReadingResultView(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract addWordPairsResultView(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract dismissProgressDialog()V
.end method

.method public abstract showProgressDialog()V
.end method
