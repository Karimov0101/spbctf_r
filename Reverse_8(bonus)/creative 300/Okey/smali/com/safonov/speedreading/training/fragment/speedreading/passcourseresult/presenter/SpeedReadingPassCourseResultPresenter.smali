.class public Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SpeedReadingPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/ISpeedReadingPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 13
    .param p1, "resultId"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 27
    iget-object v11, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-interface {v11, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v5

    .line 28
    .local v5, "currentResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    iget-object v11, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v12

    invoke-virtual {v12}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v12

    invoke-interface {v11, v12}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getBestResultByMaxSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v4

    .line 29
    .local v4, "bestMaxSpeedResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    iget-object v11, p0, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getConfig()Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v12

    invoke-virtual {v12}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getId()I

    move-result v12

    invoke-interface {v11, v12}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getBestResultByAverageSpeed(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v2

    .line 31
    .local v2, "bestAverageSpeedResult":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v8

    .line 32
    .local v8, "maxSpeed":I
    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getMaxSpeed()I

    move-result v3

    .line 33
    .local v3, "bestMaxSpeed":I
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v11

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v12

    if-ne v11, v12, :cond_1

    move v7, v9

    .line 35
    .local v7, "isNewBestMaxSpeed":Z
    :goto_0
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v0

    .line 36
    .local v0, "averageSpeed":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getAverageSpeed()I

    move-result v1

    .line 37
    .local v1, "bestAverageSpeed":I
    invoke-virtual {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v11

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->getId()I

    move-result v12

    if-ne v11, v12, :cond_2

    move v6, v9

    .line 39
    .local v6, "isNewBestAverageSpeed":Z
    :goto_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->isViewAttached()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v8}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->updateMaxSpeedView(I)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->updateBestMaxSpeedView(I)V

    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v7}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->setNewBestMaxSpeedViewVisibility(Z)V

    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->updateAverageSpeedView(I)V

    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->updateBestAverageSpeedView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-interface {v9, v6}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->setNewBestAverageSpeedViewVisibility(Z)V

    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/presenter/SpeedReadingPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v9

    check-cast v9, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;

    invoke-static {v8}, Lcom/safonov/speedreading/training/fragment/passcource/util/SpeedReadingScoreUtil;->getPassCourseScore(I)I

    move-result v10

    invoke-interface {v9, v10}, Lcom/safonov/speedreading/training/fragment/speedreading/passcourseresult/view/ISpeedReadingPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 50
    :cond_0
    return-void

    .end local v0    # "averageSpeed":I
    .end local v1    # "bestAverageSpeed":I
    .end local v6    # "isNewBestAverageSpeed":Z
    .end local v7    # "isNewBestMaxSpeed":Z
    :cond_1
    move v7, v10

    .line 33
    goto :goto_0

    .restart local v0    # "averageSpeed":I
    .restart local v1    # "bestAverageSpeed":I
    .restart local v7    # "isNewBestMaxSpeed":Z
    :cond_2
    move v6, v10

    .line 37
    goto :goto_1
.end method
