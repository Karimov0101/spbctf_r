.class public final enum Lcom/safonov/speedreading/training/TrainingType;
.super Ljava/lang/Enum;
.source "TrainingType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/training/TrainingType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum CONCENTRATION:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum EVEN_NUMBERS:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum GREEN_DOT:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum MATHEMATICS:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

.field private static final RANDOM:Ljava/util/Random;

.field public static final enum REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum REMEMBER_WORDS:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

.field private static final SIZE:I

.field public static final enum SPEED_READING:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum TRUE_COLORS:Lcom/safonov/speedreading/training/TrainingType;

.field private static final VALUES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/TrainingType;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

.field public static final enum WORD_PAIRS:Lcom/safonov/speedreading/training/TrainingType;


# instance fields
.field private final isSettingsSupported:Z

.field private final titleRes:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "PASS_COURSE"

    const v2, 0x7f0e00df

    invoke-direct {v0, v1, v4, v2, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    .line 19
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "ACCELERATOR_COURSE"

    const v2, 0x7f0e001f

    invoke-direct {v0, v1, v5, v2, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    .line 21
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "WORDS_COLUMNS"

    const v2, 0x7f0e01b0

    invoke-direct {v0, v1, v6, v2, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    .line 22
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "WORDS_BLOCK"

    const v2, 0x7f0e01ae

    invoke-direct {v0, v1, v7, v2, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    .line 23
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "FLASH_WORDS"

    const v2, 0x7f0e0072

    invoke-direct {v0, v1, v8, v2, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    .line 25
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "SCHULTE_TABLE"

    const/4 v2, 0x5

    const v3, 0x7f0e0167

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    .line 26
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "REMEMBER_NUMBER"

    const/4 v2, 0x6

    const v3, 0x7f0e014a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    .line 27
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "LINE_OF_SIGHT"

    const/4 v2, 0x7

    const v3, 0x7f0e00a6

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    .line 28
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "SPEED_READING"

    const/16 v2, 0x8

    const v3, 0x7f0e0183

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->SPEED_READING:Lcom/safonov/speedreading/training/TrainingType;

    .line 29
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "WORD_PAIRS"

    const/16 v2, 0x9

    const v3, 0x7f0e01a6

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->WORD_PAIRS:Lcom/safonov/speedreading/training/TrainingType;

    .line 30
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "EVEN_NUMBERS"

    const/16 v2, 0xa

    const v3, 0x7f0e0063

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/TrainingType;

    .line 31
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "GREEN_DOT"

    const/16 v2, 0xb

    const v3, 0x7f0e007f

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->GREEN_DOT:Lcom/safonov/speedreading/training/TrainingType;

    .line 32
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "MATHEMATICS"

    const/16 v2, 0xc

    const v3, 0x7f0e00ca

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->MATHEMATICS:Lcom/safonov/speedreading/training/TrainingType;

    .line 33
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "CONCENTRATION"

    const/16 v2, 0xd

    const v3, 0x7f0e0046

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->CONCENTRATION:Lcom/safonov/speedreading/training/TrainingType;

    .line 34
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "CUPTIMER"

    const/16 v2, 0xe

    const v3, 0x7f0e0057

    invoke-direct {v0, v1, v2, v3, v5}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    .line 35
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "REMEMBER_WORDS"

    const/16 v2, 0xf

    const v3, 0x7f0e0152

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    .line 36
    new-instance v0, Lcom/safonov/speedreading/training/TrainingType;

    const-string v1, "TRUE_COLORS"

    const/16 v2, 0x10

    const v3, 0x7f0e019d

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/training/TrainingType;-><init>(Ljava/lang/String;IIZ)V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->TRUE_COLORS:Lcom/safonov/speedreading/training/TrainingType;

    .line 17
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->SPEED_READING:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->WORD_PAIRS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->GREEN_DOT:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->MATHEMATICS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->CONCENTRATION:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->TRUE_COLORS:Lcom/safonov/speedreading/training/TrainingType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->$VALUES:[Lcom/safonov/speedreading/training/TrainingType;

    .line 64
    invoke-static {}, Lcom/safonov/speedreading/training/TrainingType;->values()[Lcom/safonov/speedreading/training/TrainingType;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->VALUES:Ljava/util/List;

    .line 65
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType;->VALUES:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sput v0, Lcom/safonov/speedreading/training/TrainingType;->SIZE:I

    .line 66
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType;->RANDOM:Ljava/util/Random;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "titleRes"    # I
    .param p4, "isSettingsSupported"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IZ)V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 42
    iput p3, p0, Lcom/safonov/speedreading/training/TrainingType;->titleRes:I

    .line 43
    iput-boolean p4, p0, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported:Z

    .line 44
    return-void
.end method

.method public static getReferenceComplexityFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 185
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 189
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    .line 187
    :goto_0
    return-object v0

    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 185
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getReferenceDescriptionFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 97
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 116
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 98
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 114
    :goto_0
    return-object v0

    .line 99
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 101
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 103
    :pswitch_3
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 104
    :pswitch_4
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 105
    :pswitch_5
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 106
    :pswitch_6
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 107
    :pswitch_7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 108
    :pswitch_8
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 109
    :pswitch_9
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 110
    :pswitch_a
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 111
    :pswitch_b
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 112
    :pswitch_c
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 113
    :pswitch_d
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 114
    :pswitch_e
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 97
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static getReferenceHelpFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 73
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 90
    :goto_0
    return-object v0

    .line 75
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 77
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 79
    :pswitch_3
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 80
    :pswitch_4
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 81
    :pswitch_5
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 82
    :pswitch_6
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 83
    :pswitch_7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 84
    :pswitch_8
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 85
    :pswitch_9
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 86
    :pswitch_a
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 87
    :pswitch_b
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 88
    :pswitch_c
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 89
    :pswitch_d
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 90
    :pswitch_e
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static getReferenceResultFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 162
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 180
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 178
    :goto_0
    return-object v0

    .line 164
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 165
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 167
    :pswitch_3
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 168
    :pswitch_4
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 169
    :pswitch_5
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 170
    :pswitch_6
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 171
    :pswitch_7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 172
    :pswitch_8
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 173
    :pswitch_9
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 174
    :pswitch_a
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 175
    :pswitch_b
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 176
    :pswitch_c
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 177
    :pswitch_d
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 178
    :pswitch_e
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 162
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static getReferenceSettingsFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 121
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 133
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 131
    :goto_0
    return-object v0

    .line 123
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 125
    :pswitch_3
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 127
    :pswitch_4
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 128
    :pswitch_5
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 129
    :pswitch_6
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 130
    :pswitch_7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 131
    :pswitch_8
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method public static getReferenceTrainingFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 138
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType$1;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 157
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported training type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 139
    :pswitch_0
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/FragmentType;

    .line 155
    :goto_0
    return-object v0

    .line 140
    :pswitch_1
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 142
    :pswitch_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_TRAINING:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 144
    :pswitch_3
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 145
    :pswitch_4
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 146
    :pswitch_5
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 147
    :pswitch_6
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 148
    :pswitch_7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 149
    :pswitch_8
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 150
    :pswitch_9
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 151
    :pswitch_a
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 152
    :pswitch_b
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 153
    :pswitch_c
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 154
    :pswitch_d
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 155
    :pswitch_e
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS:Lcom/safonov/speedreading/training/FragmentType;

    goto :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method public static isCourseTraining(Lcom/safonov/speedreading/training/TrainingType;)Z
    .locals 2
    .param p0, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    .line 57
    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-ne v1, p0, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-eq v1, p0, :cond_0

    .line 60
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static randomTraining()Lcom/safonov/speedreading/training/TrainingType;
    .locals 3

    .prologue
    .line 69
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType;->VALUES:Ljava/util/List;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->RANDOM:Ljava/util/Random;

    sget v2, Lcom/safonov/speedreading/training/TrainingType;->SIZE:I

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/TrainingType;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/training/TrainingType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/TrainingType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/training/TrainingType;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType;->$VALUES:[Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/training/TrainingType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/training/TrainingType;

    return-object v0
.end method


# virtual methods
.method public getTitleRes()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 48
    iget v0, p0, Lcom/safonov/speedreading/training/TrainingType;->titleRes:I

    return v0
.end method

.method public isSettingsSupported()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported:Z

    return v0
.end method
