.class public Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment_ViewBinding;
.super Ljava/lang/Object;
.source "CupTimerFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;

    .line 23
    const v0, 0x7f09006c

    const-string v1, "field \'timerBar\'"

    const-class v2, Landroid/widget/ProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 24
    const v0, 0x7f090146

    const-string v1, "field \'previewTitle\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0900d2

    const-string v1, "field \'imageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    .line 26
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;

    .line 32
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 33
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;

    .line 35
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->timerBar:Landroid/widget/ProgressBar;

    .line 36
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    .line 37
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    .line 38
    return-void
.end method
