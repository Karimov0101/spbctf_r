.class public Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "RememberNumberPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/IRememberNumberPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/IRememberNumberPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 7
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    invoke-interface {v5, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v3

    .line 28
    .local v3, "result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getConfig()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getId()I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v4

    .line 31
    .local v4, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getScore()I

    move-result v1

    .line 33
    .local v1, "bestScore":I
    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getId()I

    move-result v5

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->getId()I

    move-result v6

    if-ne v5, v6, :cond_1

    const/4 v2, 0x1

    .line 35
    .local v2, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->isViewAttached()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;

    invoke-interface {v5, v4}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;->updateScoreView(I)V

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;

    invoke-interface {v5, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;->updateBestScoreView(I)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;

    invoke-interface {v5, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;->setNewBestScoreViewVisibility(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/presenter/RememberNumberPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/passcource/util/RememberNumberScoreUtil;->getPassCourseScore(I)I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/remembernumber/passcourseresult/view/IRememberNumberPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 41
    :cond_0
    return-void

    .line 33
    .end local v2    # "isNewBest":Z
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
