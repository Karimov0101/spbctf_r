.class Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment$1;
.super Ljava/lang/Object;
.source "GreenDotResultFragment.java"

# interfaces
.implements Lcom/github/mikephil/charting/formatter/IValueFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->setChartViewData(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFormattedValue(FLcom/github/mikephil/charting/data/Entry;ILcom/github/mikephil/charting/utils/ViewPortHandler;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # F
    .param p2, "entry"    # Lcom/github/mikephil/charting/data/Entry;
    .param p3, "dataSetIndex"    # I
    .param p4, "viewPortHandler"    # Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->access$000(Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;)Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    move-result-object v0

    float-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
