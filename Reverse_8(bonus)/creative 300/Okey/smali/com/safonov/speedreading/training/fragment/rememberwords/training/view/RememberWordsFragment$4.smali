.class Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;
.super Ljava/lang/Object;
.source "RememberWordsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 375
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 378
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$602(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I

    .line 379
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$702(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I

    .line 380
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$802(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;I)I

    .line 382
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    invoke-virtual {v0}, Landroid/widget/TableLayout;->removeAllViews()V

    .line 383
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answersContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 385
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->requestList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 386
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 387
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsRequest:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 388
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->rowsAnswer:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 390
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 391
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    invoke-virtual {v0, v2}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 392
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->statisticsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 394
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->progressBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$900(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 396
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$500(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$4;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$1000(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 397
    return-void
.end method
