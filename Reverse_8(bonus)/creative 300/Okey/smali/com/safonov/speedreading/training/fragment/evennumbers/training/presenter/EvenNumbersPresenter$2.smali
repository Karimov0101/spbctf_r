.class Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "EvenNumbersPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private isCanceled:Z

.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->val$configId:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 70
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationCancel(Landroid/animation/Animator;)V

    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->isCanceled:Z

    .line 72
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 76
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 77
    iget-boolean v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->isCanceled:Z

    if-nez v1, :cond_0

    .line 79
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;-><init>()V

    .line 80
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setScore(I)V

    .line 81
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->setUnixTime(J)V

    .line 83
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;->addResult(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V

    .line 93
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 64
    invoke-super {p0, p1}, Landroid/animation/AnimatorListenerAdapter;->onAnimationStart(Landroid/animation/Animator;)V

    .line 65
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter$2;->isCanceled:Z

    .line 66
    return-void
.end method
