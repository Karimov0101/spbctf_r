.class public Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
.super Lio/realm/RealmObject;
.source "MathConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/MathConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COMPLEXITY:Ljava/lang/String; = "complexity"

.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"


# instance fields
.field private complexity:I

.field private duration:J

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getComplexity()I
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmGet$complexity()I

    move-result v0

    return v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmGet$duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public realmGet$complexity()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->complexity:I

    return v0
.end method

.method public realmGet$duration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->duration:J

    return-wide v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->id:I

    return v0
.end method

.method public realmSet$complexity(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->complexity:I

    return-void
.end method

.method public realmSet$duration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->duration:J

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->id:I

    return-void
.end method

.method public setComplexity(I)V
    .locals 0
    .param p1, "complexity"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmSet$complexity(I)V

    .line 39
    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmSet$duration(J)V

    .line 47
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->realmSet$id(I)V

    .line 26
    return-void
.end method
