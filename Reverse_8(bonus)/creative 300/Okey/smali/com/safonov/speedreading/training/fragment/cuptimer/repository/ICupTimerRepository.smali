.class public interface abstract Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;
.super Ljava/lang/Object;
.source "ICupTimerRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;",
            ">;"
        }
    .end annotation
.end method
