.class Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;
.super Ljava/lang/Object;
.source "WordColumnsPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 175
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v0

    if-nez v0, :cond_3

    .line 182
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getItemCount()I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getWordsPerItem()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;II)[Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->setItems([Ljava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->deselectItem(I)V

    .line 187
    :goto_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->selectItem(I)V

    .line 189
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$508(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    .line 190
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getItemCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_2

    .line 191
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$502(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;I)I

    .line 194
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 185
    :cond_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->deselectItem(I)V

    goto :goto_1
.end method
