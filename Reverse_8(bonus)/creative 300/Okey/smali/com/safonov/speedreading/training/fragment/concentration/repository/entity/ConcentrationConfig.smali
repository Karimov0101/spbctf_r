.class public Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
.super Lio/realm/RealmObject;
.source "ConcentrationConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/ConcentrationConfigRealmProxyInterface;


# static fields
.field public static final FIELD_CIRCLES_COUNT:Ljava/lang/String; = "circlesCount"

.field public static final FIELD_CIRCLES_RADIUS:Ljava/lang/String; = "circlesRadius"

.field public static final FIELD_CIRCLES_SPEED:Ljava/lang/String; = "circlesSpeed"

.field public static final FIELD_GRAY_TIME:Ljava/lang/String; = "grayTime"


# instance fields
.field private circlesCount:I

.field private circlesCountCustom:I

.field private circlesRadius:I

.field private circlesSizeCustom:I

.field private circlesSpeed:I

.field private circlesSpeedCustom:I

.field private complexity:I

.field private grayTime:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getCirclesCount()I
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesCount()I

    move-result v0

    return v0
.end method

.method public getCirclesCountCustom()I
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesCountCustom()I

    move-result v0

    return v0
.end method

.method public getCirclesRadius()I
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesRadius()I

    move-result v0

    return v0
.end method

.method public getCirclesSizeCustom()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesSizeCustom()I

    move-result v0

    return v0
.end method

.method public getCirclesSpeed()I
    .locals 1

    .prologue
    .line 80
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesSpeed()I

    move-result v0

    return v0
.end method

.method public getCirclesSpeedCustom()I
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$circlesSpeedCustom()I

    move-result v0

    return v0
.end method

.method public getComplexity()I
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$complexity()I

    move-result v0

    return v0
.end method

.method public getGrayTime()I
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$grayTime()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public realmGet$circlesCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesCount:I

    return v0
.end method

.method public realmGet$circlesCountCustom()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesCountCustom:I

    return v0
.end method

.method public realmGet$circlesRadius()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesRadius:I

    return v0
.end method

.method public realmGet$circlesSizeCustom()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSizeCustom:I

    return v0
.end method

.method public realmGet$circlesSpeed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSpeed:I

    return v0
.end method

.method public realmGet$circlesSpeedCustom()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSpeedCustom:I

    return v0
.end method

.method public realmGet$complexity()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->complexity:I

    return v0
.end method

.method public realmGet$grayTime()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->grayTime:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->id:I

    return v0
.end method

.method public realmSet$circlesCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesCount:I

    return-void
.end method

.method public realmSet$circlesCountCustom(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesCountCustom:I

    return-void
.end method

.method public realmSet$circlesRadius(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesRadius:I

    return-void
.end method

.method public realmSet$circlesSizeCustom(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSizeCustom:I

    return-void
.end method

.method public realmSet$circlesSpeed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSpeed:I

    return-void
.end method

.method public realmSet$circlesSpeedCustom(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->circlesSpeedCustom:I

    return-void
.end method

.method public realmSet$complexity(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->complexity:I

    return-void
.end method

.method public realmSet$grayTime(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->grayTime:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->id:I

    return-void
.end method

.method public setCirclesCount(I)V
    .locals 0
    .param p1, "circlesCount"    # I

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesCount(I)V

    .line 69
    return-void
.end method

.method public setCirclesCountCustom(I)V
    .locals 0
    .param p1, "circlesCountCustom"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesCountCustom(I)V

    .line 53
    return-void
.end method

.method public setCirclesRadius(I)V
    .locals 0
    .param p1, "circlesRadius"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesRadius(I)V

    .line 77
    return-void
.end method

.method public setCirclesSizeCustom(I)V
    .locals 0
    .param p1, "circlesSizeCustom"    # I

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesSizeCustom(I)V

    .line 45
    return-void
.end method

.method public setCirclesSpeed(I)V
    .locals 0
    .param p1, "circlesSpeed"    # I

    .prologue
    .line 84
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesSpeed(I)V

    .line 85
    return-void
.end method

.method public setCirclesSpeedCustom(I)V
    .locals 0
    .param p1, "circlesSpeedCustom"    # I

    .prologue
    .line 36
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$circlesSpeedCustom(I)V

    .line 37
    return-void
.end method

.method public setComplexity(I)V
    .locals 0
    .param p1, "complexity"    # I

    .prologue
    .line 92
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$complexity(I)V

    .line 93
    return-void
.end method

.method public setGrayTime(I)V
    .locals 0
    .param p1, "grayTime"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$grayTime(I)V

    .line 61
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 97
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->realmSet$id(I)V

    .line 98
    return-void
.end method
