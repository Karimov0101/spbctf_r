.class public Lcom/safonov/speedreading/training/activity/MathConfigUtil;
.super Ljava/lang/Object;
.source "MathConfigUtil.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig(J)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 2
    .param p0, "duration"    # J

    .prologue
    .line 12
    new-instance v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;-><init>()V

    .line 14
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    invoke-virtual {v0, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->setDuration(J)V

    .line 16
    return-object v0
.end method
