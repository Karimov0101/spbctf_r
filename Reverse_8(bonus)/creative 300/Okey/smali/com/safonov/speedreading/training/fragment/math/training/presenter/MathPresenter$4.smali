.class Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;
.super Ljava/lang/Object;
.source "MathPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setIncorrectAnswer(I)V

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setPointsTextViewIncorrect()V

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;->setButtonsEnabled(Z)V

    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter$4;->this$0:Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/math/training/presenter/MathPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 166
    return-void
.end method
