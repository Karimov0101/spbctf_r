.class public interface abstract Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;
.super Ljava/lang/Object;
.source "ISchulteTablePassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setNewBestTimeViewVisibility(Z)V
.end method

.method public abstract updateBestTimeView(J)V
.end method

.method public abstract updatePassCoursePointsView(I)V
.end method

.method public abstract updateTimeView(J)V
.end method
