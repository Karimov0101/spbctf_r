.class public Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment_ViewBinding;
.super Ljava/lang/Object;
.source "SchulteTableFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    .line 25
    const v2, 0x7f090182

    const-string v3, "field \'statisticsPanel\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->statisticsPanel:Landroid/view/View;

    .line 26
    const v2, 0x7f090187

    const-string v3, "field \'timeTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->timeTextView:Landroid/widget/TextView;

    .line 27
    const v2, 0x7f09017f

    const-string v3, "field \'bestTimeTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->bestTimeTextView:Landroid/widget/TextView;

    .line 28
    const v2, 0x7f090181

    const-string v3, "field \'nextItemTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->nextItemTextView:Landroid/widget/TextView;

    .line 29
    const v2, 0x7f090180

    const-string v3, "field \'gridLayout\'"

    const-class v4, Landroid/support/v7/widget/GridLayout;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/GridLayout;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 31
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 32
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 33
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f06001f

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->backgroundWhiteColor:I

    .line 34
    const v2, 0x7f060074

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->tableEdgeColor:I

    .line 35
    const v2, 0x106000b

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorWhite:I

    .line 36
    const v2, 0x1060003

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->textColorBlack:I

    .line 37
    const v2, 0x7f06001a

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->greenColor:I

    .line 38
    const v2, 0x7f06006d

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->redColor:I

    .line 39
    const v2, 0x7f0700a6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->itemMargin:I

    .line 40
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    .line 46
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 47
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;

    .line 49
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->statisticsPanel:Landroid/view/View;

    .line 50
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->timeTextView:Landroid/widget/TextView;

    .line 51
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->bestTimeTextView:Landroid/widget/TextView;

    .line 52
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->nextItemTextView:Landroid/widget/TextView;

    .line 53
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/SchulteTableFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 54
    return-void
.end method
