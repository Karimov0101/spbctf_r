.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository$OnMultiTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetPassCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 259
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted([I)V
    .locals 12
    .param p1, "ids"    # [I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 262
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 263
    .local v0, "configWrapperType1":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    aget v8, p1, v10

    invoke-virtual {v0, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 264
    sget-object v8, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 265
    invoke-virtual {v0, v10}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 267
    new-instance v1, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 268
    .local v1, "configWrapperType2":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    aget v8, p1, v11

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 269
    sget-object v8, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 270
    invoke-virtual {v1, v10}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 272
    new-instance v2, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 273
    .local v2, "configWrapperType3":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    const/4 v8, 0x2

    aget v8, p1, v8

    invoke-virtual {v2, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 274
    sget-object v8, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v2, v8}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 275
    invoke-virtual {v2, v10}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 277
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v9, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v8, v9, v11, v11, v10}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v4

    .line 278
    .local v4, "fragmentTypeListWithDescription":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v9, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v8, v9, v10, v11, v10}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v3

    .line 280
    .local v3, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v4}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 281
    .local v5, "trainingWrapperType1":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    new-instance v6, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v6, v1, v3}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 282
    .local v6, "trainingWrapperType2":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    new-instance v7, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v7, v2, v3}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 284
    .local v7, "trainingWrapperType3":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/4 v9, 0x6

    aput-object v5, v8, v9

    .line 285
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/16 v9, 0x10

    aput-object v6, v8, v9

    .line 286
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/16 v9, 0x11

    aput-object v7, v8, v9

    .line 288
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v9, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v8, v9}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 289
    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v9, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$3;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v8, v9}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 291
    :cond_0
    return-void
.end method
