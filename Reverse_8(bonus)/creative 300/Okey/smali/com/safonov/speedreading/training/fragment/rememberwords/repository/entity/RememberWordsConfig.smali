.class public Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
.super Lio/realm/RealmObject;
.source "RememberWordsConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/RememberWordsConfigRealmProxyInterface;


# static fields
.field public static final FIELD_START_WORDS_COUNT:Ljava/lang/String; = "startWordsCount"


# instance fields
.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private startWordsCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getStartWordsCount()I
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->realmGet$startWordsCount()I

    move-result v0

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->id:I

    return v0
.end method

.method public realmGet$startWordsCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->startWordsCount:I

    return v0
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->id:I

    return-void
.end method

.method public realmSet$startWordsCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->startWordsCount:I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->realmSet$id(I)V

    .line 23
    return-void
.end method

.method public setStartWordsCount(I)V
    .locals 0
    .param p1, "startWordsCount"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->realmSet$startWordsCount(I)V

    .line 36
    return-void
.end method
