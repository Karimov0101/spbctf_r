.class public Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SpeedReadingFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final ANSWER_ANIMATION_DURATION:I = 0x3e8

.field public static final ANSWER_ITEMS_COLUMN_COUNT:I = 0x2

.field public static final ANSWER_ITEMS_COUNT:I = 0x6

.field public static final ANSWER_ITEMS_ROW_COUNT:I = 0x3

.field public static final ITEMS_COLUMN_COUNT:I = 0x3

.field public static final ITEMS_COUNT:I = 0x9

.field public static final ITEMS_ROW_COUNT:I = 0x3

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field answerButtons:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f0901ab,
            0x7f0901ac,
            0x7f0901ad,
            0x7f0901ae,
            0x7f0901af,
            0x7f0901b0
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation
.end field

.field answerGridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b1
    .end annotation
.end field

.field answerSpeedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b2
    .end annotation
.end field

.field answerSpeedTitleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b3
    .end annotation
.end field

.field private configId:I

.field greenColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b9
    .end annotation
.end field

.field redColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006d
    .end annotation
.end field

.field private speedReadingModel:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;

.field private speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field speedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901bb
    .end annotation
.end field

.field statisticsView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901bc
    .end annotation
.end field

.field textColorBlack:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;

.field wordsTextViewList:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f0901bd,
            0x7f0901be,
            0x7f0901bf,
            0x7f0901c0,
            0x7f0901c1,
            0x7f0901c2,
            0x7f0901c3,
            0x7f0901c4,
            0x7f0901c5
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field wordsView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901c6
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 274
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->handler:Landroid/os/Handler;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method private getAnswerButtonIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "buttonView"    # Landroid/view/View;

    .prologue
    .line 178
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 183
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 178
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;
    .locals 3
    .param p0, "speedReadingConfigId"    # I

    .prologue
    .line 70
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;-><init>()V

    .line 71
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 72
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 73
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->setArguments(Landroid/os/Bundle;)V

    .line 74
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 57
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getSpeedReadingRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 58
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingModel:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;

    .line 60
    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingModel:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V

    return-object v1
.end method

.method public hideAnswerView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 249
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 251
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerGridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayout;->setVisibility(I)V

    .line 252
    return-void
.end method

.method public hideWordsView()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 236
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->statisticsView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 237
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 238
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 297
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 298
    return-void
.end method

.method public onAnswerButtonClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901ab,
            0x7f0901ac,
            0x7f0901ad,
            0x7f0901ae,
            0x7f0901af,
            0x7f0901b0
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getAnswerButtonIndex(Landroid/view/View;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;->answerButtonClick(I)V

    .line 130
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 319
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 320
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 321
    check-cast p1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

    .line 326
    return-void

    .line 323
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement SpeedReadingTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 80
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 81
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->configId:I

    .line 83
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 116
    const v1, 0x7f0b00aa

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 118
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->unbinder:Lbutterknife/Unbinder;

    .line 120
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 211
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 212
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingModel:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/SpeedReadingModel;

    .line 214
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->close()V

    .line 215
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedReadingRealmUtil:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;->cancelTraining()V

    .line 218
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 219
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 223
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 225
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->unbinder:Lbutterknife/Unbinder;

    .line 226
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 330
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

    .line 332
    return-void
.end method

.method public onSpeedReadingTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 312
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingTrainingCompleteListener;->onSpeedReadingTrainingCompleted(I)V

    .line 313
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 195
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 196
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;->startTraining(I)V

    .line 197
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 201
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;->pauseTraining()V

    .line 202
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;->resumeTraining()V

    .line 207
    return-void
.end method

.method public setAnswerButtonsEnabled(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    .line 262
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 263
    .local v0, "button":Landroid/widget/Button;
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    .line 265
    .end local v0    # "button":Landroid/widget/Button;
    :cond_0
    return-void
.end method

.method public setAnswerItems(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 188
    .local p1, "answers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    .line 189
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 188
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 191
    :cond_0
    return-void
.end method

.method public setWordItem(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "word"    # Ljava/lang/String;

    .prologue
    .line 256
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsTextViewList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    return-void
.end method

.method public showAnswerView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 243
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 244
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerGridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/GridLayout;->setVisibility(I)V

    .line 245
    return-void
.end method

.method public showWordsView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->statisticsView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 231
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->wordsView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 232
    return-void
.end method

.method public updateAnswerSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->textColorBlack:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 271
    return-void
.end method

.method public updateAnswerSpeedWithAnimation(IZ)V
    .locals 4
    .param p1, "speed"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 278
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 279
    if-eqz p2, :cond_0

    .line 280
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->greenColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 285
    :goto_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment$1;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 293
    return-void

    .line 282
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->answerSpeedTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->redColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 302
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 303
    return-void
.end method

.method public updateSpeed(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->speedTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0197

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/SpeedReadingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 308
    return-void
.end method
