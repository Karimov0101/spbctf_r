.class public Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;
.super Ljava/lang/Object;
.source "LineOfSightFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

.field private view2131296477:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;Landroid/view/View;)V
    .locals 7
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v6, 0x7f0900dd

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .line 30
    const v3, 0x7f0900e0

    const-string v4, "field \'progressBar\'"

    const-class v5, Landroid/widget/ProgressBar;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 31
    const v3, 0x7f0900de

    const-string v4, "field \'gridLayout\'"

    const-class v5, Landroid/support/v7/widget/GridLayout;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/GridLayout;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 32
    const-string v3, "field \'checkButton\' and method \'onCheckButtonClick\'"

    invoke-static {p2, v6, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 33
    .local v2, "view":Landroid/view/View;
    const-string v3, "field \'checkButton\'"

    const-class v4, Landroid/widget/Button;

    invoke-static {v2, v6, v3, v4}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkButton:Landroid/widget/Button;

    .line 34
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->view2131296477:Landroid/view/View;

    .line 35
    new-instance v3, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding$1;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 44
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f06001f

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->backgroundWhiteColor:I

    .line 45
    const v3, 0x1060003

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->textColorBlack:I

    .line 46
    const v3, 0x7f06001a

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->greenColor:I

    .line 47
    const v3, 0x7f060081

    invoke-static {v0, v3}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->white:I

    .line 48
    const v3, 0x7f07008a

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->boardItemHeight:I

    .line 49
    const v3, 0x7f07008b

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, p1, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->boardWidth:I

    .line 50
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .line 56
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 57
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .line 59
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 60
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    .line 61
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->checkButton:Landroid/widget/Button;

    .line 63
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->view2131296477:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 64
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment_ViewBinding;->view2131296477:Landroid/view/View;

    .line 65
    return-void
.end method
