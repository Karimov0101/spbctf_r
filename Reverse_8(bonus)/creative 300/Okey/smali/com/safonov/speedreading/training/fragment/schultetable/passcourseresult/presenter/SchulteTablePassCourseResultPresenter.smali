.class public Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SchulteTablePassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/ISchulteTablePassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    .line 21
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 6
    .param p1, "resultId"    # I

    .prologue
    .line 25
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-interface {v3, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v2

    .line 26
    .local v2, "result":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getConfig()Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v4

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->getId()I

    move-result v4

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v0

    .line 28
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getId()I

    move-result v3

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getId()I

    move-result v4

    if-ne v3, v4, :cond_1

    const/4 v1, 0x1

    .line 30
    .local v1, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->isViewAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;->updateTimeView(J)V

    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v4

    invoke-interface {v3, v4, v5}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;->updateBestTimeView(J)V

    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;

    invoke-interface {v3, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;->setNewBestTimeViewVisibility(Z)V

    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/presenter/SchulteTablePassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/safonov/speedreading/training/fragment/passcource/util/SchulteTableScoreUtil;->getPassCourseScore(J)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/schultetable/passcourseresult/view/ISchulteTablePassCourseResultView;->updatePassCoursePointsView(I)V

    .line 36
    :cond_0
    return-void

    .line 28
    .end local v1    # "isNewBest":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
