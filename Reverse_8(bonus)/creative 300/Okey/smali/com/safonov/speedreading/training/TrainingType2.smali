.class public final enum Lcom/safonov/speedreading/training/TrainingType2;
.super Ljava/lang/Enum;
.source "TrainingType2.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/training/TrainingType2;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/training/TrainingType2;


# instance fields
.field private final descriptionView:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field private final isRes:Z

.field private final isSettingsSupported:Z

.field private final settingsView:I
    .annotation build Landroid/support/annotation/XmlRes;
    .end annotation
.end field

.field private final title:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 10
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/safonov/speedreading/training/TrainingType2;

    sput-object v0, Lcom/safonov/speedreading/training/TrainingType2;->$VALUES:[Lcom/safonov/speedreading/training/TrainingType2;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIZZI)V
    .locals 0
    .param p3, "title"    # I
    .param p4, "descriptionView"    # I
    .param p5, "isRes"    # Z
    .param p6, "isSettingsSupported"    # Z
    .param p7, "settingsView"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZZI)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput p3, p0, Lcom/safonov/speedreading/training/TrainingType2;->title:I

    .line 44
    iput p4, p0, Lcom/safonov/speedreading/training/TrainingType2;->descriptionView:I

    .line 45
    iput-boolean p5, p0, Lcom/safonov/speedreading/training/TrainingType2;->isRes:Z

    .line 46
    iput-boolean p6, p0, Lcom/safonov/speedreading/training/TrainingType2;->isSettingsSupported:Z

    .line 47
    iput p7, p0, Lcom/safonov/speedreading/training/TrainingType2;->settingsView:I

    .line 48
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/training/TrainingType2;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 10
    const-class v0, Lcom/safonov/speedreading/training/TrainingType2;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/TrainingType2;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/training/TrainingType2;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/safonov/speedreading/training/TrainingType2;->$VALUES:[Lcom/safonov/speedreading/training/TrainingType2;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/training/TrainingType2;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/training/TrainingType2;

    return-object v0
.end method


# virtual methods
.method public getDescriptionView()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/safonov/speedreading/training/TrainingType2;->descriptionView:I

    return v0
.end method

.method public getSettingsView()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/safonov/speedreading/training/TrainingType2;->settingsView:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/TrainingType2;->title:I

    return v0
.end method

.method public isRes()Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/TrainingType2;->isRes:Z

    return v0
.end method

.method public isSettingsSupported()Z
    .locals 1

    .prologue
    .line 35
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/TrainingType2;->isSettingsSupported:Z

    return v0
.end method
