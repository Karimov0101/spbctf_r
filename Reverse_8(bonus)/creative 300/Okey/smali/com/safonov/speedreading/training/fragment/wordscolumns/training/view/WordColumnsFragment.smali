.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "WordColumnsFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final ITEM_TEXT_SIZE_SP:I = 0x10

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "training_config_id"


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field private configId:I

.field gridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900a6
    .end annotation
.end field

.field private itemCount:I

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field itemTextColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060004
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

.field speedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901c7
    .end annotation
.end field

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090207
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 75
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 60
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;-><init>()V

    .line 61
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 62
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 64
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getWordsColumnsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;)V

    return-object v0
.end method

.method public deselectItem(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 215
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 216
    return-void
.end method

.method public initGridLayout(II)V
    .locals 8
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I

    .prologue
    const/high16 v7, -0x80000000

    const/high16 v6, 0x3f800000    # 1.0f

    .line 173
    mul-int v4, p1, p2

    iput v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemCount:I

    .line 174
    new-instance v4, Ljava/util/ArrayList;

    iget v5, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemCount:I

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemList:Ljava/util/List;

    .line 176
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v4}, Landroid/support/v7/widget/GridLayout;->removeAllViews()V

    .line 177
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v4, p1}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 178
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v4, p2}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 180
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 181
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    if-ge v2, p2, :cond_0

    .line 182
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v4}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 184
    .local v1, "itemTextView":Landroid/widget/TextView;
    new-instance v3, Landroid/support/v7/widget/GridLayout$LayoutParams;

    .line 185
    invoke-static {v7, v6}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v4

    .line 186
    invoke-static {v7, v6}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/support/v7/widget/GridLayout$LayoutParams;-><init>(Landroid/support/v7/widget/GridLayout$Spec;Landroid/support/v7/widget/GridLayout$Spec;)V

    .line 187
    .local v3, "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/GridLayout$LayoutParams;->setGravity(I)V

    .line 189
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 190
    const/4 v4, 0x2

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v1, v4, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 191
    iget v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemTextColor:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 193
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v4, v1}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 181
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 180
    .end local v1    # "itemTextView":Landroid/widget/TextView;
    .end local v3    # "layoutParams":Landroid/support/v7/widget/GridLayout$LayoutParams;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v2    # "j":I
    :cond_1
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    invoke-interface {v4}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->startTraining()V

    .line 199
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 253
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 254
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

    .line 259
    return-void

    .line 256
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement FlashWordsTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->configId:I

    .line 73
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const v1, 0x7f0b00bd

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 92
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 94
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 113
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment$2;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/GridLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 123
    return-object v0

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 110
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 232
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;->close()V

    .line 233
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 238
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->cancelTraining()V

    .line 239
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 240
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 241
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 263
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 264
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

    .line 265
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 145
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onPause()V

    .line 146
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onResume()V

    .line 151
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 152
    return-void
.end method

.method public onSpeedDownViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901aa
        }
    .end annotation

    .prologue
    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->speedDown()V

    .line 158
    return-void
.end method

.method public onSpeedUpViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901c8
        }
    .end annotation

    .prologue
    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->speedUp()V

    .line 163
    return-void
.end method

.method public onTrainingCompleted(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 245
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsTrainingCompleteListener;->onWordColumnsTrainingCompleted(I)V

    .line 246
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 128
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 129
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->init(I)V

    .line 130
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;->pauseTraining()V

    .line 135
    return-void
.end method

.method public resumeAnimations()V
    .locals 0

    .prologue
    .line 140
    return-void
.end method

.method public selectItem(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 210
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, -0xff0001

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 211
    return-void
.end method

.method public setItems([Ljava/lang/String;)V
    .locals 3
    .param p1, "items"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemCount:I

    if-ge v0, v1, :cond_0

    .line 204
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    aget-object v2, p1, v0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method public updateSpeedView(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    .line 221
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->speedTextView:Landroid/widget/TextView;

    const v1, 0x7f0e002d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    return-void
.end method

.method public updateTimeView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 226
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->timeTextView:Landroid/widget/TextView;

    const v1, 0x7f0e002e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/WordColumnsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    return-void
.end method
