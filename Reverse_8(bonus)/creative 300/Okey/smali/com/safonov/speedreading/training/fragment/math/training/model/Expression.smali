.class public Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;
.super Ljava/lang/Object;
.source "Expression.java"


# instance fields
.field private correctAnswer:I

.field private incorrectAnswers:[I

.field private value:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I[I)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;
    .param p2, "correctAnswer"    # I
    .param p3, "incorrectAnswers"    # [I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->value:Ljava/lang/String;

    .line 15
    iput p2, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->correctAnswer:I

    .line 16
    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->incorrectAnswers:[I

    .line 17
    return-void
.end method


# virtual methods
.method public getCorrectAnswer()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->correctAnswer:I

    return v0
.end method

.method public getIncorrectAnswers()[I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->incorrectAnswers:[I

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->value:Ljava/lang/String;

    return-object v0
.end method

.method public setCorrectAnswer(I)V
    .locals 0
    .param p1, "correctAnswer"    # I

    .prologue
    .line 32
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->correctAnswer:I

    .line 33
    return-void
.end method

.method public setIncorrectAnswers([I)V
    .locals 0
    .param p1, "incorrectAnswers"    # [I

    .prologue
    .line 40
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->incorrectAnswers:[I

    .line 41
    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 0
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 24
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/training/model/Expression;->value:Ljava/lang/String;

    .line 25
    return-void
.end method
