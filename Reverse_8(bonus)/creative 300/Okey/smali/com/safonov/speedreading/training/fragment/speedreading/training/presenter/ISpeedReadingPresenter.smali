.class public interface abstract Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;
.super Ljava/lang/Object;
.source "ISpeedReadingPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract answerAnimationEnded()V
.end method

.method public abstract answerButtonClick(I)V
.end method

.method public abstract cancelTraining()V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract startTraining(I)V
.end method
