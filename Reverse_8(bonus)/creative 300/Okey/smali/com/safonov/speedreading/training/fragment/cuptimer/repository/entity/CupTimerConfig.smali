.class public Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
.super Lio/realm/RealmObject;
.source "CupTimerConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/CupTimerConfigRealmProxyInterface;


# static fields
.field public static final FIELD_DURATION:Ljava/lang/String; = "duration"

.field public static final FIELD_TYPE:Ljava/lang/String; = "type"


# instance fields
.field private duration:J

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private type:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getDuration()J
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmGet$duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmGet$type()I

    move-result v0

    return v0
.end method

.method public realmGet$duration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->duration:J

    return-wide v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->id:I

    return v0
.end method

.method public realmGet$type()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->type:I

    return v0
.end method

.method public realmSet$duration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->duration:J

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->id:I

    return-void
.end method

.method public realmSet$type(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->type:I

    return-void
.end method

.method public setDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 35
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmSet$duration(J)V

    .line 36
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmSet$id(I)V

    .line 28
    return-void
.end method

.method public setType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->realmSet$type(I)V

    return-void
.end method
