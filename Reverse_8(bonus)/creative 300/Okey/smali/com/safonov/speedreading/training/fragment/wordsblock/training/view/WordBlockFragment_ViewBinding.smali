.class public Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;
.super Ljava/lang/Object;
.source "WordBlockFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

.field private view2131296360:Landroid/view/View;

.field private view2131296531:Landroid/view/View;

.field private view2131296532:Landroid/view/View;

.field private view2131296682:Landroid/view/View;

.field private view2131296712:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

    .line 34
    const v1, 0x7f090022

    const-string v2, "field \'adView\'"

    const-class v3, Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/AdView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 35
    const v1, 0x7f090207

    const-string v2, "field \'timeTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->timeTextView:Landroid/widget/TextView;

    .line 36
    const v1, 0x7f0901c7

    const-string v2, "field \'speedTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->speedTextView:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f090246

    const-string v2, "field \'wordCountTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->wordCountTextView:Landroid/widget/TextView;

    .line 38
    const v1, 0x7f0900ce

    const-string v2, "field \'itemTextView\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->itemTextView:Landroid/widget/TextView;

    .line 39
    const v1, 0x7f090068

    const-string v2, "method \'onContentViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296360:Landroid/view/View;

    .line 41
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v1, 0x7f0901aa

    const-string v2, "method \'onSpeedDownViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 49
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v1, 0x7f0901c8

    const-string v2, "method \'onSpeedUpViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 57
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    const v1, 0x7f090114

    const-string v2, "method \'onPlusViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 64
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296532:Landroid/view/View;

    .line 65
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    const v1, 0x7f090113

    const-string v2, "method \'onMinusViewClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 72
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296531:Landroid/view/View;

    .line 73
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$5;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

    .line 85
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 86
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;

    .line 88
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->timeTextView:Landroid/widget/TextView;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->speedTextView:Landroid/widget/TextView;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->wordCountTextView:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment;->itemTextView:Landroid/widget/TextView;

    .line 94
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296360:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296360:Landroid/view/View;

    .line 96
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296682:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296682:Landroid/view/View;

    .line 98
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296712:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296712:Landroid/view/View;

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296532:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296532:Landroid/view/View;

    .line 102
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296531:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/WordBlockFragment_ViewBinding;->view2131296531:Landroid/view/View;

    .line 104
    return-void
.end method
