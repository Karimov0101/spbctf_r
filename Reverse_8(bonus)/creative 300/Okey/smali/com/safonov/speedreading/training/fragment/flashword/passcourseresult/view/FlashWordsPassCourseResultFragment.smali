.class public Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "FlashWordsPassCourseResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;",
        "Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/IFlashWordsPassCourseResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090081
    .end annotation
.end field

.field private fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

.field nextButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011e
    .end annotation
.end field

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

.field private resultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 48
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;-><init>()V

    .line 49
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 39
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getFlashWordsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    .line 41
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/FlashWordsPassCourseResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V

    return-object v0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 102
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    if-eqz v0, :cond_0

    .line 103
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 108
    return-void

    .line 105
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement CourseResultFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->resultId:I

    .line 61
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const v1, 0x7f0b00bc

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 72
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 74
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 118
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 119
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;->close()V

    .line 120
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 126
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    .line 114
    return-void
.end method

.method public onNextClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09011e
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->nextButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->fragmentCompleteListener:Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/CourseResultFragmentListener;->onCourseResultNextClick()V

    .line 95
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 80
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/presenter/IFlashWordsPassCourseResultPresenter;->initTrainingResults(I)V

    .line 81
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 5
    .param p1, "duration"    # J

    .prologue
    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0024

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/passcourseresult/view/FlashWordsPassCourseResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    return-void
.end method
