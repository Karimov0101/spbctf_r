.class public final enum Lcom/safonov/speedreading/training/FragmentType;
.super Ljava/lang/Enum;
.source "FragmentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/training/FragmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CONCENTRATION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CONCENTRATION_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CONCENTRATION_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CONCENTRATION_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CONCENTRATION_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CUPTIMER:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CUPTIMER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CUPTIMER_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CUPTIMER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum CUPTIMER_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum EVEN_NUMBERS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum EVEN_NUMBERS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum EVEN_NUMBERS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum EVEN_NUMBERS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum FLASH_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum FLASH_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum FLASH_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum FLASH_WORDS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum FLASH_WORDS_TRAINING:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum GREEN_DOT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum GREEN_DOT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum GREEN_DOT_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum GREEN_DOT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum GREEN_DOT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum INTERSTITIAL:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum LINE_OF_SIGHT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum LINE_OF_SIGHT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum LINE_OF_SIGHT_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum LINE_OF_SIGHT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum LINE_OF_SIGHT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum MATHEMATICS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum PASS_COURSE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum PREPARE:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_NUMBER:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_NUMBER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_NUMBER_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_NUMBER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_WORDS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum REMEMBER_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SCHULTE_TABLE:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SCHULTE_TABLE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SCHULTE_TABLE_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SCHULTE_TABLE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SCHULTE_TABLE_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SPEED_READING:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SPEED_READING_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SPEED_READING_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum SPEED_READING_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum TRUE_COLORS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum TRUE_COLORS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum TRUE_COLORS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum TRUE_COLORS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_BLOCK:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_BLOCK_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_BLOCK_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_BLOCK_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_BLOCK_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_COLUMNS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_COLUMNS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_COLUMNS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_COLUMNS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORDS_COLUMNS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORD_PAIRS:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORD_PAIRS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORD_PAIRS_HELP:Lcom/safonov/speedreading/training/FragmentType;

.field public static final enum WORD_PAIRS_RESULT:Lcom/safonov/speedreading/training/FragmentType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 8
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "PREPARE"

    invoke-direct {v0, v1, v3}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    .line 9
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "INTERSTITIAL"

    invoke-direct {v0, v1, v4}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->INTERSTITIAL:Lcom/safonov/speedreading/training/FragmentType;

    .line 11
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "PASS_COURSE_DESCRIPTION"

    invoke-direct {v0, v1, v5}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 12
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "PASS_COURSE_RESULT"

    invoke-direct {v0, v1, v6}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 14
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "ACCELERATOR_COURSE_RESULT"

    invoke-direct {v0, v1, v7}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 16
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_COLUMNS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/FragmentType;

    .line 17
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_COLUMNS_HELP"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 18
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_COLUMNS_DESCRIPTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 19
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_COLUMNS_SETTINGS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 20
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_COLUMNS_RESULT"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 22
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_BLOCK"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/FragmentType;

    .line 23
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_BLOCK_HELP"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 24
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_BLOCK_DESCRIPTION"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 25
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_BLOCK_SETTINGS"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 26
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORDS_BLOCK_RESULT"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 28
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "FLASH_WORDS_TRAINING"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_TRAINING:Lcom/safonov/speedreading/training/FragmentType;

    .line 29
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "FLASH_WORDS_HELP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 30
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "FLASH_WORDS_DESCRIPTION"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 31
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "FLASH_WORDS_SETTINGS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 32
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "FLASH_WORDS_RESULT"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 34
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SCHULTE_TABLE"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/FragmentType;

    .line 35
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SCHULTE_TABLE_HELP"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 36
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SCHULTE_TABLE_DESCRIPTION"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 37
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SCHULTE_TABLE_SETTINGS"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 38
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SCHULTE_TABLE_RESULT"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 40
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_NUMBER"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/FragmentType;

    .line 41
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_NUMBER_HELP"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 42
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_NUMBER_DESCRIPTION"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 43
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_NUMBER_RESULT"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 45
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "LINE_OF_SIGHT"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/FragmentType;

    .line 46
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "LINE_OF_SIGHT_DESCRIPTION"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 47
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "LINE_OF_SIGHT_HELP"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 48
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "LINE_OF_SIGHT_SETTINGS"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 49
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "LINE_OF_SIGHT_RESULT"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 51
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SPEED_READING"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING:Lcom/safonov/speedreading/training/FragmentType;

    .line 52
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SPEED_READING_DESCRIPTION"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SPEED_READING_HELP"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 54
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "SPEED_READING_RESULT"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 56
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORD_PAIRS"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS:Lcom/safonov/speedreading/training/FragmentType;

    .line 57
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORD_PAIRS_HELP"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 58
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORD_PAIRS_DESCRIPTION"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 59
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "WORD_PAIRS_RESULT"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 61
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "EVEN_NUMBERS"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/FragmentType;

    .line 62
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "EVEN_NUMBERS_HELP"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 63
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "EVEN_NUMBERS_DESCRIPTION"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 64
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "EVEN_NUMBERS_RESULT"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 66
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "GREEN_DOT"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT:Lcom/safonov/speedreading/training/FragmentType;

    .line 67
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "GREEN_DOT_HELP"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 68
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "GREEN_DOT_DESCRIPTION"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 69
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "GREEN_DOT_SETTINGS"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 70
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "GREEN_DOT_RESULT"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 72
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS:Lcom/safonov/speedreading/training/FragmentType;

    .line 73
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS_HELP"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 74
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS_DESCRIPTION"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 75
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS_RESULT"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 76
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS_COMPLEXITY"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    .line 77
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "MATHEMATICS_SETTINGS"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 79
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CONCENTRATION"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION:Lcom/safonov/speedreading/training/FragmentType;

    .line 80
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CONCENTRATION_HELP"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 81
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CONCENTRATION_DESCRIPTION"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 82
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CONCENTRATION_RESULT"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 83
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CONCENTRATION_COMPLEXITY"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    .line 85
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CUPTIMER"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER:Lcom/safonov/speedreading/training/FragmentType;

    .line 86
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CUPTIMER_HELP"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 87
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CUPTIMER_DESCRIPTION"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 88
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CUPTIMER_SETTINGS"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    .line 89
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "CUPTIMER_RESULT"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 91
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_WORDS"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/FragmentType;

    .line 92
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_WORDS_HELP"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 93
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_WORDS_DESCRIPTION"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 94
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "REMEMBER_WORDS_RESULT"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 96
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "TRUE_COLORS"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS:Lcom/safonov/speedreading/training/FragmentType;

    .line 97
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "TRUE_COLORS_HELP"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    .line 98
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "TRUE_COLORS_DESCRIPTION"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    .line 99
    new-instance v0, Lcom/safonov/speedreading/training/FragmentType;

    const-string v1, "TRUE_COLORS_RESULT"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/FragmentType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    .line 7
    const/16 v0, 0x4b

    new-array v0, v0, [Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->INTERSTITIAL:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_TRAINING:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x34

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x35

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x36

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x37

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x38

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x39

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3a

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3b

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3c

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3d

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3e

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x3f

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x40

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x41

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x42

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x43

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x44

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x45

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x46

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x47

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x48

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x49

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x4a

    sget-object v2, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/training/FragmentType;->$VALUES:[Lcom/safonov/speedreading/training/FragmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 7
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static isComplexityFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 204
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 207
    :cond_0
    :goto_0
    return v0

    .line 205
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_COMPLEXITY:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 207
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isDescriptionFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 138
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 155
    :cond_0
    :goto_0
    return v0

    .line 139
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 140
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 142
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 143
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 144
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 145
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 146
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 147
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 148
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 149
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 150
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 151
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 152
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 153
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_DESCRIPTION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 155
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHelpFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 103
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 105
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 107
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 108
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 109
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 110
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 111
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 112
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 113
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 114
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 115
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 116
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 117
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 118
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_HELP:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 120
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isResultFragment(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 159
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 179
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 162
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 163
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 164
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 166
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 167
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 168
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 169
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 170
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 171
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 172
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 173
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 174
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 175
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 176
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 177
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 179
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSettingsFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 124
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 134
    :cond_0
    :goto_0
    return v0

    .line 125
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 126
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 128
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 129
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 130
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 131
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 132
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER_SETTINGS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 134
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isTrainingFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z
    .locals 2
    .param p0, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v0, 0x1

    .line 183
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p0, v1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return v0

    .line 184
    :cond_1
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 185
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->FLASH_WORDS_TRAINING:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 187
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 188
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 189
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 190
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->SPEED_READING:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 191
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->WORD_PAIRS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 192
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 193
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->GREEN_DOT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 194
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->MATHEMATICS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 195
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CONCENTRATION:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 196
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->CUPTIMER:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 197
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 198
    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->TRUE_COLORS:Lcom/safonov/speedreading/training/FragmentType;

    if-eq p0, v1, :cond_0

    .line 200
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 7
    const-class v0, Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/training/FragmentType;
    .locals 1

    .prologue
    .line 7
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->$VALUES:[Lcom/safonov/speedreading/training/FragmentType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/training/FragmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/training/FragmentType;

    return-object v0
.end method
