.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordColumnsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/IWordColumnsPresenter;"
    }
.end annotation


# static fields
.field private static final MAX_SPEED:I = 0xbb8

.field private static final MIN_SPEED:I = 0x64

.field private static final SPEED_STEP:I = 0x32


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

.field private currentPlayedTime:J

.field private delay:I

.field private handler:Landroid/os/Handler;

.field private random:Ljava/util/Random;

.field private repository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

.field private selectedIndex:I

.field private speed:I

.field private timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

.field private trainingPaused:Z

.field private wordSelectorRunnable:Ljava/lang/Runnable;

.field private words:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 33
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->random:Ljava/util/Random;

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->handler:Landroid/os/Handler;

    .line 50
    new-instance v0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 175
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->words:[Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    return v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    return v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->selectedIndex:I

    return v0
.end method

.method static synthetic access$502(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 22
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->selectedIndex:I

    return p1
.end method

.method static synthetic access$508(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->selectedIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->selectedIndex:I

    return v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;II)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getItems(II)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->delay:I

    return v0
.end method

.method private getItems(II)[Ljava/lang/String;
    .locals 8
    .param p1, "itemCount"    # I
    .param p2, "wordsPerItem"    # I

    .prologue
    .line 36
    new-array v2, p1, [Ljava/lang/String;

    .line 37
    .local v2, "items":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 38
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->words:[Ljava/lang/String;

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->random:Ljava/util/Random;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->words:[Ljava/lang/String;

    array-length v6, v6

    invoke-virtual {v5, v6}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    aget-object v1, v4, v5

    .line 40
    .local v1, "item":Ljava/lang/String;
    const/4 v3, 0x1

    .local v3, "j":I
    :goto_1
    if-ge v3, p2, :cond_0

    .line 41
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->words:[Ljava/lang/String;

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->random:Ljava/util/Random;

    iget-object v7, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->words:[Ljava/lang/String;

    array-length v7, v7

    invoke-virtual {v6, v7}, Ljava/util/Random;->nextInt(I)I

    move-result v6

    aget-object v5, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 40
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 44
    :cond_0
    aput-object v1, v2, v0

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    .end local v1    # "item":Ljava/lang/String;
    .end local v3    # "j":I
    :cond_1
    return-object v2
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    .line 134
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 135
    return-void
.end method

.method public init(I)V
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    .line 62
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getTrainingDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->currentPlayedTime:J

    .line 64
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getWordsPerItem()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->delay:I

    .line 66
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;I)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 100
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->updateSpeedView(I)V

    .line 102
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getRowCount()I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getColumnCount()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->initGridLayout(II)V

    .line 104
    :cond_0
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->currentPlayedTime:J

    .line 122
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 128
    return-void
.end method

.method public speedDown()V
    .locals 2

    .prologue
    const/16 v1, 0x64

    .line 157
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    add-int/lit8 v0, v0, -0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    .line 158
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    if-ge v0, v1, :cond_0

    .line 159
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    .line 161
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getWordsPerItem()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->delay:I

    .line 163
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->updateSpeedView(I)V

    .line 166
    :cond_1
    return-void
.end method

.method public speedUp()V
    .locals 2

    .prologue
    const/16 v1, 0xbb8

    .line 144
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    add-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    .line 145
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    if-le v0, v1, :cond_0

    .line 146
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    .line 148
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->getWordsPerItem()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->delay:I

    .line 150
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/view/IWordColumnsView;->updateSpeedView(I)V

    .line 153
    :cond_1
    return-void
.end method

.method public startTraining()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 171
    return-void
.end method

.method public switchTrainingPauseStatus()V
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->trainingPaused:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->resumeTraining()V

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/training/presenter/WordColumnsPresenter;->pauseTraining()V

    goto :goto_0
.end method
