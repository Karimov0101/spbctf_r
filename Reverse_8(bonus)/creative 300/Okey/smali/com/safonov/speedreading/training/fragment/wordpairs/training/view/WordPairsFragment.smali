.class public Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "WordPairsFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/IWordPairsView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final ITEMS_ALPHA_ANIMATION_DURATION:I = 0x3e8

.field private static final ITEM_TEXT_SIZE_DP:I = 0x10

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field backgroundWhiteColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06002e
    .end annotation
.end field

.field private configId:I

.field greenColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field gridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090247
    .end annotation
.end field

.field private itemCount:I

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;",
            ">;"
        }
    .end annotation
.end field

.field private itemOnTouchListener:Landroid/view/View$OnTouchListener;

.field private itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014e
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015f
    .end annotation
.end field

.field redColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006d
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090188
    .end annotation
.end field

.field textColorBlack:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field textColorWhite:I
    .annotation build Lbutterknife/BindColor;
        value = 0x106000b
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;

.field private wordPairsModel:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;

.field private wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

.field private wordPairsTrainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 212
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 55
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getItemIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method private getItemIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemCount:I

    if-ge v0, v1, :cond_1

    .line 204
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 209
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 203
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 61
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;-><init>()V

    .line 62
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 64
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/application/App;

    move-object v0, v1

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 48
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getWordPairsRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    .line 49
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsModel:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;

    .line 51
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsModel:Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/WordPairsPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;)V

    return-object v1
.end method

.method public initBoard(II)V
    .locals 7
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I

    .prologue
    const/high16 v6, -0x80000000

    const/high16 v5, 0x3f800000    # 1.0f

    .line 178
    mul-int v2, p1, p2

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemCount:I

    .line 179
    new-instance v2, Ljava/util/ArrayList;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemCount:I

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    .line 181
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2}, Landroid/support/v7/widget/GridLayout;->removeAllViews()V

    .line 182
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, p2}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 183
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 185
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemCount:I

    if-ge v0, v2, :cond_0

    .line 186
    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;-><init>(Landroid/content/Context;)V

    .line 187
    .local v1, "itemTextView":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
    new-instance v2, Landroid/support/v7/widget/GridLayout$LayoutParams;

    .line 188
    invoke-static {v6, v5}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v3

    .line 189
    invoke-static {v6, v5}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/support/v7/widget/GridLayout$LayoutParams;-><init>(Landroid/support/v7/widget/GridLayout$Spec;Landroid/support/v7/widget/GridLayout$Spec;)V

    .line 187
    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 192
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->backgroundWhiteColor:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setBackgroundColor(I)V

    .line 193
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorBlack:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setTextColor(I)V

    .line 195
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 185
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    .end local v1    # "itemTextView":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;->startTraining()V

    .line 200
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 273
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 274
    return-void
.end method

.method public itemTouchDown(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 250
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    .line 251
    .local v0, "itemView":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
    if-eqz p2, :cond_0

    .line 252
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->greenColor:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setBackgroundColor(I)V

    .line 253
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setTextColor(I)V

    .line 258
    :goto_0
    return-void

    .line 255
    :cond_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->redColor:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setBackgroundColor(I)V

    .line 256
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setTextColor(I)V

    goto :goto_0
.end method

.method public itemTouchUp(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 262
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    .line 263
    .local v0, "itemView":Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
    if-eqz p2, :cond_0

    .line 264
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setEnabled(Z)V

    .line 269
    :goto_0
    return-void

    .line 266
    :cond_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->backgroundWhiteColor:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setBackgroundColor(I)V

    .line 267
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorBlack:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setTextColor(I)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 300
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 301
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 302
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsTrainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;

    .line 307
    return-void

    .line 304
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement WordPairsTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->configId:I

    .line 74
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    const v1, 0x7f0b00b9

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 98
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    .line 101
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

    .line 103
    return-object v0

    .line 100
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;->cancelTraining()V

    .line 126
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsRealmUtil:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/WordPairsRealmUtil;->close()V

    .line 127
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 133
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 311
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 312
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsTrainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;

    .line 313
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;->init(I)V

    .line 110
    return-void
.end method

.method public onWordPairsTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->wordPairsTrainingCompleteListener:Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsTrainingCompleteListener;->onWordPairsTrainingCompleted(I)V

    .line 294
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;->pauseTraining()V

    .line 115
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/presenter/IWordPairsPresenter;->resumeTraining()V

    .line 120
    return-void
.end method

.method public setBoardItems(Ljava/util/List;)V
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    .local p1, "itemsData":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemCount:I

    if-ge v0, v2, :cond_0

    .line 231
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;

    .line 232
    .local v1, "wordPair":Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->getFirstWord()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->getSecondWord()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 233
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->backgroundWhiteColor:I

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setBackgroundColor(I)V

    .line 234
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->textColorBlack:I

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setTextColor(I)V

    .line 235
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setEnabled(Z)V

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 238
    .end local v1    # "wordPair":Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    .line 239
    return-void
.end method

.method public setBoardItemsEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 243
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->itemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 244
    .local v0, "itemView":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 246
    .end local v0    # "itemView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 278
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 279
    return-void
.end method

.method public updateRecord(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 288
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e01a7

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    return-void
.end method

.method public updateScore(I)V
    .locals 5
    .param p1, "score"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->scoreTextView:Landroid/widget/TextView;

    const v1, 0x7f0e01ad

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 284
    return-void
.end method
