.class public interface abstract Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;
.super Ljava/lang/Object;
.source "ISchulteTableResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setNewBestTimeViewVisibility(Z)V
.end method

.method public abstract updateBestTimeView(J)V
.end method

.method public abstract updateTimeView(J)V
.end method
