.class public interface abstract Lcom/safonov/speedreading/training/fragment/greendot/result/view/IGreenDotResultView;
.super Ljava/lang/Object;
.source "IGreenDotResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateTrainingDurationView(J)V
.end method
