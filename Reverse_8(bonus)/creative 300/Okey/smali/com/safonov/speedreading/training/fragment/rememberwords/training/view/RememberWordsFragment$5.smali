.class Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;
.super Ljava/lang/Object;
.source "RememberWordsFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    .prologue
    .line 400
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/16 v2, 0x8

    .line 403
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->wordsLayout:Landroid/widget/TableLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TableLayout;->setVisibility(I)V

    .line 404
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->answerLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 405
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->statisticsLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 407
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$500(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment$5;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;->access$1100(Lcom/safonov/speedreading/training/fragment/rememberwords/training/view/RememberWordsFragment;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 408
    return-void
.end method
