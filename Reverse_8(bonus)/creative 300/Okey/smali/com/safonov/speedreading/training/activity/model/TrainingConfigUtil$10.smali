.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository$OnSingleTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetAcceleratorCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 448
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 451
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v3, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    const/4 v4, 0x1

    invoke-static {v2, v3, v5, v4, v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 452
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {v0, v2, p1, v5}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V

    .line 454
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/16 v3, 0x8

    new-instance v4, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v4, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    aput-object v4, v2, v3

    .line 456
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v2, v3}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 457
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$10;->val$trainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 459
    :cond_0
    return-void
.end method
