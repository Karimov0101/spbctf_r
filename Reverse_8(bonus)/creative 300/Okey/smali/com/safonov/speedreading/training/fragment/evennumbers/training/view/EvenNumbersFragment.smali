.class public Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "EvenNumbersFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final ITEMS_ALPHA_ANIMATION_DURATION:I = 0x3e8

.field private static final ITEM_TEXT_SIZE_SP:I = 0x10

.field private static final TRAINING_CONFIG_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field backgroundWhiteColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06002e
    .end annotation
.end field

.field private configId:I

.field greenColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06001a
    .end annotation
.end field

.field gridLayout:Landroid/support/v7/widget/GridLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090089
    .end annotation
.end field

.field private itemCount:I

.field private itemList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private itemOnTouchListener:Landroid/view/View$OnTouchListener;

.field private itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014e
    .end annotation
.end field

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015f
    .end annotation
.end field

.field redColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006d
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090188
    .end annotation
.end field

.field textColorBlack:I
    .annotation build Lbutterknife/BindColor;
        value = 0x1060003
    .end annotation
.end field

.field textColorWhite:I
    .annotation build Lbutterknife/BindColor;
        value = 0x106000b
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 138
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 50
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;Landroid/view/View;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getItemIndex(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method private getItemIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 155
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemCount:I

    if-ge v0, v1, :cond_1

    .line 156
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_0

    .line 161
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 155
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 161
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;
    .locals 3
    .param p0, "evenNumbersConfigId"    # I

    .prologue
    .line 56
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;-><init>()V

    .line 57
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->setArguments(Landroid/os/Bundle;)V

    .line 60
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 45
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getEvenNumbersRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 46
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/EvenNumbersModel;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/EvenNumbersModel;-><init>()V

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-direct {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/EvenNumbersPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V

    return-object v1
.end method

.method public initBoard(II)V
    .locals 7
    .param p1, "rowCount"    # I
    .param p2, "columnCount"    # I

    .prologue
    const/high16 v6, -0x80000000

    const/high16 v5, 0x3f800000    # 1.0f

    .line 113
    mul-int v2, p2, p1

    iput v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemCount:I

    .line 114
    new-instance v2, Ljava/util/ArrayList;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemCount:I

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    .line 116
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2}, Landroid/support/v7/widget/GridLayout;->removeAllViews()V

    .line 117
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, p1}, Landroid/support/v7/widget/GridLayout;->setRowCount(I)V

    .line 118
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, p2}, Landroid/support/v7/widget/GridLayout;->setColumnCount(I)V

    .line 120
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemCount:I

    if-ge v0, v2, :cond_0

    .line 121
    new-instance v1, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 122
    .local v1, "itemTextView":Landroid/widget/TextView;
    new-instance v2, Landroid/support/v7/widget/GridLayout$LayoutParams;

    .line 123
    invoke-static {v6, v5}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v3

    .line 124
    invoke-static {v6, v5}, Landroid/support/v7/widget/GridLayout;->spec(IF)Landroid/support/v7/widget/GridLayout$Spec;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/support/v7/widget/GridLayout$LayoutParams;-><init>(Landroid/support/v7/widget/GridLayout$Spec;Landroid/support/v7/widget/GridLayout$Spec;)V

    .line 122
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 126
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 128
    const/4 v2, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 129
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->backgroundWhiteColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 130
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->textColorBlack:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 132
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/GridLayout;->addView(Landroid/view/View;)V

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    .end local v1    # "itemTextView":Landroid/widget/TextView;
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;->startTraining()V

    .line 136
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 208
    return-void
.end method

.method public itemTouchDown(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 184
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 185
    .local v0, "itemTextView":Landroid/widget/TextView;
    if-eqz p2, :cond_0

    .line 186
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->greenColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 187
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->redColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 190
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->textColorWhite:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public itemTouchUp(IZ)V
    .locals 2
    .param p1, "itemIndex"    # I
    .param p2, "isTrueAnswer"    # Z

    .prologue
    .line 196
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 197
    .local v0, "itemTextView":Landroid/widget/TextView;
    if-eqz p2, :cond_0

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 203
    :goto_0
    return-void

    .line 200
    :cond_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->backgroundWhiteColor:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 201
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->textColorBlack:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 257
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 258
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 259
    check-cast p1, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

    .line 264
    return-void

    .line 261
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement EvenNumbersTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->configId:I

    .line 69
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 91
    const v1, 0x7f0b0036

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->unbinder:Lbutterknife/Unbinder;

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->gridLayout:Landroid/support/v7/widget/GridLayout;

    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    const-wide/16 v2, 0x3e8

    .line 96
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

    .line 98
    return-object v0

    .line 95
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 237
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 238
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;->cancelTraining()V

    .line 239
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->close()V

    .line 240
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 245
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 246
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 268
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

    .line 270
    return-void
.end method

.method public onEvenNumbersTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;->onEvenNumbersTrainingCompleted(I)V

    .line 251
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;->init(I)V

    .line 105
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;->pauseTraining()V

    .line 228
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/presenter/IEvenNumbersPresenter;->resumeTraining()V

    .line 233
    return-void
.end method

.method public setBoardItems(Ljava/util/List;)V
    .locals 3
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p1, "itemsData":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemCount:I

    if-ge v0, v1, :cond_0

    .line 167
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;->getNumber()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 168
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->backgroundWhiteColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 169
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->textColorBlack:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 170
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 166
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemsAlphaAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 173
    return-void
.end method

.method public setBoardItemsEnable(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 177
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->itemList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 178
    .local v0, "itemView":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 180
    .end local v0    # "itemView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 212
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 213
    return-void
.end method

.method public updateRecord(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 222
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0064

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 223
    return-void
.end method

.method public updateScore(I)V
    .locals 5
    .param p1, "score"    # I

    .prologue
    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->scoreTextView:Landroid/widget/TextView;

    const v1, 0x7f0e006a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    return-void
.end method
