.class Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;
.super Ljava/lang/Object;
.source "RememberNumberPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 172
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;Z)Z

    .line 173
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 203
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->getTrainingShowCount()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 178
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;-><init>()V

    .line 179
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->setScore(I)V

    .line 180
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->setUnixTime(J)V

    .line 182
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v3

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->updateConfigComplexity(II)V

    .line 183
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$700(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v2

    new-instance v3, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository;->addResult(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ILcom/safonov/speedreading/training/fragment/remembernumber/repository/IRememberNumberRepository$OnSingleTransactionCallback;)V

    .line 191
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$102(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;Z)Z

    goto :goto_0

    .line 195
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    :cond_1
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$600(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)I

    move-result v2

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardViews(I)V

    .line 197
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultBackground()V

    .line 198
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultTextColor()V

    .line 199
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;

    invoke-interface {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;->setCardsDefaultValues()V

    .line 202
    :cond_2
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$900(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;->access$800(Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/RememberNumberPresenter;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x2ee

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method
