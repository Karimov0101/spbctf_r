.class public Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "CupTimerResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/ICupTimerResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field private cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09006d
    .end annotation
.end field

.field durationTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0089
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09006f
    .end annotation
.end field

.field private resultId:I

.field private timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 103
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 59
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;)Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 65
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;-><init>()V

    .line 66
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 69
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 54
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getCupTimerRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .line 55
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/CupTimerResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->resultId:I

    .line 78
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 90
    const v1, 0x7f0b0026

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 92
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 94
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 166
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 167
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 169
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->close()V

    .line 170
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .line 172
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 173
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 180
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 99
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/presenter/ICupTimerResultPresenter;->initTrainingResults(I)V

    .line 101
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 120
    .local p1, "greenDotResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 122
    .local v2, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v10

    if-ge v6, v10, :cond_0

    .line 123
    new-instance v11, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v12, v6

    move-object/from16 v0, p1

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v10}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->getConfig()Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v10

    invoke-virtual {v10}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v14

    const-wide/16 v16, 0x2

    mul-long v14, v14, v16

    long-to-float v10, v14

    invoke-direct {v11, v12, v10}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 126
    :cond_0
    new-instance v4, Lcom/github/mikephil/charting/data/LineDataSet;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->durationTitle:Ljava/lang/String;

    invoke-direct {v4, v2, v10}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 127
    .local v4, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->chartLineColor:I

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 128
    sget-object v10, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 129
    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->chartLineWidthDp:I

    int-to-float v10, v10

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 130
    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v10, v10

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 131
    new-instance v10, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment$1;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;)V

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 137
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 138
    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->chartItemColor:I

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 139
    move-object/from16 v0, p0

    iget v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v10, v10

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 140
    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 142
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 143
    .local v5, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v3, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v3, v5}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 147
    .local v3, "data":Lcom/github/mikephil/charting/data/LineData;
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v9

    .line 148
    .local v9, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v10, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v9, v10}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 149
    const/high16 v10, 0x3f800000    # 1.0f

    invoke-virtual {v9, v10}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 151
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v7

    .line 152
    .local v7, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 153
    const/4 v10, 0x1

    invoke-virtual {v7, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 155
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v8

    .line 156
    .local v8, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 157
    const/4 v10, 0x1

    invoke-virtual {v8, v10}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 159
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 160
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10, v3}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 161
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v10}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 162
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 3
    .param p1, "duration"    # J

    .prologue
    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->durationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/result/view/CupTimerResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-virtual {v1, p1, p2}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    return-void
.end method
