.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnSingleTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetSchulteTableConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$isFullscreen:Z

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;ZLcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 550
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-boolean p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->val$isFullscreen:Z

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x0

    .line 553
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    iget-boolean v3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->val$isFullscreen:Z

    invoke-direct {v0, v2, p1, v3}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V

    .line 554
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v3, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    const/4 v4, 0x1

    invoke-static {v2, v3, v4, v5, v5}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 556
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    if-eqz v2, :cond_0

    .line 557
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$14;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    new-instance v3, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v3, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;->onConfigResponse(Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 559
    :cond_0
    return-void
.end method
