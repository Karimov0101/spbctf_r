.class public Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment_ViewBinding;
.super Ljava/lang/Object;
.source "GreenDotFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .line 22
    const v2, 0x7f0900a5

    const-string v3, "field \'greenDotTextView\'"

    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    .line 24
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 25
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 26
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotText:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .line 33
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 34
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;

    .line 36
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotFragment;->greenDotTextView:Lcom/safonov/speedreading/training/fragment/greendot/training/view/GreenDotTextView;

    .line 37
    return-void
.end method
