.class public Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;
.super Ljava/lang/Object;
.source "InterstitialFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

.field private view2131296442:Landroid/view/View;

.field private view2131296560:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    .line 31
    const v1, 0x7f090160

    const-string v2, "field \'recyclerView\'"

    const-class v3, Landroid/support/v7/widget/RecyclerView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 32
    const v1, 0x7f090223

    const-string v2, "field \'pager\'"

    const-class v3, Landroid/widget/LinearLayout;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->pager:Landroid/widget/LinearLayout;

    .line 33
    const v1, 0x7f09012f

    const-string v2, "field \'progressBar\'"

    const-class v3, Landroid/widget/ProgressBar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 34
    const v1, 0x7f090133

    const-string v2, "field \'trainersLeft\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainersLeft:Landroid/widget/TextView;

    .line 35
    const v1, 0x7f0900bb

    const-string v2, "field \'topIv\'"

    const-class v3, Landroid/widget/ImageView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->topIv:Landroid/widget/ImageView;

    .line 36
    const v1, 0x7f0900bc

    const-string v2, "field \'titleTv\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->titleTv:Landroid/widget/TextView;

    .line 37
    const v1, 0x7f0900ba

    const-string v2, "method \'onStartButtonClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 38
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296442:Landroid/view/View;

    .line 39
    new-instance v1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    const v1, 0x7f090130

    const-string v2, "method \'onRestartClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 46
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296560:Landroid/view/View;

    .line 47
    new-instance v1, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 53
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 58
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    .line 59
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 60
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;

    .line 62
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    .line 63
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->pager:Landroid/widget/LinearLayout;

    .line 64
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->progressBar:Landroid/widget/ProgressBar;

    .line 65
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->trainersLeft:Landroid/widget/TextView;

    .line 66
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->topIv:Landroid/widget/ImageView;

    .line 67
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment;->titleTv:Landroid/widget/TextView;

    .line 69
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296442:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296442:Landroid/view/View;

    .line 71
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296560:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/interstitial/view/InterstitialFragment_ViewBinding;->view2131296560:Landroid/view/View;

    .line 73
    return-void
.end method
