.class public abstract Lcom/safonov/speedreading/training/BaseTraining;
.super Ljava/lang/Object;
.source "BaseTraining.java"


# instance fields
.field public final descriptionView:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field public final settingsView:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation

    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field

.field public final title:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public constructor <init>(III)V
    .locals 0
    .param p1, "title"    # I
    .param p2, "descriptionView"    # I
    .param p3, "settingsView"    # I

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput p1, p0, Lcom/safonov/speedreading/training/BaseTraining;->title:I

    .line 20
    iput p2, p0, Lcom/safonov/speedreading/training/BaseTraining;->descriptionView:I

    .line 21
    iput p3, p0, Lcom/safonov/speedreading/training/BaseTraining;->settingsView:I

    .line 22
    return-void
.end method


# virtual methods
.method public getDescriptionView()I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/safonov/speedreading/training/BaseTraining;->descriptionView:I

    return v0
.end method

.method public getSettingsView()I
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 34
    iget v0, p0, Lcom/safonov/speedreading/training/BaseTraining;->settingsView:I

    return v0
.end method

.method public getTitle()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/training/BaseTraining;->title:I

    return v0
.end method
