.class public Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "SpeedReadingPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/ISpeedReadingPresenter;"
    }
.end annotation


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

.field private configId:I

.field private currentSpeed:I

.field private currentTrainingSpeeds:[I

.field private handler:Landroid/os/Handler;

.field private isPaused:Z

.field private isShowingWords:Z

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private maxSpeed:I

.field private model:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;

.field private random:Ljava/util/Random;

.field private repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

.field private showWord:Ljava/lang/Runnable;

.field private trainingShowingCount:I

.field private trueAnswerIndex:I

.field private viewIndex:I

.field private wordIndex:I

.field private wordShowingTime:I

.field private wordsCount:I


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;)V
    .locals 1
    .param p1, "model"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 36
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->handler:Landroid/os/Handler;

    .line 77
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->random:Ljava/util/Random;

    .line 130
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWord:Ljava/lang/Runnable;

    .line 32
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->model:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;

    .line 33
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isPaused:Z

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordIndex:I

    return v0
.end method

.method static synthetic access$102(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordIndex:I

    return p1
.end method

.method static synthetic access$108(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordIndex:I

    return v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    return p1
.end method

.method static synthetic access$308(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->items:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordShowingTime:I

    return v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showAnswer()V

    return-void
.end method

.method private getAverageSpeed([I)I
    .locals 4
    .param p1, "speeds"    # [I

    .prologue
    .line 232
    const/4 v0, 0x0

    .line 233
    .local v0, "result":I
    array-length v3, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_0

    aget v1, p1, v2

    .line 234
    .local v1, "speed":I
    add-int/2addr v0, v1

    .line 233
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 237
    .end local v1    # "speed":I
    :cond_0
    array-length v2, p1

    div-int v2, v0, v2

    return v2
.end method

.method private showAnswer()V
    .locals 10

    .prologue
    const/4 v9, 0x6

    const/4 v6, 0x0

    .line 100
    iput-boolean v6, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isShowingWords:Z

    .line 102
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isViewAttached()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->hideWordsView()V

    .line 104
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v5}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->showAnswerView()V

    .line 105
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    const/4 v7, 0x1

    invoke-interface {v5, v7}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setAnswerButtonsEnabled(Z)V

    .line 107
    iget v5, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    add-int/lit8 v1, v5, -0x1

    .line 108
    .local v1, "falseAnswersCount":I
    add-int/lit8 v5, v1, -0x1

    const/4 v7, 0x5

    invoke-static {v5, v7}, Lcom/safonov/speedreading/training/fragment/speedreading/util/IndexesUtil;->generateUniqueIndexes(II)[I

    move-result-object v2

    .line 112
    .local v2, "falseAnswersIndexes":[I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 113
    .local v0, "answers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->items:Ljava/util/List;

    iget v7, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v5, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    array-length v7, v2

    move v5, v6

    :goto_0
    if-ge v5, v7, :cond_0

    aget v3, v2, v5

    .line 116
    .local v3, "index":I
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->items:Ljava/util/List;

    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 115
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 119
    .end local v3    # "index":I
    :cond_0
    new-instance v4, Ljava/util/Random;

    invoke-direct {v4}, Ljava/util/Random;-><init>()V

    .line 120
    .local v4, "random":Ljava/util/Random;
    invoke-virtual {v4, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v5

    iput v5, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trueAnswerIndex:I

    .line 122
    iget v5, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trueAnswerIndex:I

    invoke-static {v0, v6, v5}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 124
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget v6, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->updateAnswerSpeed(I)V

    .line 125
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v5, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setAnswerItems(Ljava/util/List;)V

    .line 127
    .end local v0    # "answers":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "falseAnswersCount":I
    .end local v2    # "falseAnswersIndexes":[I
    .end local v4    # "random":Ljava/util/Random;
    :cond_1
    return-void
.end method

.method private showWords()V
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isShowingWords:Z

    .line 83
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->showWordsView()V

    .line 84
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->hideAnswerView()V

    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trainingShowingCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->updateProgressBar(I)V

    .line 87
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->updateSpeed(I)V

    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentTrainingSpeeds:[I

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trainingShowingCount:I

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    aput v2, v0, v1

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->random:Ljava/util/Random;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMaxWordShowCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    .line 93
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinWordShowCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->model:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordsCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;->createItems(I)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->items:Ljava/util/List;

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWord:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    return-void
.end method


# virtual methods
.method public answerAnimationEnded()V
    .locals 4

    .prologue
    .line 242
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trainingShowingCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trainingShowingCount:I

    .line 243
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trainingShowingCount:I

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getTrainingShowCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 245
    new-instance v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;-><init>()V

    .line 246
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->maxSpeed:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->setMaxSpeed(I)V

    .line 247
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentTrainingSpeeds:[I

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getAverageSpeed([I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->setAverageSpeed(I)V

    .line 248
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;->setUnixTime(J)V

    .line 250
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->configId:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->updateConfigSpeed(II)V

    .line 251
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->addResult(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;ILcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnSingleTransactionCallback;)V

    .line 262
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWords()V

    goto :goto_0
.end method

.method public answerButtonClick(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 211
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 212
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->trueAnswerIndex:I

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    .line 213
    .local v0, "isTrueAnswer":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 214
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getSpeedStep()I

    move-result v3

    add-int/2addr v1, v3

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    .line 215
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->maxSpeed:I

    if-le v1, v3, :cond_0

    .line 216
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->maxSpeed:I

    .line 224
    :cond_0
    :goto_1
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordShowingTime:I

    .line 226
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setAnswerButtonsEnabled(Z)V

    .line 227
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-interface {v1, v2, v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->updateAnswerSpeedWithAnimation(IZ)V

    .line 229
    .end local v0    # "isTrueAnswer":Z
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 212
    goto :goto_0

    .line 219
    .restart local v0    # "isTrueAnswer":Z
    :cond_3
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getSpeedStep()I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    .line 220
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinSpeed()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 221
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getMinSpeed()I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    goto :goto_1
.end method

.method public cancelTraining()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 175
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isPaused:Z

    .line 176
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWord:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 178
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->items:Ljava/util/List;

    .line 179
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->handler:Landroid/os/Handler;

    .line 181
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    .line 182
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->model:Lcom/safonov/speedreading/training/fragment/speedreading/training/model/ISpeedReadingModel;

    .line 183
    return-void
.end method

.method public pauseTraining()V
    .locals 1

    .prologue
    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isPaused:Z

    .line 188
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    iput-boolean v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isPaused:Z

    .line 194
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->isShowingWords:Z

    if-eqz v0, :cond_1

    .line 195
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    .line 196
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    if-gez v0, :cond_0

    .line 197
    const/16 v0, 0x8

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->setWordItem(ILjava/lang/String;)V

    .line 202
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordIndex:I

    .line 203
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->viewIndex:I

    .line 205
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWords()V

    .line 207
    :cond_1
    return-void
.end method

.method public startTraining(I)V
    .locals 2
    .param p1, "configId"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->configId:I

    .line 59
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->repository:Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->showWordsView()V

    .line 63
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getTrainingShowCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->initProgressBar(I)V

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/training/view/ISpeedReadingView;->updateProgressBar(I)V

    .line 66
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    .line 67
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->maxSpeed:I

    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->config:Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->getTrainingShowCount()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentTrainingSpeeds:[I

    .line 71
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->currentSpeed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->wordShowingTime:I

    .line 73
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/training/presenter/SpeedReadingPresenter;->showWords()V

    .line 74
    return-void
.end method
