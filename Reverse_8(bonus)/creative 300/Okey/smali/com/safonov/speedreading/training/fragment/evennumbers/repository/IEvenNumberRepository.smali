.class public interface abstract Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;
.super Ljava/lang/Object;
.source "IEvenNumberRepository.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;,
        Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;
    }
.end annotation


# virtual methods
.method public abstract addOrFindConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnMultiTransactionCallback;)V
    .param p1    # [Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract addResult(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ILcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
    .param p1    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract getBestResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfig(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getConfigList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getResult(I)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end method

.method public abstract getResultList(I)Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            ">;"
        }
    .end annotation
.end method
