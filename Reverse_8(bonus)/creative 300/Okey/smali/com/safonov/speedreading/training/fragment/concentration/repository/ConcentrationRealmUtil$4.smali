.class Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;
.super Ljava/lang/Object;
.source "ConcentrationRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ILcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;ILcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    .prologue
    .line 126
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->this$0:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$result:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 129
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$configId:I

    .line 130
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 131
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 133
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$result:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->setId(I)V

    .line 134
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$result:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;->setConfig(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    .line 136
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil$4;->val$result:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 137
    return-void
.end method
