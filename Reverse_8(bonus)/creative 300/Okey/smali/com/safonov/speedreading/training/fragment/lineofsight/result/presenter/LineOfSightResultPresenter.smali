.class public Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "LineOfSightResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;"
    }
.end annotation


# instance fields
.field private final repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 4
    .param p1, "resultId"    # I

    .prologue
    .line 27
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    invoke-interface {v2, p1}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v0

    .line 28
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getConfig()Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;->getId()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;->getResultList(I)Ljava/util/List;

    move-result-object v1

    .line 30
    .local v1, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;>;"
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->isViewAttached()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;->updateMistakesView(I)V

    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakeCount()I

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;->updateFoundMistakesView(I)V

    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakesAccuracy()F

    move-result v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;->updateFoundMistakesPercentView(F)V

    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;

    invoke-static {v1}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;->setChartViewData(Ljava/util/List;)V

    .line 36
    :cond_0
    return-void
.end method
