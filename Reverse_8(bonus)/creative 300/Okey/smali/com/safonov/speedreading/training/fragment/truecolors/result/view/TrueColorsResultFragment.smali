.class public Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "TrueColorsResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/result/view/ITrueColorsResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090232
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090238
    .end annotation
.end field

.field newBestScoreView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090234
    .end annotation
.end field

.field private resultId:I

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09023b
    .end annotation
.end field

.field scoreTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0150
    .end annotation
.end field

.field private trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 59
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 65
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;-><init>()V

    .line 66
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 67
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 69
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 53
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getTrueColorsRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .line 55
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/TrueColorsResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 74
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->resultId:I

    .line 78
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 93
    const v1, 0x7f0b00b4

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 95
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 97
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 178
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->close()V

    .line 179
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .line 181
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 182
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 186
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 187
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 188
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 189
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/result/presenter/ITrueColorsResultPresenter;->initTrainingResults(I)V

    .line 104
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "wordPairsResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;>;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 133
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 134
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;->getScore()I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 133
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 137
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->scoreTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 138
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 139
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 140
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 141
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 142
    new-instance v8, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 148
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 149
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 150
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 151
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 153
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 154
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 158
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 159
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 160
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 162
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 163
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v5, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 164
    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 166
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 167
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v6, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 168
    invoke-virtual {v6, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 170
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 171
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 172
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 173
    return-void
.end method

.method public setNewBestScoreViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 118
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 119
    return-void

    .line 118
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/result/view/TrueColorsResultFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method
