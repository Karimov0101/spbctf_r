.class public Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "FlashWordsPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;"
    }
.end annotation


# static fields
.field private static final BLINK_DURATION:I = 0xc8

.field private static final MAX_SPEED:I = 0xbb8

.field private static final MIN_SPEED:I = 0x64

.field private static final SPEED_STEP:I = 0x32

.field private static final WORD_COUNT:I = 0x4


# instance fields
.field private boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

.field private bordPartIndex:I

.field private config:Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

.field private currentPlayedTime:J

.field private defaultItemsRunnable:Ljava/lang/Runnable;

.field private delay:I

.field private handler:Landroid/os/Handler;

.field private itemsRunnable:Ljava/lang/Runnable;

.field private random:Ljava/util/Random;

.field private repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

.field private speed:I

.field private timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

.field private trainingPaused:Z

.field private words:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 36
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->random:Ljava/util/Random;

    .line 47
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->handler:Landroid/os/Handler;

    .line 48
    new-instance v0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 270
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->defaultItemsRunnable:Ljava/lang/Runnable;

    .line 284
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$3;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->itemsRunnable:Ljava/lang/Runnable;

    .line 32
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->words:[Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    .line 34
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    return v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->defaultItemsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->itemsRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->bordPartIndex:I

    return v0
.end method

.method static synthetic access$602(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->bordPartIndex:I

    return p1
.end method

.method static synthetic access$608(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->bordPartIndex:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->bordPartIndex:I

    return v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getItemTextList(I)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    return-object v0
.end method

.method static synthetic access$900(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    .prologue
    .line 25
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->delay:I

    return v0
.end method

.method private changeLevel(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)V
    .locals 2
    .param p1, "boardType"    # Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .prologue
    .line 224
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->bordPartIndex:I

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->currentPlayedTime:J

    .line 228
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->itemsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 229
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->defaultItemsRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 231
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 232
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateLevelView(I)V

    .line 233
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->initBoardView(I)V

    .line 235
    :cond_0
    return-void
.end method

.method private getItemTextList(I)Ljava/util/List;
    .locals 5
    .param p1, "itemCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p1}, Ljava/util/ArrayList;-><init>(I)V

    .line 40
    .local v1, "itemTextList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 41
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->words:[Ljava/lang/String;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->random:Ljava/util/Random;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->words:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 44
    :cond_0
    return-object v1
.end method


# virtual methods
.method public cancelTraining()V
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 146
    return-void
.end method

.method public init(I)V
    .locals 2
    .param p1, "configId"    # I

    .prologue
    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->repository:Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->config:Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .line 63
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->config:Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    .line 64
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->config:Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getTrainingDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->currentPlayedTime:J

    .line 65
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->config:Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getBoarType()I

    move-result v0

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->crete(I)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 67
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getBoardPartCount()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->delay:I

    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;I)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 104
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateSpeedView(I)V

    .line 106
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateLevelView(I)V

    .line 108
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getType()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->initBoardView(I)V

    .line 110
    :cond_0
    return-void
.end method

.method public levelDown()V
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getPrevious(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getPrevious(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->changeLevel(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)V

    .line 221
    :cond_0
    return-void
.end method

.method public levelUp()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getNext(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getNext(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    .line 210
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->changeLevel(Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;)V

    .line 212
    :cond_0
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->currentPlayedTime:J

    .line 134
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 138
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    .line 139
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 140
    return-void
.end method

.method public speedDown()V
    .locals 2

    .prologue
    const/16 v1, 0x64

    .line 257
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    add-int/lit8 v0, v0, -0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    .line 258
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    if-ge v0, v1, :cond_0

    .line 259
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    .line 261
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getBoardPartCount()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->delay:I

    .line 263
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateSpeedView(I)V

    .line 266
    :cond_1
    return-void
.end method

.method public speedUp()V
    .locals 2

    .prologue
    const/16 v1, 0xbb8

    .line 244
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    add-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    .line 245
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    if-le v0, v1, :cond_0

    .line 246
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    .line 248
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->boardTypeWrapper:Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter$BoardType;->getBoardPartCount()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->delay:I

    .line 250
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 251
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;->updateSpeedView(I)V

    .line 253
    :cond_1
    return-void
.end method

.method public startTraining()V
    .locals 4

    .prologue
    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    .line 127
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 128
    return-void
.end method

.method public switchTrainingPause()V
    .locals 1

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->trainingPaused:Z

    if-eqz v0, :cond_0

    .line 118
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->resumeTraining()V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;->pauseTraining()V

    goto :goto_0
.end method
