.class public Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;
.super Ljava/lang/Object;
.source "MathComplexityFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

.field private view2131296495:Landroid/view/View;

.field private view2131296496:Landroid/view/View;

.field private view2131296497:Landroid/view/View;

.field private view2131296498:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;Landroid/view/View;)V
    .locals 6
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v5, 0x7f0900f1

    const v4, 0x7f0900f0

    const v3, 0x7f0900ef

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

    .line 31
    const-string v1, "field \'complexityButton1\' and method \'setComplexityFirst\'"

    invoke-static {p2, v3, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'complexityButton1\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v0, v3, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton1:Landroid/widget/TextView;

    .line 33
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296495:Landroid/view/View;

    .line 34
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const-string v1, "field \'complexityButton2\' and method \'setComplexitySecond\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 41
    const-string v1, "field \'complexityButton2\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton2:Landroid/widget/TextView;

    .line 42
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296496:Landroid/view/View;

    .line 43
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 49
    const-string v1, "field \'complexityButton3\' and method \'setComplexityThird\'"

    invoke-static {p2, v5, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 50
    const-string v1, "field \'complexityButton3\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v0, v5, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton3:Landroid/widget/TextView;

    .line 51
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296497:Landroid/view/View;

    .line 52
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v1, 0x7f0900f2

    const-string v2, "method \'setComplexityFourth\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 59
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296498:Landroid/view/View;

    .line 60
    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

    .line 72
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 73
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;

    .line 75
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton1:Landroid/widget/TextView;

    .line 76
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton2:Landroid/widget/TextView;

    .line 77
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment;->complexityButton3:Landroid/widget/TextView;

    .line 79
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296495:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 80
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296495:Landroid/view/View;

    .line 81
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296496:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296496:Landroid/view/View;

    .line 83
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296497:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 84
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296497:Landroid/view/View;

    .line 85
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296498:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/complexity/view/MathComplexityFragment_ViewBinding;->view2131296498:Landroid/view/View;

    .line 87
    return-void
.end method
