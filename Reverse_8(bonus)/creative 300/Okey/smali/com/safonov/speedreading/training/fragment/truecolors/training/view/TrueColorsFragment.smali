.class public Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "TrueColorsFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field public static final COUNT_DOWN_INTERVAL:I = 0xa

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field answerButtons:Ljava/util/List;
    .annotation build Lbutterknife/BindViews;
        value = {
            0x7f09022e,
            0x7f09022f,
            0x7f090230,
            0x7f090231
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field answerImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d1
    .end annotation
.end field

.field private configId:I

.field private currentProgress:J

.field private mCountDownTimer:Landroid/os/CountDownTimer;

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090235
    .end annotation
.end field

.field questionTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090236
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090237
    .end annotation
.end field

.field rightAnswer:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090239
    .end annotation
.end field

.field timerBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09023d
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

.field private trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 54
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    .prologue
    .line 40
    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->currentProgress:J

    return-wide v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    .param p1, "x1"    # J

    .prologue
    .line 40
    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->currentProgress:J

    return-wide p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    .locals 5
    .param p0, "configId"    # I

    .prologue
    .line 60
    const-string v2, "configId"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;-><init>()V

    .line 62
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 64
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 49
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getTrueColorsRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .line 50
    new-instance v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/repository/ITrueColorsRepository;)V

    return-object v1
.end method

.method public disableButtons()V
    .locals 3

    .prologue
    .line 210
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 211
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_0
    return-void
.end method

.method public enableButtons()V
    .locals 3

    .prologue
    .line 217
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 218
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 217
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_0
    return-void
.end method

.method public getColors()Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 178
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f030021

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 179
    .local v4, "ta":Landroid/content/res/TypedArray;
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    new-array v1, v5, [I

    .line 180
    .local v1, "colors":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->length()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 181
    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    aput v5, v1, v2

    .line 180
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 183
    :cond_0
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 185
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f030022

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 187
    .local v0, "colorNames":[Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 189
    .local v3, "result":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;>;"
    const/4 v2, 0x0

    :goto_1
    array-length v5, v0

    if-ge v2, v5, :cond_1

    .line 190
    new-instance v5, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    aget-object v6, v0, v2

    aget v7, v1, v2

    invoke-direct {v5, v6, v7}, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 189
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 193
    :cond_1
    return-object v3
.end method

.method public getProgress()J
    .locals 2

    .prologue
    .line 173
    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->currentProgress:J

    return-wide v0
.end method

.method public hideAnswerImage()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 132
    return-void
.end method

.method public initCountDownTimer(J)V
    .locals 9
    .param p1, "milliseconds"    # J

    .prologue
    .line 144
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->timerBar:Landroid/widget/ProgressBar;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->timerBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 146
    new-instance v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;JJJ)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 159
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "maxProgress"    # I

    .prologue
    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 108
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 296
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 297
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 298
    check-cast p1, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

    .line 303
    return-void

    .line 300
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement RememberNumberTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->configId:I

    .line 74
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    const v1, 0x7f0b00b3

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 85
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 87
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 267
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;->close()V

    .line 268
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trueColorsRealmUtil:Lcom/safonov/speedreading/training/fragment/truecolors/repository/TrueColorsRealmUtil;

    .line 270
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->cancelTraining()V

    .line 271
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 273
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 275
    :cond_0
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 279
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 280
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 281
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 283
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 284
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 285
    :cond_0
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 309
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

    .line 312
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 314
    :cond_0
    return-void
.end method

.method public onNumberButtonClick(Landroid/widget/TextView;)V
    .locals 3
    .param p1, "button"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09022e,
            0x7f09022f,
            0x7f090230,
            0x7f090231
        }
    .end annotation

    .prologue
    .line 243
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->rightAnswer:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->onNumberButtonPressed(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/lang/String;)V

    .line 244
    return-void
.end method

.method public onTrueColorsTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 291
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsTrainingCompleteListener;->onTrueColorsTrainingCompleted(I)V

    .line 292
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 249
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 250
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->startTraining(I)V

    .line 251
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->pauseTraining()V

    .line 256
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->resumeTraining()V

    .line 261
    return-void
.end method

.method public setAnswerImage(Z)V
    .locals 2
    .param p1, "isCorrect"    # Z

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    const v1, 0x7f08007e

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    :goto_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 127
    return-void

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerImageView:Landroid/widget/ImageView;

    const v1, 0x7f08007d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public showLevel(Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;Ljava/util/List;)V
    .locals 5
    .param p1, "correctAnswer"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 224
    .local p2, "incorrectAnswers":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;>;"
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    .line 225
    .local v1, "random":Ljava/util/Random;
    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    add-int/lit8 v2, v3, 0x1

    .line 227
    .local v2, "randomColorIndex":I
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->questionTV:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->getColorName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    iput-object v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->rightAnswer:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    .line 229
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->questionTV:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->rightAnswer:Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->getColor()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 231
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 232
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->answerButtons:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;

    invoke-virtual {v4}, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->getColorName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_0
    return-void
.end method

.method public timerPause()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 164
    return-void
.end method

.method public timerStart()V
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->mCountDownTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 169
    return-void
.end method

.method public updateBestScoreView(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e014b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 103
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 113
    return-void
.end method

.method public updateScoreView(I)V
    .locals 5
    .param p1, "score"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->scoreTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0151

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 98
    return-void
.end method
