.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "WordColumnsSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "rootKey"    # Ljava/lang/String;

    .prologue
    .line 20
    const v0, 0x7f110008

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/settings/WordColumnsSettingsFragment;->addPreferencesFromResource(I)V

    .line 21
    return-void
.end method
