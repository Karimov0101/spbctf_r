.class public interface abstract Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/IEvenNumbersView;
.super Ljava/lang/Object;
.source "IEvenNumbersView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/training/view/EvenNumbersTrainingCompleteListener;


# virtual methods
.method public abstract initBoard(II)V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract itemTouchDown(IZ)V
.end method

.method public abstract itemTouchUp(IZ)V
.end method

.method public abstract setBoardItems(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setBoardItemsEnable(Z)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateRecord(I)V
.end method

.method public abstract updateScore(I)V
.end method
