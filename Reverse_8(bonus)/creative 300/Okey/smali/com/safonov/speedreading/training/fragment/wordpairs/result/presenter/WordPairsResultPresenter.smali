.class public Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordPairsResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/IWordPairsResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/IWordPairsResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 8
    .param p1, "trainingResultId"    # I

    .prologue
    .line 27
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-interface {v6, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v2

    .line 28
    .local v2, "currentResult":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v7

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getId()I

    move-result v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getResultList(I)Ljava/util/List;

    move-result-object v4

    .line 32
    .local v4, "resultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;>;"
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v5

    .line 33
    .local v5, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v1

    .line 35
    .local v1, "bestScore":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getId()I

    move-result v6

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getId()I

    move-result v7

    if-ne v6, v7, :cond_1

    const/4 v3, 0x1

    .line 37
    .local v3, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->isViewAttached()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;

    invoke-interface {v6, v5}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;->updateScoreView(I)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;->updateBestScoreView(I)V

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;

    invoke-interface {v6, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;->setNewBestScoreViewVisibility(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/presenter/WordPairsResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/util/ResultListUtil;->getLastPartOfList(Ljava/util/List;)Ljava/util/List;

    move-result-object v7

    invoke-interface {v6, v7}, Lcom/safonov/speedreading/training/fragment/wordpairs/result/view/IWordPairsResultView;->setChartViewData(Ljava/util/List;)V

    .line 43
    :cond_0
    return-void

    .line 35
    .end local v3    # "isNewBest":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
