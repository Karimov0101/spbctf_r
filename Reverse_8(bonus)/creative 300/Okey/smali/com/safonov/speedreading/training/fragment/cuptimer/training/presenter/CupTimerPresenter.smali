.class public Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "CupTimerPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;"
    }
.end annotation


# static fields
.field public static final DEFAULT_COUNT_TIME:I = 0xa

.field public static final MAX_COUNT:I = 0x2

.field public static final NOTIFICATION_SHOWING_TIME:I = 0x9c4


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

.field private firstImage:I

.field firstTimer:Landroid/os/CountDownTimer;

.field private handler:Landroid/os/Handler;

.field isFirst:Z

.field mainTimer:Landroid/os/CountDownTimer;

.field private repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

.field private secondImage:I

.field secondTimer:Landroid/os/CountDownTimer;

.field private startGame:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;)V
    .locals 1
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 30
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->handler:Landroid/os/Handler;

    .line 159
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startGame:Ljava/lang/Runnable;

    .line 42
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondImage:I

    return v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;J)V
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
    .param p1, "x1"    # J

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startSecondTimer(J)V

    return-void
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    .prologue
    .line 21
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstImage:I

    return v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;J)V
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
    .param p1, "x1"    # J

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startFirstTimer(J)V

    return-void
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;J)V
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
    .param p1, "x1"    # J

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startMainTimer(J)V

    return-void
.end method

.method private startFirstTimer(J)V
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->isFirst:Z

    .line 117
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$2;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;JJJ)V

    .line 127
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$2;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstTimer:Landroid/os/CountDownTimer;

    .line 128
    return-void
.end method

.method private startMainTimer(J)V
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 131
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;JJJ)V

    .line 142
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$3;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->mainTimer:Landroid/os/CountDownTimer;

    .line 143
    return-void
.end method

.method private startSecondTimer(J)V
    .locals 9
    .param p1, "time"    # J

    .prologue
    .line 146
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->isFirst:Z

    .line 147
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$4;

    const-wide/16 v4, 0xa

    move-object v1, p0

    move-wide v2, p1

    move-wide v6, p1

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$4;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;JJJ)V

    .line 156
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$4;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondTimer:Landroid/os/CountDownTimer;

    .line 157
    return-void
.end method


# virtual methods
.method public cancelTraining()V
    .locals 0

    .prologue
    .line 75
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->pauseTraining()V

    .line 76
    return-void
.end method

.method public pauseTraining()V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_1

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->mainTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->mainTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 99
    :cond_2
    return-void
.end method

.method public resumeTraining()V
    .locals 1

    .prologue
    .line 103
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->isFirst:Z

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 111
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->mainTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    .line 112
    return-void

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondTimer:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondTimer:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->start()Landroid/os/CountDownTimer;

    goto :goto_0
.end method

.method public saveResult()V
    .locals 4

    .prologue
    .line 80
    new-instance v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;-><init>()V

    .line 81
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;->setUnixTime(J)V

    .line 83
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getId()I

    move-result v2

    new-instance v3, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;->addResult(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;ILcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;)V

    .line 89
    return-void
.end method

.method public startTraining(I)V
    .locals 5
    .param p1, "configId"    # I

    .prologue
    const v4, 0x7f0e00d8

    const v3, 0x7f0800d0

    const v2, 0x7f08008b

    const v1, 0x7f080083

    .line 48
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->repository:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->hideImage()V

    .line 51
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->config:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getType()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 64
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstImage:I

    .line 65
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondImage:I

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->showNotification(I)V

    .line 70
    :goto_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->startGame:Ljava/lang/Runnable;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 71
    return-void

    .line 53
    :pswitch_0
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstImage:I

    .line 54
    iput v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondImage:I

    .line 55
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0, v4}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->showNotification(I)V

    goto :goto_0

    .line 58
    :pswitch_1
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->firstImage:I

    .line 59
    iput v3, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->secondImage:I

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->hideProgressBar()V

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    const v1, 0x7f0e00d9

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->showNotification(I)V

    goto :goto_0

    .line 51
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
