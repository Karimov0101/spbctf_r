.class Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;
.super Ljava/lang/Object;
.source "LineOfSightFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 114
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 115
    .local v0, "backgroundColor":I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)[I

    move-result-object v4

    array-length v5, v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget v1, v4, v3

    .line 116
    .local v1, "itemIndex":I
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;

    invoke-static {v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;->access$100(Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightFragment;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 115
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 118
    .end local v1    # "itemIndex":I
    :cond_0
    return-void
.end method
