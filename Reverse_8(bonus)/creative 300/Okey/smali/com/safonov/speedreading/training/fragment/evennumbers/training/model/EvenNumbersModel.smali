.class public Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/EvenNumbersModel;
.super Ljava/lang/Object;
.source "EvenNumbersModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/IEvenNumbersModel;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createItems(III)Ljava/util/List;
    .locals 1
    .param p1, "itemsCount"    # I
    .param p2, "evenNumbersCount"    # I
    .param p3, "digitPerNumber"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapper;",
            ">;"
        }
    .end annotation

    .prologue
    .line 16
    invoke-static {p3, p1, p2}, Lcom/safonov/speedreading/training/fragment/evennumbers/training/model/NumberWrapperGenerator;->createNumberWrappers(III)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
