.class Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;
.super Ljava/lang/Object;
.source "RememberWordsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->this$0:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 88
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$configId:I

    .line 89
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .line 92
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->setId(I)V

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;->setConfig(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V

    .line 95
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 96
    return-void
.end method
