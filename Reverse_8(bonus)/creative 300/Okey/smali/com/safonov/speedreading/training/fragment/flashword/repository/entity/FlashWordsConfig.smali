.class public Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
.super Lio/realm/RealmObject;
.source "FlashWordsConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/FlashWordsConfigRealmProxyInterface;


# static fields
.field public static final BOARD_TYPE_FIRST:I = 0x1

.field public static final BOARD_TYPE_SECOND:I = 0x2

.field public static final BOARD_TYPE_THIRD:I = 0x3

.field public static final FIELD_BOARD_TYPE:Ljava/lang/String; = "boarType"

.field public static final FIELD_TRAINING_DURATION:Ljava/lang/String; = "trainingDuration"


# instance fields
.field private boarType:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private speed:I

.field private trainingDuration:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getBoarType()I
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmGet$boarType()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getSpeed()I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmGet$speed()I

    move-result v0

    return v0
.end method

.method public getTrainingDuration()J
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmGet$trainingDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$boarType()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->boarType:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->id:I

    return v0
.end method

.method public realmGet$speed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->speed:I

    return v0
.end method

.method public realmGet$trainingDuration()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->trainingDuration:J

    return-wide v0
.end method

.method public realmSet$boarType(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->boarType:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->id:I

    return-void
.end method

.method public realmSet$speed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->speed:I

    return-void
.end method

.method public realmSet$trainingDuration(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->trainingDuration:J

    return-void
.end method

.method public setBoarType(I)V
    .locals 0
    .param p1, "boarType"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmSet$boarType(I)V

    .line 54
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmSet$id(I)V

    .line 38
    return-void
.end method

.method public setSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmSet$speed(I)V

    .line 46
    return-void
.end method

.method public setTrainingDuration(J)V
    .locals 1
    .param p1, "trainingDuration"    # J

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->realmSet$trainingDuration(J)V

    .line 62
    return-void
.end method
