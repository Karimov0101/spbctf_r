.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/passcourseresult/view/ILineOfSightPassCourseResultView;
.super Ljava/lang/Object;
.source "ILineOfSightPassCourseResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract updateFoundMistakesPercentView(F)V
.end method

.method public abstract updateFoundMistakesView(I)V
.end method

.method public abstract updateMistakesView(I)V
.end method

.method public abstract updatePassCoursePointsView(I)V
.end method
