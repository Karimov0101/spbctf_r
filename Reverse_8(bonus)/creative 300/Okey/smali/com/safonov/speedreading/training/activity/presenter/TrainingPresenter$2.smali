.class Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;
.super Ljava/lang/Object;
.source "TrainingPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 98
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onConfigResponse(Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V
    .locals 3
    .param p1, "trainingWrapper"    # Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .prologue
    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$302(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I

    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getConfigWrapper()Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$502(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;)Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getFragmentTypeList()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$602(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Ljava/util/List;)Ljava/util/List;

    .line 105
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$600(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$300(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v1, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$702(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/FragmentType;

    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$700(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$800(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)V

    .line 109
    :cond_0
    return-void
.end method
