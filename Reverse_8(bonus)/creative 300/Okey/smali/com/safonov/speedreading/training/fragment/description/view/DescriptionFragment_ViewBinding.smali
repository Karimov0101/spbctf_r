.class public Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;
.super Ljava/lang/Object;
.source "DescriptionFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

.field private view2131296373:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

    .line 25
    const v1, 0x7f090074

    const-string v2, "field \'contentHolderScrollView\'"

    const-class v3, Landroid/widget/ScrollView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ScrollView;

    iput-object v1, p1, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    .line 26
    const v1, 0x7f090075

    const-string v2, "method \'onStartTrainingClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 27
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->view2131296373:Landroid/view/View;

    .line 28
    new-instance v1, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 34
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 39
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

    .line 40
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 41
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;

    .line 43
    iput-object v2, v0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment;->contentHolderScrollView:Landroid/widget/ScrollView;

    .line 45
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->view2131296373:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/description/view/DescriptionFragment_ViewBinding;->view2131296373:Landroid/view/View;

    .line 47
    return-void
.end method
