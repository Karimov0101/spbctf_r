.class Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;
.super Ljava/lang/Object;
.source "WordsColumnsRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;->addResult(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;ILcom/safonov/speedreading/training/fragment/wordscolumns/repository/IWordsColumnsRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$nextId:I

.field final synthetic val$result:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;ILcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->this$0:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$configId:I

    iput-object p3, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    iput p4, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 77
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$configId:I

    .line 78
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 79
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .line 81
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$nextId:I

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->setId(I)V

    .line 82
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->setConfig(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;)V

    .line 84
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/WordsColumnsRealmUtil$2;->val$result:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 85
    return-void
.end method
