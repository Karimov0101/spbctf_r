.class public Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;
.super Ljava/lang/Object;
.source "WordsColumnsConfigUtil.java"


# static fields
.field private static final ACCELERATOR_COURSE_TYPE_1_COLUMN_COUNT:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_1_WORDS_PER_ITEM:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_2_COLUMN_COUNT:I = 0x2

.field private static final ACCELERATOR_COURSE_TYPE_2_WORDS_PER_ITEM:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_3_COLUMN_COUNT:I = 0x3

.field private static final ACCELERATOR_COURSE_TYPE_3_WORDS_PER_ITEM:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_4_COLUMN_COUNT:I = 0x2

.field private static final ACCELERATOR_COURSE_TYPE_4_WORDS_PER_ITEM:I = 0x2

.field private static final ACCELERATOR_COURSE_TYPE_5_COLUMN_COUNT:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_5_WORDS_PER_ITEM:I = 0x4

.field private static final DEFAULT_DURATION:I = 0xea60

.field private static final DEFAULT_ROW_COUNT:I = 0x10

.field private static final DEFAULT_SPEED:I = 0xc8

.field private static final DEFAULT_WORDS_PER_ITEM:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 4
    .param p0, "columnCount"    # I
    .param p1, "wordsPerItem"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;-><init>()V

    .line 55
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setRowCount(I)V

    .line 56
    invoke-virtual {v0, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setColumnCount(I)V

    .line 58
    invoke-virtual {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setWordsPerItem(I)V

    .line 60
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setSpeed(I)V

    .line 61
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setTrainingDuration(J)V

    .line 63
    return-object v0
.end method

.method public static getAcceleratorCourseType1Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 68
    invoke-static {v0, v0}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public static getAcceleratorCourseType2Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 73
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public static getAcceleratorCourseType3Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 78
    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public static getAcceleratorCourseType4Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 83
    invoke-static {v0, v0}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public static getAcceleratorCourseType5Config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 88
    const/4 v0, 0x1

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/activity/WordsColumnsConfigUtil;->getAcceleratorCourseConfig(II)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public static getUserConfig(IJ)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 3
    .param p0, "columnCount"    # I
    .param p1, "duration"    # J
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 23
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;-><init>()V

    .line 25
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setRowCount(I)V

    .line 26
    invoke-virtual {v0, p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setColumnCount(I)V

    .line 28
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setWordsPerItem(I)V

    .line 30
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setSpeed(I)V

    .line 31
    invoke-virtual {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;->setTrainingDuration(J)V

    .line 33
    return-object v0
.end method
