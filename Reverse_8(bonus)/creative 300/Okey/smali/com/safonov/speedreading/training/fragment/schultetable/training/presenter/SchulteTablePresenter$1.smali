.class Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;
.super Ljava/lang/Object;
.source "SchulteTablePresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeUpTicker$TickerUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->init(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTick(J)V
    .locals 1
    .param p1, "playedTime"    # J

    .prologue
    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/training/presenter/SchulteTablePresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;

    invoke-interface {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/schultetable/training/view/ISchulteTableView;->updateCurrentTimeView(J)V

    .line 52
    :cond_0
    return-void
.end method
