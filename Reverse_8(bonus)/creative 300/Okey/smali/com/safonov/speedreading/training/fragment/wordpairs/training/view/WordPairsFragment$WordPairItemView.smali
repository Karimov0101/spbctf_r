.class Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;
.super Landroid/widget/LinearLayout;
.source "WordPairsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "WordPairItemView"
.end annotation


# instance fields
.field private firstWordTextView:Landroid/widget/TextView;

.field private secondWordTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x2

    const/high16 v3, 0x41800000    # 16.0f

    const/4 v2, -0x2

    .line 142
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 144
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setOrientation(I)V

    .line 145
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->setGravity(I)V

    .line 147
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    .line 148
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 155
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 156
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v4, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->addView(Landroid/view/View;)V

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->addView(Landroid/view/View;)V

    .line 160
    return-void
.end method


# virtual methods
.method public setText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "firstText"    # Ljava/lang/CharSequence;
    .param p2, "secondText"    # Ljava/lang/CharSequence;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 165
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->firstWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 169
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/view/WordPairsFragment$WordPairItemView;->secondWordTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 170
    return-void
.end method
