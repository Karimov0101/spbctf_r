.class public Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
.super Ljava/lang/Object;
.source "MovingCircle.java"


# static fields
.field private static final BORDER_COEFFICENT:I = 0x7


# instance fields
.field private answerPaint:Landroid/graphics/Paint;

.field private border:I

.field private final mass:I

.field private paint:Landroid/graphics/Paint;

.field private position:Ljavax/vecmath/Vector2d;

.field private radius:I

.field private speed:Ljavax/vecmath/Vector2d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->mass:I

    .line 154
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector2d;Ljavax/vecmath/Vector2d;I)V
    .locals 1
    .param p1, "position"    # Ljavax/vecmath/Vector2d;
    .param p2, "speed"    # Ljavax/vecmath/Vector2d;
    .param p3, "radius"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0xa

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->mass:I

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    .line 29
    iput p3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    .line 30
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    .line 31
    div-int/lit8 v0, p3, 0x7

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    .line 32
    return-void
.end method

.method private getDistance(DD)D
    .locals 5
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 132
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    sub-double/2addr v0, p1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v2, Ljavax/vecmath/Vector2d;->y:D

    sub-double/2addr v2, p3

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method private getPosForCircleBottom(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D
    .locals 4
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 255
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getRadius()I

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private getPosForCircleLeft(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D
    .locals 4
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 243
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getRadius()I

    move-result v2

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private getPosForCircleRight(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D
    .locals 4
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 247
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getRadius()I

    move-result v2

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method private getPosForCircleTop(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D
    .locals 4
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 251
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getRadius()I

    move-result v2

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    return-wide v0
.end method

.method private getSumRadius(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D
    .locals 2
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 136
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getRadius()I

    move-result v1

    add-int/2addr v0, v1

    int-to-double v0, v0

    return-wide v0
.end method


# virtual methods
.method public collisionWithCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)V
    .locals 34
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v28, v0

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v30

    move-object/from16 v0, v30

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v30, v0

    sub-double v18, v28, v30

    .line 92
    .local v18, "xDist":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v28, v0

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v30

    move-object/from16 v0, v30

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v30, v0

    sub-double v24, v28, v30

    .line 93
    .local v24, "yDist":D
    mul-double v28, v18, v18

    mul-double v30, v24, v24

    add-double v12, v28, v30

    .line 95
    .local v12, "distSquared":D
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getSpeed()Ljavax/vecmath/Vector2d;

    move-result-object v28

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v30, v0

    sub-double v20, v28, v30

    .line 96
    .local v20, "xVelocity":D
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getSpeed()Ljavax/vecmath/Vector2d;

    move-result-object v28

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v30, v0

    move-object/from16 v0, v30

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v30, v0

    sub-double v26, v28, v30

    .line 98
    .local v26, "yVelocity":D
    mul-double v28, v18, v20

    mul-double v30, v24, v26

    add-double v14, v28, v30

    .line 100
    .local v14, "dotProduct":D
    const-wide/16 v28, 0x0

    cmpl-double v28, v14, v28

    if-lez v28, :cond_0

    .line 101
    div-double v4, v14, v12

    .line 102
    .local v4, "collisionScale":D
    mul-double v16, v18, v4

    .line 103
    .local v16, "xCollision":D
    mul-double v22, v24, v4

    .line 105
    .local v22, "yCollision":D
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getMass()I

    move-result v28

    add-int/lit8 v28, v28, 0xa

    move/from16 v0, v28

    int-to-double v10, v0

    .line 106
    .local v10, "combinedMass":D
    invoke-virtual/range {p1 .. p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getMass()I

    move-result v28

    mul-int/lit8 v28, v28, 0x2

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v6, v28, v10

    .line 107
    .local v6, "collisionWeightA":D
    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v28, 0x14

    move/from16 v0, v28

    int-to-double v0, v0

    move-wide/from16 v28, v0

    div-double v8, v28, v10

    .line 109
    .local v8, "collisionWeightB":D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v30, v0

    mul-double v32, v6, v16

    add-double v30, v30, v32

    move-wide/from16 v0, v30

    move-object/from16 v2, v28

    iput-wide v0, v2, Ljavax/vecmath/Vector2d;->x:D

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v30, v0

    mul-double v32, v6, v22

    add-double v30, v30, v32

    move-wide/from16 v0, v30

    move-object/from16 v2, v28

    iput-wide v0, v2, Ljavax/vecmath/Vector2d;->y:D

    .line 112
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    move-wide/from16 v30, v0

    mul-double v32, v8, v16

    sub-double v30, v30, v32

    move-wide/from16 v0, v30

    move-object/from16 v2, v28

    iput-wide v0, v2, Ljavax/vecmath/Vector2d;->x:D

    .line 113
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    move-wide/from16 v30, v0

    mul-double v32, v8, v22

    sub-double v30, v30, v32

    move-wide/from16 v0, v30

    move-object/from16 v2, v28

    iput-wide v0, v2, Ljavax/vecmath/Vector2d;->y:D

    .line 115
    .end local v4    # "collisionScale":D
    .end local v6    # "collisionWeightA":D
    .end local v8    # "collisionWeightB":D
    .end local v10    # "combinedMass":D
    .end local v16    # "xCollision":D
    .end local v22    # "yCollision":D
    :cond_0
    return-void
.end method

.method public collisionWithWalls(II)Z
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v0, 0x1

    .line 118
    invoke-virtual {p0, p2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isOnWallY(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    invoke-virtual {p0, p2}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setWallPosY(I)V

    .line 128
    :goto_0
    return v0

    .line 123
    :cond_0
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->isOnWallX(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setWallPosX(I)V

    goto :goto_0

    .line 128
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 199
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    double-to-float v0, v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v1, Ljavax/vecmath/Vector2d;->y:D

    double-to-float v1, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 200
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 203
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    double-to-float v0, v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v1, Ljavax/vecmath/Vector2d;->y:D

    double-to-float v1, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 204
    return-void
.end method

.method public drawAnswered(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 207
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    double-to-float v0, v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v1, Ljavax/vecmath/Vector2d;->y:D

    double-to-float v1, v2

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->answerPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 208
    return-void
.end method

.method public getAnswerPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->answerPaint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getMass()I
    .locals 1

    .prologue
    .line 140
    const/16 v0, 0xa

    return v0
.end method

.method public getPaint()Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->paint:Landroid/graphics/Paint;

    return-object v0
.end method

.method public getPosition()Ljavax/vecmath/Vector2d;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    return-object v0
.end method

.method public getRadius()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    return v0
.end method

.method public getSpeed()Ljavax/vecmath/Vector2d;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    return-object v0
.end method

.method public isCollisionWithCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)Z
    .locals 8
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getSumRadius(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v2

    .line 81
    .local v2, "radiusSum":D
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->x:D

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v6

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->y:D

    invoke-direct {p0, v4, v5, v6, v7}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getDistance(DD)D

    move-result-wide v0

    .line 83
    .local v0, "distance":D
    cmpg-double v4, v0, v2

    if-gtz v4, :cond_0

    .line 84
    const/4 v4, 0x1

    .line 87
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public isOnWallX(I)Z
    .locals 4
    .param p1, "width"    # I

    .prologue
    .line 40
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    int-to-double v2, p1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 41
    :cond_0
    const/4 v0, 0x1

    .line 43
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOnWallY(I)Z
    .locals 4
    .param p1, "height"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    add-double/2addr v0, v2

    int-to-double v2, p1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 48
    :cond_0
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPointIn(DD)Z
    .locals 5
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 189
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getDistance(DD)D

    move-result-wide v0

    .line 191
    .local v0, "distance":D
    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_0

    .line 192
    const/4 v2, 0x1

    .line 195
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public setAnswerPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "answerPaint"    # Landroid/graphics/Paint;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->answerPaint:Landroid/graphics/Paint;

    .line 178
    return-void
.end method

.method public setNextPos()V
    .locals 6

    .prologue
    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    iget-wide v4, v1, Ljavax/vecmath/Vector2d;->x:D

    add-double/2addr v2, v4

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    iget-wide v4, v1, Ljavax/vecmath/Vector2d;->y:D

    add-double/2addr v2, v4

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    .line 37
    return-void
.end method

.method public setOppositeSpeedX()V
    .locals 6

    .prologue
    .line 63
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    iget-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    mul-double/2addr v2, v4

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    .line 64
    return-void
.end method

.method public setOppositeSpeedY()V
    .locals 6

    .prologue
    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    iget-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    mul-double/2addr v2, v4

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    .line 77
    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0
    .param p1, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->paint:Landroid/graphics/Paint;

    .line 186
    return-void
.end method

.method public setPosOutCircle(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;II)V
    .locals 10
    .param p1, "circle"    # Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const-wide/16 v8, 0x0

    .line 213
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->x:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->x:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_0

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleRight(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    int-to-double v6, p2

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_0

    .line 214
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleLeft(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v0

    .line 226
    .local v0, "x":D
    :goto_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->y:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->y:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_3

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleBottom(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    int-to-double v6, p3

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 227
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleTop(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v2

    .line 239
    .local v2, "y":D
    :goto_1
    new-instance v4, Ljavax/vecmath/Vector2d;

    invoke-direct {v4, v0, v1, v2, v3}, Ljavax/vecmath/Vector2d;-><init>(DD)V

    invoke-virtual {p1, v4}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setPosition(Ljavax/vecmath/Vector2d;)V

    .line 240
    return-void

    .line 216
    .end local v0    # "x":D
    .end local v2    # "y":D
    :cond_0
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->x:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->x:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_1

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleRight(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    int-to-double v6, p2

    cmpg-double v4, v4, v6

    if-gez v4, :cond_1

    .line 217
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleRight(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v0

    .restart local v0    # "x":D
    goto :goto_0

    .line 219
    .end local v0    # "x":D
    :cond_1
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->x:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->x:D

    cmpg-double v4, v4, v6

    if-gez v4, :cond_2

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleLeft(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    cmpg-double v4, v4, v8

    if-gtz v4, :cond_2

    .line 220
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleRight(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v0

    .restart local v0    # "x":D
    goto :goto_0

    .line 223
    .end local v0    # "x":D
    :cond_2
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleLeft(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v0

    .restart local v0    # "x":D
    goto :goto_0

    .line 229
    :cond_3
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->y:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->y:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_4

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleBottom(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    int-to-double v6, p3

    cmpg-double v4, v4, v6

    if-gez v4, :cond_4

    .line 230
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleBottom(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v2

    .restart local v2    # "y":D
    goto :goto_1

    .line 232
    .end local v2    # "y":D
    :cond_4
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosition()Ljavax/vecmath/Vector2d;

    move-result-object v4

    iget-wide v4, v4, Ljavax/vecmath/Vector2d;->y:D

    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v6, v6, Ljavax/vecmath/Vector2d;->y:D

    cmpg-double v4, v4, v6

    if-gez v4, :cond_5

    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleTop(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v4

    cmpg-double v4, v4, v8

    if-gtz v4, :cond_5

    .line 233
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleBottom(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v2

    .restart local v2    # "y":D
    goto :goto_1

    .line 236
    .end local v2    # "y":D
    :cond_5
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->getPosForCircleTop(Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;)D

    move-result-wide v2

    .restart local v2    # "y":D
    goto :goto_1
.end method

.method public setPosition(Ljavax/vecmath/Vector2d;)V
    .locals 0
    .param p1, "position"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 161
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    .line 162
    return-void
.end method

.method public setRadius(I)V
    .locals 1
    .param p1, "radius"    # I

    .prologue
    .line 148
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    .line 149
    div-int/lit8 v0, p1, 0x7

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->border:I

    .line 150
    return-void
.end method

.method public setSpeed(Ljavax/vecmath/Vector2d;)V
    .locals 0
    .param p1, "speed"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 169
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->speed:Ljavax/vecmath/Vector2d;

    .line 170
    return-void
.end method

.method public setWallPosX(I)V
    .locals 4
    .param p1, "width"    # I

    .prologue
    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->x:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v1

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    .line 59
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setOppositeSpeedX()V

    .line 60
    return-void

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    sub-int v1, p1, v1

    int-to-double v2, v1

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->x:D

    goto :goto_0
.end method

.method public setWallPosY(I)V
    .locals 4
    .param p1, "height"    # I

    .prologue
    .line 67
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget-wide v0, v0, Ljavax/vecmath/Vector2d;->y:D

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v2

    sub-double/2addr v0, v2

    const-wide/16 v2, 0x0

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    int-to-double v2, v1

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    .line 72
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->setOppositeSpeedY()V

    .line 73
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->position:Ljavax/vecmath/Vector2d;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovingCircle;->radius:I

    sub-int v1, p1, v1

    int-to-double v2, v1

    iput-wide v2, v0, Ljavax/vecmath/Vector2d;->y:D

    goto :goto_0
.end method
