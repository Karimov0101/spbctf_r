.class public Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "CupTimerFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field private completeListener:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

.field private configId:I

.field private cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

.field imageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d2
    .end annotation
.end field

.field previewTitle:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090146
    .end annotation
.end field

.field timerBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09006c
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 67
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 78
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;-><init>()V

    .line 79
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->setArguments(Landroid/os/Bundle;)V

    .line 82
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 62
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getCupTimerRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .line 63
    new-instance v1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository;)V

    return-object v1
.end method

.method public hideImage()V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 166
    return-void
.end method

.method public hideNotification()V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 153
    return-void
.end method

.method public hideProgressBar()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->timerBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 72
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 170
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 171
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

    if-eqz v0, :cond_0

    .line 172
    check-cast p1, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

    .line 177
    return-void

    .line 174
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 105
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->configId:I

    .line 108
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    const v1, 0x7f0b0025

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 51
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->unbinder:Lbutterknife/Unbinder;

    .line 53
    return-object v0
.end method

.method public onCupTimerTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;->onCupTimerTrainingCompleted(I)V

    .line 90
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 187
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 188
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;->close()V

    .line 189
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->cupTimerRealmUtil:Lcom/safonov/speedreading/training/fragment/cuptimer/repository/CupTimerRealmUtil;

    .line 191
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;->cancelTraining()V

    .line 192
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 193
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 197
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 198
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->unbinder:Lbutterknife/Unbinder;

    .line 200
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->completeListener:Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerCompleteListener;

    .line 183
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;->startTraining(I)V

    .line 118
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;->pauseTraining()V

    .line 95
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/ICupTimerPresenter;->resumeTraining()V

    .line 100
    return-void
.end method

.method public setImage(I)V
    .locals 1
    .param p1, "resourceImage"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 129
    return-void
.end method

.method public setMaxProgress(J)V
    .locals 3
    .param p1, "maxProgress"    # J

    .prologue
    .line 138
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->timerBar:Landroid/widget/ProgressBar;

    long-to-int v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 139
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 133
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->timerBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 134
    return-void
.end method

.method public showImage()V
    .locals 3

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010010

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 159
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 160
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 161
    return-void
.end method

.method public showNotification(I)V
    .locals 3
    .param p1, "notification"    # I

    .prologue
    .line 143
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f010010

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/CupTimerFragment;->previewTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 148
    return-void
.end method
