.class public Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "EvenNumbersResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/IEvenNumbersResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestScoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090036
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field private evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09008a
    .end annotation
.end field

.field newBestScoreView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09011a
    .end annotation
.end field

.field private resultId:I

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090175
    .end annotation
.end field

.field scoreTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e01ac
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 57
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 63
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;-><init>()V

    .line 64
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 52
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getEvenNumbersRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 53
    new-instance v1, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/EvenNumbersResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->resultId:I

    .line 76
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    const v1, 0x7f0b0038

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 89
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 90
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 172
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->close()V

    .line 174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->evenNumbersRealmUtil:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .line 175
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 179
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 180
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 181
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 182
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/presenter/IEvenNumbersResultPresenter;->initTrainingResults(I)V

    .line 97
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "evenNumbersResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;>;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 129
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;->getScore()I

    move-result v8

    int-to-float v8, v8

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 132
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->scoreTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 133
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 134
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 135
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 136
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 137
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 138
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 139
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 140
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 141
    new-instance v8, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 148
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 153
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 154
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 155
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 157
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 158
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v5, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 159
    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 161
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 162
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v6, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 163
    invoke-virtual {v6, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 165
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 166
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 167
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 168
    return-void
.end method

.method public setNewBestScoreViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 111
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->newBestScoreView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 112
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->bestScoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 107
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 101
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/result/view/EvenNumbersResultFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 102
    return-void
.end method
