.class public Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
.super Lio/realm/RealmObject;
.source "EvenNumbersConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/EvenNumbersConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COLUMN_COUNT:Ljava/lang/String; = "columnCount"

.field public static final FIELD_DIGITS_PER_NUMBER:Ljava/lang/String; = "digitsPerNumber"

.field public static final FIELD_EVEN_NUMBERS_COUNT:Ljava/lang/String; = "evenNumberCount"

.field public static final FIELD_ROW_COUNT:Ljava/lang/String; = "rowCount"

.field public static final FIELD_TRAINING_DURATION:Ljava/lang/String; = "trainingDuration"


# instance fields
.field private columnCount:I

.field private digitsPerNumber:I

.field private evenNumberCount:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private rowCount:I

.field private trainingDuration:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$columnCount()I

    move-result v0

    return v0
.end method

.method public getDigitsPerNumber()I
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$digitsPerNumber()I

    move-result v0

    return v0
.end method

.method public getEvenNumberCount()I
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$evenNumberCount()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$rowCount()I

    move-result v0

    return v0
.end method

.method public getTrainingDuration()I
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmGet$trainingDuration()I

    move-result v0

    return v0
.end method

.method public realmGet$columnCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->columnCount:I

    return v0
.end method

.method public realmGet$digitsPerNumber()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->digitsPerNumber:I

    return v0
.end method

.method public realmGet$evenNumberCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->evenNumberCount:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->id:I

    return v0
.end method

.method public realmGet$rowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->rowCount:I

    return v0
.end method

.method public realmGet$trainingDuration()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->trainingDuration:I

    return v0
.end method

.method public realmSet$columnCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->columnCount:I

    return-void
.end method

.method public realmSet$digitsPerNumber(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->digitsPerNumber:I

    return-void
.end method

.method public realmSet$evenNumberCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->evenNumberCount:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->id:I

    return-void
.end method

.method public realmSet$rowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->rowCount:I

    return-void
.end method

.method public realmSet$trainingDuration(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->trainingDuration:I

    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$columnCount(I)V

    .line 53
    return-void
.end method

.method public setDigitsPerNumber(I)V
    .locals 0
    .param p1, "digitsPerNumber"    # I

    .prologue
    .line 68
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$digitsPerNumber(I)V

    .line 69
    return-void
.end method

.method public setEvenNumberCount(I)V
    .locals 0
    .param p1, "evenNumberCount"    # I

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$evenNumberCount(I)V

    .line 61
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 31
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$id(I)V

    .line 32
    return-void
.end method

.method public setRowCount(I)V
    .locals 0
    .param p1, "rowCount"    # I

    .prologue
    .line 44
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$rowCount(I)V

    .line 45
    return-void
.end method

.method public setTrainingDuration(I)V
    .locals 0
    .param p1, "trainingDuration"    # I

    .prologue
    .line 76
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->realmSet$trainingDuration(I)V

    .line 77
    return-void
.end method
