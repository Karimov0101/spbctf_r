.class public Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordPairsPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/IWordPairsPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    .line 23
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 7
    .param p1, "trainingResultId"    # I

    .prologue
    .line 27
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-interface {v5, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v2

    .line 28
    .local v2, "currentResult":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    iget-object v5, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->getId()I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/IWordPairsRepository;->getBestResult(I)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v0

    .line 30
    .local v0, "bestResult":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v4

    .line 31
    .local v4, "score":I
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getScore()I

    move-result v1

    .line 33
    .local v1, "bestScore":I
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getId()I

    move-result v5

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;->getId()I

    move-result v6

    if-ne v5, v6, :cond_1

    const/4 v3, 0x1

    .line 35
    .local v3, "isNewBest":Z
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->isViewAttached()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;

    invoke-interface {v5, v4}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;->updateScoreView(I)V

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;

    invoke-interface {v5, v1}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;->updateBestScoreView(I)V

    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;

    invoke-interface {v5, v3}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;->setNewBestScoreViewVisibility(Z)V

    .line 39
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/presenter/WordPairsPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v5

    check-cast v5, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;

    invoke-static {v4}, Lcom/safonov/speedreading/training/fragment/passcource/util/WordPairsScoreUtil;->getPassCourseScore(I)I

    move-result v6

    invoke-interface {v5, v6}, Lcom/safonov/speedreading/training/fragment/wordpairs/passcourseresult/view/IWordPairsPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 41
    :cond_0
    return-void

    .line 33
    .end local v3    # "isNewBest":Z
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method
