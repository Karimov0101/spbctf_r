.class public Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;
.super Ljava/lang/Object;
.source "WordPairsModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/IWordPairsModel;


# instance fields
.field private firstWords:[Ljava/lang/String;

.field private secondWords:[Ljava/lang/String;

.field private words:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->words:[Ljava/lang/String;

    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->firstWords:[Ljava/lang/String;

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->secondWords:[Ljava/lang/String;

    .line 24
    return-void
.end method


# virtual methods
.method public createWordPairs(II)Ljava/util/List;
    .locals 3
    .param p1, "wordPairsCountCount"    # I
    .param p2, "differentWordPairsCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;",
            ">;"
        }
    .end annotation

    .prologue
    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->words:[Ljava/lang/String;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->firstWords:[Ljava/lang/String;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsModel;->secondWords:[Ljava/lang/String;

    invoke-static {v0, v1, v2, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPairsGenerator;->createWordPairs([Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
