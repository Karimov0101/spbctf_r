.class public Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "TrainingPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/activity/view/ITrainingView;",
        ">;",
        "Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;"
    }
.end annotation


# instance fields
.field private configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

.field private courseResultsIds:[I

.field private courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

.field private courseTrainingIndex:I

.field private currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

.field private fragmentIndex:I

.field private fragmentTypeList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/FragmentType;",
            ">;"
        }
    .end annotation
.end field

.field private isActivityFullscreen:Z

.field private isFirstLaunch:Z

.field private isPauseDialogShowing:Z

.field private passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

.field private readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

.field private shouldShowPauseDialog:Z

.field private trainingConfigUtil:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;

.field private trainingType:Lcom/safonov/speedreading/training/TrainingType;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;)V
    .locals 1
    .param p1, "trainingConfigUtil"    # Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "courseSaveUtil"    # Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p3, "readerTimerModeSaveUtil"    # Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 511
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isFirstLaunch:Z

    .line 34
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingConfigUtil:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;

    .line 35
    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    .line 36
    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    .line 37
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    return-object v0
.end method

.method static synthetic access$102(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;[I)[I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # [I

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseResultsIds:[I

    return-object p1
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    return v0
.end method

.method static synthetic access$202(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    return p1
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    return v0
.end method

.method static synthetic access$302(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 23
    iput p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    return p1
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    return-object v0
.end method

.method static synthetic access$402(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # [Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    return-object p1
.end method

.method static synthetic access$502(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;)Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    return-object p1
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$602(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    return-object p1
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    return-object v0
.end method

.method static synthetic access$702(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/FragmentType;
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    return-object p1
.end method

.method static synthetic access$800(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;
    .param p1, "x1"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    return-void
.end method

.method private goToPrepareFragment()V
    .locals 2

    .prologue
    .line 579
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    .line 580
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 581
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_0

    .line 582
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 583
    return-void
.end method

.method private setComplexityActionBar()V
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowActionBar()V

    .line 388
    :cond_0
    return-void
.end method

.method private setCourseResultActionBar()V
    .locals 1

    .prologue
    .line 403
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_1

    .line 404
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 407
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 409
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 410
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowActionBar()V

    .line 412
    :cond_2
    return-void
.end method

.method private setCourseResultFragment(Lcom/safonov/speedreading/training/FragmentType;I)V
    .locals 2
    .param p1, "resultFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;
    .param p2, "resultId"    # I

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 325
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsColumnsCourseResultFragment(I)V

    goto :goto_0

    .line 328
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsBlockCourseResultFragment(I)V

    goto :goto_0

    .line 331
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setFlashWordsPassCourseResultFragment(I)V

    goto :goto_0

    .line 335
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSchulteTablePassCourseResultFragment(I)V

    goto :goto_0

    .line 338
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setRememberNumberPassCourseResultFragment(I)V

    goto :goto_0

    .line 341
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setLineOfSightPassCourseResultFragment(I)V

    goto :goto_0

    .line 344
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSpeedReadingPassCourseResultFragment(I)V

    goto :goto_0

    .line 347
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordPairsPassCourseResultFragment(I)V

    goto :goto_0

    .line 350
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setEvenNumbersPassCourseResultFragment(I)V

    goto :goto_0

    .line 353
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setGreenDotPassCourseResultFragment(I)V

    goto :goto_0

    .line 323
    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private setFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 3
    .param p1, "fragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    const/4 v1, 0x0

    .line 119
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->INTERSTITIAL:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p1, v0, :cond_2

    .line 120
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_0

    .line 121
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 122
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    iget v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    invoke-interface {v0, v1, v2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setInterstitialFragment(Lcom/safonov/speedreading/training/TrainingType;I)V

    .line 129
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowActionBar()V

    .line 234
    :cond_1
    :goto_0
    return-void

    .line 134
    :cond_2
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne p1, v0, :cond_4

    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->isFullscreen()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 136
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 137
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->hideActionBar()V

    .line 142
    :cond_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setPrepareFragment()V

    .line 144
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowActionBar()V

    goto :goto_0

    .line 149
    :cond_4
    invoke-static {p1}, Lcom/safonov/speedreading/training/FragmentType;->isDescriptionFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 150
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_5

    .line 151
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 152
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 153
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 157
    :cond_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setDescriptionFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 159
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowActionBar()V

    goto :goto_0

    .line 164
    :cond_6
    invoke-static {p1}, Lcom/safonov/speedreading/training/FragmentType;->isComplexityFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 165
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 166
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setComplexityActionBar()V

    .line 167
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 178
    :cond_7
    :goto_1
    invoke-static {p1}, Lcom/safonov/speedreading/training/FragmentType;->isTrainingFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 180
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 230
    :cond_8
    :goto_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 231
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowTrainingToolbar(Z)V

    goto/16 :goto_0

    .line 169
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setMathComplexityFragment(I)V

    goto :goto_1

    .line 172
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setConcentrationComplexityFragment(I)V

    goto :goto_1

    .line 182
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsColumnsFragment(I)V

    goto :goto_2

    .line 185
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsBlockFragment(I)V

    goto :goto_2

    .line 188
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setFlashWordsFragment(I)V

    goto :goto_2

    .line 192
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSchulteTableFragment(I)V

    goto :goto_2

    .line 195
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setRememberNumberFragment(I)V

    goto/16 :goto_2

    .line 198
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setLineOfSightFragment(I)V

    goto/16 :goto_2

    .line 201
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSpeedReadingFragment(I)V

    goto/16 :goto_2

    .line 204
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordPairsFragment(I)V

    goto/16 :goto_2

    .line 207
    :pswitch_a
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setEvenNumbersFragment(I)V

    goto/16 :goto_2

    .line 210
    :pswitch_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setGreenDotFragment(I)V

    goto/16 :goto_2

    .line 213
    :pswitch_c
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setMathFragment(I)V

    goto/16 :goto_2

    .line 216
    :pswitch_d
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setConcentrationFragment(I)V

    goto/16 :goto_2

    .line 219
    :pswitch_e
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setCupTimerFragment(I)V

    goto/16 :goto_2

    .line 222
    :pswitch_f
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setRememberWordsFragment(I)V

    goto/16 :goto_2

    .line 225
    :pswitch_10
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getConfigId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setTrueColorsFragment(I)V

    goto/16 :goto_2

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 180
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method private setHelpActionBar()V
    .locals 1

    .prologue
    .line 373
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_1

    .line 374
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 377
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 379
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 380
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setCloseActionBar()V

    .line 382
    :cond_2
    return-void
.end method

.method private setResultActionBar()V
    .locals 2

    .prologue
    .line 391
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_1

    .line 392
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 393
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 395
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 397
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 398
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setArrowTrainingToolbar(Z)V

    .line 400
    :cond_2
    return-void
.end method

.method private setResultFragment(Lcom/safonov/speedreading/training/FragmentType;I)V
    .locals 2
    .param p1, "resultFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;
    .param p2, "resultId"    # I

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsColumnsResultFragment(I)V

    goto :goto_0

    .line 275
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsBlockResultFragment(I)V

    goto :goto_0

    .line 278
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setFlashWordsResultFragment(I)V

    goto :goto_0

    .line 282
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSchulteTableResultFragment(I)V

    goto :goto_0

    .line 285
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setRememberNumberResultFragment(I)V

    goto :goto_0

    .line 288
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setLineOfSightResultFragment(I)V

    goto :goto_0

    .line 291
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSpeedReadingResultFragment(I)V

    goto :goto_0

    .line 294
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordPairsResultFragment(I)V

    goto :goto_0

    .line 297
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setEvenNumbersResultFragment(I)V

    goto :goto_0

    .line 300
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setGreenDotResultFragment(I)V

    goto :goto_0

    .line 303
    :pswitch_a
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setMathResultFragment(I)V

    goto :goto_0

    .line 306
    :pswitch_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setConcentrationResultFragment(I)V

    goto :goto_0

    .line 309
    :pswitch_c
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setCupTimerResultFragment(I)V

    goto/16 :goto_0

    .line 312
    :pswitch_d
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setRememberWordsResultFragment(I)V

    goto/16 :goto_0

    .line 315
    :pswitch_e
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, p2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setTrueColorsResultFragment(I)V

    goto/16 :goto_0

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1a
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch
.end method

.method private setSettingsActionBar()V
    .locals 1

    .prologue
    .line 360
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    if-eqz v0, :cond_1

    .line 361
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showActionBar()V

    .line 364
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isActivityFullscreen:Z

    .line 367
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 368
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setCloseActionBar()V

    .line 370
    :cond_2
    return-void
.end method

.method private setSettingsFragment(Lcom/safonov/speedreading/training/FragmentType;)V
    .locals 2
    .param p1, "settingsFragmentType"    # Lcom/safonov/speedreading/training/FragmentType;

    .prologue
    .line 237
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 238
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$FragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/FragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 266
    :cond_0
    :goto_0
    return-void

    .line 240
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsColumnsSettingsFragment()V

    goto :goto_0

    .line 243
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setWordsBlockSettingsFragment()V

    goto :goto_0

    .line 246
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setFlashWordsSettingsFragment()V

    goto :goto_0

    .line 250
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setSchulteTableSettingsFragment()V

    goto :goto_0

    .line 253
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setLineOfSightSettingsFragment()V

    goto :goto_0

    .line 256
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setGreenDotSettingsFragment()V

    goto :goto_0

    .line 259
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setMathSettingsFragment()V

    goto :goto_0

    .line 262
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setCupTimerSettingsFragment()V

    goto :goto_0

    .line 238
    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public onActionBarHelpPressed()V
    .locals 2

    .prologue
    .line 592
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getTrainingType()Lcom/safonov/speedreading/training/TrainingType;

    move-result-object v0

    invoke-static {v0}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceHelpFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 593
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setHelpActionBar()V

    .line 594
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 595
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setHelpFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 597
    :cond_0
    return-void
.end method

.method public onActionBarHomePressed()V
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isHelpFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 609
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->goToPrepareFragment()V

    .line 621
    :cond_0
    :goto_0
    return-void

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isSettingsFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 614
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    goto :goto_0

    .line 618
    :cond_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto :goto_0
.end method

.method public onActionBarRestartPressed()V
    .locals 0

    .prologue
    .line 587
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->goToPrepareFragment()V

    .line 588
    return-void
.end method

.method public onActionBarSettingsPressed()V
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceSettingsFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 602
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setSettingsActionBar()V

    .line 603
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setSettingsFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 604
    return-void
.end method

.method public onBackPressed()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 417
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isPauseDialogShowing:Z

    if-eqz v0, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-ne v0, v1, :cond_9

    .line 420
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_3

    .line 421
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->pauseFragmentAnimations()V

    .line 423
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, v2, v2, v2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto :goto_0

    .line 428
    :cond_3
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_5

    .line 429
    :cond_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto :goto_0

    .line 435
    :cond_5
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isHelpFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 436
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->goToPrepareFragment()V

    goto :goto_0

    .line 440
    :cond_6
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isTrainingFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 441
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 442
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->pauseFragmentAnimations()V

    .line 443
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, v3, v3, v2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto :goto_0

    .line 448
    :cond_7
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isResultFragment(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 449
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, v3, v3, v2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto/16 :goto_0

    .line 456
    :cond_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0, v2, v2, v2}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto/16 :goto_0

    .line 460
    :cond_9
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_a

    .line 461
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto/16 :goto_0

    .line 467
    :cond_a
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isDescriptionFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 468
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto/16 :goto_0

    .line 474
    :cond_b
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isResultFragment(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 475
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto/16 :goto_0

    .line 481
    :cond_c
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isHelpFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 482
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->goToPrepareFragment()V

    goto/16 :goto_0

    .line 486
    :cond_d
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isSettingsFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 487
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    goto/16 :goto_0

    .line 491
    :cond_e
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isComplexityFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 492
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    goto/16 :goto_0

    .line 498
    :cond_f
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isTrainingFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->pauseFragmentAnimations()V

    .line 501
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported()Z

    move-result v1

    invoke-interface {v0, v3, v3, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 535
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_1

    .line 536
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->pauseFragmentAnimations()V

    .line 549
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/FragmentType;->isTrainingFragmentType(Lcom/safonov/speedreading/training/FragmentType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->shouldShowPauseDialog:Z

    .line 544
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->pauseFragmentAnimations()V

    goto :goto_0
.end method

.method public onPauseDialogContinueClick()V
    .locals 1

    .prologue
    .line 637
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 638
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->resumeFragmentAnimations()V

    .line 639
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->dismissPauseDialog()V

    .line 641
    :cond_0
    return-void
.end method

.method public onPauseDialogDismiss()V
    .locals 1

    .prologue
    .line 632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isPauseDialogShowing:Z

    .line 633
    return-void
.end method

.method public onPauseDialogExitClick()V
    .locals 1

    .prologue
    .line 673
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->dismissPauseDialog()V

    .line 675
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->finish()V

    .line 677
    :cond_0
    return-void
.end method

.method public onPauseDialogHelpClick()V
    .locals 2

    .prologue
    .line 663
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getTrainingType()Lcom/safonov/speedreading/training/TrainingType;

    move-result-object v0

    invoke-static {v0}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceHelpFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 664
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setHelpActionBar()V

    .line 665
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 666
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setHelpFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 667
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->dismissPauseDialog()V

    .line 669
    :cond_0
    return-void
.end method

.method public onPauseDialogRestartClick()V
    .locals 1

    .prologue
    .line 655
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->goToPrepareFragment()V

    .line 656
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->dismissPauseDialog()V

    .line 659
    :cond_0
    return-void
.end method

.method public onPauseDialogSettingsClick()V
    .locals 1

    .prologue
    .line 645
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->getTrainingType()Lcom/safonov/speedreading/training/TrainingType;

    move-result-object v0

    invoke-static {v0}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceSettingsFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 646
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setSettingsActionBar()V

    .line 647
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setSettingsFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 648
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->dismissPauseDialog()V

    .line 651
    :cond_0
    return-void
.end method

.method public onPauseDialogShow()V
    .locals 1

    .prologue
    .line 627
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isPauseDialogShowing:Z

    .line 628
    return-void
.end method

.method public onReadingCompleted()V
    .locals 2

    .prologue
    .line 735
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    .line 737
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-ne v0, v1, :cond_0

    .line 738
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->PASS_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 740
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setCourseResultActionBar()V

    .line 741
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 742
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseResultsIds:[I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setPassCourseResultFragment([I)V

    .line 746
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-ne v0, v1, :cond_1

    .line 747
    sget-object v0, Lcom/safonov/speedreading/training/FragmentType;->ACCELERATOR_COURSE_RESULT:Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 749
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setCourseResultActionBar()V

    .line 750
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 751
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseResultsIds:[I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setAcceleratorCourseResultFragment([I)V

    .line 754
    :cond_1
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 553
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isFirstLaunch:Z

    if-eqz v0, :cond_1

    .line 554
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isFirstLaunch:Z

    .line 574
    :cond_0
    :goto_0
    return-void

    .line 558
    :cond_1
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->shouldShowPauseDialog:Z

    if-eqz v0, :cond_2

    .line 559
    iput-boolean v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->shouldShowPauseDialog:Z

    .line 560
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isPauseDialogShowing:Z

    if-nez v0, :cond_0

    .line 561
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 562
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/TrainingType;->isSettingsSupported()Z

    move-result v1

    invoke-interface {v0, v2, v2, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->showPauseDialog(ZZZ)V

    goto :goto_0

    .line 568
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    sget-object v1, Lcom/safonov/speedreading/training/FragmentType;->PREPARE:Lcom/safonov/speedreading/training/FragmentType;

    if-ne v0, v1, :cond_0

    .line 569
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 570
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->resumeFragmentAnimations()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 515
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v0}, Lcom/safonov/speedreading/training/TrainingType;->isCourseTraining(Lcom/safonov/speedreading/training/TrainingType;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    if-nez v0, :cond_1

    .line 517
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->reset(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 519
    sget-object v0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$3;->$SwitchMap$com$safonov$speedreading$training$TrainingType:[I

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/TrainingType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 521
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->reset(I)V

    goto :goto_0

    .line 524
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->readerTimerModeSaveUtil:Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->reset(I)V

    goto :goto_0

    .line 528
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    iget v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseResultsIds:[I

    invoke-virtual {v0, v1, v2, v3}, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->save(Lcom/safonov/speedreading/training/TrainingType;I[I)V

    goto :goto_0

    .line 519
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V
    .locals 2
    .param p1, "completedTrainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "resultId"    # I

    .prologue
    .line 716
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->PASS_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    if-ne v0, v1, :cond_2

    .line 717
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseResultsIds:[I

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    aput p2, v0, v1

    .line 719
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceResultFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 720
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 721
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setResultActionBar()V

    .line 722
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0, p2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setCourseResultFragment(Lcom/safonov/speedreading/training/FragmentType;I)V

    .line 731
    :cond_1
    :goto_0
    return-void

    .line 725
    :cond_2
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->getReferenceResultFragmentType(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 726
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 727
    invoke-direct {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setResultActionBar()V

    .line 728
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0, p2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setResultFragment(Lcom/safonov/speedreading/training/FragmentType;I)V

    goto :goto_0
.end method

.method public requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 2
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 59
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    .line 60
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/activity/view/ITrainingView;

    invoke-virtual {p1}, Lcom/safonov/speedreading/training/TrainingType;->getTitleRes()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/activity/view/ITrainingView;->setToolbarTitle(I)V

    .line 64
    :cond_0
    invoke-static {p1}, Lcom/safonov/speedreading/training/TrainingType;->isCourseTraining(Lcom/safonov/speedreading/training/TrainingType;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingConfigUtil:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;

    new-instance v1, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;-><init>(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/TrainingType;)V

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;->requestToGetCourseConfigList(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V

    .line 112
    :goto_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingConfigUtil:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;

    new-instance v1, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;

    invoke-direct {v1, p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$2;-><init>(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)V

    invoke-interface {v0, p1, v1}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil;->requestToGetTrainingConfig(Lcom/safonov/speedreading/training/TrainingType;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V

    goto :goto_0
.end method

.method public requestToSetNextCourseTraining()V
    .locals 2

    .prologue
    .line 683
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    .line 684
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    .line 685
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 686
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getConfigWrapper()Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->configWrapper:Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .line 687
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->passCourseTrainingWrapperArray:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getFragmentTypeList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    .line 689
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 690
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 695
    :cond_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->onReadingCompleted()V

    goto :goto_0
.end method

.method public requestToSetNextFragment()V
    .locals 2

    .prologue
    .line 705
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    .line 706
    iget v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 707
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentTypeList:Ljava/util/List;

    iget v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->fragmentIndex:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/FragmentType;

    iput-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    .line 708
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 709
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->currentFragmentType:Lcom/safonov/speedreading/training/FragmentType;

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->setFragment(Lcom/safonov/speedreading/training/FragmentType;)V

    .line 712
    :cond_0
    return-void
.end method

.method public restartCourse()V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    iput v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseTrainingIndex:I

    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->courseSaveUtil:Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->reset(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 55
    return-void
.end method
