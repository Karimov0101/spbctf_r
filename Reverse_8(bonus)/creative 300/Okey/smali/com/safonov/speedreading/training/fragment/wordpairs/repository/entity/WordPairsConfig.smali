.class public Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
.super Lio/realm/RealmObject;
.source "WordPairsConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/WordPairsConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COLUMN_COUNT:Ljava/lang/String; = "columnCount"

.field public static final FIELD_DIFFERENT_WORD_PAIRS_COUNT:Ljava/lang/String; = "differentWordPairsCount"

.field public static final FIELD_ROW_COUNT:Ljava/lang/String; = "rowCount"

.field public static final FIELD_TRAINING_DURATION:Ljava/lang/String; = "trainingDuration"


# instance fields
.field private columnCount:I

.field private differentWordPairsCount:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private rowCount:I

.field private trainingDuration:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmGet$columnCount()I

    move-result v0

    return v0
.end method

.method public getDifferentWordPairsCount()I
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmGet$differentWordPairsCount()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmGet$rowCount()I

    move-result v0

    return v0
.end method

.method public getTrainingDuration()I
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmGet$trainingDuration()I

    move-result v0

    return v0
.end method

.method public realmGet$columnCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->columnCount:I

    return v0
.end method

.method public realmGet$differentWordPairsCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->differentWordPairsCount:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->id:I

    return v0
.end method

.method public realmGet$rowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->rowCount:I

    return v0
.end method

.method public realmGet$trainingDuration()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->trainingDuration:I

    return v0
.end method

.method public realmSet$columnCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->columnCount:I

    return-void
.end method

.method public realmSet$differentWordPairsCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->differentWordPairsCount:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->id:I

    return-void
.end method

.method public realmSet$rowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->rowCount:I

    return-void
.end method

.method public realmSet$trainingDuration(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->trainingDuration:I

    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 49
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmSet$columnCount(I)V

    .line 50
    return-void
.end method

.method public setDifferentWordPairsCount(I)V
    .locals 0
    .param p1, "differentWordPairsCount"    # I

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmSet$differentWordPairsCount(I)V

    .line 58
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmSet$id(I)V

    .line 34
    return-void
.end method

.method public setRowCount(I)V
    .locals 0
    .param p1, "rowCount"    # I

    .prologue
    .line 41
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmSet$rowCount(I)V

    .line 42
    return-void
.end method

.method public setTrainingDuration(I)V
    .locals 0
    .param p1, "trainingDuration"    # I

    .prologue
    .line 65
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;->realmSet$trainingDuration(I)V

    .line 66
    return-void
.end method
