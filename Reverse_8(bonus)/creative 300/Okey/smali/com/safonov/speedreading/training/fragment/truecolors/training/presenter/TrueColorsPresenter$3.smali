.class Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;
.super Ljava/lang/Object;
.source "TrueColorsPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    .prologue
    .line 145
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 148
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->setAnswerImage(Z)V

    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->timerPause()V

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->disableButtons()V

    .line 152
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    iget v2, v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    const-wide/16 v4, 0x1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/ITrueColorsView;->getProgress()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    add-long/2addr v4, v6

    long-to-int v0, v4

    sub-int v0, v2, v0

    iput v0, v1, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->score:I

    .line 153
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->access$100(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter$3;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/TrueColorsPresenter;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 154
    return-void
.end method
