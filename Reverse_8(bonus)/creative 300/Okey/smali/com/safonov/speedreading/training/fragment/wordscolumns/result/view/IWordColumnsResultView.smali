.class public interface abstract Lcom/safonov/speedreading/training/fragment/wordscolumns/result/view/IWordColumnsResultView;
.super Ljava/lang/Object;
.source "IWordColumnsResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateTrainingDurationView(J)V
.end method
