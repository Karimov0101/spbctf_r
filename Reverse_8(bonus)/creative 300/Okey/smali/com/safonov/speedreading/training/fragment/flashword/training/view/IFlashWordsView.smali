.class public interface abstract Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;
.super Ljava/lang/Object;
.source "IFlashWordsView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract initBoardView(I)V
.end method

.method public abstract onTrainingCompleted(I)V
.end method

.method public abstract setItemsEmptyText()V
.end method

.method public abstract setItemsText(ILjava/util/List;)V
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateLevelView(I)V
.end method

.method public abstract updateSpeedView(I)V
.end method

.method public abstract updateTimeView(J)V
.end method
