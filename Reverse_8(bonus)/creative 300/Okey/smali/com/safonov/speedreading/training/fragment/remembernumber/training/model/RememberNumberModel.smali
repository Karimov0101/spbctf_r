.class public Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;
.super Ljava/lang/Object;
.source "RememberNumberModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/IRememberNumberModel;


# instance fields
.field private random:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 11
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;->random:Ljava/util/Random;

    return-void
.end method


# virtual methods
.method public createNumber(I)[Ljava/lang/String;
    .locals 4
    .param p1, "length"    # I

    .prologue
    .line 15
    new-array v1, p1, [Ljava/lang/String;

    .line 16
    .local v1, "resultNumber":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 17
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/training/model/RememberNumberModel;->random:Ljava/util/Random;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 16
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 20
    :cond_0
    return-object v1
.end method
