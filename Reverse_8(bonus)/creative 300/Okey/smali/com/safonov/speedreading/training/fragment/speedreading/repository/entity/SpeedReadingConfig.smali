.class public Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
.super Lio/realm/RealmObject;
.source "SpeedReadingConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/SpeedReadingConfigRealmProxyInterface;


# static fields
.field public static final FIELD_MAX_WORD_SHOW_COUNT:Ljava/lang/String; = "maxWordShowCount"

.field public static final FIELD_MIN_SPEED:Ljava/lang/String; = "minSpeed"

.field public static final FIELD_MIN_WORD_SHOW_COUNT:Ljava/lang/String; = "minWordShowCount"

.field public static final FIELD_SPEED:Ljava/lang/String; = "speed"

.field public static final FIELD_SPEED_STEP:Ljava/lang/String; = "speedStep"

.field public static final FIELD_TRAINING_SHOW_COUNT:Ljava/lang/String; = "trainingShowCount"


# instance fields
.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private maxWordShowCount:I

.field private minSpeed:I

.field private minWordShowCount:I

.field private speed:I

.field private speedStep:I

.field private trainingShowCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getId()I
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getMaxWordShowCount()I
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$maxWordShowCount()I

    move-result v0

    return v0
.end method

.method public getMinSpeed()I
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$minSpeed()I

    move-result v0

    return v0
.end method

.method public getMinWordShowCount()I
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$minWordShowCount()I

    move-result v0

    return v0
.end method

.method public getSpeed()I
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$speed()I

    move-result v0

    return v0
.end method

.method public getSpeedStep()I
    .locals 1

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$speedStep()I

    move-result v0

    return v0
.end method

.method public getTrainingShowCount()I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmGet$trainingShowCount()I

    move-result v0

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->id:I

    return v0
.end method

.method public realmGet$maxWordShowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->maxWordShowCount:I

    return v0
.end method

.method public realmGet$minSpeed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->minSpeed:I

    return v0
.end method

.method public realmGet$minWordShowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->minWordShowCount:I

    return v0
.end method

.method public realmGet$speed()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->speed:I

    return v0
.end method

.method public realmGet$speedStep()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->speedStep:I

    return v0
.end method

.method public realmGet$trainingShowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->trainingShowCount:I

    return v0
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->id:I

    return-void
.end method

.method public realmSet$maxWordShowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->maxWordShowCount:I

    return-void
.end method

.method public realmSet$minSpeed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->minSpeed:I

    return-void
.end method

.method public realmSet$minWordShowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->minWordShowCount:I

    return-void
.end method

.method public realmSet$speed(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->speed:I

    return-void
.end method

.method public realmSet$speedStep(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->speedStep:I

    return-void
.end method

.method public realmSet$trainingShowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->trainingShowCount:I

    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 32
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$id(I)V

    .line 33
    return-void
.end method

.method public setMaxWordShowCount(I)V
    .locals 0
    .param p1, "maxWordShowCount"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$maxWordShowCount(I)V

    .line 86
    return-void
.end method

.method public setMinSpeed(I)V
    .locals 0
    .param p1, "minSpeed"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$minSpeed(I)V

    .line 62
    return-void
.end method

.method public setMinWordShowCount(I)V
    .locals 0
    .param p1, "minWordShowCount"    # I

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$minWordShowCount(I)V

    .line 78
    return-void
.end method

.method public setSpeed(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$speed(I)V

    .line 54
    return-void
.end method

.method public setSpeedStep(I)V
    .locals 0
    .param p1, "speedStep"    # I

    .prologue
    .line 69
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$speedStep(I)V

    .line 70
    return-void
.end method

.method public setTrainingShowCount(I)V
    .locals 0
    .param p1, "trainingShowCount"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->realmSet$trainingShowCount(I)V

    .line 46
    return-void
.end method
