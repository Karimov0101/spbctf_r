.class Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;
.super Ljava/lang/Object;
.source "GreenDotPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->startTraining(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

.field final synthetic val$configId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->val$configId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnd()V
    .locals 4

    .prologue
    .line 56
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;-><init>()V

    .line 58
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->setUnixTime(J)V

    .line 60
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    move-result-object v1

    iget v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->val$configId:I

    new-instance v3, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1$1;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;)V

    invoke-interface {v1, v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->addResult(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;ILcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository$OnSingleTransactionCallback;)V

    .line 69
    .end local v0    # "result":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "playedTime"    # J

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$000(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$102(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;I)I

    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$002(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;Z)Z

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter$1;->this$0:Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;->access$108(Lcom/safonov/speedreading/training/fragment/greendot/training/presenter/GreenDotPresenter;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/training/view/IGreenDotView;->setGreenDotViewProgress(I)V

    .line 52
    :cond_1
    return-void
.end method
