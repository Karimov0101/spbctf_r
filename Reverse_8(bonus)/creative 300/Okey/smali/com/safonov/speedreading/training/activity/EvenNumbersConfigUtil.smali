.class public Lcom/safonov/speedreading/training/activity/EvenNumbersConfigUtil;
.super Ljava/lang/Object;
.source "EvenNumbersConfigUtil.java"


# static fields
.field private static final DEFAULT_COLUMN_COUNT:I = 0x4

.field private static final DEFAULT_DIGITS_PER_NUMBER_COUNT:I = 0x4

.field private static final DEFAULT_EVEN_NUMBERS_COUNT:I = 0x6

.field private static final DEFAULT_ROW_COUNT:I = 0xa

.field private static final DEFAULT_TRAINING_TIME:I = 0x1d4c0


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 27
    new-instance v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;-><init>()V

    .line 29
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setRowCount(I)V

    .line 30
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setColumnCount(I)V

    .line 31
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setEvenNumberCount(I)V

    .line 32
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setDigitsPerNumber(I)V

    .line 33
    const v1, 0x1d4c0

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setTrainingDuration(I)V

    .line 35
    return-object v0
.end method
