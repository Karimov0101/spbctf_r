.class public Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "FlashWordsResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;",
        "Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/result/view/IFlashWordsResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090081
    .end annotation
.end field

.field durationTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0022
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900db
    .end annotation
.end field

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

.field private resultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 61
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;-><init>()V

    .line 62
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 64
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 65
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getFlashWordsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/FlashWordsResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->resultId:I

    .line 74
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    const v1, 0x7f0b00be

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 88
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 158
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;->close()V

    .line 160
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 166
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 93
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/result/presenter/IFlashWordsResultPresenter;->initTrainingResults(I)V

    .line 95
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p1, "greenDotResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 114
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 115
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;->getConfig()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->getTrainingDuration()J

    move-result-wide v12

    long-to-float v8, v12

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 118
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->durationTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 119
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 120
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 121
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 122
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 123
    new-instance v8, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 129
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 130
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 131
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 132
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 134
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 135
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 139
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 140
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 141
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 143
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 144
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 145
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 147
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 148
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 149
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 151
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 152
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 153
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 154
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 5
    .param p1, "duration"    # J

    .prologue
    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->durationTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0024

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/result/view/FlashWordsResultFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    return-void
.end method
