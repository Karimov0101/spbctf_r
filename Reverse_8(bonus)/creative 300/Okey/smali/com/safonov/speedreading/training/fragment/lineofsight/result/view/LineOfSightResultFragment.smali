.class public Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "LineOfSightResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field foundMistakesPercentTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090006
    .end annotation
.end field

.field foundMistakesPercentTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e00ae
    .end annotation
.end field

.field foundMistakesTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09009d
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900db
    .end annotation
.end field

.field private lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

.field mistakesTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090108
    .end annotation
.end field

.field private numberFormat:Ljava/text/NumberFormat;

.field private resultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;)Ljava/text/NumberFormat;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->numberFormat:Ljava/text/NumberFormat;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 66
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;-><init>()V

    .line 67
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 70
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 54
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getLineOfSightRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 56
    new-instance v1, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/LineOfSightResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/ILineOfSightRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 75
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 76
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->resultId:I

    .line 79
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 92
    const v1, 0x7f0b0047

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 93
    .local v0, "view":Landroid/view/View;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->numberFormat:Ljava/text/NumberFormat;

    .line 94
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->numberFormat:Ljava/text/NumberFormat;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 95
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 97
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 178
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 179
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;->close()V

    .line 180
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineOfSightRealmUtil:Lcom/safonov/speedreading/training/fragment/lineofsight/repository/LineOfSightRealmUtil;

    .line 182
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->numberFormat:Ljava/text/NumberFormat;

    .line 183
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 184
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 188
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 189
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 191
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 102
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/presenter/ILineOfSightResultPresenter;->initTrainingResults(I)V

    .line 104
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "lineOfSightResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;>;"
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 133
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 134
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 135
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;->getFoundMistakesAccuracy()F

    move-result v8

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 138
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->foundMistakesPercentTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 139
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 140
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 141
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 142
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 143
    new-instance v8, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 149
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 150
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 151
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 152
    invoke-virtual {v2, v11}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 154
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 155
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 159
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 160
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 161
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 163
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 164
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v5, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 165
    invoke-virtual {v5, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 167
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 168
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    invoke-virtual {v6, v11}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 169
    invoke-virtual {v6, v12}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 171
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 172
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 173
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 174
    return-void
.end method

.method public updateFoundMistakesPercentView(F)V
    .locals 4
    .param p1, "foundMistakesPercent"    # F

    .prologue
    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->foundMistakesPercentTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->numberFormat:Ljava/text/NumberFormat;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    return-void
.end method

.method public updateFoundMistakesView(I)V
    .locals 2
    .param p1, "mistakesCount"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->foundMistakesTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 114
    return-void
.end method

.method public updateMistakesView(I)V
    .locals 2
    .param p1, "mistakesCount"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->mistakesTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 109
    return-void
.end method
