.class public Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;
.super Ljava/lang/Object;
.source "ReaderTimerModeSaveUtil.java"


# static fields
.field private static final ACCELERATOR_COURSE_TIMER_MODE_JSON:Ljava/lang/String; = "accelerator_course_timer_mode_json"

.field private static final COURSE_SAVE_PREFERENCES:Ljava/lang/String; = "course_save_preferences"

.field private static final PASS_COURSE_TIMER_MODE_JSON:Ljava/lang/String; = "pass_course_1_timer_mode_json"


# instance fields
.field private final sharedPreferences:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "course_save_preferences"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 22
    return-void
.end method


# virtual methods
.method public load(I)Lcom/safonov/speedreading/training/util/course/TimerModeSave;
    .locals 4
    .param p1, "timerMode"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 50
    packed-switch p1, :pswitch_data_0

    .line 58
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "This course type is unsupported"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 52
    :pswitch_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "pass_course_1_timer_mode_json"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    .local v0, "json":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_0

    .line 62
    new-instance v1, Lcom/google/gson/Gson;

    invoke-direct {v1}, Lcom/google/gson/Gson;-><init>()V

    const-class v2, Lcom/safonov/speedreading/training/util/course/TimerModeSave;

    invoke-virtual {v1, v0, v2}, Lcom/google/gson/Gson;->fromJson(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/util/course/TimerModeSave;

    .line 65
    :cond_0
    return-object v1

    .line 55
    .end local v0    # "json":Ljava/lang/String;
    :pswitch_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v3, "accelerator_course_timer_mode_json"

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 56
    .restart local v0    # "json":Ljava/lang/String;
    goto :goto_0

    .line 50
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public reset(I)V
    .locals 3
    .param p1, "timerMode"    # I

    .prologue
    const/4 v2, 0x0

    .line 70
    packed-switch p1, :pswitch_data_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This course type is unsupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :pswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pass_course_1_timer_mode_json"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 80
    :goto_0
    return-void

    .line 75
    :pswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accelerator_course_timer_mode_json"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 70
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public save(I[J)V
    .locals 4
    .param p1, "timerMode"    # I
    .param p2, "remainingTimes"    # [J

    .prologue
    .line 28
    new-instance v1, Lcom/safonov/speedreading/training/util/course/TimerModeSave;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/util/course/TimerModeSave;-><init>()V

    .line 30
    .local v1, "save":Lcom/safonov/speedreading/training/util/course/TimerModeSave;
    iput-object p2, v1, Lcom/safonov/speedreading/training/util/course/TimerModeSave;->remainingTimes:[J

    .line 32
    new-instance v2, Lcom/google/gson/Gson;

    invoke-direct {v2}, Lcom/google/gson/Gson;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/gson/Gson;->toJson(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 34
    .local v0, "json":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 42
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "This course type is unsupported"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 36
    :pswitch_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pass_course_1_timer_mode_json"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 44
    :goto_0
    return-void

    .line 39
    :pswitch_1
    iget-object v2, p0, Lcom/safonov/speedreading/training/util/course/ReaderTimerModeSaveUtil;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "accelerator_course_timer_mode_json"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    .line 34
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
