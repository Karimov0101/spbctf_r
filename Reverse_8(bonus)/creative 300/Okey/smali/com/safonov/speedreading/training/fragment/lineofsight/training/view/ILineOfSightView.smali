.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/ILineOfSightView;
.super Ljava/lang/Object;
.source "ILineOfSightView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/lineofsight/training/view/LineOfSightTrainingCompleteListener;


# virtual methods
.method public abstract cancelPreShowAnimation()V
.end method

.method public abstract initBoardView(III)V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract setCheckButtonEnabled(Z)V
.end method

.method public abstract setCheckedItemsData(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract setItemsData(Ljava/util/List;)V
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract startPreShowAnimation()V
.end method

.method public abstract updateProgressBar(I)V
.end method
