.class public Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
.super Lio/realm/RealmObject;
.source "RememberNumberResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/RememberNumberResultRealmProxyInterface;


# static fields
.field public static final FIELD_CONFIG_ID:Ljava/lang/String; = "config.id"

.field public static final FIELD_SCORE:Ljava/lang/String; = "score"


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private score:I

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 1

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getScore()I
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmGet$score()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->id:I

    return v0
.end method

.method public realmGet$score()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->score:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->config:Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->id:I

    return-void
.end method

.method public realmSet$score(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->score:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;)V

    .line 58
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 26
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmSet$id(I)V

    .line 27
    return-void
.end method

.method public setScore(I)V
    .locals 0
    .param p1, "score"    # I

    .prologue
    .line 39
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmSet$score(I)V

    .line 40
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;->realmSet$unixTime(J)V

    .line 50
    return-void
.end method
