.class public Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "ConcentrationFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;",
        "Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/IConcentrationPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field builder:Landroid/support/v7/app/AlertDialog$Builder;

.field circlesContatiner:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09004d
    .end annotation
.end field

.field circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

.field concentrationPointsTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09005b
    .end annotation
.end field

.field private concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

.field private config:Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

.field private configId:I

.field isPause:Z

.field layout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090061
    .end annotation
.end field

.field progressBar:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09005c
    .end annotation
.end field

.field recordTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09005d
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09005f
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 52
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;)Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;
    .locals 5
    .param p0, "configId"    # I

    .prologue
    .line 68
    const-string v2, "concentration config"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;-><init>()V

    .line 70
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 71
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 72
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->setArguments(Landroid/os/Bundle;)V

    .line 73
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/IConcentrationPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/IConcentrationPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/ConcentrationPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/training/presenter/ConcentrationPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;)V

    return-object v0
.end method

.method public hidePointsTextView()V
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 221
    return-void
.end method

.method public initProgressBar(I)V
    .locals 1
    .param p1, "maxProgress"    # I

    .prologue
    .line 180
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 181
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 233
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

    if-eqz v0, :cond_0

    .line 234
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

    .line 239
    return-void

    .line 236
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onConcentrationTrainingCompleted(I)V
    .locals 1
    .param p1, "trainingResultId"    # I

    .prologue
    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;->onConcentrationTrainingCompleted(I)V

    .line 228
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 80
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getConcentrationRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    .line 81
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "config_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->configId:I

    .line 84
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 95
    const v1, 0x7f0b0022

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 97
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->unbinder:Lbutterknife/Unbinder;

    .line 99
    return-object v0
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    .line 244
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 245
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->cancelGame()V

    .line 248
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationCompleteListener;

    .line 249
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 104
    invoke-super/range {p0 .. p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 106
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->configId:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;->getConfig(I)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v7

    .line 108
    .local v7, "config":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getComplexity()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 109
    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->configId:I

    iget-boolean v5, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    move-object v4, p0

    invoke-direct/range {v0 .. v5}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;ILcom/safonov/speedreading/training/fragment/concentration/training/view/IConcentrationView;Z)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesContatiner:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 111
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->setVisibleStatistics(Z)V

    .line 113
    :cond_0
    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getComplexity()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 114
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v10

    .line 115
    .local v10, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f0b0021

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 117
    .local v11, "promptsView":Landroid/view/View;
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->setVisibleStatistics(Z)V

    .line 119
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    .line 121
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {v0, v11}, Landroid/support/v7/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 123
    const v0, 0x7f09004f

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/NumberPicker;

    .line 124
    .local v2, "count":Landroid/widget/NumberPicker;
    const v0, 0x7f090050

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/NumberPicker;

    .line 125
    .local v6, "size":Landroid/widget/NumberPicker;
    const v0, 0x7f09004c

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/NumberPicker;

    .line 127
    .local v4, "speed":Landroid/widget/NumberPicker;
    const/16 v0, 0x1e

    invoke-virtual {v2, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 128
    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesCountCustom()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 129
    const/4 v0, 0x4

    invoke-virtual {v2, v0}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 131
    const/4 v0, 0x6

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "1"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string v1, "3"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    const-string v1, "5"

    aput-object v1, v3, v0

    const/4 v0, 0x3

    const-string v1, "7"

    aput-object v1, v3, v0

    const/4 v0, 0x4

    const-string v1, "9"

    aput-object v1, v3, v0

    const/4 v0, 0x5

    const-string v1, "14"

    aput-object v1, v3, v0

    .line 132
    .local v3, "speedValues":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 133
    invoke-virtual {v4, v3}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    .line 134
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v4, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 136
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    array-length v0, v3

    if-ge v9, v0, :cond_2

    .line 137
    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSpeedCustom()I

    move-result v0

    aget-object v1, v3, v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 138
    invoke-virtual {v4, v9}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 136
    :cond_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 142
    :cond_2
    const/4 v0, 0x5

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "30"

    aput-object v1, v5, v0

    const/4 v0, 0x1

    const-string v1, "50"

    aput-object v1, v5, v0

    const/4 v0, 0x2

    const-string v1, "70"

    aput-object v1, v5, v0

    const/4 v0, 0x3

    const-string v1, "90"

    aput-object v1, v5, v0

    const/4 v0, 0x4

    const-string v1, "110"

    aput-object v1, v5, v0

    .line 144
    .local v5, "valueSet":[Ljava/lang/String;
    const/4 v0, 0x0

    invoke-virtual {v6, v0}, Landroid/widget/NumberPicker;->setMinValue(I)V

    .line 145
    invoke-virtual {v6, v5}, Landroid/widget/NumberPicker;->setDisplayedValues([Ljava/lang/String;)V

    .line 146
    array-length v0, v5

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v6, v0}, Landroid/widget/NumberPicker;->setMaxValue(I)V

    .line 148
    const/4 v9, 0x0

    :goto_1
    array-length v0, v5

    if-ge v9, v0, :cond_4

    .line 149
    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;->getCirclesSizeCustom()I

    move-result v0

    aget-object v1, v5, v9

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 150
    invoke-virtual {v6, v9}, Landroid/widget/NumberPicker;->setValue(I)V

    .line 148
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 154
    :cond_4
    iget-object v12, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    const-string v13, "\u0417\u0430\u043f\u0443\u0441\u0442\u0438\u0442\u044c"

    new-instance v0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;Landroid/widget/NumberPicker;[Ljava/lang/String;Landroid/widget/NumberPicker;[Ljava/lang/String;Landroid/widget/NumberPicker;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    invoke-virtual {v12, v13, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->builder:Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v8

    .line 169
    .local v8, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v8}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 171
    .end local v2    # "count":Landroid/widget/NumberPicker;
    .end local v3    # "speedValues":[Ljava/lang/String;
    .end local v4    # "speed":Landroid/widget/NumberPicker;
    .end local v5    # "valueSet":[Ljava/lang/String;
    .end local v6    # "size":Landroid/widget/NumberPicker;
    .end local v8    # "dialog":Landroid/support/v7/app/AlertDialog;
    .end local v9    # "i":I
    .end local v10    # "inflater":Landroid/view/LayoutInflater;
    .end local v11    # "promptsView":Landroid/view/View;
    :cond_5
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    .line 254
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->cancelGame()V

    .line 256
    :cond_0
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->isPause:Z

    .line 261
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->circlesView:Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/concentration/training/model/MovementView;->startLevel()V

    .line 263
    :cond_0
    return-void
.end method

.method public setPointsTextViewCorrect(I)V
    .locals 5
    .param p1, "points"    # I

    .prologue
    .line 207
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 208
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v1, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 209
    .local v1, "currentLocale":Ljava/util/Locale;
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "+"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 210
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f06001a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 211
    const/4 v0, 0x0

    .line 212
    .local v0, "anim":Landroid/view/animation/Animation;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f010011

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 213
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->concentrationPointsTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 214
    return-void
.end method

.method public setVisibleStatistics(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 197
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->layout:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 201
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->layout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public updateBestScoreView(I)V
    .locals 5
    .param p1, "record"    # I

    .prologue
    .line 190
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->recordTextView:Landroid/widget/TextView;

    const v1, 0x7f0e014b

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    return-void
.end method

.method public updateProgressBar(I)V
    .locals 1
    .param p1, "progress"    # I

    .prologue
    .line 185
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->progressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 186
    return-void
.end method

.method public updateScoreView(I)V
    .locals 2
    .param p1, "score"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/training/view/ConcentrationFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    return-void
.end method
