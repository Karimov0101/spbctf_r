.class public interface abstract Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/ILineOfSightResultView;
.super Ljava/lang/Object;
.source "ILineOfSightResultView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;


# virtual methods
.method public abstract setChartViewData(Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract updateFoundMistakesPercentView(F)V
.end method

.method public abstract updateFoundMistakesView(I)V
.end method

.method public abstract updateMistakesView(I)V
.end method
