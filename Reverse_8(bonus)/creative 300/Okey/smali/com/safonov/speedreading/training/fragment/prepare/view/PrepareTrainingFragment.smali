.class public Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;
.super Landroid/support/v4/app/Fragment;
.source "PrepareTrainingFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# static fields
.field private static final SHOW_TIME:I = 0x5dc


# instance fields
.field circularProgressBar:Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090144
    .end annotation
.end field

.field private prepareFragmentListener:Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

.field private progressAnimator:Landroid/animation/ObjectAnimator;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;)Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->prepareFragmentListener:Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    return-object v0
.end method

.method public static newInstance()Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 110
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 111
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    if-eqz v0, :cond_0

    .line 112
    check-cast p1, Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->prepareFragmentListener:Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    .line 117
    return-void

    .line 114
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement PrepareFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 39
    const v1, 0x7f0b0080

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 41
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->unbinder:Lbutterknife/Unbinder;

    .line 43
    new-instance v1, Landroid/animation/ObjectAnimator;

    invoke-direct {v1}, Landroid/animation/ObjectAnimator;-><init>()V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    .line 44
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->circularProgressBar:Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 45
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    const-string v2, "progress"

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setPropertyName(Ljava/lang/String;)V

    .line 46
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setIntValues([I)V

    .line 47
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 48
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 73
    return-object v0

    .line 46
    :array_0
    .array-data 4
        0x0
        0x64
    .end array-data
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 98
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 100
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->unbinder:Lbutterknife/Unbinder;

    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 103
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    .line 104
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 121
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 122
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->prepareFragmentListener:Lcom/safonov/speedreading/training/fragment/prepare/PrepareFragmentListener;

    .line 123
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 78
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 79
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 80
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 87
    :cond_0
    return-void
.end method

.method public resumeAnimations()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->progressAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 94
    :cond_0
    return-void
.end method
