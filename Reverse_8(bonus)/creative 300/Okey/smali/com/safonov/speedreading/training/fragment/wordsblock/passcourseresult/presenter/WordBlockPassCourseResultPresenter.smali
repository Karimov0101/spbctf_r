.class public Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/WordBlockPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordBlockPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/IWordBlockPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/view/IWordBlockPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/IWordBlockPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/WordBlockPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    .line 18
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 4
    .param p1, "resultId"    # I

    .prologue
    .line 22
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/WordBlockPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    invoke-interface {v1, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v0

    .line 24
    .local v0, "result":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/WordBlockPassCourseResultPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 25
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/presenter/WordBlockPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/view/IWordBlockPassCourseResultView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;->getConfig()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getTrainingDuration()J

    move-result-wide v2

    invoke-interface {v1, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/passcourseresult/view/IWordBlockPassCourseResultView;->updateTrainingDurationView(J)V

    .line 27
    :cond_0
    return-void
.end method
