.class public Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "ConcentrationComplexityFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityFragment;"
    }
.end annotation


# static fields
.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "config_id"


# instance fields
.field private configId:I

.field private fragmentListner:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

.field private mathRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 43
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;-><init>()V

    .line 44
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 46
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->setArguments(Landroid/os/Bundle;)V

    .line 47
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;
    .locals 3

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 35
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getConcentrationRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    .line 36
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->mathRealmUtil:Lcom/safonov/speedreading/training/fragment/concentration/repository/ConcentrationRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/ConcentrationComplexityPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/concentration/repository/IConcentrationRepository;)V

    return-object v1
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 94
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

    if-eqz v0, :cond_0

    .line 95
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

    .line 100
    return-void

    .line 97
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement GreenDotTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->configId:I

    .line 56
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const v1, 0x7f0b001f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 72
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->unbinder:Lbutterknife/Unbinder;

    .line 74
    return-object v0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;->initConfig(I)V

    .line 62
    return-void
.end method

.method public setComplexityFirst(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090059
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;->setComplexity(I)V

    .line 82
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;->onConcentrationComplexityStartClick()V

    .line 83
    return-void
.end method

.method public setComplexitySecond(Landroid/widget/TextView;)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09005a
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/presenter/IConcentrationComplexityPresenter;->setComplexity(I)V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/ConcentrationComplexityFragment;->fragmentListner:Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/concentration/complexity/view/IConcentrationComplexityListner;->onConcentrationComplexityStartClick()V

    .line 89
    return-void
.end method
