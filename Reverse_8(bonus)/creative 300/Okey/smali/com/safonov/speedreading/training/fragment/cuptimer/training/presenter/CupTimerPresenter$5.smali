.class Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;
.super Ljava/lang/Object;
.source "CupTimerPresenter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x2

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->hideNotification()V

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$200(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->setImage(I)V

    .line 164
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->showImage()V

    .line 166
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v2

    mul-long/2addr v2, v4

    invoke-interface {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/view/ICupTimerView;->setMaxProgress(J)V

    .line 167
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$300(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;J)V

    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter$5;->this$0:Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$400(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;->getDuration()J

    move-result-wide v2

    mul-long/2addr v2, v4

    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;->access$500(Lcom/safonov/speedreading/training/fragment/cuptimer/training/presenter/CupTimerPresenter;J)V

    .line 169
    return-void
.end method
