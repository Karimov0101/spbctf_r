.class public Lcom/safonov/speedreading/training/activity/FlashWordsConfigUtil;
.super Ljava/lang/Object;
.source "FlashWordsConfigUtil.java"


# static fields
.field private static final ACCELERATOR_COURSE_TYPE_1_BOARD_TYPE:I = 0x1

.field private static final ACCELERATOR_COURSE_TYPE_2_BOARD_TYPE:I = 0x2

.field private static final ACCELERATOR_COURSE_TYPE_3_BOARD_TYPE:I = 0x3

.field private static final DEFAULT_DURATION:I = 0xea60

.field private static final DEFAULT_SPEED:I = 0xc8


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getAcceleratorCourse1Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 33
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;-><init>()V

    .line 35
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setSpeed(I)V

    .line 36
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setBoarType(I)V

    .line 37
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setTrainingDuration(J)V

    .line 39
    return-object v0
.end method

.method public static getAcceleratorCourse2Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;-><init>()V

    .line 46
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setSpeed(I)V

    .line 47
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setBoarType(I)V

    .line 48
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setTrainingDuration(J)V

    .line 50
    return-object v0
.end method

.method public static getAcceleratorCourse3Config()Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 55
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;-><init>()V

    .line 57
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setSpeed(I)V

    .line 58
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setBoarType(I)V

    .line 59
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setTrainingDuration(J)V

    .line 61
    return-object v0
.end method

.method public static getUserConfig(IJ)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    .locals 3
    .param p0, "boardType"    # I
    .param p1, "duration"    # J
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 18
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;-><init>()V

    .line 20
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setSpeed(I)V

    .line 21
    invoke-virtual {v0, p0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setBoarType(I)V

    .line 22
    invoke-virtual {v0, p1, p2}, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;->setTrainingDuration(J)V

    .line 24
    return-object v0
.end method
