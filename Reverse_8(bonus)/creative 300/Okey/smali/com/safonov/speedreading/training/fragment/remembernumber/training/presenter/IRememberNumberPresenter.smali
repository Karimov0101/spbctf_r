.class public interface abstract Lcom/safonov/speedreading/training/fragment/remembernumber/training/presenter/IRememberNumberPresenter;
.super Ljava/lang/Object;
.source "IRememberNumberPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/remembernumber/training/view/IRememberNumberView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract cancelTraining()V
.end method

.method public abstract onBackspaceButtonPressed()V
.end method

.method public abstract onNumberButtonPressed(Ljava/lang/String;)V
.end method

.method public abstract pauseTraining()V
.end method

.method public abstract resumeTraining()V
.end method

.method public abstract startTraining(I)V
.end method
