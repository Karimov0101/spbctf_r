.class public Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;
.super Lio/realm/RealmObject;
.source "WordsColumnsResult.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lcom/safonov/speedreading/application/realm/UnixTimeRealmObject;
.implements Lio/realm/WordsColumnsResultRealmProxyInterface;


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private unixTime:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getConfig()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmGet$config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    return-object v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getUnixTime()J
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmGet$unixTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;
    .locals 1

    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    return-object v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->id:I

    return v0
.end method

.method public realmGet$unixTime()J
    .locals 2

    iget-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->unixTime:J

    return-wide v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->config:Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->id:I

    return-void
.end method

.method public realmSet$unixTime(J)V
    .locals 1

    iput-wide p1, p0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->unixTime:J

    return-void
.end method

.method public setConfig(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;)V
    .locals 0
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmSet$config(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;)V

    .line 47
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 28
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmSet$id(I)V

    .line 29
    return-void
.end method

.method public setUnixTime(J)V
    .locals 1
    .param p1, "unixTime"    # J

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;->realmSet$unixTime(J)V

    .line 34
    return-void
.end method
