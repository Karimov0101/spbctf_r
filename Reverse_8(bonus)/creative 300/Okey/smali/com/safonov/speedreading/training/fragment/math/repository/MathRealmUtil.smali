.class public Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "MathRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 24
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V
    .locals 6
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;

    .prologue
    .line 110
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    const-string v3, "duration"

    .line 111
    invoke-virtual {p1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Long;)Lio/realm/RealmQuery;

    move-result-object v2

    .line 112
    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 114
    .local v0, "configs":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;>;"
    invoke-virtual {v0}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    invoke-virtual {v0}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 136
    :goto_0
    return-void

    .line 119
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v1

    .line 121
    .local v1, "nextId":I
    invoke-virtual {p1, v1}, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;->setId(I)V

    .line 123
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$5;

    invoke-direct {v4, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnMultiTransactionCallback;)V
    .locals 0
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnMultiTransactionCallback;

    .prologue
    .line 141
    return-void
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;ILcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;

    .prologue
    .line 68
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 70
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$1;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;ILcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$2;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;Lcom/safonov/speedreading/training/fragment/math/repository/IMathRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 90
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    .locals 3
    .param p1, "configId"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 38
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 40
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .line 37
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 56
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .line 55
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 63
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 62
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;
    .locals 3
    .param p1, "id"    # I
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .line 29
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 48
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 47
    return-object v0
.end method

.method public updateConfigComplexity(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "complexity"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$3;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/math/repository/MathRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 106
    return-void
.end method
