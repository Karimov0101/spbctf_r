.class public Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "SchulteTableResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/schultetable/result/view/ISchulteTableResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field bestTimeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090183
    .end annotation
.end field

.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090184
    .end annotation
.end field

.field newBestTimeView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090185
    .end annotation
.end field

.field private schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090186
    .end annotation
.end field

.field timeTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e016c
    .end annotation
.end field

.field private trainingResultId:I

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 57
    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 63
    new-instance v1, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;-><init>()V

    .line 64
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 52
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getSchulteTableRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/SchulteTableResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository;)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->trainingResultId:I

    .line 76
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 89
    const v1, 0x7f0b00a5

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 91
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 93
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 172
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 173
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;->close()V

    .line 174
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->schulteTableRealmUtil:Lcom/safonov/speedreading/training/fragment/schultetable/repository/SchulteTableRealmUtil;

    .line 176
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 177
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 181
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 182
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 184
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 98
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->trainingResultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/schultetable/result/presenter/ISchulteTableResultPresenter;->initTrainingResults(I)V

    .line 100
    return-void
.end method

.method public setData(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "schulteTableResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 129
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;->getTime()J

    move-result-wide v12

    long-to-float v8, v12

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 128
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 132
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->timeTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 133
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 134
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 135
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 136
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 137
    new-instance v8, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 143
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 144
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 145
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 146
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 148
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 149
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 151
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 153
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 154
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 155
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 157
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 158
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 159
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 161
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 162
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 163
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 165
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 166
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 167
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 168
    return-void
.end method

.method public setNewBestTimeViewVisibility(Z)V
    .locals 2
    .param p1, "isVisible"    # Z

    .prologue
    .line 114
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->newBestTimeView:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 115
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public updateBestTimeView(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->bestTimeTextView:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method public updateTimeView(J)V
    .locals 3
    .param p1, "time"    # J

    .prologue
    .line 104
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/result/view/SchulteTableResultFragment;->timeTextView:Landroid/widget/TextView;

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    return-void
.end method
