.class public Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;
.super Ljava/lang/Object;
.source "TrueColorsModel.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/truecolors/training/model/ITrueColorsModel;


# instance fields
.field private color:I

.field private colorName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .param p1, "colorName"    # Ljava/lang/String;
    .param p2, "color"    # I

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->colorName:Ljava/lang/String;

    .line 15
    iput p2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->color:I

    .line 16
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->color:I

    return v0
.end method

.method public getColorName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->colorName:Ljava/lang/String;

    return-object v0
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 31
    iput p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->color:I

    .line 32
    return-void
.end method

.method public setColorName(Ljava/lang/String;)V
    .locals 0
    .param p1, "colorName"    # Ljava/lang/String;

    .prologue
    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/model/TrueColorsModel;->colorName:Ljava/lang/String;

    .line 24
    return-void
.end method
