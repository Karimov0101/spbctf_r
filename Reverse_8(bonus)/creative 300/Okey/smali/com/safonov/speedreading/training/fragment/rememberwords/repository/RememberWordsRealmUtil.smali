.class public Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;
.super Lcom/safonov/speedreading/application/realm/RealmUtil;
.source "RememberWordsRealmUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository;


# direct methods
.method public constructor <init>(Lio/realm/Realm;)V
    .locals 0
    .param p1, "realm"    # Lio/realm/Realm;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/application/realm/RealmUtil;-><init>(Lio/realm/Realm;)V

    .line 25
    return-void
.end method


# virtual methods
.method public addOrFindConfig(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V
    .locals 5
    .param p1, "config"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;

    .prologue
    .line 110
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v3, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v2, v3}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v2

    invoke-virtual {v2}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v1

    .line 112
    .local v1, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;>;"
    invoke-virtual {v1}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 113
    invoke-virtual {v1}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v2

    check-cast v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->getId()I

    move-result v2

    invoke-interface {p2, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;->onTransactionCompleted(I)V

    .line 134
    :goto_0
    return-void

    .line 117
    :cond_0
    const-class v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 119
    .local v0, "nextId":I
    invoke-virtual {p1, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->setId(I)V

    .line 121
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v3, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$4;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$4;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;)V

    new-instance v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$5;

    invoke-direct {v4, p0, p2, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$5;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v2, v3, v4}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_0
.end method

.method public addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;)V
    .locals 9
    .param p1, "configs"    # [Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "callback"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;

    .prologue
    .line 139
    const-class v6, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p0, v6}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v4

    .line 141
    .local v4, "nextId":I
    array-length v6, p1

    new-array v1, v6, [I

    .line 142
    .local v1, "configIds":[I
    new-instance v2, Ljava/util/ArrayList;

    array-length v6, p1

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 144
    .local v2, "haveToSaveConfigList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, p1

    if-ge v3, v6, :cond_1

    .line 145
    aget-object v0, p1, v3

    .line 147
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v7, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v6, v7}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v6

    const-string v7, "startWordsCount"

    .line 148
    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->getStartWordsCount()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v6

    .line 149
    invoke-virtual {v6}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v5

    .line 151
    .local v5, "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;>;"
    invoke-virtual {v5}, Lio/realm/RealmResults;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 152
    invoke-virtual {v5}, Lio/realm/RealmResults;->first()Lio/realm/RealmModel;

    move-result-object v6

    check-cast v6, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->getId()I

    move-result v6

    aput v6, v1, v3

    .line 144
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 154
    :cond_0
    invoke-virtual {v0, v4}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;->setId(I)V

    .line 155
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    aput v4, v1, v3

    .line 158
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 162
    .end local v0    # "config":Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .end local v5    # "results":Lio/realm/RealmResults;, "Lio/realm/RealmResults<Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;>;"
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 163
    if-eqz p2, :cond_2

    .line 164
    invoke-interface {p2, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;->onTransactionCompleted([I)V

    .line 182
    :cond_2
    :goto_2
    return-void

    .line 169
    :cond_3
    iget-object v6, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v7, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$6;

    invoke-direct {v7, p0, v2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$6;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;Ljava/util/List;)V

    new-instance v8, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$7;

    invoke-direct {v8, p0, p2, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$7;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnMultiTransactionCallback;[I)V

    invoke-virtual {v6, v7, v8}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    goto :goto_2
.end method

.method public addResult(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;)V
    .locals 4
    .param p1, "result"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "configId"    # I
    .param p3, "callback"    # Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;

    .prologue
    .line 83
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p0, v1}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->getNextId(Ljava/lang/Class;)I

    move-result v0

    .line 85
    .local v0, "nextId":I
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;

    invoke-direct {v2, p0, p2, p1, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$2;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;ILcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;I)V

    new-instance v3, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$3;

    invoke-direct {v3, p0, p3, v0}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$3;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/IRememberWordsRepository$OnSingleTransactionCallback;I)V

    invoke-virtual {v1, v2, v3}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;Lio/realm/Realm$Transaction$OnSuccess;)Lio/realm/RealmAsyncTask;

    .line 105
    return-void
.end method

.method public getBestResult(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    .locals 3
    .param p1, "configId"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 44
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Lio/realm/Sort;->DESCENDING:Lio/realm/Sort;

    .line 46
    invoke-virtual {v0, v1, v2}, Lio/realm/RealmResults;->sort(Ljava/lang/String;Lio/realm/Sort;)Lio/realm/RealmResults;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lio/realm/RealmResults;->where()Lio/realm/RealmQuery;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .line 43
    return-object v0
.end method

.method public getConfig(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 53
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 54
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .line 53
    return-object v0
.end method

.method public getConfigList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 60
    return-object v0
.end method

.method public getResult(I)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "id"

    .line 37
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 38
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .line 36
    return-object v0
.end method

.method public getResultList(I)Ljava/util/List;
    .locals 3
    .param p1, "configId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v0

    const-string v1, "config.id"

    .line 30
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lio/realm/RealmQuery;->findAll()Lio/realm/RealmResults;

    move-result-object v0

    .line 29
    return-object v0
.end method

.method public updateConfigWordsCount(II)V
    .locals 2
    .param p1, "configId"    # I
    .param p2, "wordsCount"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;->realm:Lio/realm/Realm;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil$1;-><init>(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/RememberWordsRealmUtil;II)V

    invoke-virtual {v0, v1}, Lio/realm/Realm;->executeTransactionAsync(Lio/realm/Realm$Transaction;)Lio/realm/RealmAsyncTask;

    .line 78
    return-void
.end method
