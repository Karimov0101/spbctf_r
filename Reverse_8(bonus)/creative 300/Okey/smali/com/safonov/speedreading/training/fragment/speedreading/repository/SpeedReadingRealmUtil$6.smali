.class Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;
.super Ljava/lang/Object;
.source "SpeedReadingRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->addOrFindConfigs([Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Lcom/safonov/speedreading/training/fragment/speedreading/repository/ISpeedReadingRepository$OnMultiTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field final synthetic val$haveToSaveConfigList:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;Ljava/util/List;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 1
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$6;->val$haveToSaveConfigList:Ljava/util/List;

    invoke-virtual {p1, v0}, Lio/realm/Realm;->copyToRealm(Ljava/lang/Iterable;)Ljava/util/List;

    .line 213
    return-void
.end method
