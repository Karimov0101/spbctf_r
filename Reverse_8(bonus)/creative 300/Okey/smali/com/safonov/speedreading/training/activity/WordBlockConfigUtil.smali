.class public Lcom/safonov/speedreading/training/activity/WordBlockConfigUtil;
.super Ljava/lang/Object;
.source "WordBlockConfigUtil.java"


# static fields
.field private static final DEFAULT_DURATION:I = 0xea60

.field private static final DEFAULT_SPEED:I = 0xc8

.field private static final DEFAULT_WORD_COUNT:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 19
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;-><init>()V

    .line 21
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;
    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->setSpeed(I)V

    .line 22
    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->setTrainingDuration(J)V

    .line 23
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->setWordCount(I)V

    .line 25
    return-object v0
.end method
