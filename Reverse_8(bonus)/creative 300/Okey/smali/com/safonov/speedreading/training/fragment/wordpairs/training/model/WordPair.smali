.class public Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;
.super Ljava/lang/Object;
.source "WordPair.java"


# instance fields
.field private areWordsEqual:Z

.field private firstWord:Ljava/lang/String;

.field private secondWord:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "firstWord"    # Ljava/lang/String;
    .param p2, "secondWord"    # Ljava/lang/String;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->firstWord:Ljava/lang/String;

    .line 13
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->secondWord:Ljava/lang/String;

    .line 15
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 16
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->areWordsEqual:Z

    .line 22
    :cond_1
    :goto_0
    return-void

    .line 18
    :cond_2
    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 19
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->areWordsEqual:Z

    goto :goto_0
.end method


# virtual methods
.method public areWordsEqual()Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->areWordsEqual:Z

    return v0
.end method

.method public getFirstWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->firstWord:Ljava/lang/String;

    return-object v0
.end method

.method public getSecondWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordpairs/training/model/WordPair;->secondWord:Ljava/lang/String;

    return-object v0
.end method
