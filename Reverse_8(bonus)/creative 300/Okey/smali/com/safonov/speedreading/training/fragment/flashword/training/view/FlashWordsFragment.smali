.class public Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "FlashWordsFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;
.implements Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/flashword/training/view/IFlashWordsView;",
        "Lcom/safonov/speedreading/training/activity/view/IAnimatedFragment;"
    }
.end annotation


# static fields
.field private static final EMPTY_STRING:Ljava/lang/String; = ""

.field private static final ITEM_COUNT_PER_PART:I = 0x4

.field private static final TRAINING_CONFIG_ID_PARAM:Ljava/lang/String; = "training_config_id"


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field private configId:I

.field contentView:Landroid/view/ViewGroup;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090068
    .end annotation
.end field

.field private itemTextViewList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field levelTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900d7
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

.field speedTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901c7
    .end annotation
.end field

.field timeTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090207
    .end annotation
.end field

.field private trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 70
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;
    .locals 3
    .param p0, "configId"    # I

    .prologue
    .line 55
    new-instance v1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;-><init>()V

    .line 56
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 57
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_config_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 58
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 46
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getFlashWordsRealm()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    .line 48
    new-instance v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-direct {v0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/FlashWordsPresenter;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/flashword/repository/IFlashWordRepository;)V

    return-object v0
.end method

.method public initBoardView(I)V
    .locals 9
    .param p1, "boardType"    # I

    .prologue
    const v8, 0x7f0900c5

    const v7, 0x7f0900c4

    const v6, 0x7f0900c3

    const v5, 0x7f0900c2

    const/4 v4, 0x1

    .line 182
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 183
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    .line 185
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 186
    .local v0, "inflater":Landroid/view/LayoutInflater;
    packed-switch p1, :pswitch_data_0

    .line 227
    :goto_0
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->startTraining()V

    .line 228
    return-void

    .line 188
    :pswitch_0
    const v2, 0x7f0b0039

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 190
    .local v1, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 193
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 194
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 195
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 197
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900ca

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 199
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900bf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 201
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 205
    .end local v1    # "view":Landroid/view/View;
    :pswitch_1
    const v2, 0x7f0b003a

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 207
    .restart local v1    # "view":Landroid/view/View;
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 212
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 213
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 214
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    const v2, 0x7f0900c9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 218
    .end local v1    # "view":Landroid/view/View;
    :pswitch_2
    const v2, 0x7f0b003b

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->contentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 220
    .restart local v1    # "view":Landroid/view/View;
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 221
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 223
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-virtual {v1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 276
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 277
    instance-of v0, p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

    if-eqz v0, :cond_0

    .line 278
    check-cast p1, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

    .line 283
    return-void

    .line 280
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement FlashWordsTrainingCompleteListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onContentViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090068
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->switchTrainingPause()V

    .line 137
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_config_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->configId:I

    .line 68
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 80
    const v1, 0x7f0b003c

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 82
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 84
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 103
    :goto_0
    return-object v0

    .line 87
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 256
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->realmUtil:Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/flashword/repository/FlashWordRealmUtil;->close()V

    .line 257
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 262
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->cancelTraining()V

    .line 263
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 264
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 265
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 287
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 288
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

    .line 289
    return-void
.end method

.method public onMinusViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090113
        }
    .end annotation

    .prologue
    .line 151
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->levelDown()V

    .line 152
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 125
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onPause()V

    .line 126
    return-void
.end method

.method public onPlusViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090114
        }
    .end annotation

    .prologue
    .line 156
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->levelUp()V

    .line 157
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onResume()V

    .line 131
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 132
    return-void
.end method

.method public onSpeedDownViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901aa
        }
    .end annotation

    .prologue
    .line 141
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->speedDown()V

    .line 142
    return-void
.end method

.method public onSpeedUpViewClick()V
    .locals 1
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901c8
        }
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->speedUp()V

    .line 147
    return-void
.end method

.method public onTrainingCompleted(I)V
    .locals 1
    .param p1, "resultId"    # I

    .prologue
    .line 269
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->trainingCompleteListener:Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsTrainingCompleteListener;->onFlashWordsTrainingCompleted(I)V

    .line 270
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 108
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 109
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->configId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->init(I)V

    .line 110
    return-void
.end method

.method public pauseAnimations()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/flashword/training/presenter/IFlashWordsPresenter;->pauseTraining()V

    .line 115
    return-void
.end method

.method public resumeAnimations()V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public setItemsEmptyText()V
    .locals 3

    .prologue
    .line 248
    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 249
    .local v0, "itemTextView":Landroid/widget/TextView;
    const-string v2, ""

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 251
    .end local v0    # "itemTextView":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setItemsText(ILjava/util/List;)V
    .locals 6
    .param p1, "partIndex"    # I
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 233
    .local p2, "wordList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    mul-int/lit8 v2, p1, 0x4

    .line 234
    .local v2, "startItemIndex":I
    add-int/lit8 v4, p1, 0x1

    mul-int/lit8 v0, v4, 0x4

    .line 236
    .local v0, "endItemIndex":I
    const/4 v3, 0x0

    .line 237
    .local v3, "wordIndex":I
    move v1, v2

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 238
    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->itemTextViewList:Ljava/util/List;

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/CharSequence;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    add-int/lit8 v3, v3, 0x1

    .line 237
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 241
    :cond_0
    return-void
.end method

.method public updateLevelView(I)V
    .locals 5
    .param p1, "level"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->levelTextView:Landroid/widget/TextView;

    const v1, 0x7f0e0073

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 175
    return-void
.end method

.method public updateSpeedView(I)V
    .locals 5
    .param p1, "speed"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->speedTextView:Landroid/widget/TextView;

    const v1, 0x7f0e002d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    return-void
.end method

.method public updateTimeView(J)V
    .locals 5
    .param p1, "time"    # J

    .prologue
    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->timeTextView:Landroid/widget/TextView;

    const v1, 0x7f0e002e

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Lcom/safonov/speedreading/application/util/timeticker/TimeTickerConverterUtil;->formatToSeconds(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/safonov/speedreading/training/fragment/flashword/training/view/FlashWordsFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    return-void
.end method
