.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/cuptimer/repository/ICupTimerRepository$OnSingleTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetCupTimerConfig(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 714
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted(I)V
    .locals 6
    .param p1, "id"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 717
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    sget-object v2, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {v0, v2, p1, v5}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>(Lcom/safonov/speedreading/training/TrainingType;IZ)V

    .line 718
    .local v0, "configWrapper":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v3, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v2, v3, v5, v4, v4}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v1

    .line 720
    .local v1, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    if-eqz v2, :cond_0

    .line 721
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$23;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;

    new-instance v3, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v3, v0, v1}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    invoke-interface {v2, v3}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$ConfigResponseListener;->onConfigResponse(Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 723
    :cond_0
    return-void
.end method
