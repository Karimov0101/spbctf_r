.class Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;
.super Ljava/lang/Object;
.source "TrainingConfigUtil.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/schultetable/repository/ISchulteTableRepository$OnMultiTransactionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->requestToGetPassCourseConfigList(Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

.field final synthetic val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

.field final synthetic val$passCourseSchulteTableConfig1:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

.field final synthetic val$passCourseSchulteTableConfig2:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

.field final synthetic val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    .prologue
    .line 191
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$passCourseSchulteTableConfig1:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    iput-object p3, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$passCourseSchulteTableConfig2:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    iput-object p4, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    iput-object p5, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransactionCompleted([I)V
    .locals 11
    .param p1, "ids"    # [I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 194
    new-instance v0, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 195
    .local v0, "configWrapperType1":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    aget v7, p1, v9

    invoke-virtual {v0, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 196
    sget-object v7, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v0, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 197
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$passCourseSchulteTableConfig1:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen()Z

    move-result v7

    invoke-virtual {v0, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 199
    new-instance v1, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;-><init>()V

    .line 200
    .local v1, "configWrapperType2":Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;
    aget v7, p1, v10

    invoke-virtual {v1, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setConfigId(I)V

    .line 201
    sget-object v7, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setTrainingType(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 202
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$passCourseSchulteTableConfig2:Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v7}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen()Z

    move-result v7

    invoke-virtual {v1, v7}, Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;->setFullscreen(Z)V

    .line 204
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v8, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v7, v8, v10, v10, v9}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v3

    .line 205
    .local v3, "fragmentTypeListWithDescription":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    sget-object v8, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-static {v7, v8, v9, v10, v9}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$000(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;Lcom/safonov/speedreading/training/TrainingType;ZZZ)Ljava/util/List;

    move-result-object v2

    .line 207
    .local v2, "fragmentTypeList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/FragmentType;>;"
    new-instance v5, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v5, v0, v3}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 208
    .local v5, "trainingWrapperType1WithDescription":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    new-instance v4, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v4, v0, v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 209
    .local v4, "trainingWrapperType1":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    new-instance v6, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-direct {v6, v1, v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;-><init>(Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;Ljava/util/List;)V

    .line 211
    .local v6, "trainingWrapperType2":Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    aput-object v5, v7, v9

    .line 213
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/4 v8, 0x5

    invoke-static {v7, v10, v8, v4}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 218
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    const/16 v8, 0xa

    const/16 v9, 0xf

    invoke-static {v7, v8, v9, v6}, Ljava/util/Arrays;->fill([Ljava/lang/Object;IILjava/lang/Object;)V

    .line 223
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->this$0:Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;

    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-static {v7, v8}, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;->access$100(Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil;[Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 224
    iget-object v7, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$listener:Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;

    iget-object v8, p0, Lcom/safonov/speedreading/training/activity/model/TrainingConfigUtil$1;->val$trainingWrappers:[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    invoke-interface {v7, v8}, Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;->onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V

    .line 226
    :cond_0
    return-void
.end method
