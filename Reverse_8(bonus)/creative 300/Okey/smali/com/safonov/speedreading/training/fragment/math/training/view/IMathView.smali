.class public interface abstract Lcom/safonov/speedreading/training/fragment/math/training/view/IMathView;
.super Ljava/lang/Object;
.source "IMathView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/training/fragment/math/training/view/MathTrainingCompleteListener;


# virtual methods
.method public abstract initCountDownTimer(J)V
.end method

.method public abstract initProgressBar(I)V
.end method

.method public abstract refreshExpressionTextView()V
.end method

.method public abstract setButtonsEnabled(Z)V
.end method

.method public abstract setButtonsText([I)V
.end method

.method public abstract setCorrectAnswer()V
.end method

.method public abstract setCorrectAnswerGone()V
.end method

.method public abstract setExpressionText(Ljava/lang/String;)V
.end method

.method public abstract setIncorrectAnswer(I)V
.end method

.method public abstract setPointsTextViewCorrect(F)V
.end method

.method public abstract setPointsTextViewIncorrect()V
.end method

.method public abstract timerPause()V
.end method

.method public abstract timerStart()V
.end method

.method public abstract updateBestScoreView(I)V
.end method

.method public abstract updateProgressBar(I)V
.end method

.method public abstract updateScoreView(I)V
.end method
