.class Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;
.super Landroid/os/CountDownTimer;
.source "TrueColorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->initCountDownTimer(J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

.field final synthetic val$milliseconds:J


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;JJJ)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 146
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    iput-wide p6, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->val$milliseconds:J

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->access$100(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/truecolors/training/presenter/ITrueColorsPresenter;->finishTraining()V

    .line 156
    return-void
.end method

.method public onTick(J)V
    .locals 5
    .param p1, "millisUntilFinished"    # J

    .prologue
    .line 149
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->val$milliseconds:J

    sub-long/2addr v2, p1

    invoke-static {v0, v2, v3}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->access$002(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;J)J

    .line 150
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    iget-object v0, v0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->timerBar:Landroid/widget/ProgressBar;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;

    invoke-static {v1}, Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;->access$000(Lcom/safonov/speedreading/training/fragment/truecolors/training/view/TrueColorsFragment;)J

    move-result-wide v2

    long-to-int v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 151
    return-void
.end method
