.class public Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment_ViewBinding;
.super Ljava/lang/Object;
.source "AcceleratorCourseResultFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;

    .line 22
    const v0, 0x7f090081

    const-string v1, "field \'durationTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    .line 23
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 28
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;

    .line 29
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 30
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;

    .line 32
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/aceeleratorcourse/view/AcceleratorCourseResultFragment;->durationTextView:Landroid/widget/TextView;

    .line 33
    return-void
.end method
