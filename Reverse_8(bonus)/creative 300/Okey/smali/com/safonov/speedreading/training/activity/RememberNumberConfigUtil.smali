.class public Lcom/safonov/speedreading/training/activity/RememberNumberConfigUtil;
.super Ljava/lang/Object;
.source "RememberNumberConfigUtil.java"


# static fields
.field private static final DEFAULT_ANSWERS_TO_COMPLEXITY_DOWN:I = -0x2

.field private static final DEFAULT_ANSWERS_TO_COMPLEXITY_UP:I = 0x3

.field private static final DEFAULT_COMPLEXITY:I = 0x4

.field private static final DEFAULT_MAX_COMPLEXITY:I = 0x8

.field private static final DEFAULT_MIN_COMPLEXITY:I = 0x4

.field private static final DEFAULT_TRAINING_SHOW_COUNT:I = 0xf


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDefaultConfig()Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 44
    new-instance v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;-><init>()V

    .line 46
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setTrainingShowCount(I)V

    .line 48
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setMinComplexity(I)V

    .line 49
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setMaxComplexity(I)V

    .line 51
    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setComplexity(I)V

    .line 53
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setAnswersToComplexityUp(I)V

    .line 54
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;->setAnswersToComplexityDown(I)V

    .line 56
    return-object v0
.end method
