.class Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;
.super Ljava/lang/Object;
.source "TrainingPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/activity/model/ITrainingConfigUtil$CourseConfigResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

.field final synthetic val$trainingType:Lcom/safonov/speedreading/training/TrainingType;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    .prologue
    .line 65
    iput-object p1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iput-object p2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->val$trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCourseConfigResponse([Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)V
    .locals 4
    .param p1, "trainingWrapperArray"    # [Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .prologue
    const/4 v3, 0x0

    .line 68
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$000(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->val$trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-virtual {v1, v2}, Lcom/safonov/speedreading/training/util/course/CourseSaveUtil;->load(Lcom/safonov/speedreading/training/TrainingType;)Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;

    move-result-object v0

    .line 69
    .local v0, "save":Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;
    if-eqz v0, :cond_2

    .line 70
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v2, v0, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;->trainingResultIds:[I

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$102(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;[I)[I

    .line 71
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget v2, v0, Lcom/safonov/speedreading/training/util/course/CourseTrainingSave;->trainingCompletedCount:I

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$202(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I

    .line 77
    :goto_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$200(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I

    move-result v1

    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 82
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->onReadingCompleted()V

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1, v3}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$302(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I

    .line 86
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1, p1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$402(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;)[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    .line 88
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$400(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$200(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getConfigWrapper()Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$502(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;)Lcom/safonov/speedreading/training/activity/model/entity/ConfigWrapper;

    .line 89
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$400(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)[Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;

    move-result-object v2

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$200(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v2}, Lcom/safonov/speedreading/training/activity/model/entity/TrainingWrapper;->getFragmentTypeList()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$602(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Ljava/util/List;)Ljava/util/List;

    .line 91
    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$600(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Ljava/util/List;

    move-result-object v1

    iget-object v3, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v3}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$300(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)I

    move-result v3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/FragmentType;

    invoke-static {v2, v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$702(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)Lcom/safonov/speedreading/training/FragmentType;

    .line 92
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-virtual {v1}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->isViewAttached()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$700(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;)Lcom/safonov/speedreading/training/FragmentType;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$800(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;Lcom/safonov/speedreading/training/FragmentType;)V

    .line 95
    :cond_1
    return-void

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    invoke-static {v1, v3}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$202(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;I)I

    .line 74
    iget-object v1, p0, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter$1;->this$0:Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;

    array-length v2, p1

    new-array v2, v2, [I

    invoke-static {v1, v2}, Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;->access$102(Lcom/safonov/speedreading/training/activity/presenter/TrainingPresenter;[I)[I

    goto :goto_0
.end method
