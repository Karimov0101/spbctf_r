.class Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;
.super Ljava/lang/Object;
.source "SpeedReadingRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;->updateConfigSpeed(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

.field final synthetic val$configId:I

.field final synthetic val$speed:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;II)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    .prologue
    .line 75
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;->this$0:Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil;

    iput p2, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;->val$configId:I

    iput p3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;->val$speed:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 4
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 78
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v1}, Lio/realm/Realm;->where(Ljava/lang/Class;)Lio/realm/RealmQuery;

    move-result-object v1

    const-string v2, "id"

    iget v3, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;->val$configId:I

    .line 79
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lio/realm/RealmQuery;->equalTo(Ljava/lang/String;Ljava/lang/Integer;)Lio/realm/RealmQuery;

    move-result-object v1

    .line 80
    invoke-virtual {v1}, Lio/realm/RealmQuery;->findFirst()Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .line 82
    .local v0, "config":Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;
    if-eqz v0, :cond_0

    .line 83
    iget v1, p0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/SpeedReadingRealmUtil$1;->val$speed:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;->setSpeed(I)V

    .line 85
    :cond_0
    return-void
.end method
