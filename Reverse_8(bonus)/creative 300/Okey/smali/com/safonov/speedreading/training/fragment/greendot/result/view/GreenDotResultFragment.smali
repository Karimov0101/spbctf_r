.class public Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "GreenDotResultFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/greendot/result/view/IGreenDotResultView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/training/fragment/greendot/result/view/IGreenDotResultView;",
        "Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/IGreenDotResultPresenter;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/greendot/result/view/IGreenDotResultView;"
    }
.end annotation


# static fields
.field private static final TRAINING_RESULT_ID_PARAM:Ljava/lang/String; = "training_result_id"


# instance fields
.field chartItemCircleRadiusDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000d
    .end annotation
.end field

.field chartItemColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06006e
    .end annotation
.end field

.field chartItemValueTextSizeDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000e
    .end annotation
.end field

.field chartLineColor:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f060070
    .end annotation
.end field

.field chartLineWidthDp:I
    .annotation build Lbutterknife/BindInt;
        value = 0x7f0a000f
    .end annotation
.end field

.field durationTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900a2
    .end annotation
.end field

.field durationTitle:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e0089
    .end annotation
.end field

.field private greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

.field lineChart:Lcom/github/mikephil/charting/charts/LineChart;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900a4
    .end annotation
.end field

.field private resultId:I

.field private timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 99
    new-instance v0, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-direct {v0}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;)Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    return-object v0
.end method

.method public static newInstance(I)Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;
    .locals 3
    .param p0, "resultId"    # I

    .prologue
    .line 63
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;-><init>()V

    .line 64
    .local v1, "fragment":Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 65
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "training_result_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/IGreenDotResultPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/IGreenDotResultPresenter;
    .locals 3
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/application/App;

    .line 52
    .local v0, "app":Lcom/safonov/speedreading/application/App;
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getGreenDotRealm()Lio/realm/Realm;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 53
    new-instance v1, Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/GreenDotResultPresenter;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-direct {v1, v2}, Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/GreenDotResultPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V

    return-object v1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onCreate(Landroid/os/Bundle;)V

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "training_result_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->resultId:I

    .line 76
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 86
    const v1, 0x7f0b0040

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 88
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 90
    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 162
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 163
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    .line 165
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;->close()V

    .line 166
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->greenDotRealmUtil:Lcom/safonov/speedreading/training/fragment/greendot/repository/GreenDotRealmUtil;

    .line 168
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    .line 169
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 173
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 174
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 175
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->unbinder:Lbutterknife/Unbinder;

    .line 176
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/IGreenDotResultPresenter;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->resultId:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/result/presenter/IGreenDotResultPresenter;->initTrainingResults(I)V

    .line 97
    return-void
.end method

.method public setChartViewData(Ljava/util/List;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 116
    .local p1, "greenDotResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 118
    .local v0, "chartEntryList":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/data/Entry;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 119
    new-instance v9, Lcom/github/mikephil/charting/data/Entry;

    int-to-float v10, v4

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->getConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v8

    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v12

    long-to-float v8, v12

    invoke-direct {v9, v10, v8}, Lcom/github/mikephil/charting/data/Entry;-><init>(FF)V

    invoke-interface {v0, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 122
    :cond_0
    new-instance v2, Lcom/github/mikephil/charting/data/LineDataSet;

    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->durationTitle:Ljava/lang/String;

    invoke-direct {v2, v0, v8}, Lcom/github/mikephil/charting/data/LineDataSet;-><init>(Ljava/util/List;Ljava/lang/String;)V

    .line 123
    .local v2, "dataSet":Lcom/github/mikephil/charting/data/LineDataSet;
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->chartLineColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setColor(I)V

    .line 124
    sget-object v8, Lcom/github/mikephil/charting/data/LineDataSet$Mode;->LINEAR:Lcom/github/mikephil/charting/data/LineDataSet$Mode;

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setMode(Lcom/github/mikephil/charting/data/LineDataSet$Mode;)V

    .line 125
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->chartLineWidthDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setLineWidth(F)V

    .line 126
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->chartItemValueTextSizeDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueTextSize(F)V

    .line 127
    new-instance v8, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment$1;

    invoke-direct {v8, p0}, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment$1;-><init>(Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;)V

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setValueFormatter(Lcom/github/mikephil/charting/formatter/IValueFormatter;)V

    .line 133
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setDrawCircleHole(Z)V

    .line 134
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->chartItemColor:I

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleColor(I)V

    .line 135
    iget v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->chartItemCircleRadiusDp:I

    int-to-float v8, v8

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setCircleRadius(F)V

    .line 136
    const/4 v8, 0x0

    invoke-virtual {v2, v8}, Lcom/github/mikephil/charting/data/LineDataSet;->setHighlightEnabled(Z)V

    .line 138
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 139
    .local v3, "dataSets":Ljava/util/List;, "Ljava/util/List<Lcom/github/mikephil/charting/interfaces/datasets/ILineDataSet;>;"
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v1, Lcom/github/mikephil/charting/data/LineData;

    invoke-direct {v1, v3}, Lcom/github/mikephil/charting/data/LineData;-><init>(Ljava/util/List;)V

    .line 143
    .local v1, "data":Lcom/github/mikephil/charting/data/LineData;
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getXAxis()Lcom/github/mikephil/charting/components/XAxis;

    move-result-object v7

    .line 144
    .local v7, "xAxis":Lcom/github/mikephil/charting/components/XAxis;
    sget-object v8, Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;->BOTH_SIDED:Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setPosition(Lcom/github/mikephil/charting/components/XAxis$XAxisPosition;)V

    .line 145
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v7, v8}, Lcom/github/mikephil/charting/components/XAxis;->setGranularity(F)V

    .line 147
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisLeft()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v5

    .line 148
    .local v5, "leftAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 149
    const/4 v8, 0x1

    invoke-virtual {v5, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 151
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->getAxisRight()Lcom/github/mikephil/charting/components/YAxis;

    move-result-object v6

    .line 152
    .local v6, "rightAxis":Lcom/github/mikephil/charting/components/YAxis;
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawLabels(Z)V

    .line 153
    const/4 v8, 0x1

    invoke-virtual {v6, v8}, Lcom/github/mikephil/charting/components/YAxis;->setDrawGridLines(Z)V

    .line 155
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/github/mikephil/charting/charts/LineChart;->setDescription(Lcom/github/mikephil/charting/components/Description;)V

    .line 156
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8, v1}, Lcom/github/mikephil/charting/charts/LineChart;->setData(Lcom/github/mikephil/charting/data/ChartData;)V

    .line 157
    iget-object v8, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->lineChart:Lcom/github/mikephil/charting/charts/LineChart;

    invoke-virtual {v8}, Lcom/github/mikephil/charting/charts/LineChart;->invalidate()V

    .line 158
    return-void
.end method

.method public updateTrainingDurationView(J)V
    .locals 3
    .param p1, "duration"    # J

    .prologue
    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->durationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/greendot/result/view/GreenDotResultFragment;->timeConverterUtil:Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;

    invoke-virtual {v1, p1, p2}, Lcom/safonov/speedreading/training/fragment/greendot/util/GreenDotTimeConverterUtil;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 104
    return-void
.end method
