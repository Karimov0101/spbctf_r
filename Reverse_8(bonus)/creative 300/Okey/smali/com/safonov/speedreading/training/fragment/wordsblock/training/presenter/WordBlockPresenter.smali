.class public Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "WordBlockPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/IWordBlockPresenter;"
    }
.end annotation


# static fields
.field private static final MAX_SPEED:I = 0xbb8

.field private static final MAX_WORD_COUNT:I = 0x50

.field private static final MIN_SPEED:I = 0x64

.field private static final MIN_WORD_COUNT:I = 0x1

.field private static final SPEED_STEP:I = 0x32


# instance fields
.field private config:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

.field private currentPlayedTime:J

.field private delay:I

.field private handler:Landroid/os/Handler;

.field private random:Ljava/util/Random;

.field private repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

.field private speed:I

.field private timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

.field private trainingPaused:Z

.field private wordCount:I

.field private wordSelectorRunnable:Ljava/lang/Runnable;

.field private words:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "repository"    # Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 33
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->handler:Landroid/os/Handler;

    .line 34
    new-instance v0, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-direct {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    .line 94
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->random:Ljava/util/Random;

    .line 211
    new-instance v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$2;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)V

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    .line 29
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030020

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->words:[Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    return v0
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    return v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;I)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;
    .param p1, "x1"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getWordBlockText(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;)I
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;

    .prologue
    .line 22
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->delay:I

    return v0
.end method

.method private changeSpeed(I)V
    .locals 2
    .param p1, "speed"    # I

    .prologue
    .line 157
    invoke-static {p1}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->delay:I

    .line 159
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->updateSpeedView(I)V

    .line 162
    :cond_0
    return-void
.end method

.method private changeWordCount(I)V
    .locals 2
    .param p1, "wordCount"    # I

    .prologue
    .line 187
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    mul-int/2addr v0, p1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->delay:I

    .line 188
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->updateWordCountView(I)V

    .line 191
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    .line 192
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    .line 195
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 197
    :cond_0
    return-void
.end method

.method private getWordBlockText(I)Ljava/lang/String;
    .locals 5
    .param p1, "wordCount"    # I

    .prologue
    .line 201
    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->words:[Ljava/lang/String;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->random:Ljava/util/Random;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->words:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 203
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, p1, :cond_0

    .line 204
    const/16 v2, 0x20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 205
    iget-object v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->words:[Ljava/lang/String;

    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->random:Ljava/util/Random;

    iget-object v4, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->words:[Ljava/lang/String;

    array-length v4, v4

    invoke-virtual {v3, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method


# virtual methods
.method public cancelTraining()V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordSelectorRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 131
    return-void
.end method

.method public init(I)V
    .locals 2
    .param p1, "configId"    # I

    .prologue
    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->repository:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/IWordBlockRepository;->getConfig(I)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getSpeed()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getWordCount()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->config:Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;->getTrainingDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->currentPlayedTime:J

    .line 49
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/speedreading/util/SpeedConverterUtil;->getWordShowingTime(I)I

    move-result v0

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->delay:I

    .line 51
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    new-instance v1, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter$1;-><init>(Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;I)V

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->setTickerListener(Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker$TickerListener;)V

    .line 85
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->updateSpeedView(I)V

    .line 87
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->updateWordCountView(I)V

    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    invoke-direct {p0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->getWordBlockText(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/view/IWordBlockView;->setWordBlockItemText(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->startTraining()V

    .line 92
    :cond_0
    return-void
.end method

.method public pauseTraining()V
    .locals 2

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    .line 117
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->cancel()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->currentPlayedTime:J

    .line 118
    return-void
.end method

.method public resumeTraining()V
    .locals 4

    .prologue
    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 124
    return-void
.end method

.method public speedDown()V
    .locals 2

    .prologue
    const/16 v1, 0x64

    .line 149
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    add-int/lit8 v0, v0, -0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    .line 150
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    if-ge v0, v1, :cond_0

    .line 151
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    .line 153
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->changeSpeed(I)V

    .line 154
    return-void
.end method

.method public speedUp()V
    .locals 2

    .prologue
    const/16 v1, 0xbb8

    .line 140
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    add-int/lit8 v0, v0, 0x32

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    .line 141
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    if-le v0, v1, :cond_0

    .line 142
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    .line 144
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->speed:I

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->changeSpeed(I)V

    .line 145
    return-void
.end method

.method public startTraining()V
    .locals 4

    .prologue
    .line 102
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->timeTicker:Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;

    iget-wide v2, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->currentPlayedTime:J

    invoke-virtual {v0, v2, v3}, Lcom/safonov/speedreading/application/util/timeticker/TimeDownTicker;->start(J)V

    .line 103
    return-void
.end method

.method public switchPauseStatus()V
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->trainingPaused:Z

    if-eqz v0, :cond_0

    .line 108
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->resumeTraining()V

    .line 112
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->pauseTraining()V

    goto :goto_0
.end method

.method public wordCountMinus()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 179
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    .line 180
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    if-ge v0, v1, :cond_0

    .line 181
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    .line 183
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->changeWordCount(I)V

    .line 184
    return-void
.end method

.method public wordCountPlus()V
    .locals 2

    .prologue
    const/16 v1, 0x50

    .line 170
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    .line 171
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    if-le v0, v1, :cond_0

    .line 172
    iput v1, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    .line 174
    :cond_0
    iget v0, p0, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->wordCount:I

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/training/fragment/wordsblock/training/presenter/WordBlockPresenter;->changeWordCount(I)V

    .line 175
    return-void
.end method
