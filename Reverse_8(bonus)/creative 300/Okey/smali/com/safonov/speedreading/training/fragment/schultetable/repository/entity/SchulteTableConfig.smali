.class public Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
.super Lio/realm/RealmObject;
.source "SchulteTableConfig.java"

# interfaces
.implements Lcom/safonov/speedreading/application/realm/IdentityRealmObject;
.implements Lio/realm/SchulteTableConfigRealmProxyInterface;


# static fields
.field public static final FIELD_COLUMN_COUNT:Ljava/lang/String; = "columnCount"

.field public static final FIELD_FULLSCREEN:Ljava/lang/String; = "isFullscreen"

.field public static final FIELD_ROW_COUNT:Ljava/lang/String; = "rowCount"


# instance fields
.field private columnCount:I

.field private id:I
    .annotation build Lio/realm/annotations/PrimaryKey;
    .end annotation
.end field

.field private isFullscreen:Z

.field private rowCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Lio/realm/RealmObject;-><init>()V

    instance-of v0, p0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v0, :cond_0

    check-cast p0, Lio/realm/internal/RealmObjectProxy;

    .end local p0    # "this":Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;
    invoke-interface {p0}, Lio/realm/internal/RealmObjectProxy;->realm$injectObjectContext()V

    :cond_0
    return-void
.end method


# virtual methods
.method public getColumnCount()I
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmGet$columnCount()I

    move-result v0

    return v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmGet$id()I

    move-result v0

    return v0
.end method

.method public getRowCount()I
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmGet$rowCount()I

    move-result v0

    return v0
.end method

.method public isFullscreen()Z
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmGet$isFullscreen()Z

    move-result v0

    return v0
.end method

.method public realmGet$columnCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->columnCount:I

    return v0
.end method

.method public realmGet$id()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->id:I

    return v0
.end method

.method public realmGet$isFullscreen()Z
    .locals 1

    iget-boolean v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen:Z

    return v0
.end method

.method public realmGet$rowCount()I
    .locals 1

    iget v0, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->rowCount:I

    return v0
.end method

.method public realmSet$columnCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->columnCount:I

    return-void
.end method

.method public realmSet$id(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->id:I

    return-void
.end method

.method public realmSet$isFullscreen(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->isFullscreen:Z

    return-void
.end method

.method public realmSet$rowCount(I)V
    .locals 0

    iput p1, p0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->rowCount:I

    return-void
.end method

.method public setColumnCount(I)V
    .locals 0
    .param p1, "columnCount"    # I

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmSet$columnCount(I)V

    .line 54
    return-void
.end method

.method public setFullscreen(Z)V
    .locals 0
    .param p1, "fullscreen"    # Z

    .prologue
    .line 37
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmSet$isFullscreen(Z)V

    .line 38
    return-void
.end method

.method public setId(I)V
    .locals 0
    .param p1, "id"    # I

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmSet$id(I)V

    .line 30
    return-void
.end method

.method public setRowCount(I)V
    .locals 0
    .param p1, "rowCount"    # I

    .prologue
    .line 45
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;->realmSet$rowCount(I)V

    .line 46
    return-void
.end method
