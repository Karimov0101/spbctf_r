.class public Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "GreenDotPassCourseResultPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;",
        ">;",
        "Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/IGreenDotPassCourseResultPresenter;"
    }
.end annotation


# instance fields
.field private repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;)V
    .locals 0
    .param p1, "repository"    # Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    .line 19
    return-void
.end method


# virtual methods
.method public initTrainingResults(I)V
    .locals 5
    .param p1, "resultId"    # I

    .prologue
    .line 23
    iget-object v3, p0, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;->repository:Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;

    invoke-interface {v3, p1}, Lcom/safonov/speedreading/training/fragment/greendot/repository/IGreenDotRepository;->getResult(I)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v2

    .line 25
    .local v2, "greenDotResult":Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;
    invoke-virtual {v2}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;->getConfig()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v3

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;->getDuration()J

    move-result-wide v0

    .line 27
    .local v0, "duration":J
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;->isViewAttached()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;

    invoke-interface {v3, v0, v1}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;->updateTrainingDurationView(J)V

    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/presenter/GreenDotPassCourseResultPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;

    invoke-static {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/util/GreenDotScoreUtil;->getPassCourseScore(J)I

    move-result v4

    invoke-interface {v3, v4}, Lcom/safonov/speedreading/training/fragment/greendot/passcourseresult/view/IGreenDotPassCourseResultView;->updatePassCoursePointsView(I)V

    .line 31
    :cond_0
    return-void
.end method
