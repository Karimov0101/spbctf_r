.class Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;
.super Ljava/lang/Object;
.source "EvenNumbersRealmUtil.java"

# interfaces
.implements Lio/realm/Realm$Transaction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;->addOrFindConfig(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/IEvenNumberRepository$OnSingleTransactionCallback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

.field final synthetic val$config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

.field final synthetic val$nextId:I


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;I)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    .prologue
    .line 117
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->this$0:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil;

    iput-object p2, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->val$config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    iput p3, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->val$nextId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lio/realm/Realm;)V
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->val$config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    iget v1, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->val$nextId:I

    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;->setId(I)V

    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/EvenNumbersRealmUtil$3;->val$config:Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    .line 123
    return-void
.end method
