.class Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment$1;
.super Ljava/lang/Object;
.source "LineOfSightResultFragment.java"

# interfaces
.implements Lcom/github/mikephil/charting/formatter/IValueFormatter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->setChartViewData(Ljava/util/List;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    .prologue
    .line 143
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getFormattedValue(FLcom/github/mikephil/charting/data/Entry;ILcom/github/mikephil/charting/utils/ViewPortHandler;)Ljava/lang/String;
    .locals 4
    .param p1, "value"    # F
    .param p2, "entry"    # Lcom/github/mikephil/charting/data/Entry;
    .param p3, "dataSetIndex"    # I
    .param p4, "viewPortHandler"    # Lcom/github/mikephil/charting/utils/ViewPortHandler;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment$1;->this$0:Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;

    invoke-static {v0}, Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;->access$000(Lcom/safonov/speedreading/training/fragment/lineofsight/result/view/LineOfSightResultFragment;)Ljava/text/NumberFormat;

    move-result-object v0

    float-to-double v2, p1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
