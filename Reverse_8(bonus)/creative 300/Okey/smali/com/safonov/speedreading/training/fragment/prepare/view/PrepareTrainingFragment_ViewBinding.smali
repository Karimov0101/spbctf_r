.class public Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment_ViewBinding;
.super Ljava/lang/Object;
.source "PrepareTrainingFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    .line 20
    const v0, 0x7f090144

    const-string v1, "field \'circularProgressBar\'"

    const-class v2, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;

    iput-object v0, p1, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->circularProgressBar:Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;

    .line 21
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 26
    iget-object v0, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    .line 27
    .local v0, "target":Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 28
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment_ViewBinding;->target:Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;

    .line 30
    iput-object v1, v0, Lcom/safonov/speedreading/training/fragment/prepare/view/PrepareTrainingFragment;->circularProgressBar:Lcom/safonov/speedreading/training/fragment/prepare/view/CircularProgressBar;

    .line 31
    return-void
.end method
