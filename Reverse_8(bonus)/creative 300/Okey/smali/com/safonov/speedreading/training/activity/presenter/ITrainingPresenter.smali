.class public interface abstract Lcom/safonov/speedreading/training/activity/presenter/ITrainingPresenter;
.super Ljava/lang/Object;
.source "ITrainingPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/training/activity/view/ITrainingView;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract onActionBarHelpPressed()V
.end method

.method public abstract onActionBarHomePressed()V
.end method

.method public abstract onActionBarRestartPressed()V
.end method

.method public abstract onActionBarSettingsPressed()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onPauseDialogContinueClick()V
.end method

.method public abstract onPauseDialogDismiss()V
.end method

.method public abstract onPauseDialogExitClick()V
.end method

.method public abstract onPauseDialogHelpClick()V
.end method

.method public abstract onPauseDialogRestartClick()V
.end method

.method public abstract onPauseDialogSettingsClick()V
.end method

.method public abstract onPauseDialogShow()V
.end method

.method public abstract onReadingCompleted()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onStop()V
.end method

.method public abstract onTrainingCompleted(Lcom/safonov/speedreading/training/TrainingType;I)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract requestToLoadTraining(Lcom/safonov/speedreading/training/TrainingType;)V
    .param p1    # Lcom/safonov/speedreading/training/TrainingType;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
.end method

.method public abstract requestToSetNextCourseTraining()V
.end method

.method public abstract requestToSetNextFragment()V
.end method

.method public abstract restartCourse()V
.end method
