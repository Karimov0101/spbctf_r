.class public Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;
.super Ljava/lang/Object;
.source "PurchaseBookActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

.field private view2131296574:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p1}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;Landroid/view/View;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v4, 0x7f09013e

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    .line 31
    const v1, 0x7f09020d

    const-string v2, "field \'toolbar\'"

    const-class v3, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 32
    const-string v1, "field \'purchaseButton\' and method \'onPremiumButtonClick\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'purchaseButton\'"

    const-class v2, Landroid/widget/Button;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->purchaseButton:Landroid/widget/Button;

    .line 34
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->view2131296574:Landroid/view/View;

    .line 35
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    .line 47
    .local v0, "target":Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 48
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    .line 50
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 51
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->purchaseButton:Landroid/widget/Button;

    .line 53
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->view2131296574:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 54
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity_ViewBinding;->view2131296574:Landroid/view/View;

    .line 55
    return-void
.end method
