.class Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;
.super Ljava/lang/Object;
.source "PurchaseActivity.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->onPremiumMonthButtonClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/RequestListener",
        "<",
        "Lorg/solovyev/android/checkout/Purchase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .prologue
    .line 261
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 275
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    const v1, 0x7f0e0103

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 276
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 261
    check-cast p1, Lorg/solovyev/android/checkout/Purchase;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->onSuccess(Lorg/solovyev/android/checkout/Purchase;)V

    return-void
.end method

.method public onSuccess(Lorg/solovyev/android/checkout/Purchase;)V
    .locals 3
    .param p1, "result"    # Lorg/solovyev/android/checkout/Purchase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x4

    .line 264
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-static {v0, v2}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->access$002(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;Z)Z

    .line 265
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->access$100(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/application/util/PremiumUtil;->setPremiumUser(Z)V

    .line 266
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearButton:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearButton:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 268
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthButton:Landroid/support/constraint/ConstraintLayout;

    invoke-virtual {v0, v1}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 269
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumForeverButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    const v1, 0x7f0e0104

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 271
    return-void
.end method
