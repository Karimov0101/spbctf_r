.class public Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "PurchaseBookActivity.java"


# instance fields
.field private bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

.field private checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

.field private isPurchased:Z

.field purchaseButton:Landroid/widget/Button;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09013e
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 32
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBookUtil()Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    .line 33
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBilling()Lorg/solovyev/android/checkout/Billing;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Checkout;->forActivity(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ActivityCheckout;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    return-void
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->isPurchased:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)Lcom/safonov/speedreading/application/util/BookUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->isPurchased:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->setResult(I)V

    .line 100
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    .line 101
    return-void

    .line 99
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 68
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 69
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/ActivityCheckout;->onActivityResult(IILandroid/content/Intent;)Z

    .line 70
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f0b0085

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->setContentView(I)V

    .line 46
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->unbinder:Lbutterknife/Unbinder;

    .line 47
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ActivityCheckout;->start()V

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f060030

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 50
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 51
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f080066

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 53
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 75
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ActivityCheckout;->stop()V

    .line 76
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 77
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 57
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 62
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 59
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->finish()V

    .line 60
    const/4 v0, 0x1

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPremiumButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09013e
        }
    .end annotation

    .prologue
    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "inapp"

    const-string v2, "com.speedreading.alexander.speedreading.book"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 95
    return-void
.end method
