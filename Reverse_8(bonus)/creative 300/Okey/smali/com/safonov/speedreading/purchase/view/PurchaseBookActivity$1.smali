.class Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;
.super Ljava/lang/Object;
.source "PurchaseBookActivity.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->onPremiumButtonClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/RequestListener",
        "<",
        "Lorg/solovyev/android/checkout/Purchase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    .prologue
    .line 81
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    const v1, 0x7f0e0103

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 93
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 81
    check-cast p1, Lorg/solovyev/android/checkout/Purchase;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->onSuccess(Lorg/solovyev/android/checkout/Purchase;)V

    return-void
.end method

.method public onSuccess(Lorg/solovyev/android/checkout/Purchase;)V
    .locals 3
    .param p1, "result"    # Lorg/solovyev/android/checkout/Purchase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    invoke-static {v0, v2}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->access$002(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;Z)Z

    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->access$100(Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;)Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/application/util/BookUtil;->setBookPurchased(Z)V

    .line 86
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;->purchaseButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 87
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseBookActivity;

    const v1, 0x7f0e0104

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 88
    return-void
.end method
