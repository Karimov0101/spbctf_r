.class public Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;
.super Ljava/lang/Object;
.source "PurchaseActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

.field private view2131296571:Landroid/view/View;

.field private view2131296576:Landroid/view/View;

.field private view2131296577:Landroid/view/View;

.field private view2131296578:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 30
    invoke-virtual {p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;Landroid/view/View;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;Landroid/view/View;)V
    .locals 8
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v7, 0x7f090142

    const v6, 0x7f090141

    const v5, 0x7f090140

    const v4, 0x7f09013b

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .line 38
    const v1, 0x7f09020d

    const-string v2, "field \'toolbar\'"

    const-class v3, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 39
    const v1, 0x7f090143

    const-string v2, "field \'purchaseYearPrice\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearPrice:Landroid/widget/TextView;

    .line 40
    const v1, 0x7f09013c

    const-string v2, "field \'purchaseHalfYearPrice\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearPrice:Landroid/widget/TextView;

    .line 41
    const v1, 0x7f09013d

    const-string v2, "field \'purchaseMonthPrice\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthPrice:Landroid/widget/TextView;

    .line 42
    const-string v1, "field \'premiumForeverButton\' and method \'onPremiumForeverButtonClick\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 43
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'premiumForeverButton\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumForeverButton:Landroid/widget/TextView;

    .line 44
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296571:Landroid/view/View;

    .line 45
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    const-string v1, "field \'purchaseYearButton\' and method \'onPremiumYearButtonClick\'"

    invoke-static {p2, v7, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 52
    const-string v1, "field \'purchaseYearButton\'"

    const-class v2, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0, v7, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearButton:Landroid/support/constraint/ConstraintLayout;

    .line 53
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296578:Landroid/view/View;

    .line 54
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$2;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    const-string v1, "field \'purchaseHalfYearButton\' and method \'onPremiumHalfYearButtonClick\'"

    invoke-static {p2, v5, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 61
    const-string v1, "field \'purchaseHalfYearButton\'"

    const-class v2, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0, v5, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearButton:Landroid/support/constraint/ConstraintLayout;

    .line 62
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296576:Landroid/view/View;

    .line 63
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$3;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    const-string v1, "field \'purchaseMonthButton\' and method \'onPremiumMonthButtonClick\'"

    invoke-static {p2, v6, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 70
    const-string v1, "field \'purchaseMonthButton\'"

    const-class v2, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0, v6, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthButton:Landroid/support/constraint/ConstraintLayout;

    .line 71
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296577:Landroid/view/View;

    .line 72
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding$4;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .line 84
    .local v0, "target":Lcom/safonov/speedreading/purchase/view/PurchaseActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .line 87
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 88
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearPrice:Landroid/widget/TextView;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearPrice:Landroid/widget/TextView;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthPrice:Landroid/widget/TextView;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumForeverButton:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearButton:Landroid/support/constraint/ConstraintLayout;

    .line 93
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearButton:Landroid/support/constraint/ConstraintLayout;

    .line 94
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthButton:Landroid/support/constraint/ConstraintLayout;

    .line 96
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296571:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296571:Landroid/view/View;

    .line 98
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296578:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296578:Landroid/view/View;

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296576:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296576:Landroid/view/View;

    .line 102
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296577:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_ViewBinding;->view2131296577:Landroid/view/View;

    .line 104
    return-void
.end method
