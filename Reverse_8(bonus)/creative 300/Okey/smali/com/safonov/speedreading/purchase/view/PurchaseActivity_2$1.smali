.class Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;
.super Ljava/lang/Object;
.source "PurchaseActivity_2.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->onPremiumButtonClick()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/RequestListener",
        "<",
        "Lorg/solovyev/android/checkout/Purchase;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    .prologue
    .line 154
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 168
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    const v1, 0x7f0e0103

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 169
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 154
    check-cast p1, Lorg/solovyev/android/checkout/Purchase;

    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->onSuccess(Lorg/solovyev/android/checkout/Purchase;)V

    return-void
.end method

.method public onSuccess(Lorg/solovyev/android/checkout/Purchase;)V
    .locals 3
    .param p1, "result"    # Lorg/solovyev/android/checkout/Purchase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x4

    .line 157
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    invoke-static {v0, v2}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->access$002(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;Z)Z

    .line 158
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    invoke-static {v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->access$100(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/application/util/PremiumUtil;->setPremiumUser(Z)V

    .line 159
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseForeverButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseYearButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 161
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseHalfYearButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    iget-object v0, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseMonthButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    const v1, 0x7f0e0104

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 164
    return-void
.end method
