.class Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;
.super Ljava/lang/Object;
.source "PurchaseActivity.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Inventory$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoaded(Lorg/solovyev/android/checkout/Inventory$Products;)V
    .locals 11
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 97
    const-string v6, "subs"

    invoke-virtual {p1, v6}, Lorg/solovyev/android/checkout/Inventory$Products;->get(Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Product;

    move-result-object v3

    .line 99
    .local v3, "productSubs":Lorg/solovyev/android/checkout/Inventory$Product;
    const-string v6, "com.speedreading.alexander.speedreading.premium.subscription.month"

    invoke-virtual {v3, v6}, Lorg/solovyev/android/checkout/Inventory$Product;->getSku(Ljava/lang/String;)Lorg/solovyev/android/checkout/Sku;

    move-result-object v2

    .line 100
    .local v2, "monthSku":Lorg/solovyev/android/checkout/Sku;
    const-string v6, "com.speedreading.alexander.speedreading.premium.subscription.halfyear"

    invoke-virtual {v3, v6}, Lorg/solovyev/android/checkout/Inventory$Product;->getSku(Ljava/lang/String;)Lorg/solovyev/android/checkout/Sku;

    move-result-object v1

    .line 101
    .local v1, "halfYearSku":Lorg/solovyev/android/checkout/Sku;
    const-string v6, "com.speedreading.alexander.speedreading.premium.subscription.year"

    invoke-virtual {v3, v6}, Lorg/solovyev/android/checkout/Inventory$Product;->getSku(Ljava/lang/String;)Lorg/solovyev/android/checkout/Sku;

    move-result-object v5

    .line 103
    .local v5, "yearSku":Lorg/solovyev/android/checkout/Sku;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v8, v1, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-wide v8, v8, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    const/4 v10, 0x6

    invoke-virtual {v7, v8, v9, v10}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getPriceByMonth(JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v8, v1, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-object v8, v8, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getCurrencySymbol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 104
    .local v0, "halfYearPricePerMonth":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v8, v5, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-wide v8, v8, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    const/16 v10, 0xc

    invoke-virtual {v7, v8, v9, v10}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getPriceByMonth(JI)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v8, v5, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-object v8, v8, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getCurrencySymbol(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 106
    .local v4, "yearPricePerMonth":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 107
    iget-object v6, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v6, v6, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseMonthPrice:Landroid/widget/TextView;

    iget-object v7, v2, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 108
    :cond_0
    if-eqz v1, :cond_1

    .line 109
    iget-object v6, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v6, v6, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseHalfYearPrice:Landroid/widget/TextView;

    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 110
    :cond_1
    if-eqz v5, :cond_2

    .line 111
    iget-object v6, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v6, v6, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->purchaseYearPrice:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 115
    :cond_2
    return-void
.end method
