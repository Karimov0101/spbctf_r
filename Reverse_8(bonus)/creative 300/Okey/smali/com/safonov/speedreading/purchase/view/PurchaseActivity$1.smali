.class Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;
.super Ljava/lang/Object;
.source "PurchaseActivity.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Inventory$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoaded(Lorg/solovyev/android/checkout/Inventory$Products;)V
    .locals 8
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 88
    const-string v2, "inapp"

    invoke-virtual {p1, v2}, Lorg/solovyev/android/checkout/Inventory$Products;->get(Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Product;

    move-result-object v1

    .line 89
    .local v1, "productInApp":Lorg/solovyev/android/checkout/Inventory$Product;
    const-string v2, "com.speedreading.alexander.speedreading.premium"

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->getSku(Ljava/lang/String;)Lorg/solovyev/android/checkout/Sku;

    move-result-object v0

    .line 90
    .local v0, "inAppSku":Lorg/solovyev/android/checkout/Sku;
    iget-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    iget-object v2, v2, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumForeverButton:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;->this$0:Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-virtual {v3}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0078

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void
.end method
