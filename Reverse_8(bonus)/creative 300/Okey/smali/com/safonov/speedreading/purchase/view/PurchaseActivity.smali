.class public Lcom/safonov/speedreading/purchase/view/PurchaseActivity;
.super Landroid/support/v7/app/AppCompatActivity;
.source "PurchaseActivity.java"


# static fields
.field public static final ONE_CURRENCY_UNIT:I = 0xf4240


# instance fields
.field private checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

.field private isPurchased:Z

.field premiumForeverButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09013b
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field purchaseHalfYearButton:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090140
    .end annotation
.end field

.field purchaseHalfYearPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09013c
    .end annotation
.end field

.field purchaseMonthButton:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090141
    .end annotation
.end field

.field purchaseMonthPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09013d
    .end annotation
.end field

.field purchaseYearButton:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090142
    .end annotation
.end field

.field purchaseYearPrice:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090143
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 47
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 48
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBilling()Lorg/solovyev/android/checkout/Billing;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Checkout;->forActivity(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ActivityCheckout;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    return-void
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 45
    iput-boolean p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->isPurchased:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)Lcom/safonov/speedreading/application/util/PremiumUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 303
    iget-boolean v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->isPurchased:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->setResult(I)V

    .line 304
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    .line 305
    return-void

    .line 303
    :cond_0
     const/4 v0, -0x1

    goto :goto_0
.end method

.method public getCurrencySymbol(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "currencyCode"    # Ljava/lang/String;

    .prologue
    .line 176
    :try_start_0
    invoke-static {p1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    .line 177
    .local v0, "currency":Ljava/util/Currency;
    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    .line 179
    .end local v0    # "currency":Ljava/util/Currency;
    .end local p1    # "currencyCode":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 178
    .restart local p1    # "currencyCode":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 179
    .local v1, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public getPriceByMonth(JI)Ljava/lang/String;
    .locals 5
    .param p1, "price"    # J
    .param p3, "countMonth"    # I

    .prologue
    .line 186
    const-string v0, "%.2f"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    long-to-float v3, p1

    const v4, 0x49742400    # 1000000.0f

    div-float/2addr v3, v4

    int-to-float v4, p3

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 202
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 203
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/ActivityCheckout;->onActivityResult(IILandroid/content/Intent;)Z

    .line 204
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-super/range {p0 .. p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 65
    const v11, 0x7f0b0082

    invoke-virtual {p0, v11}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->setContentView(I)V

    .line 66
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v11

    iput-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->unbinder:Lbutterknife/Unbinder;

    .line 67
    iget-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v11}, Lorg/solovyev/android/checkout/ActivityCheckout;->start()V

    .line 69
    const/4 v4, 0x0

    .line 70
    .local v4, "connected":Z
    const-string v11, "connectivity"

    invoke-virtual {p0, v11}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/ConnectivityManager;

    .line 71
    .local v5, "connectivityManager":Landroid/net/ConnectivityManager;
    const/4 v11, 0x0

    invoke-virtual {v5, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v11

    sget-object v12, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-eq v11, v12, :cond_0

    const/4 v11, 0x1

    .line 72
    invoke-virtual {v5, v11}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v11

    invoke-virtual {v11}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v11

    sget-object v12, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v11, v12, :cond_1

    .line 73
    :cond_0
    const/4 v4, 0x1

    .line 79
    :goto_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 80
    .local v10, "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v11, "com.speedreading.alexander.speedreading.premium.subscription.month"

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    const-string v11, "com.speedreading.alexander.speedreading.premium.subscription.halfyear"

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    const-string v11, "com.speedreading.alexander.speedreading.premium.subscription.year"

    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    if-eqz v4, :cond_2

    .line 85
    iget-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-static {}, Lorg/solovyev/android/checkout/Inventory$Request;->create()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v12

    const-string v13, "inapp"

    const-string v14, "com.speedreading.alexander.speedreading.premium"

    invoke-virtual {v12, v13, v14}, Lorg/solovyev/android/checkout/Inventory$Request;->loadSkus(Ljava/lang/String;Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v12

    new-instance v13, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;

    invoke-direct {v13, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v11, v12, v13}, Lorg/solovyev/android/checkout/ActivityCheckout;->loadInventory(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)Lorg/solovyev/android/checkout/Inventory;

    .line 94
    iget-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-static {}, Lorg/solovyev/android/checkout/Inventory$Request;->create()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v12

    const-string v13, "subs"

    invoke-virtual {v12, v13, v10}, Lorg/solovyev/android/checkout/Inventory$Request;->loadSkus(Ljava/lang/String;Ljava/util/List;)Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v12

    new-instance v13, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;

    invoke-direct {v13, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$2;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v11, v12, v13}, Lorg/solovyev/android/checkout/ActivityCheckout;->loadInventory(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)Lorg/solovyev/android/checkout/Inventory;

    .line 122
    :goto_1
    iget-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v11}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 123
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v11

    const v12, 0x7f080066

    invoke-virtual {v11, v12}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 124
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v11

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 125
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v11

    const v12, 0x7f0e00fd

    invoke-virtual {v11, v12}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 127
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 128
    .local v6, "inflater":Landroid/view/LayoutInflater;
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 130
    .local v9, "pages":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const v11, 0x7f0b0084

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 132
    .local v7, "page":Landroid/view/View;
    const v11, 0x7f090154

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 133
    .local v3, "advantageTitleTV":Landroid/widget/TextView;
    const v11, 0x7f0e00d1

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(I)V

    .line 135
    const v11, 0x7f090152

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 136
    .local v1, "advantageDescriptionTV":Landroid/widget/TextView;
    const v11, 0x7f0e00d0

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 138
    const v11, 0x7f090153

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 139
    .local v2, "advantageImageView":Landroid/widget/ImageView;
    const v11, 0x7f0800cf

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 141
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    const v11, 0x7f0b0084

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 145
    const v11, 0x7f090154

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 146
    .restart local v3    # "advantageTitleTV":Landroid/widget/TextView;
    const v11, 0x7f0e00d7

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(I)V

    .line 148
    const v11, 0x7f090152

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 149
    .restart local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    const v11, 0x7f0e00d6

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 151
    const v11, 0x7f090153

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 152
    .restart local v2    # "advantageImageView":Landroid/widget/ImageView;
    const v11, 0x7f0800cc

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 154
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 156
    const v11, 0x7f0b0084

    const/4 v12, 0x0

    invoke-virtual {v6, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 158
    const v11, 0x7f090154

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 159
    .restart local v3    # "advantageTitleTV":Landroid/widget/TextView;
    const v11, 0x7f0e00cf

    invoke-virtual {v3, v11}, Landroid/widget/TextView;->setText(I)V

    .line 161
    const v11, 0x7f090152

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 162
    .restart local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    const v11, 0x7f0e00ce

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setText(I)V

    .line 164
    const v11, 0x7f090153

    invoke-virtual {v7, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 165
    .restart local v2    # "advantageImageView":Landroid/widget/ImageView;
    const v11, 0x7f0800ce

    invoke-virtual {v2, v11}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 167
    invoke-interface {v9, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 169
    new-instance v0, Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;

    invoke-direct {v0, v9}, Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;-><init>(Ljava/util/List;)V

    .line 170
    .local v0, "adapter":Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;
    const v11, 0x7f09012a

    invoke-virtual {p0, v11}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/support/v4/view/ViewPager;

    .line 171
    .local v8, "pager":Landroid/support/v4/view/ViewPager;
    invoke-virtual {v8, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 172
    return-void

    .line 76
    .end local v0    # "adapter":Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;
    .end local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    .end local v6    # "inflater":Landroid/view/LayoutInflater;
    .end local v7    # "page":Landroid/view/View;
    .end local v8    # "pager":Landroid/support/v4/view/ViewPager;
    .end local v9    # "pages":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v10    # "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 119
    .restart local v10    # "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_2
    iget-object v11, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->premiumForeverButton:Landroid/widget/TextView;

    const-string v12, "\u041f\u043e\u0434\u043a\u043b\u044e\u0447\u0438\u0442\u0435\u0441\u044c \u043a \u0438\u043d\u0442\u0435\u0440\u043d\u0435\u0442\u0443"

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 209
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ActivityCheckout;->stop()V

    .line 210
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 211
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 191
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 196
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 193
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->finish()V

    .line 194
    const/4 v0, 0x1

    goto :goto_0

    .line 191
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPremiumForeverButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09013b
        }
    .end annotation

    .prologue
    .line 282
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "inapp"

    const-string v2, "com.speedreading.alexander.speedreading.premium"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$6;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$6;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 299
    return-void
.end method

.method public onPremiumHalfYearButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090140
        }
    .end annotation

    .prologue
    .line 240
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "subs"

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.halfyear"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$4;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$4;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 257
    return-void
.end method

.method public onPremiumMonthButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090141
        }
    .end annotation

    .prologue
    .line 261
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "subs"

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.month"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$5;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 278
    return-void
.end method

.method public onPremiumYearButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090142
        }
    .end annotation

    .prologue
    .line 219
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "subs"

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.year"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$3;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity$3;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 236
    return-void
.end method
