.class public Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;
.super Landroid/support/v7/app/AppCompatActivity;
.source "PurchaseActivity_2.java"


# static fields
.field public static final publicKey:Ljava/lang/String; = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArncdZCT0YWSgOsMQxtFnJKkEwd4b8qE68HT1SHr+2GTwqluicDTBA1aHliTlwWBI9WROLAFK9xYeuxB2IYUWU73XBU9bMEGBzlEdOnCl9h4DT/4Qw0oxu5UILSTX6YShCSSohgWA1Q91/Y9k22UPFbgdjULJtGmGMhXZC9Tho72ctyKM0j1qjHkuu4OoznVy4aqxBK/bfytt5+nZI3lIft3U8FZ5nVsei4MoW7bZmGtb5vr0ZbEPfWHCi6MtH0HopSf7f9NWSWD3RyvfTpmdbh4DIo+JghHt5L08HmscZCNaGvBQzSx+uGqLZdgYYYQGhRSk5NsvrOMbqpo17lJmxwIDAQAB"


# instance fields
.field private checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

.field private isPurchased:Z

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field purchaseForeverButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09013f
    .end annotation
.end field

.field purchaseHalfYearButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090140
    .end annotation
.end field

.field purchaseMonthButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090141
    .end annotation
.end field

.field purchaseYearButton:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090142
    .end annotation
.end field

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/support/v7/app/AppCompatActivity;-><init>()V

    .line 38
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 39
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBilling()Lorg/solovyev/android/checkout/Billing;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Checkout;->forActivity(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ActivityCheckout;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    return-void
.end method

.method static synthetic access$002(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->isPurchased:Z

    return p1
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)Lcom/safonov/speedreading/application/util/PremiumUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-object v0
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->isPurchased:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->setResult(I)V

    .line 239
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->finish()V

    .line 240
    return-void

    .line 238
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 136
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/AppCompatActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 137
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/ActivityCheckout;->onActivityResult(IILandroid/content/Intent;)Z

    .line 138
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v8, 0x7f0b0082

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->setContentView(I)V

    .line 54
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->unbinder:Lbutterknife/Unbinder;

    .line 55
    iget-object v8, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v8}, Lorg/solovyev/android/checkout/ActivityCheckout;->start()V

    .line 68
    iget-object v8, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f060030

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    invoke-virtual {v8, v9}, Landroid/support/v7/widget/Toolbar;->setTitleTextColor(I)V

    .line 69
    iget-object v8, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    const v9, 0x7f080066

    invoke-virtual {v8, v9}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 72
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    const v9, 0x7f0e00fd

    invoke-virtual {v8, v9}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 75
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 76
    .local v4, "inflater":Landroid/view/LayoutInflater;
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 78
    .local v7, "pages":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    const v8, 0x7f0b0084

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 80
    .local v5, "page":Landroid/view/View;
    const v8, 0x7f090154

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 81
    .local v3, "advantageTitleTV":Landroid/widget/TextView;
    const v8, 0x7f0e00d1

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 83
    const v8, 0x7f090152

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 84
    .local v1, "advantageDescriptionTV":Landroid/widget/TextView;
    const v8, 0x7f0e00d0

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(I)V

    .line 86
    const v8, 0x7f090153

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 87
    .local v2, "advantageImageView":Landroid/widget/ImageView;
    const v8, 0x7f0800cf

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 89
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const v8, 0x7f0b0084

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 93
    const v8, 0x7f090154

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 94
    .restart local v3    # "advantageTitleTV":Landroid/widget/TextView;
    const v8, 0x7f0e00d7

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 96
    const v8, 0x7f090152

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 97
    .restart local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    const v8, 0x7f0e00d6

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(I)V

    .line 99
    const v8, 0x7f090153

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 100
    .restart local v2    # "advantageImageView":Landroid/widget/ImageView;
    const v8, 0x7f0800cc

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 102
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    const v8, 0x7f0b0084

    const/4 v9, 0x0

    invoke-virtual {v4, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 106
    const v8, 0x7f090154

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .end local v3    # "advantageTitleTV":Landroid/widget/TextView;
    check-cast v3, Landroid/widget/TextView;

    .line 107
    .restart local v3    # "advantageTitleTV":Landroid/widget/TextView;
    const v8, 0x7f0e00cf

    invoke-virtual {v3, v8}, Landroid/widget/TextView;->setText(I)V

    .line 109
    const v8, 0x7f090152

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 110
    .restart local v1    # "advantageDescriptionTV":Landroid/widget/TextView;
    const v8, 0x7f0e00ce

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setText(I)V

    .line 112
    const v8, 0x7f090153

    invoke-virtual {v5, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .end local v2    # "advantageImageView":Landroid/widget/ImageView;
    check-cast v2, Landroid/widget/ImageView;

    .line 113
    .restart local v2    # "advantageImageView":Landroid/widget/ImageView;
    const v8, 0x7f0800ce

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 115
    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v0, Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;

    invoke-direct {v0, v7}, Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;-><init>(Ljava/util/List;)V

    .line 118
    .local v0, "adapter":Lcom/safonov/speedreading/purchase/PurchasePagerAdapter;
    const v8, 0x7f09012a

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/support/v4/view/ViewPager;

    .line 119
    .local v6, "pager":Landroid/support/v4/view/ViewPager;
    invoke-virtual {v6, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 120
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 143
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ActivityCheckout;->stop()V

    .line 144
    invoke-super {p0}, Landroid/support/v7/app/AppCompatActivity;->onDestroy()V

    .line 145
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 125
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 130
    invoke-super {p0, p1}, Landroid/support/v7/app/AppCompatActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 127
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->finish()V

    .line 128
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPremiumButtonClick()V
    .locals 5
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09013f
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    const-string v1, "inapp"

    const-string v2, "com.speedreading.alexander.speedreading.premium"

    const/4 v3, 0x0

    new-instance v4, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;

    invoke-direct {v4, p0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lorg/solovyev/android/checkout/ActivityCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 171
    return-void
.end method
