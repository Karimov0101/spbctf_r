.class public Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;
.super Ljava/lang/Object;
.source "PurchaseActivity_2_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

.field private view2131296575:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;Landroid/view/View;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v4, 0x7f09013f

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    .line 31
    const v1, 0x7f09020d

    const-string v2, "field \'toolbar\'"

    const-class v3, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/Toolbar;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 32
    const-string v1, "field \'purchaseForeverButton\' and method \'onPremiumButtonClick\'"

    invoke-static {p2, v4, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 33
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'purchaseForeverButton\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {v0, v4, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseForeverButton:Landroid/widget/TextView;

    .line 34
    iput-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->view2131296575:Landroid/view/View;

    .line 35
    new-instance v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding$1;-><init>(Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 41
    const v1, 0x7f090142

    const-string v2, "field \'purchaseYearButton\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseYearButton:Landroid/widget/TextView;

    .line 42
    const v1, 0x7f090140

    const-string v2, "field \'purchaseHalfYearButton\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseHalfYearButton:Landroid/widget/TextView;

    .line 43
    const v1, 0x7f090141

    const-string v2, "field \'purchaseMonthButton\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseMonthButton:Landroid/widget/TextView;

    .line 44
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    .line 50
    .local v0, "target":Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->target:Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;

    .line 53
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 54
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseForeverButton:Landroid/widget/TextView;

    .line 55
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseYearButton:Landroid/widget/TextView;

    .line 56
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseHalfYearButton:Landroid/widget/TextView;

    .line 57
    iput-object v2, v0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2;->purchaseMonthButton:Landroid/widget/TextView;

    .line 59
    iget-object v1, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->view2131296575:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    iput-object v2, p0, Lcom/safonov/speedreading/purchase/view/PurchaseActivity_2_ViewBinding;->view2131296575:Landroid/view/View;

    .line 61
    return-void
.end method
