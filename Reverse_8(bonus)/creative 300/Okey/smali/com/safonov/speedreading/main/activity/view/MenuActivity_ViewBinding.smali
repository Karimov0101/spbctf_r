.class public Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;
.super Ljava/lang/Object;
.source "MenuActivity_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

.field private view2131296268:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V
    .locals 1
    .param p1, "target"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p1}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;Landroid/view/View;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;Landroid/view/View;)V
    .locals 6
    .param p1, "target"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->target:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .line 33
    const v3, 0x7f09020d

    const-string v4, "field \'toolbar\'"

    const-class v5, Landroid/support/v7/widget/Toolbar;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/Toolbar;

    iput-object v3, p1, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 34
    const v3, 0x7f090043

    const-string v4, "field \'navigation\'"

    const-class v5, Landroid/support/design/widget/BottomNavigationView;

    invoke-static {p2, v3, v4, v5}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/design/widget/BottomNavigationView;

    iput-object v3, p1, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->navigation:Landroid/support/design/widget/BottomNavigationView;

    .line 35
    const v3, 0x7f09000c

    const-string v4, "field \'purchasePremiumView\' and method \'onPremiumClick\'"

    invoke-static {p2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v2

    .line 36
    .local v2, "view":Landroid/view/View;
    iput-object v2, p1, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->purchasePremiumView:Landroid/view/View;

    .line 37
    iput-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->view2131296268:Landroid/view/View;

    .line 38
    new-instance v3, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding$1;

    invoke-direct {v3, p0, p1}, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 45
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 46
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 47
    .local v1, "res":Landroid/content/res/Resources;
    const v3, 0x7f0e00db

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->notifyKey:Ljava/lang/String;

    .line 48
    const v3, 0x7f0e00dc

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p1, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->notifyTimeKey:Ljava/lang/String;

    .line 49
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->target:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .line 55
    .local v0, "target":Lcom/safonov/speedreading/main/activity/view/MenuActivity;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 56
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->target:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .line 58
    iput-object v2, v0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    .line 59
    iput-object v2, v0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->navigation:Landroid/support/design/widget/BottomNavigationView;

    .line 60
    iput-object v2, v0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->purchasePremiumView:Landroid/view/View;

    .line 62
    iget-object v1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->view2131296268:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    iput-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity_ViewBinding;->view2131296268:Landroid/view/View;

    .line 64
    return-void
.end method
