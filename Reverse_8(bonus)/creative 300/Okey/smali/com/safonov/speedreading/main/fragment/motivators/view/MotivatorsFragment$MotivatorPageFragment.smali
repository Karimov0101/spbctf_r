.class public Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;
.super Landroid/support/v4/app/Fragment;
.source "MotivatorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MotivatorPageFragment"
.end annotation


# static fields
.field public static final MOTIVATORS_MESSAGE_PARAM:Ljava/lang/String; = "motivators_message"


# instance fields
.field private motivatorsMessage:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;
    .locals 3
    .param p0, "text"    # Ljava/lang/String;

    .prologue
    .line 127
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;

    invoke-direct {v1}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;-><init>()V

    .line 128
    .local v1, "fragment":Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 129
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "motivators_message"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 130
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->setArguments(Landroid/os/Bundle;)V

    .line 131
    return-object v1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "motivators_message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->motivatorsMessage:Ljava/lang/String;

    .line 140
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 145
    const v2, 0x7f0b0055

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 147
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f09010a

    invoke-static {v1, v2}, Lbutterknife/ButterKnife;->findById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 148
    .local v0, "textView":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->motivatorsMessage:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    return-object v1
.end method
