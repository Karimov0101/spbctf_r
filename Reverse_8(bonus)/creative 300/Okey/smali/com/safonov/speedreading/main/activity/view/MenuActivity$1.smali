.class Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;
.super Ljava/lang/Object;
.source "MenuActivity.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Inventory$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/main/activity/view/MenuActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 129
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLoaded(Lorg/solovyev/android/checkout/Inventory$Products;)V
    .locals 5
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 132
    const-string v2, "inapp"

    invoke-virtual {p1, v2}, Lorg/solovyev/android/checkout/Inventory$Products;->get(Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Product;

    move-result-object v0

    .line 133
    .local v0, "productInApp":Lorg/solovyev/android/checkout/Inventory$Product;
    const-string v2, "subs"

    invoke-virtual {p1, v2}, Lorg/solovyev/android/checkout/Inventory$Products;->get(Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Product;

    move-result-object v1

    .line 135
    .local v1, "productSubs":Lorg/solovyev/android/checkout/Inventory$Product;
    const-string v2, "com.speedreading.alexander.speedreading.book"

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v2}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$000(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/safonov/speedreading/application/util/BookUtil;->setBookPurchased(Z)V

    .line 141
    :goto_0
    const-string v2, "com.speedreading.alexander.speedreading.premiumsub"

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.speedreading.alexander.speedreading.premium"

    .line 142
    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.month"

    .line 143
    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.halfyear"

    .line 144
    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.speedreading.alexander.speedreading.premium.subscription.year"

    .line 145
    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Product;->isPurchased(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 146
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v2}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$100(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/safonov/speedreading/application/util/PremiumUtil;->setPremiumUser(Z)V

    .line 150
    :goto_1
    return-void

    .line 138
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v2}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$000(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/application/util/BookUtil;->setBookPurchased(Z)V

    goto :goto_0

    .line 148
    :cond_2
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v2}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$100(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/safonov/speedreading/application/util/PremiumUtil;->setPremiumUser(Z)V

    goto :goto_1
.end method
