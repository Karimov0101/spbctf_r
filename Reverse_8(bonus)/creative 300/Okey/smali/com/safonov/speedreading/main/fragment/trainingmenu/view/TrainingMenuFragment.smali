.class public Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "TrainingMenuFragment.java"


# instance fields
.field private menuFragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field startCup:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901d2
    .end annotation
.end field

.field startRememberWords:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901d4
    .end annotation
.end field

.field startTrueColors:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901d5
    .end annotation
.end field

.field private trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 188
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)Lcom/safonov/speedreading/main/TrainingFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    return-object v0
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAcceleratorMenuClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090212
        }
    .end annotation

    .prologue
    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 86
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 63
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 64
    instance-of v0, p1, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 70
    instance-of v0, p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    if-eqz v0, :cond_1

    .line 71
    check-cast p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 75
    return-void

    .line 67
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement TrainingFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement MenuFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 44
    const v1, 0x7f0b0053

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 46
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->unbinder:Lbutterknife/Unbinder;

    .line 48
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 54
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 55
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 79
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 80
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 81
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const v2, 0x7f0e0038

    const v1, 0x7f08007c

    .line 221
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 222
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startRememberWords:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 224
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startRememberWords:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 226
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startTrueColors:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 227
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startTrueColors:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 229
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startCup:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 230
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startCup:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 232
    :cond_0
    return-void
.end method

.method public onStartBaseCourse()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090211
        }
    .end annotation

    .prologue
    .line 155
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 156
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e00f4

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 157
    const v2, 0x7f0e00f1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 159
    const v2, 0x7f0e00f3

    new-instance v3, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 167
    const v2, 0x7f0e00f2

    new-instance v3, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$2;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 174
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 175
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 176
    return-void
.end method

.method public onStartConcentrationClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090214
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->CONCENTRATION:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 131
    return-void
.end method

.method public onStartCupTimerClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090215
        }
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->CUPTIMER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->showPurchasePremiumDialog()V

    goto :goto_0
.end method

.method public onStartEvenNumbersClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090216
        }
    .end annotation

    .prologue
    .line 115
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->EVEN_NUMBERS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 116
    return-void
.end method

.method public onStartGreenDotClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090218
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->GREEN_DOT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 121
    return-void
.end method

.method public onStartLineOfSightClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090219
        }
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->LINE_OF_SIGHT:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 101
    return-void
.end method

.method public onStartRememberNumberClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021b
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_NUMBER:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 96
    return-void
.end method

.method public onStartRememberWordsClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021c
        }
    .end annotation

    .prologue
    .line 145
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->REMEMBER_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->showPurchasePremiumDialog()V

    goto :goto_0
.end method

.method public onStartSchulteTableClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021d
        }
    .end annotation

    .prologue
    .line 90
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->SCHULTE_TABLE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 91
    return-void
.end method

.method public onStartSpeedReadingClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021e
        }
    .end annotation

    .prologue
    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->SPEED_READING:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 106
    return-void
.end method

.method public onStartTrueColorsClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021f
        }
    .end annotation

    .prologue
    .line 180
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->TRUE_COLORS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 186
    :goto_0
    return-void

    .line 184
    :cond_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->showPurchasePremiumDialog()V

    goto :goto_0
.end method

.method public onStartWordPairsClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090221
        }
    .end annotation

    .prologue
    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORD_PAIRS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 111
    return-void
.end method

.method public onStrartMathClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09021a
        }
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->trainingFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->MATHEMATICS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 126
    return-void
.end method

.method public showPurchasePremiumDialog()V
    .locals 4

    .prologue
    .line 191
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 192
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setCancelable(Z)Landroid/support/v7/app/AlertDialog$Builder;

    .line 193
    const v2, 0x7f0e012b

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 194
    const v2, 0x7f0e0129

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 196
    const v2, 0x7f0e0128

    new-instance v3, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$3;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$3;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 203
    const v2, 0x7f0e012a

    new-instance v3, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$4;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment$4;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 211
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 212
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 213
    return-void
.end method
