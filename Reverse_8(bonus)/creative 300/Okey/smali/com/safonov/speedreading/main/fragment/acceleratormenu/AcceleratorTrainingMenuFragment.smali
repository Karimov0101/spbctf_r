.class public Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "AcceleratorTrainingMenuFragment.java"


# instance fields
.field private menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 52
    instance-of v0, p1, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    if-eqz v0, :cond_0

    .line 53
    check-cast p1, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 58
    return-void

    .line 55
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement TrainingFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 34
    const v1, 0x7f0b004e

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 36
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->unbinder:Lbutterknife/Unbinder;

    .line 38
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 44
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 45
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 64
    return-void
.end method

.method public onStartAcceleratorClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090222
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_COLUMNS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 69
    return-void
.end method

.method public onStartAcceleratorPassCourseClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090213
        }
    .end annotation

    .prologue
    .line 83
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->ACCELERATOR_COURSE:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 84
    return-void
.end method

.method public onStartFlashWordClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090217
        }
    .end annotation

    .prologue
    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->FLASH_WORDS:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 79
    return-void
.end method

.method public onStartWordBlockClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090220
        }
    .end annotation

    .prologue
    .line 73
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/training/TrainingType;->WORDS_BLOCK:Lcom/safonov/speedreading/training/TrainingType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 74
    return-void
.end method
