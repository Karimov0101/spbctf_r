.class Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;
.super Ljava/lang/Object;
.source "MenuActivity.java"

# interfaces
.implements Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/main/activity/view/MenuActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 336
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNavigationItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 340
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 357
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 342
    :sswitch_0
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$200(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->MAIN_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    move v0, v1

    .line 343
    goto :goto_0

    .line 345
    :sswitch_1
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$300(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    move v0, v1

    .line 346
    goto :goto_0

    .line 348
    :sswitch_2
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$400(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    move v0, v1

    .line 349
    goto :goto_0

    .line 351
    :sswitch_3
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$500(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->TRAINING_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    move v0, v1

    .line 352
    goto :goto_0

    .line 354
    :sswitch_4
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$600(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->PROFILE:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    move v0, v1

    .line 355
    goto :goto_0

    .line 340
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f090014 -> :sswitch_0
        0x7f090016 -> :sswitch_2
        0x7f09001c -> :sswitch_4
        0x7f09001d -> :sswitch_1
        0x7f09001f -> :sswitch_3
    .end sparse-switch
.end method
