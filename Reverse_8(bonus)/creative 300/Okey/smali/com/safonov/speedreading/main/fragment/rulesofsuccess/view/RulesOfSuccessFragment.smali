.class public Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;
.super Landroid/support/v4/app/Fragment;
.source "RulesOfSuccessFragment.java"


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field recyclerView:Landroid/support/v7/widget/RecyclerView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090160
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 49
    const v2, 0x7f0b00a1

    const/4 v3, 0x0

    invoke-virtual {p1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    .local v1, "view":Landroid/view/View;
    invoke-static {p0, v1}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v2

    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->unbinder:Lbutterknife/Unbinder;

    .line 53
    new-instance v0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;-><init>()V

    .line 54
    .local v0, "rulesOfSuccessAdapter":Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 55
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setOverScrollMode(I)V

    .line 56
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Landroid/support/v7/widget/LinearLayoutManager;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/support/v7/widget/LinearLayoutManager;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 57
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v2

    invoke-virtual {v2}, Lcom/safonov/speedreading/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f03001d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->setItems([Ljava/lang/String;)V

    .line 59
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v2}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 78
    :goto_0
    return-object v1

    .line 62
    :cond_0
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v3, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 75
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v3, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 96
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 97
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 98
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 84
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 85
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 90
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 91
    return-void
.end method
