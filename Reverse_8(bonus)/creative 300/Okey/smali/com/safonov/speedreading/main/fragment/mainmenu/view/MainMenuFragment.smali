.class public Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;
.source "MainMenuFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpFragment",
        "<",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;",
        ">;",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;"
    }
.end annotation


# static fields
.field private static final PURCHASE_REQUEST_CODE:I = 0x3ea


# instance fields
.field colorAccent:I
    .annotation build Lbutterknife/BindColor;
        value = 0x7f06002d
    .end annotation
.end field

.field focusAttentionPB:Landroid/widget/ProgressBar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090097
    .end annotation
.end field

.field private libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

.field private menuFragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

.field private passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

.field pointsLeftTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090138
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field profileTitleTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090149
    .end annotation
.end field

.field randomRecomendationLink:Ljava/lang/String;

.field recomendationDescriptionView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015a
    .end annotation
.end field

.field recomendationIndex:I

.field recomendationTitleView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015d
    .end annotation
.end field

.field scoreTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090188
    .end annotation
.end field

.field speedReadingBookNotTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b5
    .end annotation
.end field

.field speedReadingBookStartTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0901b8
    .end annotation
.end field

.field private trainingMenuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;-><init>()V

    .line 87
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)Lcom/safonov/speedreading/main/TrainingFragmentListener;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->trainingMenuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    return-object v0
.end method

.method private getScoreByState(Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;)I
    .locals 2
    .param p1, "state"    # Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .prologue
    .line 256
    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment$3;->$SwitchMap$com$safonov$speedreading$main$fragment$mainmenu$view$PassCourseRatingState:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 264
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 257
    :pswitch_0
    const/16 v0, 0x3e8

    goto :goto_0

    .line 258
    :pswitch_1
    const/16 v0, 0x384

    goto :goto_0

    .line 259
    :pswitch_2
    const/16 v0, 0x2ee

    goto :goto_0

    .line 260
    :pswitch_3
    const/16 v0, 0x28a

    goto :goto_0

    .line 261
    :pswitch_4
    const/16 v0, 0x1f4

    goto :goto_0

    .line 262
    :pswitch_5
    const/16 v0, 0x15e

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->createPresenter()Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v1

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/App;->getPassCourseConfiguration()Lio/realm/Realm;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    .line 71
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;

    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-direct {v0, v1}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;-><init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;)V

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 373
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 374
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_0

    .line 375
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 380
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 294
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onAttach(Landroid/content/Context;)V

    .line 295
    instance-of v0, p1, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 296
    check-cast v0, Lcom/safonov/speedreading/main/TrainingFragmentListener;

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->trainingMenuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 302
    instance-of v0, p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 303
    check-cast v0, Lcom/safonov/speedreading/main/MenuFragmentListener;

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 309
    instance-of v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    if-eqz v0, :cond_2

    .line 310
    check-cast p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 315
    return-void

    .line 298
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement TrainingFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement MenuFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 312
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement LibraryFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 16
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 96
    const v12, 0x7f0b0051

    const/4 v13, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v12, v1, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v11

    .line 98
    .local v11, "view":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-static {v0, v11}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->unbinder:Lbutterknife/Unbinder;

    .line 100
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v12

    const-string v13, "load_book"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/support/v4/app/FragmentActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 101
    .local v10, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v12, "book_id"

    const-wide/16 v14, -0x1

    invoke-interface {v10, v12, v14, v15}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 103
    .local v6, "bookId":J
    const-wide/16 v12, -0x1

    cmp-long v12, v6, v12

    if-lez v12, :cond_1

    .line 104
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookStartTV:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 105
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookNotTV:Landroid/widget/TextView;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 112
    :goto_0
    new-instance v9, Lcom/safonov/speedreading/application/ratedialog/RateDialog;

    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v9, v12}, Lcom/safonov/speedreading/application/ratedialog/RateDialog;-><init>(Landroid/content/Context;)V

    .line 113
    .local v9, "rateDialog":Lcom/safonov/speedreading/application/ratedialog/RateDialog;
    invoke-virtual {v9}, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->isShouldShow()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 114
    invoke-virtual {v9}, Lcom/safonov/speedreading/application/ratedialog/RateDialog;->show()V

    .line 123
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f03001b

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 124
    .local v4, "arrayTitles":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f030019

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    .line 125
    .local v2, "arrayDescriptions":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f03001a

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 127
    .local v3, "arrayLinks":[Ljava/lang/String;
    new-instance v12, Ljava/util/Random;

    invoke-direct {v12}, Ljava/util/Random;-><init>()V

    array-length v13, v4

    invoke-virtual {v12, v13}, Ljava/util/Random;->nextInt(I)I

    move-result v12

    move-object/from16 v0, p0

    iput v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationIndex:I

    .line 128
    move-object/from16 v0, p0

    iget v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationIndex:I

    aget-object v8, v4, v12

    .line 129
    .local v8, "randomTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationIndex:I

    aget-object v5, v2, v12

    .line 130
    .local v5, "randomDescription":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationIndex:I

    aget-object v12, v3, v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->randomRecomendationLink:Ljava/lang/String;

    .line 132
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationTitleView:Landroid/widget/TextView;

    invoke-virtual {v12, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 133
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationDescriptionView:Landroid/widget/TextView;

    invoke-virtual {v12, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 135
    return-object v11

    .line 108
    .end local v2    # "arrayDescriptions":[Ljava/lang/String;
    .end local v3    # "arrayLinks":[Ljava/lang/String;
    .end local v4    # "arrayTitles":[Ljava/lang/String;
    .end local v5    # "randomDescription":Ljava/lang/String;
    .end local v8    # "randomTitle":Ljava/lang/String;
    .end local v9    # "rateDialog":Lcom/safonov/speedreading/application/ratedialog/RateDialog;
    :cond_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookStartTV:Landroid/widget/TextView;

    const/4 v13, 0x4

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 109
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookNotTV:Landroid/widget/TextView;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 276
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroy()V

    .line 277
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->close()V

    .line 278
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 282
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDestroyView()V

    .line 283
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 284
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 319
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onDetach()V

    .line 320
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->trainingMenuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    .line 321
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->menuFragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 322
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 323
    return-void
.end method

.method public onRandomTrainerClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900e9
        }
    .end annotation

    .prologue
    .line 366
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->trainingMenuFragmentListener:Lcom/safonov/speedreading/main/TrainingFragmentListener;

    invoke-static {}, Lcom/safonov/speedreading/training/TrainingType;->randomTraining()Lcom/safonov/speedreading/training/TrainingType;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/TrainingFragmentListener;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V

    .line 367
    return-void
.end method

.method public onRecomendationClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09015c
        }
    .end annotation

    .prologue
    .line 140
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->randomRecomendationLink:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 141
    .local v0, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->startActivity(Landroid/content/Intent;)V

    .line 142
    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 188
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onResume()V

    .line 191
    return-void
.end method

.method public onSpeedreadingbookClick()V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, 0x0

    .line 146
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    const-string v6, "ru"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 147
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "load_book"

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 148
    .local v3, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v5, "book_id"

    invoke-interface {v3, v5, v8, v9}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 149
    .local v0, "bookId":J
    cmp-long v5, v0, v8

    if-lez v5, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "timer_mode"

    .line 151
    invoke-virtual {v5, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 154
    .local v4, "timerModeParam":I
    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    const-class v6, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-direct {v2, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 155
    .local v2, "intent":Landroid/content/Intent;
    const-string v5, "book_description_id"

    invoke-virtual {v2, v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 156
    const-string v5, "timer_mode"

    invoke-virtual {v2, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 158
    if-nez v4, :cond_1

    .line 159
    invoke-virtual {p0, v2}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->startActivity(Landroid/content/Intent;)V

    .line 165
    .end local v0    # "bookId":J
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v3    # "sharedPreferences":Landroid/content/SharedPreferences;
    .end local v4    # "timerModeParam":I
    :cond_0
    :goto_0
    return-void

    .line 161
    .restart local v0    # "bookId":J
    .restart local v2    # "intent":Landroid/content/Intent;
    .restart local v3    # "sharedPreferences":Landroid/content/SharedPreferences;
    .restart local v4    # "timerModeParam":I
    :cond_1
    const/16 v5, 0x7ce

    invoke-virtual {p0, v2, v5}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onStartPassCourseClick()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0900e7
        }
    .end annotation

    .prologue
    .line 328
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 329
    .local v0, "builder":Landroid/support/v7/app/AlertDialog$Builder;
    const v2, 0x7f0e00f4

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 330
    const v2, 0x7f0e00f1

    invoke-virtual {v0, v2}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(I)Landroid/support/v7/app/AlertDialog$Builder;

    .line 332
    const v2, 0x7f0e00f3

    new-instance v3, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment$1;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 340
    const v2, 0x7f0e00f2

    new-instance v3, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment$2;

    invoke-direct {v3, p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment$2;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 347
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->create()Landroid/support/v7/app/AlertDialog;

    move-result-object v1

    .line 348
    .local v1, "dialog":Landroid/support/v7/app/AlertDialog;
    invoke-virtual {v1}, Landroid/support/v7/app/AlertDialog;->show()V

    .line 350
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 195
    invoke-super {p0, p1, p2}, Lcom/hannesdorfmann/mosby3/mvp/MvpFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 196
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;->init()V

    .line 197
    return-void
.end method

.method public onspeedReadingBookClick2()V
    .locals 14
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901b4
        }
    .end annotation

    .prologue
    const-wide/16 v12, -0x1

    .line 169
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "load_book"

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 170
    .local v7, "sharedPreferences":Landroid/content/SharedPreferences;
    const-string v9, "book_id"

    invoke-interface {v7, v9, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 171
    .local v4, "bookId":J
    cmp-long v9, v4, v12

    if-lez v9, :cond_0

    .line 172
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-static {v9}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v9

    .line 173
    invoke-virtual {v9}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v2

    .line 175
    .local v2, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v6, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v6, v9, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 176
    .local v6, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v3, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v3, v9, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 177
    .local v3, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v8, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 179
    .local v8, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    new-instance v0, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v0, v2, v6, v3, v8}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    .line 180
    .local v0, "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    invoke-virtual {v0, v4, v5}, Lcom/safonov/speedreading/reader/repository/BookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 182
    .local v1, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v9, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 184
    .end local v0    # "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .end local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    .end local v3    # "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    .end local v6    # "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    .end local v8    # "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    :cond_0
    return-void
.end method

.method public setPassCourseView(IILcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;II)V
    .locals 14
    .param p1, "score"    # I
    .param p2, "bestScore"    # I
    .param p3, "ratingState"    # Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    .param p4, "completedPart"    # I
    .param p5, "uncompletedPart"    # I

    .prologue
    .line 219
    const v9, 0x7f0e00eb

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    add-int v12, p5, p4

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {p0, v9, v10}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 221
    .local v6, "scoreText":Ljava/lang/String;
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->profileTitleTV:Landroid/widget/TextView;

    invoke-virtual/range {p3 .. p3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getTitleRes()I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(I)V

    .line 223
    sget-object v3, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 225
    .local v3, "nextRang":Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->values()[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    move-result-object v5

    .line 227
    .local v5, "rangs":[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    invoke-virtual/range {p3 .. p3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getUncompletedPart()I

    move-result v9

    if-eqz v9, :cond_2

    .line 228
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v9, v5

    if-ge v2, v9, :cond_1

    .line 229
    aget-object v9, v5, v2

    invoke-virtual {v9}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getCompletedPart()I

    move-result v9

    invoke-virtual/range {p3 .. p3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getCompletedPart()I

    move-result v10

    if-ne v9, v10, :cond_0

    .line 230
    add-int/lit8 v9, v2, 0x1

    aget-object v3, v5, v9

    .line 228
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 232
    :cond_1
    invoke-direct {p0, v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getScoreByState(Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;)I

    move-result v9

    sub-int v4, v9, p4

    .line 233
    .local v4, "pointsLeft":I
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->pointsLeftTV:Landroid/widget/TextView;

    const v10, 0x7f0e004d

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x1

    invoke-virtual {v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getTitleRes()I

    move-result v13

    invoke-virtual {p0, v13}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {p0, v10, v11}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 239
    .end local v2    # "i":I
    .end local v4    # "pointsLeft":I
    :goto_1
    const/16 v9, 0x2f

    invoke-virtual {v6, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v9

    add-int/lit8 v8, v9, 0x1

    .line 240
    .local v8, "startIndex":I
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v1

    .line 242
    .local v1, "endIndex":I
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->scoreTextView:Landroid/widget/TextView;

    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 244
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->focusAttentionPB:Landroid/widget/ProgressBar;

    add-int v10, p5, p4

    invoke-virtual {v9, v10}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 245
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->focusAttentionPB:Landroid/widget/ProgressBar;

    move/from16 v0, p4

    invoke-virtual {v9, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 247
    new-instance v7, Landroid/text/SpannableString;

    invoke-direct {v7, v6}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 248
    .local v7, "spannableString":Landroid/text/SpannableString;
    new-instance v9, Landroid/text/style/ForegroundColorSpan;

    iget v10, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->colorAccent:I

    invoke-direct {v9, v10}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/16 v10, 0x21

    invoke-virtual {v7, v9, v8, v1, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 253
    return-void

    .line 236
    .end local v1    # "endIndex":I
    .end local v7    # "spannableString":Landroid/text/SpannableString;
    .end local v8    # "startIndex":I
    :cond_2
    iget-object v9, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->pointsLeftTV:Landroid/widget/TextView;

    const v10, 0x7f0e004c

    invoke-virtual {p0, v10}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public speedReadingBookClick()V
    .locals 0
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f0901b4
        }
    .end annotation

    .prologue
    .line 203
    return-void
.end method
