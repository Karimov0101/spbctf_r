.class public Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;
.super Landroid/support/v7/preference/PreferenceDialogFragmentCompat;
.source "TimePreferenceFragmentDialogCompat.java"


# instance fields
.field private timePicker:Landroid/widget/TimePicker;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceDialogFragmentCompat;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    return-void
.end method

.method private initTimePicker(II)V
    .locals 2
    .param p1, "hours"    # I
    .param p2, "minutes"    # I

    .prologue
    .line 50
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-lt v0, v1, :cond_0

    .line 51
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v0, p1}, Landroid/widget/TimePicker;->setHour(I)V

    .line 52
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v0, p2}, Landroid/widget/TimePicker;->setMinute(I)V

    .line 57
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentHour(Ljava/lang/Integer;)V

    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TimePicker;->setCurrentMinute(Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;
    .locals 3
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 19
    new-instance v1, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;

    invoke-direct {v1}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;-><init>()V

    .line 20
    .local v1, "fragment":Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;
    new-instance v0, Landroid/os/Bundle;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Landroid/os/Bundle;-><init>(I)V

    .line 21
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 22
    invoke-virtual {v1, v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->setArguments(Landroid/os/Bundle;)V

    .line 23
    return-object v1
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceDialogFragmentCompat;->onBindDialogView(Landroid/view/View;)V

    .line 43
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TimePicker;->setIs24HourView(Ljava/lang/Boolean;)V

    .line 45
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->getPreference()Landroid/support/v7/preference/DialogPreference;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;

    .line 46
    .local v0, "pref":Lcom/safonov/speedreading/main/fragment/settings/TimePreference;
    invoke-virtual {v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->getHours()I

    move-result v1

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->getMinutes()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->initTimePicker(II)V

    .line 47
    return-void
.end method

.method protected onCreateDialogView(Landroid/content/Context;)Landroid/view/View;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    new-instance v0, Landroid/widget/TimePicker;

    invoke-direct {v0, p1}, Landroid/widget/TimePicker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    return-object v0
.end method

.method public onDialogClosed(Z)V
    .locals 6
    .param p1, "positiveResult"    # Z

    .prologue
    .line 61
    if-eqz p1, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->getPreference()Landroid/support/v7/preference/DialogPreference;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;

    .line 66
    .local v3, "pref":Lcom/safonov/speedreading/main/fragment/settings/TimePreference;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x17

    if-lt v4, v5, :cond_1

    .line 67
    iget-object v4, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->getHour()I

    move-result v1

    .line 68
    .local v1, "hours":I
    iget-object v4, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->getMinute()I

    move-result v2

    .line 74
    .local v2, "minutes":I
    :goto_0
    const-string v4, "TimePreference"

    const-string v5, "onDialogClosed"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    invoke-virtual {v3, v1, v2}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->convertToSaveFormat(II)I

    move-result v0

    .line 77
    .local v0, "convertedValue":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->callChangeListener(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-virtual {v3, v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->persist(I)V

    .line 79
    invoke-virtual {v3}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->updateSummary()V

    .line 82
    .end local v0    # "convertedValue":I
    .end local v1    # "hours":I
    .end local v2    # "minutes":I
    .end local v3    # "pref":Lcom/safonov/speedreading/main/fragment/settings/TimePreference;
    :cond_0
    return-void

    .line 70
    .restart local v3    # "pref":Lcom/safonov/speedreading/main/fragment/settings/TimePreference;
    :cond_1
    iget-object v4, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->getCurrentHour()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 71
    .restart local v1    # "hours":I
    iget-object v4, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->timePicker:Landroid/widget/TimePicker;

    invoke-virtual {v4}, Landroid/widget/TimePicker;->getCurrentMinute()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .restart local v2    # "minutes":I
    goto :goto_0
.end method

.method protected onPrepareDialogBuilder(Landroid/support/v7/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/support/v7/app/AlertDialog$Builder;

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceDialogFragmentCompat;->onPrepareDialogBuilder(Landroid/support/v7/app/AlertDialog$Builder;)V

    .line 31
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 32
    return-void
.end method
