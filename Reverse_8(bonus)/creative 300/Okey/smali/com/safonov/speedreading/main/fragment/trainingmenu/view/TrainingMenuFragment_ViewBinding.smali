.class public Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;
.super Ljava/lang/Object;
.source "TrainingMenuFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

.field private view2131296785:Landroid/view/View;

.field private view2131296786:Landroid/view/View;

.field private view2131296788:Landroid/view/View;

.field private view2131296789:Landroid/view/View;

.field private view2131296790:Landroid/view/View;

.field private view2131296792:Landroid/view/View;

.field private view2131296793:Landroid/view/View;

.field private view2131296794:Landroid/view/View;

.field private view2131296795:Landroid/view/View;

.field private view2131296796:Landroid/view/View;

.field private view2131296797:Landroid/view/View;

.field private view2131296798:Landroid/view/View;

.field private view2131296799:Landroid/view/View;

.field private view2131296801:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    .line 51
    const v1, 0x7f0901d4

    const-string v2, "field \'startRememberWords\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startRememberWords:Landroid/widget/TextView;

    .line 52
    const v1, 0x7f0901d5

    const-string v2, "field \'startTrueColors\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startTrueColors:Landroid/widget/TextView;

    .line 53
    const v1, 0x7f0901d2

    const-string v2, "field \'startCup\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startCup:Landroid/widget/TextView;

    .line 54
    const v1, 0x7f090212

    const-string v2, "method \'onAcceleratorMenuClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 55
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296786:Landroid/view/View;

    .line 56
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    const v1, 0x7f09021d

    const-string v2, "method \'onStartSchulteTableClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 63
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296797:Landroid/view/View;

    .line 64
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    const v1, 0x7f09021b

    const-string v2, "method \'onStartRememberNumberClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 71
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296795:Landroid/view/View;

    .line 72
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 78
    const v1, 0x7f090219

    const-string v2, "method \'onStartLineOfSightClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 79
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296793:Landroid/view/View;

    .line 80
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v1, 0x7f09021e

    const-string v2, "method \'onStartSpeedReadingClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 87
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296798:Landroid/view/View;

    .line 88
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$5;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    const v1, 0x7f090221

    const-string v2, "method \'onStartWordPairsClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 95
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296801:Landroid/view/View;

    .line 96
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$6;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$6;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 102
    const v1, 0x7f090216

    const-string v2, "method \'onStartEvenNumbersClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 103
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296790:Landroid/view/View;

    .line 104
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$7;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$7;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    const v1, 0x7f090218

    const-string v2, "method \'onStartGreenDotClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 111
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296792:Landroid/view/View;

    .line 112
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$8;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$8;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v1, 0x7f09021a

    const-string v2, "method \'onStrartMathClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 119
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296794:Landroid/view/View;

    .line 120
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$9;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$9;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 126
    const v1, 0x7f090214

    const-string v2, "method \'onStartConcentrationClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 127
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296788:Landroid/view/View;

    .line 128
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$10;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$10;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 134
    const v1, 0x7f090215

    const-string v2, "method \'onStartCupTimerClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 135
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296789:Landroid/view/View;

    .line 136
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$11;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$11;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    const v1, 0x7f09021c

    const-string v2, "method \'onStartRememberWordsClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 143
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296796:Landroid/view/View;

    .line 144
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$12;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$12;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    const v1, 0x7f090211

    const-string v2, "method \'onStartBaseCourse\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 151
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296785:Landroid/view/View;

    .line 152
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$13;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$13;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    const v1, 0x7f09021f

    const-string v2, "method \'onStartTrueColorsClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 159
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296799:Landroid/view/View;

    .line 160
    new-instance v1, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$14;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding$14;-><init>(Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 166
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 171
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    .line 172
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 173
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    .line 175
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startRememberWords:Landroid/widget/TextView;

    .line 176
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startTrueColors:Landroid/widget/TextView;

    .line 177
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->startCup:Landroid/widget/TextView;

    .line 179
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296786:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 180
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296786:Landroid/view/View;

    .line 181
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296797:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 182
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296797:Landroid/view/View;

    .line 183
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296795:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296795:Landroid/view/View;

    .line 185
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296793:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296793:Landroid/view/View;

    .line 187
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296798:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296798:Landroid/view/View;

    .line 189
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296801:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 190
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296801:Landroid/view/View;

    .line 191
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296790:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296790:Landroid/view/View;

    .line 193
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296792:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296792:Landroid/view/View;

    .line 195
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296794:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296794:Landroid/view/View;

    .line 197
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296788:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296788:Landroid/view/View;

    .line 199
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296789:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296789:Landroid/view/View;

    .line 201
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296796:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 202
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296796:Landroid/view/View;

    .line 203
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296785:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 204
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296785:Landroid/view/View;

    .line 205
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296799:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment_ViewBinding;->view2131296799:Landroid/view/View;

    .line 207
    return-void
.end method
