.class public interface abstract Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;
.super Ljava/lang/Object;
.source "IMenuPresenter.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
.implements Lcom/safonov/speedreading/main/MenuFragmentListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter",
        "<",
        "Lcom/safonov/speedreading/main/activity/view/IMenuView;",
        ">;",
        "Lcom/safonov/speedreading/main/MenuFragmentListener;"
    }
.end annotation


# virtual methods
.method public abstract onActionBarHelpPressed()V
.end method

.method public abstract onActionBarHomePressed()V
.end method

.method public abstract onActionBarSettingsPressed()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method
