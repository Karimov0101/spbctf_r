.class public interface abstract Lcom/safonov/speedreading/main/activity/view/IMenuView;
.super Ljava/lang/Object;
.source "IMenuView.java"

# interfaces
.implements Lcom/hannesdorfmann/mosby3/mvp/MvpView;
.implements Lcom/safonov/speedreading/main/MenuFragmentListener;
.implements Lcom/safonov/speedreading/main/TrainingFragmentListener;


# virtual methods
.method public abstract finish()V
.end method

.method public abstract onFileExplorerBackPressed()V
.end method

.method public abstract setAcceleratorTrainingMenuFragment()V
.end method

.method public abstract setActionBarItemsVisible(Z)V
.end method

.method public abstract setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
.end method

.method public abstract setDescriptionFragment()V
.end method

.method public abstract setFileExplorerFragment()V
.end method

.method public abstract setLibraryFragment()V
.end method

.method public abstract setMainMenuFragment()V
.end method

.method public abstract setMotivatorsFragment()V
.end method

.method public abstract setMotivatorsMenuFragment()V
.end method

.method public abstract setProfileFragment()V
.end method

.method public abstract setRecommendationFragment()V
.end method

.method public abstract setRulesOfSuccessFragment()V
.end method

.method public abstract setSettingsFragment()V
.end method

.method public abstract setTrainingMenuFragment()V
.end method
