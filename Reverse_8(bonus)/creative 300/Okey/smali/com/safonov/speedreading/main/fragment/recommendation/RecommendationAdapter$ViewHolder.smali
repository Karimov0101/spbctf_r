.class Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "RecommendationAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ViewHolder"
.end annotation


# instance fields
.field favoriteImageView:Landroid/widget/ImageView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900cb
    .end annotation
.end field

.field private itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

.field layout:Landroid/widget/LinearLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09015b
    .end annotation
.end field

.field messageTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900cd
    .end annotation
.end field

.field final synthetic this$0:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

.field titleTextView:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f0900cf
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;Landroid/view/View;Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;)V
    .locals 1
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;
    .param p2, "itemView"    # Landroid/view/View;
    .param p3, "itemClickListener"    # Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    .prologue
    .line 38
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->this$0:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

    .line 39
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 40
    invoke-static {p0, p2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    .line 42
    iput-object p3, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    .line 43
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 44
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->getAdapterPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;->onItemClick(I)V

    .line 51
    :cond_0
    return-void
.end method
