.class public Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;
.super Landroid/support/v7/preference/PreferenceFragmentCompat;
.source "MainSettingsFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/support/v7/preference/PreferenceFragmentCompat;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreatePreferences(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "rootKey"    # Ljava/lang/String;

    .prologue
    .line 24
    const v0, 0x7f110004

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;->addPreferencesFromResource(I)V

    .line 25
    return-void
.end method

.method public onDisplayPreferenceDialog(Landroid/support/v7/preference/Preference;)V
    .locals 3
    .param p1, "preference"    # Landroid/support/v7/preference/Preference;

    .prologue
    .line 29
    const/4 v0, 0x0

    .line 30
    .local v0, "dialogFragment":Landroid/support/v4/app/DialogFragment;
    instance-of v1, p1, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;

    if-eqz v1, :cond_0

    .line 31
    invoke-virtual {p1}, Landroid/support/v7/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;->newInstance(Ljava/lang/String;)Lcom/safonov/speedreading/main/fragment/settings/TimePreferenceFragmentDialogCompat;

    move-result-object v0

    .line 34
    :cond_0
    if-eqz v0, :cond_1

    .line 35
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/support/v4/app/DialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 36
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string v2, "android.support.v7.preference.PreferenceFragment.DIALOG"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_1
    invoke-super {p0, p1}, Landroid/support/v7/preference/PreferenceFragmentCompat;->onDisplayPreferenceDialog(Landroid/support/v7/preference/Preference;)V

    goto :goto_0
.end method
