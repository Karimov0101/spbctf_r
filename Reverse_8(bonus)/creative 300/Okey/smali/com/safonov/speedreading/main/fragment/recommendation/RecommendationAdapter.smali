.class public Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "RecommendationAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;,
        Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

.field private items:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;)V
    .locals 1
    .param p1, "itemClickListener"    # Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    .line 57
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    .line 58
    return-void
.end method


# virtual methods
.method public addItem(Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;)V
    .locals 1
    .param p1, "advice"    # Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 61
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 62
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->notifyDataSetChanged()V

    .line 63
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->onBindViewHolder(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 86
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;

    .line 88
    .local v0, "advice":Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
    iget-object v1, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget-object v1, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->messageTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->getDescription()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 90
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 78
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b0098

    const/4 v3, 0x0

    .line 79
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 81
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->itemClickListener:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;

    invoke-direct {v1, p0, v0, v2}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;Landroid/view/View;Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;)V

    return-object v1
.end method

.method public removeItem(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 67
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->notifyItemRemoved(I)V

    .line 68
    return-void
.end method

.method public setItems(Ljava/util/List;)V
    .locals 1
    .param p1    # Ljava/util/List;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p1, "items":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;>;"
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 72
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->items:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 73
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->notifyDataSetChanged()V

    .line 74
    return-void
.end method
