.class public Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder_ViewBinding;
.super Ljava/lang/Object;
.source "RecommendationAdapter$ViewHolder_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    .line 24
    const v0, 0x7f0900cb

    const-string v1, "field \'favoriteImageView\'"

    const-class v2, Landroid/widget/ImageView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->favoriteImageView:Landroid/widget/ImageView;

    .line 25
    const v0, 0x7f0900cf

    const-string v1, "field \'titleTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 26
    const v0, 0x7f0900cd

    const-string v1, "field \'messageTextView\'"

    const-class v2, Landroid/widget/TextView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->messageTextView:Landroid/widget/TextView;

    .line 27
    const v0, 0x7f09015b

    const-string v1, "field \'layout\'"

    const-class v2, Landroid/widget/LinearLayout;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p1, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    .line 28
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    .line 34
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 35
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;

    .line 37
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->favoriteImageView:Landroid/widget/ImageView;

    .line 38
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    .line 39
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->messageTextView:Landroid/widget/TextView;

    .line 40
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$ViewHolder;->layout:Landroid/widget/LinearLayout;

    .line 41
    return-void
.end method
