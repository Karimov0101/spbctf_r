.class public Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "MenuPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/main/activity/view/IMenuView;",
        ">;",
        "Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;"
    }
.end annotation


# instance fields
.field private menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionBarHelpPressed()V
    .locals 1

    .prologue
    .line 147
    sget-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->DESCRIPTION:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 148
    return-void
.end method

.method public onActionBarHomePressed()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/MenuFragmentType;->getParent()Lcom/safonov/speedreading/main/MenuFragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 123
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    if-nez v0, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->finish()V

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    goto :goto_0
.end method

.method public onActionBarSettingsPressed()V
    .locals 1

    .prologue
    .line 142
    sget-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->SETTINGS:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 143
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/main/MenuFragmentType;

    if-ne v0, v1, :cond_1

    .line 106
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->onFileExplorerBackPressed()V

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/MenuFragmentType;->getParent()Lcom/safonov/speedreading/main/MenuFragmentType;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 110
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    if-nez v0, :cond_2

    .line 111
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->finish()V

    goto :goto_0

    .line 116
    :cond_2
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    goto :goto_0
.end method

.method public requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 134
    sget-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 135
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 138
    :cond_0
    return-void
.end method

.method public requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V
    .locals 3
    .param p1, "menuFragmentType"    # Lcom/safonov/speedreading/main/MenuFragmentType;

    .prologue
    const/4 v2, 0x0

    .line 23
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->menuFragmentType:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 25
    sget-object v0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter$1;->$SwitchMap$com$safonov$speedreading$main$MenuFragmentType:[I

    invoke-virtual {p1}, Lcom/safonov/speedreading/main/MenuFragmentType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 27
    :pswitch_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 29
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setMainMenuFragment()V

    goto :goto_0

    .line 33
    :pswitch_1
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 35
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setSettingsFragment()V

    goto :goto_0

    .line 39
    :pswitch_2
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 41
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setDescriptionFragment()V

    goto :goto_0

    .line 45
    :pswitch_3
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 47
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setTrainingMenuFragment()V

    goto :goto_0

    .line 51
    :pswitch_4
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setAcceleratorTrainingMenuFragment()V

    goto :goto_0

    .line 57
    :pswitch_5
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 59
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setMotivatorsMenuFragment()V

    goto/16 :goto_0

    .line 63
    :pswitch_6
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 65
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setMotivatorsFragment()V

    goto/16 :goto_0

    .line 69
    :pswitch_7
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 71
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setRulesOfSuccessFragment()V

    goto/16 :goto_0

    .line 75
    :pswitch_8
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 77
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setRecommendationFragment()V

    goto/16 :goto_0

    .line 81
    :pswitch_9
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 83
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setProfileFragment()V

    goto/16 :goto_0

    .line 87
    :pswitch_a
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 89
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setLibraryFragment()V

    goto/16 :goto_0

    .line 93
    :pswitch_b
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0, v2}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setActionBarItemsVisible(Z)V

    .line 95
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/activity/view/IMenuView;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/view/IMenuView;->setFileExplorerFragment()V

    goto/16 :goto_0

    .line 25
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method
