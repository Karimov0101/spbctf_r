.class Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;
.super Ljava/lang/Object;
.source "MenuActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/main/activity/view/MenuActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

.field final synthetic val$bookId:J


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;J)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    iput-wide p2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->val$bookId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 285
    iget-wide v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->val$bookId:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 286
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-virtual {v2}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "timer_mode"

    const/4 v4, 0x0

    .line 287
    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 290
    .local v1, "timerModeParam":I
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    const-class v3, Lcom/safonov/speedreading/reader/reader/view/ReaderActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "book_description_id"

    iget-wide v4, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->val$bookId:J

    invoke-virtual {v0, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 292
    const-string v2, "timer_mode"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 294
    if-nez v1, :cond_1

    .line 295
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-virtual {v2, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 300
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "timerModeParam":I
    :cond_0
    :goto_0
    return-void

    .line 297
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v1    # "timerModeParam":I
    :cond_1
    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    const/16 v3, 0x7ce

    invoke-virtual {v2, v0, v3}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
