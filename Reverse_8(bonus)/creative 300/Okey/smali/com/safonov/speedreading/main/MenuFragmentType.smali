.class public final enum Lcom/safonov/speedreading/main/MenuFragmentType;
.super Ljava/lang/Enum;
.source "MenuFragmentType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/main/MenuFragmentType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum ACCELERATOR_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum BOOK_DETAIL:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum DESCRIPTION:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum FILE_EXPLORER:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum MAIN_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum MOTIVATORS:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum PROFILE:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum RECOMMENDATION:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum RULES_OF_SUCCESS:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum SETTINGS:Lcom/safonov/speedreading/main/MenuFragmentType;

.field public static final enum TRAINING_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;


# instance fields
.field private final parent:Lcom/safonov/speedreading/main/MenuFragmentType;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 12
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "MAIN_MENU"

    invoke-direct {v0, v1, v5, v4}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->MAIN_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 14
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "PROFILE"

    invoke-direct {v0, v1, v6, v4}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->PROFILE:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 15
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "SETTINGS"

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->PROFILE:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v7, v2}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->SETTINGS:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 17
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "LIBRARY"

    invoke-direct {v0, v1, v8, v4}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 18
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "FILE_EXPLORER"

    const/4 v2, 0x4

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 19
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "BOOK_DETAIL"

    const/4 v2, 0x5

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 21
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "TRAINING_MENU"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->TRAINING_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 22
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "ACCELERATOR_MENU"

    const/4 v2, 0x7

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->TRAINING_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->ACCELERATOR_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 24
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "MOTIVATORS_MENU"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 25
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "DESCRIPTION"

    const/16 v2, 0x9

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->DESCRIPTION:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 26
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "RULES_OF_SUCCESS"

    const/16 v2, 0xa

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->RULES_OF_SUCCESS:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 27
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "MOTIVATORS"

    const/16 v2, 0xb

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 28
    new-instance v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    const-string v1, "RECOMMENDATION"

    const/16 v2, 0xc

    sget-object v3, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-direct {v0, v1, v2, v3}, Lcom/safonov/speedreading/main/MenuFragmentType;-><init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->RECOMMENDATION:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 11
    const/16 v0, 0xd

    new-array v0, v0, [Lcom/safonov/speedreading/main/MenuFragmentType;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->MAIN_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->PROFILE:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->SETTINGS:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v1, v0, v8

    const/4 v1, 0x4

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->BOOK_DETAIL:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->TRAINING_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->ACCELERATOR_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->DESCRIPTION:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->RULES_OF_SUCCESS:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/safonov/speedreading/main/MenuFragmentType;->RECOMMENDATION:Lcom/safonov/speedreading/main/MenuFragmentType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->$VALUES:[Lcom/safonov/speedreading/main/MenuFragmentType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/safonov/speedreading/main/MenuFragmentType;)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "parent"    # Lcom/safonov/speedreading/main/MenuFragmentType;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/main/MenuFragmentType;",
            ")V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-object p3, p0, Lcom/safonov/speedreading/main/MenuFragmentType;->parent:Lcom/safonov/speedreading/main/MenuFragmentType;

    .line 33
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/main/MenuFragmentType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/MenuFragmentType;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/main/MenuFragmentType;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/safonov/speedreading/main/MenuFragmentType;->$VALUES:[Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/main/MenuFragmentType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/main/MenuFragmentType;

    return-object v0
.end method


# virtual methods
.method public getParent()Lcom/safonov/speedreading/main/MenuFragmentType;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lcom/safonov/speedreading/main/MenuFragmentType;->parent:Lcom/safonov/speedreading/main/MenuFragmentType;

    return-object v0
.end method
