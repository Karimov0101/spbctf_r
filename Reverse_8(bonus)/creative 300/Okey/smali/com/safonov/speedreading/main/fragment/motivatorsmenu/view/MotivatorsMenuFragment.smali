.class public Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;
.super Landroid/support/v4/app/Fragment;
.source "MotivatorsMenuFragment.java"


# instance fields
.field bookId:J

.field private fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

.field private libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

.field sharedPreferences:Landroid/content/SharedPreferences;

.field speedReadingBookView:Landroid/support/constraint/ConstraintLayout;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09010b
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 117
    instance-of v0, p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 118
    check-cast v0, Lcom/safonov/speedreading/main/MenuFragmentListener;

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 123
    instance-of v0, p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    if-eqz v0, :cond_1

    .line 124
    check-cast p1, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 129
    return-void

    .line 120
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement MenuFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement LibraryFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 54
    const v1, 0x7f0b0052

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 56
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "load_book"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 57
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "book_id"

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->bookId:J

    .line 59
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->unbinder:Lbutterknife/Unbinder;

    .line 61
    iget-wide v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->bookId:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 62
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->speedReadingBookView:Landroid/support/constraint/ConstraintLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/support/constraint/ConstraintLayout;->setVisibility(I)V

    .line 65
    :cond_0
    return-object v0
.end method

.method public onDescriptionClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090076
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->DESCRIPTION:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/MenuFragmentListener;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 94
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 72
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 133
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 134
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 135
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    .line 136
    return-void
.end method

.method public onMotivatorsClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09010c
        }
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->MOTIVATORS:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/MenuFragmentListener;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 109
    return-void
.end method

.method public onRecommendationsClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09015e
        }
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->RECOMMENDATION:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/MenuFragmentListener;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 99
    return-void
.end method

.method public onRulesOfSuccessClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090179
        }
    .end annotation

    .prologue
    .line 103
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->RULES_OF_SUCCESS:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/MenuFragmentListener;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 104
    return-void
.end method

.method public onSpeedReadingBookClick()V
    .locals 10
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09010b
        }
    .end annotation

    .prologue
    .line 76
    iget-wide v6, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->bookId:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v6

    .line 78
    invoke-virtual {v6}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v2

    .line 80
    .local v2, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    new-instance v4, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 81
    .local v4, "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    new-instance v3, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 82
    .local v3, "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    new-instance v5, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;-><init>(Landroid/content/Context;Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;)V

    .line 84
    .local v5, "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    new-instance v0, Lcom/safonov/speedreading/reader/repository/BookController;

    invoke-direct {v0, v2, v4, v3, v5}, Lcom/safonov/speedreading/reader/repository/BookController;-><init>(Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/IFb2Dao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/IEpubDao;Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/ITxtDao;)V

    .line 85
    .local v0, "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    iget-wide v6, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->bookId:J

    invoke-virtual {v0, v6, v7}, Lcom/safonov/speedreading/reader/repository/BookController;->findBookDescription(J)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 87
    .local v1, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    iget-object v6, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->libraryFragmentListener:Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;

    invoke-interface {v6, v1}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;->onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 89
    .end local v0    # "bookController":Lcom/safonov/speedreading/reader/repository/BookController;
    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .end local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    .end local v3    # "epubDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/epub/EpubDao;
    .end local v4    # "fb2Dao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/fb2/Fb2Dao;
    .end local v5    # "txtDao":Lcom/safonov/speedreading/reader/repository/dao/bookdao/txt/TxtDao;
    :cond_0
    return-void
.end method
