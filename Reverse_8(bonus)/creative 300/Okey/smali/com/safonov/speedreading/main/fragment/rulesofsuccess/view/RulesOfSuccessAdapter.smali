.class public Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "RulesOfSuccessAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v7/widget/RecyclerView$Adapter",
        "<",
        "Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field private items:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getItemCount()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->items:[Ljava/lang/String;

    array-length v0, v0

    return v0
.end method

.method public bridge synthetic onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;

    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->onBindViewHolder(Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;I)V

    return-void
.end method

.method public onBindViewHolder(Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;I)V
    .locals 3
    .param p1, "holder"    # Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 50
    iget-object v0, p1, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;->titleTextView:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v2, p2, 0x1

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->items:[Ljava/lang/String;

    aget-object v2, v2, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 51
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1

    .prologue
    .line 21
    invoke-virtual {p0, p1, p2}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0b00a0

    const/4 v3, 0x0

    .line 43
    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter$ViewHolder;-><init>(Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;Landroid/view/View;)V

    return-object v1
.end method

.method public setItems([Ljava/lang/String;)V
    .locals 0
    .param p1, "titles"    # [Ljava/lang/String;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 36
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->items:[Ljava/lang/String;

    .line 37
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessAdapter;->notifyDataSetChanged()V

    .line 38
    return-void
.end method
