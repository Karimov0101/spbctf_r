.class public Lcom/safonov/speedreading/main/fragment/settings/TimePreference;
.super Landroid/support/v7/preference/DialogPreference;
.source "TimePreference.java"


# static fields
.field private static final DEFAULT_VALUE:I = 0x492

.field private static final MINUTES_PER_HOUR:I = 0x3c


# instance fields
.field private timeInMinutes:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/support/v7/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    const-string v0, "TimePreference"

    const-string v1, "TimePreference"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 25
    return-void
.end method

.method private timeToString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 28
    const-string v0, "%02d:%02d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->getHours()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->getMinutes()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public convertToSaveFormat(II)I
    .locals 1
    .param p1, "hours"    # I
    .param p2, "minutes"    # I

    .prologue
    .line 66
    mul-int/lit8 v0, p1, 0x3c

    add-int/2addr v0, p2

    return v0
.end method

.method public getHours()I
    .locals 1

    .prologue
    .line 62
    iget v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method

.method public getMinutes()I
    .locals 3

    .prologue
    .line 57
    iget v1, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    div-int/lit8 v0, v1, 0x3c

    .line 58
    .local v0, "hours":I
    iget v1, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    mul-int/lit8 v2, v0, 0x3c

    sub-int/2addr v1, v2

    return v1
.end method

.method protected onGetDefaultValue(Landroid/content/res/TypedArray;I)Ljava/lang/Object;
    .locals 2
    .param p1, "a"    # Landroid/content/res/TypedArray;
    .param p2, "index"    # I

    .prologue
    .line 39
    const-string v0, "TimePreference"

    const-string v1, "onGetDefaultValue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const/16 v0, 0x492

    invoke-virtual {p1, p2, v0}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onSetInitialValue(ZLjava/lang/Object;)V
    .locals 2
    .param p1, "restoreValue"    # Z
    .param p2, "defaultValue"    # Ljava/lang/Object;

    .prologue
    .line 45
    if-eqz p1, :cond_0

    .line 46
    const-string v0, "TimePreference"

    const-string v1, "restoreValue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    const/16 v0, 0x492

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->getPersistedInt(I)I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    .line 53
    .end local p2    # "defaultValue":Ljava/lang/Object;
    :goto_0
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->updateSummary()V

    .line 54
    return-void

    .line 49
    .restart local p2    # "defaultValue":Ljava/lang/Object;
    :cond_0
    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "defaultValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    .line 50
    const-string v0, "TimePreference"

    const-string v1, "Not restoreValue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iget v0, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->persistInt(I)Z

    goto :goto_0
.end method

.method public persist(I)V
    .locals 0
    .param p1, "convertedValue"    # I

    .prologue
    .line 70
    iput p1, p0, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeInMinutes:I

    .line 71
    invoke-virtual {p0, p1}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->persistInt(I)Z

    .line 72
    return-void
.end method

.method public updateSummary()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->timeToString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/settings/TimePreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 33
    const-string v0, "TimePreference"

    const-string v1, "updateSummary"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-void
.end method
