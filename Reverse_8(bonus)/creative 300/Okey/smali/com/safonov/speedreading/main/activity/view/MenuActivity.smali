.class public Lcom/safonov/speedreading/main/activity/view/MenuActivity;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;
.source "MenuActivity.java"

# interfaces
.implements Lcom/safonov/speedreading/main/activity/view/IMenuView;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;
.implements Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpActivity",
        "<",
        "Lcom/safonov/speedreading/main/activity/view/IMenuView;",
        "Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;",
        ">;",
        "Lcom/safonov/speedreading/main/activity/view/IMenuView;",
        "Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;",
        "Lcom/safonov/speedreading/reader/library/library/view/LibraryFragmentListener;",
        "Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragmentListener;"
    }
.end annotation


# static fields
.field public static final BOOK_ID:Ljava/lang/String; = "book_id"

.field public static final LOAD_BOOK:Ljava/lang/String; = "load_book"

.field private static final PURCHASE_REQUEST_CODE:I = 0x3ea

.field public static final SHOULD_LOAD_BOOK:Ljava/lang/String; = "should_load_book"


# instance fields
.field private bookController:Lcom/safonov/speedreading/reader/repository/IBookController;

.field private bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

.field private checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

.field private currentFragment:Landroid/support/v4/app/Fragment;

.field filePath:Ljava/lang/String;

.field private interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

.field private listener:Lcom/safonov/speedreading/reader/library/fileexplorer/async/BookAddAsyncTaskListener;

.field private menu:Landroid/view/Menu;

.field navigation:Landroid/support/design/widget/BottomNavigationView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090043
    .end annotation
.end field

.field notifyKey:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e00db
    .end annotation
.end field

.field notifyTimeKey:Ljava/lang/String;
    .annotation build Lbutterknife/BindString;
        value = 0x7f0e00dc
    .end annotation
.end field

.field private onNavigationItemSelectedListener:Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;

.field path:Ljava/lang/String;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field purchasePremiumView:Landroid/view/View;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09000c
    .end annotation
.end field

.field sharedPreferences:Landroid/content/SharedPreferences;

.field toolbar:Landroid/support/v7/widget/Toolbar;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09020d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;-><init>()V

    .line 104
    new-instance v0, Lcom/google/android/gms/ads/InterstitialAd;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/InterstitialAd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    .line 106
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    .line 107
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBookUtil()Lcom/safonov/speedreading/application/util/BookUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    .line 108
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getBilling()Lorg/solovyev/android/checkout/Billing;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Checkout;->forActivity(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ActivityCheckout;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    .line 335
    new-instance v0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;

    invoke-direct {v0, p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$6;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V

    iput-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->onNavigationItemSelectedListener:Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/BookUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->bookUtil:Lcom/safonov/speedreading/application/util/BookUtil;

    return-object v0
.end method

.method static synthetic access$100(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/safonov/speedreading/application/util/PremiumUtil;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-object v0
.end method

.method static synthetic access$200(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$600(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    return-object v0
.end method

.method static synthetic access$700(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/google/android/gms/ads/InterstitialAd;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 92
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    return-object v0
.end method

.method private copyFile(Ljava/lang/String;)V
    .locals 9
    .param p1, "fileName"    # Ljava/lang/String;

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 321
    .local v0, "assetManager":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 322
    .local v3, "in":Ljava/io/InputStream;
    new-instance v4, Ljava/io/FileOutputStream;

    iget-object v6, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-direct {v4, v6}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 323
    .local v4, "out":Ljava/io/OutputStream;
    const/16 v6, 0x400

    new-array v1, v6, [B

    .line 324
    .local v1, "buffer":[B
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    .line 325
    .local v5, "read":I
    :goto_0
    const/4 v6, -0x1

    if-eq v5, v6, :cond_0

    .line 326
    const/4 v6, 0x0

    invoke-virtual {v4, v1, v6, v5}, Ljava/io/OutputStream;->write([BII)V

    .line 327
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    move-result v5

    goto :goto_0

    .line 329
    :cond_0
    iget-object v6, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "should_load_book"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    .end local v1    # "buffer":[B
    .end local v3    # "in":Ljava/io/InputStream;
    .end local v4    # "out":Ljava/io/OutputStream;
    .end local v5    # "read":I
    :goto_1
    return-void

    .line 330
    :catch_0
    move-exception v2

    .line 331
    .local v2, "e":Ljava/lang/Exception;
    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    goto :goto_1
.end method

.method private setFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2
    .param p1, "fragment"    # Landroid/support/v4/app/Fragment;

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 628
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 629
    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 630
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 632
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    .line 633
    return-void
.end method


# virtual methods
.method public bridge synthetic createPresenter()Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->createPresenter()Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    move-result-object v0

    return-object v0
.end method

.method public createPresenter()Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 101
    new-instance v0, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/activity/presenter/MenuPresenter;-><init>()V

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 609
    invoke-super {p0, p1, p2, p3}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 610
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/ActivityCheckout;->onActivityResult(IILandroid/content/Intent;)Z

    .line 611
    const/16 v0, 0x3ea

    if-ne p1, v0, :cond_0

    .line 612
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 613
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->purchasePremiumView:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 616
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v0}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->onBackPressed()V

    .line 428
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 124
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onCreate(Landroid/os/Bundle;)V

    .line 125
    const v8, 0x7f0b0048

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->setContentView(I)V

    .line 127
    invoke-static {p0}, Lcom/safonov/speedreading/application/util/ValueAnimatorEnablerUtil;->init(Landroid/content/Context;)V

    .line 128
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v8}, Lorg/solovyev/android/checkout/ActivityCheckout;->start()V

    .line 129
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-static {}, Lorg/solovyev/android/checkout/Inventory$Request;->create()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v9

    invoke-virtual {v9}, Lorg/solovyev/android/checkout/Inventory$Request;->loadAllPurchases()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v9

    new-instance v10, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;

    invoke-direct {v10, p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$1;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V

    invoke-virtual {v8, v9, v10}, Lorg/solovyev/android/checkout/ActivityCheckout;->loadInventory(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)Lorg/solovyev/android/checkout/Inventory;

    .line 153
    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v9, 0x1a

    if-ge v8, v9, :cond_0

    .line 154
    new-instance v8, Landroid/content/Intent;

    const-class v9, Lcom/safonov/speedreading/application/notifications/AlarmHandleService;

    invoke-direct {v8, p0, v9}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 157
    :cond_0
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    const v9, 0x7f0e0091

    invoke-virtual {p0, v9}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/gms/ads/InterstitialAd;->setAdUnitId(Ljava/lang/String;)V

    .line 158
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v8}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v8

    if-nez v8, :cond_1

    .line 159
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v9, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v9}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v9

    invoke-virtual {v8, v9}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 162
    :cond_1
    invoke-static {p0}, Lbutterknife/ButterKnife;->bind(Landroid/app/Activity;)Lbutterknife/Unbinder;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->unbinder:Lbutterknife/Unbinder;

    .line 165
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->toolbar:Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0, v8}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->setSupportActionBar(Landroid/support/v7/widget/Toolbar;)V

    .line 166
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v8

    const v9, 0x7f080066

    invoke-virtual {v8, v9}, Landroid/support/v7/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 167
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->navigation:Landroid/support/design/widget/BottomNavigationView;

    iget-object v9, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->onNavigationItemSelectedListener:Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;

    invoke-virtual {v8, v9}, Landroid/support/design/widget/BottomNavigationView;->setOnNavigationItemSelectedListener(Landroid/support/design/widget/BottomNavigationView$OnNavigationItemSelectedListener;)V

    .line 169
    const-string v8, "load_book"

    const/4 v9, 0x0

    invoke-virtual {p0, v8, v9}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    .line 171
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "should_load_book"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v8

    const-string v9, "ru"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 173
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "books"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->path:Ljava/lang/String;

    .line 175
    invoke-static {p0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v8

    .line 176
    invoke-virtual {v8}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v2

    .line 178
    .local v2, "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    invoke-interface {v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v4

    .line 179
    .local v4, "bookId":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->path:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "fb2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    .line 181
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-interface {v2, v8}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 183
    .local v1, "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    new-instance v1, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-direct {v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 184
    .restart local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v1, v4, v5}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 186
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "book_id"

    invoke-interface {v8, v9, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 188
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 189
    const-string v8, "fb2"

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e018b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 192
    .local v6, "bookTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e018a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 194
    .local v3, "bookLanguage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0185

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 196
    invoke-virtual {v1, v6}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 197
    invoke-virtual {v1, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 199
    invoke-interface {v2, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 201
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 203
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_2

    .line 204
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 205
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SpeedReading.json"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    .line 206
    const-string v8, "SpeedReading.json"

    invoke-direct {p0, v8}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->copyFile(Ljava/lang/String;)V

    .line 209
    :cond_2
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 211
    .local v0, "ad":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0189

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 212
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0188

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 214
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0187

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/safonov/speedreading/main/activity/view/MenuActivity$2;

    invoke-direct {v9, p0, v4, v5}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$2;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;J)V

    invoke-virtual {v0, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 233
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0186

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/safonov/speedreading/main/activity/view/MenuActivity$3;

    invoke-direct {v9, p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$3;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V

    invoke-virtual {v0, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 238
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    .line 311
    .end local v0    # "ad":Landroid/support/v7/app/AlertDialog$Builder;
    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    .end local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    .end local v3    # "bookLanguage":Ljava/lang/String;
    .end local v4    # "bookId":J
    .end local v6    # "bookTitle":Ljava/lang/String;
    .end local v7    # "file":Ljava/io/File;
    :cond_3
    :goto_0
    return-void

    .line 241
    :cond_4
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    const-string v9, "should_load_book"

    const/4 v10, 0x1

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 242
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v9

    iget-object v9, v9, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "books"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->path:Ljava/lang/String;

    .line 244
    invoke-static {p0}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/SQLiteDaoFactory;->getDaoFactory(Landroid/content/Context;)Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;

    move-result-object v8

    .line 245
    invoke-virtual {v8}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/sqlliteimpl/BookDescriptionDaoFactory;->getBookDescriptionDao()Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;

    move-result-object v2

    .line 247
    .restart local v2    # "bookDescriptionDao":Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;
    invoke-interface {v2}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->getNextItemId()J

    move-result-wide v4

    .line 248
    .restart local v4    # "bookId":J
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->path:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "fb2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    .line 250
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-interface {v2, v8}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->findBookDescription(Ljava/lang/String;)Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    move-result-object v1

    .line 252
    .restart local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    new-instance v1, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .end local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-direct {v1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;-><init>()V

    .line 253
    .restart local v1    # "bookDescription":Lcom/safonov/speedreading/reader/repository/entity/BookDescription;
    invoke-virtual {v1, v4, v5}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setId(J)V

    .line 255
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->sharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    const-string v9, "book_id"

    invoke-interface {v8, v9, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 257
    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setFilePath(Ljava/lang/String;)V

    .line 258
    const-string v8, "fb2"

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setType(Ljava/lang/String;)V

    .line 260
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e018b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 261
    .restart local v6    # "bookTitle":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e018a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 263
    .restart local v3    # "bookLanguage":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0185

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v8}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setAuthor(Ljava/lang/String;)V

    .line 265
    invoke-virtual {v1, v6}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setTitle(Ljava/lang/String;)V

    .line 266
    invoke-virtual {v1, v3}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->setLanguage(Ljava/lang/String;)V

    .line 268
    invoke-interface {v2, v1}, Lcom/safonov/speedreading/reader/repository/dao/bookdescriptiondao/IBookDescriptionDao;->addBookDescription(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)J

    .line 270
    new-instance v7, Ljava/io/File;

    iget-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 272
    .restart local v7    # "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-nez v8, :cond_5

    .line 273
    invoke-virtual {v7}, Ljava/io/File;->mkdirs()Z

    .line 274
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sget-object v9, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "SpeedReading_en.json"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->filePath:Ljava/lang/String;

    .line 275
    const-string v8, "SpeedReading_en.json"

    invoke-direct {p0, v8}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->copyFile(Ljava/lang/String;)V

    .line 278
    :cond_5
    new-instance v0, Landroid/support/v7/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/support/v7/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 280
    .restart local v0    # "ad":Landroid/support/v7/app/AlertDialog$Builder;
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0189

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/support/v7/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 281
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0188

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/support/v7/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 283
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0187

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;

    invoke-direct {v9, p0, v4, v5}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$4;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;J)V

    invoke-virtual {v0, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 302
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0186

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/safonov/speedreading/main/activity/view/MenuActivity$5;

    invoke-direct {v9, p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$5;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)V

    invoke-virtual {v0, v8, v9}, Landroid/support/v7/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/support/v7/app/AlertDialog$Builder;

    .line 307
    invoke-virtual {v0}, Landroid/support/v7/app/AlertDialog$Builder;->show()Landroid/support/v7/app/AlertDialog;

    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 379
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->MAIN_MENU:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 380
    const/4 v0, 0x1

    return v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 620
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onDestroy()V

    .line 621
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->checkout:Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ActivityCheckout;->stop()V

    .line 622
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 623
    return-void
.end method

.method public onFileExplorerBackPressed()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->currentFragment:Landroid/support/v4/app/Fragment;

    check-cast v0, Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;

    invoke-interface {v0}, Lcom/safonov/speedreading/reader/library/activity/view/BackPressedListener;->onBackPressed()V

    .line 433
    return-void
.end method

.method public onFileExplorerBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 671
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 672
    return-void
.end method

.method public onFileExplorerClose()V
    .locals 2

    .prologue
    .line 666
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->LIBRARY:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 667
    return-void
.end method

.method public onLibraryAddBookClick()V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->FILE_EXPLORER:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 652
    return-void
.end method

.method public onLibraryBookOpen(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 1
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 656
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V

    .line 657
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 394
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 410
    invoke-super {p0, p1}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    :goto_0
    return v1

    .line 396
    :sswitch_0
    iget-object v1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v1, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->onActionBarHomePressed()V

    move v1, v2

    .line 397
    goto :goto_0

    .line 399
    :sswitch_1
    iget-object v1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v1, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->onActionBarSettingsPressed()V

    move v1, v2

    .line 400
    goto :goto_0

    .line 402
    :sswitch_2
    iget-object v1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v1, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->onActionBarHelpPressed()V

    move v1, v2

    .line 403
    goto :goto_0

    .line 405
    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.SEND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "text/plain"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v3, "android.intent.extra.TEXT"

    const v4, 0x7f0e0033

    .line 406
    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 407
    .local v0, "sendIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivity(Landroid/content/Intent;)V

    move v1, v2

    .line 408
    goto :goto_0

    .line 394
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0900a9 -> :sswitch_2
        0x7f09019d -> :sswitch_1
        0x7f09019e -> :sswitch_3
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 603
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onPause()V

    .line 604
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 605
    return-void
.end method

.method public onPremiumClick()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09000c
        }
    .end annotation

    .prologue
    .line 389
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/safonov/speedreading/purchase/view/PurchaseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 390
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 596
    invoke-super {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpActivity;->onResume()V

    .line 597
    iget-object v1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->purchasePremiumView:Landroid/view/View;

    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 598
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 599
    return-void

    .line 597
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 2
    .param p1, "sharedPreferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 587
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->notifyKey:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->notifyTimeKey:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 588
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    if-ge v0, v1, :cond_1

    .line 589
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/safonov/speedreading/application/notifications/AlarmHandleService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 592
    :cond_1
    return-void
.end method

.method public requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V
    .locals 1
    .param p1, "menuFragmentType"    # Lcom/safonov/speedreading/main/MenuFragmentType;

    .prologue
    .line 437
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->presenter:Lcom/hannesdorfmann/mosby3/mvp/MvpPresenter;

    check-cast v0, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;

    invoke-interface {v0, p1}, Lcom/safonov/speedreading/main/activity/presenter/IMenuPresenter;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 438
    return-void
.end method

.method public requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 2
    .param p1, "trainingType"    # Lcom/safonov/speedreading/training/TrainingType;

    .prologue
    .line 559
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "training_type"

    .line 561
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 560
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 580
    :goto_0
    return-void

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->isLoaded()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 566
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/InterstitialAd;->show()V

    .line 567
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->interstitialAd:Lcom/google/android/gms/ads/InterstitialAd;

    new-instance v1, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;-><init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;Lcom/safonov/speedreading/training/TrainingType;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    goto :goto_0

    .line 577
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "training_type"

    .line 578
    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    .line 577
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public setAcceleratorTrainingMenuFragment()V
    .locals 3

    .prologue
    .line 493
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 494
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 495
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 496
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 498
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 499
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 500
    return-void
.end method

.method public setActionBarItemsVisible(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 423
    return-void
.end method

.method public setActionBarTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 662
    return-void
.end method

.method public setBookDetailFragment(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)V
    .locals 2
    .param p1, "bookDescription"    # Lcom/safonov/speedreading/reader/repository/entity/BookDescription;

    .prologue
    .line 637
    invoke-static {p1}, Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;->newInstance(Lcom/safonov/speedreading/reader/repository/entity/BookDescription;)Lcom/safonov/speedreading/reader/library/detail/view/BookDetailFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 639
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    invoke-virtual {p1}, Lcom/safonov/speedreading/reader/repository/entity/BookDescription;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 640
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 641
    return-void
.end method

.method public setDescriptionFragment()V
    .locals 3

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 465
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 466
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 467
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 469
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0090

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 470
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 471
    return-void
.end method

.method public setFileExplorerFragment()V
    .locals 2

    .prologue
    .line 645
    invoke-static {}, Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;->newInstance()Lcom/safonov/speedreading/reader/library/fileexplorer/view/FileExplorerFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 646
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 647
    return-void
.end method

.method public setLibraryFragment()V
    .locals 2

    .prologue
    .line 475
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0098

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 476
    invoke-static {}, Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;->newInstance()Lcom/safonov/speedreading/reader/library/library/view/LibraryFragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->setFragment(Landroid/support/v4/app/Fragment;)V

    .line 477
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 478
    return-void
.end method

.method public setMainMenuFragment()V
    .locals 3

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 443
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 444
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 445
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 447
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0031

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 448
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 449
    return-void
.end method

.method public setMotivatorsFragment()V
    .locals 3

    .prologue
    .line 526
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 527
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 528
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 529
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 531
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e00d2

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 532
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 533
    return-void
.end method

.method public setMotivatorsMenuFragment()V
    .locals 3

    .prologue
    .line 504
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 505
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 506
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 507
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 509
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e003f

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 510
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 511
    return-void
.end method

.method public setProfileFragment()V
    .locals 3

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 455
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 456
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 458
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0040

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 459
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 460
    return-void
.end method

.method public setRecommendationFragment()V
    .locals 3

    .prologue
    .line 548
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 549
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 550
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 551
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 553
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0149

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 554
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 555
    return-void
.end method

.method public setRulesOfSuccessFragment()V
    .locals 3

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 538
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 539
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/rulesofsuccess/view/RulesOfSuccessFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 540
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 542
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e0155

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 543
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 544
    return-void
.end method

.method public setSettingsFragment()V
    .locals 3

    .prologue
    .line 515
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 517
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/settings/MainSettingsFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 518
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 520
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e017a

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 521
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 522
    return-void
.end method

.method public setTrainingMenuFragment()V
    .locals 3

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    .line 483
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f09009f

    .line 484
    invoke-static {}, Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;->newInstance()Lcom/safonov/speedreading/main/fragment/trainingmenu/view/TrainingMenuFragment;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    .line 485
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 487
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const v1, 0x7f0e019c

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setTitle(I)V

    .line 488
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->getSupportActionBar()Landroid/support/v7/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 489
    return-void
.end method
