.class Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;
.super Lcom/google/android/gms/ads/AdListener;
.source "MenuActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/main/activity/view/MenuActivity;->requestToStartTraining(Lcom/safonov/speedreading/training/TrainingType;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

.field final synthetic val$trainingType:Lcom/safonov/speedreading/training/TrainingType;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/activity/view/MenuActivity;Lcom/safonov/speedreading/training/TrainingType;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    .prologue
    .line 567
    iput-object p1, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    iput-object p2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->val$trainingType:Lcom/safonov/speedreading/training/TrainingType;

    invoke-direct {p0}, Lcom/google/android/gms/ads/AdListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAdClosed()V
    .locals 4

    .prologue
    .line 570
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    invoke-static {v0}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->access$700(Lcom/safonov/speedreading/main/activity/view/MenuActivity;)Lcom/google/android/gms/ads/InterstitialAd;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/InterstitialAd;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    .line 572
    iget-object v0, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->this$0:Lcom/safonov/speedreading/main/activity/view/MenuActivity;

    const-class v3, Lcom/safonov/speedreading/training/activity/view/TrainingActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "training_type"

    iget-object v3, p0, Lcom/safonov/speedreading/main/activity/view/MenuActivity$7;->val$trainingType:Lcom/safonov/speedreading/training/TrainingType;

    .line 573
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    .line 572
    invoke-virtual {v0, v1}, Lcom/safonov/speedreading/main/activity/view/MenuActivity;->startActivity(Landroid/content/Intent;)V

    .line 574
    return-void
.end method
