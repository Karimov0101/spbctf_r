.class public Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;
.super Ljava/lang/Object;
.source "MainMenuFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

.field private view2131296487:Landroid/view/View;

.field private view2131296489:Landroid/view/View;

.field private view2131296604:Landroid/view/View;

.field private view2131296692:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    .line 34
    const v2, 0x7f09015d

    const-string v3, "field \'recomendationTitleView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationTitleView:Landroid/widget/TextView;

    .line 35
    const v2, 0x7f09015a

    const-string v3, "field \'recomendationDescriptionView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationDescriptionView:Landroid/widget/TextView;

    .line 36
    const v2, 0x7f0901b8

    const-string v3, "field \'speedReadingBookStartTV\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookStartTV:Landroid/widget/TextView;

    .line 37
    const v2, 0x7f0901b5

    const-string v3, "field \'speedReadingBookNotTV\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookNotTV:Landroid/widget/TextView;

    .line 38
    const v2, 0x7f090149

    const-string v3, "field \'profileTitleTV\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->profileTitleTV:Landroid/widget/TextView;

    .line 39
    const v2, 0x7f090188

    const-string v3, "field \'scoreTextView\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->scoreTextView:Landroid/widget/TextView;

    .line 40
    const v2, 0x7f090138

    const-string v3, "field \'pointsLeftTV\'"

    const-class v4, Landroid/widget/TextView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->pointsLeftTV:Landroid/widget/TextView;

    .line 41
    const v2, 0x7f090097

    const-string v3, "field \'focusAttentionPB\'"

    const-class v4, Landroid/widget/ProgressBar;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->focusAttentionPB:Landroid/widget/ProgressBar;

    .line 42
    const v2, 0x7f09015c

    const-string v3, "method \'onRecomendationClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 43
    .local v1, "view":Landroid/view/View;
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296604:Landroid/view/View;

    .line 44
    new-instance v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$1;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    const v2, 0x7f0901b4

    const-string v3, "method \'onspeedReadingBookClick2\' and method \'speedReadingBookClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 51
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296692:Landroid/view/View;

    .line 52
    new-instance v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$2;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    const v2, 0x7f0900e7

    const-string v3, "method \'onStartPassCourseClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 60
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296487:Landroid/view/View;

    .line 61
    new-instance v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$3;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 67
    const v2, 0x7f0900e9

    const-string v3, "method \'onRandomTrainerClick\'"

    invoke-static {p2, v2, v3}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v1

    .line 68
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296489:Landroid/view/View;

    .line 69
    new-instance v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$4;

    invoke-direct {v2, p0, p1}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 76
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 77
    .local v0, "context":Landroid/content/Context;
    const v2, 0x7f06002d

    invoke-static {v0, v2}, Landroid/support/v4/content/ContextCompat;->getColor(Landroid/content/Context;I)I

    move-result v2

    iput v2, p1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->colorAccent:I

    .line 78
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 83
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    .line 84
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 85
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;

    .line 87
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationTitleView:Landroid/widget/TextView;

    .line 88
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->recomendationDescriptionView:Landroid/widget/TextView;

    .line 89
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookStartTV:Landroid/widget/TextView;

    .line 90
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->speedReadingBookNotTV:Landroid/widget/TextView;

    .line 91
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->profileTitleTV:Landroid/widget/TextView;

    .line 92
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->scoreTextView:Landroid/widget/TextView;

    .line 93
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->pointsLeftTV:Landroid/widget/TextView;

    .line 94
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment;->focusAttentionPB:Landroid/widget/ProgressBar;

    .line 96
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296604:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296604:Landroid/view/View;

    .line 98
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296692:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296692:Landroid/view/View;

    .line 100
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296487:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 101
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296487:Landroid/view/View;

    .line 102
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296489:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/MainMenuFragment_ViewBinding;->view2131296489:Landroid/view/View;

    .line 104
    return-void
.end method
