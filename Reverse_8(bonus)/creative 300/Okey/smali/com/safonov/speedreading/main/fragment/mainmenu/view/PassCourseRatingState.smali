.class public final enum Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
.super Ljava/lang/Enum;
.source "PassCourseRatingState.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum GENIUS:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum GURU:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum LEARNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum MASTER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

.field public static final enum PROFESSOR:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;


# instance fields
.field private final progress:I

.field private final titleRes:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 12
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "BEGINNER"

    const v2, 0x7f0e00e3

    invoke-direct {v0, v1, v5, v2, v5}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 13
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "LEARNER"

    const v2, 0x7f0e00e7

    const/16 v3, 0x23

    invoke-direct {v0, v1, v6, v2, v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->LEARNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 14
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "EXPERT"

    const v2, 0x7f0e00e4

    const/16 v3, 0x32

    invoke-direct {v0, v1, v7, v2, v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 15
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "MASTER"

    const v2, 0x7f0e00e8

    const/16 v3, 0x46

    invoke-direct {v0, v1, v8, v2, v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->MASTER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 16
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "GURU"

    const v2, 0x7f0e00e6

    const/16 v3, 0x50

    invoke-direct {v0, v1, v9, v2, v3}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GURU:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 17
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "PROFESSOR"

    const/4 v2, 0x5

    const v3, 0x7f0e00ea

    const/16 v4, 0x5a

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->PROFESSOR:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 18
    new-instance v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const-string v1, "GENIUS"

    const/4 v2, 0x6

    const v3, 0x7f0e00e5

    const/16 v4, 0x64

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;-><init>(Ljava/lang/String;III)V

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GENIUS:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 11
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    sget-object v1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->LEARNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v1, v0, v7

    sget-object v1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->MASTER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v1, v0, v8

    sget-object v1, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GURU:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->PROFESSOR:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GENIUS:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->$VALUES:[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3, "titleRes"    # I
    .param p4, "progress"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 24
    iput p3, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->titleRes:I

    .line 25
    iput p4, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->progress:I

    .line 26
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 11
    const-class v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    return-object v0
.end method

.method public static values()[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->$VALUES:[Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    invoke-virtual {v0}, [Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    return-object v0
.end method


# virtual methods
.method public getCompletedPart()I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->progress:I

    return v0
.end method

.method public getTitleRes()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 30
    iget v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->titleRes:I

    return v0
.end method

.method public getUncompletedPart()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->progress:I

    rsub-int/lit8 v0, v0, 0x64

    return v0
.end method
