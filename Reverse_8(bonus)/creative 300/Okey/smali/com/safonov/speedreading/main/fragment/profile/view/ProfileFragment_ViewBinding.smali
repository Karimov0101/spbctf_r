.class public Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;
.super Ljava/lang/Object;
.source "ProfileFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

.field private view2131296583:Landroid/view/View;

.field private view2131296587:Landroid/view/View;

.field private view2131296588:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

    .line 29
    const v1, 0x7f09014d

    const-string v2, "field \'profileTypeTV\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileTypeTV:Landroid/widget/TextView;

    .line 30
    const v1, 0x7f09014a

    const-string v2, "field \'profileRatingTv\'"

    const-class v3, Landroid/widget/TextView;

    invoke-static {p2, v1, v2, v3}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileRatingTv:Landroid/widget/TextView;

    .line 31
    const v1, 0x7f090147

    const-string v2, "method \'onPrivacyPolicyClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296583:Landroid/view/View;

    .line 33
    new-instance v1, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v1, 0x7f09014b

    const-string v2, "method \'onSettingsStart\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296587:Landroid/view/View;

    .line 41
    new-instance v1, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v1, 0x7f09014c

    const-string v2, "method \'onShareStart\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296588:Landroid/view/View;

    .line 49
    new-instance v1, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 60
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

    .line 61
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 62
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

    .line 64
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileTypeTV:Landroid/widget/TextView;

    .line 65
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileRatingTv:Landroid/widget/TextView;

    .line 67
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296583:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296583:Landroid/view/View;

    .line 69
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296587:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 70
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296587:Landroid/view/View;

    .line 71
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296588:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 72
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment_ViewBinding;->view2131296588:Landroid/view/View;

    .line 73
    return-void
.end method
