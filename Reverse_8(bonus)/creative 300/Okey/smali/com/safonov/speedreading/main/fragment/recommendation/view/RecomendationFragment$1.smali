.class Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;
.super Ljava/lang/Object;
.source "RecomendationFragment.java"

# interfaces
.implements Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;


# direct methods
.method constructor <init>(Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;)V
    .locals 0
    .param p1, "this$0"    # Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;->this$0:Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;->this$0:Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    invoke-static {v2}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->access$000(Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;

    .line 53
    .local v1, "recommendation":Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1}, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->getLink()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 54
    .local v0, "browserIntent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;->this$0:Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    invoke-virtual {v2, v0}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->startActivity(Landroid/content/Intent;)V

    .line 55
    return-void
.end method
