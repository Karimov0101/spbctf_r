.class public Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;
.super Lcom/safonov/speedreading/main/fragment/recommendation/view/RecommendationAbstractFragment;
.source "RecomendationFragment.java"


# instance fields
.field private recommendationAdapter:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

.field private recommendationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecommendationAbstractFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;-><init>()V

    .line 27
    .local v0, "fragment":Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;
    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 36
    invoke-super {p0, p1, p2, p3}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecommendationAbstractFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v5

    .line 38
    .local v5, "view":Landroid/view/View;
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f03001b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 39
    .local v4, "recommendationTitleArray":[Ljava/lang/String;
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f030019

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    .line 40
    .local v1, "recommendatioMessageArray":[Ljava/lang/String;
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v6

    invoke-virtual {v6}, Lcom/safonov/speedreading/application/App;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f03001a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 42
    .local v3, "recommendationLinkArray":[Ljava/lang/String;
    new-instance v6, Ljava/util/ArrayList;

    array-length v7, v4

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationList:Ljava/util/ArrayList;

    .line 44
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v6, v4

    if-ge v0, v6, :cond_0

    .line 45
    new-instance v2, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;

    aget-object v6, v4, v0

    aget-object v7, v1, v0

    aget-object v8, v3, v0

    invoke-direct {v2, v6, v7, v8}, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    .local v2, "recommendation":Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
    iget-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationList:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    .end local v2    # "recommendation":Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
    :cond_0
    new-instance v6, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

    new-instance v7, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;

    invoke-direct {v7, p0}, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;)V

    invoke-direct {v6, v7}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;-><init>(Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter$OnItemClickListener;)V

    iput-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationAdapter:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

    .line 57
    iget-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->setHasFixedSize(Z)V

    .line 58
    iget-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recyclerView:Landroid/support/v7/widget/RecyclerView;

    iget-object v7, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationAdapter:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

    invoke-virtual {v6, v7}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 59
    iget-object v6, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationAdapter:Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;

    iget-object v7, p0, Lcom/safonov/speedreading/main/fragment/recommendation/view/RecomendationFragment;->recommendationList:Ljava/util/ArrayList;

    invoke-virtual {v6, v7}, Lcom/safonov/speedreading/main/fragment/recommendation/RecommendationAdapter;->setItems(Ljava/util/List;)V

    .line 61
    return-object v5
.end method
