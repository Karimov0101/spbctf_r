.class public Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;
.super Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;
.source "MainMenuPresenter.java"

# interfaces
.implements Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter",
        "<",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;",
        ">;",
        "Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/IMainMenuPresenter;"
    }
.end annotation


# static fields
.field private static final MAX_SCORE:I = 0x3e8


# instance fields
.field private passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;)V
    .locals 0
    .param p1, "passCourseRepository"    # Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/hannesdorfmann/mosby3/mvp/MvpBasePresenter;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    .line 23
    return-void
.end method

.method private getRatingState(I)Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 26
    const/16 v0, 0x3e8

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GENIUS:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 33
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const/16 v0, 0x384

    if-lt p1, v0, :cond_1

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->PROFESSOR:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 28
    :cond_1
    const/16 v0, 0x2ee

    if-lt p1, v0, :cond_2

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GURU:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 29
    :cond_2
    const/16 v0, 0x28a

    if-lt p1, v0, :cond_3

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->MASTER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 30
    :cond_3
    const/16 v0, 0x1f4

    if-lt p1, v0, :cond_4

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 31
    :cond_4
    const/16 v0, 0x15e

    if-lt p1, v0, :cond_5

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->LEARNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 33
    :cond_5
    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0
.end method

.method private getUncompletedPart(I)I
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 39
    rsub-int v0, p1, 0x3e8

    .line 40
    .local v0, "uncompletedPart":I
    if-gez v0, :cond_0

    .line 41
    const/4 v0, 0x0

    .line 44
    .end local v0    # "uncompletedPart":I
    :cond_0
    return v0
.end method


# virtual methods
.method public init()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 49
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;->getResultList()Ljava/util/List;

    move-result-object v7

    .line 51
    .local v7, "courseResultList":Ljava/util/List;, "Ljava/util/List<Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;>;"
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 52
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;

    sget-object v3, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    const/16 v5, 0x3e8

    move v2, v1

    move v4, v1

    invoke-interface/range {v0 .. v5}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;->setPassCourseView(IILcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;II)V

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .line 57
    .local v8, "result":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->passCourseRepository:Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;

    invoke-interface {v0}, Lcom/safonov/speedreading/training/fragment/passcource/repository/IPassCourseRepository;->getBestResult()Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v6

    .line 59
    .local v6, "bestResult":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    invoke-virtual {v8}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->getScore()I

    move-result v1

    .line 60
    .local v1, "score":I
    invoke-virtual {v6}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->getScore()I

    move-result v2

    .line 61
    .local v2, "bestScore":I
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->getRatingState(I)Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    move-result-object v3

    .line 63
    .local v3, "bestRatingState":Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    move v4, v2

    .line 64
    .local v4, "completedPart":I
    invoke-direct {p0, v2}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->getUncompletedPart(I)I

    move-result v5

    .line 66
    .local v5, "uncompletedPart":I
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->isViewAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/mainmenu/presenter/MainMenuPresenter;->getView()Lcom/hannesdorfmann/mosby3/mvp/MvpView;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;

    invoke-interface/range {v0 .. v5}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/IMainMenuView;->setPassCourseView(IILcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;II)V

    goto :goto_0
.end method
