.class public Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;
.super Ljava/lang/Object;
.source "Recommendation.java"


# instance fields
.field private description:Ljava/lang/String;

.field private link:Ljava/lang/String;

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "description"    # Ljava/lang/String;
    .param p3, "link"    # Ljava/lang/String;

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 9
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->title:Ljava/lang/String;

    .line 10
    iput-object p2, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->description:Ljava/lang/String;

    .line 11
    iput-object p3, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->link:Ljava/lang/String;

    .line 12
    return-void
.end method


# virtual methods
.method public getDescription()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->description:Ljava/lang/String;

    return-object v0
.end method

.method public getLink()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->link:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 15
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->title:Ljava/lang/String;

    return-object v0
.end method

.method public setDescription(Ljava/lang/String;)V
    .locals 0
    .param p1, "description"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->description:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public setLink(Ljava/lang/String;)V
    .locals 0
    .param p1, "link"    # Ljava/lang/String;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->link:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 19
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/recommendation/model/Recommendation;->title:Ljava/lang/String;

    .line 20
    return-void
.end method
