.class Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;
.super Landroid/support/v4/app/FragmentStatePagerAdapter;
.source "MotivatorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MotivatorsAdapter"
.end annotation


# static fields
.field public static final ITEMS_COUNT:I = 0x19


# instance fields
.field private messages:[Ljava/lang/String;

.field private random:Ljava/util/Random;

.field final synthetic this$0:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;


# direct methods
.method private constructor <init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;Landroid/support/v4/app/FragmentManager;[Ljava/lang/String;)V
    .locals 1
    .param p2, "fm"    # Landroid/support/v4/app/FragmentManager;
    .param p3, "messages"    # [Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->this$0:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    .line 106
    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentStatePagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    .line 102
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->random:Ljava/util/Random;

    .line 107
    iput-object p3, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->messages:[Ljava/lang/String;

    .line 108
    return-void
.end method

.method synthetic constructor <init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;Landroid/support/v4/app/FragmentManager;[Ljava/lang/String;Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
    .param p2, "x1"    # Landroid/support/v4/app/FragmentManager;
    .param p3, "x2"    # [Ljava/lang/String;
    .param p4, "x3"    # Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$1;

    .prologue
    .line 98
    invoke-direct {p0, p1, p2, p3}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;-><init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;Landroid/support/v4/app/FragmentManager;[Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 117
    const v0, 0x7fffffff

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->messages:[Ljava/lang/String;

    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->random:Ljava/util/Random;

    iget-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;->messages:[Ljava/lang/String;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget-object v0, v0, v1

    invoke-static {v0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;->newInstance(Ljava/lang/String;)Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;

    move-result-object v0

    return-object v0
.end method
