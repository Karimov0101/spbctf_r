.class public Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment_ViewBinding;
.super Ljava/lang/Object;
.source "DescriptionFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;

    .line 21
    const v0, 0x7f090022

    const-string v1, "field \'adView\'"

    const-class v2, Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, v0, v1, v2}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/AdView;

    iput-object v0, p1, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 22
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 27
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;

    .line 28
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 29
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;

    .line 31
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 32
    return-void
.end method
