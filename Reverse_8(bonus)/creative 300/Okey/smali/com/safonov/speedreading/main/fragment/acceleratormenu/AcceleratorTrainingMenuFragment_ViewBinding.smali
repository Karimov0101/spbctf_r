.class public Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;
.super Ljava/lang/Object;
.source "AcceleratorTrainingMenuFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

.field private view2131296787:Landroid/view/View;

.field private view2131296791:Landroid/view/View;

.field private view2131296800:Landroid/view/View;

.field private view2131296802:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;Landroid/view/View;)V
    .locals 3
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

    .line 31
    const v1, 0x7f090222

    const-string v2, "method \'onStartAcceleratorClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "view":Landroid/view/View;
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296802:Landroid/view/View;

    .line 33
    new-instance v1, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    const v1, 0x7f090220

    const-string v2, "method \'onStartWordBlockClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 40
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296800:Landroid/view/View;

    .line 41
    new-instance v1, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    const v1, 0x7f090217

    const-string v2, "method \'onStartFlashWordClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 48
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296791:Landroid/view/View;

    .line 49
    new-instance v1, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 55
    const v1, 0x7f090213

    const-string v2, "method \'onStartAcceleratorPassCourseClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 56
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296787:Landroid/view/View;

    .line 57
    new-instance v1, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 2
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 68
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bindings already cleared."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment;

    .line 72
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296802:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 73
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296802:Landroid/view/View;

    .line 74
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296800:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296800:Landroid/view/View;

    .line 76
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296791:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 77
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296791:Landroid/view/View;

    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296787:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 79
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/acceleratormenu/AcceleratorTrainingMenuFragment_ViewBinding;->view2131296787:Landroid/view/View;

    .line 80
    return-void
.end method
