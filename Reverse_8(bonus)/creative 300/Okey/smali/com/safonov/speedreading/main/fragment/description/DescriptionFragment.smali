.class public Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;
.super Landroid/support/v4/app/Fragment;
.source "DescriptionFragment.java"


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;
    .locals 1

    .prologue
    .line 30
    new-instance v0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    const v1, 0x7f0b004f

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 45
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->unbinder:Lbutterknife/Unbinder;

    .line 47
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 48
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 66
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 63
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 84
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 85
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 86
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 72
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 73
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 77
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 78
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/description/DescriptionFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 79
    return-void
.end method
