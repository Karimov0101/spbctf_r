.class public Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment_ViewBinding;
.super Ljava/lang/Object;
.source "MotivatorsFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;Landroid/view/View;)V
    .locals 5
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    .line 24
    const v2, 0x7f090243

    const-string v3, "field \'viewPager\'"

    const-class v4, Landroid/support/v4/view/ViewPager;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 25
    const v2, 0x7f090022

    const-string v3, "field \'adView\'"

    const-class v4, Lcom/google/android/gms/ads/AdView;

    invoke-static {p2, v2, v3, v4}, Lbutterknife/internal/Utils;->findRequiredViewAsType(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/ads/AdView;

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 27
    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 28
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 29
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f030014

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v2

    iput-object v2, p1, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->motivatorMessageArray:[Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    .line 36
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 37
    :cond_0
    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    .line 39
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 40
    iput-object v1, v0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    .line 41
    return-void
.end method
