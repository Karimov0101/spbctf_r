.class public Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;
.super Landroid/support/v4/app/Fragment;
.source "ProfileFragment.java"


# instance fields
.field private fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

.field private passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field profileRatingTv:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014a
    .end annotation
.end field

.field profileTypeTV:Landroid/widget/TextView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f09014d
    .end annotation
.end field

.field private unbinder:Lbutterknife/Unbinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 43
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method private getRatingState(I)Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    .locals 1
    .param p1, "score"    # I

    .prologue
    .line 47
    const/16 v0, 0x3e8

    if-lt p1, v0, :cond_0

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GENIUS:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    .line 54
    :goto_0
    return-object v0

    .line 48
    :cond_0
    const/16 v0, 0x384

    if-lt p1, v0, :cond_1

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->PROFESSOR:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 49
    :cond_1
    const/16 v0, 0x2ee

    if-lt p1, v0, :cond_2

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->GURU:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 50
    :cond_2
    const/16 v0, 0x28a

    if-lt p1, v0, :cond_3

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->MASTER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 51
    :cond_3
    const/16 v0, 0x1f4

    if-lt p1, v0, :cond_4

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->EXPERT:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 52
    :cond_4
    const/16 v0, 0x15e

    if-lt p1, v0, :cond_5

    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->LEARNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0

    .line 54
    :cond_5
    sget-object v0, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    goto :goto_0
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onAttach(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/content/Context;)V

    .line 96
    instance-of v0, p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    if-eqz v0, :cond_0

    .line 97
    check-cast p1, Lcom/safonov/speedreading/main/MenuFragmentListener;

    .end local p1    # "context":Landroid/content/Context;
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 102
    return-void

    .line 99
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must implement MenuFragmentListener"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 60
    const v3, 0x7f0b0081

    const/4 v4, 0x0

    invoke-virtual {p1, v3, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 62
    .local v2, "view":Landroid/view/View;
    invoke-static {p0, v2}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v3

    iput-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->unbinder:Lbutterknife/Unbinder;

    .line 64
    new-instance v3, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v4

    invoke-virtual {v4}, Lcom/safonov/speedreading/application/App;->getPassCourseConfiguration()Lio/realm/Realm;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;-><init>(Lio/realm/Realm;)V

    iput-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    .line 65
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->passCourseRealmUtil:Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/training/fragment/passcource/repository/PassCourseRealmUtil;->getBestResult()Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v1

    .line 67
    .local v1, "bestResult":Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;
    if-nez v1, :cond_1

    .line 68
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileRatingTv:Landroid/widget/TextView;

    sget-object v4, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->BEGINNER:Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    invoke-virtual {v4}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getTitleRes()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    :goto_0
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v3}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 78
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileTypeTV:Landroid/widget/TextView;

    const v4, 0x7f0e00fd

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 79
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileTypeTV:Landroid/widget/TextView;

    const v4, 0x7f0800ec

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 82
    :cond_0
    return-object v2

    .line 71
    :cond_1
    invoke-virtual {v1}, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;->getScore()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->getRatingState(I)Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;

    move-result-object v0

    .line 72
    .local v0, "bestRatingState":Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;
    iget-object v3, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->profileRatingTv:Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/safonov/speedreading/main/fragment/mainmenu/view/PassCourseRatingState;->getTitleRes()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 89
    return-void
.end method

.method public onDetach()V
    .locals 1

    .prologue
    .line 112
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    .line 114
    return-void
.end method

.method public onPrivacyPolicyClick()V
    .locals 3
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f090147
        }
    .end annotation

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    const-string v2, "http://speedreadingteam.com/privacy_policy.html"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 107
    .local v0, "browserIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 108
    return-void
.end method

.method public onSettingsStart()V
    .locals 2
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09014b
        }
    .end annotation

    .prologue
    .line 118
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->fragmentListener:Lcom/safonov/speedreading/main/MenuFragmentListener;

    sget-object v1, Lcom/safonov/speedreading/main/MenuFragmentType;->SETTINGS:Lcom/safonov/speedreading/main/MenuFragmentType;

    invoke-interface {v0, v1}, Lcom/safonov/speedreading/main/MenuFragmentListener;->requestToSetFragment(Lcom/safonov/speedreading/main/MenuFragmentType;)V

    .line 119
    return-void
.end method

.method public onShareStart()V
    .locals 4
    .annotation build Lbutterknife/OnClick;
        value = {
            0x7f09014c
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.extra.TEXT"

    const v3, 0x7f0e0033

    .line 124
    invoke-virtual {p0, v3}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 125
    .local v0, "sendIntent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/safonov/speedreading/main/fragment/profile/view/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    .line 126
    return-void
.end method
