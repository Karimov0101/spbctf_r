.class public Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;
.super Ljava/lang/Object;
.source "MotivatorsMenuFragment_ViewBinding.java"

# interfaces
.implements Lbutterknife/Unbinder;


# instance fields
.field private target:Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

.field private view2131296374:Landroid/view/View;

.field private view2131296523:Landroid/view/View;

.field private view2131296524:Landroid/view/View;

.field private view2131296606:Landroid/view/View;

.field private view2131296633:Landroid/view/View;


# direct methods
.method public constructor <init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;Landroid/view/View;)V
    .locals 4
    .param p1, "target"    # Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;
    .param p2, "source"    # Landroid/view/View;
    .annotation build Landroid/support/annotation/UiThread;
    .end annotation

    .prologue
    const v3, 0x7f09010b

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

    .line 33
    const-string v1, "field \'speedReadingBookView\' and method \'onSpeedReadingBookClick\'"

    invoke-static {p2, v3, v1}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 34
    .local v0, "view":Landroid/view/View;
    const-string v1, "field \'speedReadingBookView\'"

    const-class v2, Landroid/support/constraint/ConstraintLayout;

    invoke-static {v0, v3, v1, v2}, Lbutterknife/internal/Utils;->castView(Landroid/view/View;ILjava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/constraint/ConstraintLayout;

    iput-object v1, p1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->speedReadingBookView:Landroid/support/constraint/ConstraintLayout;

    .line 35
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296523:Landroid/view/View;

    .line 36
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$1;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$1;-><init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 42
    const v1, 0x7f090076

    const-string v2, "method \'onDescriptionClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 43
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296374:Landroid/view/View;

    .line 44
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$2;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$2;-><init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 50
    const v1, 0x7f09015e

    const-string v2, "method \'onRecommendationsClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 51
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296606:Landroid/view/View;

    .line 52
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$3;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$3;-><init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 58
    const v1, 0x7f090179

    const-string v2, "method \'onRulesOfSuccessClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 59
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296633:Landroid/view/View;

    .line 60
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$4;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$4;-><init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 66
    const v1, 0x7f09010c

    const-string v2, "method \'onMotivatorsClick\'"

    invoke-static {p2, v1, v2}, Lbutterknife/internal/Utils;->findRequiredView(Landroid/view/View;ILjava/lang/String;)Landroid/view/View;

    move-result-object v0

    .line 67
    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296524:Landroid/view/View;

    .line 68
    new-instance v1, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$5;

    invoke-direct {v1, p0, p1}, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding$5;-><init>(Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    return-void
.end method


# virtual methods
.method public unbind()V
    .locals 3
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 79
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

    .line 80
    .local v0, "target":Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;
    if-nez v0, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Bindings already cleared."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 81
    :cond_0
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->target:Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;

    .line 83
    iput-object v2, v0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment;->speedReadingBookView:Landroid/support/constraint/ConstraintLayout;

    .line 85
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296523:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296523:Landroid/view/View;

    .line 87
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296374:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 88
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296374:Landroid/view/View;

    .line 89
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296606:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 90
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296606:Landroid/view/View;

    .line 91
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296633:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296633:Landroid/view/View;

    .line 93
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296524:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    iput-object v2, p0, Lcom/safonov/speedreading/main/fragment/motivatorsmenu/view/MotivatorsMenuFragment_ViewBinding;->view2131296524:Landroid/view/View;

    .line 95
    return-void
.end method
