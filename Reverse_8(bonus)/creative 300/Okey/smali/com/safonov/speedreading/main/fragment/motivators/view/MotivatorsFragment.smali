.class public Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
.super Landroid/support/v4/app/Fragment;
.source "MotivatorsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorPageFragment;,
        Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;
    }
.end annotation


# instance fields
.field adView:Lcom/google/android/gms/ads/AdView;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090022
    .end annotation
.end field

.field motivatorMessageArray:[Ljava/lang/String;
    .annotation build Lbutterknife/BindArray;
        value = 0x7f030014
    .end annotation
.end field

.field private premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

.field private unbinder:Lbutterknife/Unbinder;

.field viewPager:Landroid/support/v4/view/ViewPager;
    .annotation build Lbutterknife/BindView;
        value = 0x7f090243
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 38
    invoke-static {}, Lcom/safonov/speedreading/application/App;->get()Lcom/safonov/speedreading/application/App;

    move-result-object v0

    invoke-virtual {v0}, Lcom/safonov/speedreading/application/App;->getPremiumUtil()Lcom/safonov/speedreading/application/util/PremiumUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    return-void
.end method

.method public static newInstance()Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;

    invoke-direct {v0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;-><init>()V

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 50
    const v1, 0x7f0b0054

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 52
    .local v0, "view":Landroid/view/View;
    invoke-static {p0, v0}, Lbutterknife/ButterKnife;->bind(Ljava/lang/Object;Landroid/view/View;)Lbutterknife/Unbinder;

    move-result-object v1

    iput-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->unbinder:Lbutterknife/Unbinder;

    .line 54
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;

    invoke-virtual {p0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    iget-object v4, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->motivatorMessageArray:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$MotivatorsAdapter;-><init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;Landroid/support/v4/app/FragmentManager;[Ljava/lang/String;Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$1;)V

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 55
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 57
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->premiumUtil:Lcom/safonov/speedreading/application/util/PremiumUtil;

    invoke-virtual {v1}, Lcom/safonov/speedreading/application/util/PremiumUtil;->isPremiumUser()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setVisibility(I)V

    .line 76
    :goto_0
    return-object v0

    .line 60
    :cond_0
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$1;

    invoke-direct {v2, p0}, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment$1;-><init>(Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->setAdListener(Lcom/google/android/gms/ads/AdListener;)V

    .line 73
    iget-object v1, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    new-instance v2, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/ads/AdView;->loadAd(Lcom/google/android/gms/ads/AdRequest;)V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 93
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 94
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->destroy()V

    .line 95
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->unbinder:Lbutterknife/Unbinder;

    invoke-interface {v0}, Lbutterknife/Unbinder;->unbind()V

    .line 96
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->pause()V

    .line 82
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 83
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 88
    iget-object v0, p0, Lcom/safonov/speedreading/main/fragment/motivators/view/MotivatorsFragment;->adView:Lcom/google/android/gms/ads/AdView;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdView;->resume()V

    .line 89
    return-void
.end method
