.class public final Lcom/safonov/speedreading/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final ActionBar:[I

.field public static final ActionBarLayout:[I

.field public static final ActionBarLayout_android_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0x0

.field public static final ActionBar_backgroundSplit:I = 0x1

.field public static final ActionBar_backgroundStacked:I = 0x2

.field public static final ActionBar_contentInsetEnd:I = 0x3

.field public static final ActionBar_contentInsetEndWithActions:I = 0x4

.field public static final ActionBar_contentInsetLeft:I = 0x5

.field public static final ActionBar_contentInsetRight:I = 0x6

.field public static final ActionBar_contentInsetStart:I = 0x7

.field public static final ActionBar_contentInsetStartWithNavigation:I = 0x8

.field public static final ActionBar_customNavigationLayout:I = 0x9

.field public static final ActionBar_displayOptions:I = 0xa

.field public static final ActionBar_divider:I = 0xb

.field public static final ActionBar_elevation:I = 0xc

.field public static final ActionBar_height:I = 0xd

.field public static final ActionBar_hideOnContentScroll:I = 0xe

.field public static final ActionBar_homeAsUpIndicator:I = 0xf

.field public static final ActionBar_homeLayout:I = 0x10

.field public static final ActionBar_icon:I = 0x11

.field public static final ActionBar_indeterminateProgressStyle:I = 0x12

.field public static final ActionBar_itemPadding:I = 0x13

.field public static final ActionBar_logo:I = 0x14

.field public static final ActionBar_navigationMode:I = 0x15

.field public static final ActionBar_popupTheme:I = 0x16

.field public static final ActionBar_progressBarPadding:I = 0x17

.field public static final ActionBar_progressBarStyle:I = 0x18

.field public static final ActionBar_subtitle:I = 0x19

.field public static final ActionBar_subtitleTextStyle:I = 0x1a

.field public static final ActionBar_title:I = 0x1b

.field public static final ActionBar_titleTextStyle:I = 0x1c

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_android_minWidth:I = 0x0

.field public static final ActionMenuView:[I

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x0

.field public static final ActionMode_backgroundSplit:I = 0x1

.field public static final ActionMode_closeItemLayout:I = 0x2

.field public static final ActionMode_height:I = 0x3

.field public static final ActionMode_subtitleTextStyle:I = 0x4

.field public static final ActionMode_titleTextStyle:I = 0x5

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x0

.field public static final ActivityChooserView_initialActivityCount:I = 0x1

.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adSizes:I = 0x1

.field public static final AdsAttrs_adUnitId:I = 0x2

.field public static final AlertDialog:[I

.field public static final AlertDialog_android_layout:I = 0x0

.field public static final AlertDialog_buttonPanelSideLayout:I = 0x1

.field public static final AlertDialog_listItemLayout:I = 0x2

.field public static final AlertDialog_listLayout:I = 0x3

.field public static final AlertDialog_multiChoiceItemLayout:I = 0x4

.field public static final AlertDialog_showTitle:I = 0x5

.field public static final AlertDialog_singleChoiceItemLayout:I = 0x6

.field public static final AppBarLayout:[I

.field public static final AppBarLayoutStates:[I

.field public static final AppBarLayoutStates_state_collapsed:I = 0x0

.field public static final AppBarLayoutStates_state_collapsible:I = 0x1

.field public static final AppBarLayout_Layout:[I

.field public static final AppBarLayout_Layout_layout_scrollFlags:I = 0x0

.field public static final AppBarLayout_Layout_layout_scrollInterpolator:I = 0x1

.field public static final AppBarLayout_android_background:I = 0x0

.field public static final AppBarLayout_android_keyboardNavigationCluster:I = 0x2

.field public static final AppBarLayout_android_touchscreenBlocksFocus:I = 0x1

.field public static final AppBarLayout_elevation:I = 0x3

.field public static final AppBarLayout_expanded:I = 0x4

.field public static final AppCompatImageView:[I

.field public static final AppCompatImageView_android_src:I = 0x0

.field public static final AppCompatImageView_srcCompat:I = 0x1

.field public static final AppCompatImageView_tint:I = 0x2

.field public static final AppCompatImageView_tintMode:I = 0x3

.field public static final AppCompatSeekBar:[I

.field public static final AppCompatSeekBar_android_thumb:I = 0x0

.field public static final AppCompatSeekBar_tickMark:I = 0x1

.field public static final AppCompatSeekBar_tickMarkTint:I = 0x2

.field public static final AppCompatSeekBar_tickMarkTintMode:I = 0x3

.field public static final AppCompatTextHelper:[I

.field public static final AppCompatTextHelper_android_drawableBottom:I = 0x2

.field public static final AppCompatTextHelper_android_drawableEnd:I = 0x6

.field public static final AppCompatTextHelper_android_drawableLeft:I = 0x3

.field public static final AppCompatTextHelper_android_drawableRight:I = 0x4

.field public static final AppCompatTextHelper_android_drawableStart:I = 0x5

.field public static final AppCompatTextHelper_android_drawableTop:I = 0x1

.field public static final AppCompatTextHelper_android_textAppearance:I = 0x0

.field public static final AppCompatTextView:[I

.field public static final AppCompatTextView_android_textAppearance:I = 0x0

.field public static final AppCompatTextView_autoSizeMaxTextSize:I = 0x1

.field public static final AppCompatTextView_autoSizeMinTextSize:I = 0x2

.field public static final AppCompatTextView_autoSizePresetSizes:I = 0x3

.field public static final AppCompatTextView_autoSizeStepGranularity:I = 0x4

.field public static final AppCompatTextView_autoSizeTextType:I = 0x5

.field public static final AppCompatTextView_fontFamily:I = 0x6

.field public static final AppCompatTextView_textAllCaps:I = 0x7

.field public static final AppCompatTheme:[I

.field public static final AppCompatTheme_actionBarDivider:I = 0x2

.field public static final AppCompatTheme_actionBarItemBackground:I = 0x3

.field public static final AppCompatTheme_actionBarPopupTheme:I = 0x4

.field public static final AppCompatTheme_actionBarSize:I = 0x5

.field public static final AppCompatTheme_actionBarSplitStyle:I = 0x6

.field public static final AppCompatTheme_actionBarStyle:I = 0x7

.field public static final AppCompatTheme_actionBarTabBarStyle:I = 0x8

.field public static final AppCompatTheme_actionBarTabStyle:I = 0x9

.field public static final AppCompatTheme_actionBarTabTextStyle:I = 0xa

.field public static final AppCompatTheme_actionBarTheme:I = 0xb

.field public static final AppCompatTheme_actionBarWidgetTheme:I = 0xc

.field public static final AppCompatTheme_actionButtonStyle:I = 0xd

.field public static final AppCompatTheme_actionDropDownStyle:I = 0xe

.field public static final AppCompatTheme_actionMenuTextAppearance:I = 0xf

.field public static final AppCompatTheme_actionMenuTextColor:I = 0x10

.field public static final AppCompatTheme_actionModeBackground:I = 0x11

.field public static final AppCompatTheme_actionModeCloseButtonStyle:I = 0x12

.field public static final AppCompatTheme_actionModeCloseDrawable:I = 0x13

.field public static final AppCompatTheme_actionModeCopyDrawable:I = 0x14

.field public static final AppCompatTheme_actionModeCutDrawable:I = 0x15

.field public static final AppCompatTheme_actionModeFindDrawable:I = 0x16

.field public static final AppCompatTheme_actionModePasteDrawable:I = 0x17

.field public static final AppCompatTheme_actionModePopupWindowStyle:I = 0x18

.field public static final AppCompatTheme_actionModeSelectAllDrawable:I = 0x19

.field public static final AppCompatTheme_actionModeShareDrawable:I = 0x1a

.field public static final AppCompatTheme_actionModeSplitBackground:I = 0x1b

.field public static final AppCompatTheme_actionModeStyle:I = 0x1c

.field public static final AppCompatTheme_actionModeWebSearchDrawable:I = 0x1d

.field public static final AppCompatTheme_actionOverflowButtonStyle:I = 0x1e

.field public static final AppCompatTheme_actionOverflowMenuStyle:I = 0x1f

.field public static final AppCompatTheme_activityChooserViewStyle:I = 0x20

.field public static final AppCompatTheme_alertDialogButtonGroupStyle:I = 0x21

.field public static final AppCompatTheme_alertDialogCenterButtons:I = 0x22

.field public static final AppCompatTheme_alertDialogStyle:I = 0x23

.field public static final AppCompatTheme_alertDialogTheme:I = 0x24

.field public static final AppCompatTheme_android_windowAnimationStyle:I = 0x1

.field public static final AppCompatTheme_android_windowIsFloating:I = 0x0

.field public static final AppCompatTheme_autoCompleteTextViewStyle:I = 0x25

.field public static final AppCompatTheme_borderlessButtonStyle:I = 0x26

.field public static final AppCompatTheme_buttonBarButtonStyle:I = 0x27

.field public static final AppCompatTheme_buttonBarNegativeButtonStyle:I = 0x28

.field public static final AppCompatTheme_buttonBarNeutralButtonStyle:I = 0x29

.field public static final AppCompatTheme_buttonBarPositiveButtonStyle:I = 0x2a

.field public static final AppCompatTheme_buttonBarStyle:I = 0x2b

.field public static final AppCompatTheme_buttonStyle:I = 0x2c

.field public static final AppCompatTheme_buttonStyleSmall:I = 0x2d

.field public static final AppCompatTheme_checkboxStyle:I = 0x2e

.field public static final AppCompatTheme_checkedTextViewStyle:I = 0x2f

.field public static final AppCompatTheme_colorAccent:I = 0x30

.field public static final AppCompatTheme_colorBackgroundFloating:I = 0x31

.field public static final AppCompatTheme_colorButtonNormal:I = 0x32

.field public static final AppCompatTheme_colorControlActivated:I = 0x33

.field public static final AppCompatTheme_colorControlHighlight:I = 0x34

.field public static final AppCompatTheme_colorControlNormal:I = 0x35

.field public static final AppCompatTheme_colorError:I = 0x36

.field public static final AppCompatTheme_colorPrimary:I = 0x37

.field public static final AppCompatTheme_colorPrimaryDark:I = 0x38

.field public static final AppCompatTheme_colorSwitchThumbNormal:I = 0x39

.field public static final AppCompatTheme_controlBackground:I = 0x3a

.field public static final AppCompatTheme_dialogPreferredPadding:I = 0x3b

.field public static final AppCompatTheme_dialogTheme:I = 0x3c

.field public static final AppCompatTheme_dividerHorizontal:I = 0x3d

.field public static final AppCompatTheme_dividerVertical:I = 0x3e

.field public static final AppCompatTheme_dropDownListViewStyle:I = 0x3f

.field public static final AppCompatTheme_dropdownListPreferredItemHeight:I = 0x40

.field public static final AppCompatTheme_editTextBackground:I = 0x41

.field public static final AppCompatTheme_editTextColor:I = 0x42

.field public static final AppCompatTheme_editTextStyle:I = 0x43

.field public static final AppCompatTheme_homeAsUpIndicator:I = 0x44

.field public static final AppCompatTheme_imageButtonStyle:I = 0x45

.field public static final AppCompatTheme_listChoiceBackgroundIndicator:I = 0x46

.field public static final AppCompatTheme_listDividerAlertDialog:I = 0x47

.field public static final AppCompatTheme_listMenuViewStyle:I = 0x48

.field public static final AppCompatTheme_listPopupWindowStyle:I = 0x49

.field public static final AppCompatTheme_listPreferredItemHeight:I = 0x4a

.field public static final AppCompatTheme_listPreferredItemHeightLarge:I = 0x4b

.field public static final AppCompatTheme_listPreferredItemHeightSmall:I = 0x4c

.field public static final AppCompatTheme_listPreferredItemPaddingLeft:I = 0x4d

.field public static final AppCompatTheme_listPreferredItemPaddingRight:I = 0x4e

.field public static final AppCompatTheme_panelBackground:I = 0x4f

.field public static final AppCompatTheme_panelMenuListTheme:I = 0x50

.field public static final AppCompatTheme_panelMenuListWidth:I = 0x51

.field public static final AppCompatTheme_popupMenuStyle:I = 0x52

.field public static final AppCompatTheme_popupWindowStyle:I = 0x53

.field public static final AppCompatTheme_radioButtonStyle:I = 0x54

.field public static final AppCompatTheme_ratingBarStyle:I = 0x55

.field public static final AppCompatTheme_ratingBarStyleIndicator:I = 0x56

.field public static final AppCompatTheme_ratingBarStyleSmall:I = 0x57

.field public static final AppCompatTheme_searchViewStyle:I = 0x58

.field public static final AppCompatTheme_seekBarStyle:I = 0x59

.field public static final AppCompatTheme_selectableItemBackground:I = 0x5a

.field public static final AppCompatTheme_selectableItemBackgroundBorderless:I = 0x5b

.field public static final AppCompatTheme_spinnerDropDownItemStyle:I = 0x5c

.field public static final AppCompatTheme_spinnerStyle:I = 0x5d

.field public static final AppCompatTheme_switchStyle:I = 0x5e

.field public static final AppCompatTheme_textAppearanceLargePopupMenu:I = 0x5f

.field public static final AppCompatTheme_textAppearanceListItem:I = 0x60

.field public static final AppCompatTheme_textAppearanceListItemSecondary:I = 0x61

.field public static final AppCompatTheme_textAppearanceListItemSmall:I = 0x62

.field public static final AppCompatTheme_textAppearancePopupMenuHeader:I = 0x63

.field public static final AppCompatTheme_textAppearanceSearchResultSubtitle:I = 0x64

.field public static final AppCompatTheme_textAppearanceSearchResultTitle:I = 0x65

.field public static final AppCompatTheme_textAppearanceSmallPopupMenu:I = 0x66

.field public static final AppCompatTheme_textColorAlertDialogListItem:I = 0x67

.field public static final AppCompatTheme_textColorSearchUrl:I = 0x68

.field public static final AppCompatTheme_toolbarNavigationButtonStyle:I = 0x69

.field public static final AppCompatTheme_toolbarStyle:I = 0x6a

.field public static final AppCompatTheme_tooltipForegroundColor:I = 0x6b

.field public static final AppCompatTheme_tooltipFrameBackground:I = 0x6c

.field public static final AppCompatTheme_windowActionBar:I = 0x6d

.field public static final AppCompatTheme_windowActionBarOverlay:I = 0x6e

.field public static final AppCompatTheme_windowActionModeOverlay:I = 0x6f

.field public static final AppCompatTheme_windowFixedHeightMajor:I = 0x70

.field public static final AppCompatTheme_windowFixedHeightMinor:I = 0x71

.field public static final AppCompatTheme_windowFixedWidthMajor:I = 0x72

.field public static final AppCompatTheme_windowFixedWidthMinor:I = 0x73

.field public static final AppCompatTheme_windowMinWidthMajor:I = 0x74

.field public static final AppCompatTheme_windowMinWidthMinor:I = 0x75

.field public static final AppCompatTheme_windowNoTitle:I = 0x76

.field public static final BackgroundStyle:[I

.field public static final BackgroundStyle_android_selectableItemBackground:I = 0x0

.field public static final BackgroundStyle_selectableItemBackground:I = 0x1

.field public static final BottomNavigationView:[I

.field public static final BottomNavigationView_elevation:I = 0x0

.field public static final BottomNavigationView_itemBackground:I = 0x1

.field public static final BottomNavigationView_itemIconTint:I = 0x2

.field public static final BottomNavigationView_itemTextColor:I = 0x3

.field public static final BottomNavigationView_menu:I = 0x4

.field public static final BottomSheetBehavior_Layout:[I

.field public static final BottomSheetBehavior_Layout_behavior_hideable:I = 0x0

.field public static final BottomSheetBehavior_Layout_behavior_peekHeight:I = 0x1

.field public static final BottomSheetBehavior_Layout_behavior_skipCollapsed:I = 0x2

.field public static final ButtonBarLayout:[I

.field public static final ButtonBarLayout_allowStacking:I = 0x0

.field public static final CardView:[I

.field public static final CardView_android_minHeight:I = 0x1

.field public static final CardView_android_minWidth:I = 0x0

.field public static final CardView_cardBackgroundColor:I = 0x2

.field public static final CardView_cardCornerRadius:I = 0x3

.field public static final CardView_cardElevation:I = 0x4

.field public static final CardView_cardMaxElevation:I = 0x5

.field public static final CardView_cardPreventCornerOverlap:I = 0x6

.field public static final CardView_cardUseCompatPadding:I = 0x7

.field public static final CardView_contentPadding:I = 0x8

.field public static final CardView_contentPaddingBottom:I = 0x9

.field public static final CardView_contentPaddingLeft:I = 0xa

.field public static final CardView_contentPaddingRight:I = 0xb

.field public static final CardView_contentPaddingTop:I = 0xc

.field public static final CheckBoxPreference:[I

.field public static final CheckBoxPreference_android_disableDependentsState:I = 0x2

.field public static final CheckBoxPreference_android_summaryOff:I = 0x1

.field public static final CheckBoxPreference_android_summaryOn:I = 0x0

.field public static final CheckBoxPreference_disableDependentsState:I = 0x3

.field public static final CheckBoxPreference_summaryOff:I = 0x4

.field public static final CheckBoxPreference_summaryOn:I = 0x5

.field public static final CircularProgressBar:[I

.field public static final CircularProgressBar_back_line_color:I = 0x0

.field public static final CircularProgressBar_front_line_color:I = 0x1

.field public static final CircularProgressBar_line_width:I = 0x2

.field public static final CircularProgressBar_progress:I = 0x3

.field public static final CollapsingToolbarLayout:[I

.field public static final CollapsingToolbarLayout_Layout:[I

.field public static final CollapsingToolbarLayout_Layout_layout_collapseMode:I = 0x0

.field public static final CollapsingToolbarLayout_Layout_layout_collapseParallaxMultiplier:I = 0x1

.field public static final CollapsingToolbarLayout_collapsedTitleGravity:I = 0x0

.field public static final CollapsingToolbarLayout_collapsedTitleTextAppearance:I = 0x1

.field public static final CollapsingToolbarLayout_contentScrim:I = 0x2

.field public static final CollapsingToolbarLayout_expandedTitleGravity:I = 0x3

.field public static final CollapsingToolbarLayout_expandedTitleMargin:I = 0x4

.field public static final CollapsingToolbarLayout_expandedTitleMarginBottom:I = 0x5

.field public static final CollapsingToolbarLayout_expandedTitleMarginEnd:I = 0x6

.field public static final CollapsingToolbarLayout_expandedTitleMarginStart:I = 0x7

.field public static final CollapsingToolbarLayout_expandedTitleMarginTop:I = 0x8

.field public static final CollapsingToolbarLayout_expandedTitleTextAppearance:I = 0x9

.field public static final CollapsingToolbarLayout_scrimAnimationDuration:I = 0xa

.field public static final CollapsingToolbarLayout_scrimVisibleHeightTrigger:I = 0xb

.field public static final CollapsingToolbarLayout_statusBarScrim:I = 0xc

.field public static final CollapsingToolbarLayout_title:I = 0xd

.field public static final CollapsingToolbarLayout_titleEnabled:I = 0xe

.field public static final CollapsingToolbarLayout_toolbarId:I = 0xf

.field public static final ColorStateListItem:[I

.field public static final ColorStateListItem_alpha:I = 0x2

.field public static final ColorStateListItem_android_alpha:I = 0x1

.field public static final ColorStateListItem_android_color:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_android_button:I = 0x0

.field public static final CompoundButton_buttonTint:I = 0x1

.field public static final CompoundButton_buttonTintMode:I = 0x2

.field public static final ConstraintLayout_Layout:[I

.field public static final ConstraintLayout_Layout_android_maxHeight:I = 0x2

.field public static final ConstraintLayout_Layout_android_maxWidth:I = 0x1

.field public static final ConstraintLayout_Layout_android_minHeight:I = 0x4

.field public static final ConstraintLayout_Layout_android_minWidth:I = 0x3

.field public static final ConstraintLayout_Layout_android_orientation:I = 0x0

.field public static final ConstraintLayout_Layout_barrierAllowsGoneWidgets:I = 0x5

.field public static final ConstraintLayout_Layout_barrierDirection:I = 0x6

.field public static final ConstraintLayout_Layout_chainUseRtl:I = 0x7

.field public static final ConstraintLayout_Layout_constraintSet:I = 0x8

.field public static final ConstraintLayout_Layout_constraint_referenced_ids:I = 0x9

.field public static final ConstraintLayout_Layout_layout_constrainedHeight:I = 0xa

.field public static final ConstraintLayout_Layout_layout_constrainedWidth:I = 0xb

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_creator:I = 0xc

.field public static final ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf:I = 0xd

.field public static final ConstraintLayout_Layout_layout_constraintBottom_creator:I = 0xe

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toBottomOf:I = 0xf

.field public static final ConstraintLayout_Layout_layout_constraintBottom_toTopOf:I = 0x10

.field public static final ConstraintLayout_Layout_layout_constraintCircle:I = 0x11

.field public static final ConstraintLayout_Layout_layout_constraintCircleAngle:I = 0x12

.field public static final ConstraintLayout_Layout_layout_constraintCircleRadius:I = 0x13

.field public static final ConstraintLayout_Layout_layout_constraintDimensionRatio:I = 0x14

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toEndOf:I = 0x15

.field public static final ConstraintLayout_Layout_layout_constraintEnd_toStartOf:I = 0x16

.field public static final ConstraintLayout_Layout_layout_constraintGuide_begin:I = 0x17

.field public static final ConstraintLayout_Layout_layout_constraintGuide_end:I = 0x18

.field public static final ConstraintLayout_Layout_layout_constraintGuide_percent:I = 0x19

.field public static final ConstraintLayout_Layout_layout_constraintHeight_default:I = 0x1a

.field public static final ConstraintLayout_Layout_layout_constraintHeight_max:I = 0x1b

.field public static final ConstraintLayout_Layout_layout_constraintHeight_min:I = 0x1c

.field public static final ConstraintLayout_Layout_layout_constraintHeight_percent:I = 0x1d

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_bias:I = 0x1e

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle:I = 0x1f

.field public static final ConstraintLayout_Layout_layout_constraintHorizontal_weight:I = 0x20

.field public static final ConstraintLayout_Layout_layout_constraintLeft_creator:I = 0x21

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toLeftOf:I = 0x22

.field public static final ConstraintLayout_Layout_layout_constraintLeft_toRightOf:I = 0x23

.field public static final ConstraintLayout_Layout_layout_constraintRight_creator:I = 0x24

.field public static final ConstraintLayout_Layout_layout_constraintRight_toLeftOf:I = 0x25

.field public static final ConstraintLayout_Layout_layout_constraintRight_toRightOf:I = 0x26

.field public static final ConstraintLayout_Layout_layout_constraintStart_toEndOf:I = 0x27

.field public static final ConstraintLayout_Layout_layout_constraintStart_toStartOf:I = 0x28

.field public static final ConstraintLayout_Layout_layout_constraintTop_creator:I = 0x29

.field public static final ConstraintLayout_Layout_layout_constraintTop_toBottomOf:I = 0x2a

.field public static final ConstraintLayout_Layout_layout_constraintTop_toTopOf:I = 0x2b

.field public static final ConstraintLayout_Layout_layout_constraintVertical_bias:I = 0x2c

.field public static final ConstraintLayout_Layout_layout_constraintVertical_chainStyle:I = 0x2d

.field public static final ConstraintLayout_Layout_layout_constraintVertical_weight:I = 0x2e

.field public static final ConstraintLayout_Layout_layout_constraintWidth_default:I = 0x2f

.field public static final ConstraintLayout_Layout_layout_constraintWidth_max:I = 0x30

.field public static final ConstraintLayout_Layout_layout_constraintWidth_min:I = 0x31

.field public static final ConstraintLayout_Layout_layout_constraintWidth_percent:I = 0x32

.field public static final ConstraintLayout_Layout_layout_editor_absoluteX:I = 0x33

.field public static final ConstraintLayout_Layout_layout_editor_absoluteY:I = 0x34

.field public static final ConstraintLayout_Layout_layout_goneMarginBottom:I = 0x35

.field public static final ConstraintLayout_Layout_layout_goneMarginEnd:I = 0x36

.field public static final ConstraintLayout_Layout_layout_goneMarginLeft:I = 0x37

.field public static final ConstraintLayout_Layout_layout_goneMarginRight:I = 0x38

.field public static final ConstraintLayout_Layout_layout_goneMarginStart:I = 0x39

.field public static final ConstraintLayout_Layout_layout_goneMarginTop:I = 0x3a

.field public static final ConstraintLayout_Layout_layout_optimizationLevel:I = 0x3b

.field public static final ConstraintLayout_placeholder:[I

.field public static final ConstraintLayout_placeholder_content:I = 0x0

.field public static final ConstraintLayout_placeholder_emptyVisibility:I = 0x1

.field public static final ConstraintSet:[I

.field public static final ConstraintSet_android_alpha:I = 0x9

.field public static final ConstraintSet_android_elevation:I = 0x16

.field public static final ConstraintSet_android_id:I = 0x1

.field public static final ConstraintSet_android_layout_height:I = 0x4

.field public static final ConstraintSet_android_layout_marginBottom:I = 0x8

.field public static final ConstraintSet_android_layout_marginEnd:I = 0x14

.field public static final ConstraintSet_android_layout_marginLeft:I = 0x5

.field public static final ConstraintSet_android_layout_marginRight:I = 0x7

.field public static final ConstraintSet_android_layout_marginStart:I = 0x13

.field public static final ConstraintSet_android_layout_marginTop:I = 0x6

.field public static final ConstraintSet_android_layout_width:I = 0x3

.field public static final ConstraintSet_android_orientation:I = 0x0

.field public static final ConstraintSet_android_rotation:I = 0x10

.field public static final ConstraintSet_android_rotationX:I = 0x11

.field public static final ConstraintSet_android_rotationY:I = 0x12

.field public static final ConstraintSet_android_scaleX:I = 0xe

.field public static final ConstraintSet_android_scaleY:I = 0xf

.field public static final ConstraintSet_android_transformPivotX:I = 0xa

.field public static final ConstraintSet_android_transformPivotY:I = 0xb

.field public static final ConstraintSet_android_translationX:I = 0xc

.field public static final ConstraintSet_android_translationY:I = 0xd

.field public static final ConstraintSet_android_translationZ:I = 0x15

.field public static final ConstraintSet_android_visibility:I = 0x2

.field public static final ConstraintSet_layout_constrainedHeight:I = 0x17

.field public static final ConstraintSet_layout_constrainedWidth:I = 0x18

.field public static final ConstraintSet_layout_constraintBaseline_creator:I = 0x19

.field public static final ConstraintSet_layout_constraintBaseline_toBaselineOf:I = 0x1a

.field public static final ConstraintSet_layout_constraintBottom_creator:I = 0x1b

.field public static final ConstraintSet_layout_constraintBottom_toBottomOf:I = 0x1c

.field public static final ConstraintSet_layout_constraintBottom_toTopOf:I = 0x1d

.field public static final ConstraintSet_layout_constraintCircle:I = 0x1e

.field public static final ConstraintSet_layout_constraintCircleAngle:I = 0x1f

.field public static final ConstraintSet_layout_constraintCircleRadius:I = 0x20

.field public static final ConstraintSet_layout_constraintDimensionRatio:I = 0x21

.field public static final ConstraintSet_layout_constraintEnd_toEndOf:I = 0x22

.field public static final ConstraintSet_layout_constraintEnd_toStartOf:I = 0x23

.field public static final ConstraintSet_layout_constraintGuide_begin:I = 0x24

.field public static final ConstraintSet_layout_constraintGuide_end:I = 0x25

.field public static final ConstraintSet_layout_constraintGuide_percent:I = 0x26

.field public static final ConstraintSet_layout_constraintHeight_default:I = 0x27

.field public static final ConstraintSet_layout_constraintHeight_max:I = 0x28

.field public static final ConstraintSet_layout_constraintHeight_min:I = 0x29

.field public static final ConstraintSet_layout_constraintHeight_percent:I = 0x2a

.field public static final ConstraintSet_layout_constraintHorizontal_bias:I = 0x2b

.field public static final ConstraintSet_layout_constraintHorizontal_chainStyle:I = 0x2c

.field public static final ConstraintSet_layout_constraintHorizontal_weight:I = 0x2d

.field public static final ConstraintSet_layout_constraintLeft_creator:I = 0x2e

.field public static final ConstraintSet_layout_constraintLeft_toLeftOf:I = 0x2f

.field public static final ConstraintSet_layout_constraintLeft_toRightOf:I = 0x30

.field public static final ConstraintSet_layout_constraintRight_creator:I = 0x31

.field public static final ConstraintSet_layout_constraintRight_toLeftOf:I = 0x32

.field public static final ConstraintSet_layout_constraintRight_toRightOf:I = 0x33

.field public static final ConstraintSet_layout_constraintStart_toEndOf:I = 0x34

.field public static final ConstraintSet_layout_constraintStart_toStartOf:I = 0x35

.field public static final ConstraintSet_layout_constraintTop_creator:I = 0x36

.field public static final ConstraintSet_layout_constraintTop_toBottomOf:I = 0x37

.field public static final ConstraintSet_layout_constraintTop_toTopOf:I = 0x38

.field public static final ConstraintSet_layout_constraintVertical_bias:I = 0x39

.field public static final ConstraintSet_layout_constraintVertical_chainStyle:I = 0x3a

.field public static final ConstraintSet_layout_constraintVertical_weight:I = 0x3b

.field public static final ConstraintSet_layout_constraintWidth_default:I = 0x3c

.field public static final ConstraintSet_layout_constraintWidth_max:I = 0x3d

.field public static final ConstraintSet_layout_constraintWidth_min:I = 0x3e

.field public static final ConstraintSet_layout_constraintWidth_percent:I = 0x3f

.field public static final ConstraintSet_layout_editor_absoluteX:I = 0x40

.field public static final ConstraintSet_layout_editor_absoluteY:I = 0x41

.field public static final ConstraintSet_layout_goneMarginBottom:I = 0x42

.field public static final ConstraintSet_layout_goneMarginEnd:I = 0x43

.field public static final ConstraintSet_layout_goneMarginLeft:I = 0x44

.field public static final ConstraintSet_layout_goneMarginRight:I = 0x45

.field public static final ConstraintSet_layout_goneMarginStart:I = 0x46

.field public static final ConstraintSet_layout_goneMarginTop:I = 0x47

.field public static final CoordinatorLayout:[I

.field public static final CoordinatorLayout_Layout:[I

.field public static final CoordinatorLayout_Layout_android_layout_gravity:I = 0x0

.field public static final CoordinatorLayout_Layout_layout_anchor:I = 0x1

.field public static final CoordinatorLayout_Layout_layout_anchorGravity:I = 0x2

.field public static final CoordinatorLayout_Layout_layout_behavior:I = 0x3

.field public static final CoordinatorLayout_Layout_layout_dodgeInsetEdges:I = 0x4

.field public static final CoordinatorLayout_Layout_layout_insetEdge:I = 0x5

.field public static final CoordinatorLayout_Layout_layout_keyline:I = 0x6

.field public static final CoordinatorLayout_keylines:I = 0x0

.field public static final CoordinatorLayout_statusBarBackground:I = 0x1

.field public static final DesignTheme:[I

.field public static final DesignTheme_bottomSheetDialogTheme:I = 0x0

.field public static final DesignTheme_bottomSheetStyle:I = 0x1

.field public static final DesignTheme_textColorError:I = 0x2

.field public static final DialogPreference:[I

.field public static final DialogPreference_android_dialogIcon:I = 0x2

.field public static final DialogPreference_android_dialogLayout:I = 0x5

.field public static final DialogPreference_android_dialogMessage:I = 0x1

.field public static final DialogPreference_android_dialogTitle:I = 0x0

.field public static final DialogPreference_android_negativeButtonText:I = 0x4

.field public static final DialogPreference_android_positiveButtonText:I = 0x3

.field public static final DialogPreference_dialogIcon:I = 0x6

.field public static final DialogPreference_dialogLayout:I = 0x7

.field public static final DialogPreference_dialogMessage:I = 0x8

.field public static final DialogPreference_dialogTitle:I = 0x9

.field public static final DialogPreference_negativeButtonText:I = 0xa

.field public static final DialogPreference_positiveButtonText:I = 0xb

.field public static final DrawerArrowToggle:[I

.field public static final DrawerArrowToggle_arrowHeadLength:I = 0x0

.field public static final DrawerArrowToggle_arrowShaftLength:I = 0x1

.field public static final DrawerArrowToggle_barLength:I = 0x2

.field public static final DrawerArrowToggle_color:I = 0x3

.field public static final DrawerArrowToggle_drawableSize:I = 0x4

.field public static final DrawerArrowToggle_gapBetweenBars:I = 0x5

.field public static final DrawerArrowToggle_spinBars:I = 0x6

.field public static final DrawerArrowToggle_thickness:I = 0x7

.field public static final FloatingActionButton:[I

.field public static final FloatingActionButton_Behavior_Layout:[I

.field public static final FloatingActionButton_Behavior_Layout_behavior_autoHide:I = 0x0

.field public static final FloatingActionButton_backgroundTint:I = 0x0

.field public static final FloatingActionButton_backgroundTintMode:I = 0x1

.field public static final FloatingActionButton_borderWidth:I = 0x2

.field public static final FloatingActionButton_elevation:I = 0x3

.field public static final FloatingActionButton_fabSize:I = 0x4

.field public static final FloatingActionButton_pressedTranslationZ:I = 0x5

.field public static final FloatingActionButton_rippleColor:I = 0x6

.field public static final FloatingActionButton_useCompatPadding:I = 0x7

.field public static final FontFamily:[I

.field public static final FontFamilyFont:[I

.field public static final FontFamilyFont_font:I = 0x0

.field public static final FontFamilyFont_fontStyle:I = 0x1

.field public static final FontFamilyFont_fontWeight:I = 0x2

.field public static final FontFamily_fontProviderAuthority:I = 0x0

.field public static final FontFamily_fontProviderCerts:I = 0x1

.field public static final FontFamily_fontProviderFetchStrategy:I = 0x2

.field public static final FontFamily_fontProviderFetchTimeout:I = 0x3

.field public static final FontFamily_fontProviderPackage:I = 0x4

.field public static final FontFamily_fontProviderQuery:I = 0x5

.field public static final ForegroundLinearLayout:[I

.field public static final ForegroundLinearLayout_android_foreground:I = 0x0

.field public static final ForegroundLinearLayout_android_foregroundGravity:I = 0x1

.field public static final ForegroundLinearLayout_foregroundInsidePadding:I = 0x2

.field public static final GreenDotTextView:[I

.field public static final GreenDotTextView_frameColor:I = 0x0

.field public static final GreenDotTextView_greenDotFrameSize:I = 0x1

.field public static final GreenDotTextView_greenDotHeight:I = 0x2

.field public static final GreenDotTextView_greenDotWidth:I = 0x3

.field public static final GreenDotTextView_mainColor:I = 0x4

.field public static final GreenDotTextView_progress:I = 0x5

.field public static final GreenDotTextView_shadowColor:I = 0x6

.field public static final GridLayout:[I

.field public static final GridLayout_Layout:[I

.field public static final GridLayout_Layout_android_layout_height:I = 0x1

.field public static final GridLayout_Layout_android_layout_margin:I = 0x2

.field public static final GridLayout_Layout_android_layout_marginBottom:I = 0x6

.field public static final GridLayout_Layout_android_layout_marginLeft:I = 0x3

.field public static final GridLayout_Layout_android_layout_marginRight:I = 0x5

.field public static final GridLayout_Layout_android_layout_marginTop:I = 0x4

.field public static final GridLayout_Layout_android_layout_width:I = 0x0

.field public static final GridLayout_Layout_layout_column:I = 0x7

.field public static final GridLayout_Layout_layout_columnSpan:I = 0x8

.field public static final GridLayout_Layout_layout_columnWeight:I = 0x9

.field public static final GridLayout_Layout_layout_gravity:I = 0xa

.field public static final GridLayout_Layout_layout_row:I = 0xb

.field public static final GridLayout_Layout_layout_rowSpan:I = 0xc

.field public static final GridLayout_Layout_layout_rowWeight:I = 0xd

.field public static final GridLayout_alignmentMode:I = 0x0

.field public static final GridLayout_columnCount:I = 0x1

.field public static final GridLayout_columnOrderPreserved:I = 0x2

.field public static final GridLayout_orientation:I = 0x3

.field public static final GridLayout_rowCount:I = 0x4

.field public static final GridLayout_rowOrderPreserved:I = 0x5

.field public static final GridLayout_useDefaultMargins:I = 0x6

.field public static final HorizontalPicker:[I

.field public static final HorizontalPicker_android_ellipsize:I = 0x2

.field public static final HorizontalPicker_android_marqueeRepeatLimit:I = 0x3

.field public static final HorizontalPicker_android_textColor:I = 0x1

.field public static final HorizontalPicker_android_textSize:I = 0x0

.field public static final HorizontalPicker_dividerSize:I = 0x4

.field public static final HorizontalPicker_sideItems:I = 0x5

.field public static final HorizontalPicker_values:I = 0x6

.field public static final LinearConstraintLayout:[I

.field public static final LinearConstraintLayout_android_orientation:I = 0x0

.field public static final LinearLayoutCompat:[I

.field public static final LinearLayoutCompat_Layout:[I

.field public static final LinearLayoutCompat_Layout_android_layout_gravity:I = 0x0

.field public static final LinearLayoutCompat_Layout_android_layout_height:I = 0x2

.field public static final LinearLayoutCompat_Layout_android_layout_weight:I = 0x3

.field public static final LinearLayoutCompat_Layout_android_layout_width:I = 0x1

.field public static final LinearLayoutCompat_android_baselineAligned:I = 0x2

.field public static final LinearLayoutCompat_android_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayoutCompat_android_gravity:I = 0x0

.field public static final LinearLayoutCompat_android_orientation:I = 0x1

.field public static final LinearLayoutCompat_android_weightSum:I = 0x4

.field public static final LinearLayoutCompat_divider:I = 0x5

.field public static final LinearLayoutCompat_dividerPadding:I = 0x6

.field public static final LinearLayoutCompat_measureWithLargestChild:I = 0x7

.field public static final LinearLayoutCompat_showDividers:I = 0x8

.field public static final ListPopupWindow:[I

.field public static final ListPopupWindow_android_dropDownHorizontalOffset:I = 0x0

.field public static final ListPopupWindow_android_dropDownVerticalOffset:I = 0x1

.field public static final ListPreference:[I

.field public static final ListPreference_android_entries:I = 0x0

.field public static final ListPreference_android_entryValues:I = 0x1

.field public static final ListPreference_entries:I = 0x2

.field public static final ListPreference_entryValues:I = 0x3

.field public static final MenuGroup:[I

.field public static final MenuGroup_android_checkableBehavior:I = 0x5

.field public static final MenuGroup_android_enabled:I = 0x0

.field public static final MenuGroup_android_id:I = 0x1

.field public static final MenuGroup_android_menuCategory:I = 0x3

.field public static final MenuGroup_android_orderInCategory:I = 0x4

.field public static final MenuGroup_android_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItem_actionLayout:I = 0xd

.field public static final MenuItem_actionProviderClass:I = 0xe

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_alphabeticModifiers:I = 0x10

.field public static final MenuItem_android_alphabeticShortcut:I = 0x9

.field public static final MenuItem_android_checkable:I = 0xb

.field public static final MenuItem_android_checked:I = 0x3

.field public static final MenuItem_android_enabled:I = 0x1

.field public static final MenuItem_android_icon:I = 0x0

.field public static final MenuItem_android_id:I = 0x2

.field public static final MenuItem_android_menuCategory:I = 0x5

.field public static final MenuItem_android_numericShortcut:I = 0xa

.field public static final MenuItem_android_onClick:I = 0xc

.field public static final MenuItem_android_orderInCategory:I = 0x6

.field public static final MenuItem_android_title:I = 0x7

.field public static final MenuItem_android_titleCondensed:I = 0x8

.field public static final MenuItem_android_visible:I = 0x4

.field public static final MenuItem_contentDescription:I = 0x11

.field public static final MenuItem_iconTint:I = 0x12

.field public static final MenuItem_iconTintMode:I = 0x13

.field public static final MenuItem_numericModifiers:I = 0x14

.field public static final MenuItem_showAsAction:I = 0x15

.field public static final MenuItem_tooltipText:I = 0x16

.field public static final MenuView:[I

.field public static final MenuView_android_headerBackground:I = 0x4

.field public static final MenuView_android_horizontalDivider:I = 0x2

.field public static final MenuView_android_itemBackground:I = 0x5

.field public static final MenuView_android_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_android_itemTextAppearance:I = 0x1

.field public static final MenuView_android_verticalDivider:I = 0x3

.field public static final MenuView_android_windowAnimationStyle:I = 0x0

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_subMenuArrow:I = 0x8

.field public static final MultiSelectListPreference:[I

.field public static final MultiSelectListPreference_android_entries:I = 0x0

.field public static final MultiSelectListPreference_android_entryValues:I = 0x1

.field public static final MultiSelectListPreference_entries:I = 0x2

.field public static final MultiSelectListPreference_entryValues:I = 0x3

.field public static final NavigationView:[I

.field public static final NavigationView_android_background:I = 0x0

.field public static final NavigationView_android_fitsSystemWindows:I = 0x1

.field public static final NavigationView_android_maxWidth:I = 0x2

.field public static final NavigationView_elevation:I = 0x3

.field public static final NavigationView_headerLayout:I = 0x4

.field public static final NavigationView_itemBackground:I = 0x5

.field public static final NavigationView_itemIconTint:I = 0x6

.field public static final NavigationView_itemTextAppearance:I = 0x7

.field public static final NavigationView_itemTextColor:I = 0x8

.field public static final NavigationView_menu:I = 0x9

.field public static final PageIndicatorView:[I

.field public static final PageIndicatorView_piv_animationDuration:I = 0x0

.field public static final PageIndicatorView_piv_animationType:I = 0x1

.field public static final PageIndicatorView_piv_count:I = 0x2

.field public static final PageIndicatorView_piv_dynamicCount:I = 0x3

.field public static final PageIndicatorView_piv_interactiveAnimation:I = 0x4

.field public static final PageIndicatorView_piv_padding:I = 0x5

.field public static final PageIndicatorView_piv_radius:I = 0x6

.field public static final PageIndicatorView_piv_rtl_mode:I = 0x7

.field public static final PageIndicatorView_piv_scaleFactor:I = 0x8

.field public static final PageIndicatorView_piv_select:I = 0x9

.field public static final PageIndicatorView_piv_selectedColor:I = 0xa

.field public static final PageIndicatorView_piv_strokeWidth:I = 0xb

.field public static final PageIndicatorView_piv_unselectedColor:I = 0xc

.field public static final PageIndicatorView_piv_viewPager:I = 0xd

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_android_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_android_popupBackground:I = 0x0

.field public static final PopupWindow_overlapAnchor:I = 0x2

.field public static final Preference:[I

.field public static final PreferenceFragment:[I

.field public static final PreferenceFragmentCompat:[I

.field public static final PreferenceFragmentCompat_allowDividerAfterLastItem:I = 0x3

.field public static final PreferenceFragmentCompat_android_divider:I = 0x1

.field public static final PreferenceFragmentCompat_android_dividerHeight:I = 0x2

.field public static final PreferenceFragmentCompat_android_layout:I = 0x0

.field public static final PreferenceFragment_allowDividerAfterLastItem:I = 0x3

.field public static final PreferenceFragment_android_divider:I = 0x1

.field public static final PreferenceFragment_android_dividerHeight:I = 0x2

.field public static final PreferenceFragment_android_layout:I = 0x0

.field public static final PreferenceGroup:[I

.field public static final PreferenceGroup_android_orderingFromXml:I = 0x0

.field public static final PreferenceGroup_orderingFromXml:I = 0x1

.field public static final PreferenceImageView:[I

.field public static final PreferenceImageView_android_maxHeight:I = 0x1

.field public static final PreferenceImageView_android_maxWidth:I = 0x0

.field public static final PreferenceImageView_maxHeight:I = 0x2

.field public static final PreferenceImageView_maxWidth:I = 0x3

.field public static final PreferenceTheme:[I

.field public static final PreferenceTheme_checkBoxPreferenceStyle:I = 0x0

.field public static final PreferenceTheme_dialogPreferenceStyle:I = 0x1

.field public static final PreferenceTheme_dropdownPreferenceStyle:I = 0x2

.field public static final PreferenceTheme_editTextPreferenceStyle:I = 0x3

.field public static final PreferenceTheme_preferenceActivityStyle:I = 0x4

.field public static final PreferenceTheme_preferenceCategoryStyle:I = 0x5

.field public static final PreferenceTheme_preferenceFragmentCompatStyle:I = 0x6

.field public static final PreferenceTheme_preferenceFragmentListStyle:I = 0x7

.field public static final PreferenceTheme_preferenceFragmentPaddingSide:I = 0x8

.field public static final PreferenceTheme_preferenceFragmentStyle:I = 0x9

.field public static final PreferenceTheme_preferenceHeaderPanelStyle:I = 0xa

.field public static final PreferenceTheme_preferenceInformationStyle:I = 0xb

.field public static final PreferenceTheme_preferenceLayoutChild:I = 0xc

.field public static final PreferenceTheme_preferenceListStyle:I = 0xd

.field public static final PreferenceTheme_preferencePanelStyle:I = 0xe

.field public static final PreferenceTheme_preferenceScreenStyle:I = 0xf

.field public static final PreferenceTheme_preferenceStyle:I = 0x10

.field public static final PreferenceTheme_preferenceTheme:I = 0x11

.field public static final PreferenceTheme_ringtonePreferenceStyle:I = 0x12

.field public static final PreferenceTheme_seekBarPreferenceStyle:I = 0x13

.field public static final PreferenceTheme_switchPreferenceCompatStyle:I = 0x14

.field public static final PreferenceTheme_switchPreferenceStyle:I = 0x15

.field public static final PreferenceTheme_yesNoPreferenceStyle:I = 0x16

.field public static final Preference_allowDividerAbove:I = 0x10

.field public static final Preference_allowDividerBelow:I = 0x11

.field public static final Preference_android_defaultValue:I = 0xb

.field public static final Preference_android_dependency:I = 0xa

.field public static final Preference_android_enabled:I = 0x2

.field public static final Preference_android_fragment:I = 0xd

.field public static final Preference_android_icon:I = 0x0

.field public static final Preference_android_iconSpaceReserved:I = 0xf

.field public static final Preference_android_key:I = 0x6

.field public static final Preference_android_layout:I = 0x3

.field public static final Preference_android_order:I = 0x8

.field public static final Preference_android_persistent:I = 0x1

.field public static final Preference_android_selectable:I = 0x5

.field public static final Preference_android_shouldDisableView:I = 0xc

.field public static final Preference_android_singleLineTitle:I = 0xe

.field public static final Preference_android_summary:I = 0x7

.field public static final Preference_android_title:I = 0x4

.field public static final Preference_android_widgetLayout:I = 0x9

.field public static final Preference_defaultValue:I = 0x12

.field public static final Preference_dependency:I = 0x13

.field public static final Preference_enabled:I = 0x14

.field public static final Preference_fragment:I = 0x15

.field public static final Preference_icon:I = 0x16

.field public static final Preference_iconSpaceReserved:I = 0x17

.field public static final Preference_key:I = 0x18

.field public static final Preference_layout:I = 0x19

.field public static final Preference_order:I = 0x1a

.field public static final Preference_persistent:I = 0x1b

.field public static final Preference_selectable:I = 0x1c

.field public static final Preference_shouldDisableView:I = 0x1d

.field public static final Preference_singleLineTitle:I = 0x1e

.field public static final Preference_summary:I = 0x1f

.field public static final Preference_title:I = 0x20

.field public static final Preference_widgetLayout:I = 0x21

.field public static final RecycleListView:[I

.field public static final RecycleListView_paddingBottomNoButtons:I = 0x0

.field public static final RecycleListView_paddingTopNoTitle:I = 0x1

.field public static final RecyclerView:[I

.field public static final RecyclerView_android_descendantFocusability:I = 0x1

.field public static final RecyclerView_android_orientation:I = 0x0

.field public static final RecyclerView_fastScrollEnabled:I = 0x2

.field public static final RecyclerView_fastScrollHorizontalThumbDrawable:I = 0x3

.field public static final RecyclerView_fastScrollHorizontalTrackDrawable:I = 0x4

.field public static final RecyclerView_fastScrollVerticalThumbDrawable:I = 0x5

.field public static final RecyclerView_fastScrollVerticalTrackDrawable:I = 0x6

.field public static final RecyclerView_layoutManager:I = 0x7

.field public static final RecyclerView_reverseLayout:I = 0x8

.field public static final RecyclerView_spanCount:I = 0x9

.field public static final RecyclerView_stackFromEnd:I = 0xa

.field public static final ScrimInsetsFrameLayout:[I

.field public static final ScrimInsetsFrameLayout_insetForeground:I = 0x0

.field public static final ScrollingViewBehavior_Layout:[I

.field public static final ScrollingViewBehavior_Layout_behavior_overlapTop:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_android_focusable:I = 0x0

.field public static final SearchView_android_imeOptions:I = 0x3

.field public static final SearchView_android_inputType:I = 0x2

.field public static final SearchView_android_maxWidth:I = 0x1

.field public static final SearchView_closeIcon:I = 0x4

.field public static final SearchView_commitIcon:I = 0x5

.field public static final SearchView_defaultQueryHint:I = 0x6

.field public static final SearchView_goIcon:I = 0x7

.field public static final SearchView_iconifiedByDefault:I = 0x8

.field public static final SearchView_layout:I = 0x9

.field public static final SearchView_queryBackground:I = 0xa

.field public static final SearchView_queryHint:I = 0xb

.field public static final SearchView_searchHintIcon:I = 0xc

.field public static final SearchView_searchIcon:I = 0xd

.field public static final SearchView_submitBackground:I = 0xe

.field public static final SearchView_suggestionRowLayout:I = 0xf

.field public static final SearchView_voiceIcon:I = 0x10

.field public static final SeekBarPreference:[I

.field public static final SeekBarPreference_adjustable:I = 0x2

.field public static final SeekBarPreference_android_layout:I = 0x0

.field public static final SeekBarPreference_android_max:I = 0x1

.field public static final SeekBarPreference_min:I = 0x3

.field public static final SeekBarPreference_seekBarIncrement:I = 0x4

.field public static final SeekBarPreference_showSeekBarValue:I = 0x5

.field public static final SnackbarLayout:[I

.field public static final SnackbarLayout_android_maxWidth:I = 0x0

.field public static final SnackbarLayout_elevation:I = 0x1

.field public static final SnackbarLayout_maxActionInlineWidth:I = 0x2

.field public static final Spinner:[I

.field public static final Spinner_android_dropDownWidth:I = 0x3

.field public static final Spinner_android_entries:I = 0x0

.field public static final Spinner_android_popupBackground:I = 0x1

.field public static final Spinner_android_prompt:I = 0x2

.field public static final Spinner_popupTheme:I = 0x4

.field public static final SwitchCompat:[I

.field public static final SwitchCompat_android_textOff:I = 0x1

.field public static final SwitchCompat_android_textOn:I = 0x0

.field public static final SwitchCompat_android_thumb:I = 0x2

.field public static final SwitchCompat_showText:I = 0x3

.field public static final SwitchCompat_splitTrack:I = 0x4

.field public static final SwitchCompat_switchMinWidth:I = 0x5

.field public static final SwitchCompat_switchPadding:I = 0x6

.field public static final SwitchCompat_switchTextAppearance:I = 0x7

.field public static final SwitchCompat_thumbTextPadding:I = 0x8

.field public static final SwitchCompat_thumbTint:I = 0x9

.field public static final SwitchCompat_thumbTintMode:I = 0xa

.field public static final SwitchCompat_track:I = 0xb

.field public static final SwitchCompat_trackTint:I = 0xc

.field public static final SwitchCompat_trackTintMode:I = 0xd

.field public static final SwitchPreference:[I

.field public static final SwitchPreferenceCompat:[I

.field public static final SwitchPreferenceCompat_android_disableDependentsState:I = 0x2

.field public static final SwitchPreferenceCompat_android_summaryOff:I = 0x1

.field public static final SwitchPreferenceCompat_android_summaryOn:I = 0x0

.field public static final SwitchPreferenceCompat_android_switchTextOff:I = 0x4

.field public static final SwitchPreferenceCompat_android_switchTextOn:I = 0x3

.field public static final SwitchPreferenceCompat_disableDependentsState:I = 0x5

.field public static final SwitchPreferenceCompat_summaryOff:I = 0x6

.field public static final SwitchPreferenceCompat_summaryOn:I = 0x7

.field public static final SwitchPreferenceCompat_switchTextOff:I = 0x8

.field public static final SwitchPreferenceCompat_switchTextOn:I = 0x9

.field public static final SwitchPreference_android_disableDependentsState:I = 0x2

.field public static final SwitchPreference_android_summaryOff:I = 0x1

.field public static final SwitchPreference_android_summaryOn:I = 0x0

.field public static final SwitchPreference_android_switchTextOff:I = 0x4

.field public static final SwitchPreference_android_switchTextOn:I = 0x3

.field public static final SwitchPreference_disableDependentsState:I = 0x5

.field public static final SwitchPreference_summaryOff:I = 0x6

.field public static final SwitchPreference_summaryOn:I = 0x7

.field public static final SwitchPreference_switchTextOff:I = 0x8

.field public static final SwitchPreference_switchTextOn:I = 0x9

.field public static final TabItem:[I

.field public static final TabItem_android_icon:I = 0x0

.field public static final TabItem_android_layout:I = 0x1

.field public static final TabItem_android_text:I = 0x2

.field public static final TabLayout:[I

.field public static final TabLayout_tabBackground:I = 0x0

.field public static final TabLayout_tabContentStart:I = 0x1

.field public static final TabLayout_tabGravity:I = 0x2

.field public static final TabLayout_tabIndicatorColor:I = 0x3

.field public static final TabLayout_tabIndicatorHeight:I = 0x4

.field public static final TabLayout_tabMaxWidth:I = 0x5

.field public static final TabLayout_tabMinWidth:I = 0x6

.field public static final TabLayout_tabMode:I = 0x7

.field public static final TabLayout_tabPadding:I = 0x8

.field public static final TabLayout_tabPaddingBottom:I = 0x9

.field public static final TabLayout_tabPaddingEnd:I = 0xa

.field public static final TabLayout_tabPaddingStart:I = 0xb

.field public static final TabLayout_tabPaddingTop:I = 0xc

.field public static final TabLayout_tabSelectedTextColor:I = 0xd

.field public static final TabLayout_tabTextAppearance:I = 0xe

.field public static final TabLayout_tabTextColor:I = 0xf

.field public static final TextAppearance:[I

.field public static final TextAppearance_android_fontFamily:I = 0xa

.field public static final TextAppearance_android_shadowColor:I = 0x6

.field public static final TextAppearance_android_shadowDx:I = 0x7

.field public static final TextAppearance_android_shadowDy:I = 0x8

.field public static final TextAppearance_android_shadowRadius:I = 0x9

.field public static final TextAppearance_android_textColor:I = 0x3

.field public static final TextAppearance_android_textColorHint:I = 0x4

.field public static final TextAppearance_android_textColorLink:I = 0x5

.field public static final TextAppearance_android_textSize:I = 0x0

.field public static final TextAppearance_android_textStyle:I = 0x2

.field public static final TextAppearance_android_typeface:I = 0x1

.field public static final TextAppearance_fontFamily:I = 0xb

.field public static final TextAppearance_textAllCaps:I = 0xc

.field public static final TextInputLayout:[I

.field public static final TextInputLayout_android_hint:I = 0x1

.field public static final TextInputLayout_android_textColorHint:I = 0x0

.field public static final TextInputLayout_counterEnabled:I = 0x2

.field public static final TextInputLayout_counterMaxLength:I = 0x3

.field public static final TextInputLayout_counterOverflowTextAppearance:I = 0x4

.field public static final TextInputLayout_counterTextAppearance:I = 0x5

.field public static final TextInputLayout_errorEnabled:I = 0x6

.field public static final TextInputLayout_errorTextAppearance:I = 0x7

.field public static final TextInputLayout_hintAnimationEnabled:I = 0x8

.field public static final TextInputLayout_hintEnabled:I = 0x9

.field public static final TextInputLayout_hintTextAppearance:I = 0xa

.field public static final TextInputLayout_passwordToggleContentDescription:I = 0xb

.field public static final TextInputLayout_passwordToggleDrawable:I = 0xc

.field public static final TextInputLayout_passwordToggleEnabled:I = 0xd

.field public static final TextInputLayout_passwordToggleTint:I = 0xe

.field public static final TextInputLayout_passwordToggleTintMode:I = 0xf

.field public static final Toolbar:[I

.field public static final Toolbar_android_gravity:I = 0x0

.field public static final Toolbar_android_minHeight:I = 0x1

.field public static final Toolbar_buttonGravity:I = 0x2

.field public static final Toolbar_collapseContentDescription:I = 0x3

.field public static final Toolbar_collapseIcon:I = 0x4

.field public static final Toolbar_contentInsetEnd:I = 0x5

.field public static final Toolbar_contentInsetEndWithActions:I = 0x6

.field public static final Toolbar_contentInsetLeft:I = 0x7

.field public static final Toolbar_contentInsetRight:I = 0x8

.field public static final Toolbar_contentInsetStart:I = 0x9

.field public static final Toolbar_contentInsetStartWithNavigation:I = 0xa

.field public static final Toolbar_logo:I = 0xb

.field public static final Toolbar_logoDescription:I = 0xc

.field public static final Toolbar_maxButtonHeight:I = 0xd

.field public static final Toolbar_navigationContentDescription:I = 0xe

.field public static final Toolbar_navigationIcon:I = 0xf

.field public static final Toolbar_popupTheme:I = 0x10

.field public static final Toolbar_subtitle:I = 0x11

.field public static final Toolbar_subtitleTextAppearance:I = 0x12

.field public static final Toolbar_subtitleTextColor:I = 0x13

.field public static final Toolbar_title:I = 0x14

.field public static final Toolbar_titleMargin:I = 0x15

.field public static final Toolbar_titleMarginBottom:I = 0x16

.field public static final Toolbar_titleMarginEnd:I = 0x17

.field public static final Toolbar_titleMarginStart:I = 0x18

.field public static final Toolbar_titleMarginTop:I = 0x19

.field public static final Toolbar_titleMargins:I = 0x1a

.field public static final Toolbar_titleTextAppearance:I = 0x1b

.field public static final Toolbar_titleTextColor:I = 0x1c

.field public static final View:[I

.field public static final ViewBackgroundHelper:[I

.field public static final ViewBackgroundHelper_android_background:I = 0x0

.field public static final ViewBackgroundHelper_backgroundTint:I = 0x1

.field public static final ViewBackgroundHelper_backgroundTintMode:I = 0x2

.field public static final ViewStubCompat:[I

.field public static final ViewStubCompat_android_id:I = 0x0

.field public static final ViewStubCompat_android_inflatedId:I = 0x2

.field public static final ViewStubCompat_android_layout:I = 0x1

.field public static final View_android_focusable:I = 0x1

.field public static final View_android_theme:I = 0x0

.field public static final View_paddingEnd:I = 0x2

.field public static final View_paddingStart:I = 0x3

.field public static final View_theme:I = 0x4


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x3

    .line 6390
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActionBar:[I

    .line 6411
    new-array v0, v6, [I

    const v1, 0x10100b3

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActionBarLayout:[I

    .line 6828
    new-array v0, v6, [I

    const v1, 0x101013f

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActionMenuItemView:[I

    .line 6844
    new-array v0, v4, [I

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActionMenuView:[I

    .line 6867
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActionMode:[I

    .line 6960
    new-array v0, v3, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ActivityChooserView:[I

    .line 7000
    new-array v0, v2, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AdsAttrs:[I

    .line 7056
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AlertDialog:[I

    .line 7161
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppBarLayout:[I

    .line 7178
    new-array v0, v3, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppBarLayoutStates:[I

    .line 7214
    new-array v0, v3, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppBarLayout_Layout:[I

    .line 7324
    new-array v0, v5, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppCompatImageView:[I

    .line 7402
    new-array v0, v5, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppCompatSeekBar:[I

    .line 7484
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppCompatTextHelper:[I

    .line 7615
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppCompatTextView:[I

    .line 7970
    const/16 v0, 0x77

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->AppCompatTheme:[I

    .line 9487
    new-array v0, v3, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->BackgroundStyle:[I

    .line 9533
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->BottomNavigationView:[I

    .line 9611
    new-array v0, v2, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->BottomSheetBehavior_Layout:[I

    .line 9666
    new-array v0, v6, [I

    const v1, 0x7f04002e

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ButtonBarLayout:[I

    .line 9714
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CardView:[I

    .line 9902
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CheckBoxPreference:[I

    .line 9983
    new-array v0, v5, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CircularProgressBar:[I

    .line 10071
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CollapsingToolbarLayout:[I

    .line 10090
    new-array v0, v3, [I

    fill-array-data v0, :array_14

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CollapsingToolbarLayout_Layout:[I

    .line 10350
    new-array v0, v2, [I

    fill-array-data v0, :array_15

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ColorStateListItem:[I

    .line 10397
    new-array v0, v2, [I

    fill-array-data v0, :array_16

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CompoundButton:[I

    .line 10572
    const/16 v0, 0x3c

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ConstraintLayout_Layout:[I

    .line 11473
    new-array v0, v3, [I

    fill-array-data v0, :array_18

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ConstraintLayout_placeholder:[I

    .line 11658
    const/16 v0, 0x48

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ConstraintSet:[I

    .line 12712
    new-array v0, v3, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CoordinatorLayout:[I

    .line 12738
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->CoordinatorLayout_Layout:[I

    .line 12915
    new-array v0, v2, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->DesignTheme:[I

    .line 12990
    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->DialogPreference:[I

    .line 13148
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->DrawerArrowToggle:[I

    .line 13276
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->FloatingActionButton:[I

    .line 13291
    new-array v0, v6, [I

    const v1, 0x7f040042

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->FloatingActionButton_Behavior_Layout:[I

    .line 13434
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->FontFamily:[I

    .line 13453
    new-array v0, v2, [I

    fill-array-data v0, :array_21

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->FontFamilyFont:[I

    .line 13585
    new-array v0, v2, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ForegroundLinearLayout:[I

    .line 13663
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_23

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->GreenDotTextView:[I

    .line 13771
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_24

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->GridLayout:[I

    .line 13812
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_25

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->GridLayout_Layout:[I

    .line 14117
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_26

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->HorizontalPicker:[I

    .line 14231
    new-array v0, v6, [I

    const v1, 0x10100c4

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->LinearConstraintLayout:[I

    .line 14278
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_27

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->LinearLayoutCompat:[I

    .line 14300
    new-array v0, v5, [I

    fill-array-data v0, :array_28

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->LinearLayoutCompat_Layout:[I

    .line 14528
    new-array v0, v3, [I

    fill-array-data v0, :array_29

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ListPopupWindow:[I

    .line 14574
    new-array v0, v5, [I

    fill-array-data v0, :array_2a

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ListPreference:[I

    .line 14646
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2b

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->MenuGroup:[I

    .line 14783
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_2c

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->MenuItem:[I

    .line 15106
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_2d

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->MenuView:[I

    .line 15237
    new-array v0, v5, [I

    fill-array-data v0, :array_2e

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->MultiSelectListPreference:[I

    .line 15317
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2f

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->NavigationView:[I

    .line 15480
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PageIndicatorView:[I

    .line 15672
    new-array v0, v2, [I

    fill-array-data v0, :array_31

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PopupWindow:[I

    .line 15686
    new-array v0, v6, [I

    const v1, 0x7f0401a5

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PopupWindowBackgroundState:[I

    .line 15813
    const/16 v0, 0x22

    new-array v0, v0, [I

    fill-array-data v0, :array_32

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->Preference:[I

    .line 15841
    new-array v0, v5, [I

    fill-array-data v0, :array_33

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PreferenceFragment:[I

    .line 15861
    new-array v0, v5, [I

    fill-array-data v0, :array_34

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PreferenceFragmentCompat:[I

    .line 15977
    new-array v0, v3, [I

    fill-array-data v0, :array_35

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PreferenceGroup:[I

    .line 16017
    new-array v0, v5, [I

    fill-array-data v0, :array_36

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PreferenceImageView:[I

    .line 16127
    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_37

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->PreferenceTheme:[I

    .line 16791
    new-array v0, v3, [I

    fill-array-data v0, :array_38

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->RecycleListView:[I

    .line 16851
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_39

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->RecyclerView:[I

    .line 16999
    new-array v0, v6, [I

    const v1, 0x7f0400de

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ScrimInsetsFrameLayout:[I

    .line 17028
    new-array v0, v6, [I

    const v1, 0x7f040044

    aput v1, v0, v4

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ScrollingViewBehavior_Layout:[I

    .line 17087
    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_3a

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SearchView:[I

    .line 17375
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3b

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SeekBarPreference:[I

    .line 17453
    new-array v0, v2, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SnackbarLayout:[I

    .line 17514
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_3d

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->Spinner:[I

    .line 17629
    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_3e

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SwitchCompat:[I

    .line 17843
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_3f

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SwitchPreference:[I

    .line 17877
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_40

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->SwitchPreferenceCompat:[I

    .line 18097
    new-array v0, v2, [I

    fill-array-data v0, :array_41

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->TabItem:[I

    .line 18175
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_42

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->TabLayout:[I

    .line 18424
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_43

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->TextAppearance:[I

    .line 18638
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_44

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->TextInputLayout:[I

    .line 18896
    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_45

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->Toolbar:[I

    .line 19297
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_46

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->View:[I

    .line 19316
    new-array v0, v2, [I

    fill-array-data v0, :array_47

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ViewBackgroundHelper:[I

    .line 19380
    new-array v0, v2, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/safonov/speedreading/R$styleable;->ViewStubCompat:[I

    return-void

    .line 6390
    :array_0
    .array-data 4
        0x7f04003a
        0x7f04003b
        0x7f04003c
        0x7f040078
        0x7f040079
        0x7f04007a
        0x7f04007b
        0x7f04007c
        0x7f04007d
        0x7f040089
        0x7f040095
        0x7f040096
        0x7f0400a4
        0x7f0400ce
        0x7f0400cf
        0x7f0400d3
        0x7f0400d4
        0x7f0400d6
        0x7f0400dc
        0x7f0400e2
        0x7f040138
        0x7f040145
        0x7f040168
        0x7f04017c
        0x7f04017d
        0x7f0401ac
        0x7f0401af
        0x7f0401e2
        0x7f0401ec
    .end array-data

    .line 6867
    :array_1
    .array-data 4
        0x7f04003a
        0x7f04003b
        0x7f040061
        0x7f0400ce
        0x7f0401af
        0x7f0401ec
    .end array-data

    .line 6960
    :array_2
    .array-data 4
        0x7f0400ab
        0x7f0400dd
    .end array-data

    .line 7000
    :array_3
    .array-data 4
        0x7f040022
        0x7f040023
        0x7f040024
    .end array-data

    .line 7056
    :array_4
    .array-data 4
        0x10100f2
        0x7f040051
        0x7f04012f
        0x7f040130
        0x7f040142
        0x7f04019a
        0x7f04019c
    .end array-data

    .line 7161
    :array_5
    .array-data 4
        0x10100d4
        0x101048f
        0x1010540
        0x7f0400a4
        0x7f0400ac
    .end array-data

    .line 7178
    :array_6
    .array-data 4
        0x7f0401a6
        0x7f0401a7
    .end array-data

    .line 7214
    :array_7
    .array-data 4
        0x7f04012a
        0x7f04012b
    .end array-data

    .line 7324
    :array_8
    .array-data 4
        0x1010119
        0x7f0401a3
        0x7f0401e0
        0x7f0401e1
    .end array-data

    .line 7402
    :array_9
    .array-data 4
        0x1010142
        0x7f0401dd
        0x7f0401de
        0x7f0401df
    .end array-data

    .line 7484
    :array_a
    .array-data 4
        0x1010034
        0x101016d
        0x101016e
        0x101016f
        0x1010170
        0x1010392
        0x1010393
    .end array-data

    .line 7615
    :array_b
    .array-data 4
        0x1010034
        0x7f040034
        0x7f040035
        0x7f040036
        0x7f040037
        0x7f040038
        0x7f0400bb
        0x7f0401cc
    .end array-data

    .line 7970
    :array_c
    .array-data 4
        0x1010057
        0x10100ae
        0x7f040000
        0x7f040001
        0x7f040002
        0x7f040003
        0x7f040004
        0x7f040005
        0x7f040006
        0x7f040007
        0x7f040008
        0x7f040009
        0x7f04000a
        0x7f04000b
        0x7f04000c
        0x7f04000e
        0x7f04000f
        0x7f040010
        0x7f040011
        0x7f040012
        0x7f040013
        0x7f040014
        0x7f040015
        0x7f040016
        0x7f040017
        0x7f040018
        0x7f040019
        0x7f04001a
        0x7f04001b
        0x7f04001c
        0x7f04001d
        0x7f04001e
        0x7f040021
        0x7f040026
        0x7f040027
        0x7f040028
        0x7f040029
        0x7f040033
        0x7f040048
        0x7f04004b
        0x7f04004c
        0x7f04004d
        0x7f04004e
        0x7f04004f
        0x7f040052
        0x7f040053
        0x7f04005e
        0x7f04005f
        0x7f040067
        0x7f040068
        0x7f040069
        0x7f04006a
        0x7f04006b
        0x7f04006c
        0x7f04006d
        0x7f04006e
        0x7f04006f
        0x7f040070
        0x7f040084
        0x7f040091
        0x7f040092
        0x7f040097
        0x7f04009a
        0x7f04009d
        0x7f04009e
        0x7f0400a0
        0x7f0400a1
        0x7f0400a3
        0x7f0400d3
        0x7f0400db
        0x7f04012d
        0x7f04012e
        0x7f040131
        0x7f040132
        0x7f040133
        0x7f040134
        0x7f040135
        0x7f040136
        0x7f040137
        0x7f040150
        0x7f040151
        0x7f040152
        0x7f040167
        0x7f040169
        0x7f040180
        0x7f040181
        0x7f040182
        0x7f040183
        0x7f04018d
        0x7f040190
        0x7f040192
        0x7f040193
        0x7f0401a0
        0x7f0401a1
        0x7f0401b8
        0x7f0401cd
        0x7f0401ce
        0x7f0401cf
        0x7f0401d0
        0x7f0401d1
        0x7f0401d2
        0x7f0401d3
        0x7f0401d4
        0x7f0401d5
        0x7f0401d7
        0x7f0401ee
        0x7f0401ef
        0x7f0401f0
        0x7f0401f1
        0x7f0401fb
        0x7f0401fc
        0x7f0401fd
        0x7f0401fe
        0x7f0401ff
        0x7f040200
        0x7f040201
        0x7f040202
        0x7f040203
        0x7f040204
    .end array-data

    .line 9487
    :array_d
    .array-data 4
        0x101030e
        0x7f040192
    .end array-data

    .line 9533
    :array_e
    .array-data 4
        0x7f0400a4
        0x7f0400e0
        0x7f0400e1
        0x7f0400e4
        0x7f040140
    .end array-data

    .line 9611
    :array_f
    .array-data 4
        0x7f040043
        0x7f040045
        0x7f040046
    .end array-data

    .line 9714
    :array_10
    .array-data 4
        0x101013f
        0x1010140
        0x7f040056
        0x7f040057
        0x7f040058
        0x7f040059
        0x7f04005a
        0x7f04005b
        0x7f04007e
        0x7f04007f
        0x7f040080
        0x7f040081
        0x7f040082
    .end array-data

    .line 9902
    :array_11
    .array-data 4
        0x10101ef
        0x10101f0
        0x10101f1
        0x7f040094
        0x7f0401b2
        0x7f0401b3
    .end array-data

    .line 9983
    :array_12
    .array-data 4
        0x7f040039
        0x7f0400c7
        0x7f04012c
        0x7f04017b
    .end array-data

    .line 10071
    :array_13
    .array-data 4
        0x7f040064
        0x7f040065
        0x7f040083
        0x7f0400ad
        0x7f0400ae
        0x7f0400af
        0x7f0400b0
        0x7f0400b1
        0x7f0400b2
        0x7f0400b3
        0x7f040189
        0x7f04018a
        0x7f0401a9
        0x7f0401e2
        0x7f0401e3
        0x7f0401ed
    .end array-data

    .line 10090
    :array_14
    .array-data 4
        0x7f0400ec
        0x7f0400ed
    .end array-data

    .line 10350
    :array_15
    .array-data 4
        0x10101a5
        0x101031f
        0x7f04002f
    .end array-data

    .line 10397
    :array_16
    .array-data 4
        0x1010107
        0x7f040054
        0x7f040055
    .end array-data

    .line 10572
    :array_17
    .array-data 4
        0x10100c4
        0x101011f
        0x1010120
        0x101013f
        0x1010140
        0x7f040040
        0x7f040041
        0x7f04005c
        0x7f040074
        0x7f040075
        0x7f0400f1
        0x7f0400f2
        0x7f0400f3
        0x7f0400f4
        0x7f0400f5
        0x7f0400f6
        0x7f0400f7
        0x7f0400f8
        0x7f0400f9
        0x7f0400fa
        0x7f0400fb
        0x7f0400fc
        0x7f0400fd
        0x7f0400fe
        0x7f0400ff
        0x7f040100
        0x7f040101
        0x7f040102
        0x7f040103
        0x7f040104
        0x7f040105
        0x7f040106
        0x7f040107
        0x7f040108
        0x7f040109
        0x7f04010a
        0x7f04010b
        0x7f04010c
        0x7f04010d
        0x7f04010e
        0x7f04010f
        0x7f040110
        0x7f040111
        0x7f040112
        0x7f040113
        0x7f040114
        0x7f040115
        0x7f040116
        0x7f040117
        0x7f040118
        0x7f040119
        0x7f04011b
        0x7f04011c
        0x7f04011d
        0x7f04011e
        0x7f04011f
        0x7f040120
        0x7f040121
        0x7f040122
        0x7f040126
    .end array-data

    .line 11473
    :array_18
    .array-data 4
        0x7f040076
        0x7f0400a5
    .end array-data

    .line 11658
    :array_19
    .array-data 4
        0x10100c4
        0x10100d0
        0x10100dc
        0x10100f4
        0x10100f5
        0x10100f7
        0x10100f8
        0x10100f9
        0x10100fa
        0x101031f
        0x1010320
        0x1010321
        0x1010322
        0x1010323
        0x1010324
        0x1010325
        0x1010326
        0x1010327
        0x1010328
        0x10103b5
        0x10103b6
        0x10103fa
        0x1010440
        0x7f0400f1
        0x7f0400f2
        0x7f0400f3
        0x7f0400f4
        0x7f0400f5
        0x7f0400f6
        0x7f0400f7
        0x7f0400f8
        0x7f0400f9
        0x7f0400fa
        0x7f0400fb
        0x7f0400fc
        0x7f0400fd
        0x7f0400fe
        0x7f0400ff
        0x7f040100
        0x7f040101
        0x7f040102
        0x7f040103
        0x7f040104
        0x7f040105
        0x7f040106
        0x7f040107
        0x7f040108
        0x7f040109
        0x7f04010a
        0x7f04010b
        0x7f04010c
        0x7f04010d
        0x7f04010e
        0x7f04010f
        0x7f040110
        0x7f040111
        0x7f040112
        0x7f040113
        0x7f040114
        0x7f040115
        0x7f040116
        0x7f040117
        0x7f040118
        0x7f040119
        0x7f04011b
        0x7f04011c
        0x7f04011d
        0x7f04011e
        0x7f04011f
        0x7f040120
        0x7f040121
        0x7f040122
    .end array-data

    .line 12712
    :array_1a
    .array-data 4
        0x7f0400e6
        0x7f0401a8
    .end array-data

    .line 12738
    :array_1b
    .array-data 4
        0x10100b3
        0x7f0400e9
        0x7f0400ea
        0x7f0400eb
        0x7f04011a
        0x7f040124
        0x7f040125
    .end array-data

    .line 12915
    :array_1c
    .array-data 4
        0x7f040049
        0x7f04004a
        0x7f0401d6
    .end array-data

    .line 12990
    :array_1d
    .array-data 4
        0x10101f2
        0x10101f3
        0x10101f4
        0x10101f5
        0x10101f6
        0x10101f7
        0x7f04008d
        0x7f04008e
        0x7f04008f
        0x7f040093
        0x7f040146
        0x7f04016a
    .end array-data

    .line 13148
    :array_1e
    .array-data 4
        0x7f040031
        0x7f040032
        0x7f04003f
        0x7f040066
        0x7f04009b
        0x7f0400c8
        0x7f04019f
        0x7f0401d9
    .end array-data

    .line 13276
    :array_1f
    .array-data 4
        0x7f04003d
        0x7f04003e
        0x7f040047
        0x7f0400a4
        0x7f0400b4
        0x7f04017a
        0x7f040186
        0x7f0401f6
    .end array-data

    .line 13434
    :array_20
    .array-data 4
        0x7f0400bc
        0x7f0400bd
        0x7f0400be
        0x7f0400bf
        0x7f0400c0
        0x7f0400c1
    .end array-data

    .line 13453
    :array_21
    .array-data 4
        0x7f0400ba
        0x7f0400c2
        0x7f0400c3
    .end array-data

    .line 13585
    :array_22
    .array-data 4
        0x1010109
        0x1010200
        0x7f0400c4
    .end array-data

    .line 13663
    :array_23
    .array-data 4
        0x7f0400c6
        0x7f0400ca
        0x7f0400cb
        0x7f0400cc
        0x7f04013a
        0x7f04017b
        0x7f040194
    .end array-data

    .line 13771
    :array_24
    .array-data 4
        0x7f04002a
        0x7f040071
        0x7f040072
        0x7f04014a
        0x7f040187
        0x7f040188
        0x7f0401f7
    .end array-data

    .line 13812
    :array_25
    .array-data 4
        0x10100f4
        0x10100f5
        0x10100f6
        0x10100f7
        0x10100f8
        0x10100f9
        0x10100fa
        0x7f0400ee
        0x7f0400ef
        0x7f0400f0
        0x7f040123
        0x7f040127
        0x7f040128
        0x7f040129
    .end array-data

    .line 14117
    :array_26
    .array-data 4
        0x1010095
        0x1010098
        0x10100ab
        0x101021d
        0x7f040099
        0x7f04019b
        0x7f0401f8
    .end array-data

    .line 14278
    :array_27
    .array-data 4
        0x10100af
        0x10100c4
        0x1010126
        0x1010127
        0x1010128
        0x7f040096
        0x7f040098
        0x7f04013f
        0x7f040197
    .end array-data

    .line 14300
    :array_28
    .array-data 4
        0x10100b3
        0x10100f4
        0x10100f5
        0x1010181
    .end array-data

    .line 14528
    :array_29
    .array-data 4
        0x10102ac
        0x10102ad
    .end array-data

    .line 14574
    :array_2a
    .array-data 4
        0x10100b2
        0x10101f8
        0x7f0400a7
        0x7f0400a8
    .end array-data

    .line 14646
    :array_2b
    .array-data 4
        0x101000e
        0x10100d0
        0x1010194
        0x10101de
        0x10101df
        0x10101e0
    .end array-data

    .line 14783
    :array_2c
    .array-data 4
        0x1010002
        0x101000e
        0x10100d0
        0x1010106
        0x1010194
        0x10101de
        0x10101df
        0x10101e1
        0x10101e2
        0x10101e3
        0x10101e4
        0x10101e5
        0x101026f
        0x7f04000d
        0x7f04001f
        0x7f040020
        0x7f040030
        0x7f040077
        0x7f0400d8
        0x7f0400d9
        0x7f040147
        0x7f040196
        0x7f0401f2
    .end array-data

    .line 15106
    :array_2d
    .array-data 4
        0x10100ae
        0x101012c
        0x101012d
        0x101012e
        0x101012f
        0x1010130
        0x1010131
        0x7f040179
        0x7f0401aa
    .end array-data

    .line 15237
    :array_2e
    .array-data 4
        0x10100b2
        0x10101f8
        0x7f0400a7
        0x7f0400a8
    .end array-data

    .line 15317
    :array_2f
    .array-data 4
        0x10100d4
        0x10100dd
        0x101011f
        0x7f0400a4
        0x7f0400cd
        0x7f0400e0
        0x7f0400e1
        0x7f0400e3
        0x7f0400e4
        0x7f040140
    .end array-data

    .line 15480
    :array_30
    .array-data 4
        0x7f040159
        0x7f04015a
        0x7f04015b
        0x7f04015c
        0x7f04015d
        0x7f04015e
        0x7f04015f
        0x7f040160
        0x7f040161
        0x7f040162
        0x7f040163
        0x7f040164
        0x7f040165
        0x7f040166
    .end array-data

    .line 15672
    :array_31
    .array-data 4
        0x1010176
        0x10102c9
        0x7f04014b
    .end array-data

    .line 15813
    :array_32
    .array-data 4
        0x1010002
        0x101000d
        0x101000e
        0x10100f2
        0x10101e1
        0x10101e6
        0x10101e8
        0x10101e9
        0x10101ea
        0x10101eb
        0x10101ec
        0x10101ed
        0x10101ee
        0x10102e3
        0x101055c
        0x1010561
        0x7f04002b
        0x7f04002d
        0x7f04008b
        0x7f04008c
        0x7f0400a6
        0x7f0400c5
        0x7f0400d6
        0x7f0400d7
        0x7f0400e5
        0x7f0400e7
        0x7f040148
        0x7f040158
        0x7f040191
        0x7f040195
        0x7f04019d
        0x7f0401b1
        0x7f0401e2
        0x7f0401fa
    .end array-data

    .line 15841
    :array_33
    .array-data 4
        0x10100f2
        0x1010129
        0x101012a
        0x7f04002c
    .end array-data

    .line 15861
    :array_34
    .array-data 4
        0x10100f2
        0x1010129
        0x101012a
        0x7f04002c
    .end array-data

    .line 15977
    :array_35
    .array-data 4
        0x10101e7
        0x7f040149
    .end array-data

    .line 16017
    :array_36
    .array-data 4
        0x101011f
        0x1010120
        0x7f04013d
        0x7f04013e
    .end array-data

    .line 16127
    :array_37
    .array-data 4
        0x7f04005d
        0x7f040090
        0x7f04009f
        0x7f0400a2
        0x7f04016b
        0x7f04016c
        0x7f04016d
        0x7f04016e
        0x7f04016f
        0x7f040170
        0x7f040171
        0x7f040172
        0x7f040173
        0x7f040174
        0x7f040175
        0x7f040176
        0x7f040177
        0x7f040178
        0x7f040185
        0x7f04018f
        0x7f0401b6
        0x7f0401b7
        0x7f040205
    .end array-data

    .line 16791
    :array_38
    .array-data 4
        0x7f04014c
        0x7f04014f
    .end array-data

    .line 16851
    :array_39
    .array-data 4
        0x10100c4
        0x10100f1
        0x7f0400b5
        0x7f0400b6
        0x7f0400b7
        0x7f0400b8
        0x7f0400b9
        0x7f0400e8
        0x7f040184
        0x7f04019e
        0x7f0401a4
    .end array-data

    .line 17087
    :array_3a
    .array-data 4
        0x10100da
        0x101011f
        0x1010220
        0x1010264
        0x7f040060
        0x7f040073
        0x7f04008a
        0x7f0400c9
        0x7f0400da
        0x7f0400e7
        0x7f04017e
        0x7f04017f
        0x7f04018b
        0x7f04018c
        0x7f0401ab
        0x7f0401b0
        0x7f0401f9
    .end array-data

    .line 17375
    :array_3b
    .array-data 4
        0x10100f2
        0x1010136
        0x7f040025
        0x7f040141
        0x7f04018e
        0x7f040198
    .end array-data

    .line 17453
    :array_3c
    .array-data 4
        0x101011f
        0x7f0400a4
        0x7f04013b
    .end array-data

    .line 17514
    :array_3d
    .array-data 4
        0x10100b2
        0x1010176
        0x101017b
        0x1010262
        0x7f040168
    .end array-data

    .line 17629
    :array_3e
    .array-data 4
        0x1010124
        0x1010125
        0x1010142
        0x7f040199
        0x7f0401a2
        0x7f0401b4
        0x7f0401b5
        0x7f0401b9
        0x7f0401da
        0x7f0401db
        0x7f0401dc
        0x7f0401f3
        0x7f0401f4
        0x7f0401f5
    .end array-data

    .line 17843
    :array_3f
    .array-data 4
        0x10101ef
        0x10101f0
        0x10101f1
        0x101036b
        0x101036c
        0x7f040094
        0x7f0401b2
        0x7f0401b3
        0x7f0401ba
        0x7f0401bb
    .end array-data

    .line 17877
    :array_40
    .array-data 4
        0x10101ef
        0x10101f0
        0x10101f1
        0x101036b
        0x101036c
        0x7f040094
        0x7f0401b2
        0x7f0401b3
        0x7f0401ba
        0x7f0401bb
    .end array-data

    .line 18097
    :array_41
    .array-data 4
        0x1010002
        0x10100f2
        0x101014f
    .end array-data

    .line 18175
    :array_42
    .array-data 4
        0x7f0401bc
        0x7f0401bd
        0x7f0401be
        0x7f0401bf
        0x7f0401c0
        0x7f0401c1
        0x7f0401c2
        0x7f0401c3
        0x7f0401c4
        0x7f0401c5
        0x7f0401c6
        0x7f0401c7
        0x7f0401c8
        0x7f0401c9
        0x7f0401ca
        0x7f0401cb
    .end array-data

    .line 18424
    :array_43
    .array-data 4
        0x1010095
        0x1010096
        0x1010097
        0x1010098
        0x101009a
        0x101009b
        0x1010161
        0x1010162
        0x1010163
        0x1010164
        0x10103ac
        0x7f0400bb
        0x7f0401cc
    .end array-data

    .line 18638
    :array_44
    .array-data 4
        0x101009a
        0x1010150
        0x7f040085
        0x7f040086
        0x7f040087
        0x7f040088
        0x7f0400a9
        0x7f0400aa
        0x7f0400d0
        0x7f0400d1
        0x7f0400d2
        0x7f040153
        0x7f040154
        0x7f040155
        0x7f040156
        0x7f040157
    .end array-data

    .line 18896
    :array_45
    .array-data 4
        0x10100af
        0x1010140
        0x7f040050
        0x7f040062
        0x7f040063
        0x7f040078
        0x7f040079
        0x7f04007a
        0x7f04007b
        0x7f04007c
        0x7f04007d
        0x7f040138
        0x7f040139
        0x7f04013c
        0x7f040143
        0x7f040144
        0x7f040168
        0x7f0401ac
        0x7f0401ad
        0x7f0401ae
        0x7f0401e2
        0x7f0401e4
        0x7f0401e5
        0x7f0401e6
        0x7f0401e7
        0x7f0401e8
        0x7f0401e9
        0x7f0401ea
        0x7f0401eb
    .end array-data

    .line 19297
    :array_46
    .array-data 4
        0x1010000
        0x10100da
        0x7f04014d
        0x7f04014e
        0x7f0401d8
    .end array-data

    .line 19316
    :array_47
    .array-data 4
        0x10100d4
        0x7f04003d
        0x7f04003e
    .end array-data

    .line 19380
    :array_48
    .array-data 4
        0x10100d0
        0x10100f2
        0x10100f3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
