.class public final Lcom/safonov/speedreading/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final ALT:I = 0x7f090000

.field public static final CTRL:I = 0x7f090001

.field public static final FUNCTION:I = 0x7f090002

.field public static final META:I = 0x7f090003

.field public static final SHIFT:I = 0x7f090004

.field public static final SYM:I = 0x7f090005

.field public static final accuracy_text_view:I = 0x7f090006

.field public static final accuracy_title_view:I = 0x7f090007

.field public static final action0:I = 0x7f090008

.field public static final action_bar:I = 0x7f090009

.field public static final action_bar_activity_content:I = 0x7f09000a

.field public static final action_bar_container:I = 0x7f09000b

.field public static final action_bar_premium_icon:I = 0x7f09000c

.field public static final action_bar_root:I = 0x7f09000d

.field public static final action_bar_spinner:I = 0x7f09000e

.field public static final action_bar_subtitle:I = 0x7f09000f

.field public static final action_bar_title:I = 0x7f090010

.field public static final action_container:I = 0x7f090011

.field public static final action_context_bar:I = 0x7f090012

.field public static final action_divider:I = 0x7f090013

.field public static final action_home:I = 0x7f090014

.field public static final action_image:I = 0x7f090015

.field public static final action_materials:I = 0x7f090016

.field public static final action_menu_divider:I = 0x7f090017

.field public static final action_menu_presenter:I = 0x7f090018

.field public static final action_mode_bar:I = 0x7f090019

.field public static final action_mode_bar_stub:I = 0x7f09001a

.field public static final action_mode_close_button:I = 0x7f09001b

.field public static final action_profile:I = 0x7f09001c

.field public static final action_reading:I = 0x7f09001d

.field public static final action_text:I = 0x7f09001e

.field public static final action_training:I = 0x7f09001f

.field public static final actions:I = 0x7f090020

.field public static final activity_chooser_view_content:I = 0x7f090021

.field public static final ad_view:I = 0x7f090022

.field public static final add:I = 0x7f090023

.field public static final alertTitle:I = 0x7f090024

.field public static final alignBounds:I = 0x7f090025

.field public static final alignMargins:I = 0x7f090026

.field public static final all:I = 0x7f090027

.field public static final always:I = 0x7f090028

.field public static final answerLineContainer:I = 0x7f090029

.field public static final answer_layout:I = 0x7f09002a

.field public static final appbar:I = 0x7f09002b

.field public static final async:I = 0x7f09002c

.field public static final author_text_view:I = 0x7f09002d

.field public static final author_view:I = 0x7f09002e

.field public static final auto:I = 0x7f09002f

.field public static final average_speed_text_view:I = 0x7f090030

.field public static final barrier:I = 0x7f090031

.field public static final beginning:I = 0x7f090032

.field public static final best_average_speed_text_view:I = 0x7f090033

.field public static final best_max_speed_text_view:I = 0x7f090034

.field public static final best_remember_words_score_title_view:I = 0x7f090035

.field public static final best_score_text_view:I = 0x7f090036

.field public static final best_score_title_view:I = 0x7f090037

.field public static final best_time_text_view:I = 0x7f090038

.field public static final best_time_title_view:I = 0x7f090039

.field public static final best_true_colors_score_title_view:I = 0x7f09003a

.field public static final blocking:I = 0x7f09003b

.field public static final book_cover_image_view:I = 0x7f09003c

.field public static final book_image_view:I = 0x7f09003d

.field public static final book_progress_bar:I = 0x7f09003e

.field public static final book_progress_text_view:I = 0x7f09003f

.field public static final book_read_button:I = 0x7f090040

.field public static final book_title_text_view:I = 0x7f090041

.field public static final bottom:I = 0x7f090042

.field public static final bottom_navigation:I = 0x7f090043

.field public static final buttonPanel:I = 0x7f090044

.field public static final cancel_action:I = 0x7f090045

.field public static final center:I = 0x7f090046

.field public static final center_horizontal:I = 0x7f090047

.field public static final center_vertical:I = 0x7f090048

.field public static final chains:I = 0x7f090049

.field public static final checkbox:I = 0x7f09004a

.field public static final chronometer:I = 0x7f09004b

.field public static final circlesSpeed:I = 0x7f09004c

.field public static final circles_container:I = 0x7f09004d

.field public static final circularProgressBar:I = 0x7f09004e

.field public static final cirlesCount:I = 0x7f09004f

.field public static final cirlesRadius:I = 0x7f090050

.field public static final clip_horizontal:I = 0x7f090051

.field public static final clip_vertical:I = 0x7f090052

.field public static final close_button:I = 0x7f090053

.field public static final close_dialog_view:I = 0x7f090054

.field public static final collapseActionView:I = 0x7f090055

.field public static final color:I = 0x7f090056

.field public static final concentration_best_score_text_view:I = 0x7f090057

.field public static final concentration_best_score_title_view:I = 0x7f090058

.field public static final concentration_complexity_button_1:I = 0x7f090059

.field public static final concentration_complexity_button_2:I = 0x7f09005a

.field public static final concentration_points_text_view:I = 0x7f09005b

.field public static final concentration_progress_bar:I = 0x7f09005c

.field public static final concentration_record_text_view:I = 0x7f09005d

.field public static final concentration_result_line_chart:I = 0x7f09005e

.field public static final concentration_score_text_view:I = 0x7f09005f

.field public static final concentration_score_title_view:I = 0x7f090060

.field public static final concentration_statistics_layout:I = 0x7f090061

.field public static final constraintLayout:I = 0x7f090062

.field public static final constraintLayout2:I = 0x7f090063

.field public static final container:I = 0x7f090064

.field public static final contentPanel:I = 0x7f090065

.field public static final content_holder_linear_layout:I = 0x7f090066

.field public static final content_text_view:I = 0x7f090067

.field public static final content_view:I = 0x7f090068

.field public static final continue_button:I = 0x7f090069

.field public static final control_view:I = 0x7f09006a

.field public static final coordinator:I = 0x7f09006b

.field public static final cup_timer_progress_bar:I = 0x7f09006c

.field public static final cup_timer_result_duration_text_view:I = 0x7f09006d

.field public static final cup_timer_result_duration_title_text_view:I = 0x7f09006e

.field public static final cup_timer_result_line_chart_view:I = 0x7f09006f

.field public static final custom:I = 0x7f090070

.field public static final customPanel:I = 0x7f090071

.field public static final decor_content_parent:I = 0x7f090072

.field public static final default_activity_button:I = 0x7f090073

.field public static final description_content_holder_scroll_view:I = 0x7f090074

.field public static final description_start_training_button:I = 0x7f090075

.field public static final description_view:I = 0x7f090076

.field public static final design_bottom_sheet:I = 0x7f090077

.field public static final design_menu_item_action_area:I = 0x7f090078

.field public static final design_menu_item_action_area_stub:I = 0x7f090079

.field public static final design_menu_item_text:I = 0x7f09007a

.field public static final design_navigation_view:I = 0x7f09007b

.field public static final dimensions:I = 0x7f09007c

.field public static final direct:I = 0x7f09007d

.field public static final disableHome:I = 0x7f09007e

.field public static final dont_show_again_check_box:I = 0x7f09007f

.field public static final drop:I = 0x7f090080

.field public static final duration_text_view:I = 0x7f090081

.field public static final duration_title_text_view:I = 0x7f090082

.field public static final edit_query:I = 0x7f090083

.field public static final empty_view:I = 0x7f090084

.field public static final end:I = 0x7f090085

.field public static final end_padder:I = 0x7f090086

.field public static final enterAlways:I = 0x7f090087

.field public static final enterAlwaysCollapsed:I = 0x7f090088

.field public static final even_numbers_grid_layout:I = 0x7f090089

.field public static final even_numbers_result_line_chart_view:I = 0x7f09008a

.field public static final exitUntilCollapsed:I = 0x7f09008b

.field public static final expand_activities_button:I = 0x7f09008c

.field public static final expanded_menu:I = 0x7f09008d

.field public static final file_cover_image_view:I = 0x7f09008e

.field public static final file_name_text_view:I = 0x7f09008f

.field public static final file_path_text_view:I = 0x7f090090

.field public static final file_type_text_view:I = 0x7f090091

.field public static final fill:I = 0x7f090092

.field public static final fill_horizontal:I = 0x7f090093

.field public static final fill_vertical:I = 0x7f090094

.field public static final fixed:I = 0x7f090095

.field public static final floating_button:I = 0x7f090096

.field public static final focus_attention_progress:I = 0x7f090097

.field public static final focus_attention_record:I = 0x7f090098

.field public static final focus_attention_score:I = 0x7f090099

.field public static final focus_attention_statistics:I = 0x7f09009a

.field public static final forever:I = 0x7f09009b

.field public static final format_list:I = 0x7f09009c

.field public static final found_mistakes_text_view:I = 0x7f09009d

.field public static final found_mistakes_title_view:I = 0x7f09009e

.field public static final fragment_container:I = 0x7f09009f

.field public static final ghost_view:I = 0x7f0900a0

.field public static final gone:I = 0x7f0900a1

.field public static final green_dot_result_duration_text_view:I = 0x7f0900a2

.field public static final green_dot_result_duration_title_text_view:I = 0x7f0900a3

.field public static final green_dot_result_line_chart_view:I = 0x7f0900a4

.field public static final green_dot_text_view:I = 0x7f0900a5

.field public static final grid_layout:I = 0x7f0900a6

.field public static final guideline1:I = 0x7f0900a7

.field public static final guideline2:I = 0x7f0900a8

.field public static final help:I = 0x7f0900a9

.field public static final help_content_holder_scroll_view:I = 0x7f0900aa

.field public static final home:I = 0x7f0900ab

.field public static final homeAsUp:I = 0x7f0900ac

.field public static final horizontal:I = 0x7f0900ad

.field public static final horizontalScrollView:I = 0x7f0900ae

.field public static final icon:I = 0x7f0900af

.field public static final icon_frame:I = 0x7f0900b0

.field public static final icon_group:I = 0x7f0900b1

.field public static final ifRoom:I = 0x7f0900b2

.field public static final image:I = 0x7f0900b3

.field public static final imageView:I = 0x7f0900b4

.field public static final imageView2:I = 0x7f0900b5

.field public static final imageView3:I = 0x7f0900b6

.field public static final imageView4:I = 0x7f0900b7

.field public static final info:I = 0x7f0900b8

.field public static final information_view:I = 0x7f0900b9

.field public static final interstitial_start_button:I = 0x7f0900ba

.field public static final interstitial_top_iv:I = 0x7f0900bb

.field public static final interstitial_top_title_tv:I = 0x7f0900bc

.field public static final invisible:I = 0x7f0900bd

.field public static final italic:I = 0x7f0900be

.field public static final item_10_text_view:I = 0x7f0900bf

.field public static final item_11_text_view:I = 0x7f0900c0

.field public static final item_12_text_view:I = 0x7f0900c1

.field public static final item_1_text_view:I = 0x7f0900c2

.field public static final item_2_text_view:I = 0x7f0900c3

.field public static final item_3_text_view:I = 0x7f0900c4

.field public static final item_4_text_view:I = 0x7f0900c5

.field public static final item_5_text_view:I = 0x7f0900c6

.field public static final item_6_text_view:I = 0x7f0900c7

.field public static final item_7_text_view:I = 0x7f0900c8

.field public static final item_8_text_view:I = 0x7f0900c9

.field public static final item_9_text_view:I = 0x7f0900ca

.field public static final item_favorite_image_view:I = 0x7f0900cb

.field public static final item_line_view:I = 0x7f0900cc

.field public static final item_message_text_view:I = 0x7f0900cd

.field public static final item_text_view:I = 0x7f0900ce

.field public static final item_title_text_view:I = 0x7f0900cf

.field public static final item_touch_helper_previous_elevation:I = 0x7f0900d0

.field public static final iv_answer:I = 0x7f0900d1

.field public static final iv_faces:I = 0x7f0900d2

.field public static final language_text_view:I = 0x7f0900d3

.field public static final language_view:I = 0x7f0900d4

.field public static final largeLabel:I = 0x7f0900d5

.field public static final left:I = 0x7f0900d6

.field public static final level_text_view:I = 0x7f0900d7

.field public static final library_speed_reading_book_view:I = 0x7f0900d8

.field public static final line1:I = 0x7f0900d9

.field public static final line3:I = 0x7f0900da

.field public static final line_chart:I = 0x7f0900db

.field public static final line_chart_view:I = 0x7f0900dc

.field public static final line_of_sight_check_button:I = 0x7f0900dd

.field public static final line_of_sight_grid_layout:I = 0x7f0900de

.field public static final line_of_sight_panel:I = 0x7f0900df

.field public static final line_of_sight_progress_bar:I = 0x7f0900e0

.field public static final linearLayout:I = 0x7f0900e1

.field public static final linearLayout2:I = 0x7f0900e2

.field public static final linearLayout3:I = 0x7f0900e3

.field public static final list:I = 0x7f0900e4

.field public static final listMode:I = 0x7f0900e5

.field public static final list_item:I = 0x7f0900e6

.field public static final main_menu_base_course_view:I = 0x7f0900e7

.field public static final main_menu_course_add_info:I = 0x7f0900e8

.field public static final main_menu_random_trainer_view:I = 0x7f0900e9

.field public static final masked:I = 0x7f0900ea

.field public static final math_best_score_text_view:I = 0x7f0900eb

.field public static final math_best_score_title_view:I = 0x7f0900ec

.field public static final math_buttons_layout_1:I = 0x7f0900ed

.field public static final math_buttons_layout_2:I = 0x7f0900ee

.field public static final math_complexity_button_1:I = 0x7f0900ef

.field public static final math_complexity_button_2:I = 0x7f0900f0

.field public static final math_complexity_button_3:I = 0x7f0900f1

.field public static final math_complexity_button_4:I = 0x7f0900f2

.field public static final math_correct_answer_text_view:I = 0x7f0900f3

.field public static final math_points_text_view:I = 0x7f0900f4

.field public static final math_result_line_chart:I = 0x7f0900f5

.field public static final math_score_text_view:I = 0x7f0900f6

.field public static final math_score_title_view:I = 0x7f0900f7

.field public static final mathematics_button_1:I = 0x7f0900f8

.field public static final mathematics_button_2:I = 0x7f0900f9

.field public static final mathematics_button_3:I = 0x7f0900fa

.field public static final mathematics_button_4:I = 0x7f0900fb

.field public static final mathematics_card_layout:I = 0x7f0900fc

.field public static final mathematics_expression_text_view:I = 0x7f0900fd

.field public static final mathematics_progress_bar:I = 0x7f0900fe

.field public static final mathematics_record_text_view:I = 0x7f0900ff

.field public static final mathematics_score_text_view:I = 0x7f090100

.field public static final mathematics_statistics_layout:I = 0x7f090101

.field public static final mathematics_timer_bar:I = 0x7f090102

.field public static final max_speed_text_view:I = 0x7f090103

.field public static final media_actions:I = 0x7f090104

.field public static final message:I = 0x7f090105

.field public static final middle:I = 0x7f090106

.field public static final mini:I = 0x7f090107

.field public static final mistakes_text_view:I = 0x7f090108

.field public static final mistakes_title_view:I = 0x7f090109

.field public static final motivators_message_text_view:I = 0x7f09010a

.field public static final motivators_speed_reading_book_view:I = 0x7f09010b

.field public static final motivators_view:I = 0x7f09010c

.field public static final multiply:I = 0x7f09010d

.field public static final navigation_chapter_title_text_view:I = 0x7f09010e

.field public static final navigation_header_container:I = 0x7f09010f

.field public static final navigation_next_page_view:I = 0x7f090110

.field public static final navigation_page_text_view:I = 0x7f090111

.field public static final navigation_pencil_frame_layout:I = 0x7f090112

.field public static final navigation_pencil_frame_minus_view:I = 0x7f090113

.field public static final navigation_pencil_frame_plus_view:I = 0x7f090114

.field public static final navigation_pencil_frame_selected_line_count_text_view:I = 0x7f090115

.field public static final navigation_previous_page_view:I = 0x7f090116

.field public static final navigation_seek_bar:I = 0x7f090117

.field public static final navigation_view:I = 0x7f090118

.field public static final never:I = 0x7f090119

.field public static final new_best_score_image_view:I = 0x7f09011a

.field public static final new_best_time_image_view:I = 0x7f09011b

.field public static final new_concentration_best_score_image_view:I = 0x7f09011c

.field public static final new_math_best_score_image_view:I = 0x7f09011d

.field public static final next_button:I = 0x7f09011e

.field public static final none:I = 0x7f09011f

.field public static final normal:I = 0x7f090120

.field public static final notification_background:I = 0x7f090121

.field public static final notification_main_column:I = 0x7f090122

.field public static final notification_main_column_container:I = 0x7f090123

.field public static final off:I = 0x7f090124

.field public static final on:I = 0x7f090125

.field public static final packed:I = 0x7f090126

.field public static final pageIndicatorView:I = 0x7f090127

.field public static final page_text_view:I = 0x7f090128

.field public static final page_three:I = 0x7f090129

.field public static final pager:I = 0x7f09012a

.field public static final parallax:I = 0x7f09012b

.field public static final parent:I = 0x7f09012c

.field public static final parentPanel:I = 0x7f09012d

.field public static final parent_matrix:I = 0x7f09012e

.field public static final pass_course_progress:I = 0x7f09012f

.field public static final pass_course_restart_iv:I = 0x7f090130

.field public static final pass_course_result_result_layout:I = 0x7f090131

.field public static final pass_course_score_text_view:I = 0x7f090132

.field public static final pass_course_trainers_left:I = 0x7f090133

.field public static final pencil_frame_layout:I = 0x7f090134

.field public static final pencil_frame_view:I = 0x7f090135

.field public static final percent:I = 0x7f090136

.field public static final pin:I = 0x7f090137

.field public static final pointsLeftTV:I = 0x7f090138

.field public static final points_text_view:I = 0x7f090139

.field public static final premium_features_layout:I = 0x7f09013a

.field public static final premium_forever_button:I = 0x7f09013b

.field public static final premium_halfyear_price:I = 0x7f09013c

.field public static final premium_month_price:I = 0x7f09013d

.field public static final premium_purchase_button:I = 0x7f09013e

.field public static final premium_purchase_forever_button:I = 0x7f09013f

.field public static final premium_purchase_half_year_button:I = 0x7f090140

.field public static final premium_purchase_month_button:I = 0x7f090141

.field public static final premium_purchase_year_button:I = 0x7f090142

.field public static final premium_year_price:I = 0x7f090143

.field public static final prepare_circular_progress_bar:I = 0x7f090144

.field public static final prepare_title:I = 0x7f090145

.field public static final preview_title:I = 0x7f090146

.field public static final privacy_policy_button:I = 0x7f090147

.field public static final profileIV:I = 0x7f090148

.field public static final profileTitleTV:I = 0x7f090149

.field public static final profile_rating_tv:I = 0x7f09014a

.field public static final profile_settings_view:I = 0x7f09014b

.field public static final profile_share_view:I = 0x7f09014c

.field public static final profile_type_tv:I = 0x7f09014d

.field public static final progress_bar:I = 0x7f09014e

.field public static final progress_circular:I = 0x7f09014f

.field public static final progress_horizontal:I = 0x7f090150

.field public static final progress_text_view:I = 0x7f090151

.field public static final purchase_advantage_description_tv:I = 0x7f090152

.field public static final purchase_advantage_image_view:I = 0x7f090153

.field public static final purchase_advantage_title_tv:I = 0x7f090154

.field public static final purchase_premium_image_view:I = 0x7f090155

.field public static final radio:I = 0x7f090156

.field public static final rate_app_dialog_dont_show_again:I = 0x7f090157

.field public static final rate_app_dialog_rate_now:I = 0x7f090158

.field public static final rate_app_dialog_remind_me_later:I = 0x7f090159

.field public static final recommendation_description_view:I = 0x7f09015a

.field public static final recommendation_item_view:I = 0x7f09015b

.field public static final recommendation_link_view:I = 0x7f09015c

.field public static final recommendation_title_view:I = 0x7f09015d

.field public static final recommendation_view:I = 0x7f09015e

.field public static final record_text_view:I = 0x7f09015f

.field public static final recycler_view:I = 0x7f090160

.field public static final remember_number_button_0:I = 0x7f090161

.field public static final remember_number_button_1:I = 0x7f090162

.field public static final remember_number_button_2:I = 0x7f090163

.field public static final remember_number_button_3:I = 0x7f090164

.field public static final remember_number_button_4:I = 0x7f090165

.field public static final remember_number_button_5:I = 0x7f090166

.field public static final remember_number_button_6:I = 0x7f090167

.field public static final remember_number_button_7:I = 0x7f090168

.field public static final remember_number_button_8:I = 0x7f090169

.field public static final remember_number_button_9:I = 0x7f09016a

.field public static final remember_number_button_backspace:I = 0x7f09016b

.field public static final remember_number_card_layout:I = 0x7f09016c

.field public static final remember_number_result_line_chart:I = 0x7f09016d

.field public static final remember_number_statistics_layout:I = 0x7f09016e

.field public static final remember_words_best_score_tv:I = 0x7f09016f

.field public static final remember_words_new_best_iv:I = 0x7f090170

.field public static final remember_words_result_line_chart:I = 0x7f090171

.field public static final remember_words_score_title_view:I = 0x7f090172

.field public static final remember_words_score_tv:I = 0x7f090173

.field public static final restart:I = 0x7f090174

.field public static final result_score_text_view:I = 0x7f090175

.field public static final right:I = 0x7f090176

.field public static final right_icon:I = 0x7f090177

.field public static final right_side:I = 0x7f090178

.field public static final rules_of_success_view:I = 0x7f090179

.field public static final rules_of_success_view_2:I = 0x7f09017a

.field public static final save_image_matrix:I = 0x7f09017b

.field public static final save_non_transition_alpha:I = 0x7f09017c

.field public static final save_scale_type:I = 0x7f09017d

.field public static final scale:I = 0x7f09017e

.field public static final schulte_best_time_text_view:I = 0x7f09017f

.field public static final schulte_grid_layout:I = 0x7f090180

.field public static final schulte_next_item_text_view:I = 0x7f090181

.field public static final schulte_statistics_panel:I = 0x7f090182

.field public static final schulte_table_result_best_time_text_view:I = 0x7f090183

.field public static final schulte_table_result_chart_view:I = 0x7f090184

.field public static final schulte_table_result_new_best_time_image_view:I = 0x7f090185

.field public static final schulte_table_result_time_text_view:I = 0x7f090186

.field public static final schulte_time_text_view:I = 0x7f090187

.field public static final score_text_view:I = 0x7f090188

.field public static final score_title_view:I = 0x7f090189

.field public static final screen:I = 0x7f09018a

.field public static final scroll:I = 0x7f09018b

.field public static final scrollIndicatorDown:I = 0x7f09018c

.field public static final scrollIndicatorUp:I = 0x7f09018d

.field public static final scrollView:I = 0x7f09018e

.field public static final scrollable:I = 0x7f09018f

.field public static final search_badge:I = 0x7f090190

.field public static final search_bar:I = 0x7f090191

.field public static final search_button:I = 0x7f090192

.field public static final search_close_btn:I = 0x7f090193

.field public static final search_edit_frame:I = 0x7f090194

.field public static final search_go_btn:I = 0x7f090195

.field public static final search_mag_icon:I = 0x7f090196

.field public static final search_plate:I = 0x7f090197

.field public static final search_src_text:I = 0x7f090198

.field public static final search_voice_btn:I = 0x7f090199

.field public static final seekbar:I = 0x7f09019a

.field public static final seekbar_value:I = 0x7f09019b

.field public static final select_dialog_listview:I = 0x7f09019c

.field public static final settings:I = 0x7f09019d

.field public static final share:I = 0x7f09019e

.field public static final shortcut:I = 0x7f09019f

.field public static final showCustom:I = 0x7f0901a0

.field public static final showHome:I = 0x7f0901a1

.field public static final showTitle:I = 0x7f0901a2

.field public static final show_market_view:I = 0x7f0901a3

.field public static final slide:I = 0x7f0901a4

.field public static final smallLabel:I = 0x7f0901a5

.field public static final snackbar_action:I = 0x7f0901a6

.field public static final snackbar_text:I = 0x7f0901a7

.field public static final snap:I = 0x7f0901a8

.field public static final spacer:I = 0x7f0901a9

.field public static final speed_down_view:I = 0x7f0901aa

.field public static final speed_reading_answer_button_1:I = 0x7f0901ab

.field public static final speed_reading_answer_button_2:I = 0x7f0901ac

.field public static final speed_reading_answer_button_3:I = 0x7f0901ad

.field public static final speed_reading_answer_button_4:I = 0x7f0901ae

.field public static final speed_reading_answer_button_5:I = 0x7f0901af

.field public static final speed_reading_answer_button_6:I = 0x7f0901b0

.field public static final speed_reading_answer_grid_layout:I = 0x7f0901b1

.field public static final speed_reading_answer_speed_text_view:I = 0x7f0901b2

.field public static final speed_reading_answer_speed_title_text_view:I = 0x7f0901b3

.field public static final speed_reading_book:I = 0x7f0901b4

.field public static final speed_reading_book_not_available:I = 0x7f0901b5

.field public static final speed_reading_book_progress:I = 0x7f0901b6

.field public static final speed_reading_book_progress_tv:I = 0x7f0901b7

.field public static final speed_reading_book_start_tv:I = 0x7f0901b8

.field public static final speed_reading_progress_bar:I = 0x7f0901b9

.field public static final speed_reading_result_line_chart:I = 0x7f0901ba

.field public static final speed_reading_speed_text_view:I = 0x7f0901bb

.field public static final speed_reading_statistics_layout:I = 0x7f0901bc

.field public static final speed_reading_word_view_1:I = 0x7f0901bd

.field public static final speed_reading_word_view_2:I = 0x7f0901be

.field public static final speed_reading_word_view_3:I = 0x7f0901bf

.field public static final speed_reading_word_view_4:I = 0x7f0901c0

.field public static final speed_reading_word_view_5:I = 0x7f0901c1

.field public static final speed_reading_word_view_6:I = 0x7f0901c2

.field public static final speed_reading_word_view_7:I = 0x7f0901c3

.field public static final speed_reading_word_view_8:I = 0x7f0901c4

.field public static final speed_reading_word_view_9:I = 0x7f0901c5

.field public static final speed_reading_words_view:I = 0x7f0901c6

.field public static final speed_text_view:I = 0x7f0901c7

.field public static final speed_up_view:I = 0x7f0901c8

.field public static final spinner:I = 0x7f0901c9

.field public static final split_action_bar:I = 0x7f0901ca

.field public static final spread:I = 0x7f0901cb

.field public static final spread_inside:I = 0x7f0901cc

.field public static final src_atop:I = 0x7f0901cd

.field public static final src_in:I = 0x7f0901ce

.field public static final src_over:I = 0x7f0901cf

.field public static final standard:I = 0x7f0901d0

.field public static final start:I = 0x7f0901d1

.field public static final start_cup_button:I = 0x7f0901d2

.field public static final start_reading_button:I = 0x7f0901d3

.field public static final start_remember_words_button:I = 0x7f0901d4

.field public static final start_true_colors_button:I = 0x7f0901d5

.field public static final status_bar_latest_event_content:I = 0x7f0901d6

.field public static final submenuarrow:I = 0x7f0901d7

.field public static final submit_area:I = 0x7f0901d8

.field public static final swap:I = 0x7f0901d9

.field public static final switchWidget:I = 0x7f0901da

.field public static final tabMode:I = 0x7f0901db

.field public static final tab_layout:I = 0x7f0901dc

.field public static final tableLayout:I = 0x7f0901dd

.field public static final text:I = 0x7f0901de

.field public static final text2:I = 0x7f0901df

.field public static final textSpacerNoButtons:I = 0x7f0901e0

.field public static final textSpacerNoTitle:I = 0x7f0901e1

.field public static final textView:I = 0x7f0901e2

.field public static final textView10:I = 0x7f0901e3

.field public static final textView11:I = 0x7f0901e4

.field public static final textView12:I = 0x7f0901e5

.field public static final textView13:I = 0x7f0901e6

.field public static final textView14:I = 0x7f0901e7

.field public static final textView15:I = 0x7f0901e8

.field public static final textView16:I = 0x7f0901e9

.field public static final textView17:I = 0x7f0901ea

.field public static final textView18:I = 0x7f0901eb

.field public static final textView19:I = 0x7f0901ec

.field public static final textView2:I = 0x7f0901ed

.field public static final textView20:I = 0x7f0901ee

.field public static final textView21:I = 0x7f0901ef

.field public static final textView22:I = 0x7f0901f0

.field public static final textView23:I = 0x7f0901f1

.field public static final textView24:I = 0x7f0901f2

.field public static final textView25:I = 0x7f0901f3

.field public static final textView26:I = 0x7f0901f4

.field public static final textView27:I = 0x7f0901f5

.field public static final textView28:I = 0x7f0901f6

.field public static final textView29:I = 0x7f0901f7

.field public static final textView3:I = 0x7f0901f8

.field public static final textView30:I = 0x7f0901f9

.field public static final textView34:I = 0x7f0901fa

.field public static final textView35:I = 0x7f0901fb

.field public static final textView36:I = 0x7f0901fc

.field public static final textView37:I = 0x7f0901fd

.field public static final textView4:I = 0x7f0901fe

.field public static final textView54:I = 0x7f0901ff

.field public static final textView7:I = 0x7f090200

.field public static final textView76:I = 0x7f090201

.field public static final text_input_password_toggle:I = 0x7f090202

.field public static final textinput_counter:I = 0x7f090203

.field public static final textinput_error:I = 0x7f090204

.field public static final thinWorm:I = 0x7f090205

.field public static final time:I = 0x7f090206

.field public static final time_text_view:I = 0x7f090207

.field public static final time_title_view:I = 0x7f090208

.field public static final title:I = 0x7f090209

.field public static final titleDividerNoCustom:I = 0x7f09020a

.field public static final title_template:I = 0x7f09020b

.field public static final title_text_view:I = 0x7f09020c

.field public static final toolbar:I = 0x7f09020d

.field public static final top:I = 0x7f09020e

.field public static final topPanel:I = 0x7f09020f

.field public static final touch_outside:I = 0x7f090210

.field public static final training_menu_base_course_view:I = 0x7f090211

.field public static final training_menu_start_accelerator_menu:I = 0x7f090212

.field public static final training_menu_start_accelerator_pass_course:I = 0x7f090213

.field public static final training_menu_start_concentration:I = 0x7f090214

.field public static final training_menu_start_cup_timer:I = 0x7f090215

.field public static final training_menu_start_even_numbers:I = 0x7f090216

.field public static final training_menu_start_flash_words:I = 0x7f090217

.field public static final training_menu_start_green_dot:I = 0x7f090218

.field public static final training_menu_start_line_of_sight:I = 0x7f090219

.field public static final training_menu_start_mathematics:I = 0x7f09021a

.field public static final training_menu_start_remember_number:I = 0x7f09021b

.field public static final training_menu_start_remember_words:I = 0x7f09021c

.field public static final training_menu_start_schulte_table:I = 0x7f09021d

.field public static final training_menu_start_speed_reading:I = 0x7f09021e

.field public static final training_menu_start_true_colors:I = 0x7f09021f

.field public static final training_menu_start_word_block:I = 0x7f090220

.field public static final training_menu_start_word_pairs:I = 0x7f090221

.field public static final training_menu_start_words_columns:I = 0x7f090222

.field public static final training_pager:I = 0x7f090223

.field public static final training_pause_dialog_continue_view:I = 0x7f090224

.field public static final training_pause_dialog_exit_view:I = 0x7f090225

.field public static final training_pause_dialog_help_view:I = 0x7f090226

.field public static final training_pause_dialog_restart_view:I = 0x7f090227

.field public static final training_pause_dialog_settings_view:I = 0x7f090228

.field public static final transition_current_scene:I = 0x7f090229

.field public static final transition_layout_save:I = 0x7f09022a

.field public static final transition_position:I = 0x7f09022b

.field public static final transition_scene_layoutid_cache:I = 0x7f09022c

.field public static final transition_transform:I = 0x7f09022d

.field public static final true_colors_answer_tv_1:I = 0x7f09022e

.field public static final true_colors_answer_tv_2:I = 0x7f09022f

.field public static final true_colors_answer_tv_3:I = 0x7f090230

.field public static final true_colors_answer_tv_4:I = 0x7f090231

.field public static final true_colors_best_score_tv:I = 0x7f090232

.field public static final true_colors_buttons_table:I = 0x7f090233

.field public static final true_colors_new_best_iv:I = 0x7f090234

.field public static final true_colors_progress_bar:I = 0x7f090235

.field public static final true_colors_question_tv:I = 0x7f090236

.field public static final true_colors_record_text_view:I = 0x7f090237

.field public static final true_colors_result_line_chart:I = 0x7f090238

.field public static final true_colors_score_text_view:I = 0x7f090239

.field public static final true_colors_score_title_view:I = 0x7f09023a

.field public static final true_colors_score_tv:I = 0x7f09023b

.field public static final true_colors_statistics_layout:I = 0x7f09023c

.field public static final true_colors_timer_bar:I = 0x7f09023d

.field public static final uniform:I = 0x7f09023e

.field public static final up:I = 0x7f09023f

.field public static final useLogo:I = 0x7f090240

.field public static final vertical:I = 0x7f090241

.field public static final view_offset_helper:I = 0x7f090242

.field public static final view_pager:I = 0x7f090243

.field public static final visible:I = 0x7f090244

.field public static final withText:I = 0x7f090245

.field public static final word_count_text_view:I = 0x7f090246

.field public static final word_pairs_grid_layout:I = 0x7f090247

.field public static final words_layout:I = 0x7f090248

.field public static final worm:I = 0x7f090249

.field public static final wrap:I = 0x7f09024a

.field public static final wrap_content:I = 0x7f09024b


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
