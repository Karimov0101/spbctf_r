.class public final Lcom/safonov/speedreading/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/safonov/speedreading/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f0e0000

.field public static final abc_action_bar_home_description_format:I = 0x7f0e0001

.field public static final abc_action_bar_home_subtitle_description_format:I = 0x7f0e0002

.field public static final abc_action_bar_up_description:I = 0x7f0e0003

.field public static final abc_action_menu_overflow_description:I = 0x7f0e0004

.field public static final abc_action_mode_done:I = 0x7f0e0005

.field public static final abc_activity_chooser_view_see_all:I = 0x7f0e0006

.field public static final abc_activitychooserview_choose_application:I = 0x7f0e0007

.field public static final abc_capital_off:I = 0x7f0e0008

.field public static final abc_capital_on:I = 0x7f0e0009

.field public static final abc_font_family_body_1_material:I = 0x7f0e000a

.field public static final abc_font_family_body_2_material:I = 0x7f0e000b

.field public static final abc_font_family_button_material:I = 0x7f0e000c

.field public static final abc_font_family_caption_material:I = 0x7f0e000d

.field public static final abc_font_family_display_1_material:I = 0x7f0e000e

.field public static final abc_font_family_display_2_material:I = 0x7f0e000f

.field public static final abc_font_family_display_3_material:I = 0x7f0e0010

.field public static final abc_font_family_display_4_material:I = 0x7f0e0011

.field public static final abc_font_family_headline_material:I = 0x7f0e0012

.field public static final abc_font_family_menu_material:I = 0x7f0e0013

.field public static final abc_font_family_subhead_material:I = 0x7f0e0014

.field public static final abc_font_family_title_material:I = 0x7f0e0015

.field public static final abc_search_hint:I = 0x7f0e0016

.field public static final abc_searchview_description_clear:I = 0x7f0e0017

.field public static final abc_searchview_description_query:I = 0x7f0e0018

.field public static final abc_searchview_description_search:I = 0x7f0e0019

.field public static final abc_searchview_description_submit:I = 0x7f0e001a

.field public static final abc_searchview_description_voice:I = 0x7f0e001b

.field public static final abc_shareactionprovider_share_with:I = 0x7f0e001c

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f0e001d

.field public static final abc_toolbar_collapse_description:I = 0x7f0e001e

.field public static final accelerator:I = 0x7f0e001f

.field public static final accelerator_description_message:I = 0x7f0e0020

.field public static final accelerator_description_title:I = 0x7f0e0021

.field public static final accelerator_result_duration_title:I = 0x7f0e0022

.field public static final accelerator_result_min:I = 0x7f0e0023

.field public static final accelerator_result_sec:I = 0x7f0e0024

.field public static final accelerator_settings_column_count_default_value:I = 0x7f0e0025

.field public static final accelerator_settings_column_count_key:I = 0x7f0e0026

.field public static final accelerator_settings_column_count_summary:I = 0x7f0e0027

.field public static final accelerator_settings_column_count_title:I = 0x7f0e0028

.field public static final accelerator_settings_duration_default_value:I = 0x7f0e0029

.field public static final accelerator_settings_duration_key:I = 0x7f0e002a

.field public static final accelerator_settings_duration_summary:I = 0x7f0e002b

.field public static final accelerator_settings_duration_title:I = 0x7f0e002c

.field public static final accelerator_speed:I = 0x7f0e002d

.field public static final accelerator_time:I = 0x7f0e002e

.field public static final advices:I = 0x7f0e002f

.field public static final all_books:I = 0x7f0e0030

.field public static final app_name:I = 0x7f0e0031

.field public static final appbar_scrolling_view_behavior:I = 0x7f0e0032

.field public static final application_url:I = 0x7f0e0033

.field public static final banner_advertisement_id:I = 0x7f0e0034

.field public static final base_course_name:I = 0x7f0e0035

.field public static final base_course_short_name:I = 0x7f0e0036

.field public static final base_profile:I = 0x7f0e0037

.field public static final begin:I = 0x7f0e0038

.field public static final book:I = 0x7f0e0039

.field public static final book_detail_author:I = 0x7f0e003a

.field public static final book_detail_file_path:I = 0x7f0e003b

.field public static final book_detail_language:I = 0x7f0e003c

.field public static final book_detail_title:I = 0x7f0e003d

.field public static final bottom_menu_home:I = 0x7f0e003e

.field public static final bottom_menu_materials:I = 0x7f0e003f

.field public static final bottom_menu_profile:I = 0x7f0e0040

.field public static final bottom_menu_reading:I = 0x7f0e0041

.field public static final bottom_menu_training:I = 0x7f0e0042

.field public static final bottom_sheet_behavior:I = 0x7f0e0043

.field public static final character_counter_pattern:I = 0x7f0e0044

.field public static final common_google_play_services_unknown_issue:I = 0x7f0e0045

.field public static final concentration:I = 0x7f0e0046

.field public static final concentration_complexity_auto:I = 0x7f0e0047

.field public static final concentration_complexity_self:I = 0x7f0e0048

.field public static final concentration_complexity_title:I = 0x7f0e0049

.field public static final concentration_description_text:I = 0x7f0e004a

.field public static final count:I = 0x7f0e004b

.field public static final course_max_points:I = 0x7f0e004c

.field public static final course_need_points:I = 0x7f0e004d

.field public static final course_points_add_info:I = 0x7f0e004e

.field public static final courses:I = 0x7f0e004f

.field public static final creating:I = 0x7f0e0050

.field public static final cup_timer_settings_duration_default_value:I = 0x7f0e0051

.field public static final cup_timer_settings_duration_key:I = 0x7f0e0052

.field public static final cup_timer_settings_type_default_value:I = 0x7f0e0053

.field public static final cup_timer_settings_type_key:I = 0x7f0e0054

.field public static final cup_timer_settings_type_summary:I = 0x7f0e0055

.field public static final cup_timer_settings_type_title:I = 0x7f0e0056

.field public static final cuptimer:I = 0x7f0e0057

.field public static final cuptimer_description_text:I = 0x7f0e0058

.field public static final cuptimer_info:I = 0x7f0e0059

.field public static final current_level:I = 0x7f0e005a

.field public static final default_web_client_id:I = 0x7f0e005b

.field public static final description_do_not_show_it_again:I = 0x7f0e005c

.field public static final description_start:I = 0x7f0e005d

.field public static final dialog_continue:I = 0x7f0e005e

.field public static final dialog_exit:I = 0x7f0e005f

.field public static final dialog_help:I = 0x7f0e0060

.field public static final dialog_restart:I = 0x7f0e0061

.field public static final dialog_settings:I = 0x7f0e0062

.field public static final even_numbers:I = 0x7f0e0063

.field public static final even_numbers_best_score:I = 0x7f0e0064

.field public static final even_numbers_description_message:I = 0x7f0e0065

.field public static final even_numbers_description_title:I = 0x7f0e0066

.field public static final even_numbers_result_best_score:I = 0x7f0e0067

.field public static final even_numbers_result_new_best:I = 0x7f0e0068

.field public static final even_numbers_result_score:I = 0x7f0e0069

.field public static final even_numbers_score:I = 0x7f0e006a

.field public static final file_explorer_book_adding:I = 0x7f0e006b

.field public static final file_explorer_book_adding_failed:I = 0x7f0e006c

.field public static final file_explorer_book_adding_success:I = 0x7f0e006d

.field public static final file_explorer_book_already_exist:I = 0x7f0e006e

.field public static final file_explorer_file_type:I = 0x7f0e006f

.field public static final file_explorer_open:I = 0x7f0e0070

.field public static final firebase_database_url:I = 0x7f0e0071

.field public static final flash_words:I = 0x7f0e0072

.field public static final flash_words_level:I = 0x7f0e0073

.field public static final flash_words_settings_duration_default_value:I = 0x7f0e0074

.field public static final flash_words_settings_duration_key:I = 0x7f0e0075

.field public static final flash_words_settings_duration_summary:I = 0x7f0e0076

.field public static final flash_words_settings_duration_title:I = 0x7f0e0077

.field public static final forever_premium:I = 0x7f0e0078

.field public static final gcm_defaultSenderId:I = 0x7f0e0079

.field public static final getting_rid_of_pronouncing:I = 0x7f0e007a

.field public static final google_api_key:I = 0x7f0e007b

.field public static final google_app_id:I = 0x7f0e007c

.field public static final google_crash_reporting_api_key:I = 0x7f0e007d

.field public static final google_storage_bucket:I = 0x7f0e007e

.field public static final green_dot:I = 0x7f0e007f

.field public static final green_dot_description_message_1:I = 0x7f0e0080

.field public static final green_dot_description_message_2:I = 0x7f0e0081

.field public static final green_dot_description_message_3:I = 0x7f0e0082

.field public static final green_dot_description_message_4:I = 0x7f0e0083

.field public static final green_dot_description_title:I = 0x7f0e0084

.field public static final green_dot_description_title_2:I = 0x7f0e0085

.field public static final green_dot_description_title_3:I = 0x7f0e0086

.field public static final green_dot_description_title_4:I = 0x7f0e0087

.field public static final green_dot_pass_course_result_duration:I = 0x7f0e0088

.field public static final green_dot_result_duration_title:I = 0x7f0e0089

.field public static final green_dot_settings_duration_default_value:I = 0x7f0e008a

.field public static final green_dot_settings_duration_key:I = 0x7f0e008b

.field public static final green_dot_settings_duration_summary:I = 0x7f0e008c

.field public static final green_dot_settings_duration_title:I = 0x7f0e008d

.field public static final green_dot_text:I = 0x7f0e008e

.field public static final half_year_premium:I = 0x7f0e008f

.field public static final help:I = 0x7f0e0090

.field public static final interstitial_advertisement_id:I = 0x7f0e0091

.field public static final interstitial_next_training:I = 0x7f0e0092

.field public static final interstitial_restart:I = 0x7f0e0093

.field public static final interstitial_restart_button:I = 0x7f0e0094

.field public static final interstitial_restart_description:I = 0x7f0e0095

.field public static final interstitial_start:I = 0x7f0e0096

.field public static final level:I = 0x7f0e0097

.field public static final library:I = 0x7f0e0098

.field public static final library_book_deleting:I = 0x7f0e0099

.field public static final library_book_progress:I = 0x7f0e009a

.field public static final library_book_remove_cancel:I = 0x7f0e009b

.field public static final library_book_remove_delete:I = 0x7f0e009c

.field public static final library_book_remove_message:I = 0x7f0e009d

.field public static final library_data_loading:I = 0x7f0e009e

.field public static final library_format_dialog_continue:I = 0x7f0e009f

.field public static final library_format_dialog_do_not_show_again:I = 0x7f0e00a0

.field public static final library_format_dialog_message:I = 0x7f0e00a1

.field public static final library_format_dialog_title:I = 0x7f0e00a2

.field public static final library_is_empty:I = 0x7f0e00a3

.field public static final library_permission_denied:I = 0x7f0e00a4

.field public static final library_permission_set:I = 0x7f0e00a5

.field public static final line_of_sight:I = 0x7f0e00a6

.field public static final line_of_sight_description_message:I = 0x7f0e00a7

.field public static final line_of_sight_description_title:I = 0x7f0e00a8

.field public static final line_of_sight_mistake:I = 0x7f0e00a9

.field public static final line_of_sight_pass_course_result_accuracy:I = 0x7f0e00aa

.field public static final line_of_sight_pass_course_result_found_mistakes:I = 0x7f0e00ab

.field public static final line_of_sight_pass_course_result_mistakes:I = 0x7f0e00ac

.field public static final line_of_sight_result_found_mistakes:I = 0x7f0e00ad

.field public static final line_of_sight_result_found_mistakes_accuracy:I = 0x7f0e00ae

.field public static final line_of_sight_result_mistakes:I = 0x7f0e00af

.field public static final line_of_sight_settings_board_size_summary:I = 0x7f0e00b0

.field public static final line_of_sight_settings_board_size_title:I = 0x7f0e00b1

.field public static final line_of_sight_settings_field_type_preference_default_value:I = 0x7f0e00b2

.field public static final line_of_sight_settings_field_type_preference_key:I = 0x7f0e00b3

.field public static final line_of_sight_settings_field_type_summary:I = 0x7f0e00b4

.field public static final line_of_sight_settings_field_type_title:I = 0x7f0e00b5

.field public static final line_of_sight_settings_row_count_preference_default_value:I = 0x7f0e00b6

.field public static final line_of_sight_settings_row_count_preference_key:I = 0x7f0e00b7

.field public static final main_menu_description_message_1:I = 0x7f0e00b8

.field public static final main_menu_description_message_2_part_1:I = 0x7f0e00b9

.field public static final main_menu_description_message_2_part_2:I = 0x7f0e00ba

.field public static final main_menu_description_message_4:I = 0x7f0e00bb

.field public static final main_menu_description_message_5:I = 0x7f0e00bc

.field public static final main_menu_description_title_1:I = 0x7f0e00bd

.field public static final main_menu_description_title_2:I = 0x7f0e00be

.field public static final main_menu_description_title_4:I = 0x7f0e00bf

.field public static final main_menu_description_title_5:I = 0x7f0e00c0

.field public static final math_complexity_choose:I = 0x7f0e00c1

.field public static final math_complexity_easy:I = 0x7f0e00c2

.field public static final math_complexity_hard:I = 0x7f0e00c3

.field public static final math_complexity_mixed:I = 0x7f0e00c4

.field public static final math_complexity_normal:I = 0x7f0e00c5

.field public static final math_correct_answer:I = 0x7f0e00c6

.field public static final math_description_text:I = 0x7f0e00c7

.field public static final math_settings_duration_default_value:I = 0x7f0e00c8

.field public static final math_settings_duration_key:I = 0x7f0e00c9

.field public static final mathematics:I = 0x7f0e00ca

.field public static final mixed_up_letters:I = 0x7f0e00cb

.field public static final month_premium:I = 0x7f0e00cc

.field public static final more:I = 0x7f0e00cd

.field public static final more_speed_ads_description:I = 0x7f0e00ce

.field public static final more_speed_ads_title:I = 0x7f0e00cf

.field public static final more_training_ads_description:I = 0x7f0e00d0

.field public static final more_training_ads_title:I = 0x7f0e00d1

.field public static final motivators:I = 0x7f0e00d2

.field public static final motivators_title:I = 0x7f0e00d3

.field public static final need_points:I = 0x7f0e00d4

.field public static final new_books:I = 0x7f0e00d5

.field public static final no_ads_description:I = 0x7f0e00d6

.field public static final no_ads_title:I = 0x7f0e00d7

.field public static final notification_first_type:I = 0x7f0e00d8

.field public static final notification_second_type:I = 0x7f0e00d9

.field public static final notifications:I = 0x7f0e00da

.field public static final notifications_settings_notify_key:I = 0x7f0e00db

.field public static final notifications_settings_notify_time_key:I = 0x7f0e00dc

.field public static final notifications_settings_set_time_title:I = 0x7f0e00dd

.field public static final notifications_settings_title:I = 0x7f0e00de

.field public static final pass_course:I = 0x7f0e00df

.field public static final pass_course_current_trainer:I = 0x7f0e00e0

.field public static final pass_course_next:I = 0x7f0e00e1

.field public static final pass_course_rating:I = 0x7f0e00e2

.field public static final pass_course_rating_beginner:I = 0x7f0e00e3

.field public static final pass_course_rating_expert:I = 0x7f0e00e4

.field public static final pass_course_rating_genius:I = 0x7f0e00e5

.field public static final pass_course_rating_guru:I = 0x7f0e00e6

.field public static final pass_course_rating_learner:I = 0x7f0e00e7

.field public static final pass_course_rating_master:I = 0x7f0e00e8

.field public static final pass_course_rating_not_passed:I = 0x7f0e00e9

.field public static final pass_course_rating_professor:I = 0x7f0e00ea

.field public static final pass_course_rating_score:I = 0x7f0e00eb

.field public static final pass_course_reading_description:I = 0x7f0e00ec

.field public static final pass_course_result_t:I = 0x7f0e00ed

.field public static final pass_course_score:I = 0x7f0e00ee

.field public static final pass_course_trainers_left:I = 0x7f0e00ef

.field public static final pass_course_training_number:I = 0x7f0e00f0

.field public static final pass_descr:I = 0x7f0e00f1

.field public static final pass_descr_close:I = 0x7f0e00f2

.field public static final pass_descr_start:I = 0x7f0e00f3

.field public static final pass_descr_title:I = 0x7f0e00f4

.field public static final password_toggle_content_description:I = 0x7f0e00f5

.field public static final path_password_eye:I = 0x7f0e00f6

.field public static final path_password_eye_mask_strike_through:I = 0x7f0e00f7

.field public static final path_password_eye_mask_visible:I = 0x7f0e00f8

.field public static final path_password_strike_through:I = 0x7f0e00f9

.field public static final pay_for:I = 0x7f0e00fa

.field public static final per_month:I = 0x7f0e00fb

.field public static final points_left:I = 0x7f0e00fc

.field public static final premium:I = 0x7f0e00fd

.field public static final premium_description:I = 0x7f0e00fe

.field public static final prepare_title:I = 0x7f0e00ff

.field public static final privacy_policy:I = 0x7f0e0100

.field public static final project_id:I = 0x7f0e0101

.field public static final purchase:I = 0x7f0e0102

.field public static final purchase_error:I = 0x7f0e0103

.field public static final purchase_success:I = 0x7f0e0104

.field public static final random_trainer:I = 0x7f0e0105

.field public static final rate_app_dialog_dont_show_again:I = 0x7f0e0106

.field public static final rate_app_dialog_message:I = 0x7f0e0107

.field public static final rate_app_dialog_rate_now:I = 0x7f0e0108

.field public static final rate_app_dialog_remind_me_later:I = 0x7f0e0109

.field public static final rate_app_dialog_title:I = 0x7f0e010a

.field public static final reader_exit_dialog_cancel:I = 0x7f0e010b

.field public static final reader_exit_dialog_exit:I = 0x7f0e010c

.field public static final reader_exit_dialog_message:I = 0x7f0e010d

.field public static final reader_exit_dialog_title:I = 0x7f0e010e

.field public static final reader_file_opening:I = 0x7f0e010f

.field public static final reader_file_opening_error:I = 0x7f0e0110

.field public static final reader_guide_dialog_center_description:I = 0x7f0e0111

.field public static final reader_guide_dialog_continue:I = 0x7f0e0112

.field public static final reader_guide_dialog_default_reading_center_description:I = 0x7f0e0113

.field public static final reader_guide_dialog_default_reading_message:I = 0x7f0e0114

.field public static final reader_guide_dialog_default_reading_title:I = 0x7f0e0115

.field public static final reader_guide_dialog_do_not_show_again:I = 0x7f0e0116

.field public static final reader_guide_dialog_fast_reading_message:I = 0x7f0e0117

.field public static final reader_guide_dialog_fast_reading_title:I = 0x7f0e0118

.field public static final reader_guide_dialog_flash_reading_message:I = 0x7f0e0119

.field public static final reader_guide_dialog_flash_reading_title:I = 0x7f0e011a

.field public static final reader_guide_dialog_next_page_description:I = 0x7f0e011b

.field public static final reader_guide_dialog_pencil_frame_description:I = 0x7f0e011c

.field public static final reader_guide_dialog_pencil_frame_reading_message:I = 0x7f0e011d

.field public static final reader_guide_dialog_pencil_frame_reading_title:I = 0x7f0e011e

.field public static final reader_guide_dialog_pencil_frame_selected_line_minus_description:I = 0x7f0e011f

.field public static final reader_guide_dialog_pencil_frame_selected_line_plus_description:I = 0x7f0e0120

.field public static final reader_guide_dialog_previous_page_description:I = 0x7f0e0121

.field public static final reader_guide_dialog_speed_down_description:I = 0x7f0e0122

.field public static final reader_guide_dialog_speed_up_description:I = 0x7f0e0123

.field public static final reader_navigation_page:I = 0x7f0e0124

.field public static final reader_page:I = 0x7f0e0125

.field public static final reader_pencil_frame_selected_line_count:I = 0x7f0e0126

.field public static final reader_pencil_frame_selected_line_count_key:I = 0x7f0e0127

.field public static final reader_purchase_premium_dialog_cancel:I = 0x7f0e0128

.field public static final reader_purchase_premium_dialog_message:I = 0x7f0e0129

.field public static final reader_purchase_premium_dialog_purchase:I = 0x7f0e012a

.field public static final reader_purchase_premium_dialog_title:I = 0x7f0e012b

.field public static final reader_select_chapter_dialog_title:I = 0x7f0e012c

.field public static final reader_select_page_dialog_cancel:I = 0x7f0e012d

.field public static final reader_select_page_dialog_select:I = 0x7f0e012e

.field public static final reader_select_page_dialog_title:I = 0x7f0e012f

.field public static final reader_select_reading_mode_dialog_title:I = 0x7f0e0130

.field public static final reader_settings_show_guide_key:I = 0x7f0e0131

.field public static final reader_settings_speed_key:I = 0x7f0e0132

.field public static final reader_settings_text_size_default_value:I = 0x7f0e0133

.field public static final reader_settings_text_size_key:I = 0x7f0e0134

.field public static final reader_settings_text_size_summary:I = 0x7f0e0135

.field public static final reader_settings_text_size_title:I = 0x7f0e0136

.field public static final reader_speed:I = 0x7f0e0137

.field public static final reader_timer_mode_dialog_book_ended_change_book:I = 0x7f0e0138

.field public static final reader_timer_mode_dialog_book_ended_change_page:I = 0x7f0e0139

.field public static final reader_timer_mode_dialog_book_ended_message:I = 0x7f0e013a

.field public static final reader_timer_mode_dialog_book_ended_title:I = 0x7f0e013b

.field public static final reader_timer_mode_dialog_completed_message:I = 0x7f0e013c

.field public static final reader_timer_mode_dialog_completed_title:I = 0x7f0e013d

.field public static final reader_timer_mode_dialog_continue:I = 0x7f0e013e

.field public static final reader_timer_mode_dialog_end:I = 0x7f0e013f

.field public static final reader_timer_mode_dialog_message:I = 0x7f0e0140

.field public static final reader_timer_mode_dialog_start:I = 0x7f0e0141

.field public static final reader_timer_mode_dialog_title:I = 0x7f0e0142

.field public static final reader_timer_mode_time:I = 0x7f0e0143

.field public static final reading:I = 0x7f0e0144

.field public static final reading_3d:I = 0x7f0e0145

.field public static final reading_field:I = 0x7f0e0146

.field public static final reading_with_a_pencil:I = 0x7f0e0147

.field public static final reading_without_vowels:I = 0x7f0e0148

.field public static final recommendation:I = 0x7f0e0149

.field public static final remember_number:I = 0x7f0e014a

.field public static final remember_number_best_score:I = 0x7f0e014b

.field public static final remember_number_description:I = 0x7f0e014c

.field public static final remember_number_description_title:I = 0x7f0e014d

.field public static final remember_number_result_best_score:I = 0x7f0e014e

.field public static final remember_number_result_new_best:I = 0x7f0e014f

.field public static final remember_number_result_score:I = 0x7f0e0150

.field public static final remember_number_score:I = 0x7f0e0151

.field public static final remember_words:I = 0x7f0e0152

.field public static final remember_words_description_text:I = 0x7f0e0153

.field public static final restart:I = 0x7f0e0154

.field public static final rules_of_success:I = 0x7f0e0155

.field public static final rules_of_success_title:I = 0x7f0e0156

.field public static final s1:I = 0x7f0e0157

.field public static final s2:I = 0x7f0e0158

.field public static final s3:I = 0x7f0e0159

.field public static final s4:I = 0x7f0e015a

.field public static final s5:I = 0x7f0e015b

.field public static final s6:I = 0x7f0e015c

.field public static final s7:I = 0x7f0e015d

.field public static final schulte_description_image_1_title:I = 0x7f0e015e

.field public static final schulte_description_image_2_title:I = 0x7f0e015f

.field public static final schulte_description_image_3_title:I = 0x7f0e0160

.field public static final schulte_description_part_1_message:I = 0x7f0e0161

.field public static final schulte_description_part_1_title:I = 0x7f0e0162

.field public static final schulte_description_part_2_message:I = 0x7f0e0163

.field public static final schulte_description_part_2_title:I = 0x7f0e0164

.field public static final schulte_description_part_3_message:I = 0x7f0e0165

.field public static final schulte_description_part_3_title:I = 0x7f0e0166

.field public static final schulte_table:I = 0x7f0e0167

.field public static final schulte_table_best_time:I = 0x7f0e0168

.field public static final schulte_table_description_title:I = 0x7f0e0169

.field public static final schulte_table_next_item:I = 0x7f0e016a

.field public static final schulte_table_result_best_time:I = 0x7f0e016b

.field public static final schulte_table_result_time:I = 0x7f0e016c

.field public static final schulte_table_settings_column_count_preference_default_value:I = 0x7f0e016d

.field public static final schulte_table_settings_column_count_preference_key:I = 0x7f0e016e

.field public static final schulte_table_settings_column_count_summary:I = 0x7f0e016f

.field public static final schulte_table_settings_column_count_title:I = 0x7f0e0170

.field public static final schulte_table_settings_fullscreen_preference_key:I = 0x7f0e0171

.field public static final schulte_table_settings_fullscreen_summary:I = 0x7f0e0172

.field public static final schulte_table_settings_fullscreen_title:I = 0x7f0e0173

.field public static final schulte_table_settings_row_count_preference_default_value:I = 0x7f0e0174

.field public static final schulte_table_settings_row_count_preference_key:I = 0x7f0e0175

.field public static final schulte_table_settings_row_count_summary:I = 0x7f0e0176

.field public static final schulte_table_settings_row_count_title:I = 0x7f0e0177

.field public static final schulte_table_time:I = 0x7f0e0178

.field public static final search_menu_title:I = 0x7f0e0179

.field public static final settings:I = 0x7f0e017a

.field public static final share:I = 0x7f0e017b

.field public static final share_to_friends:I = 0x7f0e017c

.field public static final sighting_reading:I = 0x7f0e017d

.field public static final size:I = 0x7f0e017e

.field public static final special_training:I = 0x7f0e017f

.field public static final special_training_close:I = 0x7f0e0180

.field public static final special_training_message:I = 0x7f0e0181

.field public static final speed:I = 0x7f0e0182

.field public static final speed_reading:I = 0x7f0e0183

.field public static final speed_reading_answer_speed_title:I = 0x7f0e0184

.field public static final speed_reading_book_author:I = 0x7f0e0185

.field public static final speed_reading_book_dialog_answer_no:I = 0x7f0e0186

.field public static final speed_reading_book_dialog_answer_yes:I = 0x7f0e0187

.field public static final speed_reading_book_dialog_description:I = 0x7f0e0188

.field public static final speed_reading_book_dialog_title:I = 0x7f0e0189

.field public static final speed_reading_book_language:I = 0x7f0e018a

.field public static final speed_reading_book_name:I = 0x7f0e018b

.field public static final speed_reading_book_purchase_description:I = 0x7f0e018c

.field public static final speed_reading_book_short_first:I = 0x7f0e018d

.field public static final speed_reading_book_short_second:I = 0x7f0e018e

.field public static final speed_reading_description_message:I = 0x7f0e018f

.field public static final speed_reading_description_title:I = 0x7f0e0190

.field public static final speed_reading_pass_course_result_average_speed:I = 0x7f0e0191

.field public static final speed_reading_pass_course_result_max_speed:I = 0x7f0e0192

.field public static final speed_reading_result_average_speed:I = 0x7f0e0193

.field public static final speed_reading_result_best_average_speed:I = 0x7f0e0194

.field public static final speed_reading_result_best_max_speed:I = 0x7f0e0195

.field public static final speed_reading_result_max_speed:I = 0x7f0e0196

.field public static final speed_reading_speed:I = 0x7f0e0197

.field public static final start_read:I = 0x7f0e0198

.field public static final status_bar_notification_info_overflow:I = 0x7f0e0199

.field public static final storm_method:I = 0x7f0e019a

.field public static final trainers:I = 0x7f0e019b

.field public static final training:I = 0x7f0e019c

.field public static final true_colors:I = 0x7f0e019d

.field public static final true_colors_description_text:I = 0x7f0e019e

.field public static final upper_course_short_name:I = 0x7f0e019f

.field public static final v7_preference_off:I = 0x7f0e01a0

.field public static final v7_preference_on:I = 0x7f0e01a1

.field public static final word_block_result_duration_title:I = 0x7f0e01a2

.field public static final word_block_result_sec:I = 0x7f0e01a3

.field public static final word_block_speed:I = 0x7f0e01a4

.field public static final word_block_time:I = 0x7f0e01a5

.field public static final word_pairs:I = 0x7f0e01a6

.field public static final word_pairs_best_score:I = 0x7f0e01a7

.field public static final word_pairs_description_message:I = 0x7f0e01a8

.field public static final word_pairs_description_title:I = 0x7f0e01a9

.field public static final word_pairs_result_best_score:I = 0x7f0e01aa

.field public static final word_pairs_result_new_best:I = 0x7f0e01ab

.field public static final word_pairs_result_score:I = 0x7f0e01ac

.field public static final word_pairs_score:I = 0x7f0e01ad

.field public static final words_block:I = 0x7f0e01ae

.field public static final words_block_word_count:I = 0x7f0e01af

.field public static final words_columns:I = 0x7f0e01b0

.field public static final year_premium:I = 0x7f0e01b1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5447
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
