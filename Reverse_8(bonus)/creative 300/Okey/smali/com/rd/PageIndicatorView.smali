.class public Lcom/rd/PageIndicatorView;
.super Landroid/view/View;
.source "PageIndicatorView.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# static fields
.field private static final COUNT_NOT_SET:I = -0x1

.field private static final DEFAULT_CIRCLES_COUNT:I = 0x3

.field private static final DEFAULT_PADDING_DP:I = 0x8

.field private static final DEFAULT_RADIUS_DP:I = 0x6


# instance fields
.field private animation:Lcom/rd/animation/ValueAnimation;

.field private animationDuration:J

.field private animationType:Lcom/rd/animation/AnimationType;

.field private count:I

.field private dynamicCount:Z

.field private fillPaint:Landroid/graphics/Paint;

.field private frameColor:I

.field private frameColorReverse:I

.field private frameHeight:I

.field private frameLeftX:I

.field private frameRadiusPx:I

.field private frameRadiusReversePx:I

.field private frameRightX:I

.field private frameStrokePx:I

.field private frameStrokeReversePx:I

.field private frameX:I

.field private frameY:I

.field private interactiveAnimation:Z

.field private isCountSet:Z

.field private isFrameValuesSet:Z

.field private lastSelectedPosition:I

.field private paddingPx:I

.field private radiusPx:I

.field private rect:Landroid/graphics/RectF;

.field private rtlMode:Lcom/rd/RtlMode;

.field private scaleFactor:F

.field private selectedColor:I

.field private selectedPosition:I

.field private selectingPosition:I

.field private setObserver:Landroid/database/DataSetObserver;

.field private strokePaint:Landroid/graphics/Paint;

.field private strokePx:I

.field private unselectedColor:I

.field private viewPager:Landroid/support/v4/view/ViewPager;

.field private viewPagerId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    .line 81
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    .line 83
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 88
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->init(Landroid/util/AttributeSet;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 96
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    .line 81
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    .line 83
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 88
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 97
    invoke-direct {p0, p2}, Lcom/rd/PageIndicatorView;->init(Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I

    .prologue
    .line 101
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    .line 81
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    .line 83
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 88
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 102
    invoke-direct {p0, p2}, Lcom/rd/PageIndicatorView;->init(Landroid/util/AttributeSet;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyleAttr"    # I
    .param p4, "defStyleRes"    # I
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 107
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    .line 81
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    .line 83
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 88
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 108
    invoke-direct {p0, p2}, Lcom/rd/PageIndicatorView;->init(Landroid/util/AttributeSet;)V

    .line 109
    return-void
.end method

.method static synthetic access$002(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    return p1
.end method

.method static synthetic access$1002(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameY:I

    return p1
.end method

.method static synthetic access$102(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    return p1
.end method

.method static synthetic access$1100(Lcom/rd/PageIndicatorView;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    return p1
.end method

.method static synthetic access$1300(Lcom/rd/PageIndicatorView;)I
    .locals 1
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 26
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    return v0
.end method

.method static synthetic access$1302(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    return p1
.end method

.method static synthetic access$1402(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    return p1
.end method

.method static synthetic access$1500(Lcom/rd/PageIndicatorView;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->endAnimation()V

    return-void
.end method

.method static synthetic access$1600(Lcom/rd/PageIndicatorView;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->resetFrameValues()V

    return-void
.end method

.method static synthetic access$202(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    return p1
.end method

.method static synthetic access$302(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    return p1
.end method

.method static synthetic access$402(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameX:I

    return p1
.end method

.method static synthetic access$502(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameLeftX:I

    return p1
.end method

.method static synthetic access$602(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameRightX:I

    return p1
.end method

.method static synthetic access$702(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameHeight:I

    return p1
.end method

.method static synthetic access$802(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameStrokePx:I

    return p1
.end method

.method static synthetic access$902(Lcom/rd/PageIndicatorView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/PageIndicatorView;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/rd/PageIndicatorView;->frameStrokeReversePx:I

    return p1
.end method

.method private drawCircle(Landroid/graphics/Canvas;III)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 678
    iget-boolean v5, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-nez v5, :cond_2

    iget v5, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-eq p2, v5, :cond_0

    iget v5, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-ne p2, v5, :cond_2

    :cond_0
    move v1, v3

    .line 679
    .local v1, "selectedItem":Z
    :goto_0
    iget-boolean v5, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v5, :cond_3

    iget v5, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-eq p2, v5, :cond_1

    iget v5, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v5, :cond_3

    :cond_1
    move v2, v3

    .line 680
    .local v2, "selectingItem":Z
    :goto_1
    or-int v0, v1, v2

    .line 682
    .local v0, "isSelectedItem":Z
    if-eqz v0, :cond_4

    .line 683
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithAnimationEffect(Landroid/graphics/Canvas;III)V

    .line 687
    :goto_2
    return-void

    .end local v0    # "isSelectedItem":Z
    .end local v1    # "selectedItem":Z
    .end local v2    # "selectingItem":Z
    :cond_2
    move v1, v4

    .line 678
    goto :goto_0

    .restart local v1    # "selectedItem":Z
    :cond_3
    move v2, v4

    .line 679
    goto :goto_1

    .line 685
    .restart local v0    # "isSelectedItem":Z
    .restart local v2    # "selectingItem":Z
    :cond_4
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithNoEffect(Landroid/graphics/Canvas;III)V

    goto :goto_2
.end method

.method private drawIndicatorView(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 669
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->getYCoordinate()I

    move-result v2

    .line 671
    .local v2, "y":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/rd/PageIndicatorView;->count:I

    if-ge v0, v3, :cond_0

    .line 672
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v1

    .line 673
    .local v1, "x":I
    invoke-direct {p0, p1, v0, v1, v2}, Lcom/rd/PageIndicatorView;->drawCircle(Landroid/graphics/Canvas;III)V

    .line 671
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 675
    .end local v1    # "x":I
    :cond_0
    return-void
.end method

.method private drawWithAnimationEffect(Landroid/graphics/Canvas;III)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 690
    sget-object v0, Lcom/rd/PageIndicatorView$3;->$SwitchMap$com$rd$animation$AnimationType:[I

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    invoke-virtual {v1}, Lcom/rd/animation/AnimationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 727
    :goto_0
    return-void

    .line 692
    :pswitch_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithNoEffect(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 696
    :pswitch_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithColorAnimation(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 700
    :pswitch_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithScaleAnimation(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 704
    :pswitch_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithSlideAnimation(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 708
    :pswitch_4
    invoke-direct {p0, p1, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithWormAnimation(Landroid/graphics/Canvas;II)V

    goto :goto_0

    .line 712
    :pswitch_5
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithFillAnimation(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 716
    :pswitch_6
    invoke-direct {p0, p1, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithThinWormAnimation(Landroid/graphics/Canvas;II)V

    goto :goto_0

    .line 720
    :pswitch_7
    invoke-direct {p0, p1, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithDropAnimation(Landroid/graphics/Canvas;II)V

    goto :goto_0

    .line 724
    :pswitch_8
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/PageIndicatorView;->drawWithSwapAnimation(Landroid/graphics/Canvas;III)V

    goto :goto_0

    .line 690
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private drawWithColorAnimation(Landroid/graphics/Canvas;III)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 753
    iget v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 755
    .local v0, "color":I
    iget-boolean v1, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v1, :cond_2

    .line 756
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-ne p2, v1, :cond_1

    .line 757
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    .line 772
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 773
    int-to-float v1, p3

    int-to-float v2, p4

    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 774
    return-void

    .line 759
    :cond_1
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v1, :cond_0

    .line 760
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    goto :goto_0

    .line 764
    :cond_2
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v1, :cond_3

    .line 765
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    goto :goto_0

    .line 767
    :cond_3
    iget v1, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-ne p2, v1, :cond_0

    .line 768
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    goto :goto_0
.end method

.method private drawWithDropAnimation(Landroid/graphics/Canvas;II)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 898
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 899
    int-to-float v0, p2

    int-to-float v1, p3

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 901
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 902
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    int-to-float v0, v0

    iget v1, p0, Lcom/rd/PageIndicatorView;->frameY:I

    int-to-float v1, v1

    iget v2, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 903
    return-void
.end method

.method private drawWithFillAnimation(Landroid/graphics/Canvas;III)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 840
    iget v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 841
    .local v0, "color":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v1, v3

    .line 842
    .local v1, "radius":F
    iget v2, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 844
    .local v2, "stroke":I
    iget-boolean v3, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v3, :cond_2

    .line 845
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-ne p2, v3, :cond_1

    .line 846
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    .line 847
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    int-to-float v1, v3

    .line 848
    iget v2, p0, Lcom/rd/PageIndicatorView;->frameStrokePx:I

    .line 869
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 870
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    iget v4, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 871
    int-to-float v3, p3

    int-to-float v4, p4

    iget v5, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 873
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    int-to-float v4, v2

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 874
    int-to-float v3, p3

    int-to-float v4, p4

    iget-object v5, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v1, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 875
    return-void

    .line 850
    :cond_1
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v3, :cond_0

    .line 851
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    .line 852
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    int-to-float v1, v3

    .line 853
    iget v2, p0, Lcom/rd/PageIndicatorView;->frameStrokeReversePx:I

    goto :goto_0

    .line 857
    :cond_2
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v3, :cond_3

    .line 858
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    .line 859
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    int-to-float v1, v3

    .line 860
    iget v2, p0, Lcom/rd/PageIndicatorView;->frameStrokePx:I

    goto :goto_0

    .line 862
    :cond_3
    iget v3, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-ne p2, v3, :cond_0

    .line 863
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    .line 864
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    int-to-float v1, v3

    .line 865
    iget v2, p0, Lcom/rd/PageIndicatorView;->frameStrokeReversePx:I

    goto :goto_0
.end method

.method private drawWithNoEffect(Landroid/graphics/Canvas;III)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 730
    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v3

    .line 731
    .local v2, "radius":F
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v4, Lcom/rd/animation/AnimationType;->SCALE:Lcom/rd/animation/AnimationType;

    if-ne v3, v4, :cond_0

    .line 732
    iget v3, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    mul-float/2addr v2, v3

    .line 735
    :cond_0
    iget v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 736
    .local v0, "color":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v3, :cond_1

    .line 737
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    .line 741
    :cond_1
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v4, Lcom/rd/animation/AnimationType;->FILL:Lcom/rd/animation/AnimationType;

    if-ne v3, v4, :cond_2

    .line 742
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    .line 743
    .local v1, "paint":Landroid/graphics/Paint;
    iget v3, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 748
    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 749
    int-to-float v3, p3

    int-to-float v4, p4

    invoke-virtual {p1, v3, v4, v2, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 750
    return-void

    .line 745
    .end local v1    # "paint":Landroid/graphics/Paint;
    :cond_2
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    .restart local v1    # "paint":Landroid/graphics/Paint;
    goto :goto_0
.end method

.method private drawWithScaleAnimation(Landroid/graphics/Canvas;III)V
    .locals 6
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 777
    iget v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 778
    .local v0, "color":I
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 780
    .local v1, "radius":I
    iget-boolean v2, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v2, :cond_2

    .line 781
    iget v2, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-ne p2, v2, :cond_1

    .line 782
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    .line 783
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    .line 801
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 802
    int-to-float v2, p3

    int-to-float v3, p4

    int-to-float v4, v1

    iget-object v5, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 803
    return-void

    .line 785
    :cond_1
    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v2, :cond_0

    .line 786
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    .line 787
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    goto :goto_0

    .line 791
    :cond_2
    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v2, :cond_3

    .line 792
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    .line 793
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    goto :goto_0

    .line 795
    :cond_3
    iget v2, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-ne p2, v2, :cond_0

    .line 796
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    .line 797
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    goto :goto_0
.end method

.method private drawWithSlideAnimation(Landroid/graphics/Canvas;III)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 806
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 807
    int-to-float v0, p3

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 809
    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-eq p2, v0, :cond_0

    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v0, :cond_2

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 811
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    int-to-float v0, v0

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 817
    :cond_1
    :goto_0
    return-void

    .line 813
    :cond_2
    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-nez v0, :cond_1

    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-eq p2, v0, :cond_3

    iget v0, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-ne p2, v0, :cond_1

    .line 814
    :cond_3
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 815
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    int-to-float v0, v0

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawWithSwapAnimation(Landroid/graphics/Canvas;III)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 906
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 908
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne p2, v0, :cond_0

    .line 909
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 910
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    int-to-float v0, v0

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 921
    :goto_0
    return-void

    .line 912
    :cond_0
    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    if-ne p2, v0, :cond_1

    .line 913
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v1}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v0, p3, v0

    int-to-float v0, v0

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 915
    :cond_1
    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-nez v0, :cond_2

    .line 916
    iget v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v1}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v1

    sub-int/2addr v0, v1

    sub-int v0, p3, v0

    int-to-float v0, v0

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 919
    :cond_2
    int-to-float v0, p3

    int-to-float v1, p4

    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private drawWithThinWormAnimation(Landroid/graphics/Canvas;II)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 878
    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 880
    .local v2, "radius":I
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameLeftX:I

    .line 881
    .local v1, "left":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRightX:I

    .line 882
    .local v3, "right":I
    iget v5, p0, Lcom/rd/PageIndicatorView;->frameHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int v4, p3, v5

    .line 883
    .local v4, "top":I
    iget v5, p0, Lcom/rd/PageIndicatorView;->frameHeight:I

    div-int/lit8 v5, v5, 0x2

    add-int v0, p3, v5

    .line 885
    .local v0, "bot":I
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v1

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 886
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v3

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 887
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v4

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 888
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v0

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 890
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 891
    int-to-float v5, p2

    int-to-float v6, p3

    int-to-float v7, v2

    iget-object v8, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 893
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 894
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v6, v6

    iget v7, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 895
    return-void
.end method

.method private drawWithWormAnimation(Landroid/graphics/Canvas;II)V
    .locals 9
    .param p1, "canvas"    # Landroid/graphics/Canvas;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 820
    iget v2, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 822
    .local v2, "radius":I
    iget v1, p0, Lcom/rd/PageIndicatorView;->frameLeftX:I

    .line 823
    .local v1, "left":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->frameRightX:I

    .line 824
    .local v3, "right":I
    sub-int v4, p3, v2

    .line 825
    .local v4, "top":I
    add-int v0, p3, v2

    .line 827
    .local v0, "bot":I
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v1

    iput v6, v5, Landroid/graphics/RectF;->left:F

    .line 828
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v3

    iput v6, v5, Landroid/graphics/RectF;->right:F

    .line 829
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v4

    iput v6, v5, Landroid/graphics/RectF;->top:F

    .line 830
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    int-to-float v6, v0

    iput v6, v5, Landroid/graphics/RectF;->bottom:F

    .line 832
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 833
    int-to-float v5, p2

    int-to-float v6, p3

    int-to-float v7, v2

    iget-object v8, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 835
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    iget v6, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 836
    iget-object v5, p0, Lcom/rd/PageIndicatorView;->rect:Landroid/graphics/RectF;

    iget v6, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v6, v6

    iget v7, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v8}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 837
    return-void
.end method

.method private endAnimation()V
    .locals 3

    .prologue
    .line 1297
    const/4 v0, 0x0

    .line 1299
    .local v0, "anim":Lcom/rd/animation/AbsAnimation;
    sget-object v1, Lcom/rd/PageIndicatorView$3;->$SwitchMap$com$rd$animation$AnimationType:[I

    iget-object v2, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    invoke-virtual {v2}, Lcom/rd/animation/AnimationType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1333
    :goto_0
    if-eqz v0, :cond_0

    .line 1334
    invoke-virtual {v0}, Lcom/rd/animation/AbsAnimation;->end()V

    .line 1336
    :cond_0
    return-void

    .line 1301
    :pswitch_0
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->color()Lcom/rd/animation/ColorAnimation;

    move-result-object v0

    .line 1302
    goto :goto_0

    .line 1305
    :pswitch_1
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->slide()Lcom/rd/animation/SlideAnimation;

    move-result-object v0

    .line 1306
    goto :goto_0

    .line 1309
    :pswitch_2
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->scale()Lcom/rd/animation/ScaleAnimation;

    move-result-object v0

    .line 1310
    goto :goto_0

    .line 1313
    :pswitch_3
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->worm()Lcom/rd/animation/WormAnimation;

    move-result-object v0

    .line 1314
    goto :goto_0

    .line 1317
    :pswitch_4
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->thinWorm()Lcom/rd/animation/ThinWormAnimation;

    move-result-object v0

    .line 1318
    goto :goto_0

    .line 1321
    :pswitch_5
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->fill()Lcom/rd/animation/FillAnimation;

    move-result-object v0

    .line 1322
    goto :goto_0

    .line 1325
    :pswitch_6
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->drop()Lcom/rd/animation/DropAnimation;

    move-result-object v0

    .line 1326
    goto :goto_0

    .line 1329
    :pswitch_7
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v1}, Lcom/rd/animation/ValueAnimation;->swap()Lcom/rd/animation/SwapAnimation;

    move-result-object v0

    goto :goto_0

    .line 1299
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private findViewPager()V
    .locals 4

    .prologue
    .line 1358
    iget v3, p0, Lcom/rd/PageIndicatorView;->viewPagerId:I

    if-nez v3, :cond_1

    .line 1371
    :cond_0
    :goto_0
    return-void

    .line 1362
    :cond_1
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1363
    .local v1, "context":Landroid/content/Context;
    instance-of v3, v1, Landroid/app/Activity;

    if-eqz v3, :cond_0

    .line 1364
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 1365
    .local v0, "activity":Landroid/app/Activity;
    iget v3, p0, Lcom/rd/PageIndicatorView;->viewPagerId:I

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1367
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    instance-of v3, v2, Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_0

    .line 1368
    check-cast v2, Landroid/support/v4/view/ViewPager;

    .end local v2    # "view":Landroid/view/View;
    invoke-virtual {p0, v2}, Lcom/rd/PageIndicatorView;->setViewPager(Landroid/support/v4/view/ViewPager;)V

    goto :goto_0
.end method

.method private getAnimationType(I)Lcom/rd/animation/AnimationType;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1080
    packed-switch p1, :pswitch_data_0

    .line 1101
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    :goto_0
    return-object v0

    .line 1082
    :pswitch_0
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1084
    :pswitch_1
    sget-object v0, Lcom/rd/animation/AnimationType;->COLOR:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1086
    :pswitch_2
    sget-object v0, Lcom/rd/animation/AnimationType;->SCALE:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1088
    :pswitch_3
    sget-object v0, Lcom/rd/animation/AnimationType;->WORM:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1090
    :pswitch_4
    sget-object v0, Lcom/rd/animation/AnimationType;->SLIDE:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1092
    :pswitch_5
    sget-object v0, Lcom/rd/animation/AnimationType;->FILL:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1094
    :pswitch_6
    sget-object v0, Lcom/rd/animation/AnimationType;->THIN_WORM:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1096
    :pswitch_7
    sget-object v0, Lcom/rd/animation/AnimationType;->DROP:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1098
    :pswitch_8
    sget-object v0, Lcom/rd/animation/AnimationType;->SWAP:Lcom/rd/animation/AnimationType;

    goto :goto_0

    .line 1080
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private getProgress(IF)Landroid/util/Pair;
    .locals 11
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IF)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 1400
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isRtl()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1401
    iget v7, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v7, v7, -0x1

    sub-int p1, v7, p1

    .line 1403
    if-gez p1, :cond_0

    .line 1404
    const/4 p1, 0x0

    .line 1408
    :cond_0
    iget v7, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-le p1, v7, :cond_4

    move v1, v5

    .line 1410
    .local v1, "isRightOverScrolled":Z
    :goto_0
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isRtl()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 1411
    add-int/lit8 v7, p1, -0x1

    iget v8, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ge v7, v8, :cond_5

    move v0, v5

    .line 1416
    .local v0, "isLeftOverScrolled":Z
    :goto_1
    if-nez v1, :cond_1

    if-eqz v0, :cond_2

    .line 1417
    :cond_1
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 1420
    :cond_2
    iget v7, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ne v7, p1, :cond_8

    cmpl-float v7, p2, v9

    if-eqz v7, :cond_8

    move v2, v5

    .line 1424
    .local v2, "isSlideToRightSide":Z
    :goto_2
    if-eqz v2, :cond_a

    .line 1425
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isRtl()Z

    move-result v5

    if-eqz v5, :cond_9

    add-int/lit8 v3, p1, -0x1

    .line 1426
    .local v3, "selectingPosition":I
    :goto_3
    move v4, p2

    .line 1433
    .local v4, "selectingProgress":F
    :goto_4
    cmpl-float v5, v4, v10

    if-lez v5, :cond_b

    .line 1434
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1440
    :cond_3
    :goto_5
    new-instance v5, Landroid/util/Pair;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v5

    .end local v0    # "isLeftOverScrolled":Z
    .end local v1    # "isRightOverScrolled":Z
    .end local v2    # "isSlideToRightSide":Z
    .end local v3    # "selectingPosition":I
    .end local v4    # "selectingProgress":F
    :cond_4
    move v1, v6

    .line 1408
    goto :goto_0

    .restart local v1    # "isRightOverScrolled":Z
    :cond_5
    move v0, v6

    .line 1411
    goto :goto_1

    .line 1413
    :cond_6
    add-int/lit8 v7, p1, 0x1

    iget v8, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-ge v7, v8, :cond_7

    move v0, v5

    .restart local v0    # "isLeftOverScrolled":Z
    :goto_6
    goto :goto_1

    .end local v0    # "isLeftOverScrolled":Z
    :cond_7
    move v0, v6

    goto :goto_6

    .restart local v0    # "isLeftOverScrolled":Z
    :cond_8
    move v2, v6

    .line 1420
    goto :goto_2

    .line 1425
    .restart local v2    # "isSlideToRightSide":Z
    :cond_9
    add-int/lit8 v3, p1, 0x1

    goto :goto_3

    .line 1429
    :cond_a
    move v3, p1

    .line 1430
    .restart local v3    # "selectingPosition":I
    sub-float v4, v10, p2

    .restart local v4    # "selectingProgress":F
    goto :goto_4

    .line 1436
    :cond_b
    cmpg-float v5, v4, v9

    if-gez v5, :cond_3

    .line 1437
    const/4 v4, 0x0

    goto :goto_5
.end method

.method private getRtlMode(I)Lcom/rd/RtlMode;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 1105
    packed-switch p1, :pswitch_data_0

    .line 1114
    sget-object v0, Lcom/rd/RtlMode;->Auto:Lcom/rd/RtlMode;

    :goto_0
    return-object v0

    .line 1107
    :pswitch_0
    sget-object v0, Lcom/rd/RtlMode;->On:Lcom/rd/RtlMode;

    goto :goto_0

    .line 1109
    :pswitch_1
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    goto :goto_0

    .line 1111
    :pswitch_2
    sget-object v0, Lcom/rd/RtlMode;->Auto:Lcom/rd/RtlMode;

    goto :goto_0

    .line 1105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getViewPagerCount()I
    .locals 1

    .prologue
    .line 1350
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1351
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    .line 1353
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    goto :goto_0
.end method

.method private getXCoordinate(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1375
    const/4 v1, 0x0

    .line 1376
    .local v1, "x":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Lcom/rd/PageIndicatorView;->count:I

    if-ge v0, v3, :cond_1

    .line 1377
    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 1379
    if-ne p1, v0, :cond_0

    move v2, v1

    .line 1386
    .end local v1    # "x":I
    .local v2, "x":I
    :goto_1
    return v2

    .line 1383
    .end local v2    # "x":I
    .restart local v1    # "x":I
    :cond_0
    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    add-int/2addr v3, v4

    add-int/2addr v1, v3

    .line 1376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 1386
    .end local v1    # "x":I
    .restart local v2    # "x":I
    goto :goto_1
.end method

.method private getYCoordinate()I
    .locals 3

    .prologue
    .line 1390
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getHeight()I

    move-result v1

    div-int/lit8 v0, v1, 0x2

    .line 1392
    .local v0, "y":I
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v2, Lcom/rd/animation/AnimationType;->DROP:Lcom/rd/animation/AnimationType;

    if-ne v1, v2, :cond_0

    .line 1393
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    add-int/2addr v0, v1

    .line 1396
    :cond_0
    return v0
.end method

.method private init(Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 924
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->setupId()V

    .line 925
    invoke-direct {p0, p1}, Lcom/rd/PageIndicatorView;->initAttributes(Landroid/util/AttributeSet;)V

    .line 926
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->initAnimation()V

    .line 928
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 929
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->fillPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 931
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 932
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 933
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->strokePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 934
    return-void
.end method

.method private initAnimation()V
    .locals 2

    .prologue
    .line 1011
    new-instance v0, Lcom/rd/animation/ValueAnimation;

    new-instance v1, Lcom/rd/PageIndicatorView$1;

    invoke-direct {v1, p0}, Lcom/rd/PageIndicatorView$1;-><init>(Lcom/rd/PageIndicatorView;)V

    invoke-direct {v0, v1}, Lcom/rd/animation/ValueAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    .line 1077
    return-void
.end method

.method private initAnimationAttribute(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "typedArray"    # Landroid/content/res/TypedArray;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 978
    sget v2, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_animationDuration:I

    const/16 v3, 0x15e

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    .line 979
    sget v2, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_interactiveAnimation:I

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    iput-boolean v2, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    .line 981
    sget v2, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_animationType:I

    sget-object v3, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    invoke-virtual {v3}, Lcom/rd/animation/AnimationType;->ordinal()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 982
    .local v0, "animIndex":I
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->getAnimationType(I)Lcom/rd/animation/AnimationType;

    move-result-object v2

    iput-object v2, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 984
    sget v2, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_rtl_mode:I

    sget-object v3, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    invoke-virtual {v3}, Lcom/rd/RtlMode;->ordinal()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 985
    .local v1, "rtlIndex":I
    invoke-direct {p0, v1}, Lcom/rd/PageIndicatorView;->getRtlMode(I)Lcom/rd/RtlMode;

    move-result-object v2

    iput-object v2, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 986
    return-void
.end method

.method private initAttributes(Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "attrs"    # Landroid/util/AttributeSet;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v3, 0x0

    .line 943
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView:[I

    invoke-virtual {v1, p1, v2, v3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 944
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->initCountAttribute(Landroid/content/res/TypedArray;)V

    .line 945
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->initColorAttribute(Landroid/content/res/TypedArray;)V

    .line 946
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->initAnimationAttribute(Landroid/content/res/TypedArray;)V

    .line 947
    invoke-direct {p0, v0}, Lcom/rd/PageIndicatorView;->initSizeAttribute(Landroid/content/res/TypedArray;)V

    .line 948
    return-void
.end method

.method private initColorAttribute(Landroid/content/res/TypedArray;)V
    .locals 2
    .param p1, "typedArray"    # Landroid/content/res/TypedArray;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 973
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_unselectedColor:I

    const-string v1, "#33ffffff"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 974
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_selectedColor:I

    const-string v1, "#ffffff"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    .line 975
    return-void
.end method

.method private initCountAttribute(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "typedArray"    # Landroid/content/res/TypedArray;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 951
    sget v1, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_dynamicCount:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/rd/PageIndicatorView;->dynamicCount:Z

    .line 952
    sget v1, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_count:I

    invoke-virtual {p1, v1, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->count:I

    .line 954
    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    if-eq v1, v3, :cond_1

    .line 955
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/rd/PageIndicatorView;->isCountSet:Z

    .line 960
    :goto_0
    sget v1, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_select:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .line 961
    .local v0, "position":I
    if-gez v0, :cond_2

    .line 962
    const/4 v0, 0x0

    .line 967
    :cond_0
    :goto_1
    iput v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 968
    iput v0, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    .line 969
    sget v1, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_viewPager:I

    invoke-virtual {p1, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->viewPagerId:I

    .line 970
    return-void

    .line 957
    .end local v0    # "position":I
    :cond_1
    const/4 v1, 0x3

    iput v1, p0, Lcom/rd/PageIndicatorView;->count:I

    goto :goto_0

    .line 963
    .restart local v0    # "position":I
    :cond_2
    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v1, v1, -0x1

    if-le v0, v1, :cond_0

    .line 964
    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v0, v1, -0x1

    goto :goto_1
.end method

.method private initSizeAttribute(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "typedArray"    # Landroid/content/res/TypedArray;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    .line 989
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_radius:I

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 990
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_padding:I

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    .line 992
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_scaleFactor:I

    const v1, 0x3f333333    # 0.7f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    .line 993
    iget v0, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    .line 994
    iput v2, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    .line 1000
    :cond_0
    :goto_0
    sget v0, Lcom/rd/pageindicatorview/R$styleable;->PageIndicatorView_piv_strokeWidth:I

    const/4 v1, 0x1

    invoke-static {v1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 1001
    iget v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    if-le v0, v1, :cond_1

    .line 1002
    iget v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iput v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 1005
    :cond_1
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v1, Lcom/rd/animation/AnimationType;->FILL:Lcom/rd/animation/AnimationType;

    if-eq v0, v1, :cond_2

    .line 1006
    const/4 v0, 0x0

    iput v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 1008
    :cond_2
    return-void

    .line 996
    :cond_3
    iget v0, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_0

    .line 997
    iput v3, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    goto :goto_0
.end method

.method private isRtl()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1444
    sget-object v2, Lcom/rd/PageIndicatorView$3;->$SwitchMap$com$rd$RtlMode:[I

    iget-object v3, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    invoke-virtual {v3}, Lcom/rd/RtlMode;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 1455
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 1449
    goto :goto_0

    .line 1452
    :pswitch_2
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v2}, Landroid/support/v4/text/TextUtilsCompat;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v2

    if-eq v2, v0, :cond_0

    move v0, v1

    goto :goto_0

    .line 1444
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isViewMeasured()Z
    .locals 1

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getMeasuredHeight()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getMeasuredWidth()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPageScroll(IF)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F

    .prologue
    .line 656
    invoke-direct {p0, p1, p2}, Lcom/rd/PageIndicatorView;->getProgress(IF)Landroid/util/Pair;

    move-result-object v0

    .line 657
    .local v0, "progressPair":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Float;>;"
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 658
    .local v1, "selectingPosition":I
    iget-object v3, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 660
    .local v2, "selectingProgress":F
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v2, v3

    if-nez v3, :cond_0

    .line 661
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    iput v3, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    .line 662
    iput v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 665
    :cond_0
    invoke-virtual {p0, v1, v2}, Lcom/rd/PageIndicatorView;->setProgress(IF)V

    .line 666
    return-void
.end method

.method private registerSetObserver()V
    .locals 3

    .prologue
    .line 1267
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1268
    new-instance v1, Lcom/rd/PageIndicatorView$2;

    invoke-direct {v1, p0}, Lcom/rd/PageIndicatorView$2;-><init>(Lcom/rd/PageIndicatorView;)V

    iput-object v1, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;

    .line 1289
    :try_start_0
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/PagerAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1294
    :cond_0
    :goto_0
    return-void

    .line 1290
    :catch_0
    move-exception v0

    .line 1291
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method private resetFrameValues()V
    .locals 1

    .prologue
    .line 1118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/rd/PageIndicatorView;->isFrameValuesSet:Z

    .line 1119
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->setupFrameValues()V

    .line 1120
    return-void
.end method

.method private setAnimationProgress(F)Lcom/rd/animation/AbsAnimation;
    .locals 9
    .param p1, "progress"    # F
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 1224
    sget-object v4, Lcom/rd/PageIndicatorView$3;->$SwitchMap$com$rd$animation$AnimationType:[I

    iget-object v5, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    invoke-virtual {v5}, Lcom/rd/animation/AnimationType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1263
    :cond_0
    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 1226
    :pswitch_0
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->color()Lcom/rd/animation/ColorAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v6, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v4, v5, v6}, Lcom/rd/animation/ColorAnimation;->with(II)Lcom/rd/animation/ColorAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/ColorAnimation;->progress(F)Lcom/rd/animation/ColorAnimation;

    move-result-object v4

    goto :goto_0

    .line 1229
    :pswitch_1
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->scale()Lcom/rd/animation/ScaleAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v6, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    iget v7, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v8, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/rd/animation/ScaleAnimation;->with(IIIF)Lcom/rd/animation/ScaleAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/ScaleAnimation;->progress(F)Lcom/rd/animation/ColorAnimation;

    move-result-object v4

    goto :goto_0

    .line 1232
    :pswitch_2
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->fill()Lcom/rd/animation/FillAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v6, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    iget v7, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v8, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/rd/animation/FillAnimation;->with(IIII)Lcom/rd/animation/FillAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/FillAnimation;->progress(F)Lcom/rd/animation/ColorAnimation;

    move-result-object v4

    goto :goto_0

    .line 1239
    :pswitch_3
    iget v4, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v4}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1240
    .local v0, "fromX":I
    iget v4, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    invoke-direct {p0, v4}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v3

    .line 1242
    .local v3, "toX":I
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->SLIDE:Lcom/rd/animation/AnimationType;

    if-ne v4, v5, :cond_1

    .line 1243
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->slide()Lcom/rd/animation/SlideAnimation;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Lcom/rd/animation/SlideAnimation;->with(II)Lcom/rd/animation/SlideAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/SlideAnimation;->progress(F)Lcom/rd/animation/SlideAnimation;

    move-result-object v4

    goto :goto_0

    .line 1245
    :cond_1
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->SWAP:Lcom/rd/animation/AnimationType;

    if-ne v4, v5, :cond_2

    .line 1246
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->swap()Lcom/rd/animation/SwapAnimation;

    move-result-object v4

    invoke-virtual {v4, v0, v3}, Lcom/rd/animation/SwapAnimation;->with(II)Lcom/rd/animation/SwapAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/SwapAnimation;->progress(F)Lcom/rd/animation/SwapAnimation;

    move-result-object v4

    goto :goto_0

    .line 1248
    :cond_2
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->WORM:Lcom/rd/animation/AnimationType;

    if-eq v4, v5, :cond_3

    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->THIN_WORM:Lcom/rd/animation/AnimationType;

    if-ne v4, v5, :cond_6

    .line 1249
    :cond_3
    iget v4, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    iget v5, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    if-le v4, v5, :cond_4

    const/4 v2, 0x1

    .line 1250
    .local v2, "isRightSide":Z
    :goto_1
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->WORM:Lcom/rd/animation/AnimationType;

    if-ne v4, v5, :cond_5

    .line 1251
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->worm()Lcom/rd/animation/WormAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v4, v0, v3, v5, v2}, Lcom/rd/animation/WormAnimation;->with(IIIZ)Lcom/rd/animation/WormAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/WormAnimation;->progress(F)Lcom/rd/animation/WormAnimation;

    move-result-object v4

    goto/16 :goto_0

    .line 1249
    .end local v2    # "isRightSide":Z
    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    .line 1253
    .restart local v2    # "isRightSide":Z
    :cond_5
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v5, Lcom/rd/animation/AnimationType;->THIN_WORM:Lcom/rd/animation/AnimationType;

    if-ne v4, v5, :cond_0

    .line 1254
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->thinWorm()Lcom/rd/animation/ThinWormAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v4, v0, v3, v5, v2}, Lcom/rd/animation/ThinWormAnimation;->with(IIIZ)Lcom/rd/animation/WormAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/WormAnimation;->progress(F)Lcom/rd/animation/WormAnimation;

    move-result-object v4

    goto/16 :goto_0

    .line 1258
    .end local v2    # "isRightSide":Z
    :cond_6
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->getYCoordinate()I

    move-result v1

    .line 1259
    .local v1, "fromY":I
    iget-object v4, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v4}, Lcom/rd/animation/ValueAnimation;->drop()Lcom/rd/animation/DropAnimation;

    move-result-object v4

    iget v5, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v4, v0, v3, v1, v5}, Lcom/rd/animation/DropAnimation;->with(IIII)Lcom/rd/animation/DropAnimation;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/rd/animation/DropAnimation;->progress(F)Lcom/rd/animation/DropAnimation;

    move-result-object v4

    goto/16 :goto_0

    .line 1224
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private setupFrameValues()V
    .locals 3

    .prologue
    .line 1123
    iget-boolean v1, p0, Lcom/rd/PageIndicatorView;->isFrameValuesSet:Z

    if-eqz v1, :cond_0

    .line 1162
    :goto_0
    return-void

    .line 1128
    :cond_0
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameColor:I

    .line 1129
    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameColorReverse:I

    .line 1132
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    .line 1133
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    .line 1136
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v1}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1137
    .local v0, "xCoordinate":I
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    sub-int v1, v0, v1

    if-ltz v1, :cond_2

    .line 1138
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    sub-int v1, v0, v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameLeftX:I

    .line 1139
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRightX:I

    .line 1147
    :goto_1
    iput v0, p0, Lcom/rd/PageIndicatorView;->frameX:I

    .line 1148
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->getYCoordinate()I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameY:I

    .line 1151
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameStrokePx:I

    .line 1152
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameStrokeReversePx:I

    .line 1154
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v2, Lcom/rd/animation/AnimationType;->FILL:Lcom/rd/animation/AnimationType;

    if-ne v1, v2, :cond_1

    .line 1155
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusPx:I

    .line 1156
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRadiusReversePx:I

    .line 1160
    :cond_1
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameHeight:I

    .line 1161
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/rd/PageIndicatorView;->isFrameValuesSet:Z

    goto :goto_0

    .line 1142
    :cond_2
    iput v0, p0, Lcom/rd/PageIndicatorView;->frameLeftX:I

    .line 1143
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v0

    iput v1, p0, Lcom/rd/PageIndicatorView;->frameRightX:I

    goto :goto_1
.end method

.method private setupId()V
    .locals 2

    .prologue
    .line 937
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->getId()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 938
    invoke-static {}, Lcom/rd/Utils;->generateViewId()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/rd/PageIndicatorView;->setId(I)V

    .line 940
    :cond_0
    return-void
.end method

.method private startColorAnimation()V
    .locals 4

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->color()Lcom/rd/animation/ColorAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/ColorAnimation;->end()V

    .line 1166
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->color()Lcom/rd/animation/ColorAnimation;

    move-result-object v0

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    invoke-virtual {v0, v1, v2}, Lcom/rd/animation/ColorAnimation;->with(II)Lcom/rd/animation/ColorAnimation;

    move-result-object v0

    iget-wide v2, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v0, v2, v3}, Lcom/rd/animation/ColorAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/AbsAnimation;->start()V

    .line 1167
    return-void
.end method

.method private startDropAnimation()V
    .locals 6

    .prologue
    .line 1206
    iget v3, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1207
    .local v0, "fromX":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v2

    .line 1208
    .local v2, "toX":I
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->getYCoordinate()I

    move-result v1

    .line 1210
    .local v1, "fromY":I
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->drop()Lcom/rd/animation/DropAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/DropAnimation;->end()V

    .line 1211
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->drop()Lcom/rd/animation/DropAnimation;

    move-result-object v3

    iget-wide v4, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v3, v4, v5}, Lcom/rd/animation/DropAnimation;->duration(J)Lcom/rd/animation/DropAnimation;

    move-result-object v3

    iget v4, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v3, v0, v2, v1, v4}, Lcom/rd/animation/DropAnimation;->with(IIII)Lcom/rd/animation/DropAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/DropAnimation;->start()V

    .line 1212
    return-void
.end method

.method private startFillAnimation()V
    .locals 5

    .prologue
    .line 1192
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->fill()Lcom/rd/animation/FillAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/FillAnimation;->end()V

    .line 1193
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->fill()Lcom/rd/animation/FillAnimation;

    move-result-object v0

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/rd/animation/FillAnimation;->with(IIII)Lcom/rd/animation/FillAnimation;

    move-result-object v0

    iget-wide v2, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v0, v2, v3}, Lcom/rd/animation/FillAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/AbsAnimation;->start()V

    .line 1194
    return-void
.end method

.method private startScaleAnimation()V
    .locals 5

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->scale()Lcom/rd/animation/ScaleAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/ScaleAnimation;->end()V

    .line 1171
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v0}, Lcom/rd/animation/ValueAnimation;->scale()Lcom/rd/animation/ScaleAnimation;

    move-result-object v0

    iget v1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    iget v3, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/rd/animation/ScaleAnimation;->with(IIIF)Lcom/rd/animation/ScaleAnimation;

    move-result-object v0

    iget-wide v2, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v0, v2, v3}, Lcom/rd/animation/ScaleAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/rd/animation/AbsAnimation;->start()V

    .line 1172
    return-void
.end method

.method private startSlideAnimation()V
    .locals 6

    .prologue
    .line 1175
    iget v2, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-direct {p0, v2}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1176
    .local v0, "fromX":I
    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v2}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v1

    .line 1178
    .local v1, "toX":I
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v2}, Lcom/rd/animation/ValueAnimation;->slide()Lcom/rd/animation/SlideAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/rd/animation/SlideAnimation;->end()V

    .line 1179
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v2}, Lcom/rd/animation/ValueAnimation;->slide()Lcom/rd/animation/SlideAnimation;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/rd/animation/SlideAnimation;->with(II)Lcom/rd/animation/SlideAnimation;

    move-result-object v2

    iget-wide v4, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v2, v4, v5}, Lcom/rd/animation/SlideAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/rd/animation/AbsAnimation;->start()V

    .line 1180
    return-void
.end method

.method private startSwapAnimation()V
    .locals 6

    .prologue
    .line 1215
    iget v2, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-direct {p0, v2}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1216
    .local v0, "fromX":I
    iget v2, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v2}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v1

    .line 1218
    .local v1, "toX":I
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v2}, Lcom/rd/animation/ValueAnimation;->swap()Lcom/rd/animation/SwapAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/rd/animation/SwapAnimation;->end()V

    .line 1219
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v2}, Lcom/rd/animation/ValueAnimation;->swap()Lcom/rd/animation/SwapAnimation;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/rd/animation/SwapAnimation;->with(II)Lcom/rd/animation/SwapAnimation;

    move-result-object v2

    iget-wide v4, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v2, v4, v5}, Lcom/rd/animation/SwapAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/rd/animation/AbsAnimation;->start()V

    .line 1220
    return-void
.end method

.method private startThinWormAnimation()V
    .locals 6

    .prologue
    .line 1197
    iget v3, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1198
    .local v0, "fromX":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v2

    .line 1199
    .local v2, "toX":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-le v3, v4, :cond_0

    const/4 v1, 0x1

    .line 1201
    .local v1, "isRightSide":Z
    :goto_0
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->thinWorm()Lcom/rd/animation/ThinWormAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/ThinWormAnimation;->end()V

    .line 1202
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->thinWorm()Lcom/rd/animation/ThinWormAnimation;

    move-result-object v3

    iget-wide v4, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v3, v4, v5}, Lcom/rd/animation/ThinWormAnimation;->duration(J)Lcom/rd/animation/ThinWormAnimation;

    move-result-object v3

    iget v4, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v3, v0, v2, v4, v1}, Lcom/rd/animation/ThinWormAnimation;->with(IIIZ)Lcom/rd/animation/WormAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/WormAnimation;->start()V

    .line 1203
    return-void

    .line 1199
    .end local v1    # "isRightSide":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private startWormAnimation()V
    .locals 6

    .prologue
    .line 1183
    iget v3, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v0

    .line 1184
    .local v0, "fromX":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-direct {p0, v3}, Lcom/rd/PageIndicatorView;->getXCoordinate(I)I

    move-result v2

    .line 1185
    .local v2, "toX":I
    iget v3, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    iget v4, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    if-le v3, v4, :cond_0

    const/4 v1, 0x1

    .line 1187
    .local v1, "isRightSide":Z
    :goto_0
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->worm()Lcom/rd/animation/WormAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/WormAnimation;->end()V

    .line 1188
    iget-object v3, p0, Lcom/rd/PageIndicatorView;->animation:Lcom/rd/animation/ValueAnimation;

    invoke-virtual {v3}, Lcom/rd/animation/ValueAnimation;->worm()Lcom/rd/animation/WormAnimation;

    move-result-object v3

    iget-wide v4, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    invoke-virtual {v3, v4, v5}, Lcom/rd/animation/WormAnimation;->duration(J)Lcom/rd/animation/WormAnimation;

    move-result-object v3

    iget v4, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    invoke-virtual {v3, v0, v2, v4, v1}, Lcom/rd/animation/WormAnimation;->with(IIIZ)Lcom/rd/animation/WormAnimation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/rd/animation/WormAnimation;->start()V

    .line 1189
    return-void

    .line 1185
    .end local v1    # "isRightSide":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private unRegisterSetObserver()V
    .locals 3

    .prologue
    .line 1339
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1341
    :try_start_0
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/PagerAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 1342
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/rd/PageIndicatorView;->setObserver:Landroid/database/DataSetObserver;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1347
    :cond_0
    :goto_0
    return-void

    .line 1343
    :catch_0
    move-exception v0

    .line 1344
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getAnimationDuration()J
    .locals 2

    .prologue
    .line 484
    iget-wide v0, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    return-wide v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    return v0
.end method

.method public getPadding()I
    .locals 1

    .prologue
    .line 353
    iget v0, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    return v0
.end method

.method public getRadius()I
    .locals 1

    .prologue
    .line 317
    iget v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    return v0
.end method

.method public getScaleFactor()F
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    return v0
.end method

.method public getSelectedColor()I
    .locals 1

    .prologue
    .line 465
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    return v0
.end method

.method public getSelection()I
    .locals 1

    .prologue
    .line 599
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    return v0
.end method

.method public getStrokeWidth()I
    .locals 1

    .prologue
    .line 429
    iget v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    return v0
.end method

.method public getUnselectedColor()I
    .locals 1

    .prologue
    .line 447
    iget v0, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 113
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 114
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->findViewPager()V

    .line 115
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->unRegisterSetObserver()V

    .line 120
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 121
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/rd/PageIndicatorView;->drawIndicatorView(Landroid/graphics/Canvas;)V

    .line 213
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 206
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 207
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->setupFrameValues()V

    .line 208
    return-void
.end method

.method protected onMeasure(II)V
    .locals 14
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 149
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 150
    .local v10, "widthMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v11

    .line 152
    .local v11, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 153
    .local v5, "heightMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 155
    .local v6, "heightSize":I
    iget v12, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    mul-int/lit8 v0, v12, 0x2

    .line 156
    .local v0, "circleDiameterPx":I
    iget v12, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    add-int v1, v0, v12

    .line 157
    .local v1, "desiredHeight":I
    const/4 v2, 0x0

    .line 159
    .local v2, "desiredWidth":I
    iget v12, p0, Lcom/rd/PageIndicatorView;->count:I

    if-eqz v12, :cond_0

    .line 160
    iget v12, p0, Lcom/rd/PageIndicatorView;->count:I

    mul-int v3, v0, v12

    .line 161
    .local v3, "diameterSum":I
    iget v12, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    mul-int/lit8 v12, v12, 0x2

    iget v13, p0, Lcom/rd/PageIndicatorView;->count:I

    mul-int v8, v12, v13

    .line 162
    .local v8, "strokeSum":I
    iget v12, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    iget v13, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v13, v13, -0x1

    mul-int v7, v12, v13

    .line 163
    .local v7, "paddingSum":I
    add-int v12, v3, v8

    add-int v2, v12, v7

    .line 169
    .end local v3    # "diameterSum":I
    .end local v7    # "paddingSum":I
    .end local v8    # "strokeSum":I
    :cond_0
    const/high16 v12, 0x40000000    # 2.0f

    if-ne v10, v12, :cond_4

    .line 170
    move v9, v11

    .line 177
    .local v9, "width":I
    :goto_0
    const/high16 v12, 0x40000000    # 2.0f

    if-ne v5, v12, :cond_6

    .line 178
    move v4, v6

    .line 185
    .local v4, "height":I
    :goto_1
    iget-object v12, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v13, Lcom/rd/animation/AnimationType;->DROP:Lcom/rd/animation/AnimationType;

    if-ne v12, v13, :cond_1

    .line 186
    mul-int/lit8 v4, v4, 0x2

    .line 189
    :cond_1
    if-gez v9, :cond_2

    .line 190
    const/4 v9, 0x0

    .line 193
    :cond_2
    if-gez v4, :cond_3

    .line 194
    const/4 v4, 0x0

    .line 197
    :cond_3
    invoke-virtual {p0, v9, v4}, Lcom/rd/PageIndicatorView;->setMeasuredDimension(II)V

    .line 198
    return-void

    .line 171
    .end local v4    # "height":I
    .end local v9    # "width":I
    :cond_4
    const/high16 v12, -0x80000000

    if-ne v10, v12, :cond_5

    .line 172
    invoke-static {v2, v11}, Ljava/lang/Math;->min(II)I

    move-result v9

    .restart local v9    # "width":I
    goto :goto_0

    .line 174
    .end local v9    # "width":I
    :cond_5
    move v9, v2

    .restart local v9    # "width":I
    goto :goto_0

    .line 179
    :cond_6
    const/high16 v12, -0x80000000

    if-ne v5, v12, :cond_7

    .line 180
    invoke-static {v1, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    .restart local v4    # "height":I
    goto :goto_1

    .line 182
    .end local v4    # "height":I
    :cond_7
    move v4, v1

    .restart local v4    # "height":I
    goto :goto_1
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 242
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isViewMeasured()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v0, :cond_0

    .line 218
    invoke-direct {p0, p1, p2}, Lcom/rd/PageIndicatorView;->onPageScroll(IF)V

    .line 220
    :cond_0
    return-void
.end method

.method public onPageSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 224
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v0

    .line 227
    .local v0, "pageCount":I
    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    if-ge v0, v1, :cond_1

    .line 239
    .end local v0    # "pageCount":I
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isViewMeasured()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    sget-object v2, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    if-ne v1, v2, :cond_0

    .line 233
    :cond_2
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isRtl()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 234
    iget v1, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v1, v1, -0x1

    sub-int p1, v1, p1

    .line 237
    :cond_3
    invoke-virtual {p0, p1}, Lcom/rd/PageIndicatorView;->setSelection(I)V

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 135
    instance-of v1, p1, Lcom/rd/PositionSavedState;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 136
    check-cast v0, Lcom/rd/PositionSavedState;

    .line 137
    .local v0, "positionSavedState":Lcom/rd/PositionSavedState;
    invoke-virtual {v0}, Lcom/rd/PositionSavedState;->getSelectedPosition()I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 138
    invoke-virtual {v0}, Lcom/rd/PositionSavedState;->getSelectingPosition()I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    .line 139
    invoke-virtual {v0}, Lcom/rd/PositionSavedState;->getLastSelectedPosition()I

    move-result v1

    iput v1, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    .line 140
    invoke-virtual {v0}, Lcom/rd/PositionSavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 144
    .end local v0    # "positionSavedState":Lcom/rd/PositionSavedState;
    :goto_0
    return-void

    .line 142
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 125
    new-instance v0, Lcom/rd/PositionSavedState;

    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/rd/PositionSavedState;-><init>(Landroid/os/Parcelable;)V

    .line 126
    .local v0, "positionSavedState":Lcom/rd/PositionSavedState;
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    invoke-virtual {v0, v1}, Lcom/rd/PositionSavedState;->setSelectedPosition(I)V

    .line 127
    iget v1, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    invoke-virtual {v0, v1}, Lcom/rd/PositionSavedState;->setSelectingPosition(I)V

    .line 128
    iget v1, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    invoke-virtual {v0, v1}, Lcom/rd/PositionSavedState;->setLastSelectedPosition(I)V

    .line 130
    return-object v0
.end method

.method public releaseViewPager()V
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->removeOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 635
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 637
    :cond_0
    return-void
.end method

.method public setAnimationDuration(J)V
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 476
    iput-wide p1, p0, Lcom/rd/PageIndicatorView;->animationDuration:J

    .line 477
    return-void
.end method

.method public setAnimationType(Lcom/rd/animation/AnimationType;)V
    .locals 1
    .param p1, "type"    # Lcom/rd/animation/AnimationType;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 494
    if-eqz p1, :cond_0

    .line 495
    iput-object p1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    .line 499
    :goto_0
    return-void

    .line 497
    :cond_0
    sget-object v0, Lcom/rd/animation/AnimationType;->NONE:Lcom/rd/animation/AnimationType;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    goto :goto_0
.end method

.method public setCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 250
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    if-eq v0, p1, :cond_0

    .line 251
    iput p1, p0, Lcom/rd/PageIndicatorView;->count:I

    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/rd/PageIndicatorView;->isCountSet:Z

    .line 254
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->requestLayout()V

    .line 256
    :cond_0
    return-void
.end method

.method public setDynamicCount(Z)V
    .locals 0
    .param p1, "dynamicCount"    # Z

    .prologue
    .line 274
    iput-boolean p1, p0, Lcom/rd/PageIndicatorView;->dynamicCount:Z

    .line 275
    if-eqz p1, :cond_0

    .line 276
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->registerSetObserver()V

    .line 280
    :goto_0
    return-void

    .line 278
    :cond_0
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->unRegisterSetObserver()V

    goto :goto_0
.end method

.method public setInteractiveAnimation(Z)V
    .locals 0
    .param p1, "isInteractive"    # Z

    .prologue
    .line 507
    iput-boolean p1, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    .line 508
    return-void
.end method

.method public setPadding(F)V
    .locals 1
    .param p1, "paddingPx"    # F

    .prologue
    .line 340
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 341
    const/4 p1, 0x0

    .line 344
    :cond_0
    float-to-int v0, p1

    iput v0, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    .line 345
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 346
    return-void
.end method

.method public setPadding(I)V
    .locals 1
    .param p1, "paddingDp"    # I

    .prologue
    .line 326
    if-gez p1, :cond_0

    .line 327
    const/4 p1, 0x0

    .line 330
    :cond_0
    invoke-static {p1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->paddingPx:I

    .line 331
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 332
    return-void
.end method

.method public setProgress(IF)V
    .locals 1
    .param p1, "selectingPosition"    # I
    .param p2, "progress"    # F

    .prologue
    .line 518
    iget-boolean v0, p0, Lcom/rd/PageIndicatorView;->interactiveAnimation:Z

    if-eqz v0, :cond_3

    .line 520
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    if-lez v0, :cond_0

    if-gez p1, :cond_4

    .line 521
    :cond_0
    const/4 p1, 0x0

    .line 527
    :cond_1
    :goto_0
    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gez v0, :cond_5

    .line 528
    const/4 p2, 0x0

    .line 534
    :cond_2
    :goto_1
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectingPosition:I

    .line 535
    invoke-direct {p0, p2}, Lcom/rd/PageIndicatorView;->setAnimationProgress(F)Lcom/rd/animation/AbsAnimation;

    .line 537
    :cond_3
    return-void

    .line 523
    :cond_4
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 524
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 p1, v0, -0x1

    goto :goto_0

    .line 530
    :cond_5
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p2, v0

    if-lez v0, :cond_2

    .line 531
    const/high16 p2, 0x3f800000    # 1.0f

    goto :goto_1
.end method

.method public setRadius(F)V
    .locals 1
    .param p1, "radiusPx"    # F

    .prologue
    .line 304
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 305
    const/4 p1, 0x0

    .line 308
    :cond_0
    float-to-int v0, p1

    iput v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 309
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 310
    return-void
.end method

.method public setRadius(I)V
    .locals 1
    .param p1, "radiusDp"    # I

    .prologue
    .line 289
    if-gez p1, :cond_0

    .line 290
    const/4 p1, 0x0

    .line 293
    :cond_0
    invoke-static {p1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v0

    iput v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    .line 294
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 295
    return-void
.end method

.method public setRtlMode(Lcom/rd/RtlMode;)V
    .locals 1
    .param p1, "mode"    # Lcom/rd/RtlMode;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 648
    if-nez p1, :cond_0

    .line 649
    sget-object v0, Lcom/rd/RtlMode;->Off:Lcom/rd/RtlMode;

    iput-object v0, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    .line 653
    :goto_0
    return-void

    .line 651
    :cond_0
    iput-object p1, p0, Lcom/rd/PageIndicatorView;->rtlMode:Lcom/rd/RtlMode;

    goto :goto_0
.end method

.method public setScaleFactor(F)V
    .locals 1
    .param p1, "factor"    # F

    .prologue
    .line 365
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 366
    const/high16 p1, 0x3f800000    # 1.0f

    .line 371
    :cond_0
    :goto_0
    iput p1, p0, Lcom/rd/PageIndicatorView;->scaleFactor:F

    .line 372
    return-void

    .line 367
    :cond_1
    const v0, 0x3e99999a    # 0.3f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    .line 368
    const p1, 0x3e99999a    # 0.3f

    goto :goto_0
.end method

.method public setSelectedColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 456
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectedColor:I

    .line 457
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 458
    return-void
.end method

.method public setSelection(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 546
    if-gez p1, :cond_1

    .line 547
    const/4 p1, 0x0

    .line 553
    :cond_0
    :goto_0
    iget v0, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    iput v0, p0, Lcom/rd/PageIndicatorView;->lastSelectedPosition:I

    .line 554
    iput p1, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 556
    sget-object v0, Lcom/rd/PageIndicatorView$3;->$SwitchMap$com$rd$animation$AnimationType:[I

    iget-object v1, p0, Lcom/rd/PageIndicatorView;->animationType:Lcom/rd/animation/AnimationType;

    invoke-virtual {v1}, Lcom/rd/animation/AnimationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 593
    :goto_1
    return-void

    .line 549
    :cond_1
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_0

    .line 550
    iget v0, p0, Lcom/rd/PageIndicatorView;->count:I

    add-int/lit8 p1, v0, -0x1

    goto :goto_0

    .line 558
    :pswitch_0
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    goto :goto_1

    .line 562
    :pswitch_1
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startColorAnimation()V

    goto :goto_1

    .line 566
    :pswitch_2
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startScaleAnimation()V

    goto :goto_1

    .line 570
    :pswitch_3
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startWormAnimation()V

    goto :goto_1

    .line 574
    :pswitch_4
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startFillAnimation()V

    goto :goto_1

    .line 578
    :pswitch_5
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startSlideAnimation()V

    goto :goto_1

    .line 582
    :pswitch_6
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startThinWormAnimation()V

    goto :goto_1

    .line 586
    :pswitch_7
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startDropAnimation()V

    goto :goto_1

    .line 590
    :pswitch_8
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->startSwapAnimation()V

    goto :goto_1

    .line 556
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public setStrokeWidth(F)V
    .locals 1
    .param p1, "strokePx"    # F

    .prologue
    .line 393
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 394
    const/4 p1, 0x0

    .line 400
    :cond_0
    :goto_0
    float-to-int v0, p1

    iput v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 401
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 402
    return-void

    .line 396
    :cond_1
    iget v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 397
    iget v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    int-to-float p1, v0

    goto :goto_0
.end method

.method public setStrokeWidth(I)V
    .locals 2
    .param p1, "strokeDp"    # I

    .prologue
    .line 412
    invoke-static {p1}, Lcom/rd/utils/DensityUtils;->dpToPx(I)I

    move-result v0

    .line 414
    .local v0, "strokePx":I
    if-gez v0, :cond_1

    .line 415
    const/4 v0, 0x0

    .line 421
    :cond_0
    :goto_0
    iput v0, p0, Lcom/rd/PageIndicatorView;->strokePx:I

    .line 422
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 423
    return-void

    .line 417
    :cond_1
    iget v1, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    if-le v0, v1, :cond_0

    .line 418
    iget v0, p0, Lcom/rd/PageIndicatorView;->radiusPx:I

    goto :goto_0
.end method

.method public setUnselectedColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 438
    iput p1, p0, Lcom/rd/PageIndicatorView;->unselectedColor:I

    .line 439
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 440
    return-void
.end method

.method public setViewPager(Landroid/support/v4/view/ViewPager;)V
    .locals 3
    .param p1, "pager"    # Landroid/support/v4/view/ViewPager;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 609
    invoke-virtual {p0}, Lcom/rd/PageIndicatorView;->releaseViewPager()V

    .line 611
    if-eqz p1, :cond_1

    .line 612
    iput-object p1, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    .line 613
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2, p0}, Landroid/support/v4/view/ViewPager;->addOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 615
    iget-boolean v2, p0, Lcom/rd/PageIndicatorView;->dynamicCount:Z

    invoke-virtual {p0, v2}, Lcom/rd/PageIndicatorView;->setDynamicCount(Z)V

    .line 616
    iget-boolean v2, p0, Lcom/rd/PageIndicatorView;->isCountSet:Z

    if-nez v2, :cond_1

    .line 618
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->getViewPagerCount()I

    move-result v0

    .line 619
    .local v0, "count":I
    invoke-direct {p0}, Lcom/rd/PageIndicatorView;->isRtl()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 620
    iget-object v2, p0, Lcom/rd/PageIndicatorView;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 621
    .local v1, "selected":I
    add-int/lit8 v2, v0, -0x1

    sub-int/2addr v2, v1

    iput v2, p0, Lcom/rd/PageIndicatorView;->selectedPosition:I

    .line 624
    .end local v1    # "selected":I
    :cond_0
    invoke-virtual {p0, v0}, Lcom/rd/PageIndicatorView;->setCount(I)V

    .line 627
    .end local v0    # "count":I
    :cond_1
    return-void
.end method
