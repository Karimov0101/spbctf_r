.class public Lcom/rd/animation/SlideAnimation;
.super Lcom/rd/animation/AbsAnimation;
.source "SlideAnimation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/rd/animation/AbsAnimation",
        "<",
        "Landroid/animation/ValueAnimator;",
        ">;"
    }
.end annotation


# static fields
.field private static final ANIMATION_X_COORDINATE:Ljava/lang/String; = "ANIMATION_X_COORDINATE"

.field private static final COORDINATE_NONE:I = -0x1


# instance fields
.field private xEndCoordinate:I

.field private xStartCoordinate:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    const/4 v0, -0x1

    .line 18
    invoke-direct {p0, p1}, Lcom/rd/animation/AbsAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 14
    iput v0, p0, Lcom/rd/animation/SlideAnimation;->xStartCoordinate:I

    .line 15
    iput v0, p0, Lcom/rd/animation/SlideAnimation;->xEndCoordinate:I

    .line 19
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/SlideAnimation;Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/SlideAnimation;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/rd/animation/SlideAnimation;->onAnimateUpdated(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method private createColorPropertyHolder()Landroid/animation/PropertyValuesHolder;
    .locals 5

    .prologue
    .line 65
    const-string v1, "ANIMATION_X_COORDINATE"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/rd/animation/SlideAnimation;->xStartCoordinate:I

    aput v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/rd/animation/SlideAnimation;->xEndCoordinate:I

    aput v4, v2, v3

    invoke-static {v1, v2}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 66
    .local v0, "holder":Landroid/animation/PropertyValuesHolder;
    new-instance v1, Landroid/animation/IntEvaluator;

    invoke-direct {v1}, Landroid/animation/IntEvaluator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 68
    return-object v0
.end method

.method private hasChanges(II)Z
    .locals 2
    .param p1, "startValue"    # I
    .param p2, "endValue"    # I

    .prologue
    const/4 v0, 0x1

    .line 81
    iget v1, p0, Lcom/rd/animation/SlideAnimation;->xStartCoordinate:I

    if-eq v1, p1, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    iget v1, p0, Lcom/rd/animation/SlideAnimation;->xEndCoordinate:I

    if-ne v1, p2, :cond_0

    .line 89
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAnimateUpdated(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 72
    const-string v1, "ANIMATION_X_COORDINATE"

    invoke-virtual {p1, v1}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 74
    .local v0, "xCoordinate":I
    iget-object v1, p0, Lcom/rd/animation/SlideAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    if-eqz v1, :cond_0

    .line 75
    iget-object v1, p0, Lcom/rd/animation/SlideAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-interface {v1, v0}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onSlideAnimationUpdated(I)V

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/SlideAnimation;->createAnimator()Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/ValueAnimator;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 24
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 25
    .local v0, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 26
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 27
    new-instance v1, Lcom/rd/animation/SlideAnimation$1;

    invoke-direct {v1, p0}, Lcom/rd/animation/SlideAnimation$1;-><init>(Lcom/rd/animation/SlideAnimation;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 34
    return-object v0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/rd/animation/SlideAnimation;->progress(F)Lcom/rd/animation/SlideAnimation;

    move-result-object v0

    return-object v0
.end method

.method public progress(F)Lcom/rd/animation/SlideAnimation;
    .locals 4
    .param p1, "progress"    # F

    .prologue
    .line 39
    iget-object v2, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    .line 40
    iget-wide v2, p0, Lcom/rd/animation/SlideAnimation;->animationDuration:J

    long-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-long v0, v2

    .line 42
    .local v0, "playTime":J
    iget-object v2, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_0

    .line 43
    iget-object v2, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 47
    .end local v0    # "playTime":J
    :cond_0
    return-object p0
.end method

.method public with(II)Lcom/rd/animation/SlideAnimation;
    .locals 4
    .param p1, "startValue"    # I
    .param p2, "endValue"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 52
    iget-object v1, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    if-eqz v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/rd/animation/SlideAnimation;->hasChanges(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    iput p1, p0, Lcom/rd/animation/SlideAnimation;->xStartCoordinate:I

    .line 55
    iput p2, p0, Lcom/rd/animation/SlideAnimation;->xEndCoordinate:I

    .line 57
    invoke-direct {p0}, Lcom/rd/animation/SlideAnimation;->createColorPropertyHolder()Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 58
    .local v0, "holder":Landroid/animation/PropertyValuesHolder;
    iget-object v1, p0, Lcom/rd/animation/SlideAnimation;->animator:Landroid/animation/Animator;

    check-cast v1, Landroid/animation/ValueAnimator;

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/animation/PropertyValuesHolder;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 61
    .end local v0    # "holder":Landroid/animation/PropertyValuesHolder;
    :cond_0
    return-object p0
.end method
