.class public Lcom/rd/animation/ValueAnimation;
.super Ljava/lang/Object;
.source "ValueAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/rd/animation/ValueAnimation$UpdateListener;
    }
.end annotation


# instance fields
.field private colorAnimation:Lcom/rd/animation/ColorAnimation;

.field private dropAnimation:Lcom/rd/animation/DropAnimation;

.field private fillAnimation:Lcom/rd/animation/FillAnimation;

.field private scaleAnimation:Lcom/rd/animation/ScaleAnimation;

.field private slideAnimation:Lcom/rd/animation/SlideAnimation;

.field private swapAnimation:Lcom/rd/animation/SwapAnimation;

.field private thinWormAnimation:Lcom/rd/animation/ThinWormAnimation;

.field private updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

.field private wormAnimation:Lcom/rd/animation/WormAnimation;


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    .line 40
    return-void
.end method


# virtual methods
.method public color()Lcom/rd/animation/ColorAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 44
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->colorAnimation:Lcom/rd/animation/ColorAnimation;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/rd/animation/ColorAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/ColorAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->colorAnimation:Lcom/rd/animation/ColorAnimation;

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->colorAnimation:Lcom/rd/animation/ColorAnimation;

    return-object v0
.end method

.method public drop()Lcom/rd/animation/DropAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 98
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->dropAnimation:Lcom/rd/animation/DropAnimation;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/rd/animation/DropAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/DropAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->dropAnimation:Lcom/rd/animation/DropAnimation;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->dropAnimation:Lcom/rd/animation/DropAnimation;

    return-object v0
.end method

.method public fill()Lcom/rd/animation/FillAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->fillAnimation:Lcom/rd/animation/FillAnimation;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lcom/rd/animation/FillAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/FillAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->fillAnimation:Lcom/rd/animation/FillAnimation;

    .line 84
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->fillAnimation:Lcom/rd/animation/FillAnimation;

    return-object v0
.end method

.method public scale()Lcom/rd/animation/ScaleAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->scaleAnimation:Lcom/rd/animation/ScaleAnimation;

    if-nez v0, :cond_0

    .line 54
    new-instance v0, Lcom/rd/animation/ScaleAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/ScaleAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->scaleAnimation:Lcom/rd/animation/ScaleAnimation;

    .line 57
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->scaleAnimation:Lcom/rd/animation/ScaleAnimation;

    return-object v0
.end method

.method public slide()Lcom/rd/animation/SlideAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->slideAnimation:Lcom/rd/animation/SlideAnimation;

    if-nez v0, :cond_0

    .line 72
    new-instance v0, Lcom/rd/animation/SlideAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/SlideAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->slideAnimation:Lcom/rd/animation/SlideAnimation;

    .line 75
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->slideAnimation:Lcom/rd/animation/SlideAnimation;

    return-object v0
.end method

.method public swap()Lcom/rd/animation/SwapAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->swapAnimation:Lcom/rd/animation/SwapAnimation;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/rd/animation/SwapAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/SwapAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->swapAnimation:Lcom/rd/animation/SwapAnimation;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->swapAnimation:Lcom/rd/animation/SwapAnimation;

    return-object v0
.end method

.method public thinWorm()Lcom/rd/animation/ThinWormAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 89
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->thinWormAnimation:Lcom/rd/animation/ThinWormAnimation;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/rd/animation/ThinWormAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/ThinWormAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->thinWormAnimation:Lcom/rd/animation/ThinWormAnimation;

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->thinWormAnimation:Lcom/rd/animation/ThinWormAnimation;

    return-object v0
.end method

.method public worm()Lcom/rd/animation/WormAnimation;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 62
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->wormAnimation:Lcom/rd/animation/WormAnimation;

    if-nez v0, :cond_0

    .line 63
    new-instance v0, Lcom/rd/animation/WormAnimation;

    iget-object v1, p0, Lcom/rd/animation/ValueAnimation;->updateListener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-direct {v0, v1}, Lcom/rd/animation/WormAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    iput-object v0, p0, Lcom/rd/animation/ValueAnimation;->wormAnimation:Lcom/rd/animation/WormAnimation;

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/rd/animation/ValueAnimation;->wormAnimation:Lcom/rd/animation/WormAnimation;

    return-object v0
.end method
