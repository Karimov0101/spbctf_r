.class public Lcom/rd/animation/ColorAnimation;
.super Lcom/rd/animation/AbsAnimation;
.source "ColorAnimation.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/rd/animation/AbsAnimation",
        "<",
        "Landroid/animation/ValueAnimator;",
        ">;"
    }
.end annotation


# static fields
.field private static final ANIMATION_COLOR:Ljava/lang/String; = "ANIMATION_COLOR"

.field private static final ANIMATION_COLOR_REVERSE:Ljava/lang/String; = "ANIMATION_COLOR_REVERSE"

.field public static final DEFAULT_SELECTED_COLOR:Ljava/lang/String; = "#ffffff"

.field public static final DEFAULT_UNSELECTED_COLOR:Ljava/lang/String; = "#33ffffff"


# instance fields
.field protected endColor:I

.field protected startColor:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/rd/animation/AbsAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/ColorAnimation;Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/ColorAnimation;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/rd/animation/ColorAnimation;->onAnimateUpdated(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method private hasChanges(II)Z
    .locals 2
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I

    .prologue
    const/4 v0, 0x1

    .line 93
    iget v1, p0, Lcom/rd/animation/ColorAnimation;->startColor:I

    if-eq v1, p1, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 97
    :cond_1
    iget v1, p0, Lcom/rd/animation/ColorAnimation;->endColor:I

    if-ne v1, p2, :cond_0

    .line 101
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAnimateUpdated(Landroid/animation/ValueAnimator;)V
    .locals 3
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 105
    const-string v2, "ANIMATION_COLOR"

    invoke-virtual {p1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 106
    .local v0, "color":I
    const-string v2, "ANIMATION_COLOR_REVERSE"

    invoke-virtual {p1, v2}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 108
    .local v1, "colorReverse":I
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    if-eqz v2, :cond_0

    .line 109
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-interface {v2, v0, v1}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onColorAnimationUpdated(II)V

    .line 111
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/ColorAnimation;->createAnimator()Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/ValueAnimator;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 27
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 28
    .local v0, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 29
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 30
    new-instance v1, Lcom/rd/animation/ColorAnimation$1;

    invoke-direct {v1, p0}, Lcom/rd/animation/ColorAnimation$1;-><init>(Lcom/rd/animation/ColorAnimation;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 37
    return-object v0
.end method

.method protected createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;
    .locals 6
    .param p1, "isReverse"    # Z

    .prologue
    .line 74
    if-eqz p1, :cond_0

    .line 75
    const-string v2, "ANIMATION_COLOR_REVERSE"

    .line 76
    .local v2, "propertyName":Ljava/lang/String;
    iget v3, p0, Lcom/rd/animation/ColorAnimation;->endColor:I

    .line 77
    .local v3, "startColorValue":I
    iget v0, p0, Lcom/rd/animation/ColorAnimation;->startColor:I

    .line 85
    .local v0, "endColorValue":I
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v3, v4, v5

    const/4 v5, 0x1

    aput v0, v4, v5

    invoke-static {v2, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 86
    .local v1, "holder":Landroid/animation/PropertyValuesHolder;
    new-instance v4, Landroid/animation/ArgbEvaluator;

    invoke-direct {v4}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 88
    return-object v1

    .line 80
    .end local v0    # "endColorValue":I
    .end local v1    # "holder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "propertyName":Ljava/lang/String;
    .end local v3    # "startColorValue":I
    :cond_0
    const-string v2, "ANIMATION_COLOR"

    .line 81
    .restart local v2    # "propertyName":Ljava/lang/String;
    iget v3, p0, Lcom/rd/animation/ColorAnimation;->startColor:I

    .line 82
    .restart local v3    # "startColorValue":I
    iget v0, p0, Lcom/rd/animation/ColorAnimation;->endColor:I

    .restart local v0    # "endColorValue":I
    goto :goto_0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/rd/animation/ColorAnimation;->progress(F)Lcom/rd/animation/ColorAnimation;

    move-result-object v0

    return-object v0
.end method

.method public progress(F)Lcom/rd/animation/ColorAnimation;
    .locals 4
    .param p1, "progress"    # F

    .prologue
    .line 42
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    .line 43
    iget-wide v2, p0, Lcom/rd/animation/ColorAnimation;->animationDuration:J

    long-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-long v0, v2

    .line 45
    .local v0, "playTime":J
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    array-length v2, v2

    if-lez v2, :cond_0

    .line 46
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0, v1}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 50
    .end local v0    # "playTime":J
    :cond_0
    return-object p0
.end method

.method public with(II)Lcom/rd/animation/ColorAnimation;
    .locals 6
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 55
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    if-eqz v2, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/rd/animation/ColorAnimation;->hasChanges(II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    iput p1, p0, Lcom/rd/animation/ColorAnimation;->startColor:I

    .line 58
    iput p2, p0, Lcom/rd/animation/ColorAnimation;->endColor:I

    .line 60
    invoke-virtual {p0, v4}, Lcom/rd/animation/ColorAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 61
    .local v0, "colorHolder":Landroid/animation/PropertyValuesHolder;
    invoke-virtual {p0, v5}, Lcom/rd/animation/ColorAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 63
    .local v1, "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    iget-object v2, p0, Lcom/rd/animation/ColorAnimation;->animator:Landroid/animation/Animator;

    check-cast v2, Landroid/animation/ValueAnimator;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v3, v4

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 66
    .end local v0    # "colorHolder":Landroid/animation/PropertyValuesHolder;
    .end local v1    # "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    :cond_0
    return-object p0
.end method
