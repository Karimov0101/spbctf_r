.class public Lcom/rd/animation/FillAnimation;
.super Lcom/rd/animation/ColorAnimation;
.source "FillAnimation.java"


# static fields
.field private static final ANIMATION_COLOR:Ljava/lang/String; = "ANIMATION_COLOR"

.field private static final ANIMATION_COLOR_REVERSE:Ljava/lang/String; = "ANIMATION_COLOR_REVERSE"

.field private static final ANIMATION_RADIUS:Ljava/lang/String; = "ANIMATION_RADIUS"

.field private static final ANIMATION_RADIUS_REVERSE:Ljava/lang/String; = "ANIMATION_RADIUS_REVERSE"

.field private static final ANIMATION_STROKE:Ljava/lang/String; = "ANIMATION_STROKE"

.field private static final ANIMATION_STROKE_REVERSE:Ljava/lang/String; = "ANIMATION_STROKE_REVERSE"

.field public static final DEFAULT_STROKE_DP:I = 0x1


# instance fields
.field private radiusPx:I

.field private strokePx:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/rd/animation/ColorAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/FillAnimation;Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/FillAnimation;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/rd/animation/FillAnimation;->onAnimateUpdated(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method private createRadiusPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;
    .locals 6
    .param p1, "isReverse"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 82
    if-eqz p1, :cond_0

    .line 83
    const-string v2, "ANIMATION_RADIUS_REVERSE"

    .line 84
    .local v2, "propertyName":Ljava/lang/String;
    iget v4, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    div-int/lit8 v3, v4, 0x2

    .line 85
    .local v3, "startRadiusValue":I
    iget v0, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    .line 92
    .local v0, "endRadiusValue":I
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v3, v4, v5

    const/4 v5, 0x1

    aput v0, v4, v5

    invoke-static {v2, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 93
    .local v1, "holder":Landroid/animation/PropertyValuesHolder;
    new-instance v4, Landroid/animation/IntEvaluator;

    invoke-direct {v4}, Landroid/animation/IntEvaluator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 95
    return-object v1

    .line 87
    .end local v0    # "endRadiusValue":I
    .end local v1    # "holder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "propertyName":Ljava/lang/String;
    .end local v3    # "startRadiusValue":I
    :cond_0
    const-string v2, "ANIMATION_RADIUS"

    .line 88
    .restart local v2    # "propertyName":Ljava/lang/String;
    iget v3, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    .line 89
    .restart local v3    # "startRadiusValue":I
    iget v4, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    div-int/lit8 v0, v4, 0x2

    .restart local v0    # "endRadiusValue":I
    goto :goto_0
.end method

.method private createStrokePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;
    .locals 6
    .param p1, "isReverse"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 104
    if-eqz p1, :cond_0

    .line 105
    const-string v2, "ANIMATION_STROKE_REVERSE"

    .line 106
    .local v2, "propertyName":Ljava/lang/String;
    iget v3, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    .line 107
    .local v3, "startStrokeValue":I
    const/4 v0, 0x0

    .line 114
    .local v0, "endStrokeValue":I
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v3, v4, v5

    const/4 v5, 0x1

    aput v0, v4, v5

    invoke-static {v2, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 115
    .local v1, "holder":Landroid/animation/PropertyValuesHolder;
    new-instance v4, Landroid/animation/IntEvaluator;

    invoke-direct {v4}, Landroid/animation/IntEvaluator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 117
    return-object v1

    .line 109
    .end local v0    # "endStrokeValue":I
    .end local v1    # "holder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "propertyName":Ljava/lang/String;
    .end local v3    # "startStrokeValue":I
    :cond_0
    const-string v2, "ANIMATION_STROKE"

    .line 110
    .restart local v2    # "propertyName":Ljava/lang/String;
    const/4 v3, 0x0

    .line 111
    .restart local v3    # "startStrokeValue":I
    iget v0, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    .restart local v0    # "endStrokeValue":I
    goto :goto_0
.end method

.method private hasChanges(IIII)Z
    .locals 2
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I
    .param p3, "radiusValue"    # I
    .param p4, "strokeValue"    # I

    .prologue
    const/4 v0, 0x1

    .line 145
    iget v1, p0, Lcom/rd/animation/FillAnimation;->startColor:I

    if-eq v1, p1, :cond_1

    .line 161
    :cond_0
    :goto_0
    return v0

    .line 149
    :cond_1
    iget v1, p0, Lcom/rd/animation/FillAnimation;->endColor:I

    if-ne v1, p2, :cond_0

    .line 153
    iget v1, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    if-ne v1, p3, :cond_0

    .line 157
    iget v1, p0, Lcom/rd/animation/FillAnimation;->strokePx:I

    if-ne v1, p4, :cond_0

    .line 161
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAnimateUpdated(Landroid/animation/ValueAnimator;)V
    .locals 7
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 121
    const-string v0, "ANIMATION_COLOR"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 122
    .local v1, "color":I
    const-string v0, "ANIMATION_COLOR_REVERSE"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 124
    .local v2, "colorReverse":I
    const-string v0, "ANIMATION_RADIUS"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 125
    .local v3, "radius":I
    const-string v0, "ANIMATION_RADIUS_REVERSE"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 127
    .local v4, "radiusReverse":I
    const-string v0, "ANIMATION_STROKE"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 128
    .local v5, "stroke":I
    const-string v0, "ANIMATION_STROKE_REVERSE"

    invoke-virtual {p1, v0}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 130
    .local v6, "strokeReverse":I
    iget-object v0, p0, Lcom/rd/animation/FillAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/rd/animation/FillAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-interface/range {v0 .. v6}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onFillAnimationUpdated(IIIIII)V

    .line 141
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/FillAnimation;->createAnimator()Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/ValueAnimator;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 32
    .local v0, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 33
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 34
    new-instance v1, Lcom/rd/animation/FillAnimation$1;

    invoke-direct {v1, p0}, Lcom/rd/animation/FillAnimation$1;-><init>(Lcom/rd/animation/FillAnimation;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 41
    return-object v0
.end method

.method public with(IIII)Lcom/rd/animation/FillAnimation;
    .locals 10
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I
    .param p3, "radiusValue"    # I
    .param p4, "strokeValue"    # I
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 46
    iget-object v6, p0, Lcom/rd/animation/FillAnimation;->animator:Landroid/animation/Animator;

    if-eqz v6, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/animation/FillAnimation;->hasChanges(IIII)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 48
    iput p1, p0, Lcom/rd/animation/FillAnimation;->startColor:I

    .line 49
    iput p2, p0, Lcom/rd/animation/FillAnimation;->endColor:I

    .line 50
    iput p3, p0, Lcom/rd/animation/FillAnimation;->radiusPx:I

    .line 51
    iput p4, p0, Lcom/rd/animation/FillAnimation;->strokePx:I

    .line 53
    invoke-virtual {p0, v8}, Lcom/rd/animation/FillAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 54
    .local v0, "colorHolder":Landroid/animation/PropertyValuesHolder;
    invoke-virtual {p0, v9}, Lcom/rd/animation/FillAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 56
    .local v3, "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v8}, Lcom/rd/animation/FillAnimation;->createRadiusPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 57
    .local v1, "radiusHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v9}, Lcom/rd/animation/FillAnimation;->createRadiusPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 59
    .local v2, "radiusReverseHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v8}, Lcom/rd/animation/FillAnimation;->createStrokePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 60
    .local v4, "strokeHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v9}, Lcom/rd/animation/FillAnimation;->createStrokePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 62
    .local v5, "strokeReverseHolder":Landroid/animation/PropertyValuesHolder;
    iget-object v6, p0, Lcom/rd/animation/FillAnimation;->animator:Landroid/animation/Animator;

    check-cast v6, Landroid/animation/ValueAnimator;

    const/4 v7, 0x6

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v8

    aput-object v3, v7, v9

    const/4 v8, 0x2

    aput-object v1, v7, v8

    const/4 v8, 0x3

    aput-object v2, v7, v8

    const/4 v8, 0x4

    aput-object v4, v7, v8

    const/4 v8, 0x5

    aput-object v5, v7, v8

    invoke-virtual {v6, v7}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 73
    .end local v0    # "colorHolder":Landroid/animation/PropertyValuesHolder;
    .end local v1    # "radiusHolder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "radiusReverseHolder":Landroid/animation/PropertyValuesHolder;
    .end local v3    # "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    .end local v4    # "strokeHolder":Landroid/animation/PropertyValuesHolder;
    .end local v5    # "strokeReverseHolder":Landroid/animation/PropertyValuesHolder;
    :cond_0
    return-object p0
.end method
