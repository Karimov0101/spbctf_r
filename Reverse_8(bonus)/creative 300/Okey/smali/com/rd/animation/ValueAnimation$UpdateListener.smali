.class public interface abstract Lcom/rd/animation/ValueAnimation$UpdateListener;
.super Ljava/lang/Object;
.source "ValueAnimation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/rd/animation/ValueAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "UpdateListener"
.end annotation


# virtual methods
.method public abstract onColorAnimationUpdated(II)V
.end method

.method public abstract onDropAnimationUpdated(III)V
.end method

.method public abstract onFillAnimationUpdated(IIIIII)V
.end method

.method public abstract onScaleAnimationUpdated(IIII)V
.end method

.method public abstract onSlideAnimationUpdated(I)V
.end method

.method public abstract onSwapAnimationUpdated(I)V
.end method

.method public abstract onThinWormAnimationUpdated(III)V
.end method

.method public abstract onWormAnimationUpdated(II)V
.end method
