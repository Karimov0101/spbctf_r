.class public Lcom/rd/animation/ThinWormAnimation;
.super Lcom/rd/animation/WormAnimation;
.source "ThinWormAnimation.java"


# static fields
.field private static final PERCENTAGE_HEIGHT_DURATION:F = 0.25f

.field private static final PERCENTAGE_REVERSE_HEIGHT_DELAY:F = 0.65f

.field private static final PERCENTAGE_SIZE_DURATION_DELAY:F = 0.7f


# instance fields
.field private height:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/rd/animation/WormAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 16
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/ThinWormAnimation;)I
    .locals 1
    .param p0, "x0"    # Lcom/rd/animation/ThinWormAnimation;

    .prologue
    .line 7
    iget v0, p0, Lcom/rd/animation/ThinWormAnimation;->height:I

    return v0
.end method

.method static synthetic access$002(Lcom/rd/animation/ThinWormAnimation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/ThinWormAnimation;
    .param p1, "x1"    # I

    .prologue
    .line 7
    iput p1, p0, Lcom/rd/animation/ThinWormAnimation;->height:I

    return p1
.end method

.method private createHeightAnimator(IIJ)Landroid/animation/ValueAnimator;
    .locals 5
    .param p1, "fromHeight"    # I
    .param p2, "toHeight"    # I
    .param p3, "startDelay"    # J

    .prologue
    .line 57
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 58
    .local v0, "anim":Landroid/animation/ValueAnimator;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 59
    iget-wide v2, p0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    long-to-float v1, v2

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float/2addr v1, v2

    float-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 60
    invoke-virtual {v0, p3, p4}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 61
    new-instance v1, Lcom/rd/animation/ThinWormAnimation$1;

    invoke-direct {v1, p0}, Lcom/rd/animation/ThinWormAnimation$1;-><init>(Lcom/rd/animation/ThinWormAnimation;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 69
    return-object v0
.end method


# virtual methods
.method public bridge synthetic duration(J)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1, p2}, Lcom/rd/animation/ThinWormAnimation;->duration(J)Lcom/rd/animation/ThinWormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public duration(J)Lcom/rd/animation/ThinWormAnimation;
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 20
    invoke-super {p0, p1, p2}, Lcom/rd/animation/WormAnimation;->duration(J)Lcom/rd/animation/WormAnimation;

    .line 21
    return-object p0
.end method

.method public bridge synthetic duration(J)Lcom/rd/animation/WormAnimation;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1, p2}, Lcom/rd/animation/ThinWormAnimation;->duration(J)Lcom/rd/animation/ThinWormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/rd/animation/ThinWormAnimation;->progress(F)Lcom/rd/animation/ThinWormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public progress(F)Lcom/rd/animation/ThinWormAnimation;
    .locals 12
    .param p1, "progress"    # F

    .prologue
    .line 74
    iget-object v9, p0, Lcom/rd/animation/ThinWormAnimation;->animator:Landroid/animation/Animator;

    if-eqz v9, :cond_0

    .line 75
    iget-wide v10, p0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    long-to-float v9, v10

    mul-float/2addr v9, p1

    float-to-long v4, v9

    .line 76
    .local v4, "duration":J
    iget-object v9, p0, Lcom/rd/animation/ThinWormAnimation;->animator:Landroid/animation/Animator;

    check-cast v9, Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 77
    .local v8, "size":I
    iget-wide v10, p0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    long-to-float v9, v10

    const v10, 0x3f266666    # 0.65f

    mul-float/2addr v9, v10

    float-to-long v6, v9

    .line 79
    .local v6, "minus":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_0

    .line 80
    iget-object v9, p0, Lcom/rd/animation/ThinWormAnimation;->animator:Landroid/animation/Animator;

    check-cast v9, Landroid/animation/AnimatorSet;

    invoke-virtual {v9}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 82
    .local v0, "anim":Landroid/animation/ValueAnimator;
    const/4 v9, 0x3

    if-ne v1, v9, :cond_2

    .line 83
    cmp-long v9, v4, v6

    if-gez v9, :cond_1

    .line 101
    .end local v0    # "anim":Landroid/animation/ValueAnimator;
    .end local v1    # "i":I
    .end local v4    # "duration":J
    .end local v6    # "minus":J
    .end local v8    # "size":I
    :cond_0
    return-object p0

    .line 86
    .restart local v0    # "anim":Landroid/animation/ValueAnimator;
    .restart local v1    # "i":I
    .restart local v4    # "duration":J
    .restart local v6    # "minus":J
    .restart local v8    # "size":I
    :cond_1
    sub-long/2addr v4, v6

    .line 90
    :cond_2
    move-wide v2, v4

    .line 91
    .local v2, "currPlayTime":J
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v10

    cmp-long v9, v2, v10

    if-ltz v9, :cond_3

    .line 92
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    .line 95
    :cond_3
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    if-eqz v9, :cond_4

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    array-length v9, v9

    if-lez v9, :cond_4

    .line 96
    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 79
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/WormAnimation;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/rd/animation/ThinWormAnimation;->progress(F)Lcom/rd/animation/ThinWormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public with(IIIZ)Lcom/rd/animation/WormAnimation;
    .locals 25
    .param p1, "fromValue"    # I
    .param p2, "toValue"    # I
    .param p3, "radius"    # I
    .param p4, "isRightSide"    # Z

    .prologue
    .line 26
    invoke-virtual/range {p0 .. p4}, Lcom/rd/animation/ThinWormAnimation;->hasChanges(IIIZ)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 27
    invoke-virtual/range {p0 .. p0}, Lcom/rd/animation/ThinWormAnimation;->createAnimator()Landroid/animation/AnimatorSet;

    move-result-object v5

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/rd/animation/ThinWormAnimation;->animator:Landroid/animation/Animator;

    .line 29
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/ThinWormAnimation;->fromValue:I

    .line 30
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/ThinWormAnimation;->toValue:I

    .line 31
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/ThinWormAnimation;->radius:I

    .line 32
    mul-int/lit8 v5, p3, 0x2

    move-object/from16 v0, p0

    iput v5, v0, Lcom/rd/animation/ThinWormAnimation;->height:I

    .line 33
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/rd/animation/ThinWormAnimation;->isRightSide:Z

    .line 35
    sub-int v5, p1, p3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/rd/animation/ThinWormAnimation;->rectLeftX:I

    .line 36
    add-int v5, p1, p3

    move-object/from16 v0, p0

    iput v5, v0, Lcom/rd/animation/ThinWormAnimation;->rectRightX:I

    .line 38
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    long-to-float v5, v6

    const v6, 0x3f333333    # 0.7f

    mul-float/2addr v5, v6

    float-to-long v8, v5

    .line 39
    .local v8, "straightSizeDuration":J
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    .line 41
    .local v14, "reverseSizeDuration":J
    const-wide/16 v22, 0x0

    .line 42
    .local v22, "straightHeightDelay":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/rd/animation/ThinWormAnimation;->animationDuration:J

    long-to-float v5, v6

    const v6, 0x3f266666    # 0.65f

    mul-float/2addr v5, v6

    float-to-long v0, v5

    move-wide/from16 v18, v0

    .line 44
    .local v18, "reverseHeightDelay":J
    move-object/from16 v0, p0

    move/from16 v1, p4

    invoke-virtual {v0, v1}, Lcom/rd/animation/ThinWormAnimation;->createAnimationValues(Z)Lcom/rd/animation/WormAnimation$AnimationValues;

    move-result-object v24

    .line 45
    .local v24, "values":Lcom/rd/animation/WormAnimation$AnimationValues;
    move-object/from16 v0, v24

    iget v6, v0, Lcom/rd/animation/WormAnimation$AnimationValues;->fromX:I

    move-object/from16 v0, v24

    iget v7, v0, Lcom/rd/animation/WormAnimation$AnimationValues;->toX:I

    const/4 v10, 0x0

    move-object/from16 v5, p0

    invoke-virtual/range {v5 .. v10}, Lcom/rd/animation/ThinWormAnimation;->createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;

    move-result-object v20

    .line 46
    .local v20, "straightAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/rd/animation/ThinWormAnimation;->height:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/rd/animation/ThinWormAnimation;->height:I

    div-int/lit8 v6, v6, 0x2

    move-object/from16 v0, p0

    move-wide/from16 v1, v22

    invoke-direct {v0, v5, v6, v1, v2}, Lcom/rd/animation/ThinWormAnimation;->createHeightAnimator(IIJ)Landroid/animation/ValueAnimator;

    move-result-object v21

    .line 48
    .local v21, "straightHeightAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, v24

    iget v12, v0, Lcom/rd/animation/WormAnimation$AnimationValues;->reverseFromX:I

    move-object/from16 v0, v24

    iget v13, v0, Lcom/rd/animation/WormAnimation$AnimationValues;->reverseToX:I

    const/16 v16, 0x1

    move-object/from16 v11, p0

    invoke-virtual/range {v11 .. v16}, Lcom/rd/animation/ThinWormAnimation;->createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;

    move-result-object v4

    .line 49
    .local v4, "reverseAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p0

    iget v5, v0, Lcom/rd/animation/ThinWormAnimation;->height:I

    div-int/lit8 v5, v5, 0x2

    move-object/from16 v0, p0

    iget v6, v0, Lcom/rd/animation/ThinWormAnimation;->height:I

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    invoke-direct {v0, v5, v6, v1, v2}, Lcom/rd/animation/ThinWormAnimation;->createHeightAnimator(IIJ)Landroid/animation/ValueAnimator;

    move-result-object v17

    .line 51
    .local v17, "reverseHeightAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/rd/animation/ThinWormAnimation;->animator:Landroid/animation/Animator;

    check-cast v5, Landroid/animation/AnimatorSet;

    const/4 v6, 0x4

    new-array v6, v6, [Landroid/animation/Animator;

    const/4 v7, 0x0

    aput-object v20, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v7, 0x2

    aput-object v21, v6, v7

    const/4 v7, 0x3

    aput-object v17, v6, v7

    invoke-virtual {v5, v6}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 53
    .end local v4    # "reverseAnimator":Landroid/animation/ValueAnimator;
    .end local v8    # "straightSizeDuration":J
    .end local v14    # "reverseSizeDuration":J
    .end local v17    # "reverseHeightAnimator":Landroid/animation/ValueAnimator;
    .end local v18    # "reverseHeightDelay":J
    .end local v20    # "straightAnimator":Landroid/animation/ValueAnimator;
    .end local v21    # "straightHeightAnimator":Landroid/animation/ValueAnimator;
    .end local v22    # "straightHeightDelay":J
    .end local v24    # "values":Lcom/rd/animation/WormAnimation$AnimationValues;
    :cond_0
    return-object p0
.end method
