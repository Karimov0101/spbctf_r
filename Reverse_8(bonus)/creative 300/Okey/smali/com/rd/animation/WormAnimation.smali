.class public Lcom/rd/animation/WormAnimation;
.super Lcom/rd/animation/AbsAnimation;
.source "WormAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/rd/animation/WormAnimation$AnimationValues;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/rd/animation/AbsAnimation",
        "<",
        "Landroid/animation/AnimatorSet;",
        ">;"
    }
.end annotation


# instance fields
.field fromValue:I

.field isRightSide:Z

.field radius:I

.field rectLeftX:I

.field rectRightX:I

.field toValue:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/rd/animation/AbsAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 21
    return-void
.end method


# virtual methods
.method createAnimationValues(Z)Lcom/rd/animation/WormAnimation$AnimationValues;
    .locals 6
    .param p1, "isRightSide"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 149
    if-eqz p1, :cond_0

    .line 150
    iget v0, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    add-int v2, v0, v1

    .line 151
    .local v2, "fromX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    add-int v3, v0, v1

    .line 153
    .local v3, "toX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    sub-int v4, v0, v1

    .line 154
    .local v4, "reverseFromX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    sub-int v5, v0, v1

    .line 164
    .local v5, "reverseToX":I
    :goto_0
    new-instance v0, Lcom/rd/animation/WormAnimation$AnimationValues;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/rd/animation/WormAnimation$AnimationValues;-><init>(Lcom/rd/animation/WormAnimation;IIII)V

    return-object v0

    .line 157
    .end local v2    # "fromX":I
    .end local v3    # "toX":I
    .end local v4    # "reverseFromX":I
    .end local v5    # "reverseToX":I
    :cond_0
    iget v0, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    sub-int v2, v0, v1

    .line 158
    .restart local v2    # "fromX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    sub-int v3, v0, v1

    .line 160
    .restart local v3    # "toX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    add-int v4, v0, v1

    .line 161
    .restart local v4    # "reverseFromX":I
    iget v0, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    add-int v5, v0, v1

    .restart local v5    # "reverseToX":I
    goto :goto_0
.end method

.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/WormAnimation;->createAnimator()Landroid/animation/AnimatorSet;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/AnimatorSet;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 26
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 27
    .local v0, "animator":Landroid/animation/AnimatorSet;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 29
    return-object v0
.end method

.method createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;
    .locals 3
    .param p1, "fromX"    # I
    .param p2, "toX"    # I
    .param p3, "duration"    # J
    .param p5, "isReverse"    # Z

    .prologue
    .line 90
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 91
    .local v0, "anim":Landroid/animation/ValueAnimator;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 92
    invoke-virtual {v0, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 93
    new-instance v1, Lcom/rd/animation/WormAnimation$1;

    invoke-direct {v1, p0, p5}, Lcom/rd/animation/WormAnimation$1;-><init>(Lcom/rd/animation/WormAnimation;Z)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 117
    return-object v0
.end method

.method public bridge synthetic duration(J)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/rd/animation/WormAnimation;->duration(J)Lcom/rd/animation/WormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public duration(J)Lcom/rd/animation/WormAnimation;
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 34
    invoke-super {p0, p1, p2}, Lcom/rd/animation/AbsAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    .line 35
    return-object p0
.end method

.method hasChanges(IIIZ)Z
    .locals 2
    .param p1, "fromValue"    # I
    .param p2, "toValue"    # I
    .param p3, "radius"    # I
    .param p4, "isRightSide"    # Z

    .prologue
    const/4 v0, 0x1

    .line 122
    iget v1, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    if-eq v1, p1, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    iget v1, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    if-ne v1, p2, :cond_0

    .line 130
    iget v1, p0, Lcom/rd/animation/WormAnimation;->radius:I

    if-ne v1, p3, :cond_0

    .line 134
    iget-boolean v1, p0, Lcom/rd/animation/WormAnimation;->isRightSide:Z

    if-ne v1, p4, :cond_0

    .line 138
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/rd/animation/WormAnimation;->progress(F)Lcom/rd/animation/WormAnimation;

    move-result-object v0

    return-object v0
.end method

.method public progress(F)Lcom/rd/animation/WormAnimation;
    .locals 12
    .param p1, "progress"    # F

    .prologue
    .line 63
    iget-object v8, p0, Lcom/rd/animation/WormAnimation;->animator:Landroid/animation/Animator;

    if-eqz v8, :cond_3

    .line 64
    iget-wide v8, p0, Lcom/rd/animation/WormAnimation;->animationDuration:J

    long-to-float v8, v8

    mul-float/2addr v8, p1

    float-to-long v6, v8

    .line 66
    .local v6, "playTimeLeft":J
    iget-object v8, p0, Lcom/rd/animation/WormAnimation;->animator:Landroid/animation/Animator;

    check-cast v8, Landroid/animation/AnimatorSet;

    invoke-virtual {v8}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .local v0, "anim":Landroid/animation/Animator;
    move-object v1, v0

    .line 67
    check-cast v1, Landroid/animation/ValueAnimator;

    .line 68
    .local v1, "animator":Landroid/animation/ValueAnimator;
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    .line 70
    .local v2, "animDuration":J
    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-gez v9, :cond_0

    .line 71
    const-wide/16 v6, 0x0

    .line 74
    :cond_0
    move-wide v4, v6

    .line 75
    .local v4, "currPlayTime":J
    cmp-long v9, v4, v2

    if-ltz v9, :cond_1

    .line 76
    move-wide v4, v2

    .line 79
    :cond_1
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v9

    array-length v9, v9

    if-lez v9, :cond_2

    .line 80
    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 82
    :cond_2
    sub-long/2addr v6, v4

    .line 83
    goto :goto_0

    .line 86
    .end local v0    # "anim":Landroid/animation/Animator;
    .end local v1    # "animator":Landroid/animation/ValueAnimator;
    .end local v2    # "animDuration":J
    .end local v4    # "currPlayTime":J
    .end local v6    # "playTimeLeft":J
    :cond_3
    return-object p0
.end method

.method public with(IIIZ)Lcom/rd/animation/WormAnimation;
    .locals 14
    .param p1, "fromValue"    # I
    .param p2, "toValue"    # I
    .param p3, "radius"    # I
    .param p4, "isRightSide"    # Z

    .prologue
    .line 39
    invoke-virtual/range {p0 .. p4}, Lcom/rd/animation/WormAnimation;->hasChanges(IIIZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/rd/animation/WormAnimation;->createAnimator()Landroid/animation/AnimatorSet;

    move-result-object v3

    iput-object v3, p0, Lcom/rd/animation/WormAnimation;->animator:Landroid/animation/Animator;

    .line 42
    iput p1, p0, Lcom/rd/animation/WormAnimation;->fromValue:I

    .line 43
    move/from16 v0, p2

    iput v0, p0, Lcom/rd/animation/WormAnimation;->toValue:I

    .line 44
    move/from16 v0, p3

    iput v0, p0, Lcom/rd/animation/WormAnimation;->radius:I

    .line 45
    move/from16 v0, p4

    iput-boolean v0, p0, Lcom/rd/animation/WormAnimation;->isRightSide:Z

    .line 47
    sub-int v3, p1, p3

    iput v3, p0, Lcom/rd/animation/WormAnimation;->rectLeftX:I

    .line 48
    add-int v3, p1, p3

    iput v3, p0, Lcom/rd/animation/WormAnimation;->rectRightX:I

    .line 50
    move/from16 v0, p4

    invoke-virtual {p0, v0}, Lcom/rd/animation/WormAnimation;->createAnimationValues(Z)Lcom/rd/animation/WormAnimation$AnimationValues;

    move-result-object v10

    .line 51
    .local v10, "values":Lcom/rd/animation/WormAnimation$AnimationValues;
    iget-wide v4, p0, Lcom/rd/animation/WormAnimation;->animationDuration:J

    const-wide/16 v12, 0x2

    div-long v6, v4, v12

    .line 53
    .local v6, "duration":J
    iget v4, v10, Lcom/rd/animation/WormAnimation$AnimationValues;->fromX:I

    iget v5, v10, Lcom/rd/animation/WormAnimation$AnimationValues;->toX:I

    const/4 v8, 0x0

    move-object v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/rd/animation/WormAnimation;->createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;

    move-result-object v9

    .line 54
    .local v9, "straightAnimator":Landroid/animation/ValueAnimator;
    iget v4, v10, Lcom/rd/animation/WormAnimation$AnimationValues;->reverseFromX:I

    iget v5, v10, Lcom/rd/animation/WormAnimation$AnimationValues;->reverseToX:I

    const/4 v8, 0x1

    move-object v3, p0

    invoke-virtual/range {v3 .. v8}, Lcom/rd/animation/WormAnimation;->createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;

    move-result-object v2

    .line 56
    .local v2, "reverseAnimator":Landroid/animation/ValueAnimator;
    iget-object v3, p0, Lcom/rd/animation/WormAnimation;->animator:Landroid/animation/Animator;

    check-cast v3, Landroid/animation/AnimatorSet;

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/animation/Animator;

    const/4 v5, 0x0

    aput-object v9, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playSequentially([Landroid/animation/Animator;)V

    .line 58
    .end local v2    # "reverseAnimator":Landroid/animation/ValueAnimator;
    .end local v6    # "duration":J
    .end local v9    # "straightAnimator":Landroid/animation/ValueAnimator;
    .end local v10    # "values":Lcom/rd/animation/WormAnimation$AnimationValues;
    :cond_0
    return-object p0
.end method
