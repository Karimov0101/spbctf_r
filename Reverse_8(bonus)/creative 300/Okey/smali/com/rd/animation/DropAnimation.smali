.class public Lcom/rd/animation/DropAnimation;
.super Lcom/rd/animation/AbsAnimation;
.source "DropAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/rd/animation/DropAnimation$AnimationType;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/rd/animation/AbsAnimation",
        "<",
        "Landroid/animation/AnimatorSet;",
        ">;"
    }
.end annotation


# instance fields
.field private center:I

.field private frameRadius:I

.field private frameX:I

.field private frameY:I

.field private radius:I

.field private xFromValue:I

.field private xToValue:I


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/rd/animation/AbsAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/DropAnimation;ILcom/rd/animation/DropAnimation$AnimationType;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/DropAnimation;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/rd/animation/DropAnimation$AnimationType;

    .prologue
    .line 9
    invoke-direct {p0, p1, p2}, Lcom/rd/animation/DropAnimation;->onAnimatorUpdate(ILcom/rd/animation/DropAnimation$AnimationType;)V

    return-void
.end method

.method private createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;
    .locals 3
    .param p1, "fromValue"    # I
    .param p2, "toValue"    # I
    .param p3, "duration"    # J
    .param p5, "type"    # Lcom/rd/animation/DropAnimation$AnimationType;

    .prologue
    .line 113
    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 114
    .local v0, "anim":Landroid/animation/ValueAnimator;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 115
    invoke-virtual {v0, p3, p4}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 116
    new-instance v1, Lcom/rd/animation/DropAnimation$1;

    invoke-direct {v1, p0, p5}, Lcom/rd/animation/DropAnimation$1;-><init>(Lcom/rd/animation/DropAnimation;Lcom/rd/animation/DropAnimation$AnimationType;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 124
    return-object v0
.end method

.method private hasChanges(IIII)Z
    .locals 2
    .param p1, "fromValue"    # I
    .param p2, "toValue"    # I
    .param p3, "center"    # I
    .param p4, "radius"    # I

    .prologue
    const/4 v0, 0x1

    .line 149
    iget v1, p0, Lcom/rd/animation/DropAnimation;->xFromValue:I

    if-eq v1, p1, :cond_1

    .line 165
    :cond_0
    :goto_0
    return v0

    .line 153
    :cond_1
    iget v1, p0, Lcom/rd/animation/DropAnimation;->xToValue:I

    if-ne v1, p2, :cond_0

    .line 157
    iget v1, p0, Lcom/rd/animation/DropAnimation;->center:I

    if-ne v1, p3, :cond_0

    .line 161
    iget v1, p0, Lcom/rd/animation/DropAnimation;->radius:I

    if-ne v1, p4, :cond_0

    .line 165
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAnimatorUpdate(ILcom/rd/animation/DropAnimation$AnimationType;)V
    .locals 4
    .param p1, "value"    # I
    .param p2, "type"    # Lcom/rd/animation/DropAnimation$AnimationType;

    .prologue
    .line 128
    sget-object v0, Lcom/rd/animation/DropAnimation$2;->$SwitchMap$com$rd$animation$DropAnimation$AnimationType:[I

    invoke-virtual {p2}, Lcom/rd/animation/DropAnimation$AnimationType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 142
    :goto_0
    iget-object v0, p0, Lcom/rd/animation/DropAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/rd/animation/DropAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    iget v1, p0, Lcom/rd/animation/DropAnimation;->frameX:I

    iget v2, p0, Lcom/rd/animation/DropAnimation;->frameY:I

    iget v3, p0, Lcom/rd/animation/DropAnimation;->frameRadius:I

    invoke-interface {v0, v1, v2, v3}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onDropAnimationUpdated(III)V

    .line 145
    :cond_0
    return-void

    .line 130
    :pswitch_0
    iput p1, p0, Lcom/rd/animation/DropAnimation;->frameX:I

    goto :goto_0

    .line 134
    :pswitch_1
    iput p1, p0, Lcom/rd/animation/DropAnimation;->frameY:I

    goto :goto_0

    .line 138
    :pswitch_2
    iput p1, p0, Lcom/rd/animation/DropAnimation;->frameRadius:I

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/DropAnimation;->createAnimator()Landroid/animation/AnimatorSet;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/AnimatorSet;
    .locals 2
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 30
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    .line 31
    .local v0, "animator":Landroid/animation/AnimatorSet;
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 33
    return-object v0
.end method

.method public bridge synthetic duration(J)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1, p2}, Lcom/rd/animation/DropAnimation;->duration(J)Lcom/rd/animation/DropAnimation;

    move-result-object v0

    return-object v0
.end method

.method public duration(J)Lcom/rd/animation/DropAnimation;
    .locals 1
    .param p1, "duration"    # J

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/rd/animation/AbsAnimation;->duration(J)Lcom/rd/animation/AbsAnimation;

    .line 74
    return-object p0
.end method

.method public bridge synthetic progress(F)Lcom/rd/animation/AbsAnimation;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0, p1}, Lcom/rd/animation/DropAnimation;->progress(F)Lcom/rd/animation/DropAnimation;

    move-result-object v0

    return-object v0
.end method

.method public progress(F)Lcom/rd/animation/DropAnimation;
    .locals 12
    .param p1, "progress"    # F

    .prologue
    .line 38
    iget-object v7, p0, Lcom/rd/animation/DropAnimation;->animator:Landroid/animation/Animator;

    if-eqz v7, :cond_4

    .line 39
    iget-wide v10, p0, Lcom/rd/animation/DropAnimation;->animationDuration:J

    long-to-float v7, v10

    mul-float/2addr v7, p1

    float-to-long v8, v7

    .line 40
    .local v8, "playTimeLeft":J
    const/4 v6, 0x0

    .line 42
    .local v6, "isReverse":Z
    iget-object v7, p0, Lcom/rd/animation/DropAnimation;->animator:Landroid/animation/Animator;

    check-cast v7, Landroid/animation/AnimatorSet;

    invoke-virtual {v7}, Landroid/animation/AnimatorSet;->getChildAnimations()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/Animator;

    .local v0, "anim":Landroid/animation/Animator;
    move-object v1, v0

    .line 43
    check-cast v1, Landroid/animation/ValueAnimator;

    .line 44
    .local v1, "animator":Landroid/animation/ValueAnimator;
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    .line 45
    .local v2, "animDuration":J
    move-wide v4, v8

    .line 47
    .local v4, "currPlayTime":J
    if-eqz v6, :cond_1

    .line 48
    sub-long/2addr v4, v2

    .line 51
    :cond_1
    const-wide/16 v10, 0x0

    cmp-long v10, v4, v10

    if-ltz v10, :cond_0

    .line 54
    cmp-long v10, v4, v2

    if-ltz v10, :cond_2

    .line 55
    move-wide v4, v2

    .line 58
    :cond_2
    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->getValues()[Landroid/animation/PropertyValuesHolder;

    move-result-object v10

    array-length v10, v10

    if-lez v10, :cond_3

    .line 59
    invoke-virtual {v1, v4, v5}, Landroid/animation/ValueAnimator;->setCurrentPlayTime(J)V

    .line 62
    :cond_3
    if-nez v6, :cond_0

    iget-wide v10, p0, Lcom/rd/animation/DropAnimation;->animationDuration:J

    cmp-long v10, v2, v10

    if-ltz v10, :cond_0

    .line 63
    const/4 v6, 0x1

    goto :goto_0

    .line 68
    .end local v0    # "anim":Landroid/animation/Animator;
    .end local v1    # "animator":Landroid/animation/ValueAnimator;
    .end local v2    # "animDuration":J
    .end local v4    # "currPlayTime":J
    .end local v6    # "isReverse":Z
    .end local v8    # "playTimeLeft":J
    :cond_4
    return-object p0
.end method

.method public with(IIII)Lcom/rd/animation/DropAnimation;
    .locals 19
    .param p1, "xFromValue"    # I
    .param p2, "xToValue"    # I
    .param p3, "center"    # I
    .param p4, "radius"    # I

    .prologue
    .line 79
    invoke-direct/range {p0 .. p4}, Lcom/rd/animation/DropAnimation;->hasChanges(IIII)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/rd/animation/DropAnimation;->createAnimator()Landroid/animation/AnimatorSet;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/rd/animation/DropAnimation;->animator:Landroid/animation/Animator;

    .line 82
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->xFromValue:I

    .line 83
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->xToValue:I

    .line 85
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->center:I

    .line 86
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->radius:I

    .line 88
    move/from16 v17, p3

    .line 89
    .local v17, "yFromValue":I
    div-int/lit8 v18, p3, 0x3

    .line 91
    .local v18, "yToValue":I
    move/from16 v2, p4

    .line 92
    .local v2, "fromSelectedRadius":I
    move/from16 v0, p4

    int-to-double v4, v0

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    div-double/2addr v4, v6

    double-to-int v15, v4

    .line 93
    .local v15, "toSelectedRadius":I
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/rd/animation/DropAnimation;->animationDuration:J

    const-wide/16 v6, 0x2

    div-long v10, v4, v6

    .line 95
    .local v10, "halfDuration":J
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->frameX:I

    .line 96
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->frameY:I

    .line 97
    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/rd/animation/DropAnimation;->frameRadius:I

    .line 99
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/rd/animation/DropAnimation;->animationDuration:J

    sget-object v8, Lcom/rd/animation/DropAnimation$AnimationType;->Width:Lcom/rd/animation/DropAnimation$AnimationType;

    move-object/from16 v3, p0

    move/from16 v4, p1

    move/from16 v5, p2

    invoke-direct/range {v3 .. v8}, Lcom/rd/animation/DropAnimation;->createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;

    move-result-object v16

    .line 100
    .local v16, "widthAnimator":Landroid/animation/ValueAnimator;
    sget-object v8, Lcom/rd/animation/DropAnimation$AnimationType;->Height:Lcom/rd/animation/DropAnimation$AnimationType;

    move-object/from16 v3, p0

    move/from16 v4, v17

    move/from16 v5, v18

    move-wide v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/rd/animation/DropAnimation;->createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;

    move-result-object v12

    .line 101
    .local v12, "heightForwardAnimator":Landroid/animation/ValueAnimator;
    sget-object v8, Lcom/rd/animation/DropAnimation$AnimationType;->Height:Lcom/rd/animation/DropAnimation$AnimationType;

    move-object/from16 v3, p0

    move/from16 v4, v18

    move/from16 v5, v17

    move-wide v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/rd/animation/DropAnimation;->createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;

    move-result-object v9

    .line 103
    .local v9, "heightBackwardAnimator":Landroid/animation/ValueAnimator;
    sget-object v8, Lcom/rd/animation/DropAnimation$AnimationType;->Radius:Lcom/rd/animation/DropAnimation$AnimationType;

    move-object/from16 v3, p0

    move v4, v2

    move v5, v15

    move-wide v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/rd/animation/DropAnimation;->createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;

    move-result-object v14

    .line 104
    .local v14, "radiusForwardAnimator":Landroid/animation/ValueAnimator;
    sget-object v8, Lcom/rd/animation/DropAnimation$AnimationType;->Radius:Lcom/rd/animation/DropAnimation$AnimationType;

    move-object/from16 v3, p0

    move v4, v15

    move v5, v2

    move-wide v6, v10

    invoke-direct/range {v3 .. v8}, Lcom/rd/animation/DropAnimation;->createValueAnimation(IIJLcom/rd/animation/DropAnimation$AnimationType;)Landroid/animation/ValueAnimator;

    move-result-object v13

    .line 106
    .local v13, "radiusBackwardAnimator":Landroid/animation/ValueAnimator;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/rd/animation/DropAnimation;->animator:Landroid/animation/Animator;

    check-cast v3, Landroid/animation/AnimatorSet;

    invoke-virtual {v3, v12}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v14}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v13}, Landroid/animation/AnimatorSet$Builder;->before(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    .line 109
    .end local v2    # "fromSelectedRadius":I
    .end local v9    # "heightBackwardAnimator":Landroid/animation/ValueAnimator;
    .end local v10    # "halfDuration":J
    .end local v12    # "heightForwardAnimator":Landroid/animation/ValueAnimator;
    .end local v13    # "radiusBackwardAnimator":Landroid/animation/ValueAnimator;
    .end local v14    # "radiusForwardAnimator":Landroid/animation/ValueAnimator;
    .end local v15    # "toSelectedRadius":I
    .end local v16    # "widthAnimator":Landroid/animation/ValueAnimator;
    .end local v17    # "yFromValue":I
    .end local v18    # "yToValue":I
    :cond_0
    return-object p0
.end method
