.class Lcom/rd/animation/ThinWormAnimation$1;
.super Ljava/lang/Object;
.source "ThinWormAnimation.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rd/animation/ThinWormAnimation;->createHeightAnimator(IIJ)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rd/animation/ThinWormAnimation;


# direct methods
.method constructor <init>(Lcom/rd/animation/ThinWormAnimation;)V
    .locals 0
    .param p1, "this$0"    # Lcom/rd/animation/ThinWormAnimation;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 64
    iget-object v1, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Lcom/rd/animation/ThinWormAnimation;->access$002(Lcom/rd/animation/ThinWormAnimation;I)I

    .line 65
    iget-object v0, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    iget-object v0, v0, Lcom/rd/animation/ThinWormAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    iget-object v1, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    iget v1, v1, Lcom/rd/animation/ThinWormAnimation;->rectLeftX:I

    iget-object v2, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    iget v2, v2, Lcom/rd/animation/ThinWormAnimation;->rectRightX:I

    iget-object v3, p0, Lcom/rd/animation/ThinWormAnimation$1;->this$0:Lcom/rd/animation/ThinWormAnimation;

    invoke-static {v3}, Lcom/rd/animation/ThinWormAnimation;->access$000(Lcom/rd/animation/ThinWormAnimation;)I

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onThinWormAnimationUpdated(III)V

    .line 66
    return-void
.end method
