.class Lcom/rd/animation/WormAnimation$1;
.super Ljava/lang/Object;
.source "WormAnimation.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rd/animation/WormAnimation;->createWormAnimator(IIJZ)Landroid/animation/ValueAnimator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rd/animation/WormAnimation;

.field final synthetic val$isReverse:Z


# direct methods
.method constructor <init>(Lcom/rd/animation/WormAnimation;Z)V
    .locals 0
    .param p1, "this$0"    # Lcom/rd/animation/WormAnimation;

    .prologue
    .line 93
    iput-object p1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iput-boolean p2, p0, Lcom/rd/animation/WormAnimation$1;->val$isReverse:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 96
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 98
    .local v0, "value":I
    iget-boolean v1, p0, Lcom/rd/animation/WormAnimation$1;->val$isReverse:Z

    if-nez v1, :cond_1

    .line 99
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iget-boolean v1, v1, Lcom/rd/animation/WormAnimation;->isRightSide:Z

    if-eqz v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iput v0, v1, Lcom/rd/animation/WormAnimation;->rectRightX:I

    .line 113
    :goto_0
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iget-object v1, v1, Lcom/rd/animation/WormAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    iget-object v2, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iget v2, v2, Lcom/rd/animation/WormAnimation;->rectLeftX:I

    iget-object v3, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iget v3, v3, Lcom/rd/animation/WormAnimation;->rectRightX:I

    invoke-interface {v1, v2, v3}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onWormAnimationUpdated(II)V

    .line 114
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iput v0, v1, Lcom/rd/animation/WormAnimation;->rectLeftX:I

    goto :goto_0

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iget-boolean v1, v1, Lcom/rd/animation/WormAnimation;->isRightSide:Z

    if-eqz v1, :cond_2

    .line 107
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iput v0, v1, Lcom/rd/animation/WormAnimation;->rectLeftX:I

    goto :goto_0

    .line 109
    :cond_2
    iget-object v1, p0, Lcom/rd/animation/WormAnimation$1;->this$0:Lcom/rd/animation/WormAnimation;

    iput v0, v1, Lcom/rd/animation/WormAnimation;->rectRightX:I

    goto :goto_0
.end method
