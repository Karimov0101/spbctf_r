.class public Lcom/rd/animation/ScaleAnimation;
.super Lcom/rd/animation/ColorAnimation;
.source "ScaleAnimation.java"


# static fields
.field private static final ANIMATION_COLOR:Ljava/lang/String; = "ANIMATION_COLOR"

.field private static final ANIMATION_COLOR_REVERSE:Ljava/lang/String; = "ANIMATION_COLOR_REVERSE"

.field private static final ANIMATION_SCALE:Ljava/lang/String; = "ANIMATION_SCALE"

.field private static final ANIMATION_SCALE_REVERSE:Ljava/lang/String; = "ANIMATION_SCALE_REVERSE"

.field public static final DEFAULT_SCALE_FACTOR:F = 0.7f

.field public static final MAX_SCALE_FACTOR:F = 1.0f

.field public static final MIN_SCALE_FACTOR:F = 0.3f


# instance fields
.field private radiusPx:I

.field private scaleFactor:F


# direct methods
.method public constructor <init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/rd/animation/ValueAnimation$UpdateListener;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/rd/animation/ColorAnimation;-><init>(Lcom/rd/animation/ValueAnimation$UpdateListener;)V

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/rd/animation/ScaleAnimation;Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p0, "x0"    # Lcom/rd/animation/ScaleAnimation;
    .param p1, "x1"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 9
    invoke-direct {p0, p1}, Lcom/rd/animation/ScaleAnimation;->onAnimateUpdated(Landroid/animation/ValueAnimator;)V

    return-void
.end method

.method private createScalePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;
    .locals 6
    .param p1, "isReverse"    # Z
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 83
    if-eqz p1, :cond_0

    .line 84
    const-string v2, "ANIMATION_SCALE_REVERSE"

    .line 85
    .local v2, "propertyName":Ljava/lang/String;
    iget v3, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    .line 86
    .local v3, "startRadiusValue":I
    iget v4, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    int-to-float v4, v4

    iget v5, p0, Lcom/rd/animation/ScaleAnimation;->scaleFactor:F

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 93
    .local v0, "endRadiusValue":I
    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [I

    const/4 v5, 0x0

    aput v3, v4, v5

    const/4 v5, 0x1

    aput v0, v4, v5

    invoke-static {v2, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 94
    .local v1, "holder":Landroid/animation/PropertyValuesHolder;
    new-instance v4, Landroid/animation/IntEvaluator;

    invoke-direct {v4}, Landroid/animation/IntEvaluator;-><init>()V

    invoke-virtual {v1, v4}, Landroid/animation/PropertyValuesHolder;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 96
    return-object v1

    .line 88
    .end local v0    # "endRadiusValue":I
    .end local v1    # "holder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "propertyName":Ljava/lang/String;
    .end local v3    # "startRadiusValue":I
    :cond_0
    const-string v2, "ANIMATION_SCALE"

    .line 89
    .restart local v2    # "propertyName":Ljava/lang/String;
    iget v4, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    int-to-float v4, v4

    iget v5, p0, Lcom/rd/animation/ScaleAnimation;->scaleFactor:F

    mul-float/2addr v4, v5

    float-to-int v3, v4

    .line 90
    .restart local v3    # "startRadiusValue":I
    iget v0, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    .restart local v0    # "endRadiusValue":I
    goto :goto_0
.end method

.method private hasChanges(IIIF)Z
    .locals 2
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I
    .param p3, "radiusValue"    # I
    .param p4, "scaleFactorValue"    # F

    .prologue
    const/4 v0, 0x1

    .line 101
    iget v1, p0, Lcom/rd/animation/ScaleAnimation;->startColor:I

    if-eq v1, p1, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 105
    :cond_1
    iget v1, p0, Lcom/rd/animation/ScaleAnimation;->endColor:I

    if-ne v1, p2, :cond_0

    .line 109
    iget v1, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    if-ne v1, p3, :cond_0

    .line 113
    iget v1, p0, Lcom/rd/animation/ScaleAnimation;->scaleFactor:F

    cmpl-float v1, v1, p4

    if-nez v1, :cond_0

    .line 117
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAnimateUpdated(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1, "animation"    # Landroid/animation/ValueAnimator;
        .annotation build Landroid/support/annotation/NonNull;
        .end annotation
    .end param

    .prologue
    .line 66
    const-string v4, "ANIMATION_COLOR"

    invoke-virtual {p1, v4}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 67
    .local v0, "color":I
    const-string v4, "ANIMATION_COLOR_REVERSE"

    invoke-virtual {p1, v4}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 69
    .local v1, "colorReverse":I
    const-string v4, "ANIMATION_SCALE"

    invoke-virtual {p1, v4}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 70
    .local v2, "radius":I
    const-string v4, "ANIMATION_SCALE_REVERSE"

    invoke-virtual {p1, v4}, Landroid/animation/ValueAnimator;->getAnimatedValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 72
    .local v3, "radiusReverse":I
    iget-object v4, p0, Lcom/rd/animation/ScaleAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    if-eqz v4, :cond_0

    .line 73
    iget-object v4, p0, Lcom/rd/animation/ScaleAnimation;->listener:Lcom/rd/animation/ValueAnimation$UpdateListener;

    invoke-interface {v4, v0, v1, v2, v3}, Lcom/rd/animation/ValueAnimation$UpdateListener;->onScaleAnimationUpdated(IIII)V

    .line 75
    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic createAnimator()Landroid/animation/Animator;
    .locals 1
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 9
    invoke-virtual {p0}, Lcom/rd/animation/ScaleAnimation;->createAnimator()Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method public createAnimator()Landroid/animation/ValueAnimator;
    .locals 4
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    .line 31
    new-instance v0, Landroid/animation/ValueAnimator;

    invoke-direct {v0}, Landroid/animation/ValueAnimator;-><init>()V

    .line 32
    .local v0, "animator":Landroid/animation/ValueAnimator;
    const-wide/16 v2, 0x15e

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 33
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 34
    new-instance v1, Lcom/rd/animation/ScaleAnimation$1;

    invoke-direct {v1, p0}, Lcom/rd/animation/ScaleAnimation$1;-><init>(Lcom/rd/animation/ScaleAnimation;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 41
    return-object v0
.end method

.method public with(IIIF)Lcom/rd/animation/ScaleAnimation;
    .locals 8
    .param p1, "colorStartValue"    # I
    .param p2, "colorEndValue"    # I
    .param p3, "radiusValue"    # I
    .param p4, "scaleFactorValue"    # F
    .annotation build Landroid/support/annotation/NonNull;
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 46
    iget-object v4, p0, Lcom/rd/animation/ScaleAnimation;->animator:Landroid/animation/Animator;

    if-eqz v4, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/rd/animation/ScaleAnimation;->hasChanges(IIIF)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 48
    iput p1, p0, Lcom/rd/animation/ScaleAnimation;->startColor:I

    .line 49
    iput p2, p0, Lcom/rd/animation/ScaleAnimation;->endColor:I

    .line 50
    iput p3, p0, Lcom/rd/animation/ScaleAnimation;->radiusPx:I

    .line 51
    iput p4, p0, Lcom/rd/animation/ScaleAnimation;->scaleFactor:F

    .line 53
    invoke-virtual {p0, v6}, Lcom/rd/animation/ScaleAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 54
    .local v0, "colorHolder":Landroid/animation/PropertyValuesHolder;
    invoke-virtual {p0, v7}, Lcom/rd/animation/ScaleAnimation;->createColorPropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v1

    .line 56
    .local v1, "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v6}, Lcom/rd/animation/ScaleAnimation;->createScalePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v2

    .line 57
    .local v2, "scaleHolder":Landroid/animation/PropertyValuesHolder;
    invoke-direct {p0, v7}, Lcom/rd/animation/ScaleAnimation;->createScalePropertyHolder(Z)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 59
    .local v3, "scaleReverseHolder":Landroid/animation/PropertyValuesHolder;
    iget-object v4, p0, Lcom/rd/animation/ScaleAnimation;->animator:Landroid/animation/Animator;

    check-cast v4, Landroid/animation/ValueAnimator;

    const/4 v5, 0x4

    new-array v5, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v5, v6

    aput-object v1, v5, v7

    const/4 v6, 0x2

    aput-object v2, v5, v6

    const/4 v6, 0x3

    aput-object v3, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/ValueAnimator;->setValues([Landroid/animation/PropertyValuesHolder;)V

    .line 62
    .end local v0    # "colorHolder":Landroid/animation/PropertyValuesHolder;
    .end local v1    # "reverseColorHolder":Landroid/animation/PropertyValuesHolder;
    .end local v2    # "scaleHolder":Landroid/animation/PropertyValuesHolder;
    .end local v3    # "scaleReverseHolder":Landroid/animation/PropertyValuesHolder;
    :cond_0
    return-object p0
.end method
