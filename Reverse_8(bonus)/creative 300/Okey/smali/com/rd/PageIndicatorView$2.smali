.class Lcom/rd/PageIndicatorView$2;
.super Landroid/database/DataSetObserver;
.source "PageIndicatorView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rd/PageIndicatorView;->registerSetObserver()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rd/PageIndicatorView;


# direct methods
.method constructor <init>(Lcom/rd/PageIndicatorView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 1268
    iput-object p1, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 5

    .prologue
    .line 1271
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1100(Lcom/rd/PageIndicatorView;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1100(Lcom/rd/PageIndicatorView;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1273
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1100(Lcom/rd/PageIndicatorView;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/PagerAdapter;->getCount()I

    move-result v1

    .line 1274
    .local v1, "newCount":I
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1100(Lcom/rd/PageIndicatorView;)Landroid/support/v4/view/ViewPager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    .line 1276
    .local v0, "currItem":I
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2, v0}, Lcom/rd/PageIndicatorView;->access$1202(Lcom/rd/PageIndicatorView;I)I

    .line 1277
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2, v0}, Lcom/rd/PageIndicatorView;->access$1302(Lcom/rd/PageIndicatorView;I)I

    .line 1278
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2, v0}, Lcom/rd/PageIndicatorView;->access$1402(Lcom/rd/PageIndicatorView;I)I

    .line 1280
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1500(Lcom/rd/PageIndicatorView;)V

    .line 1281
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v2, v1}, Lcom/rd/PageIndicatorView;->setCount(I)V

    .line 1282
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v2}, Lcom/rd/PageIndicatorView;->access$1600(Lcom/rd/PageIndicatorView;)V

    .line 1283
    iget-object v2, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    iget-object v3, p0, Lcom/rd/PageIndicatorView$2;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v3}, Lcom/rd/PageIndicatorView;->access$1300(Lcom/rd/PageIndicatorView;)I

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4}, Lcom/rd/PageIndicatorView;->setProgress(IF)V

    .line 1285
    .end local v0    # "currItem":I
    .end local v1    # "newCount":I
    :cond_0
    return-void
.end method
