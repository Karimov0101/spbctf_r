.class Lcom/rd/PageIndicatorView$1;
.super Ljava/lang/Object;
.source "PageIndicatorView.java"

# interfaces
.implements Lcom/rd/animation/ValueAnimation$UpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/rd/PageIndicatorView;->initAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/rd/PageIndicatorView;


# direct methods
.method constructor <init>(Lcom/rd/PageIndicatorView;)V
    .locals 0
    .param p1, "this$0"    # Lcom/rd/PageIndicatorView;

    .prologue
    .line 1011
    iput-object p1, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onColorAnimationUpdated(II)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "colorReverse"    # I

    .prologue
    .line 1014
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$002(Lcom/rd/PageIndicatorView;I)I

    .line 1015
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$102(Lcom/rd/PageIndicatorView;I)I

    .line 1016
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1017
    return-void
.end method

.method public onDropAnimationUpdated(III)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "selectedRadius"    # I

    .prologue
    .line 1065
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$402(Lcom/rd/PageIndicatorView;I)I

    .line 1066
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$1002(Lcom/rd/PageIndicatorView;I)I

    .line 1067
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p3}, Lcom/rd/PageIndicatorView;->access$202(Lcom/rd/PageIndicatorView;I)I

    .line 1068
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1069
    return-void
.end method

.method public onFillAnimationUpdated(IIIIII)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "colorReverse"    # I
    .param p3, "radius"    # I
    .param p4, "radiusReverse"    # I
    .param p5, "stroke"    # I
    .param p6, "strokeReverse"    # I

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$002(Lcom/rd/PageIndicatorView;I)I

    .line 1053
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$102(Lcom/rd/PageIndicatorView;I)I

    .line 1055
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p3}, Lcom/rd/PageIndicatorView;->access$202(Lcom/rd/PageIndicatorView;I)I

    .line 1056
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p4}, Lcom/rd/PageIndicatorView;->access$302(Lcom/rd/PageIndicatorView;I)I

    .line 1058
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p5}, Lcom/rd/PageIndicatorView;->access$802(Lcom/rd/PageIndicatorView;I)I

    .line 1059
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p6}, Lcom/rd/PageIndicatorView;->access$902(Lcom/rd/PageIndicatorView;I)I

    .line 1060
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1061
    return-void
.end method

.method public onScaleAnimationUpdated(IIII)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "colorReverse"    # I
    .param p3, "radius"    # I
    .param p4, "radiusReverse"    # I

    .prologue
    .line 1021
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$002(Lcom/rd/PageIndicatorView;I)I

    .line 1022
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$102(Lcom/rd/PageIndicatorView;I)I

    .line 1024
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p3}, Lcom/rd/PageIndicatorView;->access$202(Lcom/rd/PageIndicatorView;I)I

    .line 1025
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p4}, Lcom/rd/PageIndicatorView;->access$302(Lcom/rd/PageIndicatorView;I)I

    .line 1026
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1027
    return-void
.end method

.method public onSlideAnimationUpdated(I)V
    .locals 1
    .param p1, "xCoordinate"    # I

    .prologue
    .line 1031
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$402(Lcom/rd/PageIndicatorView;I)I

    .line 1032
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1033
    return-void
.end method

.method public onSwapAnimationUpdated(I)V
    .locals 1
    .param p1, "xCoordinate"    # I

    .prologue
    .line 1073
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$402(Lcom/rd/PageIndicatorView;I)I

    .line 1074
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1075
    return-void
.end method

.method public onThinWormAnimationUpdated(III)V
    .locals 1
    .param p1, "leftX"    # I
    .param p2, "rightX"    # I
    .param p3, "height"    # I

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$502(Lcom/rd/PageIndicatorView;I)I

    .line 1045
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$602(Lcom/rd/PageIndicatorView;I)I

    .line 1046
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p3}, Lcom/rd/PageIndicatorView;->access$702(Lcom/rd/PageIndicatorView;I)I

    .line 1047
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1048
    return-void
.end method

.method public onWormAnimationUpdated(II)V
    .locals 1
    .param p1, "leftX"    # I
    .param p2, "rightX"    # I

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p1}, Lcom/rd/PageIndicatorView;->access$502(Lcom/rd/PageIndicatorView;I)I

    .line 1038
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-static {v0, p2}, Lcom/rd/PageIndicatorView;->access$602(Lcom/rd/PageIndicatorView;I)I

    .line 1039
    iget-object v0, p0, Lcom/rd/PageIndicatorView$1;->this$0:Lcom/rd/PageIndicatorView;

    invoke-virtual {v0}, Lcom/rd/PageIndicatorView;->invalidate()V

    .line 1040
    return-void
.end method
