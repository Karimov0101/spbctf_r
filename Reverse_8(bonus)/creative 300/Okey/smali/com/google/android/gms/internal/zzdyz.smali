.class public interface abstract Lcom/google/android/gms/internal/zzdyz;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# virtual methods
.method public abstract getKey()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TK;"
        }
    .end annotation
.end method

.method public abstract getValue()Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract size()I
.end method

.method public abstract zza(Ljava/lang/Object;Ljava/lang/Object;ILcom/google/android/gms/internal/zzdyz;Lcom/google/android/gms/internal/zzdyz;)Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Ljava/lang/Integer;",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;)",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zza(Ljava/lang/Object;Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;",
            "Ljava/util/Comparator",
            "<TK;>;)",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zza(Ljava/lang/Object;Ljava/util/Comparator;)Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;",
            "Ljava/util/Comparator",
            "<TK;>;)",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zza(Lcom/google/android/gms/internal/zzdzb;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/internal/zzdzb",
            "<TK;TV;>;)V"
        }
    .end annotation
.end method

.method public abstract zzbrp()Z
.end method

.method public abstract zzbrr()Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zzbrs()Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zzbrt()Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method

.method public abstract zzbru()Lcom/google/android/gms/internal/zzdyz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/gms/internal/zzdyz",
            "<TK;TV;>;"
        }
    .end annotation
.end method
