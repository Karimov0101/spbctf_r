.class final Lorg/solovyev/android/checkout/Checkout$Listeners;
.super Ljava/lang/Object;
.source "Checkout.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Checkout$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Checkout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Listeners"
.end annotation


# instance fields
.field private final mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Checkout$Listener;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 400
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 401
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    return-void
.end method

.method synthetic constructor <init>(Lorg/solovyev/android/checkout/Checkout$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/solovyev/android/checkout/Checkout$1;

    .prologue
    .line 400
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Checkout$Listeners;-><init>()V

    return-void
.end method


# virtual methods
.method public add(Lorg/solovyev/android/checkout/Checkout$Listener;)V
    .locals 1
    .param p1, "l"    # Lorg/solovyev/android/checkout/Checkout$Listener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 405
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 406
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 428
    return-void
.end method

.method public onReady(Lorg/solovyev/android/checkout/BillingRequests;)V
    .locals 4
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 412
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 413
    .local v1, "localList":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Checkout$Listener;>;"
    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 414
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Checkout$Listener;

    .line 415
    .local v0, "listener":Lorg/solovyev/android/checkout/Checkout$Listener;
    invoke-interface {v0, p1}, Lorg/solovyev/android/checkout/Checkout$Listener;->onReady(Lorg/solovyev/android/checkout/BillingRequests;)V

    goto :goto_0

    .line 417
    .end local v0    # "listener":Lorg/solovyev/android/checkout/Checkout$Listener;
    :cond_0
    return-void
.end method

.method public onReady(Lorg/solovyev/android/checkout/BillingRequests;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "billingSupported"    # Z

    .prologue
    .line 421
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout$Listeners;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Checkout$Listener;

    .line 422
    .local v0, "listener":Lorg/solovyev/android/checkout/Checkout$Listener;
    invoke-interface {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/Checkout$Listener;->onReady(Lorg/solovyev/android/checkout/BillingRequests;Ljava/lang/String;Z)V

    goto :goto_0

    .line 424
    .end local v0    # "listener":Lorg/solovyev/android/checkout/Checkout$Listener;
    :cond_0
    return-void
.end method
