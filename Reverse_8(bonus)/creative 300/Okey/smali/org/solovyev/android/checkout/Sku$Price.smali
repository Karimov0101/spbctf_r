.class public final Lorg/solovyev/android/checkout/Sku$Price;
.super Ljava/lang/Object;
.source "Sku.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Sku;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Price"
.end annotation


# static fields
.field public static final EMPTY:Lorg/solovyev/android/checkout/Sku$Price;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# instance fields
.field public final amount:J

.field public final currency:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 230
    new-instance v0, Lorg/solovyev/android/checkout/Sku$Price;

    const-wide/16 v2, 0x0

    const-string v1, ""

    invoke-direct {v0, v2, v3, v1}, Lorg/solovyev/android/checkout/Sku$Price;-><init>(JLjava/lang/String;)V

    sput-object v0, Lorg/solovyev/android/checkout/Sku$Price;->EMPTY:Lorg/solovyev/android/checkout/Sku$Price;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;)V
    .locals 1
    .param p1, "amount"    # J
    .param p3, "currency"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 242
    iput-wide p1, p0, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    .line 243
    iput-object p3, p0, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    .line 244
    return-void
.end method

.method static synthetic access$000(Lorg/json/JSONObject;)Lorg/solovyev/android/checkout/Sku$Price;
    .locals 1
    .param p0, "x0"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 227
    invoke-static {p0}, Lorg/solovyev/android/checkout/Sku$Price;->fromJson(Lorg/json/JSONObject;)Lorg/solovyev/android/checkout/Sku$Price;

    move-result-object v0

    return-object v0
.end method

.method private static fromJson(Lorg/json/JSONObject;)Lorg/solovyev/android/checkout/Sku$Price;
    .locals 6
    .param p0, "json"    # Lorg/json/JSONObject;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 248
    const-string v3, "price_amount_micros"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 249
    .local v0, "amount":J
    const-string v3, "price_currency_code"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 250
    .local v2, "currency":Ljava/lang/String;
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 251
    :cond_0
    sget-object v3, Lorg/solovyev/android/checkout/Sku$Price;->EMPTY:Lorg/solovyev/android/checkout/Sku$Price;

    .line 253
    :goto_0
    return-object v3

    :cond_1
    new-instance v3, Lorg/solovyev/android/checkout/Sku$Price;

    invoke-direct {v3, v0, v1, v2}, Lorg/solovyev/android/checkout/Sku$Price;-><init>(JLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public isValid()Z
    .locals 4

    .prologue
    .line 261
    iget-wide v0, p0, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 266
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
