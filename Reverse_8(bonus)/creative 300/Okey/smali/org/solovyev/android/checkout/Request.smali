.class abstract Lorg/solovyev/android/checkout/Request;
.super Ljava/lang/Object;
.source "Request.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final sCounter:Ljava/util/concurrent/atomic/AtomicInteger;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# instance fields
.field protected final mApiVersion:I

.field private final mId:I

.field private mListener:Lorg/solovyev/android/checkout/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private mListenerCalled:Z
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private mTag:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mType:Lorg/solovyev/android/checkout/RequestType;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lorg/solovyev/android/checkout/Request;->sCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method constructor <init>(Lorg/solovyev/android/checkout/RequestType;)V
    .locals 1
    .param p1, "type"    # Lorg/solovyev/android/checkout/RequestType;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 60
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;I)V

    .line 61
    return-void
.end method

.method constructor <init>(Lorg/solovyev/android/checkout/RequestType;I)V
    .locals 1
    .param p1, "type"    # Lorg/solovyev/android/checkout/RequestType;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "apiVersion"    # I

    .prologue
    .line 63
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lorg/solovyev/android/checkout/Request;->mType:Lorg/solovyev/android/checkout/RequestType;

    .line 65
    iput p2, p0, Lorg/solovyev/android/checkout/Request;->mApiVersion:I

    .line 66
    sget-object v0, Lorg/solovyev/android/checkout/Request;->sCounter:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lorg/solovyev/android/checkout/Request;->mId:I

    .line 67
    return-void
.end method

.method constructor <init>(Lorg/solovyev/android/checkout/RequestType;Lorg/solovyev/android/checkout/Request;)V
    .locals 1
    .param p1, "type"    # Lorg/solovyev/android/checkout/RequestType;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestType;",
            "Lorg/solovyev/android/checkout/Request",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 69
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    .local p2, "request":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lorg/solovyev/android/checkout/Request;->mType:Lorg/solovyev/android/checkout/RequestType;

    .line 71
    iget v0, p2, Lorg/solovyev/android/checkout/Request;->mId:I

    iput v0, p0, Lorg/solovyev/android/checkout/Request;->mId:I

    .line 72
    iget v0, p2, Lorg/solovyev/android/checkout/Request;->mApiVersion:I

    iput v0, p0, Lorg/solovyev/android/checkout/Request;->mApiVersion:I

    .line 73
    monitor-enter p2

    .line 74
    :try_start_0
    iget-object v0, p2, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    iput-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 75
    monitor-exit p2

    .line 76
    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private checkListenerCalled()Z
    .locals 2

    .prologue
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    const/4 v0, 0x1

    .line 135
    monitor-enter p0

    .line 136
    :try_start_0
    iget-boolean v1, p0, Lorg/solovyev/android/checkout/Request;->mListenerCalled:Z

    if-eqz v1, :cond_0

    .line 137
    monitor-exit p0

    .line 141
    :goto_0
    return v0

    .line 139
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/solovyev/android/checkout/Request;->mListenerCalled:Z

    .line 140
    monitor-exit p0

    .line 141
    const/4 v0, 0x0

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private onError(ILjava/lang/Exception;)V
    .locals 2
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 156
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    const/4 v1, 0x0

    invoke-static {v1, p1}, Lorg/solovyev/android/checkout/Check;->notEquals(II)V

    .line 157
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Request;->getListener()Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    .line 158
    .local v0, "l":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    if-eqz v0, :cond_0

    .line 159
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Request;->checkListenerCalled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/RequestListener;->onError(ILjava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method cancel()V
    .locals 1

    .prologue
    .line 109
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    monitor-enter p0

    .line 110
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    if-eqz v0, :cond_0

    .line 111
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->cancel(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 113
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 114
    monitor-exit p0

    .line 115
    return-void

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method abstract getCacheKey()Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end method

.method protected getId()I
    .locals 1

    .prologue
    .line 82
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    iget v0, p0, Lorg/solovyev/android/checkout/Request;->mId:I

    return v0
.end method

.method getListener()Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 179
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    monitor-enter p0

    .line 180
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    monitor-exit p0

    return-object v0

    .line 181
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method getTag()Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 93
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method getType()Lorg/solovyev/android/checkout/RequestType;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 102
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mType:Lorg/solovyev/android/checkout/RequestType;

    return-object v0
.end method

.method protected final handleError(I)Z
    .locals 1
    .param p1, "response"    # I

    .prologue
    .line 170
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    if-eqz p1, :cond_0

    .line 171
    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/Request;->onError(I)V

    .line 172
    const/4 v0, 0x1

    .line 174
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final handleError(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 165
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    if-eqz p1, :cond_0

    const-string v1, "RESPONSE_CODE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 166
    .local v0, "response":I
    :goto_0
    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Request;->handleError(I)Z

    move-result v1

    return v1

    .line 165
    .end local v0    # "response":I
    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method isCancelled()Z
    .locals 1

    .prologue
    .line 121
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    monitor-enter p0

    .line 122
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onError(I)V
    .locals 2
    .param p1, "response"    # I

    .prologue
    .line 145
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error response: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lorg/solovyev/android/checkout/ResponseCodes;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;)V

    .line 146
    new-instance v0, Lorg/solovyev/android/checkout/BillingException;

    invoke-direct {v0, p1}, Lorg/solovyev/android/checkout/BillingException;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lorg/solovyev/android/checkout/Request;->onError(ILjava/lang/Exception;)V

    .line 147
    return-void
.end method

.method public onError(Ljava/lang/Exception;)V
    .locals 2
    .param p1, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 150
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    instance-of v0, p1, Lorg/solovyev/android/checkout/BillingException;

    const-string v1, "Use onError(int) instead"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isFalse(ZLjava/lang/String;)V

    .line 151
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Exception in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " request: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 152
    const/16 v0, 0x2711

    invoke-direct {p0, v0, p1}, Lorg/solovyev/android/checkout/Request;->onError(ILjava/lang/Exception;)V

    .line 153
    return-void
.end method

.method protected onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Request;->getListener()Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    .line 128
    .local v0, "l":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Request;->checkListenerCalled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    invoke-interface {v0, p1}, Lorg/solovyev/android/checkout/RequestListener;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method setListener(Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 185
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    monitor-enter p0

    .line 186
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Check;->isNull(Ljava/lang/Object;)V

    .line 187
    iput-object p1, p0, Lorg/solovyev/android/checkout/Request;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 188
    monitor-exit p0

    .line 189
    return-void

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method setTag(Ljava/lang/Object;)V
    .locals 0
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 97
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    iput-object p1, p0, Lorg/solovyev/android/checkout/Request;->mTag:Ljava/lang/Object;

    .line 98
    return-void
.end method

.method abstract start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    .param p1    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/solovyev/android/checkout/RequestException;
        }
    .end annotation
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 193
    .local p0, "this":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Request;->getCacheKey()Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "cacheKey":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 195
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 197
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
