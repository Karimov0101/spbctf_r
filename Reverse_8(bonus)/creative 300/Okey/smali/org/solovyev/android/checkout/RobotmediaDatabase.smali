.class public final Lorg/solovyev/android/checkout/RobotmediaDatabase;
.super Ljava/lang/Object;
.source "RobotmediaDatabase.java"


# static fields
.field static final NAME:Ljava/lang/String; = "billing.db"


# instance fields
.field private final mContext:Landroid/content/Context;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lorg/solovyev/android/checkout/RobotmediaDatabase;->mContext:Landroid/content/Context;

    .line 49
    return-void
.end method

.method public static exists(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 52
    invoke-static {p0}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->getDatabaseFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 53
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getDatabaseFile(Landroid/content/Context;)Ljava/io/File;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 58
    const-string v0, "billing.db"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getDatabasePath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method static getDatabasePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 63
    invoke-static {p0}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->getDatabaseFile(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    .line 64
    .local v0, "file":Ljava/io/File;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadProducts(Lorg/solovyev/android/checkout/Inventory$Request;Landroid/database/sqlite/SQLiteDatabase;)Lorg/solovyev/android/checkout/Inventory$Products;
    .locals 7
    .param p1, "request"    # Lorg/solovyev/android/checkout/Inventory$Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 108
    new-instance v2, Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-direct {v2}, Lorg/solovyev/android/checkout/Inventory$Products;-><init>()V

    .line 109
    .local v2, "result":Lorg/solovyev/android/checkout/Inventory$Products;
    sget-object v4, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 110
    .local v1, "productId":Ljava/lang/String;
    new-instance v0, Lorg/solovyev/android/checkout/Inventory$Product;

    const/4 v5, 0x1

    invoke-direct {v0, v1, v5}, Lorg/solovyev/android/checkout/Inventory$Product;-><init>(Ljava/lang/String;Z)V

    .line 112
    .local v0, "product":Lorg/solovyev/android/checkout/Inventory$Product;
    invoke-virtual {p1, v1}, Lorg/solovyev/android/checkout/Inventory$Request;->getSkus(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 113
    .local v3, "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 114
    invoke-direct {p0, v3, p2}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->loadPurchases(Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/solovyev/android/checkout/Inventory$Product;->setPurchases(Ljava/util/List;)V

    .line 119
    :goto_1
    invoke-virtual {v2, v0}, Lorg/solovyev/android/checkout/Inventory$Products;->add(Lorg/solovyev/android/checkout/Inventory$Product;)V

    goto :goto_0

    .line 116
    :cond_0
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "There are no SKUs for \""

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v0, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\" product. No purchase information will be loaded"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    goto :goto_1

    .line 121
    .end local v0    # "product":Lorg/solovyev/android/checkout/Inventory$Product;
    .end local v1    # "productId":Ljava/lang/String;
    .end local v3    # "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    return-object v2
.end method

.method private loadPurchases(Ljava/util/List;Landroid/database/sqlite/SQLiteDatabase;)Ljava/util/List;
    .locals 21
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "db"    # Landroid/database/sqlite/SQLiteDatabase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/database/sqlite/SQLiteDatabase;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 126
    .local p1, "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static/range {p1 .. p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/util/Collection;)V

    .line 127
    new-instance v20, Ljava/util/ArrayList;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v20

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 128
    .local v20, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v2, 0x5

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "state"

    aput-object v3, v4, v2

    const/4 v2, 0x2

    const-string v3, "productId"

    aput-object v3, v4, v2

    const/4 v2, 0x3

    const-string v3, "purchaseTime"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string v3, "developerPayload"

    aput-object v3, v4, v2

    .line 129
    .local v4, "columns":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lorg/solovyev/android/checkout/RobotmediaDatabase;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v19

    .line 131
    .local v19, "packageName":Ljava/lang/String;
    const/16 v18, 0x0

    .line 133
    .local v18, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "purchases"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "productId in "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->makeInClause(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v18

    .line 134
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 136
    :cond_0
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 137
    .local v8, "orderId":Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 138
    .local v12, "state":I
    const/4 v2, 0x2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 139
    .local v7, "sku":Ljava/lang/String;
    const/4 v2, 0x3

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 140
    .local v10, "time":J
    const/4 v2, 0x4

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 141
    .local v13, "payload":Ljava/lang/String;
    new-instance v6, Lorg/solovyev/android/checkout/Purchase;

    const-string v14, ""

    const/4 v15, 0x0

    const-string v16, ""

    const-string v17, ""

    move-object/from16 v9, v19

    invoke-direct/range {v6 .. v17}, Lorg/solovyev/android/checkout/Purchase;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 142
    .local v6, "p":Lorg/solovyev/android/checkout/Purchase;
    move-object/from16 v0, v20

    invoke-interface {v0, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 146
    .end local v6    # "p":Lorg/solovyev/android/checkout/Purchase;
    .end local v7    # "sku":Ljava/lang/String;
    .end local v8    # "orderId":Ljava/lang/String;
    .end local v10    # "time":J
    .end local v12    # "state":I
    .end local v13    # "payload":Ljava/lang/String;
    :cond_1
    if-eqz v18, :cond_2

    .line 147
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 150
    :cond_2
    return-object v20

    .line 146
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_3

    .line 147
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2
.end method

.method static makeInClause(I)Ljava/lang/String;
    .locals 4
    .param p0, "count"    # I
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 78
    if-lez p0, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "Should be positive"

    invoke-static {v2, v3}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 79
    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v2, p0, 0x2

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 80
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 81
    const-string v2, "?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    if-ge v0, p0, :cond_1

    .line 83
    const-string v2, ",?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 78
    .end local v0    # "i":I
    .end local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 85
    .restart local v0    # "i":I
    .restart local v1    # "sb":Ljava/lang/StringBuilder;
    :cond_1
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 86
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method static toInventoryProducts(Ljava/util/Collection;)Lorg/solovyev/android/checkout/Inventory$Products;
    .locals 5
    .param p0    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/solovyev/android/checkout/Inventory$Products;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 69
    .local p0, "products":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    new-instance v1, Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-direct {v1}, Lorg/solovyev/android/checkout/Inventory$Products;-><init>()V

    .line 70
    .local v1, "result":Lorg/solovyev/android/checkout/Inventory$Products;
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 71
    .local v0, "productId":Ljava/lang/String;
    new-instance v3, Lorg/solovyev/android/checkout/Inventory$Product;

    const/4 v4, 0x1

    invoke-direct {v3, v0, v4}, Lorg/solovyev/android/checkout/Inventory$Product;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v3}, Lorg/solovyev/android/checkout/Inventory$Products;->add(Lorg/solovyev/android/checkout/Inventory$Product;)V

    goto :goto_0

    .line 73
    .end local v0    # "productId":Ljava/lang/String;
    :cond_0
    return-object v1
.end method


# virtual methods
.method load(Lorg/solovyev/android/checkout/Inventory$Request;)Lorg/solovyev/android/checkout/Inventory$Products;
    .locals 5
    .param p1, "request"    # Lorg/solovyev/android/checkout/Inventory$Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 91
    const/4 v1, 0x0

    .line 93
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    iget-object v3, p0, Lorg/solovyev/android/checkout/RobotmediaDatabase;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->getDatabasePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "databasePath":Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->openDatabase(Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 95
    invoke-direct {p0, p1, v1}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->loadProducts(Lorg/solovyev/android/checkout/Inventory$Request;Landroid/database/sqlite/SQLiteDatabase;)Lorg/solovyev/android/checkout/Inventory$Products;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 99
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 103
    .end local v0    # "databasePath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v3

    .line 96
    :catch_0
    move-exception v2

    .line 97
    .local v2, "e":Ljava/lang/RuntimeException;
    :try_start_1
    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 99
    if-eqz v1, :cond_1

    .line 100
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 103
    :cond_1
    sget-object v3, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-static {v3}, Lorg/solovyev/android/checkout/RobotmediaDatabase;->toInventoryProducts(Ljava/util/Collection;)Lorg/solovyev/android/checkout/Inventory$Products;

    move-result-object v3

    goto :goto_0

    .line 99
    .end local v2    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    if-eqz v1, :cond_2

    .line 100
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    :cond_2
    throw v3
.end method
