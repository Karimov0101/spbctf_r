.class final Lorg/solovyev/android/checkout/GetSkuDetailsRequest;
.super Lorg/solovyev/android/checkout/Request;
.source "GetSkuDetailsRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/solovyev/android/checkout/Request",
        "<",
        "Lorg/solovyev/android/checkout/Skus;",
        ">;"
    }
.end annotation


# static fields
.field private static final MAX_SIZE_PER_REQUEST:I = 0x14


# instance fields
.field private final mProduct:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mSkus:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p2, "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v0, Lorg/solovyev/android/checkout/RequestType;->GET_SKU_DETAILS:Lorg/solovyev/android/checkout/RequestType;

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;)V

    .line 52
    iput-object p1, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    .line 54
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 55
    return-void
.end method

.method private getSkuDetails(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;Ljava/util/ArrayList;)Lorg/solovyev/android/checkout/Skus;
    .locals 4
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "packageName"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/android/vending/billing/IInAppBillingService;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/solovyev/android/checkout/Skus;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/solovyev/android/checkout/RequestException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 77
    .local p3, "skuBatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/16 v3, 0x14

    if-gt v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    const-string v3, "SKU list is too big"

    invoke-static {v2, v3}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 78
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 79
    .local v1, "skusBundle":Landroid/os/Bundle;
    const-string v2, "ITEM_ID_LIST"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 80
    const/4 v2, 0x3

    iget-object v3, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    invoke-interface {p1, v2, p2, v3, v1}, Lcom/android/vending/billing/IInAppBillingService;->getSkuDetails(ILjava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 81
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->handleError(Landroid/os/Bundle;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 82
    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    invoke-static {v0, v2}, Lorg/solovyev/android/checkout/Skus;->fromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lorg/solovyev/android/checkout/Skus;

    move-result-object v2

    .line 84
    :goto_1
    return-object v2

    .line 77
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v1    # "skusBundle":Landroid/os/Bundle;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 84
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v1    # "skusBundle":Landroid/os/Bundle;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected getCacheKey()Ljava/lang/String;
    .locals 5
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 90
    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 91
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 102
    :goto_0
    return-object v2

    .line 93
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 94
    .local v1, "sb":Ljava/lang/StringBuilder;
    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 96
    if-lez v0, :cond_1

    .line 97
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 99
    :cond_1
    iget-object v2, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 101
    :cond_2
    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 102
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    .locals 7
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "packageName"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/solovyev/android/checkout/RequestException;
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v0, "allSkuDetails":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Sku;>;"
    const/4 v4, 0x0

    .local v4, "start":I
    :goto_0
    iget-object v5, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v4, v5, :cond_0

    .line 61
    iget-object v5, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    add-int/lit8 v6, v4, 0x14

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 62
    .local v1, "end":I
    new-instance v2, Ljava/util/ArrayList;

    iget-object v5, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mSkus:Ljava/util/ArrayList;

    invoke-virtual {v5, v4, v1}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 63
    .local v2, "skuBatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0, p1, p2, v2}, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->getSkuDetails(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;Ljava/util/ArrayList;)Lorg/solovyev/android/checkout/Skus;

    move-result-object v3

    .line 64
    .local v3, "skuDetails":Lorg/solovyev/android/checkout/Skus;
    if-eqz v3, :cond_1

    .line 65
    iget-object v5, v3, Lorg/solovyev/android/checkout/Skus;->list:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 60
    add-int/lit8 v4, v4, 0x14

    goto :goto_0

    .line 71
    .end local v1    # "end":I
    .end local v2    # "skuBatch":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "skuDetails":Lorg/solovyev/android/checkout/Skus;
    :cond_0
    new-instance v5, Lorg/solovyev/android/checkout/Skus;

    iget-object v6, p0, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->mProduct:Ljava/lang/String;

    invoke-direct {v5, v6, v0}, Lorg/solovyev/android/checkout/Skus;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {p0, v5}, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;->onSuccess(Ljava/lang/Object;)V

    .line 72
    :cond_1
    return-void
.end method
