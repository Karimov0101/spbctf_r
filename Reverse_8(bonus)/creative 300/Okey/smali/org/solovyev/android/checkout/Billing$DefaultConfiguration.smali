.class public abstract Lorg/solovyev/android/checkout/Billing$DefaultConfiguration;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Billing$Configuration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DefaultConfiguration"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getCache()Lorg/solovyev/android/checkout/Cache;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 722
    invoke-static {}, Lorg/solovyev/android/checkout/Billing;->newCache()Lorg/solovyev/android/checkout/Cache;

    move-result-object v0

    return-object v0
.end method

.method public getFallbackInventory(Lorg/solovyev/android/checkout/Checkout;Ljava/util/concurrent/Executor;)Lorg/solovyev/android/checkout/Inventory;
    .locals 1
    .param p1, "checkout"    # Lorg/solovyev/android/checkout/Checkout;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "onLoadExecutor"    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 735
    const/4 v0, 0x0

    return-object v0
.end method

.method public getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 728
    const-string v0, "Default purchase verification procedure is used, please read https://github.com/serso/android-checkout#purchase-verification"

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    .line 729
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing$DefaultConfiguration;->getPublicKey()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->newPurchaseVerifier(Ljava/lang/String;)Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v0

    return-object v0
.end method

.method public isAutoConnect()Z
    .locals 1

    .prologue
    .line 740
    const/4 v0, 0x1

    return v0
.end method
