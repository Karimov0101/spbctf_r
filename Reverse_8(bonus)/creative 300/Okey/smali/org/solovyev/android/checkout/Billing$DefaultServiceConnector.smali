.class final Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Billing$ServiceConnector;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DefaultServiceConnector"
.end annotation


# instance fields
.field private final mConnection:Landroid/content/ServiceConnection;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/Billing;


# direct methods
.method private constructor <init>(Lorg/solovyev/android/checkout/Billing;)V
    .locals 1

    .prologue
    .line 1224
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1225
    new-instance v0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector$1;

    invoke-direct {v0, p0}, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector$1;-><init>(Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->mConnection:Landroid/content/ServiceConnection;

    return-void
.end method

.method synthetic constructor <init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Billing$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/solovyev/android/checkout/Billing;
    .param p2, "x1"    # Lorg/solovyev/android/checkout/Billing$1;

    .prologue
    .line 1224
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    return-void
.end method


# virtual methods
.method public connect()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1242
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1243
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "com.android.vending"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 1244
    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->access$1000(Lorg/solovyev/android/checkout/Billing;)Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->mConnection:Landroid/content/ServiceConnection;

    const/4 v5, 0x1

    invoke-virtual {v3, v1, v4, v5}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    .line 1254
    .end local v1    # "intent":Landroid/content/Intent;
    :goto_0
    return v2

    .line 1245
    :catch_0
    move-exception v0

    .line 1249
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    goto :goto_0

    .line 1250
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1254
    .local v0, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public disconnect()V
    .locals 2

    .prologue
    .line 1260
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->access$1000(Lorg/solovyev/android/checkout/Billing;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;->mConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1261
    return-void
.end method
