.class final Lorg/solovyev/android/checkout/MainThreadRequestListener;
.super Lorg/solovyev/android/checkout/RequestListenerWrapper;
.source "MainThreadRequestListener.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/solovyev/android/checkout/RequestListenerWrapper",
        "<TR;>;"
    }
.end annotation


# instance fields
.field private mErrorRunnable:Ljava/lang/Runnable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mSuccessRunnable:Ljava/lang/Runnable;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/solovyev/android/checkout/CancellableExecutor;Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 0
    .param p1, "mainThread"    # Lorg/solovyev/android/checkout/CancellableExecutor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/CancellableExecutor;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p0, "this":Lorg/solovyev/android/checkout/MainThreadRequestListener;, "Lorg/solovyev/android/checkout/MainThreadRequestListener<TR;>;"
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    invoke-direct {p0, p2}, Lorg/solovyev/android/checkout/RequestListenerWrapper;-><init>(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 47
    iput-object p1, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    .line 48
    return-void
.end method


# virtual methods
.method public onCancel()V
    .locals 3

    .prologue
    .local p0, "this":Lorg/solovyev/android/checkout/MainThreadRequestListener;, "Lorg/solovyev/android/checkout/MainThreadRequestListener<TR;>;"
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mSuccessRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    iget-object v1, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mSuccessRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/CancellableExecutor;->cancel(Ljava/lang/Runnable;)V

    .line 75
    iput-object v2, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mSuccessRunnable:Ljava/lang/Runnable;

    .line 78
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mErrorRunnable:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 79
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    iget-object v1, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mErrorRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/CancellableExecutor;->cancel(Ljava/lang/Runnable;)V

    .line 80
    iput-object v2, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mErrorRunnable:Ljava/lang/Runnable;

    .line 82
    :cond_1
    return-void
.end method

.method public onError(ILjava/lang/Exception;)V
    .locals 2
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 63
    .local p0, "this":Lorg/solovyev/android/checkout/MainThreadRequestListener;, "Lorg/solovyev/android/checkout/MainThreadRequestListener<TR;>;"
    new-instance v0, Lorg/solovyev/android/checkout/MainThreadRequestListener$2;

    invoke-direct {v0, p0, p1, p2}, Lorg/solovyev/android/checkout/MainThreadRequestListener$2;-><init>(Lorg/solovyev/android/checkout/MainThreadRequestListener;ILjava/lang/Exception;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mErrorRunnable:Ljava/lang/Runnable;

    .line 69
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    iget-object v1, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mErrorRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/CancellableExecutor;->execute(Ljava/lang/Runnable;)V

    .line 70
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p0, "this":Lorg/solovyev/android/checkout/MainThreadRequestListener;, "Lorg/solovyev/android/checkout/MainThreadRequestListener<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    new-instance v0, Lorg/solovyev/android/checkout/MainThreadRequestListener$1;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/MainThreadRequestListener$1;-><init>(Lorg/solovyev/android/checkout/MainThreadRequestListener;Ljava/lang/Object;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mSuccessRunnable:Ljava/lang/Runnable;

    .line 58
    iget-object v0, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    iget-object v1, p0, Lorg/solovyev/android/checkout/MainThreadRequestListener;->mSuccessRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/CancellableExecutor;->execute(Ljava/lang/Runnable;)V

    .line 59
    return-void
.end method
