.class public final Lorg/solovyev/android/checkout/PurchaseFlow;
.super Ljava/lang/Object;
.source "PurchaseFlow.java"

# interfaces
.implements Lorg/solovyev/android/checkout/CancellableRequestListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/PurchaseFlow$VerificationListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/CancellableRequestListener",
        "<",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# static fields
.field static final EXTRA_PURCHASE_DATA:Ljava/lang/String; = "INAPP_PURCHASE_DATA"

.field static final EXTRA_PURCHASE_SIGNATURE:Ljava/lang/String; = "INAPP_DATA_SIGNATURE"

.field static final EXTRA_RESPONSE:Ljava/lang/String; = "RESPONSE_CODE"


# instance fields
.field private final mIntentStarter:Lorg/solovyev/android/checkout/IntentStarter;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mListener:Lorg/solovyev/android/checkout/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mRequestCode:I

.field private final mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/solovyev/android/checkout/IntentStarter;ILorg/solovyev/android/checkout/RequestListener;Lorg/solovyev/android/checkout/PurchaseVerifier;)V
    .locals 0
    .param p1, "intentStarter"    # Lorg/solovyev/android/checkout/IntentStarter;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "requestCode"    # I
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4, "verifier"    # Lorg/solovyev/android/checkout/PurchaseVerifier;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/IntentStarter;",
            "I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;",
            "Lorg/solovyev/android/checkout/PurchaseVerifier;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput-object p1, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mIntentStarter:Lorg/solovyev/android/checkout/IntentStarter;

    .line 77
    iput p2, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mRequestCode:I

    .line 78
    iput-object p3, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 79
    iput-object p4, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    .line 80
    return-void
.end method

.method static synthetic access$100(Lorg/solovyev/android/checkout/PurchaseFlow;I)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/PurchaseFlow;
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(I)V

    return-void
.end method

.method static synthetic access$200(Lorg/solovyev/android/checkout/PurchaseFlow;)Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/PurchaseFlow;

    .prologue
    .line 62
    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    return-object v0
.end method

.method static synthetic access$300(Lorg/solovyev/android/checkout/PurchaseFlow;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/PurchaseFlow;
    .param p1, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(Ljava/lang/Exception;)V

    return-void
.end method

.method private handleError(I)V
    .locals 2
    .param p1, "response"    # I

    .prologue
    .line 121
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error response: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in Purchase/ChangePurchase request"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;)V

    .line 122
    new-instance v0, Lorg/solovyev/android/checkout/BillingException;

    invoke-direct {v0, p1}, Lorg/solovyev/android/checkout/BillingException;-><init>(I)V

    invoke-virtual {p0, p1, v0}, Lorg/solovyev/android/checkout/PurchaseFlow;->onError(ILjava/lang/Exception;)V

    .line 123
    return-void
.end method

.method private handleError(Ljava/lang/Exception;)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 126
    const-string v0, "Exception in Purchase/ChangePurchase request: "

    invoke-static {v0, p1}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 127
    const/16 v0, 0x2711

    invoke-virtual {p0, v0, p1}, Lorg/solovyev/android/checkout/PurchaseFlow;->onError(ILjava/lang/Exception;)V

    .line 128
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    if-nez v0, :cond_0

    .line 151
    :goto_0
    return-void

    .line 149
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->cancel(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    goto :goto_0
.end method

.method onActivityResult(IILandroid/content/Intent;)V
    .locals 9
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 97
    :try_start_0
    iget v5, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mRequestCode:I

    invoke-static {v5, p1}, Lorg/solovyev/android/checkout/Check;->equals(II)V

    .line 98
    if-nez p3, :cond_0

    .line 100
    const/16 v5, 0x2713

    invoke-direct {p0, v5}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(I)V

    .line 118
    :goto_0
    return-void

    .line 103
    :cond_0
    const-string v5, "RESPONSE_CODE"

    const/4 v6, 0x0

    invoke-virtual {p3, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 104
    .local v3, "responseCode":I
    const/4 v5, -0x1

    if-ne p2, v5, :cond_1

    if-eqz v3, :cond_2

    .line 105
    :cond_1
    invoke-direct {p0, v3}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 115
    .end local v3    # "responseCode":I
    :catch_0
    move-exception v1

    .line 116
    .local v1, "e":Ljava/lang/Exception;
    :goto_1
    invoke-direct {p0, v1}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 108
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v3    # "responseCode":I
    :cond_2
    :try_start_1
    const-string v5, "INAPP_PURCHASE_DATA"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    .local v0, "data":Ljava/lang/String;
    const-string v5, "INAPP_DATA_SIGNATURE"

    invoke-virtual {p3, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 110
    .local v4, "signature":Ljava/lang/String;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;)V

    .line 111
    invoke-static {v4}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;)V

    .line 113
    invoke-static {v0, v4}, Lorg/solovyev/android/checkout/Purchase;->fromJson(Ljava/lang/String;Ljava/lang/String;)Lorg/solovyev/android/checkout/Purchase;

    move-result-object v2

    .line 114
    .local v2, "purchase":Lorg/solovyev/android/checkout/Purchase;
    iget-object v5, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    new-instance v7, Lorg/solovyev/android/checkout/PurchaseFlow$VerificationListener;

    const/4 v8, 0x0

    invoke-direct {v7, p0, v8}, Lorg/solovyev/android/checkout/PurchaseFlow$VerificationListener;-><init>(Lorg/solovyev/android/checkout/PurchaseFlow;Lorg/solovyev/android/checkout/PurchaseFlow$1;)V

    invoke-interface {v5, v6, v7}, Lorg/solovyev/android/checkout/PurchaseVerifier;->verify(Ljava/util/List;Lorg/solovyev/android/checkout/RequestListener;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 115
    .end local v0    # "data":Ljava/lang/String;
    .end local v2    # "purchase":Lorg/solovyev/android/checkout/Purchase;
    .end local v3    # "responseCode":I
    .end local v4    # "signature":Ljava/lang/String;
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public onError(ILjava/lang/Exception;)V
    .locals 1
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 132
    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    if-nez v0, :cond_0

    .line 136
    :goto_0
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/RequestListener;->onError(ILjava/lang/Exception;)V

    goto :goto_0
.end method

.method public onSuccess(Landroid/app/PendingIntent;)V
    .locals 5
    .param p1, "purchaseIntent"    # Landroid/app/PendingIntent;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 84
    iget-object v1, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    if-nez v1, :cond_0

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_0
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mIntentStarter:Lorg/solovyev/android/checkout/IntentStarter;

    invoke-virtual {p1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    iget v3, p0, Lorg/solovyev/android/checkout/PurchaseFlow;->mRequestCode:I

    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    invoke-interface {v1, v2, v3, v4}, Lorg/solovyev/android/checkout/IntentStarter;->startForResult(Landroid/content/IntentSender;ILandroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/PurchaseFlow;->handleError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 62
    check-cast p1, Landroid/app/PendingIntent;

    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/PurchaseFlow;->onSuccess(Landroid/app/PendingIntent;)V

    return-void
.end method
