.class final Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OnConnectedServiceRunnable"
.end annotation


# instance fields
.field private mRequest:Lorg/solovyev/android/checkout/Request;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/Billing;


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;)V
    .locals 0
    .param p1    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "request"    # Lorg/solovyev/android/checkout/Request;

    .prologue
    .line 801
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 802
    iput-object p2, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    .line 803
    return-void
.end method

.method private checkCache(Lorg/solovyev/android/checkout/Request;)Z
    .locals 5
    .param p1, "request"    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 846
    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v3

    invoke-virtual {v3}, Lorg/solovyev/android/checkout/ConcurrentCache;->hasCache()Z

    move-result v3

    if-nez v3, :cond_1

    .line 858
    :cond_0
    :goto_0
    return v2

    .line 849
    :cond_1
    invoke-virtual {p1}, Lorg/solovyev/android/checkout/Request;->getCacheKey()Ljava/lang/String;

    move-result-object v1

    .line 850
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 853
    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v3

    invoke-virtual {p1}, Lorg/solovyev/android/checkout/Request;->getType()Lorg/solovyev/android/checkout/RequestType;

    move-result-object v4

    invoke-virtual {v4, v1}, Lorg/solovyev/android/checkout/RequestType;->getCacheKey(Ljava/lang/String;)Lorg/solovyev/android/checkout/Cache$Key;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/solovyev/android/checkout/ConcurrentCache;->get(Lorg/solovyev/android/checkout/Cache$Key;)Lorg/solovyev/android/checkout/Cache$Entry;

    move-result-object v0

    .line 854
    .local v0, "entry":Lorg/solovyev/android/checkout/Cache$Entry;
    if-eqz v0, :cond_0

    .line 857
    iget-object v2, v0, Lorg/solovyev/android/checkout/Cache$Entry;->data:Ljava/lang/Object;

    invoke-virtual {p1, v2}, Lorg/solovyev/android/checkout/Request;->onSuccess(Ljava/lang/Object;)V

    .line 858
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 870
    monitor-enter p0

    .line 871
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    if-eqz v0, :cond_0

    .line 872
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Cancelling request: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 873
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Request;->cancel()V

    .line 875
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    .line 876
    monitor-exit p0

    .line 877
    return-void

    .line 876
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getId()I
    .locals 1

    .prologue
    .line 881
    monitor-enter p0

    .line 882
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Request;->getId()I

    move-result v0

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0

    .line 883
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getRequest()Lorg/solovyev/android/checkout/Request;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 864
    monitor-enter p0

    .line 865
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    monitor-exit p0

    return-object v0

    .line 866
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 889
    monitor-enter p0

    .line 890
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Request;->getTag()Ljava/lang/Object;

    move-result-object v0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 891
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 807
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->getRequest()Lorg/solovyev/android/checkout/Request;

    move-result-object v1

    .line 808
    .local v1, "localRequest":Lorg/solovyev/android/checkout/Request;
    if-nez v1, :cond_1

    .line 842
    :cond_0
    :goto_0
    return v4

    .line 813
    :cond_1
    invoke-direct {p0, v1}, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->checkCache(Lorg/solovyev/android/checkout/Request;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 818
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v5}, Lorg/solovyev/android/checkout/Billing;->access$700(Lorg/solovyev/android/checkout/Billing;)Ljava/lang/Object;

    move-result-object v5

    monitor-enter v5

    .line 819
    :try_start_0
    iget-object v6, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v6}, Lorg/solovyev/android/checkout/Billing;->access$800(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$State;

    move-result-object v3

    .line 820
    .local v3, "localState":Lorg/solovyev/android/checkout/Billing$State;
    iget-object v6, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v6}, Lorg/solovyev/android/checkout/Billing;->access$900(Lorg/solovyev/android/checkout/Billing;)Lcom/android/vending/billing/IInAppBillingService;

    move-result-object v2

    .line 821
    .local v2, "localService":Lcom/android/vending/billing/IInAppBillingService;
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 822
    sget-object v5, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v3, v5, :cond_2

    .line 823
    invoke-static {v2}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;)V

    .line 826
    :try_start_1
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v5}, Lorg/solovyev/android/checkout/Billing;->access$1000(Lorg/solovyev/android/checkout/Billing;)Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, Lorg/solovyev/android/checkout/Request;->start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/solovyev/android/checkout/RequestException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    .line 827
    :catch_0
    move-exception v0

    .line 828
    .local v0, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v1, v0}, Lorg/solovyev/android/checkout/Request;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 821
    .end local v0    # "e":Ljava/lang/Exception;
    .end local v2    # "localService":Lcom/android/vending/billing/IInAppBillingService;
    .end local v3    # "localState":Lorg/solovyev/android/checkout/Billing$State;
    :catchall_0
    move-exception v4

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .line 832
    .restart local v2    # "localService":Lcom/android/vending/billing/IInAppBillingService;
    .restart local v3    # "localState":Lorg/solovyev/android/checkout/Billing$State;
    :cond_2
    sget-object v5, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v3, v5, :cond_3

    .line 834
    iget-object v4, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {v4}, Lorg/solovyev/android/checkout/Billing;->connect()V

    .line 835
    const/4 v4, 0x0

    goto :goto_0

    .line 838
    :cond_3
    const/16 v5, 0x2710

    invoke-virtual {v1, v5}, Lorg/solovyev/android/checkout/Request;->onError(I)V

    goto :goto_0

    .line 827
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
