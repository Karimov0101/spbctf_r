.class final Lorg/solovyev/android/checkout/Billing$StaticConfiguration;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Billing$Configuration;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StaticConfiguration"
.end annotation


# instance fields
.field private final mOriginal:Lorg/solovyev/android/checkout/Billing$Configuration;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPublicKey:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mPurchaseVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lorg/solovyev/android/checkout/Billing$Configuration;)V
    .locals 1
    .param p1, "original"    # Lorg/solovyev/android/checkout/Billing$Configuration;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 756
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 757
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mOriginal:Lorg/solovyev/android/checkout/Billing$Configuration;

    .line 758
    invoke-interface {p1}, Lorg/solovyev/android/checkout/Billing$Configuration;->getPublicKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mPublicKey:Ljava/lang/String;

    .line 759
    invoke-interface {p1}, Lorg/solovyev/android/checkout/Billing$Configuration;->getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mPurchaseVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    .line 760
    return-void
.end method

.method synthetic constructor <init>(Lorg/solovyev/android/checkout/Billing$Configuration;Lorg/solovyev/android/checkout/Billing$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/solovyev/android/checkout/Billing$Configuration;
    .param p2, "x1"    # Lorg/solovyev/android/checkout/Billing$1;

    .prologue
    .line 748
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;-><init>(Lorg/solovyev/android/checkout/Billing$Configuration;)V

    return-void
.end method


# virtual methods
.method public getCache()Lorg/solovyev/android/checkout/Cache;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 771
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mOriginal:Lorg/solovyev/android/checkout/Billing$Configuration;

    invoke-interface {v0}, Lorg/solovyev/android/checkout/Billing$Configuration;->getCache()Lorg/solovyev/android/checkout/Cache;

    move-result-object v0

    return-object v0
.end method

.method public getFallbackInventory(Lorg/solovyev/android/checkout/Checkout;Ljava/util/concurrent/Executor;)Lorg/solovyev/android/checkout/Inventory;
    .locals 1
    .param p1, "checkout"    # Lorg/solovyev/android/checkout/Checkout;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "onLoadExecutor"    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 787
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mOriginal:Lorg/solovyev/android/checkout/Billing$Configuration;

    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/Billing$Configuration;->getFallbackInventory(Lorg/solovyev/android/checkout/Checkout;Ljava/util/concurrent/Executor;)Lorg/solovyev/android/checkout/Inventory;

    move-result-object v0

    return-object v0
.end method

.method public getPublicKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 765
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mPublicKey:Ljava/lang/String;

    return-object v0
.end method

.method public getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 777
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mPurchaseVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    return-object v0
.end method

.method public isAutoConnect()Z
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mOriginal:Lorg/solovyev/android/checkout/Billing$Configuration;

    invoke-interface {v0}, Lorg/solovyev/android/checkout/Billing$Configuration;->isAutoConnect()Z

    move-result v0

    return v0
.end method

.method setPurchaseVerifier(Lorg/solovyev/android/checkout/PurchaseVerifier;)V
    .locals 0
    .param p1, "purchaseVerifier"    # Lorg/solovyev/android/checkout/PurchaseVerifier;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 781
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->mPurchaseVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    .line 782
    return-void
.end method
