.class Lorg/solovyev/android/checkout/RequestListenerWrapper;
.super Ljava/lang/Object;
.source "RequestListenerWrapper.java"

# interfaces
.implements Lorg/solovyev/android/checkout/CancellableRequestListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/CancellableRequestListener",
        "<TR;>;"
    }
.end annotation


# instance fields
.field protected final mListener:Lorg/solovyev/android/checkout/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 0
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lorg/solovyev/android/checkout/RequestListenerWrapper;, "Lorg/solovyev/android/checkout/RequestListenerWrapper<TR;>;"
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lorg/solovyev/android/checkout/RequestListenerWrapper;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 34
    return-void
.end method


# virtual methods
.method public final cancel()V
    .locals 1

    .prologue
    .line 47
    .local p0, "this":Lorg/solovyev/android/checkout/RequestListenerWrapper;, "Lorg/solovyev/android/checkout/RequestListenerWrapper<TR;>;"
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/RequestListenerWrapper;->onCancel()V

    .line 48
    iget-object v0, p0, Lorg/solovyev/android/checkout/RequestListenerWrapper;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->cancel(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 49
    return-void
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 52
    .local p0, "this":Lorg/solovyev/android/checkout/RequestListenerWrapper;, "Lorg/solovyev/android/checkout/RequestListenerWrapper<TR;>;"
    return-void
.end method

.method public onError(ILjava/lang/Exception;)V
    .locals 1
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 43
    .local p0, "this":Lorg/solovyev/android/checkout/RequestListenerWrapper;, "Lorg/solovyev/android/checkout/RequestListenerWrapper<TR;>;"
    iget-object v0, p0, Lorg/solovyev/android/checkout/RequestListenerWrapper;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/RequestListener;->onError(ILjava/lang/Exception;)V

    .line 44
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p0, "this":Lorg/solovyev/android/checkout/RequestListenerWrapper;, "Lorg/solovyev/android/checkout/RequestListenerWrapper<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lorg/solovyev/android/checkout/RequestListenerWrapper;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-interface {v0, p1}, Lorg/solovyev/android/checkout/RequestListener;->onSuccess(Ljava/lang/Object;)V

    .line 39
    return-void
.end method
