.class final Lorg/solovyev/android/checkout/PendingRequests;
.super Ljava/lang/Object;
.source "PendingRequests.java"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/RequestRunnable;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mList"
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    return-void
.end method

.method private remove(Lorg/solovyev/android/checkout/RequestRunnable;)V
    .locals 4
    .param p1, "runnable"    # Lorg/solovyev/android/checkout/RequestRunnable;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 173
    iget-object v2, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v2

    .line 174
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 175
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 176
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 177
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removing pending request: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 178
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 182
    :cond_1
    monitor-exit v2

    .line 183
    return-void

    .line 182
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method


# virtual methods
.method add(Lorg/solovyev/android/checkout/RequestRunnable;)V
    .locals 3
    .param p1, "runnable"    # Lorg/solovyev/android/checkout/RequestRunnable;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 48
    iget-object v1, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v1

    .line 49
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Adding pending request: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 50
    iget-object v0, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 51
    monitor-exit v1

    .line 52
    return-void

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method cancel(I)V
    .locals 5
    .param p1, "requestId"    # I

    .prologue
    .line 105
    iget-object v3, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v3

    .line 106
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cancelling pending request with id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 107
    iget-object v2, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 108
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 109
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/RequestRunnable;

    .line 110
    .local v1, "request":Lorg/solovyev/android/checkout/RequestRunnable;
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->getId()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 111
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->cancel()V

    .line 112
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    .line 116
    .end local v1    # "request":Lorg/solovyev/android/checkout/RequestRunnable;
    :cond_1
    monitor-exit v3

    .line 117
    return-void

    .line 116
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method cancelAll()V
    .locals 4

    .prologue
    .line 58
    iget-object v3, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v3

    .line 59
    :try_start_0
    const-string v2, "Cancelling all pending requests"

    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 60
    iget-object v2, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 61
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/RequestRunnable;

    .line 63
    .local v1, "request":Lorg/solovyev/android/checkout/RequestRunnable;
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->cancel()V

    .line 64
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 66
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    .end local v1    # "request":Lorg/solovyev/android/checkout/RequestRunnable;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .restart local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 67
    return-void
.end method

.method cancelAll(Ljava/lang/Object;)V
    .locals 6
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 75
    iget-object v4, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v4

    .line 76
    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cancelling all pending requests with tag="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 77
    iget-object v3, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 78
    .local v0, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 79
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/RequestRunnable;

    .line 80
    .local v1, "request":Lorg/solovyev/android/checkout/RequestRunnable;
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->getTag()Ljava/lang/Object;

    move-result-object v2

    .line 81
    .local v2, "requestTag":Ljava/lang/Object;
    if-ne v2, p1, :cond_1

    .line 82
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->cancel()V

    .line 83
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 96
    .end local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    .end local v1    # "request":Lorg/solovyev/android/checkout/RequestRunnable;
    .end local v2    # "requestTag":Ljava/lang/Object;
    :catchall_0
    move-exception v3

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3

    .line 87
    .restart local v0    # "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<Lorg/solovyev/android/checkout/RequestRunnable;>;"
    .restart local v1    # "request":Lorg/solovyev/android/checkout/RequestRunnable;
    .restart local v2    # "requestTag":Ljava/lang/Object;
    :cond_1
    if-eqz v2, :cond_2

    if-eqz p1, :cond_0

    .line 91
    :cond_2
    if-eqz v2, :cond_0

    :try_start_1
    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->cancel()V

    .line 93
    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 96
    .end local v1    # "request":Lorg/solovyev/android/checkout/RequestRunnable;
    .end local v2    # "requestTag":Ljava/lang/Object;
    :cond_3
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    return-void
.end method

.method onConnectionFailed()V
    .locals 3

    .prologue
    .line 189
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 190
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/PendingRequests;->pop()Lorg/solovyev/android/checkout/RequestRunnable;

    move-result-object v1

    .line 191
    .local v1, "requestRunnable":Lorg/solovyev/android/checkout/RequestRunnable;
    :goto_0
    if-eqz v1, :cond_1

    .line 192
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->getRequest()Lorg/solovyev/android/checkout/Request;

    move-result-object v0

    .line 193
    .local v0, "request":Lorg/solovyev/android/checkout/Request;
    if-eqz v0, :cond_0

    .line 194
    const/16 v2, 0x2710

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/Request;->onError(I)V

    .line 195
    invoke-interface {v1}, Lorg/solovyev/android/checkout/RequestRunnable;->cancel()V

    .line 197
    :cond_0
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/PendingRequests;->pop()Lorg/solovyev/android/checkout/RequestRunnable;

    move-result-object v1

    .line 198
    goto :goto_0

    .line 199
    .end local v0    # "request":Lorg/solovyev/android/checkout/Request;
    :cond_1
    return-void
.end method

.method peek()Lorg/solovyev/android/checkout/RequestRunnable;
    .locals 3
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 142
    iget-object v1, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v1

    .line 143
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/RequestRunnable;

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method pop()Lorg/solovyev/android/checkout/RequestRunnable;
    .locals 4
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 126
    iget-object v2, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    monitor-enter v2

    .line 127
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/solovyev/android/checkout/PendingRequests;->mList:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/RequestRunnable;

    move-object v0, v1

    .line 128
    .local v0, "runnable":Lorg/solovyev/android/checkout/RequestRunnable;
    :goto_0
    if-eqz v0, :cond_0

    .line 129
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removing pending request: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 131
    :cond_0
    monitor-exit v2

    return-object v0

    .line 127
    .end local v0    # "runnable":Lorg/solovyev/android/checkout/RequestRunnable;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public run()V
    .locals 3

    .prologue
    .line 153
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/PendingRequests;->peek()Lorg/solovyev/android/checkout/RequestRunnable;

    move-result-object v0

    .line 154
    .local v0, "runnable":Lorg/solovyev/android/checkout/RequestRunnable;
    :goto_0
    if-eqz v0, :cond_0

    .line 155
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Running pending request: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->debug(Ljava/lang/String;)V

    .line 156
    invoke-interface {v0}, Lorg/solovyev/android/checkout/RequestRunnable;->run()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/PendingRequests;->remove(Lorg/solovyev/android/checkout/RequestRunnable;)V

    .line 158
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/PendingRequests;->peek()Lorg/solovyev/android/checkout/RequestRunnable;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_0
    return-void
.end method
