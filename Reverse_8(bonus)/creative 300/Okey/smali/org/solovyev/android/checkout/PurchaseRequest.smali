.class final Lorg/solovyev/android/checkout/PurchaseRequest;
.super Lorg/solovyev/android/checkout/Request;
.source "PurchaseRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/solovyev/android/checkout/Request",
        "<",
        "Landroid/app/PendingIntent;",
        ">;"
    }
.end annotation


# instance fields
.field private final mPayload:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mProduct:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mSku:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 46
    sget-object v0, Lorg/solovyev/android/checkout/RequestType;->PURCHASE:Lorg/solovyev/android/checkout/RequestType;

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;)V

    .line 47
    iput-object p1, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mProduct:Ljava/lang/String;

    .line 48
    iput-object p2, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mSku:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mPayload:Ljava/lang/String;

    .line 50
    return-void
.end method


# virtual methods
.method protected getCacheKey()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    .locals 8
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "packageName"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lorg/solovyev/android/checkout/RequestException;
        }
    .end annotation

    .prologue
    .line 54
    iget v1, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mApiVersion:I

    iget-object v3, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mSku:Ljava/lang/String;

    iget-object v4, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mProduct:Ljava/lang/String;

    iget-object v0, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mPayload:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string v5, ""

    :goto_0
    move-object v0, p1

    move-object v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/android/vending/billing/IInAppBillingService;->getBuyIntent(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v6

    .line 55
    .local v6, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0, v6}, Lorg/solovyev/android/checkout/PurchaseRequest;->handleError(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    :goto_1
    return-void

    .line 54
    .end local v6    # "bundle":Landroid/os/Bundle;
    :cond_0
    iget-object v5, p0, Lorg/solovyev/android/checkout/PurchaseRequest;->mPayload:Ljava/lang/String;

    goto :goto_0

    .line 58
    .restart local v6    # "bundle":Landroid/os/Bundle;
    :cond_1
    const-string v0, "BUY_INTENT"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Landroid/app/PendingIntent;

    .line 59
    .local v7, "pendingIntent":Landroid/app/PendingIntent;
    invoke-static {v7}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;)V

    .line 60
    invoke-virtual {p0, v7}, Lorg/solovyev/android/checkout/PurchaseRequest;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1
.end method
