.class final Lorg/solovyev/android/checkout/SafeCache;
.super Ljava/lang/Object;
.source "SafeCache.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Cache;


# instance fields
.field private final mCache:Lorg/solovyev/android/checkout/Cache;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Lorg/solovyev/android/checkout/Cache;)V
    .locals 0
    .param p1, "cache"    # Lorg/solovyev/android/checkout/Cache;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    .line 37
    return-void
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    .line 89
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1}, Lorg/solovyev/android/checkout/Cache;->clear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public get(Lorg/solovyev/android/checkout/Cache$Key;)Lorg/solovyev/android/checkout/Cache$Entry;
    .locals 2
    .param p1, "key"    # Lorg/solovyev/android/checkout/Cache$Key;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 43
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1, p1}, Lorg/solovyev/android/checkout/Cache;->get(Lorg/solovyev/android/checkout/Cache$Key;)Lorg/solovyev/android/checkout/Cache$Entry;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 46
    :goto_0
    return-object v1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    .line 46
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public init()V
    .locals 2

    .prologue
    .line 62
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1}, Lorg/solovyev/android/checkout/Cache;->init()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public put(Lorg/solovyev/android/checkout/Cache$Key;Lorg/solovyev/android/checkout/Cache$Entry;)V
    .locals 2
    .param p1, "key"    # Lorg/solovyev/android/checkout/Cache$Key;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "entry"    # Lorg/solovyev/android/checkout/Cache$Entry;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 53
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1, p1, p2}, Lorg/solovyev/android/checkout/Cache;->put(Lorg/solovyev/android/checkout/Cache$Key;Lorg/solovyev/android/checkout/Cache$Entry;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 57
    :goto_0
    return-void

    .line 54
    :catch_0
    move-exception v0

    .line 55
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public remove(Lorg/solovyev/android/checkout/Cache$Key;)V
    .locals 2
    .param p1, "key"    # Lorg/solovyev/android/checkout/Cache$Key;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 71
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1, p1}, Lorg/solovyev/android/checkout/Cache;->remove(Lorg/solovyev/android/checkout/Cache$Key;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :goto_0
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public removeAll(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 80
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/SafeCache;->mCache:Lorg/solovyev/android/checkout/Cache;

    invoke-interface {v1, p1}, Lorg/solovyev/android/checkout/Cache;->removeAll(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method
