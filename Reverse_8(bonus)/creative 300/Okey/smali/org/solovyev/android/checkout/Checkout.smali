.class public Lorg/solovyev/android/checkout/Checkout;
.super Ljava/lang/Object;
.source "Checkout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;,
        Lorg/solovyev/android/checkout/Checkout$Listeners;,
        Lorg/solovyev/android/checkout/Checkout$EmptyListener;,
        Lorg/solovyev/android/checkout/Checkout$Listener;,
        Lorg/solovyev/android/checkout/Checkout$State;
    }
.end annotation


# instance fields
.field protected final mBilling:Lorg/solovyev/android/checkout/Billing;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field final mLock:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mOnLoadExecutor:Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mRequests:Lorg/solovyev/android/checkout/Billing$Requests;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private mState:Lorg/solovyev/android/checkout/Checkout$State;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mSupportedProducts:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mTag:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    .line 114
    new-instance v0, Lorg/solovyev/android/checkout/Checkout$Listeners;

    invoke-direct {v0, v1}, Lorg/solovyev/android/checkout/Checkout$Listeners;-><init>(Lorg/solovyev/android/checkout/Checkout$1;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    .line 117
    new-instance v0, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;

    invoke-direct {v0, p0, v1}, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;-><init>(Lorg/solovyev/android/checkout/Checkout;Lorg/solovyev/android/checkout/Checkout$1;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mOnLoadExecutor:Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;

    .line 121
    sget-object v0, Lorg/solovyev/android/checkout/Checkout$State;->INITIAL:Lorg/solovyev/android/checkout/Checkout$State;

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    .line 126
    iput-object p1, p0, Lorg/solovyev/android/checkout/Checkout;->mTag:Ljava/lang/Object;

    .line 127
    iput-object p2, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    .line 128
    return-void
.end method

.method static synthetic access$200(Lorg/solovyev/android/checkout/Checkout;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Checkout;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Lorg/solovyev/android/checkout/Checkout;->onBillingSupported(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$300(Lorg/solovyev/android/checkout/Checkout;)Lorg/solovyev/android/checkout/Billing$Requests;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Checkout;

    .prologue
    .line 103
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    return-object v0
.end method

.method private checkIsNotStopped()V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    sget-object v1, Lorg/solovyev/android/checkout/Checkout$State;->STOPPED:Lorg/solovyev/android/checkout/Checkout$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Checkout is stopped"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isFalse(ZLjava/lang/String;)V

    .line 264
    return-void

    .line 263
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static forActivity(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ActivityCheckout;
    .locals 1
    .param p0, "activity"    # Landroid/app/Activity;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 175
    new-instance v0, Lorg/solovyev/android/checkout/ActivityCheckout;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/ActivityCheckout;-><init>(Landroid/app/Activity;Lorg/solovyev/android/checkout/Billing;)V

    return-object v0
.end method

.method public static forApplication(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Checkout;
    .locals 2
    .param p0, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 185
    new-instance v0, Lorg/solovyev/android/checkout/Checkout;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lorg/solovyev/android/checkout/Checkout;-><init>(Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V

    return-object v0
.end method

.method public static forFragment(Landroid/app/Fragment;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/UiCheckout;
    .locals 1
    .param p0, "fragment"    # Landroid/app/Fragment;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation build Landroid/annotation/TargetApi;
        value = 0x18
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 170
    new-instance v0, Lorg/solovyev/android/checkout/FragmentCheckout;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/FragmentCheckout;-><init>(Landroid/app/Fragment;Lorg/solovyev/android/checkout/Billing;)V

    return-object v0
.end method

.method public static forService(Landroid/app/Service;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Checkout;
    .locals 1
    .param p0, "service"    # Landroid/app/Service;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 180
    new-instance v0, Lorg/solovyev/android/checkout/Checkout;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/Checkout;-><init>(Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V

    return-object v0
.end method

.method public static forUi(Lorg/solovyev/android/checkout/IntentStarter;Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/UiCheckout;
    .locals 1
    .param p0, "intentStarter"    # Lorg/solovyev/android/checkout/IntentStarter;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 137
    new-instance v0, Lorg/solovyev/android/checkout/CustomUiCheckout;

    invoke-direct {v0, p0, p1, p2}, Lorg/solovyev/android/checkout/CustomUiCheckout;-><init>(Lorg/solovyev/android/checkout/IntentStarter;Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V

    return-object v0
.end method

.method private isReady()Z
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Should be called from synchronized block"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 268
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    sget-object v1, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onBillingSupported(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "supported"    # Z

    .prologue
    .line 272
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 273
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-virtual {v0, v2, p1, p2}, Lorg/solovyev/android/checkout/Checkout$Listeners;->onReady(Lorg/solovyev/android/checkout/BillingRequests;Ljava/lang/String;Z)V

    .line 275
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Checkout;->isReady()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/Checkout$Listeners;->onReady(Lorg/solovyev/android/checkout/BillingRequests;)V

    .line 277
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Checkout$Listeners;->clear()V

    .line 279
    :cond_0
    monitor-exit v1

    .line 280
    return-void

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method getContext()Landroid/content/Context;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 190
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public isBillingSupported(Ljava/lang/String;)Z
    .locals 2
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 351
    sget-object v0, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Product should be added to the products list"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 352
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Billing information is not ready yet"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 353
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public loadInventory(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)Lorg/solovyev/android/checkout/Inventory;
    .locals 1
    .param p1, "request"    # Lorg/solovyev/android/checkout/Inventory$Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "callback"    # Lorg/solovyev/android/checkout/Inventory$Callback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 292
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Checkout;->makeInventory()Lorg/solovyev/android/checkout/Inventory;

    move-result-object v0

    .line 293
    .local v0, "inventory":Lorg/solovyev/android/checkout/Inventory;
    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/Inventory;->load(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)I

    .line 294
    return-object v0
.end method

.method public makeInventory()Lorg/solovyev/android/checkout/Inventory;
    .locals 4
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 306
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 308
    iget-object v3, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 309
    :try_start_0
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Checkout;->checkIsNotStopped()V

    .line 310
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing;->getConfiguration()Lorg/solovyev/android/checkout/Billing$Configuration;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Checkout;->mOnLoadExecutor:Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;

    invoke-interface {v2, p0, v3}, Lorg/solovyev/android/checkout/Billing$Configuration;->getFallbackInventory(Lorg/solovyev/android/checkout/Checkout;Ljava/util/concurrent/Executor;)Lorg/solovyev/android/checkout/Inventory;

    move-result-object v0

    .line 314
    .local v0, "fallbackInventory":Lorg/solovyev/android/checkout/Inventory;
    if-nez v0, :cond_0

    .line 315
    new-instance v1, Lorg/solovyev/android/checkout/CheckoutInventory;

    invoke-direct {v1, p0}, Lorg/solovyev/android/checkout/CheckoutInventory;-><init>(Lorg/solovyev/android/checkout/Checkout;)V

    .line 319
    .local v1, "inventory":Lorg/solovyev/android/checkout/Inventory;
    :goto_0
    return-object v1

    .line 310
    .end local v0    # "fallbackInventory":Lorg/solovyev/android/checkout/Inventory;
    .end local v1    # "inventory":Lorg/solovyev/android/checkout/Inventory;
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 317
    .restart local v0    # "fallbackInventory":Lorg/solovyev/android/checkout/Inventory;
    :cond_0
    new-instance v1, Lorg/solovyev/android/checkout/FallingBackInventory;

    invoke-direct {v1, p0, v0}, Lorg/solovyev/android/checkout/FallingBackInventory;-><init>(Lorg/solovyev/android/checkout/Checkout;Lorg/solovyev/android/checkout/Inventory;)V

    .restart local v1    # "inventory":Lorg/solovyev/android/checkout/Inventory;
    goto :goto_0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Checkout;->start(Lorg/solovyev/android/checkout/Checkout$Listener;)V

    .line 198
    return-void
.end method

.method public start(Lorg/solovyev/android/checkout/Checkout$Listener;)V
    .locals 5
    .param p1, "listener"    # Lorg/solovyev/android/checkout/Checkout$Listener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 207
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 209
    iget-object v2, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 210
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    sget-object v3, Lorg/solovyev/android/checkout/Checkout$State;->STARTED:Lorg/solovyev/android/checkout/Checkout$State;

    if-ne v1, v3, :cond_1

    const/4 v1, 0x1

    :goto_0
    const-string v3, "Already started"

    invoke-static {v1, v3}, Lorg/solovyev/android/checkout/Check;->isFalse(ZLjava/lang/String;)V

    .line 211
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    const-string v3, "Already started"

    invoke-static {v1, v3}, Lorg/solovyev/android/checkout/Check;->isNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 212
    sget-object v1, Lorg/solovyev/android/checkout/Checkout$State;->STARTED:Lorg/solovyev/android/checkout/Checkout$State;

    iput-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    .line 213
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/Billing;->onCheckoutStarted()V

    .line 214
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    iget-object v3, p0, Lorg/solovyev/android/checkout/Checkout;->mTag:Ljava/lang/Object;

    invoke-virtual {v1, v3}, Lorg/solovyev/android/checkout/Billing;->getRequests(Ljava/lang/Object;)Lorg/solovyev/android/checkout/Billing$Requests;

    move-result-object v1

    iput-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    .line 215
    if-eqz p1, :cond_0

    .line 216
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    invoke-virtual {v1, p1}, Lorg/solovyev/android/checkout/Checkout$Listeners;->add(Lorg/solovyev/android/checkout/Checkout$Listener;)V

    .line 218
    :cond_0
    sget-object v1, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 219
    .local v0, "product":Ljava/lang/String;
    iget-object v3, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    new-instance v4, Lorg/solovyev/android/checkout/Checkout$1;

    invoke-direct {v4, p0, v0}, Lorg/solovyev/android/checkout/Checkout$1;-><init>(Lorg/solovyev/android/checkout/Checkout;Ljava/lang/String;)V

    invoke-virtual {v3, v0, v4}, Lorg/solovyev/android/checkout/Billing$Requests;->isBillingSupported(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I

    goto :goto_1

    .line 231
    .end local v0    # "product":Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 210
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 231
    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 328
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 330
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 331
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 332
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Checkout$Listeners;->clear()V

    .line 333
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    sget-object v2, Lorg/solovyev/android/checkout/Checkout$State;->INITIAL:Lorg/solovyev/android/checkout/Checkout$State;

    if-eq v0, v2, :cond_0

    .line 334
    sget-object v0, Lorg/solovyev/android/checkout/Checkout$State;->STOPPED:Lorg/solovyev/android/checkout/Checkout$State;

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    .line 336
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    if-eqz v0, :cond_1

    .line 337
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$Requests;->cancelAll()V

    .line 338
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    .line 340
    :cond_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mState:Lorg/solovyev/android/checkout/Checkout$State;

    sget-object v2, Lorg/solovyev/android/checkout/Checkout$State;->STOPPED:Lorg/solovyev/android/checkout/Checkout$State;

    if-ne v0, v2, :cond_2

    .line 341
    iget-object v0, p0, Lorg/solovyev/android/checkout/Checkout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing;->onCheckoutStopped()V

    .line 343
    :cond_2
    monitor-exit v1

    .line 344
    return-void

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public whenReady(Lorg/solovyev/android/checkout/Checkout$Listener;)V
    .locals 6
    .param p1, "listener"    # Lorg/solovyev/android/checkout/Checkout$Listener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 244
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 246
    iget-object v3, p0, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 247
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mSupportedProducts:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 248
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v5, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-interface {p1, v5, v1, v2}, Lorg/solovyev/android/checkout/Checkout$Listener;->onReady(Lorg/solovyev/android/checkout/BillingRequests;Ljava/lang/String;Z)V

    goto :goto_0

    .line 259
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/lang/Boolean;>;"
    :catchall_0
    move-exception v1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 251
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Checkout;->isReady()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 252
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Checkout;->checkIsNotStopped()V

    .line 253
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;)V

    .line 254
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mRequests:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-interface {p1, v1}, Lorg/solovyev/android/checkout/Checkout$Listener;->onReady(Lorg/solovyev/android/checkout/BillingRequests;)V

    .line 259
    :goto_1
    monitor-exit v3

    .line 260
    return-void

    .line 257
    :cond_1
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout;->mListeners:Lorg/solovyev/android/checkout/Checkout$Listeners;

    invoke-virtual {v1, p1}, Lorg/solovyev/android/checkout/Checkout$Listeners;->add(Lorg/solovyev/android/checkout/Checkout$Listener;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
