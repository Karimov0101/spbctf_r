.class final Lorg/solovyev/android/checkout/GetPurchasesRequest;
.super Lorg/solovyev/android/checkout/Request;
.source "GetPurchasesRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/solovyev/android/checkout/Request",
        "<",
        "Lorg/solovyev/android/checkout/Purchases;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContinuationToken:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mProduct:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseVerifier;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "verifier"    # Lorg/solovyev/android/checkout/PurchaseVerifier;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 51
    sget-object v0, Lorg/solovyev/android/checkout/RequestType;->GET_PURCHASES:Lorg/solovyev/android/checkout/RequestType;

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;)V

    .line 52
    iput-object p1, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    .line 53
    iput-object p2, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    .line 54
    iput-object p3, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    .line 55
    return-void
.end method

.method constructor <init>(Lorg/solovyev/android/checkout/GetPurchasesRequest;Ljava/lang/String;)V
    .locals 1
    .param p1, "request"    # Lorg/solovyev/android/checkout/GetPurchasesRequest;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 58
    sget-object v0, Lorg/solovyev/android/checkout/RequestType;->GET_PURCHASES:Lorg/solovyev/android/checkout/RequestType;

    invoke-direct {p0, v0, p1}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;Lorg/solovyev/android/checkout/Request;)V

    .line 59
    iget-object v0, p1, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    iput-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    .line 60
    iput-object p2, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    .line 61
    iget-object v0, p1, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    iput-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    .line 62
    return-void
.end method


# virtual methods
.method protected getCacheKey()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 100
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    goto :goto_0
.end method

.method getContinuationToken()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 71
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    return-object v0
.end method

.method getProduct()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    return-object v0
.end method

.method start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    .locals 8
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "packageName"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 76
    iget v5, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mApiVersion:I

    iget-object v6, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    iget-object v7, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mContinuationToken:Ljava/lang/String;

    invoke-interface {p1, v5, p2, v6, v7}, Lcom/android/vending/billing/IInAppBillingService;->getPurchases(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/GetPurchasesRequest;->handleError(Landroid/os/Bundle;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    :try_start_0
    invoke-static {v0}, Lorg/solovyev/android/checkout/Purchases;->getContinuationTokenFromBundle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    .line 82
    .local v1, "continuationToken":Ljava/lang/String;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Purchases;->getListFromBundle(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v4

    .line 83
    .local v4, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 84
    new-instance v5, Lorg/solovyev/android/checkout/Purchases;

    iget-object v6, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    invoke-direct {v5, v6, v4, v1}, Lorg/solovyev/android/checkout/Purchases;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {p0, v5}, Lorg/solovyev/android/checkout/GetPurchasesRequest;->onSuccess(Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 92
    .end local v1    # "continuationToken":Ljava/lang/String;
    .end local v4    # "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    :catch_0
    move-exception v2

    .line 93
    .local v2, "e":Lorg/json/JSONException;
    invoke-virtual {p0, v2}, Lorg/solovyev/android/checkout/GetPurchasesRequest;->onError(Ljava/lang/Exception;)V

    goto :goto_0

    .line 87
    .end local v2    # "e":Lorg/json/JSONException;
    .restart local v1    # "continuationToken":Ljava/lang/String;
    .restart local v4    # "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    :cond_2
    :try_start_1
    new-instance v3, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;

    iget-object v5, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mProduct:Ljava/lang/String;

    invoke-direct {v3, p0, v5, v1}, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;-><init>(Lorg/solovyev/android/checkout/Request;Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v3, "listener":Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;
    iget-object v5, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest;->mVerifier:Lorg/solovyev/android/checkout/PurchaseVerifier;

    invoke-interface {v5, v4, v3}, Lorg/solovyev/android/checkout/PurchaseVerifier;->verify(Ljava/util/List;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 89
    invoke-static {v3}, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->access$000(Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 90
    const/16 v5, 0x2711

    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Either onSuccess or onError methods must be called by PurchaseVerifier"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5, v6}, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->onError(ILjava/lang/Exception;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
