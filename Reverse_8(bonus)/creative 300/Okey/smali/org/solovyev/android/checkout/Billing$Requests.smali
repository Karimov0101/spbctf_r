.class final Lorg/solovyev/android/checkout/Billing$Requests;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/BillingRequests;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "Requests"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;,
        Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;
    }
.end annotation


# instance fields
.field private final mOnMainThread:Z

.field private final mTag:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/Billing;


# direct methods
.method private constructor <init>(Lorg/solovyev/android/checkout/Billing;Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "this$0"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "tag"    # Ljava/lang/Object;
    .param p3, "onMainThread"    # Z

    .prologue
    .line 963
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 964
    iput-object p2, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    .line 965
    iput-boolean p3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mOnMainThread:Z

    .line 966
    return-void
.end method

.method synthetic constructor <init>(Lorg/solovyev/android/checkout/Billing;Ljava/lang/Object;ZLorg/solovyev/android/checkout/Billing$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/solovyev/android/checkout/Billing;
    .param p2, "x1"    # Ljava/lang/Object;
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lorg/solovyev/android/checkout/Billing$1;

    .prologue
    .line 956
    invoke-direct {p0, p1, p2, p3}, Lorg/solovyev/android/checkout/Billing$Requests;-><init>(Lorg/solovyev/android/checkout/Billing;Ljava/lang/Object;Z)V

    return-void
.end method

.method static synthetic access$1800(Lorg/solovyev/android/checkout/Billing$Requests;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing$Requests;

    .prologue
    .line 956
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    return-object v0
.end method

.method private wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 992
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    iget-boolean v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mOnMainThread:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v0, p1}, Lorg/solovyev/android/checkout/Billing;->access$1300(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object p1

    .end local p1    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    :cond_0
    return-object p1
.end method


# virtual methods
.method public cancel(I)V
    .locals 1
    .param p1, "requestId"    # I

    .prologue
    .line 1084
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->access$300(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/PendingRequests;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/PendingRequests;->cancel(I)V

    .line 1085
    return-void
.end method

.method public cancelAll()V
    .locals 2

    .prologue
    .line 1079
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->access$300(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/PendingRequests;

    move-result-object v0

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lorg/solovyev/android/checkout/PendingRequests;->cancelAll(Ljava/lang/Object;)V

    .line 1080
    return-void
.end method

.method public changeSubscription(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "newSku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "purchaseFlow"    # Lorg/solovyev/android/checkout/PurchaseFlow;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/PurchaseFlow;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1042
    .local p1, "oldSkus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/util/Collection;)V

    .line 1043
    invoke-static {p2}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1044
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/ChangePurchaseRequest;

    const-string v2, "subs"

    invoke-direct {v1, v2, p1, p2, p3}, Lorg/solovyev/android/checkout/ChangePurchaseRequest;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    invoke-direct {p0, p4}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    .line 1044
    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public changeSubscription(Ljava/util/List;Lorg/solovyev/android/checkout/Sku;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I
    .locals 5
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "newSku"    # Lorg/solovyev/android/checkout/Sku;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "purchaseFlow"    # Lorg/solovyev/android/checkout/PurchaseFlow;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Sku;",
            ">;",
            "Lorg/solovyev/android/checkout/Sku;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/PurchaseFlow;",
            ")I"
        }
    .end annotation

    .prologue
    .line 1052
    .local p1, "oldSkus":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Sku;>;"
    const-string v2, "subs"

    iget-object v3, p2, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v3, v3, Lorg/solovyev/android/checkout/Sku$Id;->product:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "Only subscriptions can be downgraded/upgraded"

    invoke-static {v2, v3}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 1053
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 1054
    .local v1, "oldSkuIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Sku;

    .line 1055
    .local v0, "oldSku":Lorg/solovyev/android/checkout/Sku;
    iget-object v3, v0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v3, v3, Lorg/solovyev/android/checkout/Sku$Id;->product:Ljava/lang/String;

    iget-object v4, p2, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v4, v4, Lorg/solovyev/android/checkout/Sku$Id;->product:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "Product type can\'t be changed"

    invoke-static {v3, v4}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 1056
    iget-object v3, v0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v3, v3, Lorg/solovyev/android/checkout/Sku$Id;->code:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1058
    .end local v0    # "oldSku":Lorg/solovyev/android/checkout/Sku;
    :cond_0
    iget-object v2, p2, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v2, v2, Lorg/solovyev/android/checkout/Sku$Id;->code:Ljava/lang/String;

    invoke-virtual {p0, v1, v2, p3, p4}, Lorg/solovyev/android/checkout/Billing$Requests;->changeSubscription(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I

    move-result v2

    return v2
.end method

.method public consume(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 4
    .param p1, "token"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1073
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Ljava/lang/Object;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1074
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/ConsumePurchaseRequest;

    invoke-direct {v1, p1}, Lorg/solovyev/android/checkout/ConsumePurchaseRequest;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getAllPurchases(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 5
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1008
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchases;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1009
    new-instance v0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;

    invoke-direct {v0, p0, p2}, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;-><init>(Lorg/solovyev/android/checkout/Billing$Requests;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 1010
    .local v0, "getAllPurchasesListener":Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;
    new-instance v1, Lorg/solovyev/android/checkout/GetPurchasesRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->access$1500(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lorg/solovyev/android/checkout/GetPurchasesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseVerifier;)V

    .line 1011
    .local v1, "request":Lorg/solovyev/android/checkout/GetPurchasesRequest;
    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->access$1602(Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;Lorg/solovyev/android/checkout/GetPurchasesRequest;)Lorg/solovyev/android/checkout/GetPurchasesRequest;

    .line 1012
    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v3

    iget-object v4, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method getDeliveryExecutor()Ljava/util/concurrent/Executor;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 997
    iget-boolean v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mOnMainThread:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->access$1400(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/CancellableExecutor;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lorg/solovyev/android/checkout/SameThreadExecutor;->INSTANCE:Lorg/solovyev/android/checkout/SameThreadExecutor;

    goto :goto_0
.end method

.method public getPurchases(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 4
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "continuationToken"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1002
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchases;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1003
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/GetPurchasesRequest;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing;->access$1500(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v2

    invoke-direct {v1, p1, p2, v2}, Lorg/solovyev/android/checkout/GetPurchasesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseVerifier;)V

    invoke-direct {p0, p3}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public getSkus(Ljava/lang/String;Ljava/util/List;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 4
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Skus;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1026
    .local p2, "skus":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Skus;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1027
    invoke-static {p2}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/util/Collection;)V

    .line 1028
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;

    invoke-direct {v1, p1, p2}, Lorg/solovyev/android/checkout/GetSkuDetailsRequest;-><init>(Ljava/lang/String;Ljava/util/List;)V

    invoke-direct {p0, p3}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isBillingSupported(Ljava/lang/String;)I
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 970
    invoke-static {}, Lorg/solovyev/android/checkout/Billing;->access$1200()Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/solovyev/android/checkout/Billing$Requests;->isBillingSupported(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I

    move-result v0

    return v0
.end method

.method public isBillingSupported(Ljava/lang/String;I)I
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "apiVersion"    # I

    .prologue
    .line 975
    invoke-static {}, Lorg/solovyev/android/checkout/Billing;->access$1200()Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/solovyev/android/checkout/Billing$Requests;->isBillingSupported(Ljava/lang/String;ILorg/solovyev/android/checkout/RequestListener;)I

    move-result v0

    return v0
.end method

.method public isBillingSupported(Ljava/lang/String;ILorg/solovyev/android/checkout/RequestListener;)I
    .locals 4
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "apiVersion"    # I
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 981
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Ljava/lang/Object;>;"
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 982
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/BillingSupportedRequest;

    invoke-direct {v1, p1, p2}, Lorg/solovyev/android/checkout/BillingSupportedRequest;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, p3}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public isBillingSupported(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 987
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Ljava/lang/Object;>;"
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, p2}, Lorg/solovyev/android/checkout/Billing$Requests;->isBillingSupported(Ljava/lang/String;ILorg/solovyev/android/checkout/RequestListener;)I

    move-result v0

    return v0
.end method

.method public isChangeSubscriptionSupported(Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Ljava/lang/Object;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1063
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Ljava/lang/Object;>;"
    const-string v0, "subs"

    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1, p1}, Lorg/solovyev/android/checkout/Billing$Requests;->isBillingSupported(Ljava/lang/String;ILorg/solovyev/android/checkout/RequestListener;)I

    move-result v0

    return v0
.end method

.method public isPurchased(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I
    .locals 5
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Ljava/lang/Boolean;",
            ">;)I"
        }
    .end annotation

    .prologue
    .line 1017
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Ljava/lang/Boolean;>;"
    invoke-static {p2}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1018
    new-instance v0, Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;

    invoke-direct {v0, p0, p2, p3}, Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;-><init>(Lorg/solovyev/android/checkout/Billing$Requests;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 1019
    .local v0, "isPurchasedListener":Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;
    new-instance v1, Lorg/solovyev/android/checkout/GetPurchasesRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v3}, Lorg/solovyev/android/checkout/Billing;->access$1500(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    move-result-object v3

    invoke-virtual {v3}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v3

    invoke-direct {v1, p1, v2, v3}, Lorg/solovyev/android/checkout/GetPurchasesRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseVerifier;)V

    .line 1020
    .local v1, "request":Lorg/solovyev/android/checkout/GetPurchasesRequest;
    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;->access$1702(Lorg/solovyev/android/checkout/Billing$Requests$IsPurchasedListener;Lorg/solovyev/android/checkout/GetPurchasesRequest;)Lorg/solovyev/android/checkout/GetPurchasesRequest;

    .line 1021
    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v3

    iget-object v4, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v2, v1, v3, v4}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v2

    return v2
.end method

.method public purchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I
    .locals 4
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4, "purchaseFlow"    # Lorg/solovyev/android/checkout/PurchaseFlow;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1033
    invoke-static {p1}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1034
    invoke-static {p2}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 1035
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    new-instance v1, Lorg/solovyev/android/checkout/PurchaseRequest;

    invoke-direct {v1, p1, p2, p3}, Lorg/solovyev/android/checkout/PurchaseRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p4}, Lorg/solovyev/android/checkout/Billing$Requests;->wrapListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests;->mTag:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2, v3}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public purchase(Lorg/solovyev/android/checkout/Sku;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I
    .locals 2
    .param p1, "sku"    # Lorg/solovyev/android/checkout/Sku;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "purchaseFlow"    # Lorg/solovyev/android/checkout/PurchaseFlow;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1068
    iget-object v0, p1, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v0, v0, Lorg/solovyev/android/checkout/Sku$Id;->product:Ljava/lang/String;

    iget-object v1, p1, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v1, v1, Lorg/solovyev/android/checkout/Sku$Id;->code:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/solovyev/android/checkout/Billing$Requests;->purchase(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/PurchaseFlow;)I

    move-result v0

    return v0
.end method
