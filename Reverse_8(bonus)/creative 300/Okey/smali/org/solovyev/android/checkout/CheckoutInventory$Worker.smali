.class Lorg/solovyev/android/checkout/CheckoutInventory$Worker;
.super Ljava/lang/Object;
.source "CheckoutInventory.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Checkout$Listener;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/CheckoutInventory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Worker"
.end annotation


# instance fields
.field private mCount:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mProducts:Lorg/solovyev/android/checkout/Inventory$Products;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/CheckoutInventory;


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/CheckoutInventory;Lorg/solovyev/android/checkout/BaseInventory$Task;)V
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/CheckoutInventory;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "task"    # Lorg/solovyev/android/checkout/BaseInventory$Task;

    .prologue
    .line 44
    iput-object p1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/Inventory$Products;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    .line 45
    iput-object p2, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    .line 46
    return-void
.end method

.method static synthetic access$000(Lorg/solovyev/android/checkout/CheckoutInventory$Worker;)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/CheckoutInventory$Worker;

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown()V

    return-void
.end method

.method private countDown()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    iget-object v0, v0, Lorg/solovyev/android/checkout/CheckoutInventory;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Must be synchronized"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 84
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown(I)V

    .line 85
    return-void
.end method

.method private countDown(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    iget-object v0, v0, Lorg/solovyev/android/checkout/CheckoutInventory;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Must be synchronized"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 89
    iget v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mCount:I

    sub-int/2addr v0, p1

    iput v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mCount:I

    .line 90
    iget v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mCount:I

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t be negative"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 91
    iget v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mCount:I

    if-nez v0, :cond_0

    .line 92
    iget-object v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-virtual {v0, v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->onDone(Lorg/solovyev/android/checkout/Inventory$Products;)V

    .line 94
    :cond_0
    return-void

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadPurchases(Lorg/solovyev/android/checkout/BillingRequests;Lorg/solovyev/android/checkout/Inventory$Product;)V
    .locals 3
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "product"    # Lorg/solovyev/android/checkout/Inventory$Product;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 98
    iget-object v0, p2, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    new-instance v2, Lorg/solovyev/android/checkout/CheckoutInventory$Worker$1;

    invoke-direct {v2, p0, p2}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker$1;-><init>(Lorg/solovyev/android/checkout/CheckoutInventory$Worker;Lorg/solovyev/android/checkout/Inventory$Product;)V

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/CheckoutInventory;->synchronizedListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/solovyev/android/checkout/BillingRequests;->getAllPurchases(Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)I

    .line 110
    return-void
.end method

.method private loadSkus(Lorg/solovyev/android/checkout/BillingRequests;Lorg/solovyev/android/checkout/Inventory$Product;)V
    .locals 4
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "product"    # Lorg/solovyev/android/checkout/Inventory$Product;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 113
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->getRequest()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v1

    iget-object v2, p2, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Inventory$Request;->getSkus(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 114
    .local v0, "skuIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "There are no SKUs for \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" product. No SKU information will be loaded"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    .line 117
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    iget-object v2, v1, Lorg/solovyev/android/checkout/CheckoutInventory;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 118
    :try_start_0
    invoke-direct {p0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown()V

    .line 119
    monitor-exit v2

    .line 134
    :goto_0
    return-void

    .line 119
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 122
    :cond_0
    iget-object v1, p2, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    iget-object v2, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    new-instance v3, Lorg/solovyev/android/checkout/CheckoutInventory$Worker$2;

    invoke-direct {v3, p0, p2}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker$2;-><init>(Lorg/solovyev/android/checkout/CheckoutInventory$Worker;Lorg/solovyev/android/checkout/Inventory$Product;)V

    invoke-virtual {v2, v3}, Lorg/solovyev/android/checkout/CheckoutInventory;->synchronizedListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v2

    invoke-interface {p1, v1, v0, v2}, Lorg/solovyev/android/checkout/BillingRequests;->getSkus(Ljava/lang/String;Ljava/util/List;Lorg/solovyev/android/checkout/RequestListener;)I

    goto :goto_0
.end method


# virtual methods
.method public onReady(Lorg/solovyev/android/checkout/BillingRequests;)V
    .locals 0
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 60
    return-void
.end method

.method public onReady(Lorg/solovyev/android/checkout/BillingRequests;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "requests"    # Lorg/solovyev/android/checkout/BillingRequests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "productId"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "billingSupported"    # Z

    .prologue
    .line 65
    new-instance v0, Lorg/solovyev/android/checkout/Inventory$Product;

    invoke-direct {v0, p2, p3}, Lorg/solovyev/android/checkout/Inventory$Product;-><init>(Ljava/lang/String;Z)V

    .line 66
    .local v0, "product":Lorg/solovyev/android/checkout/Inventory$Product;
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    iget-object v2, v1, Lorg/solovyev/android/checkout/CheckoutInventory;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 67
    :try_start_0
    invoke-direct {p0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown()V

    .line 68
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-virtual {v1, v0}, Lorg/solovyev/android/checkout/Inventory$Products;->add(Lorg/solovyev/android/checkout/Inventory$Product;)V

    .line 69
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, v0, Lorg/solovyev/android/checkout/Inventory$Product;->supported:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->getRequest()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/solovyev/android/checkout/Inventory$Request;->shouldLoadPurchases(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    invoke-direct {p0, p1, v0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->loadPurchases(Lorg/solovyev/android/checkout/BillingRequests;Lorg/solovyev/android/checkout/Inventory$Product;)V

    .line 74
    :goto_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lorg/solovyev/android/checkout/Inventory$Product;->supported:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mTask:Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/BaseInventory$Task;->getRequest()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/solovyev/android/checkout/Inventory$Request;->shouldLoadSkus(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 75
    invoke-direct {p0, p1, v0}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->loadSkus(Lorg/solovyev/android/checkout/BillingRequests;Lorg/solovyev/android/checkout/Inventory$Product;)V

    .line 79
    :goto_1
    monitor-exit v2

    .line 80
    return-void

    .line 72
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown(I)V

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 77
    :cond_1
    const/4 v1, 0x1

    :try_start_1
    invoke-direct {p0, v1}, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->countDown(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public run()V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    iput v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->mCount:I

    .line 55
    iget-object v0, p0, Lorg/solovyev/android/checkout/CheckoutInventory$Worker;->this$0:Lorg/solovyev/android/checkout/CheckoutInventory;

    iget-object v0, v0, Lorg/solovyev/android/checkout/CheckoutInventory;->mCheckout:Lorg/solovyev/android/checkout/Checkout;

    invoke-virtual {v0, p0}, Lorg/solovyev/android/checkout/Checkout;->whenReady(Lorg/solovyev/android/checkout/Checkout$Listener;)V

    .line 56
    return-void
.end method
