.class final Lorg/solovyev/android/checkout/BillingSupportedRequest;
.super Lorg/solovyev/android/checkout/Request;
.source "BillingSupportedRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/solovyev/android/checkout/Request",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final mProduct:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 40
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lorg/solovyev/android/checkout/BillingSupportedRequest;-><init>(Ljava/lang/String;I)V

    .line 41
    return-void
.end method

.method constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "apiVersion"    # I

    .prologue
    .line 44
    sget-object v0, Lorg/solovyev/android/checkout/RequestType;->BILLING_SUPPORTED:Lorg/solovyev/android/checkout/RequestType;

    invoke-direct {p0, v0, p2}, Lorg/solovyev/android/checkout/Request;-><init>(Lorg/solovyev/android/checkout/RequestType;I)V

    .line 45
    iput-object p1, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mProduct:Ljava/lang/String;

    .line 46
    return-void
.end method


# virtual methods
.method protected getCacheKey()Ljava/lang/String;
    .locals 2
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 59
    iget v0, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mApiVersion:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 60
    iget-object v0, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mProduct:Ljava/lang/String;

    .line 62
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mProduct:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mApiVersion:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public start(Lcom/android/vending/billing/IInAppBillingService;Ljava/lang/String;)V
    .locals 3
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "packageName"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 50
    iget v1, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mApiVersion:I

    iget-object v2, p0, Lorg/solovyev/android/checkout/BillingSupportedRequest;->mProduct:Ljava/lang/String;

    invoke-interface {p1, v1, p2, v2}, Lcom/android/vending/billing/IInAppBillingService;->isBillingSupported(ILjava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 51
    .local v0, "response":I
    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/BillingSupportedRequest;->handleError(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, v1}, Lorg/solovyev/android/checkout/BillingSupportedRequest;->onSuccess(Ljava/lang/Object;)V

    .line 54
    :cond_0
    return-void
.end method
