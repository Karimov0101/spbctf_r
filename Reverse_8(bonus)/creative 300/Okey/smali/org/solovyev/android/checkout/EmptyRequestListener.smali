.class public Lorg/solovyev/android/checkout/EmptyRequestListener;
.super Ljava/lang/Object;
.source "EmptyRequestListener.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestListener;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/RequestListener",
        "<TR;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    .local p0, "this":Lorg/solovyev/android/checkout/EmptyRequestListener;, "Lorg/solovyev/android/checkout/EmptyRequestListener<TR;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 0
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 39
    .local p0, "this":Lorg/solovyev/android/checkout/EmptyRequestListener;, "Lorg/solovyev/android/checkout/EmptyRequestListener<TR;>;"
    return-void
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lorg/solovyev/android/checkout/EmptyRequestListener;, "Lorg/solovyev/android/checkout/EmptyRequestListener<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    return-void
.end method
