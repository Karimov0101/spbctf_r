.class public abstract Lorg/solovyev/android/checkout/UiCheckout;
.super Lorg/solovyev/android/checkout/Checkout;
.source "UiCheckout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/UiCheckout$OneShotRequestListener;
    }
.end annotation


# static fields
.field static final DEFAULT_REQUEST_CODE:I = 0xcafe


# instance fields
.field private final mFlows:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lorg/solovyev/android/checkout/PurchaseFlow;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "billing"    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lorg/solovyev/android/checkout/Checkout;-><init>(Ljava/lang/Object;Lorg/solovyev/android/checkout/Billing;)V

    .line 56
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    .line 61
    return-void
.end method

.method private createPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;Z)Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 5
    .param p1, "requestCode"    # I
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "oneShot"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;Z)",
            "Lorg/solovyev/android/checkout/PurchaseFlow;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 196
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    iget-object v2, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 197
    .local v0, "flow":Lorg/solovyev/android/checkout/PurchaseFlow;
    if-eqz v0, :cond_0

    .line 198
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Purchase flow associated with requestCode="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " already exists"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 200
    :cond_0
    if-eqz p3, :cond_1

    .line 201
    new-instance v1, Lorg/solovyev/android/checkout/UiCheckout$OneShotRequestListener;

    invoke-direct {v1, p0, p2, p1}, Lorg/solovyev/android/checkout/UiCheckout$OneShotRequestListener;-><init>(Lorg/solovyev/android/checkout/UiCheckout;Lorg/solovyev/android/checkout/RequestListener;I)V

    .end local p2    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    .local v1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    move-object p2, v1

    .line 203
    .end local v1    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    .restart local p2    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    :cond_1
    iget-object v2, p0, Lorg/solovyev/android/checkout/UiCheckout;->mBilling:Lorg/solovyev/android/checkout/Billing;

    invoke-virtual {p0}, Lorg/solovyev/android/checkout/UiCheckout;->makeIntentStarter()Lorg/solovyev/android/checkout/IntentStarter;

    move-result-object v3

    invoke-virtual {v2, v3, p1, p2}, Lorg/solovyev/android/checkout/Billing;->createPurchaseFlow(Lorg/solovyev/android/checkout/IntentStarter;ILorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;

    move-result-object v0

    .line 204
    iget-object v2, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v2, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 205
    return-object v0
.end method


# virtual methods
.method public createOneShotPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 1
    .param p1, "requestCode"    # I
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)",
            "Lorg/solovyev/android/checkout/PurchaseFlow;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 191
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/solovyev/android/checkout/UiCheckout;->createPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;Z)Lorg/solovyev/android/checkout/PurchaseFlow;

    move-result-object v0

    return-object v0
.end method

.method public createOneShotPurchaseFlow(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)",
            "Lorg/solovyev/android/checkout/PurchaseFlow;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 144
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    const v0, 0xcafe

    invoke-virtual {p0, v0, p1}, Lorg/solovyev/android/checkout/UiCheckout;->createOneShotPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;

    move-result-object v0

    return-object v0
.end method

.method public createPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/solovyev/android/checkout/UiCheckout;->createPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;Z)Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 90
    return-void
.end method

.method public createPurchaseFlow(Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    const v0, 0xcafe

    invoke-virtual {p0, v0, p1}, Lorg/solovyev/android/checkout/UiCheckout;->createPurchaseFlow(ILorg/solovyev/android/checkout/RequestListener;)V

    .line 74
    return-void
.end method

.method public destroyPurchaseFlow()V
    .locals 1

    .prologue
    .line 96
    const v0, 0xcafe

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/UiCheckout;->destroyPurchaseFlow(I)V

    .line 97
    return-void
.end method

.method public destroyPurchaseFlow(I)V
    .locals 2
    .param p1, "requestCode"    # I

    .prologue
    .line 106
    iget-object v1, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 107
    .local v0, "flow":Lorg/solovyev/android/checkout/PurchaseFlow;
    if-nez v0, :cond_0

    .line 114
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->delete(I)V

    .line 113
    invoke-virtual {v0}, Lorg/solovyev/android/checkout/PurchaseFlow;->cancel()V

    goto :goto_0
.end method

.method public getPurchaseFlow()Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 121
    const v0, 0xcafe

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/UiCheckout;->getPurchaseFlow(I)Lorg/solovyev/android/checkout/PurchaseFlow;

    move-result-object v0

    return-object v0
.end method

.method public getPurchaseFlow(I)Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 3
    .param p1, "requestCode"    # I
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 131
    iget-object v1, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 132
    .local v0, "flow":Lorg/solovyev/android/checkout/PurchaseFlow;
    if-nez v0, :cond_0

    .line 133
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Purchase flow doesn\'t exist. Have you forgotten to create it?"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 135
    :cond_0
    return-object v0
.end method

.method protected abstract makeIntentStarter()Lorg/solovyev/android/checkout/IntentStarter;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 243
    iget-object v1, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 244
    .local v0, "flow":Lorg/solovyev/android/checkout/PurchaseFlow;
    if-nez v0, :cond_0

    .line 245
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Purchase flow doesn\'t exist for requestCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Have you forgotten to create it?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    .line 246
    const/4 v1, 0x0

    .line 249
    :goto_0
    return v1

    .line 248
    :cond_0
    invoke-virtual {v0, p1, p2, p3}, Lorg/solovyev/android/checkout/PurchaseFlow;->onActivityResult(IILandroid/content/Intent;)V

    .line 249
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
    .param p2, "sku"    # Ljava/lang/String;
    .param p3, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 216
    .local p4, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-virtual {p0, p4}, Lorg/solovyev/android/checkout/UiCheckout;->createOneShotPurchaseFlow(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;

    .line 217
    new-instance v0, Lorg/solovyev/android/checkout/UiCheckout$1;

    invoke-direct {v0, p0, p1, p2, p3}, Lorg/solovyev/android/checkout/UiCheckout$1;-><init>(Lorg/solovyev/android/checkout/UiCheckout;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/UiCheckout;->whenReady(Lorg/solovyev/android/checkout/Checkout$Listener;)V

    .line 223
    return-void
.end method

.method public startPurchaseFlow(Lorg/solovyev/android/checkout/Sku;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 2
    .param p1, "sku"    # Lorg/solovyev/android/checkout/Sku;
    .param p2, "payload"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/Sku;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 229
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    iget-object v0, p1, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v0, v0, Lorg/solovyev/android/checkout/Sku$Id;->product:Ljava/lang/String;

    iget-object v1, p1, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v1, v1, Lorg/solovyev/android/checkout/Sku$Id;->code:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, p2, p3}, Lorg/solovyev/android/checkout/UiCheckout;->startPurchaseFlow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/RequestListener;)V

    .line 230
    return-void
.end method

.method public stop()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lorg/solovyev/android/checkout/UiCheckout;->mFlows:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 66
    invoke-super {p0}, Lorg/solovyev/android/checkout/Checkout;->stop()V

    .line 67
    return-void
.end method
