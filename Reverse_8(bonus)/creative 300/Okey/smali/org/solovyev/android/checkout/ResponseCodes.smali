.class public final Lorg/solovyev/android/checkout/ResponseCodes;
.super Ljava/lang/Object;
.source "ResponseCodes.java"


# static fields
.field public static final ACCOUNT_ERROR:I = 0x2

.field public static final BILLING_UNAVAILABLE:I = 0x3

.field public static final DEVELOPER_ERROR:I = 0x5

.field public static final ERROR:I = 0x6

.field public static final EXCEPTION:I = 0x2711

.field public static final ITEM_ALREADY_OWNED:I = 0x7

.field public static final ITEM_NOT_OWNED:I = 0x8

.field public static final ITEM_UNAVAILABLE:I = 0x4

.field public static final NULL_INTENT:I = 0x2713

.field public static final OK:I = 0x0

.field public static final SERVICE_NOT_CONNECTED:I = 0x2710

.field public static final USER_CANCELED:I = 0x1

.field public static final WRONG_SIGNATURE:I = 0x2712


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method public static toString(I)Ljava/lang/String;
    .locals 1
    .param p0, "code"    # I

    .prologue
    .line 97
    sparse-switch p0, :sswitch_data_0

    .line 125
    const-string v0, "UNKNOWN"

    :goto_0
    return-object v0

    .line 99
    :sswitch_0
    const-string v0, "OK"

    goto :goto_0

    .line 101
    :sswitch_1
    const-string v0, "USER_CANCELED"

    goto :goto_0

    .line 103
    :sswitch_2
    const-string v0, "ACCOUNT_ERROR"

    goto :goto_0

    .line 105
    :sswitch_3
    const-string v0, "BILLING_UNAVAILABLE"

    goto :goto_0

    .line 107
    :sswitch_4
    const-string v0, "ITEM_UNAVAILABLE"

    goto :goto_0

    .line 109
    :sswitch_5
    const-string v0, "DEVELOPER_ERROR"

    goto :goto_0

    .line 111
    :sswitch_6
    const-string v0, "ERROR"

    goto :goto_0

    .line 113
    :sswitch_7
    const-string v0, "ITEM_ALREADY_OWNED"

    goto :goto_0

    .line 115
    :sswitch_8
    const-string v0, "ITEM_NOT_OWNED"

    goto :goto_0

    .line 117
    :sswitch_9
    const-string v0, "SERVICE_NOT_CONNECTED"

    goto :goto_0

    .line 119
    :sswitch_a
    const-string v0, "EXCEPTION"

    goto :goto_0

    .line 121
    :sswitch_b
    const-string v0, "WRONG_SIGNATURE"

    goto :goto_0

    .line 123
    :sswitch_c
    const-string v0, "NULL_INTENT"

    goto :goto_0

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x2710 -> :sswitch_9
        0x2711 -> :sswitch_a
        0x2712 -> :sswitch_b
        0x2713 -> :sswitch_c
    .end sparse-switch
.end method
