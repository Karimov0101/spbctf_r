.class public final Lorg/solovyev/android/checkout/BaseInventory$Task;
.super Ljava/lang/Object;
.source "BaseInventory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/BaseInventory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x14
    name = "Task"
.end annotation


# instance fields
.field private mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mId:I

.field private final mProducts:Lorg/solovyev/android/checkout/Inventory$Products;
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mRequest:Lorg/solovyev/android/checkout/Inventory$Request;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/BaseInventory;


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/BaseInventory;Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)V
    .locals 1
    .param p1, "this$0"    # Lorg/solovyev/android/checkout/BaseInventory;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "request"    # Lorg/solovyev/android/checkout/Inventory$Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "callback"    # Lorg/solovyev/android/checkout/Inventory$Callback;

    .prologue
    .line 115
    iput-object p1, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory;->access$200(Lorg/solovyev/android/checkout/BaseInventory;)Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    iput v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mId:I

    .line 112
    new-instance v0, Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/Inventory$Products;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    .line 116
    invoke-virtual {p2}, Lorg/solovyev/android/checkout/Inventory$Request;->copy()Lorg/solovyev/android/checkout/Inventory$Request;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mRequest:Lorg/solovyev/android/checkout/Inventory$Request;

    .line 117
    iput-object p3, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    .line 118
    return-void
.end method

.method static synthetic access$000(Lorg/solovyev/android/checkout/BaseInventory$Task;)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/BaseInventory$Task;

    .prologue
    .line 104
    invoke-direct {p0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->cancel()V

    return-void
.end method

.method static synthetic access$100(Lorg/solovyev/android/checkout/BaseInventory$Task;)I
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/BaseInventory$Task;

    .prologue
    .line 104
    iget v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mId:I

    return v0
.end method

.method private cancel()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v1, v0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    .line 129
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory;->access$300(Lorg/solovyev/android/checkout/BaseInventory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 130
    monitor-exit v1

    .line 131
    return-void

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private existsUnsupported()Z
    .locals 3

    .prologue
    .line 172
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v1, v1, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    const-string v2, "Must be synchronized"

    invoke-static {v1, v2}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 173
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/Inventory$Products;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Inventory$Product;

    .line 174
    .local v0, "product":Lorg/solovyev/android/checkout/Inventory$Product;
    iget-boolean v2, v0, Lorg/solovyev/android/checkout/Inventory$Product;->supported:Z

    if-nez v2, :cond_0

    .line 175
    const/4 v1, 0x1

    .line 179
    .end local v0    # "product":Lorg/solovyev/android/checkout/Inventory$Product;
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private onDone()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v0, v0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Must be synchronized"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 162
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory;->access$300(Lorg/solovyev/android/checkout/BaseInventory;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 166
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/Inventory$Callback;->onLoaded(Lorg/solovyev/android/checkout/Inventory$Products;)V

    .line 167
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    goto :goto_0
.end method


# virtual methods
.method public getRequest()Lorg/solovyev/android/checkout/Inventory$Request;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mRequest:Lorg/solovyev/android/checkout/Inventory$Request;

    return-object v0
.end method

.method public isCancelled()Z
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v1, v0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mCallback:Lorg/solovyev/android/checkout/Inventory$Callback;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onDone(Lorg/solovyev/android/checkout/Inventory$Products;)V
    .locals 2
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 143
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v1, v0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/Inventory$Products;->merge(Lorg/solovyev/android/checkout/Inventory$Products;)V

    .line 145
    invoke-direct {p0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->onDone()V

    .line 146
    monitor-exit v1

    .line 147
    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onMaybeDone(Lorg/solovyev/android/checkout/Inventory$Products;)Z
    .locals 2
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 150
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    iget-object v1, v0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 151
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->mProducts:Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/Inventory$Products;->merge(Lorg/solovyev/android/checkout/Inventory$Products;)V

    .line 152
    invoke-direct {p0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->existsUnsupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 153
    invoke-direct {p0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->onDone()V

    .line 154
    const/4 v0, 0x1

    monitor-exit v1

    .line 156
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory$Task;->this$0:Lorg/solovyev/android/checkout/BaseInventory;

    invoke-virtual {v0, p0}, Lorg/solovyev/android/checkout/BaseInventory;->createWorker(Lorg/solovyev/android/checkout/BaseInventory$Task;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 135
    return-void
.end method
