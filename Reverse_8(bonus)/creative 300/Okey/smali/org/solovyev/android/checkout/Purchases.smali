.class public final Lorg/solovyev/android/checkout/Purchases;
.super Ljava/lang/Object;
.source "Purchases.java"


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field static final BUNDLE_CONTINUATION_TOKEN:Ljava/lang/String; = "INAPP_CONTINUATION_TOKEN"

.field static final BUNDLE_DATA_LIST:Ljava/lang/String; = "INAPP_PURCHASE_DATA_LIST"

.field static final BUNDLE_SIGNATURE_LIST:Ljava/lang/String; = "INAPP_DATA_SIGNATURE_LIST"


# instance fields
.field public final continuationToken:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final list:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final product:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "continuationToken"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "list":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lorg/solovyev/android/checkout/Purchases;->product:Ljava/lang/String;

    .line 74
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    .line 75
    iput-object p3, p0, Lorg/solovyev/android/checkout/Purchases;->continuationToken:Ljava/lang/String;

    .line 76
    return-void
.end method

.method private static extractDatasList(Landroid/os/Bundle;)Ljava/util/List;
    .locals 2
    .param p0, "bundle"    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 106
    const-string v1, "INAPP_PURCHASE_DATA_LIST"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 107
    .local v0, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .end local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :goto_0
    return-object v0

    .restart local v0    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method static fromBundle(Landroid/os/Bundle;Ljava/lang/String;)Lorg/solovyev/android/checkout/Purchases;
    .locals 3
    .param p0, "bundle"    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 80
    invoke-static {p0}, Lorg/solovyev/android/checkout/Purchases;->getContinuationTokenFromBundle(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 81
    .local v0, "continuationToken":Ljava/lang/String;
    invoke-static {p0}, Lorg/solovyev/android/checkout/Purchases;->getListFromBundle(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v1

    .line 82
    .local v1, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    new-instance v2, Lorg/solovyev/android/checkout/Purchases;

    invoke-direct {v2, p1, v1, v0}, Lorg/solovyev/android/checkout/Purchases;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    return-object v2
.end method

.method static getContinuationTokenFromBundle(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1
    .param p0, "bundle"    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 87
    const-string v0, "INAPP_CONTINUATION_TOKEN"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getListFromBundle(Landroid/os/Bundle;)Ljava/util/List;
    .locals 7
    .param p0, "bundle"    # Landroid/os/Bundle;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/Bundle;",
            ")",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 92
    invoke-static {p0}, Lorg/solovyev/android/checkout/Purchases;->extractDatasList(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object v1

    .line 93
    .local v1, "datas":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v6, "INAPP_DATA_SIGNATURE_LIST"

    invoke-virtual {p0, v6}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 95
    .local v5, "signatures":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v3, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 96
    .local v3, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 97
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 98
    .local v0, "data":Ljava/lang/String;
    if-eqz v5, :cond_0

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    move-object v4, v6

    .line 99
    .local v4, "signature":Ljava/lang/String;
    :goto_1
    invoke-static {v0, v4}, Lorg/solovyev/android/checkout/Purchase;->fromJson(Ljava/lang/String;Ljava/lang/String;)Lorg/solovyev/android/checkout/Purchase;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 98
    .end local v4    # "signature":Ljava/lang/String;
    :cond_0
    const-string v4, ""

    goto :goto_1

    .line 101
    .end local v0    # "data":Ljava/lang/String;
    :cond_1
    return-object v3
.end method

.method static getPurchaseInState(Ljava/util/List;Ljava/lang/String;Lorg/solovyev/android/checkout/Purchase$State;)Lorg/solovyev/android/checkout/Purchase;
    .locals 3
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "state"    # Lorg/solovyev/android/checkout/Purchase$State;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/Purchase$State;",
            ")",
            "Lorg/solovyev/android/checkout/Purchase;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 112
    .local p0, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Purchase;

    .line 113
    .local v0, "purchase":Lorg/solovyev/android/checkout/Purchase;
    iget-object v2, v0, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 114
    iget-object v2, v0, Lorg/solovyev/android/checkout/Purchase;->state:Lorg/solovyev/android/checkout/Purchase$State;

    if-ne v2, p2, :cond_0

    .line 119
    .end local v0    # "purchase":Lorg/solovyev/android/checkout/Purchase;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isDangling(Ljava/util/List;Lorg/solovyev/android/checkout/Purchase;)Z
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "purchase"    # Lorg/solovyev/android/checkout/Purchase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;",
            "Lorg/solovyev/android/checkout/Purchase;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 157
    iget-object v2, p1, Lorg/solovyev/android/checkout/Purchase;->state:Lorg/solovyev/android/checkout/Purchase$State;

    sget-object v5, Lorg/solovyev/android/checkout/Purchase$State;->PURCHASED:Lorg/solovyev/android/checkout/Purchase$State;

    if-ne v2, v5, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Must not be PURCHASED"

    invoke-static {v2, v5}, Lorg/solovyev/android/checkout/Check;->isFalse(ZLjava/lang/String;)V

    .line 158
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 159
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/Purchase;

    .line 160
    .local v1, "same":Lorg/solovyev/android/checkout/Purchase;
    iget-object v2, v1, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    iget-object v5, p1, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 166
    .end local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :goto_2
    return v3

    .end local v0    # "i":I
    :cond_0
    move v2, v4

    .line 157
    goto :goto_0

    .line 158
    .restart local v0    # "i":I
    .restart local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :cond_2
    move v3, v4

    .line 166
    goto :goto_2
.end method

.method private static isNeutralized(Ljava/util/List;Lorg/solovyev/android/checkout/Purchase;)Z
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "purchase"    # Lorg/solovyev/android/checkout/Purchase;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;",
            "Lorg/solovyev/android/checkout/Purchase;",
            ")Z"
        }
    .end annotation

    .prologue
    .local p0, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 170
    iget-object v2, p1, Lorg/solovyev/android/checkout/Purchase;->state:Lorg/solovyev/android/checkout/Purchase$State;

    sget-object v5, Lorg/solovyev/android/checkout/Purchase$State;->PURCHASED:Lorg/solovyev/android/checkout/Purchase$State;

    if-ne v2, v5, :cond_0

    move v2, v3

    :goto_0
    const-string v5, "Must be PURCHASED"

    invoke-static {v2, v5}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 171
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 172
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/Purchase;

    .line 173
    .local v1, "same":Lorg/solovyev/android/checkout/Purchase;
    iget-object v2, v1, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    iget-object v5, p1, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    sget-object v2, Lorg/solovyev/android/checkout/Purchases$1;->$SwitchMap$org$solovyev$android$checkout$Purchase$State:[I

    iget-object v4, v1, Lorg/solovyev/android/checkout/Purchase;->state:Lorg/solovyev/android/checkout/Purchase$State;

    invoke-virtual {v4}, Lorg/solovyev/android/checkout/Purchase$State;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 191
    .end local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :goto_2
    return v3

    .end local v0    # "i":I
    :cond_0
    move v2, v4

    .line 170
    goto :goto_0

    .line 178
    .restart local v0    # "i":I
    .restart local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :pswitch_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Two purchases with same SKU found: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " and "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    goto :goto_2

    .line 184
    :pswitch_1
    invoke-interface {p0, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_2

    .line 171
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v1    # "same":Lorg/solovyev/android/checkout/Purchase;
    :cond_2
    move v3, v4

    .line 191
    goto :goto_2

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method static neutralize(Ljava/util/List;)Ljava/util/List;
    .locals 6
    .param p0    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .local p0, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    const/4 v5, 0x0

    .line 126
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1, p0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 128
    .end local p0    # "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    .local v1, "purchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 130
    .local v2, "result":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    invoke-static {}, Lorg/solovyev/android/checkout/PurchaseComparator;->earliestFirst()Ljava/util/Comparator;

    move-result-object v3

    invoke-static {v1, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 131
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 132
    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Purchase;

    .line 133
    .local v0, "purchase":Lorg/solovyev/android/checkout/Purchase;
    sget-object v3, Lorg/solovyev/android/checkout/Purchases$1;->$SwitchMap$org$solovyev$android$checkout$Purchase$State:[I

    iget-object v4, v0, Lorg/solovyev/android/checkout/Purchase;->state:Lorg/solovyev/android/checkout/Purchase$State;

    invoke-virtual {v4}, Lorg/solovyev/android/checkout/Purchase$State;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 147
    :cond_0
    :goto_1
    invoke-interface {v1, v5}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 135
    :pswitch_0
    invoke-static {v1, v0}, Lorg/solovyev/android/checkout/Purchases;->isNeutralized(Ljava/util/List;Lorg/solovyev/android/checkout/Purchase;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 136
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 142
    :pswitch_1
    invoke-static {v1, v0}, Lorg/solovyev/android/checkout/Purchases;->isDangling(Ljava/util/List;Lorg/solovyev/android/checkout/Purchase;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 143
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 151
    .end local v0    # "purchase":Lorg/solovyev/android/checkout/Purchase;
    :cond_1
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 153
    return-object v2

    .line 133
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getPurchase(Ljava/lang/String;)Lorg/solovyev/android/checkout/Purchase;
    .locals 3
    .param p1, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 233
    iget-object v1, p0, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Purchase;

    .line 234
    .local v0, "purchase":Lorg/solovyev/android/checkout/Purchase;
    iget-object v2, v0, Lorg/solovyev/android/checkout/Purchase;->sku:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 238
    .end local v0    # "purchase":Lorg/solovyev/android/checkout/Purchase;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPurchaseInState(Ljava/lang/String;Lorg/solovyev/android/checkout/Purchase$State;)Lorg/solovyev/android/checkout/Purchase;
    .locals 1
    .param p1, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "state"    # Lorg/solovyev/android/checkout/Purchase$State;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 262
    iget-object v0, p0, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    invoke-static {v0, p1, p2}, Lorg/solovyev/android/checkout/Purchases;->getPurchaseInState(Ljava/util/List;Ljava/lang/String;Lorg/solovyev/android/checkout/Purchase$State;)Lorg/solovyev/android/checkout/Purchase;

    move-result-object v0

    return-object v0
.end method

.method public hasPurchase(Ljava/lang/String;)Z
    .locals 1
    .param p1, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 248
    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/Purchases;->getPurchase(Ljava/lang/String;)Lorg/solovyev/android/checkout/Purchase;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPurchaseInState(Ljava/lang/String;Lorg/solovyev/android/checkout/Purchase$State;)Z
    .locals 1
    .param p1, "sku"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "state"    # Lorg/solovyev/android/checkout/Purchase$State;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 257
    invoke-virtual {p0, p1, p2}, Lorg/solovyev/android/checkout/Purchases;->getPurchaseInState(Ljava/lang/String;Lorg/solovyev/android/checkout/Purchase$State;)Lorg/solovyev/android/checkout/Purchase;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toJson()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 201
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Purchases;->toJson(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toJson(Z)Ljava/lang/String;
    .locals 1
    .param p1, "withSignatures"    # Z
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 210
    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/Purchases;->toJsonObject(Z)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method toJsonObject(Z)Lorg/json/JSONObject;
    .locals 7
    .param p1, "withSignatures"    # Z
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 215
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 217
    .local v3, "json":Lorg/json/JSONObject;
    :try_start_0
    const-string v5, "product"

    iget-object v6, p0, Lorg/solovyev/android/checkout/Purchases;->product:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 218
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 219
    .local v0, "array":Lorg/json/JSONArray;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 220
    iget-object v5, p0, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/solovyev/android/checkout/Purchase;

    .line 221
    .local v4, "purchase":Lorg/solovyev/android/checkout/Purchase;
    invoke-virtual {v4, p1}, Lorg/solovyev/android/checkout/Purchase;->toJsonObject(Z)Lorg/json/JSONObject;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 219
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 223
    .end local v4    # "purchase":Lorg/solovyev/android/checkout/Purchase;
    :cond_0
    const-string v5, "list"

    invoke-virtual {v3, v5, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    return-object v3

    .line 224
    .end local v0    # "array":Lorg/json/JSONArray;
    .end local v2    # "i":I
    :catch_0
    move-exception v1

    .line 226
    .local v1, "e":Lorg/json/JSONException;
    new-instance v5, Ljava/lang/AssertionError;

    invoke-direct {v5, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v5
.end method
