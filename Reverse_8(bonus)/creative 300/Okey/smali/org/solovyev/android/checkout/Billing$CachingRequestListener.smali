.class Lorg/solovyev/android/checkout/Billing$CachingRequestListener;
.super Lorg/solovyev/android/checkout/RequestListenerWrapper;
.source "Billing.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CachingRequestListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Lorg/solovyev/android/checkout/RequestListenerWrapper",
        "<TR;>;"
    }
.end annotation


# instance fields
.field private final mRequest:Lorg/solovyev/android/checkout/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/Request",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final synthetic this$0:Lorg/solovyev/android/checkout/Billing;


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 2
    .param p1    # Lorg/solovyev/android/checkout/Billing;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/Request",
            "<TR;>;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)V"
        }
    .end annotation

    .prologue
    .line 1175
    .local p0, "this":Lorg/solovyev/android/checkout/Billing$CachingRequestListener;, "Lorg/solovyev/android/checkout/Billing$CachingRequestListener<TR;>;"
    .local p2, "request":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->this$0:Lorg/solovyev/android/checkout/Billing;

    .line 1176
    invoke-direct {p0, p3}, Lorg/solovyev/android/checkout/RequestListenerWrapper;-><init>(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 1177
    invoke-static {p1}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v0

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/ConcurrentCache;->hasCache()Z

    move-result v0

    const-string v1, "Cache must exist"

    invoke-static {v0, v1}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 1178
    iput-object p2, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    .line 1179
    return-void
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1204
    .local p0, "this":Lorg/solovyev/android/checkout/Billing$CachingRequestListener;, "Lorg/solovyev/android/checkout/Billing$CachingRequestListener<TR;>;"
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/Request;->getType()Lorg/solovyev/android/checkout/RequestType;

    move-result-object v0

    .line 1207
    .local v0, "type":Lorg/solovyev/android/checkout/RequestType;
    sget-object v1, Lorg/solovyev/android/checkout/Billing$7;->$SwitchMap$org$solovyev$android$checkout$RequestType:[I

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/RequestType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1220
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lorg/solovyev/android/checkout/RequestListenerWrapper;->onError(ILjava/lang/Exception;)V

    .line 1221
    return-void

    .line 1210
    :pswitch_0
    const/4 v1, 0x7

    if-ne p1, v1, :cond_0

    .line 1211
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v1

    sget-object v2, Lorg/solovyev/android/checkout/RequestType;->GET_PURCHASES:Lorg/solovyev/android/checkout/RequestType;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/RequestType;->getCacheKeyType()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/ConcurrentCache;->removeAll(I)V

    goto :goto_0

    .line 1215
    :pswitch_1
    const/16 v1, 0x8

    if-ne p1, v1, :cond_0

    .line 1216
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v1

    sget-object v2, Lorg/solovyev/android/checkout/RequestType;->GET_PURCHASES:Lorg/solovyev/android/checkout/RequestType;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/RequestType;->getCacheKeyType()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/ConcurrentCache;->removeAll(I)V

    goto :goto_0

    .line 1207
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSuccess(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    .prologue
    .line 1183
    .local p0, "this":Lorg/solovyev/android/checkout/Billing$CachingRequestListener;, "Lorg/solovyev/android/checkout/Billing$CachingRequestListener<TR;>;"
    .local p1, "result":Ljava/lang/Object;, "TR;"
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v5}, Lorg/solovyev/android/checkout/Request;->getCacheKey()Ljava/lang/String;

    move-result-object v1

    .line 1184
    .local v1, "key":Ljava/lang/String;
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v5}, Lorg/solovyev/android/checkout/Request;->getType()Lorg/solovyev/android/checkout/RequestType;

    move-result-object v4

    .line 1185
    .local v4, "type":Lorg/solovyev/android/checkout/RequestType;
    if-eqz v1, :cond_0

    .line 1186
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1187
    .local v2, "now":J
    new-instance v0, Lorg/solovyev/android/checkout/Cache$Entry;

    iget-wide v6, v4, Lorg/solovyev/android/checkout/RequestType;->expiresIn:J

    add-long/2addr v6, v2

    invoke-direct {v0, p1, v6, v7}, Lorg/solovyev/android/checkout/Cache$Entry;-><init>(Ljava/lang/Object;J)V

    .line 1188
    .local v0, "entry":Lorg/solovyev/android/checkout/Cache$Entry;
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v5}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v5

    invoke-virtual {v4, v1}, Lorg/solovyev/android/checkout/RequestType;->getCacheKey(Ljava/lang/String;)Lorg/solovyev/android/checkout/Cache$Key;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Lorg/solovyev/android/checkout/ConcurrentCache;->putIfNotExist(Lorg/solovyev/android/checkout/Cache$Key;Lorg/solovyev/android/checkout/Cache$Entry;)V

    .line 1190
    .end local v0    # "entry":Lorg/solovyev/android/checkout/Cache$Entry;
    .end local v2    # "now":J
    :cond_0
    sget-object v5, Lorg/solovyev/android/checkout/Billing$7;->$SwitchMap$org$solovyev$android$checkout$RequestType:[I

    invoke-virtual {v4}, Lorg/solovyev/android/checkout/RequestType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 1199
    :goto_0
    invoke-super {p0, p1}, Lorg/solovyev/android/checkout/RequestListenerWrapper;->onSuccess(Ljava/lang/Object;)V

    .line 1200
    return-void

    .line 1196
    :pswitch_0
    iget-object v5, p0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;->this$0:Lorg/solovyev/android/checkout/Billing;

    invoke-static {v5}, Lorg/solovyev/android/checkout/Billing;->access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;

    move-result-object v5

    sget-object v6, Lorg/solovyev/android/checkout/RequestType;->GET_PURCHASES:Lorg/solovyev/android/checkout/RequestType;

    invoke-virtual {v6}, Lorg/solovyev/android/checkout/RequestType;->getCacheKeyType()I

    move-result v6

    invoke-virtual {v5, v6}, Lorg/solovyev/android/checkout/ConcurrentCache;->removeAll(I)V

    goto :goto_0

    .line 1190
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
