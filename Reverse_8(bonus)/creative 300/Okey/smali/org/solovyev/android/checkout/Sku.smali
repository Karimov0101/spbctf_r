.class public final Lorg/solovyev/android/checkout/Sku;
.super Ljava/lang/Object;
.source "Sku.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/Sku$Price;,
        Lorg/solovyev/android/checkout/Sku$Id;
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# instance fields
.field public final description:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final id:Lorg/solovyev/android/checkout/Sku$Id;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mDisplayTitle:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final price:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field public final title:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "json"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 71
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "object":Lorg/json/JSONObject;
    new-instance v1, Lorg/solovyev/android/checkout/Sku$Id;

    const-string v2, "productId"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lorg/solovyev/android/checkout/Sku$Id;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    .line 73
    const-string v1, "price"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    .line 74
    invoke-static {v0}, Lorg/solovyev/android/checkout/Sku$Price;->access$000(Lorg/json/JSONObject;)Lorg/solovyev/android/checkout/Sku$Price;

    move-result-object v1

    iput-object v1, p0, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    .line 75
    const-string v1, "title"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/solovyev/android/checkout/Sku;->title:Ljava/lang/String;

    .line 76
    const-string v1, "description"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lorg/solovyev/android/checkout/Sku;->description:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/solovyev/android/checkout/Sku$Price;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "code"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "price"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p4, "detailedPrice"    # Lorg/solovyev/android/checkout/Sku$Price;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p5, "title"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p6, "description"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Lorg/solovyev/android/checkout/Sku$Id;

    invoke-direct {v0, p1, p2}, Lorg/solovyev/android/checkout/Sku$Id;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    .line 64
    iput-object p3, p0, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    .line 65
    iput-object p4, p0, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    .line 66
    iput-object p5, p0, Lorg/solovyev/android/checkout/Sku;->title:Ljava/lang/String;

    .line 67
    iput-object p6, p0, Lorg/solovyev/android/checkout/Sku;->description:Ljava/lang/String;

    .line 68
    return-void
.end method

.method static fromJson(Ljava/lang/String;Ljava/lang/String;)Lorg/solovyev/android/checkout/Sku;
    .locals 1
    .param p0, "json"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 81
    new-instance v0, Lorg/solovyev/android/checkout/Sku;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/Sku;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static indexOfAppName(Ljava/lang/String;)I
    .locals 4
    .param p0, "title"    # Ljava/lang/String;

    .prologue
    .line 141
    const/4 v1, 0x0

    .line 142
    .local v1, "depth":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_3

    .line 143
    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 144
    .local v0, "c":C
    const/16 v3, 0x29

    if-ne v0, v3, :cond_1

    .line 145
    add-int/lit8 v1, v1, 0x1

    .line 149
    :cond_0
    :goto_1
    if-nez v1, :cond_2

    .line 153
    .end local v0    # "c":C
    .end local v2    # "i":I
    :goto_2
    return v2

    .line 146
    .restart local v0    # "c":C
    .restart local v2    # "i":I
    :cond_1
    const/16 v3, 0x28

    if-ne v0, v3, :cond_0

    .line 147
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 142
    :cond_2
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 153
    .end local v0    # "c":C
    :cond_3
    const/4 v2, -0x1

    goto :goto_2
.end method

.method private static makeDisplayTitle(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "title"    # Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 120
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 121
    const-string p0, ""

    .line 131
    .end local p0    # "title":Ljava/lang/String;
    .local v1, "lastChar":C
    :cond_0
    :goto_0
    return-object p0

    .line 123
    .end local v1    # "lastChar":C
    .restart local p0    # "title":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    .line 124
    .restart local v1    # "lastChar":C
    const/16 v2, 0x29

    if-ne v1, v2, :cond_0

    .line 127
    invoke-static {p0}, Lorg/solovyev/android/checkout/Sku;->indexOfAppName(Ljava/lang/String;)I

    move-result v0

    .line 128
    .local v0, "i":I
    if-lez v0, :cond_0

    .line 129
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private toJsonObject()Lorg/json/JSONObject;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 91
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 92
    .local v0, "json":Lorg/json/JSONObject;
    const-string v1, "productId"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v2, v2, Lorg/solovyev/android/checkout/Sku$Id;->code:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 93
    const-string v1, "price"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 94
    const-string v1, "price_amount_micros"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-wide v2, v2, Lorg/solovyev/android/checkout/Sku$Price;->amount:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 95
    const-string v1, "price_currency_code"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->detailedPrice:Lorg/solovyev/android/checkout/Sku$Price;

    iget-object v2, v2, Lorg/solovyev/android/checkout/Sku$Price;->currency:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 96
    const-string v1, "title"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->title:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 97
    const-string v1, "description"

    iget-object v2, p0, Lorg/solovyev/android/checkout/Sku;->description:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 98
    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    .line 158
    if-ne p0, p1, :cond_0

    const/4 v1, 0x1

    .line 162
    :goto_0
    return v1

    .line 159
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 161
    check-cast v0, Lorg/solovyev/android/checkout/Sku;

    .line 162
    .local v0, "that":Lorg/solovyev/android/checkout/Sku;
    iget-object v1, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    iget-object v2, v0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/Sku$Id;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getDisplayTitle()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 112
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->mDisplayTitle:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->title:Ljava/lang/String;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Sku;->makeDisplayTitle(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/Sku;->mDisplayTitle:Ljava/lang/String;

    .line 115
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->mDisplayTitle:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Sku$Id;->hashCode()I

    move-result v0

    return v0
.end method

.method public isInApp()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Sku$Id;->isInApp()Z

    move-result v0

    return v0
.end method

.method public isSubscription()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Sku$Id;->isSubscription()Z

    move-result v0

    return v0
.end method

.method toJson()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 86
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Sku;->toJsonObject()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/solovyev/android/checkout/Sku;->id:Lorg/solovyev/android/checkout/Sku$Id;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Sku;->getDisplayTitle()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/solovyev/android/checkout/Sku;->price:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
