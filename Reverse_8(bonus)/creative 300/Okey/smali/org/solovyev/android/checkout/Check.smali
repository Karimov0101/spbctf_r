.class final Lorg/solovyev/android/checkout/Check;
.super Ljava/lang/Object;
.source "Check.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/Check$AssertionException;
    }
.end annotation


# static fields
.field private static final sJunit:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isJunit()Z

    move-result v0

    sput-boolean v0, Lorg/solovyev/android/checkout/Check;->sJunit:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method static equals(II)V
    .locals 3
    .param p0, "expected"    # I
    .param p1, "actual"    # I

    .prologue
    .line 73
    if-eq p0, p1, :cond_0

    .line 74
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "Should be equal"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 76
    :cond_0
    return-void
.end method

.method static equals(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p0, "expected"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "actual"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 79
    const-string v0, "Should be equal"

    invoke-static {p0, p1, v0}, Lorg/solovyev/android/checkout/Check;->equals(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method static equals(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "expected"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "actual"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 83
    if-ne p0, p1, :cond_1

    .line 90
    :cond_0
    return-void

    .line 88
    :cond_1
    if-eqz p0, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 93
    :cond_2
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0
.end method

.method static isFalse(ZLjava/lang/String;)V
    .locals 2
    .param p0, "expression"    # Z
    .param p1, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 103
    if-eqz p0, :cond_0

    .line 104
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 106
    :cond_0
    return-void
.end method

.method private static isJunit()Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    .line 42
    .local v1, "stackTrace":[Ljava/lang/StackTraceElement;
    array-length v4, v1

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v0, v1, v3

    .line 43
    .local v0, "element":Ljava/lang/StackTraceElement;
    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v5

    const-string v6, "org.junit."

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 44
    const/4 v2, 0x1

    .line 47
    .end local v0    # "element":Ljava/lang/StackTraceElement;
    :cond_0
    return v2

    .line 42
    .restart local v0    # "element":Ljava/lang/StackTraceElement;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method static isMainThread()V
    .locals 3

    .prologue
    .line 51
    sget-boolean v0, Lorg/solovyev/android/checkout/Check;->sJunit:Z

    if-nez v0, :cond_0

    invoke-static {}, Lorg/solovyev/android/checkout/MainThread;->isMainThread()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "Should be called on the main thread"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 54
    :cond_0
    return-void
.end method

.method static isNotEmpty(Ljava/lang/String;)V
    .locals 3
    .param p0, "s"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 119
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 120
    :cond_0
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "String should not be empty"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 122
    :cond_1
    return-void
.end method

.method static isNotEmpty(Ljava/util/Collection;)V
    .locals 3
    .param p0    # Ljava/util/Collection;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "c":Ljava/util/Collection;, "Ljava/util/Collection<*>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 126
    :cond_0
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "Collection should not be empty"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 128
    :cond_1
    return-void
.end method

.method static isNotNull(Ljava/lang/Object;)V
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 57
    const-string v0, "Object should not be null"

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Check;->isNotNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    return-void
.end method

.method static isNotNull(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "o"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 61
    if-nez p0, :cond_0

    .line 62
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 64
    :cond_0
    return-void
.end method

.method static isNull(Ljava/lang/Object;)V
    .locals 1
    .param p0, "o"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 109
    const-string v0, "Object should be null"

    invoke-static {p0, v0}, Lorg/solovyev/android/checkout/Check;->isNull(Ljava/lang/Object;Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method static isNull(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2
    .param p0, "o"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 113
    if-eqz p0, :cond_0

    .line 114
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 116
    :cond_0
    return-void
.end method

.method static isTrue(ZLjava/lang/String;)V
    .locals 2
    .param p0, "expression"    # Z
    .param p1, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 97
    if-nez p0, :cond_0

    .line 98
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 100
    :cond_0
    return-void
.end method

.method static notEquals(II)V
    .locals 3
    .param p0, "expected"    # I
    .param p1, "actual"    # I

    .prologue
    .line 67
    if-ne p0, p1, :cond_0

    .line 68
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "Should not be equal"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 70
    :cond_0
    return-void
.end method

.method static same(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p0, "expected"    # Ljava/lang/Object;
    .param p1, "actual"    # Ljava/lang/Object;

    .prologue
    .line 131
    if-eq p0, p1, :cond_0

    .line 132
    new-instance v0, Lorg/solovyev/android/checkout/Check$AssertionException;

    const-string v1, "Objects should be the same"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/Check$AssertionException;-><init>(Ljava/lang/String;Lorg/solovyev/android/checkout/Check$1;)V

    throw v0

    .line 134
    :cond_0
    return-void
.end method
