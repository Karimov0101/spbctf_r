.class final Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;
.super Ljava/lang/Object;
.source "Checkout.java"

# interfaces
.implements Ljava/util/concurrent/Executor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Checkout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "OnLoadExecutor"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/solovyev/android/checkout/Checkout;


# direct methods
.method private constructor <init>(Lorg/solovyev/android/checkout/Checkout;)V
    .locals 0

    .prologue
    .line 431
    iput-object p1, p0, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;->this$0:Lorg/solovyev/android/checkout/Checkout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lorg/solovyev/android/checkout/Checkout;Lorg/solovyev/android/checkout/Checkout$1;)V
    .locals 0
    .param p1, "x0"    # Lorg/solovyev/android/checkout/Checkout;
    .param p2, "x1"    # Lorg/solovyev/android/checkout/Checkout$1;

    .prologue
    .line 431
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;-><init>(Lorg/solovyev/android/checkout/Checkout;)V

    return-void
.end method


# virtual methods
.method public execute(Ljava/lang/Runnable;)V
    .locals 3
    .param p1, "command"    # Ljava/lang/Runnable;

    .prologue
    .line 435
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;->this$0:Lorg/solovyev/android/checkout/Checkout;

    iget-object v2, v1, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 436
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;->this$0:Lorg/solovyev/android/checkout/Checkout;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Checkout;->access$300(Lorg/solovyev/android/checkout/Checkout;)Lorg/solovyev/android/checkout/Billing$Requests;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/solovyev/android/checkout/Checkout$OnLoadExecutor;->this$0:Lorg/solovyev/android/checkout/Checkout;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Checkout;->access$300(Lorg/solovyev/android/checkout/Checkout;)Lorg/solovyev/android/checkout/Billing$Requests;

    move-result-object v1

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/Billing$Requests;->getDeliveryExecutor()Ljava/util/concurrent/Executor;

    move-result-object v0

    .line 437
    .local v0, "executor":Ljava/util/concurrent/Executor;
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    if-eqz v0, :cond_1

    .line 440
    invoke-interface {v0, p1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 444
    :goto_1
    return-void

    .line 436
    .end local v0    # "executor":Ljava/util/concurrent/Executor;
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 437
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 442
    .restart local v0    # "executor":Ljava/util/concurrent/Executor;
    :cond_1
    const-string v1, "Trying to deliver result on a stopped checkout."

    invoke-static {v1}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;)V

    goto :goto_1
.end method
