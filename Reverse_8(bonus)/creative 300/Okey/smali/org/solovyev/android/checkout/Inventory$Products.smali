.class public final Lorg/solovyev/android/checkout/Inventory$Products;
.super Ljava/lang/Object;
.source "Inventory.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Inventory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Products"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<",
        "Lorg/solovyev/android/checkout/Inventory$Product;",
        ">;"
    }
.end annotation

.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field static final sEmpty:Lorg/solovyev/android/checkout/Inventory$Products;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# instance fields
.field private final mMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lorg/solovyev/android/checkout/Inventory$Product;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 98
    new-instance v0, Lorg/solovyev/android/checkout/Inventory$Products;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/Inventory$Products;-><init>()V

    sput-object v0, Lorg/solovyev/android/checkout/Inventory$Products;->sEmpty:Lorg/solovyev/android/checkout/Inventory$Products;

    return-void
.end method

.method constructor <init>()V
    .locals 5

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    .line 104
    sget-object v1, Lorg/solovyev/android/checkout/ProductTypes;->ALL:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 105
    .local v0, "product":Ljava/lang/String;
    iget-object v2, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    new-instance v3, Lorg/solovyev/android/checkout/Inventory$Product;

    const/4 v4, 0x0

    invoke-direct {v3, v0, v4}, Lorg/solovyev/android/checkout/Inventory$Product;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 107
    .end local v0    # "product":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static empty()Lorg/solovyev/android/checkout/Inventory$Products;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 111
    sget-object v0, Lorg/solovyev/android/checkout/Inventory$Products;->sEmpty:Lorg/solovyev/android/checkout/Inventory$Products;

    return-object v0
.end method


# virtual methods
.method add(Lorg/solovyev/android/checkout/Inventory$Product;)V
    .locals 2
    .param p1, "product"    # Lorg/solovyev/android/checkout/Inventory$Product;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 115
    iget-object v0, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    iget-object v1, p1, Lorg/solovyev/android/checkout/Inventory$Product;->id:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    return-void
.end method

.method public get(Ljava/lang/String;)Lorg/solovyev/android/checkout/Inventory$Product;
    .locals 1
    .param p1, "productId"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 124
    invoke-static {p1}, Lorg/solovyev/android/checkout/ProductTypes;->checkSupported(Ljava/lang/String;)V

    .line 125
    iget-object v0, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Inventory$Product;

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Lorg/solovyev/android/checkout/Inventory$Product;",
            ">;"
        }
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method merge(Lorg/solovyev/android/checkout/Inventory$Products;)V
    .locals 5
    .param p1, "products"    # Lorg/solovyev/android/checkout/Inventory$Products;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 144
    iget-object v2, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 145
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/solovyev/android/checkout/Inventory$Product;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/solovyev/android/checkout/Inventory$Product;

    iget-boolean v2, v2, Lorg/solovyev/android/checkout/Inventory$Product;->supported:Z

    if-nez v2, :cond_0

    .line 146
    iget-object v2, p1, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/solovyev/android/checkout/Inventory$Product;

    .line 147
    .local v1, "product":Lorg/solovyev/android/checkout/Inventory$Product;
    if-eqz v1, :cond_0

    .line 148
    invoke-interface {v0, v1}, Ljava/util/Map$Entry;->setValue(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 152
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lorg/solovyev/android/checkout/Inventory$Product;>;"
    .end local v1    # "product":Lorg/solovyev/android/checkout/Inventory$Product;
    :cond_1
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lorg/solovyev/android/checkout/Inventory$Products;->mMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method
