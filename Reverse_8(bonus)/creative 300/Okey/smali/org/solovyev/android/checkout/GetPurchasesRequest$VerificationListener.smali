.class Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;
.super Ljava/lang/Object;
.source "GetPurchasesRequest.java"

# interfaces
.implements Lorg/solovyev/android/checkout/RequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/GetPurchasesRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VerificationListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/RequestListener",
        "<",
        "Ljava/util/List",
        "<",
        "Lorg/solovyev/android/checkout/Purchase;",
        ">;>;"
    }
.end annotation


# instance fields
.field private mCalled:Z

.field private final mContinuationToken:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final mOriginalThread:Ljava/lang/Thread;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mProduct:Ljava/lang/String;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mRequest:Lorg/solovyev/android/checkout/Request;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/Request",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/Request;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "product"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "continuationToken"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/Request",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 118
    .local p1, "request":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<Lorg/solovyev/android/checkout/Purchases;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    .line 120
    iput-object p2, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mProduct:Ljava/lang/String;

    .line 121
    iput-object p3, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mContinuationToken:Ljava/lang/String;

    .line 122
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mOriginalThread:Ljava/lang/Thread;

    .line 123
    return-void
.end method

.method static synthetic access$000(Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;)Z
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;

    .prologue
    .line 107
    iget-boolean v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mCalled:Z

    return v0
.end method


# virtual methods
.method public onError(ILjava/lang/Exception;)V
    .locals 3
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 134
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mOriginalThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Must be called on the same thread"

    invoke-static {v0, v1, v2}, Lorg/solovyev/android/checkout/Check;->equals(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mCalled:Z

    .line 136
    const/16 v0, 0x2711

    if-ne p1, v0, :cond_0

    .line 137
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0, p2}, Lorg/solovyev/android/checkout/Request;->onError(Ljava/lang/Exception;)V

    .line 141
    :goto_0
    return-void

    .line 139
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/Request;->onError(I)V

    goto :goto_0
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 107
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->onSuccess(Ljava/util/List;)V

    return-void
.end method

.method public onSuccess(Ljava/util/List;)V
    .locals 4
    .param p1    # Ljava/util/List;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 127
    .local p1, "verifiedPurchases":Ljava/util/List;, "Ljava/util/List<Lorg/solovyev/android/checkout/Purchase;>;"
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mOriginalThread:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    const-string v2, "Must be called on the same thread"

    invoke-static {v0, v1, v2}, Lorg/solovyev/android/checkout/Check;->equals(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/String;)V

    .line 128
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mCalled:Z

    .line 129
    iget-object v0, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mRequest:Lorg/solovyev/android/checkout/Request;

    new-instance v1, Lorg/solovyev/android/checkout/Purchases;

    iget-object v2, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mProduct:Ljava/lang/String;

    iget-object v3, p0, Lorg/solovyev/android/checkout/GetPurchasesRequest$VerificationListener;->mContinuationToken:Ljava/lang/String;

    invoke-direct {v1, v2, p1, v3}, Lorg/solovyev/android/checkout/Purchases;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lorg/solovyev/android/checkout/Request;->onSuccess(Ljava/lang/Object;)V

    .line 130
    return-void
.end method
