.class final Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;
.super Ljava/lang/Object;
.source "Billing.java"

# interfaces
.implements Lorg/solovyev/android/checkout/CancellableRequestListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/solovyev/android/checkout/Billing$Requests;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "GetAllPurchasesListener"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/solovyev/android/checkout/CancellableRequestListener",
        "<",
        "Lorg/solovyev/android/checkout/Purchases;",
        ">;"
    }
.end annotation


# instance fields
.field private final mListener:Lorg/solovyev/android/checkout/RequestListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPurchases:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mRequest:Lorg/solovyev/android/checkout/GetPurchasesRequest;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field final synthetic this$1:Lorg/solovyev/android/checkout/Billing$Requests;


# direct methods
.method public constructor <init>(Lorg/solovyev/android/checkout/Billing$Requests;Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/Billing$Requests;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchases;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1142
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchases;>;"
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->this$1:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1137
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mPurchases:Ljava/util/List;

    .line 1143
    iput-object p2, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    .line 1144
    return-void
.end method

.method static synthetic access$1602(Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;Lorg/solovyev/android/checkout/GetPurchasesRequest;)Lorg/solovyev/android/checkout/GetPurchasesRequest;
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;
    .param p1, "x1"    # Lorg/solovyev/android/checkout/GetPurchasesRequest;

    .prologue
    .line 1134
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mRequest:Lorg/solovyev/android/checkout/GetPurchasesRequest;

    return-object p1
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 1165
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->cancel(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 1166
    return-void
.end method

.method public onError(ILjava/lang/Exception;)V
    .locals 1
    .param p1, "response"    # I
    .param p2, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1160
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    invoke-interface {v0, p1, p2}, Lorg/solovyev/android/checkout/RequestListener;->onError(ILjava/lang/Exception;)V

    .line 1161
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1134
    check-cast p1, Lorg/solovyev/android/checkout/Purchases;

    invoke-virtual {p0, p1}, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->onSuccess(Lorg/solovyev/android/checkout/Purchases;)V

    return-void
.end method

.method public onSuccess(Lorg/solovyev/android/checkout/Purchases;)V
    .locals 5
    .param p1, "purchases"    # Lorg/solovyev/android/checkout/Purchases;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 1148
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mPurchases:Ljava/util/List;

    iget-object v1, p1, Lorg/solovyev/android/checkout/Purchases;->list:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 1150
    iget-object v0, p1, Lorg/solovyev/android/checkout/Purchases;->continuationToken:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1151
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mListener:Lorg/solovyev/android/checkout/RequestListener;

    new-instance v1, Lorg/solovyev/android/checkout/Purchases;

    iget-object v2, p1, Lorg/solovyev/android/checkout/Purchases;->product:Ljava/lang/String;

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mPurchases:Ljava/util/List;

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lorg/solovyev/android/checkout/Purchases;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lorg/solovyev/android/checkout/RequestListener;->onSuccess(Ljava/lang/Object;)V

    .line 1156
    :goto_0
    return-void

    .line 1154
    :cond_0
    new-instance v0, Lorg/solovyev/android/checkout/GetPurchasesRequest;

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mRequest:Lorg/solovyev/android/checkout/GetPurchasesRequest;

    iget-object v2, p1, Lorg/solovyev/android/checkout/Purchases;->continuationToken:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lorg/solovyev/android/checkout/GetPurchasesRequest;-><init>(Lorg/solovyev/android/checkout/GetPurchasesRequest;Ljava/lang/String;)V

    iput-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mRequest:Lorg/solovyev/android/checkout/GetPurchasesRequest;

    .line 1155
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->this$1:Lorg/solovyev/android/checkout/Billing$Requests;

    iget-object v0, v0, Lorg/solovyev/android/checkout/Billing$Requests;->this$0:Lorg/solovyev/android/checkout/Billing;

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->mRequest:Lorg/solovyev/android/checkout/GetPurchasesRequest;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing$Requests$GetAllPurchasesListener;->this$1:Lorg/solovyev/android/checkout/Billing$Requests;

    invoke-static {v2}, Lorg/solovyev/android/checkout/Billing$Requests;->access$1800(Lorg/solovyev/android/checkout/Billing$Requests;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lorg/solovyev/android/checkout/Billing;->access$1900(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;Ljava/lang/Object;)I

    goto :goto_0
.end method
