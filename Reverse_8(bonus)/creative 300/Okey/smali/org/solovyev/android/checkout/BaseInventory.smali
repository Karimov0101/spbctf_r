.class public abstract Lorg/solovyev/android/checkout/BaseInventory;
.super Ljava/lang/Object;
.source "BaseInventory.java"

# interfaces
.implements Lorg/solovyev/android/checkout/Inventory;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/BaseInventory$SynchronizedRequestListener;,
        Lorg/solovyev/android/checkout/BaseInventory$Task;
    }
.end annotation


# instance fields
.field protected final mCheckout:Lorg/solovyev/android/checkout/Checkout;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field protected final mLock:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mTaskIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final mTasks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/BaseInventory$Task;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lorg/solovyev/android/checkout/Checkout;)V
    .locals 1
    .param p1, "checkout"    # Lorg/solovyev/android/checkout/Checkout;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTaskIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 49
    iput-object p1, p0, Lorg/solovyev/android/checkout/BaseInventory;->mCheckout:Lorg/solovyev/android/checkout/Checkout;

    .line 50
    iget-object v0, p1, Lorg/solovyev/android/checkout/Checkout;->mLock:Ljava/lang/Object;

    iput-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    .line 51
    return-void
.end method

.method static synthetic access$200(Lorg/solovyev/android/checkout/BaseInventory;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/BaseInventory;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTaskIdGenerator:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic access$300(Lorg/solovyev/android/checkout/BaseInventory;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/BaseInventory;

    .prologue
    .line 37
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    return-object v0
.end method

.method private getTasksCopy()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/BaseInventory$Task;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 74
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 75
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public cancel()V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Lorg/solovyev/android/checkout/BaseInventory;->getTasksCopy()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/BaseInventory$Task;

    .line 56
    .local v0, "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->access$000(Lorg/solovyev/android/checkout/BaseInventory$Task;)V

    goto :goto_0

    .line 58
    .end local v0    # "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    :cond_0
    return-void
.end method

.method public cancel(I)V
    .locals 4
    .param p1, "id"    # I

    .prologue
    .line 62
    iget-object v2, p0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 63
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/BaseInventory$Task;

    .line 64
    .local v0, "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->access$100(Lorg/solovyev/android/checkout/BaseInventory$Task;)I

    move-result v3

    if-ne v3, p1, :cond_0

    .line 65
    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->access$000(Lorg/solovyev/android/checkout/BaseInventory$Task;)V

    .line 69
    .end local v0    # "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    :cond_1
    monitor-exit v2

    .line 70
    return-void

    .line 69
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected abstract createWorker(Lorg/solovyev/android/checkout/BaseInventory$Task;)Ljava/lang/Runnable;
    .param p1    # Lorg/solovyev/android/checkout/BaseInventory$Task;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public isLoading()Z
    .locals 2

    .prologue
    .line 81
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 82
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 83
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public load(Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)I
    .locals 3
    .param p1, "request"    # Lorg/solovyev/android/checkout/Inventory$Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "callback"    # Lorg/solovyev/android/checkout/Inventory$Callback;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 89
    iget-object v2, p0, Lorg/solovyev/android/checkout/BaseInventory;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 90
    :try_start_0
    new-instance v0, Lorg/solovyev/android/checkout/BaseInventory$Task;

    invoke-direct {v0, p0, p1, p2}, Lorg/solovyev/android/checkout/BaseInventory$Task;-><init>(Lorg/solovyev/android/checkout/BaseInventory;Lorg/solovyev/android/checkout/Inventory$Request;Lorg/solovyev/android/checkout/Inventory$Callback;)V

    .line 91
    .local v0, "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    iget-object v1, p0, Lorg/solovyev/android/checkout/BaseInventory;->mTasks:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    invoke-virtual {v0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->run()V

    .line 93
    invoke-static {v0}, Lorg/solovyev/android/checkout/BaseInventory$Task;->access$100(Lorg/solovyev/android/checkout/BaseInventory$Task;)I

    move-result v1

    monitor-exit v2

    return v1

    .line 94
    .end local v0    # "task":Lorg/solovyev/android/checkout/BaseInventory$Task;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method protected final synchronizedListener(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 98
    .local p1, "l":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    new-instance v0, Lorg/solovyev/android/checkout/BaseInventory$SynchronizedRequestListener;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/BaseInventory$SynchronizedRequestListener;-><init>(Lorg/solovyev/android/checkout/BaseInventory;Lorg/solovyev/android/checkout/RequestListener;)V

    return-object v0
.end method
