.class public final Lorg/solovyev/android/checkout/Billing;
.super Ljava/lang/Object;
.source "Billing.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;,
        Lorg/solovyev/android/checkout/Billing$CachingRequestListener;,
        Lorg/solovyev/android/checkout/Billing$Requests;,
        Lorg/solovyev/android/checkout/Billing$RequestsBuilder;,
        Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;,
        Lorg/solovyev/android/checkout/Billing$StaticConfiguration;,
        Lorg/solovyev/android/checkout/Billing$DefaultConfiguration;,
        Lorg/solovyev/android/checkout/Billing$Configuration;,
        Lorg/solovyev/android/checkout/Billing$ServiceConnector;,
        Lorg/solovyev/android/checkout/Billing$State;
    }
.end annotation


# static fields
.field static final DAY:J = 0x5265c00L

.field static final HOUR:J = 0x36ee80L

.field static final MINUTE:J = 0xea60L

.field static final SECOND:J = 0x3e8L

.field private static final TAG:Ljava/lang/String; = "Checkout"
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field static final V3:I = 0x3

.field static final V5:I = 0x5

.field private static final sEmptyListener:Lorg/solovyev/android/checkout/EmptyRequestListener;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private static sLogger:Lorg/solovyev/android/checkout/Logger;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private static final sPreviousStates:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lorg/solovyev/android/checkout/Billing$State;",
            "Ljava/util/List",
            "<",
            "Lorg/solovyev/android/checkout/Billing$State;",
            ">;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field


# instance fields
.field private mBackground:Ljava/util/concurrent/Executor;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mCache:Lorg/solovyev/android/checkout/ConcurrentCache;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mCheckoutCount:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mLock:Ljava/lang/Object;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private final mPlayStoreListener:Lorg/solovyev/android/checkout/PlayStoreListener;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private final mRequests:Lorg/solovyev/android/checkout/BillingRequests;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation
.end field

.field private mService:Lcom/android/vending/billing/IInAppBillingService;
    .annotation runtime Ljavax/annotation/Nullable;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field

.field private mState:Lorg/solovyev/android/checkout/Billing$State;
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "mLock"
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 80
    new-instance v0, Lorg/solovyev/android/checkout/EmptyRequestListener;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/EmptyRequestListener;-><init>()V

    sput-object v0, Lorg/solovyev/android/checkout/Billing;->sEmptyListener:Lorg/solovyev/android/checkout/EmptyRequestListener;

    .line 83
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lorg/solovyev/android/checkout/Billing$State;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    .line 85
    invoke-static {}, Lorg/solovyev/android/checkout/Billing;->newLogger()Lorg/solovyev/android/checkout/Logger;

    move-result-object v0

    sput-object v0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    .line 88
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->INITIAL:Lorg/solovyev/android/checkout/Billing$State;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    const/4 v2, 0x4

    new-array v2, v2, [Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->INITIAL:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v3, v2, v5

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v3, v2, v6

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v3, v2, v7

    const/4 v3, 0x3

    sget-object v4, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    new-array v2, v7, [Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v3, v2, v5

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    aput-object v3, v2, v6

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lorg/solovyev/android/checkout/Billing$Configuration;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "handler"    # Landroid/os/Handler;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p3, "configuration"    # Lorg/solovyev/android/checkout/Billing$Configuration;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    .line 104
    new-instance v2, Lorg/solovyev/android/checkout/PendingRequests;

    invoke-direct {v2}, Lorg/solovyev/android/checkout/PendingRequests;-><init>()V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    .line 107
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing;->newRequestsBuilder()Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->withTag(Ljava/lang/Object;)Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->onBackgroundThread()Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    move-result-object v2

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->create()Lorg/solovyev/android/checkout/BillingRequests;

    move-result-object v2

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mRequests:Lorg/solovyev/android/checkout/BillingRequests;

    .line 111
    new-instance v2, Lorg/solovyev/android/checkout/Billing$1;

    invoke-direct {v2, p0}, Lorg/solovyev/android/checkout/Billing$1;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreListener:Lorg/solovyev/android/checkout/PlayStoreListener;

    .line 121
    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->INITIAL:Lorg/solovyev/android/checkout/Billing$State;

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    .line 126
    new-instance v2, Lorg/solovyev/android/checkout/Billing$2;

    invoke-direct {v2, p0}, Lorg/solovyev/android/checkout/Billing$2;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    .line 127
    invoke-static {v2}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mBackground:Ljava/util/concurrent/Executor;

    .line 133
    new-instance v2, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;

    invoke-direct {v2, p0, v1}, Lorg/solovyev/android/checkout/Billing$DefaultServiceConnector;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Billing$1;)V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    .line 149
    instance-of v2, p1, Landroid/app/Application;

    if-eqz v2, :cond_0

    .line 152
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mContext:Landroid/content/Context;

    .line 156
    :goto_0
    new-instance v2, Lorg/solovyev/android/checkout/MainThread;

    invoke-direct {v2, p2}, Lorg/solovyev/android/checkout/MainThread;-><init>(Landroid/os/Handler;)V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    .line 157
    new-instance v2, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-direct {v2, p3, v1}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;-><init>(Lorg/solovyev/android/checkout/Billing$Configuration;Lorg/solovyev/android/checkout/Billing$1;)V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    .line 158
    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->getPublicKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/solovyev/android/checkout/Check;->isNotEmpty(Ljava/lang/String;)V

    .line 159
    invoke-interface {p3}, Lorg/solovyev/android/checkout/Billing$Configuration;->getCache()Lorg/solovyev/android/checkout/Cache;

    move-result-object v0

    .line 160
    .local v0, "cache":Lorg/solovyev/android/checkout/Cache;
    new-instance v2, Lorg/solovyev/android/checkout/ConcurrentCache;

    if-nez v0, :cond_1

    :goto_1
    invoke-direct {v2, v1}, Lorg/solovyev/android/checkout/ConcurrentCache;-><init>(Lorg/solovyev/android/checkout/Cache;)V

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mCache:Lorg/solovyev/android/checkout/ConcurrentCache;

    .line 161
    new-instance v1, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    invoke-direct {v1, v2, v3}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    iput-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    .line 162
    return-void

    .line 154
    .end local v0    # "cache":Lorg/solovyev/android/checkout/Cache;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mContext:Landroid/content/Context;

    goto :goto_0

    .line 160
    .restart local v0    # "cache":Lorg/solovyev/android/checkout/Cache;
    :cond_1
    new-instance v1, Lorg/solovyev/android/checkout/SafeCache;

    invoke-direct {v1, v0}, Lorg/solovyev/android/checkout/SafeCache;-><init>(Lorg/solovyev/android/checkout/Cache;)V

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;Lorg/solovyev/android/checkout/Billing$Configuration;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "configuration"    # Lorg/solovyev/android/checkout/Billing$Configuration;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lorg/solovyev/android/checkout/Billing;-><init>(Landroid/content/Context;Landroid/os/Handler;Lorg/solovyev/android/checkout/Billing$Configuration;)V

    .line 140
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 141
    return-void
.end method

.method static synthetic access$000(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/ConcurrentCache;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mCache:Lorg/solovyev/android/checkout/ConcurrentCache;

    return-object v0
.end method

.method static synthetic access$1000(Lorg/solovyev/android/checkout/Billing;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200()Lorg/solovyev/android/checkout/RequestListener;
    .locals 1

    .prologue
    .line 67
    invoke-static {}, Lorg/solovyev/android/checkout/Billing;->emptyListener()Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;
    .param p1, "x1"    # Lorg/solovyev/android/checkout/RequestListener;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/Billing;->onMainThread(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/CancellableExecutor;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    return-object v0
.end method

.method static synthetic access$1500(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$StaticConfiguration;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    return-object v0
.end method

.method static synthetic access$1900(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;Ljava/lang/Object;)I
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;
    .param p1, "x1"    # Lorg/solovyev/android/checkout/Request;
    .param p2, "x2"    # Ljava/lang/Object;

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/PendingRequests;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    return-object v0
.end method

.method static synthetic access$400(Lorg/solovyev/android/checkout/Billing;)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Billing;->connectOnMainThread()V

    return-void
.end method

.method static synthetic access$500(Lorg/solovyev/android/checkout/Billing;)V
    .locals 0
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Billing;->disconnectOnMainThread()V

    return-void
.end method

.method static synthetic access$700(Lorg/solovyev/android/checkout/Billing;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Lorg/solovyev/android/checkout/Billing;)Lorg/solovyev/android/checkout/Billing$State;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    return-object v0
.end method

.method static synthetic access$900(Lorg/solovyev/android/checkout/Billing;)Lcom/android/vending/billing/IInAppBillingService;
    .locals 1
    .param p0, "x0"    # Lorg/solovyev/android/checkout/Billing;

    .prologue
    .line 67
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mService:Lcom/android/vending/billing/IInAppBillingService;

    return-object v0
.end method

.method static cancel(Lorg/solovyev/android/checkout/RequestListener;)V
    .locals 1
    .param p0    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p0, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<*>;"
    instance-of v0, p0, Lorg/solovyev/android/checkout/CancellableRequestListener;

    if-eqz v0, :cond_0

    .line 263
    check-cast p0, Lorg/solovyev/android/checkout/CancellableRequestListener;

    .end local p0    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<*>;"
    invoke-interface {p0}, Lorg/solovyev/android/checkout/CancellableRequestListener;->cancel()V

    .line 265
    :cond_0
    return-void
.end method

.method private connectOnMainThread()V
    .locals 2

    .prologue
    .line 413
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 414
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    invoke-interface {v1}, Lorg/solovyev/android/checkout/Billing$ServiceConnector;->connect()Z

    move-result v0

    .line 415
    .local v0, "connecting":Z
    if-nez v0, :cond_0

    .line 416
    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {p0, v1}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V

    .line 418
    :cond_0
    return-void
.end method

.method static debug(Ljava/lang/String;)V
    .locals 2
    .param p0, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 213
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v1, "Checkout"

    invoke-interface {v0, v1, p0}, Lorg/solovyev/android/checkout/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    return-void
.end method

.method static debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "subTag"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 209
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Checkout/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lorg/solovyev/android/checkout/Logger;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    return-void
.end method

.method private disconnectOnMainThread()V
    .locals 1

    .prologue
    .line 483
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 484
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    invoke-interface {v0}, Lorg/solovyev/android/checkout/Billing$ServiceConnector;->disconnect()V

    .line 485
    return-void
.end method

.method private static emptyListener()Lorg/solovyev/android/checkout/RequestListener;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">()",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 180
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sEmptyListener:Lorg/solovyev/android/checkout/EmptyRequestListener;

    return-object v0
.end method

.method static error(Ljava/lang/Exception;)V
    .locals 1
    .param p0, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 188
    invoke-virtual {p0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/String;Ljava/lang/Exception;)V

    .line 189
    return-void
.end method

.method static error(Ljava/lang/String;)V
    .locals 2
    .param p0, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 184
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v1, "Checkout"

    invoke-interface {v0, v1, p0}, Lorg/solovyev/android/checkout/Logger;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method static error(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3
    .param p0, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p1, "e"    # Ljava/lang/Exception;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 192
    instance-of v1, p1, Lorg/solovyev/android/checkout/BillingException;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 193
    check-cast v0, Lorg/solovyev/android/checkout/BillingException;

    .line 194
    .local v0, "be":Lorg/solovyev/android/checkout/BillingException;
    invoke-virtual {v0}, Lorg/solovyev/android/checkout/BillingException;->getResponse()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 201
    sget-object v1, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v2, "Checkout"

    invoke-interface {v1, v2, p0, p1}, Lorg/solovyev/android/checkout/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 206
    .end local v0    # "be":Lorg/solovyev/android/checkout/BillingException;
    :goto_0
    return-void

    .line 198
    .restart local v0    # "be":Lorg/solovyev/android/checkout/BillingException;
    :pswitch_0
    sget-object v1, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v2, "Checkout"

    invoke-interface {v1, v2, p0, p1}, Lorg/solovyev/android/checkout/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 204
    .end local v0    # "be":Lorg/solovyev/android/checkout/BillingException;
    :cond_0
    sget-object v1, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v2, "Checkout"

    invoke-interface {v1, v2, p0, p1}, Lorg/solovyev/android/checkout/Logger;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 194
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private executePendingRequests()V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mBackground:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 339
    return-void
.end method

.method public static newCache()Lorg/solovyev/android/checkout/Cache;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 229
    new-instance v0, Lorg/solovyev/android/checkout/MapCache;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/MapCache;-><init>()V

    return-object v0
.end method

.method public static newLogger()Lorg/solovyev/android/checkout/Logger;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 245
    new-instance v0, Lorg/solovyev/android/checkout/DefaultLogger;

    invoke-direct {v0}, Lorg/solovyev/android/checkout/DefaultLogger;-><init>()V

    return-object v0
.end method

.method public static newMainThreadLogger(Lorg/solovyev/android/checkout/Logger;)Lorg/solovyev/android/checkout/Logger;
    .locals 1
    .param p0, "logger"    # Lorg/solovyev/android/checkout/Logger;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 253
    new-instance v0, Lorg/solovyev/android/checkout/MainThreadLogger;

    invoke-direct {v0, p0}, Lorg/solovyev/android/checkout/MainThreadLogger;-><init>(Lorg/solovyev/android/checkout/Logger;)V

    return-object v0
.end method

.method public static newPurchaseVerifier(Ljava/lang/String;)Lorg/solovyev/android/checkout/PurchaseVerifier;
    .locals 1
    .param p0, "publicKey"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 237
    new-instance v0, Lorg/solovyev/android/checkout/DefaultPurchaseVerifier;

    invoke-direct {v0, p0}, Lorg/solovyev/android/checkout/DefaultPurchaseVerifier;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private onConnectedService(Lorg/solovyev/android/checkout/Request;)Lorg/solovyev/android/checkout/RequestRunnable;
    .locals 1
    .param p1, "request"    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 526
    new-instance v0, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;

    invoke-direct {v0, p0, p1}, Lorg/solovyev/android/checkout/Billing$OnConnectedServiceRunnable;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;)V

    return-object v0
.end method

.method private onMainThread(Lorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/RequestListener;
    .locals 2
    .param p1    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;)",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 580
    .local p1, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    new-instance v0, Lorg/solovyev/android/checkout/MainThreadRequestListener;

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    invoke-direct {v0, v1, p1}, Lorg/solovyev/android/checkout/MainThreadRequestListener;-><init>(Lorg/solovyev/android/checkout/CancellableExecutor;Lorg/solovyev/android/checkout/RequestListener;)V

    return-object v0
.end method

.method private runWhenConnected(Lorg/solovyev/android/checkout/Request;Ljava/lang/Object;)I
    .locals 1
    .param p1, "request"    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 488
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/solovyev/android/checkout/Billing;->runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static setLogger(Lorg/solovyev/android/checkout/Logger;)V
    .locals 0
    .param p0, "logger"    # Lorg/solovyev/android/checkout/Logger;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 221
    if-nez p0, :cond_0

    new-instance p0, Lorg/solovyev/android/checkout/EmptyLogger;

    .end local p0    # "logger":Lorg/solovyev/android/checkout/Logger;
    invoke-direct {p0}, Lorg/solovyev/android/checkout/EmptyLogger;-><init>()V

    :cond_0
    sput-object p0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    .line 222
    return-void
.end method

.method static waitGooglePlay()V
    .locals 4

    .prologue
    .line 171
    const-wide/16 v2, 0x64

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    .local v0, "e":Ljava/lang/InterruptedException;
    :goto_0
    return-void

    .line 172
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_0
    move-exception v0

    .line 173
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->error(Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method static warning(Ljava/lang/String;)V
    .locals 2
    .param p0, "message"    # Ljava/lang/String;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 217
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sLogger:Lorg/solovyev/android/checkout/Logger;

    const-string v1, "Checkout"

    invoke-interface {v0, v1, p0}, Lorg/solovyev/android/checkout/Logger;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    return-void
.end method


# virtual methods
.method public addPlayStoreListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V
    .locals 2
    .param p1, "listener"    # Lorg/solovyev/android/checkout/PlayStoreListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 428
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 429
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;->addListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V

    .line 430
    monitor-exit v1

    .line 431
    return-void

    .line 430
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public cancel(I)V
    .locals 1
    .param p1, "requestId"    # I

    .prologue
    .line 514
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/PendingRequests;->cancel(I)V

    .line 515
    return-void
.end method

.method public cancelAll()V
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/PendingRequests;->cancelAll()V

    .line 522
    return-void
.end method

.method public connect()V
    .locals 3

    .prologue
    .line 391
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 392
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, v2, :cond_0

    .line 393
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Billing;->executePendingRequests()V

    .line 394
    monitor-exit v1

    .line 410
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, v2, :cond_1

    .line 397
    monitor-exit v1

    goto :goto_0

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 399
    :cond_1
    :try_start_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    if-gtz v0, :cond_2

    .line 400
    const-string v0, "Auto connection feature is turned on. There is no need in calling Billing.connect() manually. See Billing.Configuration.isAutoConnect"

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    .line 402
    :cond_2
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V

    .line 403
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    new-instance v2, Lorg/solovyev/android/checkout/Billing$4;

    invoke-direct {v2, p0}, Lorg/solovyev/android/checkout/Billing$4;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    invoke-interface {v0, v2}, Lorg/solovyev/android/checkout/CancellableExecutor;->execute(Ljava/lang/Runnable;)V

    .line 409
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method createPurchaseFlow(Lorg/solovyev/android/checkout/IntentStarter;ILorg/solovyev/android/checkout/RequestListener;)Lorg/solovyev/android/checkout/PurchaseFlow;
    .locals 3
    .param p1, "intentStarter"    # Lorg/solovyev/android/checkout/IntentStarter;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2, "requestCode"    # I
    .param p3    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/solovyev/android/checkout/IntentStarter;",
            "I",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<",
            "Lorg/solovyev/android/checkout/Purchase;",
            ">;)",
            "Lorg/solovyev/android/checkout/PurchaseFlow;"
        }
    .end annotation

    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 566
    .local p3, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mCache:Lorg/solovyev/android/checkout/ConcurrentCache;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/ConcurrentCache;->hasCache()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 567
    new-instance v0, Lorg/solovyev/android/checkout/Billing$6;

    invoke-direct {v0, p0, p3}, Lorg/solovyev/android/checkout/Billing$6;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/RequestListener;)V

    .end local p3    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    .local v0, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    move-object p3, v0

    .line 575
    .end local v0    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    .restart local p3    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<Lorg/solovyev/android/checkout/Purchase;>;"
    :cond_0
    new-instance v1, Lorg/solovyev/android/checkout/PurchaseFlow;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->getPurchaseVerifier()Lorg/solovyev/android/checkout/PurchaseVerifier;

    move-result-object v2

    invoke-direct {v1, p1, p2, p3, v2}, Lorg/solovyev/android/checkout/PurchaseFlow;-><init>(Lorg/solovyev/android/checkout/IntentStarter;ILorg/solovyev/android/checkout/RequestListener;Lorg/solovyev/android/checkout/PurchaseVerifier;)V

    return-object v1
.end method

.method public disconnect()V
    .locals 3

    .prologue
    .line 453
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 454
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->INITIAL:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, v2, :cond_1

    .line 455
    :cond_0
    monitor-exit v1

    .line 480
    :goto_0
    return-void

    .line 457
    :cond_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, v2, :cond_2

    .line 461
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/PendingRequests;->cancelAll()V

    .line 462
    monitor-exit v1

    goto :goto_0

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 464
    :cond_2
    :try_start_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v2, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, v2, :cond_3

    .line 465
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V

    .line 466
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    new-instance v2, Lorg/solovyev/android/checkout/Billing$5;

    invoke-direct {v2, p0}, Lorg/solovyev/android/checkout/Billing$5;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    invoke-interface {v0, v2}, Lorg/solovyev/android/checkout/CancellableExecutor;->execute(Ljava/lang/Runnable;)V

    .line 478
    :goto_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/PendingRequests;->cancelAll()V

    .line 479
    monitor-exit v1

    goto :goto_0

    .line 474
    :cond_3
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method getConfiguration()Lorg/solovyev/android/checkout/Billing$Configuration;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 274
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    return-object v0
.end method

.method getConnector()Lorg/solovyev/android/checkout/Billing$ServiceConnector;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 279
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 269
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public getRequests(Ljava/lang/Object;)Lorg/solovyev/android/checkout/Billing$Requests;
    .locals 2
    .param p1, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 558
    if-nez p1, :cond_0

    .line 559
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing;->getRequests()Lorg/solovyev/android/checkout/BillingRequests;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Billing$Requests;

    .line 561
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Billing$1;)V

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->withTag(Ljava/lang/Object;)Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->onMainThread()Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;->create()Lorg/solovyev/android/checkout/BillingRequests;

    move-result-object v0

    check-cast v0, Lorg/solovyev/android/checkout/Billing$Requests;

    goto :goto_0
.end method

.method public getRequests()Lorg/solovyev/android/checkout/BillingRequests;
    .locals 1
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 545
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mRequests:Lorg/solovyev/android/checkout/BillingRequests;

    return-object v0
.end method

.method getState()Lorg/solovyev/android/checkout/Billing$State;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 343
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 344
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    monitor-exit v1

    return-object v0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public newRequestsBuilder()Lorg/solovyev/android/checkout/Billing$RequestsBuilder;
    .locals 2
    .annotation runtime Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 536
    new-instance v0, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/solovyev/android/checkout/Billing$RequestsBuilder;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Billing$1;)V

    return-object v0
.end method

.method public onCheckoutStarted()V
    .locals 2

    .prologue
    .line 584
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 585
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 586
    :try_start_0
    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    .line 587
    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 588
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing;->connect()V

    .line 590
    :cond_0
    monitor-exit v1

    .line 591
    return-void

    .line 590
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method onCheckoutStopped()V
    .locals 2

    .prologue
    .line 594
    invoke-static {}, Lorg/solovyev/android/checkout/Check;->isMainThread()V

    .line 595
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 596
    :try_start_0
    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    .line 597
    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    if-gez v0, :cond_0

    .line 598
    const/4 v0, 0x0

    iput v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    .line 599
    const-string v0, "Billing#onCheckoutStopped is called more than Billing#onCheckoutStarted"

    invoke-static {v0}, Lorg/solovyev/android/checkout/Billing;->warning(Ljava/lang/String;)V

    .line 601
    :cond_0
    iget v0, p0, Lorg/solovyev/android/checkout/Billing;->mCheckoutCount:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v0}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->isAutoConnect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 602
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing;->disconnect()V

    .line 604
    :cond_1
    monitor-exit v1

    .line 605
    return-void

    .line 604
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public removePlayStoreListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V
    .locals 2
    .param p1, "listener"    # Lorg/solovyev/android/checkout/PlayStoreListener;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 440
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 441
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;->removeListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V

    .line 442
    monitor-exit v1

    .line 443
    return-void

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method runWhenConnected(Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;Ljava/lang/Object;)I
    .locals 3
    .param p1    # Lorg/solovyev/android/checkout/Request;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lorg/solovyev/android/checkout/RequestListener;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3, "tag"    # Ljava/lang/Object;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lorg/solovyev/android/checkout/Request",
            "<TR;>;",
            "Lorg/solovyev/android/checkout/RequestListener",
            "<TR;>;",
            "Ljava/lang/Object;",
            ")I"
        }
    .end annotation

    .prologue
    .line 492
    .local p1, "request":Lorg/solovyev/android/checkout/Request;, "Lorg/solovyev/android/checkout/Request<TR;>;"
    .local p2, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    if-eqz p2, :cond_1

    .line 493
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mCache:Lorg/solovyev/android/checkout/ConcurrentCache;

    invoke-virtual {v1}, Lorg/solovyev/android/checkout/ConcurrentCache;->hasCache()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 494
    new-instance v0, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;

    invoke-direct {v0, p0, p1, p2}, Lorg/solovyev/android/checkout/Billing$CachingRequestListener;-><init>(Lorg/solovyev/android/checkout/Billing;Lorg/solovyev/android/checkout/Request;Lorg/solovyev/android/checkout/RequestListener;)V

    .end local p2    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    .local v0, "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    move-object p2, v0

    .line 496
    .end local v0    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    .restart local p2    # "listener":Lorg/solovyev/android/checkout/RequestListener;, "Lorg/solovyev/android/checkout/RequestListener<TR;>;"
    :cond_0
    invoke-virtual {p1, p2}, Lorg/solovyev/android/checkout/Request;->setListener(Lorg/solovyev/android/checkout/RequestListener;)V

    .line 498
    :cond_1
    if-eqz p3, :cond_2

    .line 499
    invoke-virtual {p1, p3}, Lorg/solovyev/android/checkout/Request;->setTag(Ljava/lang/Object;)V

    .line 502
    :cond_2
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mPendingRequests:Lorg/solovyev/android/checkout/PendingRequests;

    invoke-direct {p0, p1}, Lorg/solovyev/android/checkout/Billing;->onConnectedService(Lorg/solovyev/android/checkout/Request;)Lorg/solovyev/android/checkout/RequestRunnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/solovyev/android/checkout/PendingRequests;->add(Lorg/solovyev/android/checkout/RequestRunnable;)V

    .line 503
    invoke-virtual {p0}, Lorg/solovyev/android/checkout/Billing;->connect()V

    .line 505
    invoke-virtual {p1}, Lorg/solovyev/android/checkout/Request;->getId()I

    move-result v1

    return v1
.end method

.method setBackground(Ljava/util/concurrent/Executor;)V
    .locals 0
    .param p1, "background"    # Ljava/util/concurrent/Executor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 326
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mBackground:Ljava/util/concurrent/Executor;

    .line 327
    return-void
.end method

.method setConnector(Lorg/solovyev/android/checkout/Billing$ServiceConnector;)V
    .locals 0
    .param p1, "connector"    # Lorg/solovyev/android/checkout/Billing$ServiceConnector;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 283
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    .line 284
    return-void
.end method

.method setMainThread(Lorg/solovyev/android/checkout/CancellableExecutor;)V
    .locals 0
    .param p1, "mainThread"    # Lorg/solovyev/android/checkout/CancellableExecutor;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 330
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    .line 331
    return-void
.end method

.method setPurchaseVerifier(Lorg/solovyev/android/checkout/PurchaseVerifier;)V
    .locals 1
    .param p1, "purchaseVerifier"    # Lorg/solovyev/android/checkout/PurchaseVerifier;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 334
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mConfiguration:Lorg/solovyev/android/checkout/Billing$StaticConfiguration;

    invoke-virtual {v0, p1}, Lorg/solovyev/android/checkout/Billing$StaticConfiguration;->setPurchaseVerifier(Lorg/solovyev/android/checkout/PurchaseVerifier;)V

    .line 335
    return-void
.end method

.method setService(Lcom/android/vending/billing/IInAppBillingService;Z)V
    .locals 5
    .param p1, "service"    # Lcom/android/vending/billing/IInAppBillingService;
        .annotation runtime Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2, "connecting"    # Z

    .prologue
    .line 287
    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v2

    .line 289
    if-eqz p2, :cond_3

    .line 290
    :try_start_0
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v1, v3, :cond_1

    .line 292
    if-eqz p1, :cond_0

    .line 293
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mConnector:Lorg/solovyev/android/checkout/Billing$ServiceConnector;

    invoke-interface {v1}, Lorg/solovyev/android/checkout/Billing$ServiceConnector;->disconnect()V

    .line 295
    :cond_0
    monitor-exit v2

    .line 323
    :goto_0
    return-void

    .line 297
    :cond_1
    if-nez p1, :cond_2

    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    .line 320
    .local v0, "newState":Lorg/solovyev/android/checkout/Billing$State;
    :goto_1
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mService:Lcom/android/vending/billing/IInAppBillingService;

    .line 321
    invoke-virtual {p0, v0}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V

    .line 322
    monitor-exit v2

    goto :goto_0

    .end local v0    # "newState":Lorg/solovyev/android/checkout/Billing$State;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 297
    :cond_2
    :try_start_1
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    goto :goto_1

    .line 299
    :cond_3
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->INITIAL:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v1, v3, :cond_5

    .line 301
    :cond_4
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mService:Lcom/android/vending/billing/IInAppBillingService;

    invoke-static {v1}, Lorg/solovyev/android/checkout/Check;->isNull(Ljava/lang/Object;)V

    .line 302
    monitor-exit v2

    goto :goto_0

    .line 308
    :cond_5
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->CONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v1, v3, :cond_6

    .line 309
    sget-object v1, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {p0, v1}, Lorg/solovyev/android/checkout/Billing;->setState(Lorg/solovyev/android/checkout/Billing$State;)V

    .line 311
    :cond_6
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v1, v3, :cond_7

    .line 312
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->DISCONNECTED:Lorg/solovyev/android/checkout/Billing$State;

    .restart local v0    # "newState":Lorg/solovyev/android/checkout/Billing$State;
    goto :goto_1

    .line 314
    .end local v0    # "newState":Lorg/solovyev/android/checkout/Billing$State;
    :cond_7
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    sget-object v3, Lorg/solovyev/android/checkout/Billing$State;->CONNECTING:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v1, v3, :cond_8

    const/4 v1, 0x1

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 317
    sget-object v0, Lorg/solovyev/android/checkout/Billing$State;->FAILED:Lorg/solovyev/android/checkout/Billing$State;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .restart local v0    # "newState":Lorg/solovyev/android/checkout/Billing$State;
    goto :goto_1

    .line 314
    .end local v0    # "newState":Lorg/solovyev/android/checkout/Billing$State;
    :cond_8
    const/4 v1, 0x0

    goto :goto_2
.end method

.method setState(Lorg/solovyev/android/checkout/Billing$State;)V
    .locals 4
    .param p1, "newState"    # Lorg/solovyev/android/checkout/Billing$State;
        .annotation runtime Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 349
    iget-object v1, p0, Lorg/solovyev/android/checkout/Billing;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 350
    :try_start_0
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    if-ne v0, p1, :cond_0

    .line 351
    monitor-exit v1

    .line 382
    :goto_0
    return-void

    .line 353
    :cond_0
    sget-object v0, Lorg/solovyev/android/checkout/Billing;->sPreviousStates:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "State "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " can\'t come right after "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " state"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 354
    iput-object p1, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    .line 355
    sget-object v0, Lorg/solovyev/android/checkout/Billing$7;->$SwitchMap$org$solovyev$android$checkout$Billing$State:[I

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mState:Lorg/solovyev/android/checkout/Billing$State;

    invoke-virtual {v2}, Lorg/solovyev/android/checkout/Billing$State;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 381
    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 360
    :pswitch_0
    :try_start_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreListener:Lorg/solovyev/android/checkout/PlayStoreListener;

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;->removeListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V

    goto :goto_1

    .line 366
    :pswitch_1
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreListener:Lorg/solovyev/android/checkout/PlayStoreListener;

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;->addListener(Lorg/solovyev/android/checkout/PlayStoreListener;)V

    .line 367
    invoke-direct {p0}, Lorg/solovyev/android/checkout/Billing;->executePendingRequests()V

    goto :goto_1

    .line 372
    :pswitch_2
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreBroadcastReceiver:Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;

    iget-object v2, p0, Lorg/solovyev/android/checkout/Billing;->mPlayStoreListener:Lorg/solovyev/android/checkout/PlayStoreListener;

    invoke-virtual {v0, v2}, Lorg/solovyev/android/checkout/PlayStoreBroadcastReceiver;->contains(Lorg/solovyev/android/checkout/PlayStoreListener;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    const-string v2, "Leaking the listener"

    invoke-static {v0, v2}, Lorg/solovyev/android/checkout/Check;->isTrue(ZLjava/lang/String;)V

    .line 373
    iget-object v0, p0, Lorg/solovyev/android/checkout/Billing;->mMainThread:Lorg/solovyev/android/checkout/CancellableExecutor;

    new-instance v2, Lorg/solovyev/android/checkout/Billing$3;

    invoke-direct {v2, p0}, Lorg/solovyev/android/checkout/Billing$3;-><init>(Lorg/solovyev/android/checkout/Billing;)V

    invoke-interface {v0, v2}, Lorg/solovyev/android/checkout/CancellableExecutor;->execute(Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 372
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 355
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
