.class public Ljavax/vecmath/Matrix3d;
.super Ljava/lang/Object;
.source "Matrix3d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final EPS:D = 1.110223024E-16

.field private static final ERR_EPS:D = 1.0E-8

.field static final serialVersionUID:J = 0x5ee3cf78d011e116L

.field private static xin:D

.field private static xout:D

.field private static yin:D

.field private static yout:D

.field private static zin:D

.field private static zout:D


# instance fields
.field public m00:D

.field public m01:D

.field public m02:D

.field public m10:D

.field public m11:D

.field public m12:D

.field public m20:D

.field public m21:D

.field public m22:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 196
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 197
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 198
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 200
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 201
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 202
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 204
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 205
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 206
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 208
    return-void
.end method

.method public constructor <init>(DDDDDDDDD)V
    .locals 3
    .param p1, "m00"    # D
    .param p3, "m01"    # D
    .param p5, "m02"    # D
    .param p7, "m10"    # D
    .param p9, "m11"    # D
    .param p11, "m12"    # D
    .param p13, "m20"    # D
    .param p15, "m21"    # D
    .param p17, "m22"    # D

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 115
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 116
    iput-wide p5, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 118
    iput-wide p7, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 119
    iput-wide p9, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 120
    iput-wide p11, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 122
    move-wide/from16 v0, p13

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 123
    move-wide/from16 v0, p15

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 124
    move-wide/from16 v0, p17

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 126
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 157
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 158
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 160
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 161
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 162
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 164
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 165
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 166
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 168
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 176
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 178
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 179
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 181
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 182
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 183
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 185
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 186
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 187
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 189
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "v"    # [D

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 136
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 137
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 139
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 140
    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 141
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 143
    const/4 v0, 0x6

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 144
    const/4 v0, 0x7

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 145
    const/16 v0, 0x8

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 147
    return-void
.end method

.method private static final almostEqual(DD)Z
    .locals 16
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 3085
    cmpl-double v12, p0, p2

    if-nez v12, :cond_0

    .line 3086
    const/4 v12, 0x1

    .line 3101
    :goto_0
    return v12

    .line 3088
    :cond_0
    const-wide v0, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    .line 3089
    .local v0, "EPSILON_ABSOLUTE":D
    const-wide v2, 0x3f1a36e2eb1c432dL    # 1.0E-4

    .line 3090
    .local v2, "EPSILON_RELATIVE":D
    sub-double v12, p0, p2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    .line 3091
    .local v8, "diff":D
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    .line 3092
    .local v4, "absA":D
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    .line 3093
    .local v6, "absB":D
    cmpl-double v12, v4, v6

    if-ltz v12, :cond_1

    move-wide v10, v4

    .line 3095
    .local v10, "max":D
    :goto_1
    const-wide v12, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v12, v8, v12

    if-gez v12, :cond_2

    .line 3096
    const/4 v12, 0x1

    goto :goto_0

    .end local v10    # "max":D
    :cond_1
    move-wide v10, v6

    .line 3093
    goto :goto_1

    .line 3098
    .restart local v10    # "max":D
    :cond_2
    div-double v12, v8, v10

    const-wide v14, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpg-double v12, v12, v14

    if-gez v12, :cond_3

    .line 3099
    const/4 v12, 0x1

    goto :goto_0

    .line 3101
    :cond_3
    const/4 v12, 0x0

    goto :goto_0
.end method

.method static compute_2X2(DDD[D[D[D[D[DI)I
    .locals 64
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "h"    # D
    .param p6, "single_values"    # [D
    .param p7, "snl"    # [D
    .param p8, "csl"    # [D
    .param p9, "snr"    # [D
    .param p10, "csr"    # [D
    .param p11, "index"    # I

    .prologue
    .line 2759
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 2760
    .local v6, "c_b3":D
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 2772
    .local v8, "c_b4":D
    const/16 v51, 0x0

    aget-wide v46, p6, v51

    .line 2773
    .local v46, "ssmax":D
    const/16 v51, 0x1

    aget-wide v48, p6, v51

    .line 2774
    .local v48, "ssmin":D
    const-wide/16 v10, 0x0

    .line 2775
    .local v10, "clt":D
    const-wide/16 v12, 0x0

    .line 2776
    .local v12, "crt":D
    const-wide/16 v42, 0x0

    .line 2777
    .local v42, "slt":D
    const-wide/16 v44, 0x0

    .line 2778
    .local v44, "srt":D
    const-wide/16 v56, 0x0

    .line 2780
    .local v56, "tsign":D
    move-wide/from16 v20, p0

    .line 2781
    .local v20, "ft":D
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    .line 2782
    .local v18, "fa":D
    move-wide/from16 v30, p4

    .line 2783
    .local v30, "ht":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    .line 2785
    .local v28, "ha":D
    const/16 v25, 0x1

    .line 2786
    .local v25, "pmax":I
    cmpl-double v51, v28, v18

    if-lez v51, :cond_1

    .line 2787
    const/16 v50, 0x1

    .line 2791
    .local v50, "swap":Z
    :goto_0
    if-eqz v50, :cond_0

    .line 2792
    const/16 v25, 0x3

    .line 2793
    move-wide/from16 v54, v20

    .line 2794
    .local v54, "temp":D
    move-wide/from16 v20, v30

    .line 2795
    move-wide/from16 v30, v54

    .line 2796
    move-wide/from16 v54, v18

    .line 2797
    move-wide/from16 v18, v28

    .line 2798
    move-wide/from16 v28, v54

    .line 2801
    .end local v54    # "temp":D
    :cond_0
    move-wide/from16 v26, p2

    .line 2802
    .local v26, "gt":D
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    .line 2803
    .local v22, "ga":D
    const-wide/16 v60, 0x0

    cmpl-double v51, v22, v60

    if-nez v51, :cond_2

    .line 2805
    const/16 v51, 0x1

    aput-wide v28, p6, v51

    .line 2806
    const/16 v51, 0x0

    aput-wide v18, p6, v51

    .line 2807
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2808
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 2809
    const-wide/16 v42, 0x0

    .line 2810
    const-wide/16 v44, 0x0

    .line 2947
    :goto_1
    const/16 v51, 0x0

    return v51

    .line 2789
    .end local v22    # "ga":D
    .end local v26    # "gt":D
    .end local v50    # "swap":Z
    :cond_1
    const/16 v50, 0x0

    .restart local v50    # "swap":Z
    goto :goto_0

    .line 2812
    .restart local v22    # "ga":D
    .restart local v26    # "gt":D
    :cond_2
    const/16 v24, 0x1

    .line 2814
    .local v24, "gasmal":Z
    cmpl-double v51, v22, v18

    if-lez v51, :cond_3

    .line 2815
    const/16 v25, 0x2

    .line 2816
    div-double v60, v18, v22

    const-wide v62, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v51, v60, v62

    if-gez v51, :cond_3

    .line 2818
    const/16 v24, 0x0

    .line 2819
    move-wide/from16 v46, v22

    .line 2820
    const-wide/high16 v60, 0x3ff0000000000000L    # 1.0

    cmpl-double v51, v28, v60

    if-lez v51, :cond_9

    .line 2821
    div-double v60, v22, v28

    div-double v48, v18, v60

    .line 2825
    :goto_2
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2826
    div-double v42, v30, v26

    .line 2827
    const-wide/high16 v44, 0x3ff0000000000000L    # 1.0

    .line 2828
    div-double v12, v20, v26

    .line 2831
    :cond_3
    if-eqz v24, :cond_5

    .line 2833
    sub-double v14, v18, v28

    .line 2834
    .local v14, "d":D
    cmpl-double v51, v14, v18

    if-nez v51, :cond_a

    .line 2836
    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    .line 2841
    .local v32, "l":D
    :goto_3
    div-double v34, v26, v20

    .line 2843
    .local v34, "m":D
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    sub-double v52, v60, v32

    .line 2845
    .local v52, "t":D
    mul-double v36, v34, v34

    .line 2846
    .local v36, "mm":D
    mul-double v58, v52, v52

    .line 2847
    .local v58, "tt":D
    add-double v60, v58, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v40

    .line 2849
    .local v40, "s":D
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_b

    .line 2850
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v38

    .line 2855
    .local v38, "r":D
    :goto_4
    add-double v60, v40, v38

    const-wide/high16 v62, 0x3fe0000000000000L    # 0.5

    mul-double v4, v60, v62

    .line 2857
    .local v4, "a":D
    cmpl-double v51, v22, v18

    if-lez v51, :cond_4

    .line 2858
    const/16 v25, 0x2

    .line 2859
    div-double v60, v18, v22

    const-wide v62, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v51, v60, v62

    if-gez v51, :cond_4

    .line 2861
    const/16 v24, 0x0

    .line 2862
    move-wide/from16 v46, v22

    .line 2863
    const-wide/high16 v60, 0x3ff0000000000000L    # 1.0

    cmpl-double v51, v28, v60

    if-lez v51, :cond_c

    .line 2864
    div-double v60, v22, v28

    div-double v48, v18, v60

    .line 2868
    :goto_5
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2869
    div-double v42, v30, v26

    .line 2870
    const-wide/high16 v44, 0x3ff0000000000000L    # 1.0

    .line 2871
    div-double v12, v20, v26

    .line 2874
    :cond_4
    if-eqz v24, :cond_5

    .line 2876
    sub-double v14, v18, v28

    .line 2877
    cmpl-double v51, v14, v18

    if-nez v51, :cond_d

    .line 2879
    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    .line 2884
    :goto_6
    div-double v34, v26, v20

    .line 2886
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    sub-double v52, v60, v32

    .line 2888
    mul-double v36, v34, v34

    .line 2889
    mul-double v58, v52, v52

    .line 2890
    add-double v60, v58, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v40

    .line 2892
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_e

    .line 2893
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v38

    .line 2898
    :goto_7
    add-double v60, v40, v38

    const-wide/high16 v62, 0x3fe0000000000000L    # 0.5

    mul-double v4, v60, v62

    .line 2901
    div-double v48, v28, v4

    .line 2902
    mul-double v46, v18, v4

    .line 2903
    const-wide/16 v60, 0x0

    cmpl-double v51, v36, v60

    if-nez v51, :cond_10

    .line 2905
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_f

    .line 2906
    move-wide/from16 v0, v20

    invoke-static {v6, v7, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    move-wide/from16 v0, v26

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v52, v60, v62

    .line 2913
    :goto_8
    mul-double v60, v52, v52

    const-wide/high16 v62, 0x4010000000000000L    # 4.0

    add-double v60, v60, v62

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v32

    .line 2914
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    div-double v12, v60, v32

    .line 2915
    div-double v44, v52, v32

    .line 2916
    mul-double v60, v44, v34

    add-double v60, v60, v12

    div-double v10, v60, v4

    .line 2917
    div-double v60, v30, v20

    mul-double v60, v60, v44

    div-double v42, v60, v4

    .line 2920
    .end local v4    # "a":D
    .end local v14    # "d":D
    .end local v32    # "l":D
    .end local v34    # "m":D
    .end local v36    # "mm":D
    .end local v38    # "r":D
    .end local v40    # "s":D
    .end local v52    # "t":D
    .end local v58    # "tt":D
    :cond_5
    if-eqz v50, :cond_11

    .line 2921
    const/16 v51, 0x0

    aput-wide v44, p8, v51

    .line 2922
    const/16 v51, 0x0

    aput-wide v12, p7, v51

    .line 2923
    const/16 v51, 0x0

    aput-wide v42, p10, v51

    .line 2924
    const/16 v51, 0x0

    aput-wide v10, p9, v51

    .line 2932
    :goto_9
    const/16 v51, 0x1

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_6

    .line 2933
    const/16 v51, 0x0

    aget-wide v60, p10, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p8, v51

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p0

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2935
    :cond_6
    const/16 v51, 0x2

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_7

    .line 2936
    const/16 v51, 0x0

    aget-wide v60, p9, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p8, v51

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p2

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2938
    :cond_7
    const/16 v51, 0x3

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_8

    .line 2939
    const/16 v51, 0x0

    aget-wide v60, p9, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p7, v51

    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p4

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2941
    :cond_8
    move-wide/from16 v0, v46

    move-wide/from16 v2, v56

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    aput-wide v60, p6, p11

    .line 2942
    move-wide/from16 v0, p0

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    mul-double v60, v60, v56

    move-wide/from16 v0, p4

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v62

    mul-double v16, v60, v62

    .line 2943
    .local v16, "d__1":D
    add-int/lit8 v51, p11, 0x1

    move-wide/from16 v0, v48

    move-wide/from16 v2, v16

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    aput-wide v60, p6, v51

    goto/16 :goto_1

    .line 2823
    .end local v16    # "d__1":D
    :cond_9
    div-double v60, v18, v22

    mul-double v48, v60, v28

    goto/16 :goto_2

    .line 2838
    .restart local v14    # "d":D
    :cond_a
    div-double v32, v14, v18

    .restart local v32    # "l":D
    goto/16 :goto_3

    .line 2852
    .restart local v34    # "m":D
    .restart local v36    # "mm":D
    .restart local v40    # "s":D
    .restart local v52    # "t":D
    .restart local v58    # "tt":D
    :cond_b
    mul-double v60, v32, v32

    add-double v60, v60, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    .restart local v38    # "r":D
    goto/16 :goto_4

    .line 2866
    .restart local v4    # "a":D
    :cond_c
    div-double v60, v18, v22

    mul-double v48, v60, v28

    goto/16 :goto_5

    .line 2881
    :cond_d
    div-double v32, v14, v18

    goto/16 :goto_6

    .line 2895
    :cond_e
    mul-double v60, v32, v32

    add-double v60, v60, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    goto/16 :goto_7

    .line 2908
    :cond_f
    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v60

    div-double v60, v26, v60

    div-double v62, v34, v52

    add-double v52, v60, v62

    goto/16 :goto_8

    .line 2911
    :cond_10
    add-double v60, v40, v52

    div-double v60, v34, v60

    add-double v62, v38, v32

    div-double v62, v34, v62

    add-double v60, v60, v62

    const-wide/high16 v62, 0x3ff0000000000000L    # 1.0

    add-double v62, v62, v4

    mul-double v52, v60, v62

    goto/16 :goto_8

    .line 2926
    .end local v4    # "a":D
    .end local v14    # "d":D
    .end local v32    # "l":D
    .end local v34    # "m":D
    .end local v36    # "mm":D
    .end local v38    # "r":D
    .end local v40    # "s":D
    .end local v52    # "t":D
    .end local v58    # "tt":D
    :cond_11
    const/16 v51, 0x0

    aput-wide v10, p8, v51

    .line 2927
    const/16 v51, 0x0

    aput-wide v42, p7, v51

    .line 2928
    const/16 v51, 0x0

    aput-wide v12, p10, v51

    .line 2929
    const/16 v51, 0x0

    aput-wide v44, p9, v51

    goto/16 :goto_9
.end method

.method static compute_qr([D[D[D[D)I
    .locals 40
    .param p0, "s"    # [D
    .param p1, "e"    # [D
    .param p2, "u"    # [D
    .param p3, "v"    # [D

    .prologue
    .line 2540
    const/4 v6, 0x2

    new-array v0, v6, [D

    move-object/from16 v18, v0

    .line 2541
    .local v18, "cosl":[D
    const/4 v6, 0x2

    new-array v0, v6, [D

    move-object/from16 v20, v0

    .line 2542
    .local v20, "cosr":[D
    const/4 v6, 0x2

    new-array v0, v6, [D

    move-object/from16 v17, v0

    .line 2543
    .local v17, "sinl":[D
    const/4 v6, 0x2

    new-array v0, v6, [D

    move-object/from16 v19, v0

    .line 2544
    .local v19, "sinr":[D
    const/16 v6, 0x9

    new-array v0, v6, [D

    move-object/from16 v31, v0

    .line 2549
    .local v31, "m":[D
    const/16 v24, 0xa

    .line 2550
    .local v24, "MAX_INTERATIONS":I
    const-wide v22, 0x3cf605c9419ea60aL    # 4.89E-15

    .line 2552
    .local v22, "CONVERGE_TOL":D
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    .line 2553
    .local v26, "c_b48":D
    const-wide/high16 v28, -0x4010000000000000L    # -1.0

    .line 2555
    .local v28, "c_b71":D
    const/16 v25, 0x0

    .line 2558
    .local v25, "converged":Z
    const/4 v9, 0x1

    .line 2560
    .local v9, "first":I
    const/4 v6, 0x1

    aget-wide v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v6, v6, v10

    if-ltz v6, :cond_0

    const/4 v6, 0x0

    aget-wide v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v6, v6, v10

    if-gez v6, :cond_1

    :cond_0
    const/16 v25, 0x1

    .line 2562
    :cond_1
    const/16 v30, 0x0

    .local v30, "k":I
    :goto_0
    const/16 v6, 0xa

    move/from16 v0, v30

    if-ge v0, v6, :cond_4

    if-nez v25, :cond_4

    .line 2563
    const/4 v6, 0x1

    aget-wide v2, p0, v6

    const/4 v6, 0x1

    aget-wide v4, p1, v6

    const/4 v6, 0x2

    aget-wide v6, p0, v6

    invoke-static/range {v2 .. v7}, Ljavax/vecmath/Matrix3d;->compute_shift(DDD)D

    move-result-wide v34

    .line 2564
    .local v34, "shift":D
    const/4 v6, 0x0

    aget-wide v6, p0, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    sub-double v6, v6, v34

    const/4 v8, 0x0

    aget-wide v10, p0, v8

    move-wide/from16 v0, v26

    invoke-static {v0, v1, v10, v11}, Ljavax/vecmath/Matrix3d;->d_sign(DD)D

    move-result-wide v10

    const/4 v8, 0x0

    aget-wide v12, p0, v8

    div-double v12, v34, v12

    add-double/2addr v10, v12

    mul-double v2, v6, v10

    .line 2565
    .local v2, "f":D
    const/4 v6, 0x0

    aget-wide v4, p1, v6

    .line 2566
    .local v4, "g":D
    const/4 v8, 0x0

    move-object/from16 v6, v19

    move-object/from16 v7, v20

    invoke-static/range {v2 .. v9}, Ljavax/vecmath/Matrix3d;->compute_rot(DD[D[DII)D

    move-result-wide v32

    .line 2567
    .local v32, "r":D
    const/4 v6, 0x0

    aget-wide v6, v20, v6

    const/4 v8, 0x0

    aget-wide v10, p0, v8

    mul-double/2addr v6, v10

    const/4 v8, 0x0

    aget-wide v10, v19, v8

    const/4 v8, 0x0

    aget-wide v12, p1, v8

    mul-double/2addr v10, v12

    add-double v2, v6, v10

    .line 2568
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    const/4 v7, 0x0

    aget-wide v12, p1, v7

    mul-double/2addr v10, v12

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x0

    aget-wide v14, p0, v7

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    aput-wide v10, p1, v6

    .line 2569
    const/4 v6, 0x0

    aget-wide v6, v19, v6

    const/4 v8, 0x1

    aget-wide v10, p0, v8

    mul-double v4, v6, v10

    .line 2570
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    const/4 v7, 0x1

    aget-wide v12, p0, v7

    mul-double/2addr v10, v12

    aput-wide v10, p0, v6

    .line 2572
    const/4 v8, 0x0

    move-object/from16 v6, v17

    move-object/from16 v7, v18

    invoke-static/range {v2 .. v9}, Ljavax/vecmath/Matrix3d;->compute_rot(DD[D[DII)D

    move-result-wide v32

    .line 2573
    const/4 v9, 0x0

    .line 2574
    const/4 v6, 0x0

    aput-wide v32, p0, v6

    .line 2575
    const/4 v6, 0x0

    aget-wide v6, v18, v6

    const/4 v8, 0x0

    aget-wide v10, p1, v8

    mul-double/2addr v6, v10

    const/4 v8, 0x0

    aget-wide v10, v17, v8

    const/4 v8, 0x1

    aget-wide v12, p0, v8

    mul-double/2addr v10, v12

    add-double v2, v6, v10

    .line 2576
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    const/4 v7, 0x1

    aget-wide v12, p0, v7

    mul-double/2addr v10, v12

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x0

    aget-wide v14, p1, v7

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    aput-wide v10, p0, v6

    .line 2577
    const/4 v6, 0x0

    aget-wide v6, v17, v6

    const/4 v8, 0x1

    aget-wide v10, p1, v8

    mul-double v4, v6, v10

    .line 2578
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    const/4 v7, 0x1

    aget-wide v12, p1, v7

    mul-double/2addr v10, v12

    aput-wide v10, p1, v6

    .line 2580
    const/4 v8, 0x1

    move-object/from16 v6, v19

    move-object/from16 v7, v20

    invoke-static/range {v2 .. v9}, Ljavax/vecmath/Matrix3d;->compute_rot(DD[D[DII)D

    move-result-wide v32

    .line 2581
    const/4 v6, 0x0

    aput-wide v32, p1, v6

    .line 2582
    const/4 v6, 0x1

    aget-wide v6, v20, v6

    const/4 v8, 0x1

    aget-wide v10, p0, v8

    mul-double/2addr v6, v10

    const/4 v8, 0x1

    aget-wide v10, v19, v8

    const/4 v8, 0x1

    aget-wide v12, p1, v8

    mul-double/2addr v10, v12

    add-double v2, v6, v10

    .line 2583
    const/4 v6, 0x1

    const/4 v7, 0x1

    aget-wide v10, v20, v7

    const/4 v7, 0x1

    aget-wide v12, p1, v7

    mul-double/2addr v10, v12

    const/4 v7, 0x1

    aget-wide v12, v19, v7

    const/4 v7, 0x1

    aget-wide v14, p0, v7

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    aput-wide v10, p1, v6

    .line 2584
    const/4 v6, 0x1

    aget-wide v6, v19, v6

    const/4 v8, 0x2

    aget-wide v10, p0, v8

    mul-double v4, v6, v10

    .line 2585
    const/4 v6, 0x2

    const/4 v7, 0x1

    aget-wide v10, v20, v7

    const/4 v7, 0x2

    aget-wide v12, p0, v7

    mul-double/2addr v10, v12

    aput-wide v10, p0, v6

    .line 2587
    const/4 v8, 0x1

    move-object/from16 v6, v17

    move-object/from16 v7, v18

    invoke-static/range {v2 .. v9}, Ljavax/vecmath/Matrix3d;->compute_rot(DD[D[DII)D

    move-result-wide v32

    .line 2588
    const/4 v6, 0x1

    aput-wide v32, p0, v6

    .line 2589
    const/4 v6, 0x1

    aget-wide v6, v18, v6

    const/4 v8, 0x1

    aget-wide v10, p1, v8

    mul-double/2addr v6, v10

    const/4 v8, 0x1

    aget-wide v10, v17, v8

    const/4 v8, 0x2

    aget-wide v12, p0, v8

    mul-double/2addr v10, v12

    add-double v2, v6, v10

    .line 2590
    const/4 v6, 0x2

    const/4 v7, 0x1

    aget-wide v10, v18, v7

    const/4 v7, 0x2

    aget-wide v12, p0, v7

    mul-double/2addr v10, v12

    const/4 v7, 0x1

    aget-wide v12, v17, v7

    const/4 v7, 0x1

    aget-wide v14, p1, v7

    mul-double/2addr v12, v14

    sub-double/2addr v10, v12

    aput-wide v10, p0, v6

    .line 2591
    const/4 v6, 0x1

    aput-wide v2, p1, v6

    .line 2594
    const/4 v6, 0x0

    aget-wide v36, p2, v6

    .line 2595
    .local v36, "utemp":D
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x3

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2596
    const/4 v6, 0x3

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x3

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2597
    const/4 v6, 0x1

    aget-wide v36, p2, v6

    .line 2598
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x4

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2599
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x4

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2600
    const/4 v6, 0x2

    aget-wide v36, p2, v6

    .line 2601
    const/4 v6, 0x2

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x5

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2602
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x5

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2604
    const/4 v6, 0x3

    aget-wide v36, p2, v6

    .line 2605
    const/4 v6, 0x3

    const/4 v7, 0x1

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v17, v7

    const/4 v7, 0x6

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2606
    const/4 v6, 0x6

    const/4 v7, 0x1

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v18, v7

    const/4 v7, 0x6

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2607
    const/4 v6, 0x4

    aget-wide v36, p2, v6

    .line 2608
    const/4 v6, 0x4

    const/4 v7, 0x1

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v17, v7

    const/4 v7, 0x7

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2609
    const/4 v6, 0x7

    const/4 v7, 0x1

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v18, v7

    const/4 v7, 0x7

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2610
    const/4 v6, 0x5

    aget-wide v36, p2, v6

    .line 2611
    const/4 v6, 0x5

    const/4 v7, 0x1

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v17, v7

    const/16 v7, 0x8

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2612
    const/16 v6, 0x8

    const/4 v7, 0x1

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x1

    aget-wide v12, v18, v7

    const/16 v7, 0x8

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2616
    const/4 v6, 0x0

    aget-wide v38, p3, v6

    .line 2617
    .local v38, "vtemp":D
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x1

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2618
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x1

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2619
    const/4 v6, 0x3

    aget-wide v38, p3, v6

    .line 2620
    const/4 v6, 0x3

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x4

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2621
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x4

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2622
    const/4 v6, 0x6

    aget-wide v38, p3, v6

    .line 2623
    const/4 v6, 0x6

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x7

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2624
    const/4 v6, 0x7

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x7

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2626
    const/4 v6, 0x1

    aget-wide v38, p3, v6

    .line 2627
    const/4 v6, 0x1

    const/4 v7, 0x1

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v19, v7

    const/4 v7, 0x2

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2628
    const/4 v6, 0x2

    const/4 v7, 0x1

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v20, v7

    const/4 v7, 0x2

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2629
    const/4 v6, 0x4

    aget-wide v38, p3, v6

    .line 2630
    const/4 v6, 0x4

    const/4 v7, 0x1

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v19, v7

    const/4 v7, 0x5

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2631
    const/4 v6, 0x5

    const/4 v7, 0x1

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v20, v7

    const/4 v7, 0x5

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2632
    const/4 v6, 0x7

    aget-wide v38, p3, v6

    .line 2633
    const/4 v6, 0x7

    const/4 v7, 0x1

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v19, v7

    const/16 v7, 0x8

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2634
    const/16 v6, 0x8

    const/4 v7, 0x1

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x1

    aget-wide v12, v20, v7

    const/16 v7, 0x8

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2637
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, p0, v7

    aput-wide v10, v31, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, p1, v7

    aput-wide v10, v31, v6

    const/4 v6, 0x2

    const-wide/16 v10, 0x0

    aput-wide v10, v31, v6

    .line 2638
    const/4 v6, 0x3

    const-wide/16 v10, 0x0

    aput-wide v10, v31, v6

    const/4 v6, 0x4

    const/4 v7, 0x1

    aget-wide v10, p0, v7

    aput-wide v10, v31, v6

    const/4 v6, 0x5

    const/4 v7, 0x1

    aget-wide v10, p1, v7

    aput-wide v10, v31, v6

    .line 2639
    const/4 v6, 0x6

    const-wide/16 v10, 0x0

    aput-wide v10, v31, v6

    const/4 v6, 0x7

    const-wide/16 v10, 0x0

    aput-wide v10, v31, v6

    const/16 v6, 0x8

    const/4 v7, 0x2

    aget-wide v10, p0, v7

    aput-wide v10, v31, v6

    .line 2641
    const/4 v6, 0x1

    aget-wide v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v6, v6, v10

    if-ltz v6, :cond_2

    const/4 v6, 0x0

    aget-wide v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v6, v6, v10

    if-gez v6, :cond_3

    :cond_2
    const/16 v25, 0x1

    .line 2562
    :cond_3
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_0

    .line 2644
    .end local v2    # "f":D
    .end local v4    # "g":D
    .end local v32    # "r":D
    .end local v34    # "shift":D
    .end local v36    # "utemp":D
    .end local v38    # "vtemp":D
    :cond_4
    const/4 v6, 0x1

    aget-wide v6, p1, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v6, v6, v10

    if-gez v6, :cond_5

    .line 2645
    const/4 v6, 0x0

    aget-wide v10, p0, v6

    const/4 v6, 0x0

    aget-wide v12, p1, v6

    const/4 v6, 0x1

    aget-wide v14, p0, v6

    const/16 v21, 0x0

    move-object/from16 v16, p0

    invoke-static/range {v10 .. v21}, Ljavax/vecmath/Matrix3d;->compute_2X2(DDD[D[D[D[D[DI)I

    .line 2647
    const/4 v6, 0x0

    aget-wide v36, p2, v6

    .line 2648
    .restart local v36    # "utemp":D
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x3

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2649
    const/4 v6, 0x3

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x3

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2650
    const/4 v6, 0x1

    aget-wide v36, p2, v6

    .line 2651
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x4

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2652
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x4

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2653
    const/4 v6, 0x2

    aget-wide v36, p2, v6

    .line 2654
    const/4 v6, 0x2

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x5

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2655
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x5

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2659
    const/4 v6, 0x0

    aget-wide v38, p3, v6

    .line 2660
    .restart local v38    # "vtemp":D
    const/4 v6, 0x0

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x1

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2661
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x1

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2662
    const/4 v6, 0x3

    aget-wide v38, p3, v6

    .line 2663
    const/4 v6, 0x3

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x4

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2664
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x4

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2665
    const/4 v6, 0x6

    aget-wide v38, p3, v6

    .line 2666
    const/4 v6, 0x6

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x7

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2667
    const/4 v6, 0x7

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x7

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2694
    :goto_1
    const/4 v6, 0x0

    return v6

    .line 2669
    .end local v36    # "utemp":D
    .end local v38    # "vtemp":D
    :cond_5
    const/4 v6, 0x1

    aget-wide v10, p0, v6

    const/4 v6, 0x1

    aget-wide v12, p1, v6

    const/4 v6, 0x2

    aget-wide v14, p0, v6

    const/16 v21, 0x1

    move-object/from16 v16, p0

    invoke-static/range {v10 .. v21}, Ljavax/vecmath/Matrix3d;->compute_2X2(DDD[D[D[D[D[DI)I

    .line 2671
    const/4 v6, 0x3

    aget-wide v36, p2, v6

    .line 2672
    .restart local v36    # "utemp":D
    const/4 v6, 0x3

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x6

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2673
    const/4 v6, 0x6

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x6

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2674
    const/4 v6, 0x4

    aget-wide v36, p2, v6

    .line 2675
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/4 v7, 0x7

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2676
    const/4 v6, 0x7

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/4 v7, 0x7

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2677
    const/4 v6, 0x5

    aget-wide v36, p2, v6

    .line 2678
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-wide v10, v18, v7

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v17, v7

    const/16 v7, 0x8

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2679
    const/16 v6, 0x8

    const/4 v7, 0x0

    aget-wide v10, v17, v7

    neg-double v10, v10

    mul-double v10, v10, v36

    const/4 v7, 0x0

    aget-wide v12, v18, v7

    const/16 v7, 0x8

    aget-wide v14, p2, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p2, v6

    .line 2683
    const/4 v6, 0x1

    aget-wide v38, p3, v6

    .line 2684
    .restart local v38    # "vtemp":D
    const/4 v6, 0x1

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x2

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2685
    const/4 v6, 0x2

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x2

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2686
    const/4 v6, 0x4

    aget-wide v38, p3, v6

    .line 2687
    const/4 v6, 0x4

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/4 v7, 0x5

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2688
    const/4 v6, 0x5

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/4 v7, 0x5

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2689
    const/4 v6, 0x7

    aget-wide v38, p3, v6

    .line 2690
    const/4 v6, 0x7

    const/4 v7, 0x0

    aget-wide v10, v20, v7

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v19, v7

    const/16 v7, 0x8

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    .line 2691
    const/16 v6, 0x8

    const/4 v7, 0x0

    aget-wide v10, v19, v7

    neg-double v10, v10

    mul-double v10, v10, v38

    const/4 v7, 0x0

    aget-wide v12, v20, v7

    const/16 v7, 0x8

    aget-wide v14, p3, v7

    mul-double/2addr v12, v14

    add-double/2addr v10, v12

    aput-wide v10, p3, v6

    goto/16 :goto_1
.end method

.method static compute_rot(DD[D[DII)D
    .locals 26
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "sin"    # [D
    .param p5, "cos"    # [D
    .param p6, "index"    # I
    .param p7, "first"    # I

    .prologue
    .line 2958
    const-wide/high16 v14, 0x21b0000000000000L    # 2.002083095183101E-146

    .line 2959
    .local v14, "safmn2":D
    const-wide/high16 v16, 0x5e30000000000000L    # 4.9947976805055876E145

    .line 2961
    .local v16, "safmx2":D
    const-wide/16 v22, 0x0

    cmpl-double v11, p2, v22

    if-nez v11, :cond_1

    .line 2962
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 2963
    .local v4, "cs":D
    const-wide/16 v20, 0x0

    .line 2964
    .local v20, "sn":D
    move-wide/from16 v12, p0

    .line 3014
    .local v12, "r":D
    :cond_0
    :goto_0
    aput-wide v20, p4, p6

    .line 3015
    aput-wide v4, p5, p6

    .line 3016
    return-wide v12

    .line 2965
    .end local v4    # "cs":D
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_1
    const-wide/16 v22, 0x0

    cmpl-double v11, p0, v22

    if-nez v11, :cond_2

    .line 2966
    const-wide/16 v4, 0x0

    .line 2967
    .restart local v4    # "cs":D
    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    .line 2968
    .restart local v20    # "sn":D
    move-wide/from16 v12, p2

    .restart local v12    # "r":D
    goto :goto_0

    .line 2970
    .end local v4    # "cs":D
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_2
    move-wide/from16 v6, p0

    .line 2971
    .local v6, "f1":D
    move-wide/from16 v8, p2

    .line 2972
    .local v8, "g1":D
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/Matrix3d;->max(DD)D

    move-result-wide v18

    .line 2973
    .local v18, "scale":D
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    cmpl-double v11, v18, v22

    if-ltz v11, :cond_4

    .line 2974
    const/4 v2, 0x0

    .line 2975
    .local v2, "count":I
    :goto_1
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    cmpl-double v11, v18, v22

    if-ltz v11, :cond_3

    .line 2976
    add-int/lit8 v2, v2, 0x1

    .line 2977
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v6, v6, v22

    .line 2978
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v8, v8, v22

    .line 2979
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/Matrix3d;->max(DD)D

    move-result-wide v18

    goto :goto_1

    .line 2981
    :cond_3
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2982
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 2983
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 2984
    .restart local v20    # "sn":D
    move v10, v2

    .line 2985
    .local v10, "i__1":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_2
    if-gt v3, v2, :cond_7

    .line 2986
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v12, v12, v22

    .line 2985
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2988
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "cs":D
    .end local v10    # "i__1":I
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_4
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    cmpg-double v11, v18, v22

    if-gtz v11, :cond_6

    .line 2989
    const/4 v2, 0x0

    .line 2990
    .restart local v2    # "count":I
    :goto_3
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    cmpg-double v11, v18, v22

    if-gtz v11, :cond_5

    .line 2991
    add-int/lit8 v2, v2, 0x1

    .line 2992
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v6, v6, v22

    .line 2993
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v8, v8, v22

    .line 2994
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/Matrix3d;->max(DD)D

    move-result-wide v18

    goto :goto_3

    .line 2996
    :cond_5
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2997
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 2998
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 2999
    .restart local v20    # "sn":D
    move v10, v2

    .line 3000
    .restart local v10    # "i__1":I
    const/4 v3, 0x1

    .restart local v3    # "i":I
    :goto_4
    if-gt v3, v2, :cond_7

    .line 3001
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v12, v12, v22

    .line 3000
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 3004
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "cs":D
    .end local v10    # "i__1":I
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_6
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 3005
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 3006
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 3008
    .restart local v20    # "sn":D
    :cond_7
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    cmpl-double v11, v22, v24

    if-lez v11, :cond_0

    const-wide/16 v22, 0x0

    cmpg-double v11, v4, v22

    if-gez v11, :cond_0

    .line 3009
    neg-double v4, v4

    .line 3010
    move-wide/from16 v0, v20

    neg-double v0, v0

    move-wide/from16 v20, v0

    .line 3011
    neg-double v12, v12

    goto/16 :goto_0
.end method

.method static compute_shift(DDD)D
    .locals 34
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "h"    # D

    .prologue
    .line 2719
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    .line 2720
    .local v14, "fa":D
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    .line 2721
    .local v20, "ga":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    .line 2722
    .local v22, "ha":D
    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/Matrix3d;->min(DD)D

    move-result-wide v16

    .line 2723
    .local v16, "fhmn":D
    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/Matrix3d;->max(DD)D

    move-result-wide v18

    .line 2724
    .local v18, "fhmx":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v16, v26

    if-nez v26, :cond_1

    .line 2725
    const-wide/16 v24, 0x0

    .line 2726
    .local v24, "ssmin":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v18, v26

    if-nez v26, :cond_0

    .line 2754
    :goto_0
    return-wide v24

    .line 2728
    :cond_0
    invoke-static/range {v18 .. v21}, Ljavax/vecmath/Matrix3d;->min(DD)D

    move-result-wide v26

    invoke-static/range {v18 .. v21}, Ljavax/vecmath/Matrix3d;->max(DD)D

    move-result-wide v28

    div-double v26, v26, v28

    goto :goto_0

    .line 2731
    .end local v24    # "ssmin":D
    :cond_1
    cmpg-double v26, v20, v18

    if-gez v26, :cond_2

    .line 2732
    div-double v26, v16, v18

    const-wide/high16 v28, 0x3ff0000000000000L    # 1.0

    add-double v2, v26, v28

    .line 2733
    .local v2, "as":D
    sub-double v26, v18, v16

    div-double v4, v26, v18

    .line 2734
    .local v4, "at":D
    div-double v10, v20, v18

    .line 2735
    .local v10, "d__1":D
    mul-double v6, v10, v10

    .line 2736
    .local v6, "au":D
    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    mul-double v28, v2, v2

    add-double v28, v28, v6

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    mul-double v30, v4, v4

    add-double v30, v30, v6

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    add-double v28, v28, v30

    div-double v8, v26, v28

    .line 2737
    .local v8, "c":D
    mul-double v24, v16, v8

    .restart local v24    # "ssmin":D
    goto :goto_0

    .line 2739
    .end local v2    # "as":D
    .end local v4    # "at":D
    .end local v6    # "au":D
    .end local v8    # "c":D
    .end local v10    # "d__1":D
    .end local v24    # "ssmin":D
    :cond_2
    div-double v6, v18, v20

    .line 2740
    .restart local v6    # "au":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v6, v26

    if-nez v26, :cond_3

    .line 2741
    mul-double v26, v16, v18

    div-double v24, v26, v20

    .restart local v24    # "ssmin":D
    goto :goto_0

    .line 2743
    .end local v24    # "ssmin":D
    :cond_3
    div-double v26, v16, v18

    const-wide/high16 v28, 0x3ff0000000000000L    # 1.0

    add-double v2, v26, v28

    .line 2744
    .restart local v2    # "as":D
    sub-double v26, v18, v16

    div-double v4, v26, v18

    .line 2745
    .restart local v4    # "at":D
    mul-double v10, v2, v6

    .line 2746
    .restart local v10    # "d__1":D
    mul-double v12, v4, v6

    .line 2747
    .local v12, "d__2":D
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-double v28, v10, v10

    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    add-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    mul-double v30, v12, v12

    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    add-double v30, v30, v32

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    add-double v28, v28, v30

    div-double v8, v26, v28

    .line 2748
    .restart local v8    # "c":D
    mul-double v26, v16, v8

    mul-double v24, v26, v6

    .line 2749
    .restart local v24    # "ssmin":D
    add-double v24, v24, v24

    goto :goto_0
.end method

.method static compute_svd([D[D[D)V
    .locals 42
    .param p0, "m"    # [D
    .param p1, "outScale"    # [D
    .param p2, "outRot"    # [D

    .prologue
    .line 2118
    const/16 v4, 0x9

    new-array v0, v4, [D

    move-object/from16 v34, v0

    .line 2119
    .local v34, "u1":[D
    const/16 v4, 0x9

    new-array v0, v4, [D

    move-object/from16 v35, v0

    .line 2120
    .local v35, "v1":[D
    const/16 v4, 0x9

    new-array v5, v4, [D

    .line 2121
    .local v5, "t1":[D
    const/16 v4, 0x9

    new-array v6, v4, [D

    .line 2123
    .local v6, "t2":[D
    move-object/from16 v33, v5

    .line 2124
    .local v33, "tmp":[D
    move-object/from16 v32, v6

    .line 2126
    .local v32, "single_values":[D
    const/16 v4, 0x9

    new-array v0, v4, [D

    move-object/from16 v23, v0

    .line 2127
    .local v23, "rot":[D
    const/4 v4, 0x3

    new-array v0, v4, [D

    move-object/from16 v18, v0

    .line 2128
    .local v18, "e":[D
    const/4 v4, 0x3

    new-array v7, v4, [D

    .line 2130
    .local v7, "scales":[D
    const/16 v22, 0x0

    .line 2137
    .local v22, "negCnt":I
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_0
    const/16 v4, 0x9

    move/from16 v0, v19

    if-ge v0, v4, :cond_0

    .line 2138
    aget-wide v8, p0, v19

    aput-wide v8, v23, v19

    .line 2137
    add-int/lit8 v19, v19, 0x1

    goto :goto_0

    .line 2142
    :cond_0
    const/4 v4, 0x3

    aget-wide v8, p0, v4

    const/4 v4, 0x3

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_2

    .line 2143
    const/4 v4, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    const/4 v4, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2144
    const/4 v4, 0x3

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    const/4 v4, 0x5

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2145
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x7

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/16 v4, 0x8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    .line 2183
    :goto_1
    const/4 v4, 0x6

    aget-wide v8, p0, v4

    const/4 v4, 0x6

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_4

    .line 2236
    :goto_2
    const/4 v4, 0x2

    aget-wide v8, p0, v4

    const/4 v4, 0x2

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_6

    .line 2237
    const/4 v4, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    const/4 v4, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    .line 2238
    const/4 v4, 0x3

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x4

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    const/4 v4, 0x5

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    .line 2239
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x7

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/16 v4, 0x8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    .line 2278
    :goto_3
    const/4 v4, 0x7

    aget-wide v8, p0, v4

    const/4 v4, 0x7

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_8

    .line 2331
    :goto_4
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, p0, v8

    aput-wide v8, v32, v4

    .line 2332
    const/4 v4, 0x1

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    aput-wide v8, v32, v4

    .line 2333
    const/4 v4, 0x2

    const/16 v8, 0x8

    aget-wide v8, p0, v8

    aput-wide v8, v32, v4

    .line 2334
    const/4 v4, 0x0

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    aput-wide v8, v18, v4

    .line 2335
    const/4 v4, 0x1

    const/4 v8, 0x5

    aget-wide v8, p0, v8

    aput-wide v8, v18, v4

    .line 2337
    const/4 v4, 0x0

    aget-wide v8, v18, v4

    const/4 v4, 0x0

    aget-wide v36, v18, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_a

    const/4 v4, 0x1

    aget-wide v8, v18, v4

    const/4 v4, 0x1

    aget-wide v36, v18, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_a

    .line 2343
    :goto_5
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v32, v8

    aput-wide v8, v7, v4

    .line 2344
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v32, v8

    aput-wide v8, v7, v4

    .line 2345
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, v32, v8

    aput-wide v8, v7, v4

    .line 2349
    const/4 v4, 0x0

    aget-wide v8, v7, v4

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v36

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->almostEqual(DD)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v4, 0x1

    aget-wide v8, v7, v4

    .line 2350
    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v36

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->almostEqual(DD)Z

    move-result v4

    if-eqz v4, :cond_d

    const/4 v4, 0x2

    aget-wide v8, v7, v4

    .line 2351
    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v36

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/Matrix3d;->almostEqual(DD)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2354
    const/16 v19, 0x0

    :goto_6
    const/4 v4, 0x3

    move/from16 v0, v19

    if-ge v0, v4, :cond_b

    .line 2355
    aget-wide v8, v7, v19

    const-wide/16 v36, 0x0

    cmpg-double v4, v8, v36

    if-gez v4, :cond_1

    .line 2356
    add-int/lit8 v22, v22, 0x1

    .line 2354
    :cond_1
    add-int/lit8 v19, v19, 0x1

    goto :goto_6

    .line 2146
    :cond_2
    const/4 v4, 0x0

    aget-wide v8, p0, v4

    const/4 v4, 0x0

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_3

    .line 2147
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2148
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2149
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2150
    const/4 v4, 0x0

    const/4 v8, 0x3

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2151
    const/4 v4, 0x1

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2152
    const/4 v4, 0x2

    const/4 v8, 0x5

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2154
    const/4 v4, 0x3

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2155
    const/4 v4, 0x4

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2156
    const/4 v4, 0x5

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2158
    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2159
    const/4 v4, 0x3

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    aput-wide v8, v34, v4

    const/4 v4, 0x4

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x5

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2160
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x7

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/16 v4, 0x8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    goto/16 :goto_1

    .line 2162
    :cond_3
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x0

    aget-wide v36, p0, v4

    const/4 v4, 0x0

    aget-wide v38, p0, v4

    mul-double v36, v36, v38

    const/4 v4, 0x3

    aget-wide v38, p0, v4

    const/4 v4, 0x3

    aget-wide v40, p0, v4

    mul-double v38, v38, v40

    add-double v36, v36, v38

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    div-double v20, v8, v36

    .line 2163
    .local v20, "g":D
    const/4 v4, 0x0

    aget-wide v8, p0, v4

    mul-double v10, v8, v20

    .line 2164
    .local v10, "c1":D
    const/4 v4, 0x3

    aget-wide v8, p0, v4

    mul-double v24, v8, v20

    .line 2165
    .local v24, "s1":D
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, p0, v8

    mul-double/2addr v8, v10

    const/16 v36, 0x3

    aget-wide v36, p0, v36

    mul-double v36, v36, v24

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2166
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    mul-double/2addr v8, v10

    const/16 v36, 0x4

    aget-wide v36, p0, v36

    mul-double v36, v36, v24

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2167
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, p0, v8

    mul-double/2addr v8, v10

    const/16 v36, 0x5

    aget-wide v36, p0, v36

    mul-double v36, v36, v24

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2169
    const/4 v4, 0x3

    move-wide/from16 v0, v24

    neg-double v8, v0

    const/16 v36, 0x0

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x3

    aget-wide v36, p0, v36

    mul-double v36, v36, v10

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2170
    const/4 v4, 0x4

    move-wide/from16 v0, v24

    neg-double v8, v0

    const/16 v36, 0x1

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x4

    aget-wide v36, p0, v36

    mul-double v36, v36, v10

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2171
    const/4 v4, 0x5

    move-wide/from16 v0, v24

    neg-double v8, v0

    const/16 v36, 0x2

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x5

    aget-wide v36, p0, v36

    mul-double v36, v36, v10

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2173
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2174
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2175
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2176
    const/4 v4, 0x0

    aput-wide v10, v34, v4

    const/4 v4, 0x1

    aput-wide v24, v34, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2177
    const/4 v4, 0x3

    move-wide/from16 v0, v24

    neg-double v8, v0

    aput-wide v8, v34, v4

    const/4 v4, 0x4

    aput-wide v10, v34, v4

    const/4 v4, 0x5

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    .line 2178
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/4 v4, 0x7

    const-wide/16 v8, 0x0

    aput-wide v8, v34, v4

    const/16 v4, 0x8

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v34, v4

    goto/16 :goto_1

    .line 2184
    .end local v10    # "c1":D
    .end local v20    # "g":D
    .end local v24    # "s1":D
    :cond_4
    const/4 v4, 0x0

    aget-wide v8, p0, v4

    const/4 v4, 0x0

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_5

    .line 2185
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2186
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2187
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2188
    const/4 v4, 0x0

    const/4 v8, 0x6

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2189
    const/4 v4, 0x1

    const/4 v8, 0x7

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2190
    const/4 v4, 0x2

    const/16 v8, 0x8

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2192
    const/4 v4, 0x6

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2193
    const/4 v4, 0x7

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2194
    const/16 v4, 0x8

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2196
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2197
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2198
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2199
    const/4 v4, 0x0

    const/4 v8, 0x6

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2200
    const/4 v4, 0x1

    const/4 v8, 0x7

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2201
    const/4 v4, 0x2

    const/16 v8, 0x8

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2203
    const/4 v4, 0x6

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    .line 2204
    const/4 v4, 0x7

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    .line 2205
    const/16 v4, 0x8

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    goto/16 :goto_2

    .line 2207
    :cond_5
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x0

    aget-wide v36, p0, v4

    const/4 v4, 0x0

    aget-wide v38, p0, v4

    mul-double v36, v36, v38

    const/4 v4, 0x6

    aget-wide v38, p0, v4

    const/4 v4, 0x6

    aget-wide v40, p0, v4

    mul-double v38, v38, v40

    add-double v36, v36, v38

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    div-double v20, v8, v36

    .line 2208
    .restart local v20    # "g":D
    const/4 v4, 0x0

    aget-wide v8, p0, v4

    mul-double v12, v8, v20

    .line 2209
    .local v12, "c2":D
    const/4 v4, 0x6

    aget-wide v8, p0, v4

    mul-double v26, v8, v20

    .line 2210
    .local v26, "s2":D
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, p0, v8

    mul-double/2addr v8, v12

    const/16 v36, 0x6

    aget-wide v36, p0, v36

    mul-double v36, v36, v26

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2211
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    mul-double/2addr v8, v12

    const/16 v36, 0x7

    aget-wide v36, p0, v36

    mul-double v36, v36, v26

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2212
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, p0, v8

    mul-double/2addr v8, v12

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v26

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2214
    const/4 v4, 0x6

    move-wide/from16 v0, v26

    neg-double v8, v0

    const/16 v36, 0x0

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x6

    aget-wide v36, p0, v36

    mul-double v36, v36, v12

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2215
    const/4 v4, 0x7

    move-wide/from16 v0, v26

    neg-double v8, v0

    const/16 v36, 0x1

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x7

    aget-wide v36, p0, v36

    mul-double v36, v36, v12

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2216
    const/16 v4, 0x8

    move-wide/from16 v0, v26

    neg-double v8, v0

    const/16 v36, 0x2

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v12

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2217
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2218
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2219
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2221
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v34, v8

    mul-double/2addr v8, v12

    aput-wide v8, v33, v4

    .line 2222
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v34, v8

    mul-double/2addr v8, v12

    aput-wide v8, v33, v4

    .line 2223
    const/4 v4, 0x2

    aput-wide v26, v34, v4

    .line 2225
    const/4 v4, 0x6

    const/4 v8, 0x0

    aget-wide v8, v34, v8

    neg-double v8, v8

    mul-double v8, v8, v26

    aput-wide v8, v33, v4

    .line 2226
    const/4 v4, 0x7

    const/4 v8, 0x1

    aget-wide v8, v34, v8

    neg-double v8, v8

    mul-double v8, v8, v26

    aput-wide v8, v33, v4

    .line 2227
    const/16 v4, 0x8

    aput-wide v12, v34, v4

    .line 2228
    const/4 v4, 0x0

    const/4 v8, 0x0

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    .line 2229
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    .line 2230
    const/4 v4, 0x6

    const/4 v8, 0x6

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    .line 2231
    const/4 v4, 0x7

    const/4 v8, 0x7

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    goto/16 :goto_2

    .line 2240
    .end local v12    # "c2":D
    .end local v20    # "g":D
    .end local v26    # "s2":D
    :cond_6
    const/4 v4, 0x1

    aget-wide v8, p0, v4

    const/4 v4, 0x1

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_7

    .line 2241
    const/4 v4, 0x2

    const/4 v8, 0x2

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2242
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2243
    const/16 v4, 0x8

    const/16 v8, 0x8

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2244
    const/4 v4, 0x2

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2245
    const/4 v4, 0x5

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2246
    const/16 v4, 0x8

    const/4 v8, 0x7

    aget-wide v8, p0, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2248
    const/4 v4, 0x1

    const/4 v8, 0x2

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2249
    const/4 v4, 0x4

    const/4 v8, 0x5

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2250
    const/4 v4, 0x7

    const/16 v8, 0x8

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2252
    const/4 v4, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    const/4 v4, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    .line 2253
    const/4 v4, 0x3

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x4

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x5

    const-wide/high16 v8, -0x4010000000000000L    # -1.0

    aput-wide v8, v35, v4

    .line 2254
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x7

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    const/16 v4, 0x8

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    goto/16 :goto_3

    .line 2256
    :cond_7
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x1

    aget-wide v36, p0, v4

    const/4 v4, 0x1

    aget-wide v38, p0, v4

    mul-double v36, v36, v38

    const/4 v4, 0x2

    aget-wide v38, p0, v4

    const/4 v4, 0x2

    aget-wide v40, p0, v4

    mul-double v38, v38, v40

    add-double v36, v36, v38

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    div-double v20, v8, v36

    .line 2257
    .restart local v20    # "g":D
    const/4 v4, 0x1

    aget-wide v8, p0, v4

    mul-double v14, v8, v20

    .line 2258
    .local v14, "c3":D
    const/4 v4, 0x2

    aget-wide v8, p0, v4

    mul-double v28, v8, v20

    .line 2259
    .local v28, "s3":D
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, p0, v8

    mul-double/2addr v8, v14

    const/16 v36, 0x2

    aget-wide v36, p0, v36

    mul-double v36, v36, v28

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2260
    const/4 v4, 0x2

    move-wide/from16 v0, v28

    neg-double v8, v0

    const/16 v36, 0x1

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x2

    aget-wide v36, p0, v36

    mul-double v36, v36, v14

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2261
    const/4 v4, 0x1

    const/4 v8, 0x1

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2263
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    mul-double/2addr v8, v14

    const/16 v36, 0x5

    aget-wide v36, p0, v36

    mul-double v36, v36, v28

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2264
    const/4 v4, 0x5

    move-wide/from16 v0, v28

    neg-double v8, v0

    const/16 v36, 0x4

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x5

    aget-wide v36, p0, v36

    mul-double v36, v36, v14

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2265
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2267
    const/4 v4, 0x7

    const/4 v8, 0x7

    aget-wide v8, p0, v8

    mul-double/2addr v8, v14

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v28

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2268
    const/16 v4, 0x8

    move-wide/from16 v0, v28

    neg-double v8, v0

    const/16 v36, 0x7

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v14

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2269
    const/4 v4, 0x7

    const/4 v8, 0x7

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2271
    const/4 v4, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v35, v4

    const/4 v4, 0x1

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x2

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    .line 2272
    const/4 v4, 0x3

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x4

    aput-wide v14, v35, v4

    const/4 v4, 0x5

    move-wide/from16 v0, v28

    neg-double v8, v0

    aput-wide v8, v35, v4

    .line 2273
    const/4 v4, 0x6

    const-wide/16 v8, 0x0

    aput-wide v8, v35, v4

    const/4 v4, 0x7

    aput-wide v28, v35, v4

    const/16 v4, 0x8

    aput-wide v14, v35, v4

    goto/16 :goto_3

    .line 2279
    .end local v14    # "c3":D
    .end local v20    # "g":D
    .end local v28    # "s3":D
    :cond_8
    const/4 v4, 0x4

    aget-wide v8, p0, v4

    const/4 v4, 0x4

    aget-wide v36, p0, v4

    mul-double v8, v8, v36

    const-wide v36, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v4, v8, v36

    if-gez v4, :cond_9

    .line 2280
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2281
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2282
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, p0, v8

    aput-wide v8, v33, v4

    .line 2283
    const/4 v4, 0x3

    const/4 v8, 0x6

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2284
    const/4 v4, 0x4

    const/4 v8, 0x7

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2285
    const/4 v4, 0x5

    const/16 v8, 0x8

    aget-wide v8, p0, v8

    aput-wide v8, p0, v4

    .line 2287
    const/4 v4, 0x6

    const/4 v8, 0x3

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2288
    const/4 v4, 0x7

    const/4 v8, 0x4

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2289
    const/16 v4, 0x8

    const/4 v8, 0x5

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, p0, v4

    .line 2291
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2292
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2293
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, v34, v8

    aput-wide v8, v33, v4

    .line 2294
    const/4 v4, 0x3

    const/4 v8, 0x6

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2295
    const/4 v4, 0x4

    const/4 v8, 0x7

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2296
    const/4 v4, 0x5

    const/16 v8, 0x8

    aget-wide v8, v34, v8

    aput-wide v8, v34, v4

    .line 2298
    const/4 v4, 0x6

    const/4 v8, 0x3

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    .line 2299
    const/4 v4, 0x7

    const/4 v8, 0x4

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    .line 2300
    const/16 v4, 0x8

    const/4 v8, 0x5

    aget-wide v8, v33, v8

    neg-double v8, v8

    aput-wide v8, v34, v4

    goto/16 :goto_4

    .line 2303
    :cond_9
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x4

    aget-wide v36, p0, v4

    const/4 v4, 0x4

    aget-wide v38, p0, v4

    mul-double v36, v36, v38

    const/4 v4, 0x7

    aget-wide v38, p0, v4

    const/4 v4, 0x7

    aget-wide v40, p0, v4

    mul-double v38, v38, v40

    add-double v36, v36, v38

    invoke-static/range {v36 .. v37}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v36

    div-double v20, v8, v36

    .line 2304
    .restart local v20    # "g":D
    const/4 v4, 0x4

    aget-wide v8, p0, v4

    mul-double v16, v8, v20

    .line 2305
    .local v16, "c4":D
    const/4 v4, 0x7

    aget-wide v8, p0, v4

    mul-double v30, v8, v20

    .line 2306
    .local v30, "s4":D
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, p0, v8

    mul-double v8, v8, v16

    const/16 v36, 0x6

    aget-wide v36, p0, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2307
    const/4 v4, 0x6

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x3

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x6

    aget-wide v36, p0, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2308
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2310
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, p0, v8

    mul-double v8, v8, v16

    const/16 v36, 0x7

    aget-wide v36, p0, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2311
    const/4 v4, 0x7

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x4

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x7

    aget-wide v36, p0, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2312
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2314
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, p0, v8

    mul-double v8, v8, v16

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2315
    const/16 v4, 0x8

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x5

    aget-wide v36, p0, v36

    mul-double v8, v8, v36

    const/16 v36, 0x8

    aget-wide v36, p0, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, p0, v4

    .line 2316
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, v33, v8

    aput-wide v8, p0, v4

    .line 2318
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, v34, v8

    mul-double v8, v8, v16

    const/16 v36, 0x6

    aget-wide v36, v34, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2319
    const/4 v4, 0x6

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x3

    aget-wide v36, v34, v36

    mul-double v8, v8, v36

    const/16 v36, 0x6

    aget-wide v36, v34, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, v34, v4

    .line 2320
    const/4 v4, 0x3

    const/4 v8, 0x3

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    .line 2322
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, v34, v8

    mul-double v8, v8, v16

    const/16 v36, 0x7

    aget-wide v36, v34, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2323
    const/4 v4, 0x7

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x4

    aget-wide v36, v34, v36

    mul-double v8, v8, v36

    const/16 v36, 0x7

    aget-wide v36, v34, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, v34, v4

    .line 2324
    const/4 v4, 0x4

    const/4 v8, 0x4

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    .line 2326
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, v34, v8

    mul-double v8, v8, v16

    const/16 v36, 0x8

    aget-wide v36, v34, v36

    mul-double v36, v36, v30

    add-double v8, v8, v36

    aput-wide v8, v33, v4

    .line 2327
    const/16 v4, 0x8

    move-wide/from16 v0, v30

    neg-double v8, v0

    const/16 v36, 0x5

    aget-wide v36, v34, v36

    mul-double v8, v8, v36

    const/16 v36, 0x8

    aget-wide v36, v34, v36

    mul-double v36, v36, v16

    add-double v8, v8, v36

    aput-wide v8, v34, v4

    .line 2328
    const/4 v4, 0x5

    const/4 v8, 0x5

    aget-wide v8, v33, v8

    aput-wide v8, v34, v4

    goto/16 :goto_4

    .line 2340
    .end local v16    # "c4":D
    .end local v20    # "g":D
    .end local v30    # "s4":D
    :cond_a
    move-object/from16 v0, v32

    move-object/from16 v1, v18

    move-object/from16 v2, v34

    move-object/from16 v3, v35

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/Matrix3d;->compute_qr([D[D[D[D)I

    goto/16 :goto_5

    .line 2358
    :cond_b
    if-eqz v22, :cond_c

    const/4 v4, 0x2

    move/from16 v0, v22

    if-ne v0, v4, :cond_d

    .line 2360
    :cond_c
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x2

    const-wide/high16 v36, 0x3ff0000000000000L    # 1.0

    aput-wide v36, p1, v9

    aput-wide v36, p1, v8

    aput-wide v36, p1, v4

    .line 2361
    const/16 v19, 0x0

    :goto_7
    const/16 v4, 0x9

    move/from16 v0, v19

    if-ge v0, v4, :cond_e

    .line 2362
    aget-wide v8, v23, v19

    aput-wide v8, p2, v19

    .line 2361
    add-int/lit8 v19, v19, 0x1

    goto :goto_7

    .line 2369
    :cond_d
    move-object/from16 v0, v34

    invoke-static {v0, v5}, Ljavax/vecmath/Matrix3d;->transpose_mat([D[D)V

    .line 2370
    move-object/from16 v0, v35

    invoke-static {v0, v6}, Ljavax/vecmath/Matrix3d;->transpose_mat([D[D)V

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    move-object/from16 v9, p1

    .line 2384
    invoke-static/range {v4 .. v9}, Ljavax/vecmath/Matrix3d;->svdReorder([D[D[D[D[D[D)V

    .line 2386
    :cond_e
    return-void
.end method

.method static d_sign(DD)D
    .locals 6
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    const-wide/16 v4, 0x0

    .line 2710
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_0

    move-wide v0, p0

    .line 2711
    .local v0, "x":D
    :goto_0
    cmpl-double v2, p2, v4

    if-ltz v2, :cond_1

    .end local v0    # "x":D
    :goto_1
    return-wide v0

    .line 2710
    :cond_0
    neg-double v0, p0

    goto :goto_0

    .line 2711
    .restart local v0    # "x":D
    :cond_1
    neg-double v0, v0

    goto :goto_1
.end method

.method private final invertGeneral(Ljavax/vecmath/Matrix3d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1087
    const/16 v4, 0x9

    new-array v1, v4, [D

    .line 1088
    .local v1, "result":[D
    new-array v2, v11, [I

    .line 1090
    .local v2, "row_perm":[I
    const/16 v4, 0x9

    new-array v3, v4, [D

    .line 1096
    .local v3, "tmp":[D
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v4, v3, v10

    .line 1097
    const/4 v4, 0x1

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v6, v3, v4

    .line 1098
    const/4 v4, 0x2

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v6, v3, v4

    .line 1100
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v4, v3, v11

    .line 1101
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v4, v3, v12

    .line 1102
    const/4 v4, 0x5

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v6, v3, v4

    .line 1104
    const/4 v4, 0x6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v6, v3, v4

    .line 1105
    const/4 v4, 0x7

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v6, v3, v4

    .line 1106
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v4, v3, v13

    .line 1110
    invoke-static {v3, v2}, Ljavax/vecmath/Matrix3d;->luDecomposition([D[I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1112
    new-instance v4, Ljavax/vecmath/SingularMatrixException;

    const-string v5, "Matrix3d12"

    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1116
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x9

    if-ge v0, v4, :cond_1

    const-wide/16 v4, 0x0

    aput-wide v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1117
    :cond_1
    aput-wide v8, v1, v10

    aput-wide v8, v1, v12

    aput-wide v8, v1, v13

    .line 1118
    invoke-static {v3, v2, v1}, Ljavax/vecmath/Matrix3d;->luBacksubstitution([D[I[D)V

    .line 1120
    aget-wide v4, v1, v10

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1121
    const/4 v4, 0x1

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1122
    const/4 v4, 0x2

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1124
    aget-wide v4, v1, v11

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1125
    aget-wide v4, v1, v12

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1126
    const/4 v4, 0x5

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1128
    const/4 v4, 0x6

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1129
    const/4 v4, 0x7

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1130
    aget-wide v4, v1, v13

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1132
    return-void
.end method

.method static luBacksubstitution([D[I[D)V
    .locals 18
    .param p0, "matrix1"    # [D
    .param p1, "row_perm"    # [I
    .param p2, "matrix2"    # [D

    .prologue
    .line 1313
    const/4 v6, 0x0

    .line 1316
    .local v6, "rp":I
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_0
    const/4 v10, 0x3

    if-ge v5, v10, :cond_3

    .line 1318
    move v0, v5

    .line 1319
    .local v0, "cv":I
    const/4 v2, -0x1

    .line 1322
    .local v2, "ii":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v10, 0x3

    if-ge v1, v10, :cond_2

    .line 1325
    add-int v10, v6, v1

    aget v3, p1, v10

    .line 1326
    .local v3, "ip":I
    mul-int/lit8 v10, v3, 0x3

    add-int/2addr v10, v0

    aget-wide v8, p2, v10

    .line 1327
    .local v8, "sum":D
    mul-int/lit8 v10, v3, 0x3

    add-int/2addr v10, v0

    mul-int/lit8 v11, v1, 0x3

    add-int/2addr v11, v0

    aget-wide v12, p2, v11

    aput-wide v12, p2, v10

    .line 1328
    if-ltz v2, :cond_0

    .line 1330
    mul-int/lit8 v7, v1, 0x3

    .line 1331
    .local v7, "rv":I
    move v4, v2

    .local v4, "j":I
    :goto_2
    add-int/lit8 v10, v1, -0x1

    if-gt v4, v10, :cond_1

    .line 1332
    add-int v10, v7, v4

    aget-wide v10, p0, v10

    mul-int/lit8 v12, v4, 0x3

    add-int/2addr v12, v0

    aget-wide v12, p2, v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    .line 1331
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1335
    .end local v4    # "j":I
    .end local v7    # "rv":I
    :cond_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 1336
    move v2, v1

    .line 1338
    :cond_1
    mul-int/lit8 v10, v1, 0x3

    add-int/2addr v10, v0

    aput-wide v8, p2, v10

    .line 1322
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1343
    .end local v3    # "ip":I
    .end local v8    # "sum":D
    :cond_2
    const/4 v7, 0x6

    .line 1344
    .restart local v7    # "rv":I
    add-int/lit8 v10, v0, 0x6

    aget-wide v12, p2, v10

    const/16 v11, 0x8

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1346
    add-int/lit8 v7, v7, -0x3

    .line 1347
    add-int/lit8 v10, v0, 0x3

    add-int/lit8 v11, v0, 0x3

    aget-wide v12, p2, v11

    const/4 v11, 0x5

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x6

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x4

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1350
    add-int/lit8 v7, v7, -0x3

    .line 1351
    add-int/lit8 v10, v0, 0x0

    add-int/lit8 v11, v0, 0x0

    aget-wide v12, p2, v11

    const/4 v11, 0x1

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x3

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x2

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x6

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x0

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1316
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 1356
    .end local v0    # "cv":I
    .end local v1    # "i":I
    .end local v2    # "ii":I
    .end local v7    # "rv":I
    :cond_3
    return-void
.end method

.method static luDecomposition([D[I)Z
    .locals 30
    .param p0, "matrix0"    # [D
    .param p1, "row_perm"    # [I

    .prologue
    .line 1157
    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 1165
    .local v18, "row_scale":[D
    const/16 v16, 0x0

    .line 1166
    .local v16, "ptr":I
    const/16 v19, 0x0

    .line 1169
    .local v19, "rs":I
    const/4 v4, 0x3

    .local v4, "i":I
    move/from16 v20, v19

    .end local v19    # "rs":I
    .local v20, "rs":I
    move v5, v4

    .line 1170
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_2

    .line 1171
    const-wide/16 v2, 0x0

    .line 1174
    .local v2, "big":D
    const/4 v7, 0x3

    .local v7, "j":I
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .local v17, "ptr":I
    move v8, v7

    .line 1175
    .end local v7    # "j":I
    .local v8, "j":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "j":I
    .restart local v7    # "j":I
    if-eqz v8, :cond_0

    .line 1176
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    aget-wide v24, p0, v17

    .line 1177
    .local v24, "temp":D
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    .line 1178
    cmpl-double v26, v24, v2

    if-lez v26, :cond_e

    .line 1179
    move-wide/from16 v2, v24

    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto :goto_1

    .line 1184
    .end local v8    # "j":I
    .end local v24    # "temp":D
    .restart local v7    # "j":I
    :cond_0
    const-wide/16 v26, 0x0

    cmpl-double v26, v2, v26

    if-nez v26, :cond_1

    .line 1185
    const/16 v26, 0x0

    .line 1283
    .end local v2    # "big":D
    .end local v17    # "ptr":I
    :goto_2
    return v26

    .line 1187
    .restart local v2    # "big":D
    .restart local v17    # "ptr":I
    :cond_1
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "rs":I
    .restart local v19    # "rs":I
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v2

    aput-wide v26, v18, v20

    move/from16 v20, v19

    .end local v19    # "rs":I
    .restart local v20    # "rs":I
    move/from16 v16, v17

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 1195
    .end local v2    # "big":D
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v4    # "i":I
    :cond_2
    const/4 v11, 0x0

    .line 1198
    .local v11, "mtx":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v7, v0, :cond_d

    .line 1204
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_4

    .line 1205
    mul-int/lit8 v26, v4, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1206
    .local v21, "target":I
    aget-wide v22, p0, v21

    .line 1207
    .local v22, "sum":D
    move v9, v4

    .line 1208
    .local v9, "k":I
    mul-int/lit8 v26, v4, 0x3

    add-int v12, v11, v26

    .line 1209
    .local v12, "p1":I
    add-int v14, v11, v7

    .local v14, "p2":I
    move v10, v9

    .line 1210
    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_3

    .line 1211
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1212
    add-int/lit8 v12, v12, 0x1

    .line 1213
    add-int/lit8 v14, v14, 0x3

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 1215
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_3
    aput-wide v22, p0, v21

    .line 1204
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1220
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    :cond_4
    const-wide/16 v2, 0x0

    .line 1221
    .restart local v2    # "big":D
    const/4 v6, -0x1

    .line 1222
    .local v6, "imax":I
    move v4, v7

    :goto_6
    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v4, v0, :cond_7

    .line 1223
    mul-int/lit8 v26, v4, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1224
    .restart local v21    # "target":I
    aget-wide v22, p0, v21

    .line 1225
    .restart local v22    # "sum":D
    move v9, v7

    .line 1226
    .restart local v9    # "k":I
    mul-int/lit8 v26, v4, 0x3

    add-int v12, v11, v26

    .line 1227
    .restart local v12    # "p1":I
    add-int v14, v11, v7

    .restart local v14    # "p2":I
    move v10, v9

    .line 1228
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_7
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_5

    .line 1229
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1230
    add-int/lit8 v12, v12, 0x1

    .line 1231
    add-int/lit8 v14, v14, 0x3

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_7

    .line 1233
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_5
    aput-wide v22, p0, v21

    .line 1236
    aget-wide v26, v18, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    mul-double v24, v26, v28

    .restart local v24    # "temp":D
    cmpl-double v26, v24, v2

    if-ltz v26, :cond_6

    .line 1237
    move-wide/from16 v2, v24

    .line 1238
    move v6, v4

    .line 1222
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1242
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    .end local v24    # "temp":D
    :cond_7
    if-gez v6, :cond_8

    .line 1243
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "Matrix3d13"

    invoke-static/range {v27 .. v27}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 1247
    :cond_8
    if-eq v7, v6, :cond_a

    .line 1249
    const/4 v9, 0x3

    .line 1250
    .restart local v9    # "k":I
    mul-int/lit8 v26, v6, 0x3

    add-int v12, v11, v26

    .line 1251
    .restart local v12    # "p1":I
    mul-int/lit8 v26, v7, 0x3

    add-int v14, v11, v26

    .restart local v14    # "p2":I
    move v15, v14

    .end local v14    # "p2":I
    .local v15, "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .local v13, "p1":I
    move v10, v9

    .line 1252
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_8
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_9

    .line 1253
    aget-wide v24, p0, v13

    .line 1254
    .restart local v24    # "temp":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "p1":I
    .restart local v12    # "p1":I
    aget-wide v26, p0, v15

    aput-wide v26, p0, v13

    .line 1255
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "p2":I
    .restart local v14    # "p2":I
    aput-wide v24, p0, v15

    move v15, v14

    .end local v14    # "p2":I
    .restart local v15    # "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .restart local v13    # "p1":I
    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_8

    .line 1259
    .end local v10    # "k":I
    .end local v24    # "temp":D
    .restart local v9    # "k":I
    :cond_9
    aget-wide v26, v18, v7

    aput-wide v26, v18, v6

    .line 1263
    .end local v9    # "k":I
    .end local v13    # "p1":I
    .end local v15    # "p2":I
    :cond_a
    aput v6, p1, v7

    .line 1266
    mul-int/lit8 v26, v7, 0x3

    add-int v26, v26, v11

    add-int v26, v26, v7

    aget-wide v26, p0, v26

    const-wide/16 v28, 0x0

    cmpl-double v26, v26, v28

    if-nez v26, :cond_b

    .line 1267
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 1271
    :cond_b
    const/16 v26, 0x2

    move/from16 v0, v26

    if-eq v7, v0, :cond_c

    .line 1272
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-int/lit8 v28, v7, 0x3

    add-int v28, v28, v11

    add-int v28, v28, v7

    aget-wide v28, p0, v28

    div-double v24, v26, v28

    .line 1273
    .restart local v24    # "temp":D
    add-int/lit8 v26, v7, 0x1

    mul-int/lit8 v26, v26, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1274
    .restart local v21    # "target":I
    rsub-int/lit8 v4, v7, 0x2

    move v5, v4

    .line 1275
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :goto_9
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_c

    .line 1276
    aget-wide v26, p0, v21

    mul-double v26, v26, v24

    aput-wide v26, p0, v21

    .line 1277
    add-int/lit8 v21, v21, 0x3

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_9

    .line 1198
    .end local v5    # "i":I
    .end local v21    # "target":I
    .end local v24    # "temp":D
    .restart local v4    # "i":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 1283
    .end local v2    # "big":D
    .end local v6    # "imax":I
    :cond_d
    const/16 v26, 0x1

    goto/16 :goto_2

    .end local v11    # "mtx":I
    .restart local v2    # "big":D
    .restart local v24    # "temp":D
    :cond_e
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto/16 :goto_1
.end method

.method static mat_mul([D[D[D)V
    .locals 10
    .param p0, "m1"    # [D
    .param p1, "m2"    # [D
    .param p2, "m3"    # [D

    .prologue
    .line 3039
    const/16 v2, 0x9

    new-array v1, v2, [D

    .line 3041
    .local v1, "tmp":[D
    const/4 v2, 0x0

    const/4 v3, 0x0

    aget-wide v4, p0, v3

    const/4 v3, 0x0

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x1

    aget-wide v6, p0, v3

    const/4 v3, 0x3

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x2

    aget-wide v6, p0, v3

    const/4 v3, 0x6

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3042
    const/4 v2, 0x1

    const/4 v3, 0x0

    aget-wide v4, p0, v3

    const/4 v3, 0x1

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x1

    aget-wide v6, p0, v3

    const/4 v3, 0x4

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x2

    aget-wide v6, p0, v3

    const/4 v3, 0x7

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3043
    const/4 v2, 0x2

    const/4 v3, 0x0

    aget-wide v4, p0, v3

    const/4 v3, 0x2

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x1

    aget-wide v6, p0, v3

    const/4 v3, 0x5

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x2

    aget-wide v6, p0, v3

    const/16 v3, 0x8

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3045
    const/4 v2, 0x3

    const/4 v3, 0x3

    aget-wide v4, p0, v3

    const/4 v3, 0x0

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x4

    aget-wide v6, p0, v3

    const/4 v3, 0x3

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x5

    aget-wide v6, p0, v3

    const/4 v3, 0x6

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3046
    const/4 v2, 0x4

    const/4 v3, 0x3

    aget-wide v4, p0, v3

    const/4 v3, 0x1

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x4

    aget-wide v6, p0, v3

    const/4 v3, 0x4

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x5

    aget-wide v6, p0, v3

    const/4 v3, 0x7

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3047
    const/4 v2, 0x5

    const/4 v3, 0x3

    aget-wide v4, p0, v3

    const/4 v3, 0x2

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x4

    aget-wide v6, p0, v3

    const/4 v3, 0x5

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/4 v3, 0x5

    aget-wide v6, p0, v3

    const/16 v3, 0x8

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3049
    const/4 v2, 0x6

    const/4 v3, 0x6

    aget-wide v4, p0, v3

    const/4 v3, 0x0

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x7

    aget-wide v6, p0, v3

    const/4 v3, 0x3

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/16 v3, 0x8

    aget-wide v6, p0, v3

    const/4 v3, 0x6

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3050
    const/4 v2, 0x7

    const/4 v3, 0x6

    aget-wide v4, p0, v3

    const/4 v3, 0x1

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x7

    aget-wide v6, p0, v3

    const/4 v3, 0x4

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/16 v3, 0x8

    aget-wide v6, p0, v3

    const/4 v3, 0x7

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3051
    const/16 v2, 0x8

    const/4 v3, 0x6

    aget-wide v4, p0, v3

    const/4 v3, 0x2

    aget-wide v6, p1, v3

    mul-double/2addr v4, v6

    const/4 v3, 0x7

    aget-wide v6, p0, v3

    const/4 v3, 0x5

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    const/16 v3, 0x8

    aget-wide v6, p0, v3

    const/16 v3, 0x8

    aget-wide v8, p1, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v2

    .line 3053
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x9

    if-ge v0, v2, :cond_0

    .line 3054
    aget-wide v2, v1, v0

    aput-wide v2, p2, v0

    .line 3053
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3056
    :cond_0
    return-void
.end method

.method static max(DD)D
    .locals 2
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 2697
    cmpl-double v0, p0, p2

    if-lez v0, :cond_0

    .line 2700
    .end local p0    # "a":D
    :goto_0
    return-wide p0

    .restart local p0    # "a":D
    :cond_0
    move-wide p0, p2

    goto :goto_0
.end method

.method static max3([D)D
    .locals 7
    .param p0, "values"    # [D

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 3071
    aget-wide v0, p0, v5

    aget-wide v2, p0, v6

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 3072
    aget-wide v0, p0, v5

    aget-wide v2, p0, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 3073
    aget-wide v0, p0, v5

    .line 3080
    :goto_0
    return-wide v0

    .line 3075
    :cond_0
    aget-wide v0, p0, v4

    goto :goto_0

    .line 3077
    :cond_1
    aget-wide v0, p0, v6

    aget-wide v2, p0, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_2

    .line 3078
    aget-wide v0, p0, v6

    goto :goto_0

    .line 3080
    :cond_2
    aget-wide v0, p0, v4

    goto :goto_0
.end method

.method static min(DD)D
    .locals 2
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 2703
    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    .line 2706
    .end local p0    # "a":D
    :goto_0
    return-wide p0

    .restart local p0    # "a":D
    :cond_0
    move-wide p0, p2

    goto :goto_0
.end method

.method static print_det([D)V
    .locals 13
    .param p0, "mat"    # [D

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 3029
    aget-wide v2, p0, v8

    aget-wide v4, p0, v12

    mul-double/2addr v2, v4

    const/16 v4, 0x8

    aget-wide v4, p0, v4

    mul-double/2addr v2, v4

    aget-wide v4, p0, v9

    const/4 v6, 0x5

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    const/4 v6, 0x6

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    aget-wide v4, p0, v10

    aget-wide v6, p0, v11

    mul-double/2addr v4, v6

    const/4 v6, 0x7

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    aget-wide v4, p0, v10

    aget-wide v6, p0, v12

    mul-double/2addr v4, v6

    const/4 v6, 0x6

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    aget-wide v4, p0, v8

    const/4 v6, 0x5

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    const/4 v6, 0x7

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    aget-wide v4, p0, v9

    aget-wide v6, p0, v11

    mul-double/2addr v4, v6

    const/16 v6, 0x8

    aget-wide v6, p0, v6

    mul-double/2addr v4, v6

    sub-double v0, v2, v4

    .line 3035
    .local v0, "det":D
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "det= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3036
    return-void
.end method

.method static print_mat([D)V
    .locals 6
    .param p0, "mat"    # [D

    .prologue
    .line 3021
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 3022
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x0

    aget-wide v4, p0, v3

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x1

    aget-wide v4, p0, v3

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    mul-int/lit8 v3, v0, 0x3

    add-int/lit8 v3, v3, 0x2

    aget-wide v4, p0, v3

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 3021
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3025
    :cond_0
    return-void
.end method

.method static svdReorder([D[D[D[D[D[D)V
    .locals 18
    .param p0, "m"    # [D
    .param p1, "t1"    # [D
    .param p2, "t2"    # [D
    .param p3, "scales"    # [D
    .param p4, "outRot"    # [D
    .param p5, "outScale"    # [D

    .prologue
    .line 2391
    const/4 v11, 0x3

    new-array v9, v11, [I

    .line 2392
    .local v9, "out":[I
    const/4 v11, 0x3

    new-array v3, v11, [I

    .line 2394
    .local v3, "in":[I
    const/4 v11, 0x3

    new-array v8, v11, [D

    .line 2395
    .local v8, "mag":[D
    const/16 v11, 0x9

    new-array v10, v11, [D

    .line 2399
    .local v10, "rot":[D
    const/4 v11, 0x0

    aget-wide v12, p3, v11

    const-wide/16 v14, 0x0

    cmpg-double v11, v12, v14

    if-gez v11, :cond_0

    .line 2400
    const/4 v11, 0x0

    const/4 v12, 0x0

    aget-wide v12, p3, v12

    neg-double v12, v12

    aput-wide v12, p3, v11

    .line 2401
    const/4 v11, 0x0

    const/4 v12, 0x0

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2402
    const/4 v11, 0x1

    const/4 v12, 0x1

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2403
    const/4 v11, 0x2

    const/4 v12, 0x2

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2405
    :cond_0
    const/4 v11, 0x1

    aget-wide v12, p3, v11

    const-wide/16 v14, 0x0

    cmpg-double v11, v12, v14

    if-gez v11, :cond_1

    .line 2406
    const/4 v11, 0x1

    const/4 v12, 0x1

    aget-wide v12, p3, v12

    neg-double v12, v12

    aput-wide v12, p3, v11

    .line 2407
    const/4 v11, 0x3

    const/4 v12, 0x3

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2408
    const/4 v11, 0x4

    const/4 v12, 0x4

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2409
    const/4 v11, 0x5

    const/4 v12, 0x5

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2411
    :cond_1
    const/4 v11, 0x2

    aget-wide v12, p3, v11

    const-wide/16 v14, 0x0

    cmpg-double v11, v12, v14

    if-gez v11, :cond_2

    .line 2412
    const/4 v11, 0x2

    const/4 v12, 0x2

    aget-wide v12, p3, v12

    neg-double v12, v12

    aput-wide v12, p3, v11

    .line 2413
    const/4 v11, 0x6

    const/4 v12, 0x6

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2414
    const/4 v11, 0x7

    const/4 v12, 0x7

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2415
    const/16 v11, 0x8

    const/16 v12, 0x8

    aget-wide v12, p2, v12

    neg-double v12, v12

    aput-wide v12, p2, v11

    .line 2418
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-static {v0, v1, v10}, Ljavax/vecmath/Matrix3d;->mat_mul([D[D[D)V

    .line 2421
    const/4 v11, 0x0

    aget-wide v12, p3, v11

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const/4 v11, 0x1

    aget-wide v14, p3, v11

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    invoke-static {v12, v13, v14, v15}, Ljavax/vecmath/Matrix3d;->almostEqual(DD)Z

    move-result v11

    if-eqz v11, :cond_4

    const/4 v11, 0x1

    aget-wide v12, p3, v11

    .line 2422
    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const/4 v11, 0x2

    aget-wide v14, p3, v11

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    invoke-static {v12, v13, v14, v15}, Ljavax/vecmath/Matrix3d;->almostEqual(DD)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2423
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/16 v11, 0x9

    if-ge v2, v11, :cond_3

    .line 2424
    aget-wide v12, v10, v2

    aput-wide v12, p4, v2

    .line 2423
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2426
    :cond_3
    const/4 v2, 0x0

    :goto_1
    const/4 v11, 0x3

    if-ge v2, v11, :cond_5

    .line 2427
    aget-wide v12, p3, v2

    aput-wide v12, p5, v2

    .line 2426
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2433
    .end local v2    # "i":I
    :cond_4
    const/4 v11, 0x0

    aget-wide v12, p3, v11

    const/4 v11, 0x1

    aget-wide v14, p3, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_8

    .line 2434
    const/4 v11, 0x0

    aget-wide v12, p3, v11

    const/4 v11, 0x2

    aget-wide v14, p3, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_7

    .line 2435
    const/4 v11, 0x2

    aget-wide v12, p3, v11

    const/4 v11, 0x1

    aget-wide v14, p3, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_6

    .line 2436
    const/4 v11, 0x0

    const/4 v12, 0x0

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x2

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x1

    aput v12, v9, v11

    .line 2463
    :goto_2
    const/4 v11, 0x0

    const/4 v12, 0x0

    aget-wide v12, p0, v12

    const/4 v14, 0x0

    aget-wide v14, p0, v14

    mul-double/2addr v12, v14

    const/4 v14, 0x1

    aget-wide v14, p0, v14

    const/16 v16, 0x1

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    const/4 v14, 0x2

    aget-wide v14, p0, v14

    const/16 v16, 0x2

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    aput-wide v12, v8, v11

    .line 2464
    const/4 v11, 0x1

    const/4 v12, 0x3

    aget-wide v12, p0, v12

    const/4 v14, 0x3

    aget-wide v14, p0, v14

    mul-double/2addr v12, v14

    const/4 v14, 0x4

    aget-wide v14, p0, v14

    const/16 v16, 0x4

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    const/4 v14, 0x5

    aget-wide v14, p0, v14

    const/16 v16, 0x5

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    aput-wide v12, v8, v11

    .line 2465
    const/4 v11, 0x2

    const/4 v12, 0x6

    aget-wide v12, p0, v12

    const/4 v14, 0x6

    aget-wide v14, p0, v14

    mul-double/2addr v12, v14

    const/4 v14, 0x7

    aget-wide v14, p0, v14

    const/16 v16, 0x7

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    const/16 v14, 0x8

    aget-wide v14, p0, v14

    const/16 v16, 0x8

    aget-wide v16, p0, v16

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    aput-wide v12, v8, v11

    .line 2467
    const/4 v11, 0x0

    aget-wide v12, v8, v11

    const/4 v11, 0x1

    aget-wide v14, v8, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_d

    .line 2468
    const/4 v11, 0x0

    aget-wide v12, v8, v11

    const/4 v11, 0x2

    aget-wide v14, v8, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_c

    .line 2469
    const/4 v11, 0x2

    aget-wide v12, v8, v11

    const/4 v11, 0x1

    aget-wide v14, v8, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_b

    .line 2471
    const/4 v4, 0x0

    .local v4, "in0":I
    const/4 v6, 0x1

    .local v6, "in2":I
    const/4 v5, 0x2

    .line 2496
    .local v5, "in1":I
    :goto_3
    aget v7, v9, v4

    .line 2497
    .local v7, "index":I
    const/4 v11, 0x0

    aget-wide v12, p3, v7

    aput-wide v12, p5, v11

    .line 2499
    aget v7, v9, v5

    .line 2500
    const/4 v11, 0x1

    aget-wide v12, p3, v7

    aput-wide v12, p5, v11

    .line 2502
    aget v7, v9, v6

    .line 2503
    const/4 v11, 0x2

    aget-wide v12, p3, v7

    aput-wide v12, p5, v11

    .line 2506
    aget v7, v9, v4

    .line 2507
    const/4 v11, 0x0

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2509
    aget v11, v9, v4

    add-int/lit8 v7, v11, 0x3

    .line 2510
    const/4 v11, 0x3

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2512
    aget v11, v9, v4

    add-int/lit8 v7, v11, 0x6

    .line 2513
    const/4 v11, 0x6

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2515
    aget v7, v9, v5

    .line 2516
    const/4 v11, 0x1

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2518
    aget v11, v9, v5

    add-int/lit8 v7, v11, 0x3

    .line 2519
    const/4 v11, 0x4

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2521
    aget v11, v9, v5

    add-int/lit8 v7, v11, 0x6

    .line 2522
    const/4 v11, 0x7

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2524
    aget v7, v9, v6

    .line 2525
    const/4 v11, 0x2

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2527
    aget v11, v9, v6

    add-int/lit8 v7, v11, 0x3

    .line 2528
    const/4 v11, 0x5

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2530
    aget v11, v9, v6

    add-int/lit8 v7, v11, 0x6

    .line 2531
    const/16 v11, 0x8

    aget-wide v12, v10, v7

    aput-wide v12, p4, v11

    .line 2533
    .end local v4    # "in0":I
    .end local v5    # "in1":I
    .end local v6    # "in2":I
    .end local v7    # "index":I
    :cond_5
    return-void

    .line 2438
    :cond_6
    const/4 v11, 0x0

    const/4 v12, 0x0

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x2

    aput v12, v9, v11

    goto/16 :goto_2

    .line 2441
    :cond_7
    const/4 v11, 0x0

    const/4 v12, 0x2

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x0

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x1

    aput v12, v9, v11

    goto/16 :goto_2

    .line 2444
    :cond_8
    const/4 v11, 0x1

    aget-wide v12, p3, v11

    const/4 v11, 0x2

    aget-wide v14, p3, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_a

    .line 2445
    const/4 v11, 0x2

    aget-wide v12, p3, v11

    const/4 v11, 0x0

    aget-wide v14, p3, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_9

    .line 2446
    const/4 v11, 0x0

    const/4 v12, 0x1

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x2

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput v12, v9, v11

    goto/16 :goto_2

    .line 2448
    :cond_9
    const/4 v11, 0x0

    const/4 v12, 0x1

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x0

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x2

    aput v12, v9, v11

    goto/16 :goto_2

    .line 2451
    :cond_a
    const/4 v11, 0x0

    const/4 v12, 0x2

    aput v12, v9, v11

    const/4 v11, 0x1

    const/4 v12, 0x1

    aput v12, v9, v11

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput v12, v9, v11

    goto/16 :goto_2

    .line 2474
    :cond_b
    const/4 v4, 0x0

    .restart local v4    # "in0":I
    const/4 v5, 0x1

    .restart local v5    # "in1":I
    const/4 v6, 0x2

    .restart local v6    # "in2":I
    goto/16 :goto_3

    .line 2478
    .end local v4    # "in0":I
    .end local v5    # "in1":I
    .end local v6    # "in2":I
    :cond_c
    const/4 v6, 0x0

    .restart local v6    # "in2":I
    const/4 v4, 0x1

    .restart local v4    # "in0":I
    const/4 v5, 0x2

    .restart local v5    # "in1":I
    goto/16 :goto_3

    .line 2481
    .end local v4    # "in0":I
    .end local v5    # "in1":I
    .end local v6    # "in2":I
    :cond_d
    const/4 v11, 0x1

    aget-wide v12, v8, v11

    const/4 v11, 0x2

    aget-wide v14, v8, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_f

    .line 2482
    const/4 v11, 0x2

    aget-wide v12, v8, v11

    const/4 v11, 0x0

    aget-wide v14, v8, v11

    cmpl-double v11, v12, v14

    if-lez v11, :cond_e

    .line 2484
    const/4 v5, 0x0

    .restart local v5    # "in1":I
    const/4 v6, 0x1

    .restart local v6    # "in2":I
    const/4 v4, 0x2

    .restart local v4    # "in0":I
    goto/16 :goto_3

    .line 2487
    .end local v4    # "in0":I
    .end local v5    # "in1":I
    .end local v6    # "in2":I
    :cond_e
    const/4 v5, 0x0

    .restart local v5    # "in1":I
    const/4 v4, 0x1

    .restart local v4    # "in0":I
    const/4 v6, 0x2

    .restart local v6    # "in2":I
    goto/16 :goto_3

    .line 2491
    .end local v4    # "in0":I
    .end local v5    # "in1":I
    .end local v6    # "in2":I
    :cond_f
    const/4 v6, 0x0

    .restart local v6    # "in2":I
    const/4 v5, 0x1

    .restart local v5    # "in1":I
    const/4 v4, 0x2

    .restart local v4    # "in0":I
    goto/16 :goto_3
.end method

.method static transpose_mat([D[D)V
    .locals 7
    .param p0, "in"    # [D
    .param p1, "out"    # [D

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 3058
    aget-wide v0, p0, v2

    aput-wide v0, p1, v2

    .line 3059
    aget-wide v0, p0, v5

    aput-wide v0, p1, v3

    .line 3060
    const/4 v0, 0x6

    aget-wide v0, p0, v0

    aput-wide v0, p1, v4

    .line 3062
    aget-wide v0, p0, v3

    aput-wide v0, p1, v5

    .line 3063
    aget-wide v0, p0, v6

    aput-wide v0, p1, v6

    .line 3064
    const/4 v0, 0x5

    const/4 v1, 0x7

    aget-wide v2, p0, v1

    aput-wide v2, p1, v0

    .line 3066
    const/4 v0, 0x6

    aget-wide v2, p0, v4

    aput-wide v2, p1, v0

    .line 3067
    const/4 v0, 0x7

    const/4 v1, 0x5

    aget-wide v2, p0, v1

    aput-wide v2, p1, v0

    .line 3068
    const/16 v0, 0x8

    const/16 v1, 0x8

    aget-wide v2, p0, v1

    aput-wide v2, p1, v0

    .line 3069
    return-void
.end method


# virtual methods
.method public final add(D)V
    .locals 3
    .param p1, "scalar"    # D

    .prologue
    .line 705
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 706
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 707
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 709
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 710
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 711
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 713
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 714
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 715
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 717
    return-void
.end method

.method public final add(DLjavax/vecmath/Matrix3d;)V
    .locals 3
    .param p1, "scalar"    # D
    .param p3, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 727
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m00:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 728
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m01:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 729
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m02:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 731
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m10:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 732
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 733
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m12:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 735
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m20:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 736
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m21:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 737
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 738
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix3d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 766
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 767
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 768
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 770
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 771
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 772
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 774
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 775
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 776
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 777
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 747
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m00:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 748
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m01:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 749
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m02:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 751
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m10:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 752
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 753
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m12:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 755
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m20:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 756
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m21:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 757
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 758
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3113
    const/4 v1, 0x0

    .line 3115
    .local v1, "m1":Ljavax/vecmath/Matrix3d;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m1":Ljavax/vecmath/Matrix3d;
    check-cast v1, Ljavax/vecmath/Matrix3d;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3122
    .restart local v1    # "m1":Ljavax/vecmath/Matrix3d;
    return-object v1

    .line 3116
    .end local v1    # "m1":Ljavax/vecmath/Matrix3d;
    :catch_0
    move-exception v0

    .line 3118
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/InternalError;

    invoke-direct {v2}, Ljava/lang/InternalError;-><init>()V

    throw v2
.end method

.method public final determinant()D
    .locals 12

    .prologue
    .line 1366
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v10, p0, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v10, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    mul-double/2addr v4, v6

    add-double v0, v2, v4

    .line 1369
    .local v0, "total":D
    return-wide v0
.end method

.method public epsilonEquals(Ljavax/vecmath/Matrix3d;D)Z
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "epsilon"    # D

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 1951
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m00:D

    sub-double v0, v4, v6

    .line 1952
    .local v0, "diff":D
    cmpg-double v3, v0, v8

    if-gez v3, :cond_1

    neg-double v4, v0

    :goto_0
    cmpl-double v3, v4, p2

    if-lez v3, :cond_2

    .line 1978
    :cond_0
    :goto_1
    return v2

    :cond_1
    move-wide v4, v0

    .line 1952
    goto :goto_0

    .line 1954
    :cond_2
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double v0, v4, v6

    .line 1955
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1957
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    sub-double v0, v4, v6

    .line 1958
    cmpg-double v3, v0, v8

    if-gez v3, :cond_4

    neg-double v4, v0

    :goto_3
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1960
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m10:D

    sub-double v0, v4, v6

    .line 1961
    cmpg-double v3, v0, v8

    if-gez v3, :cond_5

    neg-double v4, v0

    :goto_4
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1963
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    sub-double v0, v4, v6

    .line 1964
    cmpg-double v3, v0, v8

    if-gez v3, :cond_6

    neg-double v4, v0

    :goto_5
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1966
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double v0, v4, v6

    .line 1967
    cmpg-double v3, v0, v8

    if-gez v3, :cond_7

    neg-double v4, v0

    :goto_6
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1969
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double v0, v4, v6

    .line 1970
    cmpg-double v3, v0, v8

    if-gez v3, :cond_8

    neg-double v4, v0

    :goto_7
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1972
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    sub-double v0, v4, v6

    .line 1973
    cmpg-double v3, v0, v8

    if-gez v3, :cond_9

    neg-double v4, v0

    :goto_8
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1975
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    sub-double v0, v4, v6

    .line 1976
    cmpg-double v3, v0, v8

    if-gez v3, :cond_a

    neg-double v4, v0

    :goto_9
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 1978
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    move-wide v4, v0

    .line 1955
    goto :goto_2

    :cond_4
    move-wide v4, v0

    .line 1958
    goto :goto_3

    :cond_5
    move-wide v4, v0

    .line 1961
    goto :goto_4

    :cond_6
    move-wide v4, v0

    .line 1964
    goto :goto_5

    :cond_7
    move-wide v4, v0

    .line 1967
    goto :goto_6

    :cond_8
    move-wide v4, v0

    .line 1970
    goto :goto_7

    :cond_9
    move-wide v4, v0

    .line 1973
    goto :goto_8

    :cond_a
    move-wide v4, v0

    .line 1976
    goto :goto_9
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 1928
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Matrix3d;

    move-object v4, v0

    .line 1929
    .local v4, "m2":Ljavax/vecmath/Matrix3d;
    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m00:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m01:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m02:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m10:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m11:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m12:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m20:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m21:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix3d;->m22:D
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 1934
    .end local v4    # "m2":Ljavax/vecmath/Matrix3d;
    :cond_0
    :goto_0
    return v5

    .line 1933
    :catch_0
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 1934
    .end local v2    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Matrix3d;)Z
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v1, 0x0

    .line 1910
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m02:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m12:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m21:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m22:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1914
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final getColumn(ILjavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 447
    if-nez p1, :cond_0

    .line 448
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 449
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 450
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    .line 463
    :goto_0
    return-void

    .line 451
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 452
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 453
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 454
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    goto :goto_0

    .line 455
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 456
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 457
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 458
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    goto :goto_0

    .line 460
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getColumn(I[D)V
    .locals 5
    .param p1, "column"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 472
    if-nez p1, :cond_0

    .line 473
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v0, p2, v4

    .line 474
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v0, p2, v2

    .line 475
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v0, p2, v3

    .line 488
    :goto_0
    return-void

    .line 476
    :cond_0
    if-ne p1, v2, :cond_1

    .line 477
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v0, p2, v4

    .line 478
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v0, p2, v2

    .line 479
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v0, p2, v3

    goto :goto_0

    .line 480
    :cond_1
    if-ne p1, v3, :cond_2

    .line 481
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v0, p2, v4

    .line 482
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v0, p2, v2

    .line 483
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v0, p2, v3

    goto :goto_0

    .line 485
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getElement(II)D
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 342
    packed-switch p1, :pswitch_data_0

    .line 389
    :goto_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d1"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 348
    :pswitch_1
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 379
    :goto_1
    return-wide v0

    .line 350
    :pswitch_2
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    goto :goto_1

    .line 352
    :pswitch_3
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    goto :goto_1

    .line 358
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 361
    :pswitch_5
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    goto :goto_1

    .line 363
    :pswitch_6
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    goto :goto_1

    .line 365
    :pswitch_7
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    goto :goto_1

    .line 372
    :pswitch_8
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 375
    :pswitch_9
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    goto :goto_1

    .line 377
    :pswitch_a
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    goto :goto_1

    .line 379
    :pswitch_b
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_1

    .line 342
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch

    .line 345
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 358
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 372
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final getM00()D
    .locals 2

    .prologue
    .line 3131
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    return-wide v0
.end method

.method public final getM01()D
    .locals 2

    .prologue
    .line 3153
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    return-wide v0
.end method

.method public final getM02()D
    .locals 2

    .prologue
    .line 3175
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    return-wide v0
.end method

.method public final getM10()D
    .locals 2

    .prologue
    .line 3197
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    return-wide v0
.end method

.method public final getM11()D
    .locals 2

    .prologue
    .line 3219
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    return-wide v0
.end method

.method public final getM12()D
    .locals 2

    .prologue
    .line 3241
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    return-wide v0
.end method

.method public final getM20()D
    .locals 2

    .prologue
    .line 3263
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    return-wide v0
.end method

.method public final getM21()D
    .locals 2

    .prologue
    .line 3285
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    return-wide v0
.end method

.method public final getM22()D
    .locals 2

    .prologue
    .line 3307
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    return-wide v0
.end method

.method public final getRow(ILjavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 398
    if-nez p1, :cond_0

    .line 399
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 400
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 401
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    .line 414
    :goto_0
    return-void

    .line 402
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 403
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 404
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 405
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    goto :goto_0

    .line 406
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 407
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 408
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 409
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    goto :goto_0

    .line 411
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRow(I[D)V
    .locals 5
    .param p1, "row"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 422
    if-nez p1, :cond_0

    .line 423
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v0, p2, v4

    .line 424
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v0, p2, v2

    .line 425
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v0, p2, v3

    .line 438
    :goto_0
    return-void

    .line 426
    :cond_0
    if-ne p1, v2, :cond_1

    .line 427
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v0, p2, v4

    .line 428
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v0, p2, v2

    .line 429
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v0, p2, v3

    goto :goto_0

    .line 430
    :cond_1
    if-ne p1, v3, :cond_2

    .line 431
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v0, p2, v4

    .line 432
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v0, p2, v2

    .line 433
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v0, p2, v3

    goto :goto_0

    .line 435
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getScale()D
    .locals 4

    .prologue
    .line 691
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 692
    .local v1, "tmp_scale":[D
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 693
    .local v0, "tmp_rot":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3d;->getScaleRotate([D[D)V

    .line 695
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    return-wide v2
.end method

.method final getScaleRotate([D[D)V
    .locals 4
    .param p1, "scales"    # [D
    .param p2, "rots"    # [D

    .prologue
    .line 2097
    const/16 v1, 0x9

    new-array v0, v1, [D

    .line 2099
    .local v0, "tmp":[D
    const/4 v1, 0x0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v2, v0, v1

    .line 2100
    const/4 v1, 0x1

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v2, v0, v1

    .line 2101
    const/4 v1, 0x2

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v2, v0, v1

    .line 2103
    const/4 v1, 0x3

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v2, v0, v1

    .line 2104
    const/4 v1, 0x4

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v2, v0, v1

    .line 2105
    const/4 v1, 0x5

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v2, v0, v1

    .line 2107
    const/4 v1, 0x6

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v2, v0, v1

    .line 2108
    const/4 v1, 0x7

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v2, v0, v1

    .line 2109
    const/16 v1, 0x8

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v2, v0, v1

    .line 2110
    invoke-static {v0, p1, p2}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 2112
    return-void
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 1991
    const-wide/16 v0, 0x1

    .line 1992
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1993
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1994
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1995
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1996
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1997
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1998
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 1999
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 2000
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 2001
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final invert()V
    .locals 0

    .prologue
    .line 1075
    invoke-direct {p0, p0}, Ljavax/vecmath/Matrix3d;->invertGeneral(Ljavax/vecmath/Matrix3d;)V

    .line 1076
    return-void
.end method

.method public final invert(Ljavax/vecmath/Matrix3d;)V
    .locals 0
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1067
    invoke-direct {p0, p1}, Ljavax/vecmath/Matrix3d;->invertGeneral(Ljavax/vecmath/Matrix3d;)V

    .line 1068
    return-void
.end method

.method public final mul(D)V
    .locals 3
    .param p1, "scalar"    # D

    .prologue
    .line 1473
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1474
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1475
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1477
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1478
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1479
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1481
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1482
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1483
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1485
    return-void
.end method

.method public final mul(DLjavax/vecmath/Matrix3d;)V
    .locals 3
    .param p1, "scalar"    # D
    .param p3, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1495
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1496
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1497
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1499
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1500
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1501
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1503
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1504
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1505
    iget-wide v0, p3, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1507
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix3d;)V
    .locals 28
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1520
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v4, v22, v24

    .line 1521
    .local v4, "m00":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v6, v22, v24

    .line 1522
    .local v6, "m01":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v8, v22, v24

    .line 1524
    .local v8, "m02":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v10, v22, v24

    .line 1525
    .local v10, "m10":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v12, v22, v24

    .line 1526
    .local v12, "m11":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v14, v22, v24

    .line 1528
    .local v14, "m12":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v16, v22, v24

    .line 1529
    .local v16, "m20":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v18, v22, v24

    .line 1530
    .local v18, "m21":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v20, v22, v24

    .line 1532
    .local v20, "m22":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1533
    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1534
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1535
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 28
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1545
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 1546
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1547
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1548
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1550
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1551
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1552
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1554
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1555
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1556
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1578
    :goto_0
    return-void

    .line 1562
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v4, v22, v24

    .line 1563
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v6, v22, v24

    .line 1564
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v8, v22, v24

    .line 1566
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v10, v22, v24

    .line 1567
    .local v10, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v12, v22, v24

    .line 1568
    .local v12, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v14, v22, v24

    .line 1570
    .local v14, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v16, v22, v24

    .line 1571
    .local v16, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v18, v22, v24

    .line 1572
    .local v18, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v20, v22, v24

    .line 1574
    .local v20, "m22":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1575
    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1576
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final mulNormalize(Ljavax/vecmath/Matrix3d;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1588
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1589
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1590
    .local v1, "tmp_rot":[D
    const/4 v3, 0x3

    new-array v2, v3, [D

    .line 1592
    .local v2, "tmp_scale":[D
    const/4 v3, 0x0

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1593
    const/4 v3, 0x1

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1594
    const/4 v3, 0x2

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1596
    const/4 v3, 0x3

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1597
    const/4 v3, 0x4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1598
    const/4 v3, 0x5

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1600
    const/4 v3, 0x6

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1601
    const/4 v3, 0x7

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1602
    const/16 v3, 0x8

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1604
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1606
    const/4 v3, 0x0

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1607
    const/4 v3, 0x1

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1608
    const/4 v3, 0x2

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1610
    const/4 v3, 0x3

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1611
    const/4 v3, 0x4

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1612
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1614
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1615
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1616
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1618
    return-void
.end method

.method public final mulNormalize(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1630
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1631
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1632
    .local v1, "tmp_rot":[D
    const/4 v3, 0x3

    new-array v2, v3, [D

    .line 1634
    .local v2, "tmp_scale":[D
    const/4 v3, 0x0

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1635
    const/4 v3, 0x1

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1636
    const/4 v3, 0x2

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1638
    const/4 v3, 0x3

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1639
    const/4 v3, 0x4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1640
    const/4 v3, 0x5

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1642
    const/4 v3, 0x6

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1643
    const/4 v3, 0x7

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1644
    const/16 v3, 0x8

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p2, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p2, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v0, v3

    .line 1646
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1648
    const/4 v3, 0x0

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1649
    const/4 v3, 0x1

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1650
    const/4 v3, 0x2

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1652
    const/4 v3, 0x3

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1653
    const/4 v3, 0x4

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1654
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1656
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1657
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1658
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1660
    return-void
.end method

.method public final mulTransposeBoth(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 28
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1670
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 1671
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1672
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1673
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1675
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1676
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1677
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1679
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1680
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1681
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1704
    :goto_0
    return-void

    .line 1687
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v4, v22, v24

    .line 1688
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v6, v22, v24

    .line 1689
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v8, v22, v24

    .line 1691
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v10, v22, v24

    .line 1692
    .local v10, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v12, v22, v24

    .line 1693
    .local v12, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v14, v22, v24

    .line 1695
    .local v14, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v16, v22, v24

    .line 1696
    .local v16, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v18, v22, v24

    .line 1697
    .local v18, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v20, v22, v24

    .line 1699
    .local v20, "m22":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1700
    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1701
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final mulTransposeLeft(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 28
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1757
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 1758
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1759
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1760
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1762
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1763
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1764
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1766
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1767
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1768
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1790
    :goto_0
    return-void

    .line 1774
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v4, v22, v24

    .line 1775
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v6, v22, v24

    .line 1776
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v8, v22, v24

    .line 1778
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v10, v22, v24

    .line 1779
    .local v10, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v12, v22, v24

    .line 1780
    .local v12, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v14, v22, v24

    .line 1782
    .local v14, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v16, v22, v24

    .line 1783
    .local v16, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v18, v22, v24

    .line 1784
    .local v18, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v20, v22, v24

    .line 1786
    .local v20, "m22":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1787
    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1788
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final mulTransposeRight(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 28
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1714
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 1715
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1716
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1717
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1719
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1720
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1721
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1723
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1724
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1725
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1747
    :goto_0
    return-void

    .line 1731
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v4, v22, v24

    .line 1732
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v6, v22, v24

    .line 1733
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v8, v22, v24

    .line 1735
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v10, v22, v24

    .line 1736
    .local v10, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v12, v22, v24

    .line 1737
    .local v12, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v14, v22, v24

    .line 1739
    .local v14, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m02:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v16, v22, v24

    .line 1740
    .local v16, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m12:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v18, v22, v24

    .line 1741
    .local v18, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v20, v22, v24

    .line 1743
    .local v20, "m22":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix3d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix3d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1744
    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix3d;->m10:D

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix3d;->m11:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1745
    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final negate()V
    .locals 2

    .prologue
    .line 2029
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 2030
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 2031
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 2033
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 2034
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 2035
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 2037
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 2038
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 2039
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 2041
    return-void
.end method

.method public final negate(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 2050
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 2051
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 2052
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 2054
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 2055
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 2056
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 2058
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 2059
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 2060
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 2062
    return-void
.end method

.method public final normalize()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 1798
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 1799
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 1801
    .local v1, "tmp_scale":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3d;->getScaleRotate([D[D)V

    .line 1803
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1804
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1805
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1807
    aget-wide v2, v0, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1808
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1809
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1811
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1812
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1813
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1815
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Matrix3d;)V
    .locals 11
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 1825
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1826
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1827
    .local v1, "tmp_rot":[D
    new-array v2, v6, [D

    .line 1829
    .local v2, "tmp_scale":[D
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v4, v0, v7

    .line 1830
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v4, v0, v8

    .line 1831
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v4, v0, v9

    .line 1833
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v4, v0, v6

    .line 1834
    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v4, v0, v10

    .line 1835
    const/4 v3, 0x5

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v4, v0, v3

    .line 1837
    const/4 v3, 0x6

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v4, v0, v3

    .line 1838
    const/4 v3, 0x7

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v4, v0, v3

    .line 1839
    const/16 v3, 0x8

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v4, v0, v3

    .line 1841
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1843
    aget-wide v4, v1, v7

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1844
    aget-wide v4, v1, v8

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1845
    aget-wide v4, v1, v9

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1847
    aget-wide v4, v1, v6

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1848
    aget-wide v4, v1, v10

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1849
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1851
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1852
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1853
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1854
    return-void
.end method

.method public final normalizeCP()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1863
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double v0, v8, v2

    .line 1864
    .local v0, "mag":D
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1865
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1866
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1868
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double v0, v8, v2

    .line 1869
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1870
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1871
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1873
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1874
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1875
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1876
    return-void
.end method

.method public final normalizeCP(Ljavax/vecmath/Matrix3d;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1886
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double v0, v8, v2

    .line 1887
    .local v0, "mag":D
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1888
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1889
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1891
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    div-double v0, v8, v2

    .line 1892
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1893
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1894
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1896
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1897
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1898
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1899
    return-void
.end method

.method public final rotX(D)V
    .locals 9
    .param p1, "angle"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 1401
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 1402
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 1404
    .local v0, "cosAngle":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1405
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1406
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1408
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1409
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1410
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1412
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1413
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1414
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1415
    return-void
.end method

.method public final rotY(D)V
    .locals 9
    .param p1, "angle"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 1426
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 1427
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 1429
    .local v0, "cosAngle":D
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1430
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1431
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1433
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1434
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1435
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1437
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1438
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1439
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1440
    return-void
.end method

.method public final rotZ(D)V
    .locals 9
    .param p1, "angle"    # D

    .prologue
    const-wide/16 v6, 0x0

    .line 1451
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 1452
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 1454
    .local v0, "cosAngle":D
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1455
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1456
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1458
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1459
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1460
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1462
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1463
    iput-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1464
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1465
    return-void
.end method

.method public final set(D)V
    .locals 3
    .param p1, "scale"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 1379
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1380
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1381
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1383
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1384
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1385
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1387
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1388
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1389
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1390
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 30
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 889
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v28, v0

    mul-double v26, v26, v28

    add-double v24, v24, v26

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v28, v0

    mul-double v26, v26, v28

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 891
    .local v12, "mag":D
    const-wide v24, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v24, v12, v24

    if-gez v24, :cond_0

    .line 892
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 893
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 894
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 896
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 897
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 898
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 900
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 901
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 902
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 929
    :goto_0
    return-void

    .line 904
    :cond_0
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    div-double v12, v24, v12

    .line 905
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v24, v0

    mul-double v4, v24, v12

    .line 906
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v24, v0

    mul-double v6, v24, v12

    .line 907
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v24, v0

    mul-double v8, v24, v12

    .line 909
    .local v8, "az":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 910
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 911
    .local v10, "cosTheta":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    sub-double v16, v24, v10

    .line 913
    .local v16, "t":D
    mul-double v20, v4, v8

    .line 914
    .local v20, "xz":D
    mul-double v18, v4, v6

    .line 915
    .local v18, "xy":D
    mul-double v22, v6, v8

    .line 917
    .local v22, "yz":D
    mul-double v24, v16, v4

    mul-double v24, v24, v4

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 918
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 919
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 921
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 922
    mul-double v24, v16, v6

    mul-double v24, v24, v6

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 923
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 925
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 926
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 927
    mul-double v24, v16, v8

    mul-double v24, v24, v8

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 28
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 958
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 959
    .local v12, "mag":D
    const-wide v24, 0x3c9fffffffb29c00L    # 1.110223024E-16

    cmpg-double v24, v12, v24

    if-gez v24, :cond_0

    .line 960
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 961
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 962
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 964
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 965
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 966
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 968
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 969
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 970
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    .line 996
    :goto_0
    return-void

    .line 972
    :cond_0
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    div-double v12, v24, v12

    .line 973
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v4, v24, v12

    .line 974
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v6, v24, v12

    .line 975
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v8, v24, v12

    .line 976
    .local v8, "az":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 977
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 978
    .local v10, "cosTheta":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    sub-double v16, v24, v10

    .line 980
    .local v16, "t":D
    mul-double v20, v4, v8

    .line 981
    .local v20, "xz":D
    mul-double v18, v4, v6

    .line 982
    .local v18, "xy":D
    mul-double v22, v6, v8

    .line 984
    .local v22, "yz":D
    mul-double v24, v16, v4

    mul-double v24, v24, v4

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m00:D

    .line 985
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m01:D

    .line 986
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m02:D

    .line 988
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m10:D

    .line 989
    mul-double v24, v16, v6

    mul-double v24, v24, v6

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m11:D

    .line 990
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m12:D

    .line 992
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m20:D

    .line 993
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m21:D

    .line 994
    mul-double v24, v16, v8

    mul-double v24, v24, v8

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix3d;->m22:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1025
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1026
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1027
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1029
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1030
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1031
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1033
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1034
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1035
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1036
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1005
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1006
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1007
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1009
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1010
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1011
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1013
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1014
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1015
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1016
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 869
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 870
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 871
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 873
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 874
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 875
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 877
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 878
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 879
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 880
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 938
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 939
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 940
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 942
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 943
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 944
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 946
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 947
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 948
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 949
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "m"    # [D

    .prologue
    .line 1046
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 1047
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 1048
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1050
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 1051
    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 1052
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1054
    const/4 v0, 0x6

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 1055
    const/4 v0, 0x7

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 1056
    const/16 v0, 0x8

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1058
    return-void
.end method

.method public final setColumn(IDDD)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "x"    # D
    .param p4, "y"    # D
    .param p6, "z"    # D

    .prologue
    .line 595
    packed-switch p1, :pswitch_data_0

    .line 615
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 597
    :pswitch_0
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 598
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 599
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 617
    :goto_0
    return-void

    .line 603
    :pswitch_1
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 604
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 605
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    goto :goto_0

    .line 609
    :pswitch_2
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 610
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 611
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setColumn(ILjavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 626
    packed-switch p1, :pswitch_data_0

    .line 646
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 628
    :pswitch_0
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 629
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 630
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 648
    :goto_0
    return-void

    .line 634
    :pswitch_1
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 635
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 636
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    goto :goto_0

    .line 640
    :pswitch_2
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 641
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 642
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 626
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setColumn(I[D)V
    .locals 4
    .param p1, "column"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 657
    packed-switch p1, :pswitch_data_0

    .line 677
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 659
    :pswitch_0
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 660
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 661
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 679
    :goto_0
    return-void

    .line 665
    :pswitch_1
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 666
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 667
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    goto :goto_0

    .line 671
    :pswitch_2
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 672
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 673
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 657
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setElement(IID)V
    .locals 3
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "value"    # D

    .prologue
    .line 274
    packed-switch p1, :pswitch_data_0

    .line 329
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 277
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 289
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :pswitch_1
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 331
    :goto_0
    return-void

    .line 283
    :pswitch_2
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m01:D

    goto :goto_0

    .line 286
    :pswitch_3
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m02:D

    goto :goto_0

    .line 294
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    .line 306
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :pswitch_5
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m10:D

    goto :goto_0

    .line 300
    :pswitch_6
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m11:D

    goto :goto_0

    .line 303
    :pswitch_7
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m12:D

    goto :goto_0

    .line 312
    :pswitch_8
    packed-switch p2, :pswitch_data_3

    .line 324
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 315
    :pswitch_9
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m20:D

    goto :goto_0

    .line 318
    :pswitch_a
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m21:D

    goto :goto_0

    .line 321
    :pswitch_b
    iput-wide p3, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 274
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch

    .line 277
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 294
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 312
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final setIdentity()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 226
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 227
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 228
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 230
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 231
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 232
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 234
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 235
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 236
    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 237
    return-void
.end method

.method public final setM00(D)V
    .locals 1
    .param p1, "m00"    # D

    .prologue
    .line 3142
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 3143
    return-void
.end method

.method public final setM01(D)V
    .locals 1
    .param p1, "m01"    # D

    .prologue
    .line 3164
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 3165
    return-void
.end method

.method public final setM02(D)V
    .locals 1
    .param p1, "m02"    # D

    .prologue
    .line 3186
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 3187
    return-void
.end method

.method public final setM10(D)V
    .locals 1
    .param p1, "m10"    # D

    .prologue
    .line 3208
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 3209
    return-void
.end method

.method public final setM11(D)V
    .locals 1
    .param p1, "m11"    # D

    .prologue
    .line 3230
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 3231
    return-void
.end method

.method public final setM12(D)V
    .locals 1
    .param p1, "m12"    # D

    .prologue
    .line 3252
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 3253
    return-void
.end method

.method public final setM20(D)V
    .locals 1
    .param p1, "m20"    # D

    .prologue
    .line 3274
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 3275
    return-void
.end method

.method public final setM21(D)V
    .locals 1
    .param p1, "m21"    # D

    .prologue
    .line 3296
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 3297
    return-void
.end method

.method public final setM22(D)V
    .locals 1
    .param p1, "m22"    # D

    .prologue
    .line 3318
    iput-wide p1, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 3319
    return-void
.end method

.method public final setRow(IDDD)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "x"    # D
    .param p4, "y"    # D
    .param p6, "z"    # D

    .prologue
    .line 500
    packed-switch p1, :pswitch_data_0

    .line 520
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :pswitch_0
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 503
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 504
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 522
    :goto_0
    return-void

    .line 508
    :pswitch_1
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 509
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 510
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    goto :goto_0

    .line 514
    :pswitch_2
    iput-wide p2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 515
    iput-wide p4, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 516
    iput-wide p6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 500
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setRow(ILjavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 531
    packed-switch p1, :pswitch_data_0

    .line 551
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :pswitch_0
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 534
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 535
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 553
    :goto_0
    return-void

    .line 539
    :pswitch_1
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 540
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 541
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    goto :goto_0

    .line 545
    :pswitch_2
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 546
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 547
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 531
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setRow(I[D)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 562
    packed-switch p1, :pswitch_data_0

    .line 582
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3d6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 564
    :pswitch_0
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 565
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 566
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 584
    :goto_0
    return-void

    .line 570
    :pswitch_1
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 571
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 572
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    goto :goto_0

    .line 576
    :pswitch_2
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 577
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 578
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0

    .line 562
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setScale(D)V
    .locals 5
    .param p1, "scale"    # D

    .prologue
    const/4 v4, 0x3

    .line 248
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 249
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 251
    .local v1, "tmp_scale":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3d;->getScaleRotate([D[D)V

    .line 253
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 254
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 255
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 257
    aget-wide v2, v0, v4

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 258
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 259
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 261
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 262
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 263
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 264
    return-void
.end method

.method public final setZero()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 2010
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 2011
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 2012
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 2014
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 2015
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 2016
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 2018
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 2019
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 2020
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 2022
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix3d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 807
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 808
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 809
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 811
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 812
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 813
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 815
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 816
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 817
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 818
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Matrix3d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 787
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m00:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 788
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 789
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m02:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 791
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m10:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 792
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m11:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 793
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 795
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 796
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m21:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 797
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix3d;->m22:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 798
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m00:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m11:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m22:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljavax/vecmath/Tuple3d;)V
    .locals 12
    .param p1, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 2071
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v8, v10

    add-double v2, v0, v8

    .line 2072
    .local v2, "x":D
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v8, v10

    add-double v4, v0, v8

    .line 2073
    .local v4, "y":D
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v0, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v8, v10

    add-double v6, v0, v8

    .local v6, "z":D
    move-object v1, p1

    .line 2074
    invoke-virtual/range {v1 .. v7}, Ljavax/vecmath/Tuple3d;->set(DDD)V

    .line 2075
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;)V
    .locals 10
    .param p1, "t"    # Ljavax/vecmath/Tuple3d;
    .param p2, "result"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 2085
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v6, v8

    add-double v0, v4, v6

    .line 2086
    .local v0, "x":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 2087
    .local v2, "y":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix3d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iput-wide v4, p2, Ljavax/vecmath/Tuple3d;->z:D

    .line 2088
    iput-wide v0, p2, Ljavax/vecmath/Tuple3d;->x:D

    .line 2089
    iput-wide v2, p2, Ljavax/vecmath/Tuple3d;->y:D

    .line 2090
    return-void
.end method

.method public final transpose()V
    .locals 4

    .prologue
    .line 827
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 828
    .local v0, "temp":D
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 829
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 831
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 832
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 833
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 835
    iget-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 836
    iget-wide v2, p0, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 837
    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 838
    return-void
.end method

.method public final transpose(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 846
    if-eq p0, p1, :cond_0

    .line 847
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m00:D

    .line 848
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m01:D

    .line 849
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m02:D

    .line 851
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m10:D

    .line 852
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m11:D

    .line 853
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m12:D

    .line 855
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m20:D

    .line 856
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m21:D

    .line 857
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix3d;->m22:D

    .line 860
    :goto_0
    return-void

    .line 859
    :cond_0
    invoke-virtual {p0}, Ljavax/vecmath/Matrix3d;->transpose()V

    goto :goto_0
.end method
