.class Ljavax/vecmath/VecMathUtil;
.super Ljava/lang/Object;
.source "VecMathUtil.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    return-void
.end method

.method static doubleToLongBits(D)J
    .locals 2
    .param p0, "d"    # D

    .prologue
    .line 89
    const-wide/16 v0, 0x0

    cmpl-double v0, p0, v0

    if-nez v0, :cond_0

    .line 90
    const-wide/16 v0, 0x0

    .line 93
    :goto_0
    return-wide v0

    :cond_0
    invoke-static {p0, p1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    goto :goto_0
.end method

.method static floatToIntBits(F)I
    .locals 1
    .param p0, "f"    # F

    .prologue
    .line 60
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 64
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    goto :goto_0
.end method
