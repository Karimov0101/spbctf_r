.class public Ljavax/vecmath/Vector4f;
.super Ljavax/vecmath/Tuple4f;
.source "Vector4f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x796bd41865b19413L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 129
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 119
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    iget v2, p1, Ljavax/vecmath/Tuple3f;->z:F

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 120
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector4d;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector4f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 76
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>([F)V

    .line 66
    return-void
.end method


# virtual methods
.method public final angle(Ljavax/vecmath/Vector4f;)F
    .locals 5
    .param p1, "v1"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Ljavax/vecmath/Vector4f;->dot(Ljavax/vecmath/Vector4f;)F

    move-result v2

    invoke-virtual {p0}, Ljavax/vecmath/Vector4f;->length()F

    move-result v3

    invoke-virtual {p1}, Ljavax/vecmath/Vector4f;->length()F

    move-result v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-double v0, v2

    .line 223
    .local v0, "vDot":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 224
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 225
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public final dot(Ljavax/vecmath/Vector4f;)F
    .locals 3
    .param p1, "v1"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 176
    iget v0, p0, Ljavax/vecmath/Vector4f;->x:F

    iget v1, p1, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->y:F

    iget v2, p1, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->z:F

    iget v2, p1, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->w:F

    iget v2, p1, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final length()F
    .locals 3

    .prologue
    .line 154
    iget v0, p0, Ljavax/vecmath/Vector4f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->z:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->w:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    .line 155
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final lengthSquared()F
    .locals 3

    .prologue
    .line 165
    iget v0, p0, Ljavax/vecmath/Vector4f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->z:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector4f;->w:F

    iget v2, p0, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final normalize()V
    .locals 6

    .prologue
    .line 204
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p0, Ljavax/vecmath/Vector4f;->x:F

    iget v4, p0, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector4f;->y:F

    iget v5, p0, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector4f;->z:F

    iget v5, p0, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector4f;->w:F

    iget v5, p0, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 206
    .local v0, "norm":F
    iget v1, p0, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->x:F

    .line 207
    iget v1, p0, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->y:F

    .line 208
    iget v1, p0, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->z:F

    .line 209
    iget v1, p0, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->w:F

    .line 210
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Vector4f;)V
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 188
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p1, Ljavax/vecmath/Vector4f;->x:F

    iget v4, p1, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector4f;->y:F

    iget v5, p1, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector4f;->z:F

    iget v5, p1, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector4f;->w:F

    iget v5, p1, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 190
    .local v0, "norm":F
    iget v1, p1, Ljavax/vecmath/Vector4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->x:F

    .line 191
    iget v1, p1, Ljavax/vecmath/Vector4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->y:F

    .line 192
    iget v1, p1, Ljavax/vecmath/Vector4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->z:F

    .line 193
    iget v1, p1, Ljavax/vecmath/Vector4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector4f;->w:F

    .line 194
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 141
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Vector4f;->x:F

    .line 142
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Vector4f;->y:F

    .line 143
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Vector4f;->z:F

    .line 144
    const/4 v0, 0x0

    iput v0, p0, Ljavax/vecmath/Vector4f;->w:F

    .line 145
    return-void
.end method
