.class public Ljavax/vecmath/Vector3f;
.super Ljavax/vecmath/Tuple3f;
.source "Vector3f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x61966f242321b146L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljavax/vecmath/Tuple3f;-><init>()V

    .line 113
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3f;-><init>(FFF)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 95
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector3d;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector3f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 76
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>([F)V

    .line 66
    return-void
.end method


# virtual methods
.method public final angle(Ljavax/vecmath/Vector3f;)F
    .locals 5
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 199
    invoke-virtual {p0, p1}, Ljavax/vecmath/Vector3f;->dot(Ljavax/vecmath/Vector3f;)F

    move-result v2

    invoke-virtual {p0}, Ljavax/vecmath/Vector3f;->length()F

    move-result v3

    invoke-virtual {p1}, Ljavax/vecmath/Vector3f;->length()F

    move-result v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-double v0, v2

    .line 200
    .local v0, "vDot":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 201
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 202
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public final cross(Ljavax/vecmath/Vector3f;Ljavax/vecmath/Vector3f;)V
    .locals 5
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;
    .param p2, "v2"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 145
    iget v2, p1, Ljavax/vecmath/Vector3f;->y:F

    iget v3, p2, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Vector3f;->z:F

    iget v4, p2, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    sub-float v0, v2, v3

    .line 146
    .local v0, "x":F
    iget v2, p2, Ljavax/vecmath/Vector3f;->x:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v2, v3

    iget v3, p2, Ljavax/vecmath/Vector3f;->z:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v3, v4

    sub-float v1, v2, v3

    .line 147
    .local v1, "y":F
    iget v2, p1, Ljavax/vecmath/Vector3f;->x:F

    iget v3, p2, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Vector3f;->y:F

    iget v4, p2, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iput v2, p0, Ljavax/vecmath/Vector3f;->z:F

    .line 148
    iput v0, p0, Ljavax/vecmath/Vector3f;->x:F

    .line 149
    iput v1, p0, Ljavax/vecmath/Vector3f;->y:F

    .line 150
    return-void
.end method

.method public final dot(Ljavax/vecmath/Vector3f;)F
    .locals 3
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 159
    iget v0, p0, Ljavax/vecmath/Vector3f;->x:F

    iget v1, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->y:F

    iget v2, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->z:F

    iget v2, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final length()F
    .locals 3

    .prologue
    .line 131
    iget v0, p0, Ljavax/vecmath/Vector3f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->z:F

    iget v2, p0, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    .line 132
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final lengthSquared()F
    .locals 3

    .prologue
    .line 122
    iget v0, p0, Ljavax/vecmath/Vector3f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector3f;->z:F

    iget v2, p0, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final normalize()V
    .locals 6

    .prologue
    .line 183
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p0, Ljavax/vecmath/Vector3f;->x:F

    iget v4, p0, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector3f;->y:F

    iget v5, p0, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector3f;->z:F

    iget v5, p0, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    .line 184
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 185
    .local v0, "norm":F
    iget v1, p0, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->x:F

    .line 186
    iget v1, p0, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->y:F

    .line 187
    iget v1, p0, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->z:F

    .line 188
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Vector3f;)V
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 170
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p1, Ljavax/vecmath/Vector3f;->x:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    iget v5, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    iget v5, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 171
    .local v0, "norm":F
    iget v1, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->x:F

    .line 172
    iget v1, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->y:F

    .line 173
    iget v1, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector3f;->z:F

    .line 174
    return-void
.end method
