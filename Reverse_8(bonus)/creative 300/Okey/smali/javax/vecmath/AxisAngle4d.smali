.class public Ljavax/vecmath/AxisAngle4d;
.super Ljava/lang/Object;
.source "AxisAngle4d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final EPS:D = 1.0E-12

.field static final serialVersionUID:J = 0x32932416f31765edL


# instance fields
.field public angle:D

.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 147
    iput-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 148
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 149
    iput-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 150
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "angle"    # D

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 80
    iput-wide p3, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 81
    iput-wide p5, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 82
    iput-wide p7, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 83
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/AxisAngle4d;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 104
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 105
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 106
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 107
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 108
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/AxisAngle4f;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 119
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 120
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 121
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 122
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector3d;D)V
    .locals 2
    .param p1, "axis"    # Ljavax/vecmath/Vector3d;
    .param p2, "angle"    # D

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 135
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 136
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 137
    iput-wide p2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 138
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "a"    # [D

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 94
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 95
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 96
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 97
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 547
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 548
    :catch_0
    move-exception v0

    .line 550
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/AxisAngle4d;D)Z
    .locals 10
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;
    .param p2, "epsilon"    # D

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 503
    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    sub-double v0, v4, v6

    .line 504
    .local v0, "diff":D
    cmpg-double v3, v0, v8

    if-gez v3, :cond_1

    neg-double v4, v0

    :goto_0
    cmpl-double v3, v4, p2

    if-lez v3, :cond_2

    .line 515
    :cond_0
    :goto_1
    return v2

    :cond_1
    move-wide v4, v0

    .line 504
    goto :goto_0

    .line 506
    :cond_2
    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    sub-double v0, v4, v6

    .line 507
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 509
    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    sub-double v0, v4, v6

    .line 510
    cmpg-double v3, v0, v8

    if-gez v3, :cond_4

    neg-double v4, v0

    :goto_3
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 512
    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    sub-double v0, v4, v6

    .line 513
    cmpg-double v3, v0, v8

    if-gez v3, :cond_5

    neg-double v4, v0

    :goto_4
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 515
    const/4 v2, 0x1

    goto :goto_1

    :cond_3
    move-wide v4, v0

    .line 507
    goto :goto_2

    :cond_4
    move-wide v4, v0

    .line 510
    goto :goto_3

    :cond_5
    move-wide v4, v0

    .line 513
    goto :goto_4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "o1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 480
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/AxisAngle4d;

    move-object v2, v0

    .line 481
    .local v2, "a2":Ljavax/vecmath/AxisAngle4d;
    iget-wide v6, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v8, v2, Ljavax/vecmath/AxisAngle4d;->x:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v8, v2, Ljavax/vecmath/AxisAngle4d;->y:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v8, v2, Ljavax/vecmath/AxisAngle4d;->z:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    iget-wide v8, v2, Ljavax/vecmath/AxisAngle4d;->angle:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 485
    .end local v2    # "a2":Ljavax/vecmath/AxisAngle4d;
    :cond_0
    :goto_0
    return v5

    .line 484
    :catch_0
    move-exception v4

    .local v4, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 485
    .end local v4    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v3

    .local v3, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/AxisAngle4d;)Z
    .locals 6
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    const/4 v1, 0x0

    .line 464
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->angle:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 467
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get([D)V
    .locals 4
    .param p1, "a"    # [D

    .prologue
    .line 231
    const/4 v0, 0x0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    aput-wide v2, p1, v0

    .line 232
    const/4 v0, 0x1

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    aput-wide v2, p1, v0

    .line 233
    const/4 v0, 0x2

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    aput-wide v2, p1, v0

    .line 234
    const/4 v0, 0x3

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    aput-wide v2, p1, v0

    .line 235
    return-void
.end method

.method public final getAngle()D
    .locals 2

    .prologue
    .line 564
    iget-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    return-wide v0
.end method

.method public getX()D
    .locals 2

    .prologue
    .line 589
    iget-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    return-wide v0
.end method

.method public final getY()D
    .locals 2

    .prologue
    .line 613
    iget-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    return-wide v0
.end method

.method public getZ()D
    .locals 2

    .prologue
    .line 637
    iget-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 528
    const-wide/16 v0, 0x1

    .line 529
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 530
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 531
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 532
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 533
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final set(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "angle"    # D

    .prologue
    .line 162
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 163
    iput-wide p3, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 164
    iput-wide p5, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 165
    iput-wide p7, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 166
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 188
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 189
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 190
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 191
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 192
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 201
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 202
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 203
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 204
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 205
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 360
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 361
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 362
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 364
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v8, v10

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v12, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v12, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v10, v12

    add-double v4, v8, v10

    .line 366
    .local v4, "mag":D
    const-wide v8, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 367
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 369
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v6, v8, v4

    .line 370
    .local v6, "sin":D
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v12, p1, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v10, v12

    iget-wide v12, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v10, v12

    mul-double v0, v8, v10

    .line 372
    .local v0, "cos":D
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v8, v8

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 374
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double v2, v8, v4

    .line 375
    .local v2, "invMag":D
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 376
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 377
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 385
    .end local v0    # "cos":D
    .end local v2    # "invMag":D
    .end local v6    # "sin":D
    :goto_0
    return-void

    .line 379
    :cond_0
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 380
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 381
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 382
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 326
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v8, v9

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 327
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v8, v9

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 328
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v8, v9

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 329
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v8, v10

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v12, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    iget-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v12, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v10, v12

    add-double v4, v8, v10

    .line 331
    .local v4, "mag":D
    const-wide v8, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 332
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 334
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v6, v8, v4

    .line 335
    .local v6, "sin":D
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v10, v11

    float-to-double v10, v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v10, v12

    mul-double v0, v8, v10

    .line 336
    .local v0, "cos":D
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v8, v8

    float-to-double v8, v8

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 338
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double v2, v8, v4

    .line 339
    .local v2, "invMag":D
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 340
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 341
    iget-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v8, v2

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 348
    .end local v0    # "cos":D
    .end local v2    # "invMag":D
    .end local v6    # "sin":D
    :goto_0
    return-void

    .line 343
    :cond_0
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 344
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 345
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 346
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 18
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 287
    new-instance v6, Ljavax/vecmath/Matrix3d;

    invoke-direct {v6}, Ljavax/vecmath/Matrix3d;-><init>()V

    .line 289
    .local v6, "m3d":Ljavax/vecmath/Matrix3d;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljavax/vecmath/Matrix4d;->get(Ljavax/vecmath/Matrix3d;)V

    .line 291
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 292
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 293
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 295
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double v8, v12, v14

    .line 297
    .local v8, "mag":D
    const-wide v12, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v7, v8, v12

    if-lez v7, :cond_0

    .line 298
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 300
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double v10, v12, v8

    .line 301
    .local v10, "sin":D
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v14, v14, v16

    mul-double v2, v12, v14

    .line 302
    .local v2, "cos":D
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 304
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double v4, v12, v8

    .line 305
    .local v4, "invMag":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 306
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 307
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 314
    .end local v2    # "cos":D
    .end local v4    # "invMag":D
    .end local v10    # "sin":D
    :goto_0
    return-void

    .line 309
    :cond_0
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 310
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 311
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 312
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 18
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 248
    new-instance v6, Ljavax/vecmath/Matrix3d;

    invoke-direct {v6}, Ljavax/vecmath/Matrix3d;-><init>()V

    .line 250
    .local v6, "m3d":Ljavax/vecmath/Matrix3d;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljavax/vecmath/Matrix4f;->get(Ljavax/vecmath/Matrix3d;)V

    .line 252
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 253
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 254
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 255
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double v8, v12, v14

    .line 257
    .local v8, "mag":D
    const-wide v12, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v7, v8, v12

    if-lez v7, :cond_0

    .line 258
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 259
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double v10, v12, v8

    .line 260
    .local v10, "sin":D
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v14, v14, v16

    mul-double v2, v12, v14

    .line 262
    .local v2, "cos":D
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v7, v12

    float-to-double v12, v7

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 264
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double v4, v12, v8

    .line 265
    .local v4, "invMag":D
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 266
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 267
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v12, v4

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 274
    .end local v2    # "cos":D
    .end local v4    # "invMag":D
    .end local v10    # "sin":D
    :goto_0
    return-void

    .line 269
    :cond_0
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 270
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 271
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 272
    const-wide/16 v12, 0x0

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 14
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const-wide/16 v10, 0x0

    .line 426
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 428
    .local v2, "mag":D
    const-wide v4, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 429
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 430
    div-double v0, v12, v2

    .line 432
    .local v0, "invMag":D
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 433
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 434
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 435
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 442
    .end local v0    # "invMag":D
    :goto_0
    return-void

    .line 437
    :cond_0
    iput-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 438
    iput-wide v12, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 439
    iput-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 440
    iput-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    .line 398
    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v2, v4

    .line 400
    .local v2, "mag":D
    const-wide v4, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 401
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 402
    div-double v0, v10, v2

    .line 404
    .local v0, "invMag":D
    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 405
    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 406
    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 407
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v6, v6

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    iput-wide v4, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 414
    .end local v0    # "invMag":D
    :goto_0
    return-void

    .line 409
    :cond_0
    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 410
    iput-wide v10, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 411
    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 412
    iput-wide v8, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Vector3d;D)V
    .locals 2
    .param p1, "axis"    # Ljavax/vecmath/Vector3d;
    .param p2, "angle"    # D

    .prologue
    .line 217
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 218
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 219
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 220
    iput-wide p2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 221
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "a"    # [D

    .prologue
    .line 175
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 176
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 177
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 178
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 179
    return-void
.end method

.method public final setAngle(D)V
    .locals 1
    .param p1, "angle"    # D

    .prologue
    .line 577
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    .line 578
    return-void
.end method

.method public final setX(D)V
    .locals 1
    .param p1, "x"    # D

    .prologue
    .line 601
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    .line 602
    return-void
.end method

.method public final setY(D)V
    .locals 1
    .param p1, "y"    # D

    .prologue
    .line 625
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    .line 626
    return-void
.end method

.method public final setZ(D)V
    .locals 1
    .param p1, "z"    # D

    .prologue
    .line 649
    iput-wide p1, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    .line 650
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 451
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->z:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/AxisAngle4d;->angle:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
