.class public Ljavax/vecmath/Color3b;
.super Ljavax/vecmath/Tuple3b;
.source "Color3b.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x5c0ba4cfbc0e93baL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljavax/vecmath/Tuple3b;-><init>()V

    .line 118
    return-void
.end method

.method public constructor <init>(BBB)V
    .locals 0
    .param p1, "c1"    # B
    .param p2, "c2"    # B
    .param p3, "c3"    # B

    .prologue
    .line 64
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3b;-><init>(BBB)V

    .line 65
    return-void
.end method

.method public constructor <init>(Ljava/awt/Color;)V
    .locals 3
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    .line 107
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-byte v0, v0

    .line 108
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v1

    int-to-byte v1, v1

    .line 109
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v2

    int-to-byte v2, v2

    .line 107
    invoke-direct {p0, v0, v1, v2}, Ljavax/vecmath/Tuple3b;-><init>(BBB)V

    .line 110
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Color3b;)V
    .locals 0
    .param p1, "c1"    # Ljavax/vecmath/Color3b;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3b;-><init>(Ljavax/vecmath/Tuple3b;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3b;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3b;

    .prologue
    .line 91
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3b;-><init>(Ljavax/vecmath/Tuple3b;)V

    .line 92
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "c"    # [B

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3b;-><init>([B)V

    .line 74
    return-void
.end method


# virtual methods
.method public final get()Ljava/awt/Color;
    .locals 4

    .prologue
    .line 147
    iget-byte v3, p0, Ljavax/vecmath/Color3b;->x:B

    and-int/lit16 v2, v3, 0xff

    .line 148
    .local v2, "r":I
    iget-byte v3, p0, Ljavax/vecmath/Color3b;->y:B

    and-int/lit16 v1, v3, 0xff

    .line 149
    .local v1, "g":I
    iget-byte v3, p0, Ljavax/vecmath/Color3b;->z:B

    and-int/lit16 v0, v3, 0xff

    .line 151
    .local v0, "b":I
    new-instance v3, Ljava/awt/Color;

    invoke-direct {v3, v2, v1, v0}, Ljava/awt/Color;-><init>(III)V

    return-object v3
.end method

.method public final set(Ljava/awt/Color;)V
    .locals 1
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    .line 132
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color3b;->x:B

    .line 133
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color3b;->y:B

    .line 134
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color3b;->z:B

    .line 135
    return-void
.end method
