.class public Ljavax/vecmath/Vector2d;
.super Ljavax/vecmath/Tuple2d;
.source "Vector2d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x76f828730ce7b0b1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljavax/vecmath/Tuple2d;-><init>()V

    .line 113
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple2d;-><init>(DD)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector2d;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector2f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector2f;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 84
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .param p1, "v"    # [D

    .prologue
    .line 63
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>([D)V

    .line 64
    return-void
.end method


# virtual methods
.method public final angle(Ljavax/vecmath/Vector2d;)D
    .locals 8
    .param p1, "v1"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 179
    invoke-virtual {p0, p1}, Ljavax/vecmath/Vector2d;->dot(Ljavax/vecmath/Vector2d;)D

    move-result-wide v2

    invoke-virtual {p0}, Ljavax/vecmath/Vector2d;->length()D

    move-result-wide v4

    invoke-virtual {p1}, Ljavax/vecmath/Vector2d;->length()D

    move-result-wide v6

    mul-double/2addr v4, v6

    div-double v0, v2, v4

    .line 180
    .local v0, "vDot":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 181
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 182
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    return-wide v2
.end method

.method public final dot(Ljavax/vecmath/Vector2d;)D
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 122
    iget-wide v0, p0, Ljavax/vecmath/Vector2d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final length()D
    .locals 6

    .prologue
    .line 132
    iget-wide v0, p0, Ljavax/vecmath/Vector2d;->x:D

    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    iget-wide v4, p0, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final lengthSquared()D
    .locals 6

    .prologue
    .line 141
    iget-wide v0, p0, Ljavax/vecmath/Vector2d;->x:D

    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    iget-wide v4, p0, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final normalize()V
    .locals 10

    .prologue
    .line 164
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, Ljavax/vecmath/Vector2d;->x:D

    iget-wide v6, p0, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Vector2d;->y:D

    iget-wide v8, p0, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 165
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 166
    .local v0, "norm":D
    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector2d;->x:D

    .line 167
    iget-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    .line 168
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Vector2d;)V
    .locals 10
    .param p1, "v1"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 152
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Vector2d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Vector2d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 153
    .local v0, "norm":D
    iget-wide v2, p1, Ljavax/vecmath/Vector2d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector2d;->x:D

    .line 154
    iget-wide v2, p1, Ljavax/vecmath/Vector2d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector2d;->y:D

    .line 155
    return-void
.end method
