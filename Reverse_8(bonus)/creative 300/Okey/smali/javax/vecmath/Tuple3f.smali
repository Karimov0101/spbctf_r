.class public abstract Ljavax/vecmath/Tuple3f;
.super Ljava/lang/Object;
.source "Tuple3f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x45aa08fd7d928da0L


# instance fields
.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 117
    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 118
    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 119
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 70
    iput p2, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 71
    iput p3, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 72
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 106
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 107
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 108
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 94
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 95
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 96
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 82
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 83
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 84
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 586
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 587
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 588
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 590
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 520
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 521
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 522
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 523
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 226
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 227
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 228
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 229
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3f;Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 214
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 215
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 216
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 217
    return-void
.end method

.method public final clamp(FF)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 534
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_3

    .line 535
    iput p2, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 540
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_4

    .line 541
    iput p2, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 546
    :cond_1
    :goto_1
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_5

    .line 547
    iput p2, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 552
    :cond_2
    :goto_2
    return-void

    .line 536
    :cond_3
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 537
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    goto :goto_0

    .line 542
    :cond_4
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 543
    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    goto :goto_1

    .line 548
    :cond_5
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    .line 549
    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    goto :goto_2
.end method

.method public final clamp(FFLjavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 428
    iget v0, p3, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_0

    .line 429
    iput p2, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 436
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_2

    .line 437
    iput p2, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 444
    :goto_1
    iget v0, p3, Ljavax/vecmath/Tuple3f;->z:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_4

    .line 445
    iput p2, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 452
    :goto_2
    return-void

    .line 430
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple3f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 431
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    goto :goto_0

    .line 433
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    goto :goto_0

    .line 438
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple3f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    .line 439
    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    goto :goto_1

    .line 441
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    goto :goto_1

    .line 446
    :cond_4
    iget v0, p3, Ljavax/vecmath/Tuple3f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_5

    .line 447
    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    goto :goto_2

    .line 449
    :cond_5
    iget v0, p3, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    goto :goto_2
.end method

.method public final clampMax(F)V
    .locals 1
    .param p1, "max"    # F

    .prologue
    .line 574
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 575
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 576
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_2

    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 578
    :cond_2
    return-void
.end method

.method public final clampMax(FLjavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "max"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 492
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    .line 493
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 498
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    .line 499
    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 504
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_2

    .line 505
    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 510
    :goto_2
    return-void

    .line 495
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    goto :goto_0

    .line 501
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    goto :goto_1

    .line 507
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    goto :goto_2
.end method

.method public final clampMin(F)V
    .locals 1
    .param p1, "min"    # F

    .prologue
    .line 561
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 562
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 563
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 565
    :cond_2
    return-void
.end method

.method public final clampMin(FLjavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 463
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 464
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 469
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 470
    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 475
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    .line 476
    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 481
    :goto_2
    return-void

    .line 466
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    goto :goto_0

    .line 472
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    goto :goto_1

    .line 478
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    goto :goto_2
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 636
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 637
    :catch_0
    move-exception v0

    .line 639
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple3f;F)Z
    .locals 5
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p2, "epsilon"    # F

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 384
    iget v2, p0, Ljavax/vecmath/Tuple3f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    sub-float v0, v2, v3

    .line 385
    .local v0, "diff":F
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 396
    :cond_0
    :goto_0
    return v1

    .line 386
    :cond_1
    cmpg-float v2, v0, v4

    if-gez v2, :cond_2

    neg-float v2, v0

    :goto_1
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 388
    iget v2, p0, Ljavax/vecmath/Tuple3f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->y:F

    sub-float v0, v2, v3

    .line 389
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 390
    cmpg-float v2, v0, v4

    if-gez v2, :cond_3

    neg-float v2, v0

    :goto_2
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 392
    iget v2, p0, Ljavax/vecmath/Tuple3f;->z:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->z:F

    sub-float v0, v2, v3

    .line 393
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 394
    cmpg-float v2, v0, v4

    if-gez v2, :cond_4

    neg-float v2, v0

    :goto_3
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 396
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v2, v0

    .line 386
    goto :goto_1

    :cond_3
    move v2, v0

    .line 390
    goto :goto_2

    :cond_4
    move v2, v0

    .line 394
    goto :goto_3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 363
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple3f;

    move-object v3, v0

    .line 364
    .local v3, "t2":Ljavax/vecmath/Tuple3f;
    iget v5, p0, Ljavax/vecmath/Tuple3f;->x:F

    iget v6, v3, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple3f;->y:F

    iget v6, v3, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple3f;->z:F

    iget v6, v3, Ljavax/vecmath/Tuple3f;->z:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 367
    .end local v3    # "t2":Ljavax/vecmath/Tuple3f;
    :cond_0
    :goto_0
    return v4

    .line 366
    :catch_0
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 367
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple3f;)Z
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    const/4 v1, 0x0

    .line 349
    :try_start_0
    iget v2, p0, Ljavax/vecmath/Tuple3f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple3f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple3f;->z:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->z:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 351
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 201
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    .line 202
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    .line 203
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    .line 204
    return-void
.end method

.method public final get([F)V
    .locals 2
    .param p1, "t"    # [F

    .prologue
    .line 189
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple3f;->x:F

    aput v1, p1, v0

    .line 190
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple3f;->y:F

    aput v1, p1, v0

    .line 191
    const/4 v0, 0x2

    iget v1, p0, Ljavax/vecmath/Tuple3f;->z:F

    aput v1, p1, v0

    .line 192
    return-void
.end method

.method public final getX()F
    .locals 1

    .prologue
    .line 652
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    return v0
.end method

.method public final getY()F
    .locals 1

    .prologue
    .line 676
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    return v0
.end method

.method public final getZ()F
    .locals 1

    .prologue
    .line 699
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 410
    const-wide/16 v0, 0x1

    .line 411
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3f;->x:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 412
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3f;->y:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 413
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3f;->z:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 414
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p2, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 618
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 619
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 620
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 623
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3f;Ljavax/vecmath/Tuple3f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3f;
    .param p3, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 602
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 603
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 604
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 607
    return-void
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 277
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 278
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 279
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 265
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 266
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 267
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 268
    return-void
.end method

.method public final scale(F)V
    .locals 1
    .param p1, "s"    # F

    .prologue
    .line 303
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 304
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 305
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 306
    return-void
.end method

.method public final scale(FLjavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 290
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 291
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 292
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 293
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 333
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 334
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 335
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 336
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple3f;Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p3, "t2"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 318
    iget v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 319
    iget v0, p2, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 320
    iget v0, p2, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 321
    return-void
.end method

.method public final set(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 140
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 141
    iput p2, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 142
    iput p3, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 143
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 177
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 178
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 179
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 180
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 165
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 166
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 167
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 168
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 153
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 154
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 155
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 156
    return-void
.end method

.method public final setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 664
    iput p1, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 665
    return-void
.end method

.method public final setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 688
    iput p1, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 689
    return-void
.end method

.method public final setZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 711
    iput p1, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 712
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 253
    iget v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 254
    iget v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 255
    iget v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 256
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3f;Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 240
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->x:F

    .line 241
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->y:F

    .line 242
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iget v1, p2, Ljavax/vecmath/Tuple3f;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3f;->z:F

    .line 243
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3f;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
