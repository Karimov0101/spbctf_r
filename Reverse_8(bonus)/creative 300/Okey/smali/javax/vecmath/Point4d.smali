.class public Ljavax/vecmath/Point4d;
.super Ljavax/vecmath/Tuple4d;
.source "Point4d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x180e87814afb5935L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 130
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "w"    # D

    .prologue
    .line 56
    invoke-direct/range {p0 .. p8}, Ljavax/vecmath/Tuple4d;-><init>(DDDD)V

    .line 57
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point4d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point4f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 120
    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->z:D

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Ljavax/vecmath/Tuple4d;-><init>(DDDD)V

    .line 121
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 97
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .param p1, "p"    # [D

    .prologue
    .line 66
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>([D)V

    .line 67
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point4d;)D
    .locals 12
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 175
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->x:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->x:D

    sub-double v2, v8, v10

    .line 176
    .local v2, "dx":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->y:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->y:D

    sub-double v4, v8, v10

    .line 177
    .local v4, "dy":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->z:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->z:D

    sub-double v6, v8, v10

    .line 178
    .local v6, "dz":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->w:D

    sub-double v0, v8, v10

    .line 179
    .local v0, "dw":D
    mul-double v8, v2, v2

    mul-double v10, v4, v4

    add-double/2addr v8, v10

    mul-double v10, v6, v6

    add-double/2addr v8, v10

    mul-double v10, v0, v0

    add-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    return-wide v8
.end method

.method public final distanceL1(Ljavax/vecmath/Point4d;)D
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 191
    iget-wide v0, p0, Ljavax/vecmath/Point4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Point4d;->x:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Ljavax/vecmath/Point4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Point4d;->y:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Point4d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Point4d;->z:D

    sub-double/2addr v2, v4

    .line 192
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Point4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Point4d;->w:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point4d;)D
    .locals 10
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 204
    iget-wide v4, p0, Ljavax/vecmath/Point4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Point4d;->x:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget-wide v6, p0, Ljavax/vecmath/Point4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Point4d;->y:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 205
    .local v0, "t1":D
    iget-wide v4, p0, Ljavax/vecmath/Point4d;->z:D

    iget-wide v6, p1, Ljavax/vecmath/Point4d;->z:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    iget-wide v6, p0, Ljavax/vecmath/Point4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Point4d;->w:D

    sub-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    .line 207
    .local v2, "t2":D
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v4

    return-wide v4
.end method

.method public final distanceSquared(Ljavax/vecmath/Point4d;)D
    .locals 12
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 158
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->x:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->x:D

    sub-double v2, v8, v10

    .line 159
    .local v2, "dx":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->y:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->y:D

    sub-double v4, v8, v10

    .line 160
    .local v4, "dy":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->z:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->z:D

    sub-double v6, v8, v10

    .line 161
    .local v6, "dz":D
    iget-wide v8, p0, Ljavax/vecmath/Point4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Point4d;->w:D

    sub-double v0, v8, v10

    .line 162
    .local v0, "dw":D
    mul-double v8, v2, v2

    mul-double v10, v4, v4

    add-double/2addr v8, v10

    mul-double v10, v6, v6

    add-double/2addr v8, v10

    mul-double v10, v0, v0

    add-double/2addr v8, v10

    return-wide v8
.end method

.method public final project(Ljavax/vecmath/Point4d;)V
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 220
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->w:D

    div-double v0, v4, v2

    .line 221
    .local v0, "oneOw":D
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point4d;->x:D

    .line 222
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point4d;->y:D

    .line 223
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point4d;->z:D

    .line 224
    iput-wide v4, p0, Ljavax/vecmath/Point4d;->w:D

    .line 226
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 142
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Point4d;->x:D

    .line 143
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Point4d;->y:D

    .line 144
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Point4d;->z:D

    .line 145
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Point4d;->w:D

    .line 146
    return-void
.end method
