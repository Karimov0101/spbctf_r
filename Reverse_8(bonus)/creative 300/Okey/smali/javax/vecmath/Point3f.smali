.class public Ljavax/vecmath/Point3f;
.super Ljavax/vecmath/Tuple3f;
.source "Point3f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x7896bab4b7c5e53fL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljavax/vecmath/Tuple3f;-><init>()V

    .line 115
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3f;-><init>(FFF)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point3d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 86
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point3f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 105
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 96
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "p"    # [F

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>([F)V

    .line 66
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point3f;)F
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 144
    iget v3, p0, Ljavax/vecmath/Point3f;->x:F

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    sub-float v0, v3, v4

    .line 145
    .local v0, "dx":F
    iget v3, p0, Ljavax/vecmath/Point3f;->y:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    sub-float v1, v3, v4

    .line 146
    .local v1, "dy":F
    iget v3, p0, Ljavax/vecmath/Point3f;->z:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    sub-float v2, v3, v4

    .line 147
    .local v2, "dz":F
    mul-float v3, v0, v0

    mul-float v4, v1, v1

    add-float/2addr v3, v4

    mul-float v4, v2, v2

    add-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v3, v4

    return v3
.end method

.method public final distanceL1(Ljavax/vecmath/Point3f;)F
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 160
    iget v0, p0, Ljavax/vecmath/Point3f;->x:F

    iget v1, p1, Ljavax/vecmath/Point3f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Ljavax/vecmath/Point3f;->y:F

    iget v2, p1, Ljavax/vecmath/Point3f;->y:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Point3f;->z:F

    iget v2, p1, Ljavax/vecmath/Point3f;->z:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point3f;)F
    .locals 4
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 174
    iget v1, p0, Ljavax/vecmath/Point3f;->x:F

    iget v2, p1, Ljavax/vecmath/Point3f;->x:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v2, p0, Ljavax/vecmath/Point3f;->y:F

    iget v3, p1, Ljavax/vecmath/Point3f;->y:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 175
    .local v0, "tmp":F
    iget v1, p0, Ljavax/vecmath/Point3f;->z:F

    iget v2, p1, Ljavax/vecmath/Point3f;->z:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    return v1
.end method

.method public final distanceSquared(Ljavax/vecmath/Point3f;)F
    .locals 5
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 128
    iget v3, p0, Ljavax/vecmath/Point3f;->x:F

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    sub-float v0, v3, v4

    .line 129
    .local v0, "dx":F
    iget v3, p0, Ljavax/vecmath/Point3f;->y:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    sub-float v1, v3, v4

    .line 130
    .local v1, "dy":F
    iget v3, p0, Ljavax/vecmath/Point3f;->z:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    sub-float v2, v3, v4

    .line 131
    .local v2, "dz":F
    mul-float v3, v0, v0

    mul-float v4, v1, v1

    add-float/2addr v3, v4

    mul-float v4, v2, v2

    add-float/2addr v3, v4

    return v3
.end method

.method public final project(Ljavax/vecmath/Point4f;)V
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 189
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p1, Ljavax/vecmath/Point4f;->w:F

    div-float v0, v1, v2

    .line 190
    .local v0, "oneOw":F
    iget v1, p1, Ljavax/vecmath/Point4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point3f;->x:F

    .line 191
    iget v1, p1, Ljavax/vecmath/Point4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point3f;->y:F

    .line 192
    iget v1, p1, Ljavax/vecmath/Point4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point3f;->z:F

    .line 194
    return-void
.end method
