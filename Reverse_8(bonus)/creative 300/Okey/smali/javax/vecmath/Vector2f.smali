.class public Ljavax/vecmath/Vector2f;
.super Ljavax/vecmath/Tuple2f;
.source "Vector2f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x1e16f940b1e8b000L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljavax/vecmath/Tuple2f;-><init>()V

    .line 114
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljavax/vecmath/Tuple2f;-><init>(FF)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector2d;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector2d;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector2f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector2f;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 74
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 63
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>([F)V

    .line 64
    return-void
.end method


# virtual methods
.method public final angle(Ljavax/vecmath/Vector2f;)F
    .locals 5
    .param p1, "v1"    # Ljavax/vecmath/Vector2f;

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Ljavax/vecmath/Vector2f;->dot(Ljavax/vecmath/Vector2f;)F

    move-result v2

    invoke-virtual {p0}, Ljavax/vecmath/Vector2f;->length()F

    move-result v3

    invoke-virtual {p1}, Ljavax/vecmath/Vector2f;->length()F

    move-result v4

    mul-float/2addr v3, v4

    div-float/2addr v2, v3

    float-to-double v0, v2

    .line 181
    .local v0, "vDot":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 182
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 183
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public final dot(Ljavax/vecmath/Vector2f;)F
    .locals 3
    .param p1, "v1"    # Ljavax/vecmath/Vector2f;

    .prologue
    .line 123
    iget v0, p0, Ljavax/vecmath/Vector2f;->x:F

    iget v1, p1, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector2f;->y:F

    iget v2, p1, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final length()F
    .locals 3

    .prologue
    .line 133
    iget v0, p0, Ljavax/vecmath/Vector2f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector2f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public final lengthSquared()F
    .locals 3

    .prologue
    .line 142
    iget v0, p0, Ljavax/vecmath/Vector2f;->x:F

    iget v1, p0, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Vector2f;->y:F

    iget v2, p0, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public final normalize()V
    .locals 6

    .prologue
    .line 165
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p0, Ljavax/vecmath/Vector2f;->x:F

    iget v4, p0, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v1, v4

    iget v4, p0, Ljavax/vecmath/Vector2f;->y:F

    iget v5, p0, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    .line 166
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 167
    .local v0, "norm":F
    iget v1, p0, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector2f;->x:F

    .line 168
    iget v1, p0, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector2f;->y:F

    .line 169
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Vector2f;)V
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector2f;

    .prologue
    .line 153
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p1, Ljavax/vecmath/Vector2f;->x:F

    iget v4, p1, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Vector2f;->y:F

    iget v5, p1, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 154
    .local v0, "norm":F
    iget v1, p1, Ljavax/vecmath/Vector2f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector2f;->x:F

    .line 155
    iget v1, p1, Ljavax/vecmath/Vector2f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Vector2f;->y:F

    .line 156
    return-void
.end method
