.class public Ljavax/vecmath/Point2i;
.super Ljavax/vecmath/Tuple2i;
.source "Point2i.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x7fc9a50a3b04535aL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Ljavax/vecmath/Tuple2i;-><init>()V

    .line 81
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljavax/vecmath/Tuple2i;-><init>(II)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2i;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2i;-><init>(Ljavax/vecmath/Tuple2i;)V

    .line 73
    return-void
.end method

.method public constructor <init>([I)V
    .locals 0
    .param p1, "t"    # [I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2i;-><init>([I)V

    .line 63
    return-void
.end method
