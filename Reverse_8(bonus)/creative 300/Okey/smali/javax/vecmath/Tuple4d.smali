.class public abstract Ljavax/vecmath/Tuple4d;
.super Ljava/lang/Object;
.source "Tuple4d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = -0x41e7ac386c94bf4cL


# instance fields
.field public w:D

.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 128
    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 129
    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 130
    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 131
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "w"    # D

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 76
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 77
    iput-wide p5, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 78
    iput-wide p7, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 79
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 103
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 104
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 105
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 106
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 116
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 117
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 118
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 119
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 90
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 91
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 92
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 93
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 2

    .prologue
    .line 704
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 705
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 706
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 707
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 709
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 608
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 609
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 610
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 611
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 613
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 238
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 239
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 240
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 241
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 242
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 225
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 226
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 227
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 228
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->w:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 229
    return-void
.end method

.method public final clamp(DD)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 631
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_4

    .line 632
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 637
    :cond_0
    :goto_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_5

    .line 638
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 643
    :cond_1
    :goto_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_6

    .line 644
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 649
    :cond_2
    :goto_2
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_7

    .line 650
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 655
    :cond_3
    :goto_3
    return-void

    .line 633
    :cond_4
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 634
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    goto :goto_0

    .line 639
    :cond_5
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 640
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    goto :goto_1

    .line 645
    :cond_6
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    .line 646
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    goto :goto_2

    .line 651
    :cond_7
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    .line 652
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    goto :goto_3
.end method

.method public final clamp(DDLjavax/vecmath/Tuple4d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 482
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_0

    .line 483
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 490
    :goto_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_2

    .line 491
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 498
    :goto_1
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_4

    .line 499
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 506
    :goto_2
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->w:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_6

    .line 507
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 514
    :goto_3
    return-void

    .line 484
    :cond_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 485
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    goto :goto_0

    .line 487
    :cond_1
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    goto :goto_0

    .line 492
    :cond_2
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    .line 493
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    goto :goto_1

    .line 495
    :cond_3
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    goto :goto_1

    .line 500
    :cond_4
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_5

    .line 501
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    goto :goto_2

    .line 503
    :cond_5
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    goto :goto_2

    .line 508
    :cond_6
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->w:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_7

    .line 509
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    goto :goto_3

    .line 511
    :cond_7
    iget-wide v0, p5, Ljavax/vecmath/Tuple4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    goto :goto_3
.end method

.method public final clamp(FF)V
    .locals 4
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 621
    float-to-double v0, p1

    float-to-double v2, p2

    invoke-virtual {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple4d;->clamp(DD)V

    .line 622
    return-void
.end method

.method public final clamp(FFLjavax/vecmath/Tuple4d;)V
    .locals 7
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 470
    float-to-double v2, p1

    float-to-double v4, p2

    move-object v1, p0

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Ljavax/vecmath/Tuple4d;->clamp(DDLjavax/vecmath/Tuple4d;)V

    .line 471
    return-void
.end method

.method public final clampMax(D)V
    .locals 3
    .param p1, "max"    # D

    .prologue
    .line 691
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 692
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 693
    :cond_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_2

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 694
    :cond_2
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_3

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 696
    :cond_3
    return-void
.end method

.method public final clampMax(DLjavax/vecmath/Tuple4d;)V
    .locals 3
    .param p1, "max"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 574
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    .line 575
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 580
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    .line 581
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 586
    :goto_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_2

    .line 587
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 592
    :goto_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->w:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_3

    .line 593
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 598
    :goto_3
    return-void

    .line 577
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    goto :goto_0

    .line 583
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    goto :goto_1

    .line 589
    :cond_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    goto :goto_2

    .line 595
    :cond_3
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    goto :goto_3
.end method

.method public final clampMax(F)V
    .locals 2
    .param p1, "max"    # F

    .prologue
    .line 682
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Ljavax/vecmath/Tuple4d;->clampMax(D)V

    .line 683
    return-void
.end method

.method public final clampMax(FLjavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "max"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 563
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1, p2}, Ljavax/vecmath/Tuple4d;->clampMax(DLjavax/vecmath/Tuple4d;)V

    .line 564
    return-void
.end method

.method public final clampMin(D)V
    .locals 3
    .param p1, "min"    # D

    .prologue
    .line 671
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 672
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 673
    :cond_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 674
    :cond_2
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 675
    :cond_3
    return-void
.end method

.method public final clampMin(DLjavax/vecmath/Tuple4d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 532
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 533
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 538
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 539
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 544
    :goto_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    .line 545
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 550
    :goto_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->w:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    .line 551
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 556
    :goto_3
    return-void

    .line 535
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    goto :goto_0

    .line 541
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    goto :goto_1

    .line 547
    :cond_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    goto :goto_2

    .line 553
    :cond_3
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    goto :goto_3
.end method

.method public final clampMin(F)V
    .locals 2
    .param p1, "min"    # F

    .prologue
    .line 662
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Ljavax/vecmath/Tuple4d;->clampMin(D)V

    .line 663
    return-void
.end method

.method public final clampMin(FLjavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "min"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 521
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1, p2}, Ljavax/vecmath/Tuple4d;->clampMin(DLjavax/vecmath/Tuple4d;)V

    .line 522
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 767
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 768
    :catch_0
    move-exception v0

    .line 770
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple4d;D)Z
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "epsilon"    # D

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 427
    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->x:D

    sub-double v0, v4, v6

    .line 428
    .local v0, "diff":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 443
    :cond_0
    :goto_0
    return v2

    .line 429
    :cond_1
    cmpg-double v3, v0, v8

    if-gez v3, :cond_2

    neg-double v4, v0

    :goto_1
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 431
    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->y:D

    sub-double v0, v4, v6

    .line 432
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 433
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 435
    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->z:D

    sub-double v0, v4, v6

    .line 436
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 437
    cmpg-double v3, v0, v8

    if-gez v3, :cond_4

    neg-double v4, v0

    :goto_3
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 439
    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->w:D

    sub-double v0, v4, v6

    .line 440
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 441
    cmpg-double v3, v0, v8

    if-gez v3, :cond_5

    neg-double v4, v0

    :goto_4
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 443
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move-wide v4, v0

    .line 429
    goto :goto_1

    :cond_3
    move-wide v4, v0

    .line 433
    goto :goto_2

    :cond_4
    move-wide v4, v0

    .line 437
    goto :goto_3

    :cond_5
    move-wide v4, v0

    .line 441
    goto :goto_4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 404
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple4d;

    move-object v4, v0

    .line 405
    .local v4, "t2":Ljavax/vecmath/Tuple4d;
    iget-wide v6, p0, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple4d;->w:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 409
    .end local v4    # "t2":Ljavax/vecmath/Tuple4d;
    :cond_0
    :goto_0
    return v5

    .line 408
    :catch_0
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 409
    .end local v3    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple4d;)Z
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    const/4 v1, 0x0

    .line 387
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->z:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->w:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 390
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 211
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    .line 212
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    .line 213
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    .line 214
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    .line 215
    return-void
.end method

.method public final get([D)V
    .locals 4
    .param p1, "t"    # [D

    .prologue
    .line 196
    const/4 v0, 0x0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->x:D

    aput-wide v2, p1, v0

    .line 197
    const/4 v0, 0x1

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->y:D

    aput-wide v2, p1, v0

    .line 198
    const/4 v0, 0x2

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->z:D

    aput-wide v2, p1, v0

    .line 199
    const/4 v0, 0x3

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->w:D

    aput-wide v2, p1, v0

    .line 200
    return-void
.end method

.method public final getW()D
    .locals 2

    .prologue
    .line 853
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    return-wide v0
.end method

.method public final getX()D
    .locals 2

    .prologue
    .line 782
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    return-wide v0
.end method

.method public final getY()D
    .locals 2

    .prologue
    .line 806
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    return-wide v0
.end method

.method public final getZ()D
    .locals 2

    .prologue
    .line 829
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 457
    const-wide/16 v0, 0x1

    .line 458
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->x:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 459
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->y:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 460
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->z:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 461
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple4d;->w:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 462
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public interpolate(Ljavax/vecmath/Tuple4d;D)V
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 750
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 751
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 752
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 753
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 754
    return-void
.end method

.method public interpolate(Ljavax/vecmath/Tuple4d;F)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "alpha"    # F

    .prologue
    .line 739
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Ljavax/vecmath/Tuple4d;->interpolate(Ljavax/vecmath/Tuple4d;D)V

    .line 740
    return-void
.end method

.method public interpolate(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;D)V
    .locals 7
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4d;
    .param p3, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 728
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 729
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 730
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 731
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 732
    return-void
.end method

.method public interpolate(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;F)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4d;
    .param p3, "alpha"    # F

    .prologue
    .line 716
    float-to-double v0, p3

    invoke-virtual {p0, p1, p2, v0, v1}, Ljavax/vecmath/Tuple4d;->interpolate(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;D)V

    .line 717
    return-void
.end method

.method public final negate()V
    .locals 2

    .prologue
    .line 292
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 293
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 294
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 295
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 296
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 280
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 281
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 282
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 283
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 284
    return-void
.end method

.method public final scale(D)V
    .locals 3
    .param p1, "s"    # D

    .prologue
    .line 321
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 322
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 323
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 324
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 325
    return-void
.end method

.method public final scale(DLjavax/vecmath/Tuple4d;)V
    .locals 3
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 307
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 308
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 309
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 310
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 311
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple4d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 360
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple4d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 361
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple4d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 362
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple4d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 363
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple4d;->w:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 364
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p4, "t2"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 337
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple4d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 338
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple4d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 339
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple4d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 340
    iget-wide v0, p3, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple4d;->w:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 341
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 349
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1, p2}, Ljavax/vecmath/Tuple4d;->scaleAdd(DLjavax/vecmath/Tuple4d;)V

    .line 350
    return-void
.end method

.method public final set(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "w"    # D

    .prologue
    .line 143
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 144
    iput-wide p3, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 145
    iput-wide p5, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 146
    iput-wide p7, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 147
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 169
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 170
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 171
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 172
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 173
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 182
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 183
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 184
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 185
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 186
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 156
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 157
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 158
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 159
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 160
    return-void
.end method

.method public final setW(D)V
    .locals 1
    .param p1, "w"    # D

    .prologue
    .line 865
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 866
    return-void
.end method

.method public final setX(D)V
    .locals 1
    .param p1, "x"    # D

    .prologue
    .line 794
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 795
    return-void
.end method

.method public final setY(D)V
    .locals 1
    .param p1, "y"    # D

    .prologue
    .line 818
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 819
    return-void
.end method

.method public final setZ(D)V
    .locals 1
    .param p1, "z"    # D

    .prologue
    .line 841
    iput-wide p1, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 842
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 267
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 268
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 269
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 270
    iget-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 271
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 253
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->x:D

    .line 254
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->y:D

    .line 255
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->z:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->z:D

    .line 256
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple4d;->w:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple4d;->w:D

    .line 257
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 374
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->z:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple4d;->w:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
