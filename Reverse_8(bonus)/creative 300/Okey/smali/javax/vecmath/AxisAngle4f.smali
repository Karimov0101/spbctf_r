.class public Ljavax/vecmath/AxisAngle4f;
.super Ljava/lang/Object;
.source "AxisAngle4f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final EPS:D = 1.0E-6

.field static final serialVersionUID:J = -0x243f7b21bcc1c49L


# instance fields
.field public angle:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    iput v1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 147
    iput v1, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 148
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 149
    iput v1, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 150
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "angle"    # F

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 79
    iput p2, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 80
    iput p3, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 81
    iput p4, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 82
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/AxisAngle4d;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 119
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 120
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 121
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 122
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/AxisAngle4f;)V
    .locals 1
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 106
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 107
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 108
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 109
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector3f;F)V
    .locals 1
    .param p1, "axis"    # Ljavax/vecmath/Vector3f;
    .param p2, "angle"    # F

    .prologue
    .line 133
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 134
    iget v0, p1, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 135
    iget v0, p1, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 136
    iget v0, p1, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 137
    iput p2, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 138
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "a"    # [F

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 92
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 93
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 94
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 95
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 544
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 545
    :catch_0
    move-exception v0

    .line 547
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/AxisAngle4f;F)Z
    .locals 5
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;
    .param p2, "epsilon"    # F

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 499
    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    sub-float v0, v2, v3

    .line 500
    .local v0, "diff":F
    cmpg-float v2, v0, v4

    if-gez v2, :cond_1

    neg-float v2, v0

    :goto_0
    cmpl-float v2, v2, p2

    if-lez v2, :cond_2

    .line 511
    :cond_0
    :goto_1
    return v1

    :cond_1
    move v2, v0

    .line 500
    goto :goto_0

    .line 502
    :cond_2
    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    sub-float v0, v2, v3

    .line 503
    cmpg-float v2, v0, v4

    if-gez v2, :cond_3

    neg-float v2, v0

    :goto_2
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 505
    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    sub-float v0, v2, v3

    .line 506
    cmpg-float v2, v0, v4

    if-gez v2, :cond_4

    neg-float v2, v0

    :goto_3
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 508
    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    sub-float v0, v2, v3

    .line 509
    cmpg-float v2, v0, v4

    if-gez v2, :cond_5

    neg-float v2, v0

    :goto_4
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 511
    const/4 v1, 0x1

    goto :goto_1

    :cond_3
    move v2, v0

    .line 503
    goto :goto_2

    :cond_4
    move v2, v0

    .line 506
    goto :goto_3

    :cond_5
    move v2, v0

    .line 509
    goto :goto_4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 477
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/AxisAngle4f;

    move-object v1, v0

    .line 478
    .local v1, "a2":Ljavax/vecmath/AxisAngle4f;
    iget v5, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v6, v1, Ljavax/vecmath/AxisAngle4f;->x:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v6, v1, Ljavax/vecmath/AxisAngle4f;->y:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v6, v1, Ljavax/vecmath/AxisAngle4f;->z:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    iget v6, v1, Ljavax/vecmath/AxisAngle4f;->angle:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 482
    .end local v1    # "a2":Ljavax/vecmath/AxisAngle4f;
    :cond_0
    :goto_0
    return v4

    .line 481
    :catch_0
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 482
    .end local v3    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/AxisAngle4f;)Z
    .locals 4
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    const/4 v1, 0x0

    .line 460
    :try_start_0
    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->angle:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 463
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get([F)V
    .locals 2
    .param p1, "a"    # [F

    .prologue
    .line 231
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    aput v1, p1, v0

    .line 232
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    aput v1, p1, v0

    .line 233
    const/4 v0, 0x2

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    aput v1, p1, v0

    .line 234
    const/4 v0, 0x3

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    aput v1, p1, v0

    .line 235
    return-void
.end method

.method public final getAngle()F
    .locals 1

    .prologue
    .line 561
    iget v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    return v0
.end method

.method public final getX()F
    .locals 1

    .prologue
    .line 586
    iget v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    return v0
.end method

.method public final getY()F
    .locals 1

    .prologue
    .line 610
    iget v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    return v0
.end method

.method public final getZ()F
    .locals 1

    .prologue
    .line 634
    iget v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 525
    const-wide/16 v0, 0x1

    .line 526
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 527
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 528
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 529
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 530
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final set(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "angle"    # F

    .prologue
    .line 162
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 163
    iput p2, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 164
    iput p3, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 165
    iput p4, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 166
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 2
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 202
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 203
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 204
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 205
    iget-wide v0, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 206
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 1
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 189
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 190
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 191
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 192
    iget v0, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 193
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 416
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 417
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 418
    iget-wide v8, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v8, v10

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 419
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v8, v9

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v10, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v10, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-double v4, v8

    .line 421
    .local v4, "mag":D
    const-wide v8, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 422
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 423
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v6, v8, v4

    .line 424
    .local v6, "sin":D
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    iget-wide v10, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v12, p1, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v10, v12

    iget-wide v12, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v10, v12

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v10, v12

    mul-double v0, v8, v10

    .line 426
    .local v0, "cos":D
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 428
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double v2, v8, v4

    .line 429
    .local v2, "invMag":D
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 430
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 431
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 438
    .end local v0    # "cos":D
    .end local v2    # "invMag":D
    .end local v6    # "sin":D
    :goto_0
    return-void

    .line 433
    :cond_0
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 434
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 435
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 436
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 381
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v8, v9

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 382
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v8, v9

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 383
    iget v8, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v8, v9

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 384
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v8, v9

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v10, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    iget v9, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v10, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    float-to-double v4, v8

    .line 385
    .local v4, "mag":D
    const-wide v8, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v8, v4, v8

    if-lez v8, :cond_0

    .line 386
    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    .line 387
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    mul-double v6, v8, v4

    .line 388
    .local v6, "sin":D
    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v10, v11

    float-to-double v10, v10

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v10, v12

    mul-double v0, v8, v10

    .line 390
    .local v0, "cos":D
    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v8

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 392
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    div-double v2, v8, v4

    .line 393
    .local v2, "invMag":D
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 394
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 395
    iget v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v8, v8

    mul-double/2addr v8, v2

    double-to-float v8, v8

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 403
    .end local v0    # "cos":D
    .end local v2    # "invMag":D
    .end local v6    # "sin":D
    :goto_0
    return-void

    .line 397
    :cond_0
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 398
    const/high16 v8, 0x3f800000    # 1.0f

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 399
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 400
    const/4 v8, 0x0

    iput v8, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 18
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 342
    new-instance v6, Ljavax/vecmath/Matrix3d;

    invoke-direct {v6}, Ljavax/vecmath/Matrix3d;-><init>()V

    .line 344
    .local v6, "m3d":Ljavax/vecmath/Matrix3d;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljavax/vecmath/Matrix4d;->get(Ljavax/vecmath/Matrix3d;)V

    .line 347
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 348
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 349
    iget-wide v12, v6, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v12, v14

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 350
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v7, v12

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v12, v13

    add-float/2addr v7, v12

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v12, v13

    add-float/2addr v7, v12

    float-to-double v8, v7

    .line 352
    .local v8, "mag":D
    const-wide v12, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v7, v8, v12

    if-lez v7, :cond_0

    .line 353
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 354
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double v10, v12, v8

    .line 355
    .local v10, "sin":D
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    iget-wide v14, v6, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m11:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    iget-wide v0, v6, Ljavax/vecmath/Matrix3d;->m22:D

    move-wide/from16 v16, v0

    add-double v14, v14, v16

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v14, v14, v16

    mul-double v2, v12, v14

    .line 356
    .local v2, "cos":D
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 358
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double v4, v12, v8

    .line 359
    .local v4, "invMag":D
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 360
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 361
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 369
    .end local v2    # "cos":D
    .end local v4    # "invMag":D
    .end local v10    # "sin":D
    :goto_0
    return-void

    .line 363
    :cond_0
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 364
    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 365
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 366
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 18
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 303
    new-instance v6, Ljavax/vecmath/Matrix3f;

    invoke-direct {v6}, Ljavax/vecmath/Matrix3f;-><init>()V

    .line 305
    .local v6, "m3f":Ljavax/vecmath/Matrix3f;
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Ljavax/vecmath/Matrix4f;->get(Ljavax/vecmath/Matrix3f;)V

    .line 307
    iget v7, v6, Ljavax/vecmath/Matrix3f;->m21:F

    iget v12, v6, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 308
    iget v7, v6, Ljavax/vecmath/Matrix3f;->m02:F

    iget v12, v6, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 309
    iget v7, v6, Ljavax/vecmath/Matrix3f;->m10:F

    iget v12, v6, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 310
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v7, v12

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v12, v13

    add-float/2addr v7, v12

    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v12, v13

    add-float/2addr v7, v12

    float-to-double v8, v7

    .line 312
    .local v8, "mag":D
    const-wide v12, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v7, v8, v12

    if-lez v7, :cond_0

    .line 313
    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    .line 314
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    mul-double v10, v12, v8

    .line 315
    .local v10, "sin":D
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    iget v7, v6, Ljavax/vecmath/Matrix3f;->m00:F

    iget v14, v6, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v7, v14

    iget v14, v6, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v7, v14

    float-to-double v14, v7

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    sub-double v14, v14, v16

    mul-double v2, v12, v14

    .line 317
    .local v2, "cos":D
    invoke-static {v10, v11, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 318
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    div-double v4, v12, v8

    .line 319
    .local v4, "invMag":D
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 320
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 321
    move-object/from16 v0, p0

    iget v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v12, v7

    mul-double/2addr v12, v4

    double-to-float v7, v12

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 330
    .end local v2    # "cos":D
    .end local v4    # "invMag":D
    .end local v10    # "sin":D
    :goto_0
    return-void

    .line 323
    :cond_0
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 324
    const/high16 v7, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 325
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 326
    const/4 v7, 0x0

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 11
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const/4 v10, 0x0

    .line 275
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 277
    .local v2, "mag":D
    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 278
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 279
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, v2

    .line 281
    .local v0, "invMag":D
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 282
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 283
    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 284
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 291
    .end local v0    # "invMag":D
    :goto_0
    return-void

    .line 286
    :cond_0
    iput v10, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 287
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 288
    iput v10, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 289
    iput v10, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 8
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/4 v7, 0x0

    .line 247
    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v2, v4

    .line 249
    .local v2, "mag":D
    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v4, v2, v4

    if-lez v4, :cond_0

    .line 250
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 251
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, v2

    .line 253
    .local v0, "invMag":D
    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 254
    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 255
    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 256
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v6, v6

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v6

    mul-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 263
    .end local v0    # "invMag":D
    :goto_0
    return-void

    .line 258
    :cond_0
    iput v7, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 259
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 260
    iput v7, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 261
    iput v7, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Vector3f;F)V
    .locals 1
    .param p1, "axis"    # Ljavax/vecmath/Vector3f;
    .param p2, "angle"    # F

    .prologue
    .line 218
    iget v0, p1, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 219
    iget v0, p1, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 220
    iget v0, p1, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 221
    iput p2, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 222
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "a"    # [F

    .prologue
    .line 176
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 177
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 178
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 179
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 180
    return-void
.end method

.method public final setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 574
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    .line 575
    return-void
.end method

.method public final setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 598
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    .line 599
    return-void
.end method

.method public final setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 622
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    .line 623
    return-void
.end method

.method public final setZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 646
    iput p1, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    .line 647
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 447
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/AxisAngle4f;->angle:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
