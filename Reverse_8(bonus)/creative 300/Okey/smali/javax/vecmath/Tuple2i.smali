.class public abstract Ljavax/vecmath/Tuple2i;
.super Ljava/lang/Object;
.source "Tuple2i.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = -0x315863cfd9a9c926L


# instance fields
.field public x:I

.field public y:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 95
    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 96
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 65
    iput p2, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 66
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 86
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 87
    return-void
.end method

.method public constructor <init>([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 75
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 76
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 438
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 439
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 440
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 383
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 384
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 385
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 168
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple2i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 169
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple2i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 170
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2i;Ljavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 158
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple2i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 159
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple2i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 160
    return-void
.end method

.method public final clamp(II)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 394
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    if-le v0, p2, :cond_2

    .line 395
    iput p2, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 400
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    if-le v0, p2, :cond_3

    .line 401
    iput p2, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 405
    :cond_1
    :goto_1
    return-void

    .line 396
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    if-ge v0, p1, :cond_0

    .line 397
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    goto :goto_0

    .line 402
    :cond_3
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    if-ge v0, p1, :cond_1

    .line 403
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    goto :goto_1
.end method

.method public final clamp(IILjavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "t"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 317
    iget v0, p3, Ljavax/vecmath/Tuple2i;->x:I

    if-le v0, p2, :cond_0

    .line 318
    iput p2, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 325
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple2i;->y:I

    if-le v0, p2, :cond_2

    .line 326
    iput p2, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 332
    :goto_1
    return-void

    .line 319
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple2i;->x:I

    if-ge v0, p1, :cond_1

    .line 320
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    goto :goto_0

    .line 322
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    goto :goto_0

    .line 327
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple2i;->y:I

    if-ge v0, p1, :cond_3

    .line 328
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    goto :goto_1

    .line 330
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    goto :goto_1
.end method

.method public final clampMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 426
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    if-le v0, p1, :cond_0

    .line 427
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 429
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    if-le v0, p1, :cond_1

    .line 430
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 431
    :cond_1
    return-void
.end method

.method public final clampMax(ILjavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "max"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 363
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    if-le v0, p1, :cond_0

    .line 364
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 369
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    if-le v0, p1, :cond_1

    .line 370
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 374
    :goto_1
    return-void

    .line 366
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    goto :goto_0

    .line 372
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    goto :goto_1
.end method

.method public final clampMin(I)V
    .locals 1
    .param p1, "min"    # I

    .prologue
    .line 413
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    if-ge v0, p1, :cond_0

    .line 414
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 416
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    if-ge v0, p1, :cond_1

    .line 417
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 418
    :cond_1
    return-void
.end method

.method public final clampMin(ILjavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 342
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    if-ge v0, p1, :cond_0

    .line 343
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 348
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    if-ge v0, p1, :cond_1

    .line 349
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 353
    :goto_1
    return-void

    .line 345
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    goto :goto_0

    .line 351
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    goto :goto_1
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 452
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 453
    :catch_0
    move-exception v0

    .line 455
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 281
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple2i;

    move-object v3, v0

    .line 282
    .local v3, "t2":Ljavax/vecmath/Tuple2i;
    iget v5, p0, Ljavax/vecmath/Tuple2i;->x:I

    iget v6, v3, Ljavax/vecmath/Tuple2i;->x:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple2i;->y:I

    iget v6, v3, Ljavax/vecmath/Tuple2i;->y:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    .line 288
    .end local v3    # "t2":Ljavax/vecmath/Tuple2i;
    :cond_0
    :goto_0
    return v4

    .line 284
    :catch_0
    move-exception v2

    .line 285
    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 287
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 288
    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 147
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    .line 148
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    .line 149
    return-void
.end method

.method public final get([I)V
    .locals 2
    .param p1, "t"    # [I

    .prologue
    .line 137
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple2i;->x:I

    aput v1, p1, v0

    .line 138
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple2i;->y:I

    aput v1, p1, v0

    .line 139
    return-void
.end method

.method public final getX()I
    .locals 1

    .prologue
    .line 468
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    return v0
.end method

.method public final getY()I
    .locals 1

    .prologue
    .line 492
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 302
    const-wide/16 v0, 0x1

    .line 303
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple2i;->x:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 304
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple2i;->y:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 305
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 211
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 212
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 201
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 202
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 203
    return-void
.end method

.method public final scale(I)V
    .locals 1
    .param p1, "s"    # I

    .prologue
    .line 233
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 234
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 235
    return-void
.end method

.method public final scale(ILjavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 222
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 223
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 224
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 258
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple2i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 259
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple2i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 260
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple2i;Ljavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple2i;
    .param p3, "t2"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 246
    iget v0, p2, Ljavax/vecmath/Tuple2i;->x:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple2i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 247
    iget v0, p2, Ljavax/vecmath/Tuple2i;->y:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple2i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 248
    return-void
.end method

.method public final set(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 106
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 107
    iput p2, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 108
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 127
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 128
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 129
    return-void
.end method

.method public final set([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 117
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 118
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 119
    return-void
.end method

.method public final setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 480
    iput p1, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 481
    return-void
.end method

.method public final setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 504
    iput p1, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 505
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 191
    iget v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple2i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 192
    iget v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple2i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 193
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2i;Ljavax/vecmath/Tuple2i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2i;

    .prologue
    .line 180
    iget v0, p1, Ljavax/vecmath/Tuple2i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple2i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->x:I

    .line 181
    iget v0, p1, Ljavax/vecmath/Tuple2i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple2i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2i;->y:I

    .line 182
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 269
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple2i;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple2i;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
