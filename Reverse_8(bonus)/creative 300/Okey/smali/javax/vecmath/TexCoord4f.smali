.class public Ljavax/vecmath/TexCoord4f;
.super Ljavax/vecmath/Tuple4f;
.source "TexCoord4f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x30d182bfea20bcf1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 107
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 58
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/TexCoord4f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/TexCoord4f;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 88
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 67
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>([F)V

    .line 68
    return-void
.end method
