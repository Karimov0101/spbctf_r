.class public Ljavax/vecmath/GVector;
.super Ljava/lang/Object;
.source "GVector.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x1369b6b27719fba8L


# instance fields
.field private length:I

.field values:[D


# direct methods
.method public constructor <init>(I)V
    .locals 4
    .param p1, "length"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput p1, p0, Ljavax/vecmath/GVector;->length:I

    .line 59
    new-array v1, p1, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 60
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/GVector;)V
    .locals 4
    .param p1, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    new-array v1, v1, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 89
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    iput v1, p0, Ljavax/vecmath/GVector;->length:I

    .line 90
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 5
    .param p1, "tuple"    # Ljavax/vecmath/Tuple2f;

    .prologue
    const/4 v4, 0x2

    .line 99
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    new-array v0, v4, [D

    iput-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    .line 101
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x0

    iget v2, p1, Ljavax/vecmath/Tuple2f;->x:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 102
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x1

    iget v2, p1, Ljavax/vecmath/Tuple2f;->y:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 103
    iput v4, p0, Ljavax/vecmath/GVector;->length:I

    .line 104
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 5
    .param p1, "tuple"    # Ljavax/vecmath/Tuple3d;

    .prologue
    const/4 v4, 0x3

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    new-array v0, v4, [D

    iput-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    .line 128
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x0

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    aput-wide v2, v0, v1

    .line 129
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x1

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->y:D

    aput-wide v2, v0, v1

    .line 130
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x2

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->z:D

    aput-wide v2, v0, v1

    .line 131
    iput v4, p0, Ljavax/vecmath/GVector;->length:I

    .line 132
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 5
    .param p1, "tuple"    # Ljavax/vecmath/Tuple3f;

    .prologue
    const/4 v4, 0x3

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    new-array v0, v4, [D

    iput-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    .line 114
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x0

    iget v2, p1, Ljavax/vecmath/Tuple3f;->x:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 115
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x1

    iget v2, p1, Ljavax/vecmath/Tuple3f;->y:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 116
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x2

    iget v2, p1, Ljavax/vecmath/Tuple3f;->z:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 117
    iput v4, p0, Ljavax/vecmath/GVector;->length:I

    .line 118
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 5
    .param p1, "tuple"    # Ljavax/vecmath/Tuple4d;

    .prologue
    const/4 v4, 0x4

    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    new-array v0, v4, [D

    iput-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    .line 157
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x0

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    aput-wide v2, v0, v1

    .line 158
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x1

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    aput-wide v2, v0, v1

    .line 159
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x2

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    aput-wide v2, v0, v1

    .line 160
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x3

    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    aput-wide v2, v0, v1

    .line 161
    iput v4, p0, Ljavax/vecmath/GVector;->length:I

    .line 162
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 5
    .param p1, "tuple"    # Ljavax/vecmath/Tuple4f;

    .prologue
    const/4 v4, 0x4

    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    new-array v0, v4, [D

    iput-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    .line 142
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x0

    iget v2, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 143
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x1

    iget v2, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 144
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x2

    iget v2, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 145
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v1, 0x3

    iget v2, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 146
    iput v4, p0, Ljavax/vecmath/GVector;->length:I

    .line 147
    return-void
.end method

.method public constructor <init>([D)V
    .locals 4
    .param p1, "vector"    # [D

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    array-length v1, p1

    iput v1, p0, Ljavax/vecmath/GVector;->length:I

    .line 75
    array-length v1, p1

    new-array v1, v1, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 76
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, p1, v0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method

.method public constructor <init>([DI)V
    .locals 4
    .param p1, "vector"    # [D
    .param p2, "length"    # I

    .prologue
    .line 174
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 177
    iput p2, p0, Ljavax/vecmath/GVector;->length:I

    .line 178
    new-array v1, p2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 179
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p2, :cond_0

    .line 180
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, p1, v0

    aput-wide v2, v1, v0

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    return-void
.end method


# virtual methods
.method public final LUDBackSolve(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;)V
    .locals 10
    .param p1, "LU"    # Ljavax/vecmath/GMatrix;
    .param p2, "b"    # Ljavax/vecmath/GVector;
    .param p3, "permutation"    # Ljavax/vecmath/GVector;

    .prologue
    .line 808
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v7, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int v4, v6, v7

    .line 810
    .local v4, "size":I
    new-array v5, v4, [D

    .line 811
    .local v5, "temp":[D
    new-array v2, v4, [D

    .line 812
    .local v2, "result":[D
    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v6

    new-array v3, v6, [I

    .line 815
    .local v3, "row_perm":[I
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v7

    if-eq v6, v7, :cond_0

    .line 816
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GVector16"

    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 819
    :cond_0
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p3}, Ljavax/vecmath/GVector;->getSize()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 820
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GVector24"

    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 823
    :cond_1
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v7, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v6, v7, :cond_2

    .line 824
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GVector25"

    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 827
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v6, :cond_4

    .line 828
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v6, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v6, :cond_3

    .line 829
    iget v6, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v6, v0

    add-int/2addr v6, v1

    iget-object v7, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v7, v7, v0

    aget-wide v8, v7, v1

    aput-wide v8, v5, v6

    .line 828
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 827
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 833
    .end local v1    # "j":I
    :cond_4
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v4, :cond_5

    const-wide/16 v6, 0x0

    aput-wide v6, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 834
    :cond_5
    const/4 v0, 0x0

    :goto_3
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v6, :cond_6

    iget v6, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v6, v0

    iget-object v7, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v8, v7, v0

    aput-wide v8, v2, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 835
    :cond_6
    const/4 v0, 0x0

    :goto_4
    iget v6, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v6, :cond_7

    iget-object v6, p3, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v6, v0

    double-to-int v6, v6

    aput v6, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 837
    :cond_7
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-static {v6, v5, v3, v2}, Ljavax/vecmath/GMatrix;->luBacksubstitution(I[D[I[D)V

    .line 839
    const/4 v0, 0x0

    :goto_5
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v6, :cond_8

    iget-object v6, p0, Ljavax/vecmath/GVector;->values:[D

    iget v7, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v7, v0

    aget-wide v8, v2, v7

    aput-wide v8, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 840
    :cond_8
    return-void
.end method

.method public final SVDBackSolve(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GVector;)V
    .locals 3
    .param p1, "U"    # Ljavax/vecmath/GMatrix;
    .param p2, "W"    # Ljavax/vecmath/GMatrix;
    .param p3, "V"    # Ljavax/vecmath/GMatrix;
    .param p4, "b"    # Ljavax/vecmath/GVector;

    .prologue
    .line 774
    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p4}, Ljavax/vecmath/GVector;->getSize()I

    move-result v2

    if-ne v1, v2, :cond_0

    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v2, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v1, v2, :cond_0

    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v2, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v1, v2, :cond_1

    .line 777
    :cond_0
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector15"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 780
    :cond_1
    iget v1, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget-object v2, p0, Ljavax/vecmath/GVector;->values:[D

    array-length v2, v2

    if-ne v1, v2, :cond_2

    iget v1, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget v2, p3, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v1, v2, :cond_2

    iget v1, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget v2, p3, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v1, v2, :cond_3

    .line 783
    :cond_2
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector23"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 786
    :cond_3
    new-instance v0, Ljavax/vecmath/GMatrix;

    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v2, p2, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-direct {v0, v1, v2}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 787
    .local v0, "tmp":Ljavax/vecmath/GMatrix;
    invoke-virtual {v0, p1, p3}, Ljavax/vecmath/GMatrix;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 788
    invoke-virtual {v0, p1, p2}, Ljavax/vecmath/GMatrix;->mulTransposeRight(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 789
    invoke-virtual {v0}, Ljavax/vecmath/GMatrix;->invert()V

    .line 790
    invoke-virtual {p0, v0, p4}, Ljavax/vecmath/GVector;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GVector;)V

    .line 792
    return-void
.end method

.method public final add(Ljavax/vecmath/GVector;)V
    .locals 6
    .param p1, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 328
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 329
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector4"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 331
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 332
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v1, v0

    iget-object v4, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 331
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 334
    :cond_1
    return-void
.end method

.method public final add(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;)V
    .locals 6
    .param p1, "vector1"    # Ljavax/vecmath/GVector;
    .param p2, "vector2"    # Ljavax/vecmath/GVector;

    .prologue
    .line 346
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    iget v2, p2, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 347
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector5"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 349
    :cond_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_1

    .line 350
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector6"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 352
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 353
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    iget-object v4, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 354
    :cond_2
    return-void
.end method

.method public final angle(Ljavax/vecmath/GVector;)D
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/GVector;

    .prologue
    .line 851
    invoke-virtual {p0, p1}, Ljavax/vecmath/GVector;->dot(Ljavax/vecmath/GVector;)D

    move-result-wide v0

    invoke-virtual {p0}, Ljavax/vecmath/GVector;->norm()D

    move-result-wide v2

    invoke-virtual {p1}, Ljavax/vecmath/GVector;->norm()D

    move-result-wide v4

    mul-double/2addr v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public clone()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 916
    const/4 v2, 0x0

    .line 918
    .local v2, "v1":Ljavax/vecmath/GVector;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "v1":Ljavax/vecmath/GVector;
    check-cast v2, Ljavax/vecmath/GVector;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 925
    .restart local v2    # "v1":Ljavax/vecmath/GVector;
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    new-array v3, v3, [D

    iput-object v3, v2, Ljavax/vecmath/GVector;->values:[D

    .line 926
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v3, :cond_0

    .line 927
    iget-object v3, v2, Ljavax/vecmath/GVector;->values:[D

    iget-object v4, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v1

    aput-wide v4, v3, v1

    .line 926
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 919
    .end local v1    # "i":I
    .end local v2    # "v1":Ljavax/vecmath/GVector;
    :catch_0
    move-exception v0

    .line 921
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v3, Ljava/lang/InternalError;

    invoke-direct {v3}, Ljava/lang/InternalError;-><init>()V

    throw v3

    .line 930
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v1    # "i":I
    .restart local v2    # "v1":Ljavax/vecmath/GVector;
    :cond_0
    return-object v2
.end method

.method public final dot(Ljavax/vecmath/GVector;)D
    .locals 8
    .param p1, "v1"    # Ljavax/vecmath/GVector;

    .prologue
    .line 750
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v4, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v4, :cond_0

    .line 751
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v4, "GVector14"

    invoke-static {v4}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 753
    :cond_0
    const-wide/16 v2, 0x0

    .line 754
    .local v2, "result":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 755
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v1, v0

    iget-object v1, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v1, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 754
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 757
    :cond_1
    return-wide v2
.end method

.method public epsilonEquals(Ljavax/vecmath/GVector;D)Z
    .locals 8
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "epsilon"    # D

    .prologue
    const/4 v3, 0x0

    .line 734
    iget v4, p0, Ljavax/vecmath/GVector;->length:I

    iget v5, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v4, v5, :cond_1

    .line 740
    :cond_0
    :goto_0
    return v3

    .line 736
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v2, v4, :cond_3

    .line 737
    iget-object v4, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v2

    iget-object v6, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v6, v2

    sub-double v0, v4, v6

    .line 738
    .local v0, "diff":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v0, v4

    if-gez v4, :cond_2

    neg-double v0, v0

    .end local v0    # "diff":D
    :cond_2
    cmpl-double v4, v0, p2

    if-gtz v4, :cond_0

    .line 736
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 740
    :cond_3
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o1"    # Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    .line 707
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/GVector;

    move-object v5, v0

    .line 709
    .local v5, "v2":Ljavax/vecmath/GVector;
    iget v7, p0, Ljavax/vecmath/GVector;->length:I

    iget v8, v5, Ljavax/vecmath/GVector;->length:I

    if-eq v7, v8, :cond_1

    .line 717
    .end local v5    # "v2":Ljavax/vecmath/GVector;
    :cond_0
    :goto_0
    return v6

    .line 711
    .restart local v5    # "v2":Ljavax/vecmath/GVector;
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget v7, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v4, v7, :cond_2

    .line 712
    iget-object v7, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v8, v7, v4

    iget-object v7, v5, Ljavax/vecmath/GVector;->values:[D

    aget-wide v10, v7, v4
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v7, v8, v10

    if-nez v7, :cond_0

    .line 711
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 714
    :cond_2
    const/4 v6, 0x1

    goto :goto_0

    .line 716
    .end local v4    # "i":I
    .end local v5    # "v2":Ljavax/vecmath/GVector;
    :catch_0
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 717
    .end local v2    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/GVector;)Z
    .locals 8
    .param p1, "vector1"    # Ljavax/vecmath/GVector;

    .prologue
    const/4 v2, 0x0

    .line 686
    :try_start_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    iget v4, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v3, v4, :cond_1

    .line 694
    :cond_0
    :goto_0
    return v2

    .line 688
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v3, :cond_2

    .line 689
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v3, v1

    iget-object v3, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v3, v1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v3, v4, v6

    if-nez v3, :cond_0

    .line 688
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 692
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    .line 694
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final getElement(I)D
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 625
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public final getSize()I
    .locals 1

    .prologue
    .line 615
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    array-length v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 667
    const-wide/16 v0, 0x1

    .line 669
    .local v0, "bits":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v2, v3, :cond_0

    .line 670
    const-wide/16 v4, 0x1f

    mul-long/2addr v4, v0

    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v3, v2

    invoke-static {v6, v7}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v6

    add-long v0, v4, v6

    .line 669
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 673
    :cond_0
    const/16 v3, 0x20

    shr-long v4, v0, v3

    xor-long/2addr v4, v0

    long-to-int v3, v4

    return v3
.end method

.method public final interpolate(Ljavax/vecmath/GVector;D)V
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "alpha"    # D

    .prologue
    .line 899
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    iget v2, p0, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 900
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector22"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 902
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 903
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, p2

    iget-object v4, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    mul-double/2addr v2, v4

    iget-object v4, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    mul-double/2addr v4, p2

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 902
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 905
    :cond_1
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/GVector;F)V
    .locals 2
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "alpha"    # F

    .prologue
    .line 867
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Ljavax/vecmath/GVector;->interpolate(Ljavax/vecmath/GVector;D)V

    .line 868
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;D)V
    .locals 7
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "v2"    # Ljavax/vecmath/GVector;
    .param p3, "alpha"    # D

    .prologue
    .line 880
    iget v1, p2, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 881
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector20"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 883
    :cond_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_1

    .line 884
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector21"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 886
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 887
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, p3

    iget-object v4, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    mul-double/2addr v2, v4

    iget-object v4, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    mul-double/2addr v4, p3

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 886
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 889
    :cond_2
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;F)V
    .locals 2
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "v2"    # Ljavax/vecmath/GVector;
    .param p3, "alpha"    # F

    .prologue
    .line 859
    float-to-double v0, p3

    invoke-virtual {p0, p1, p2, v0, v1}, Ljavax/vecmath/GVector;->interpolate(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;D)V

    .line 860
    return-void
.end method

.method public final mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GVector;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "v1"    # Ljavax/vecmath/GVector;

    .prologue
    .line 401
    invoke-virtual {p1}, Ljavax/vecmath/GMatrix;->getNumCol()I

    move-result v3

    iget v4, p2, Ljavax/vecmath/GVector;->length:I

    if-eq v3, v4, :cond_0

    .line 402
    new-instance v3, Ljavax/vecmath/MismatchedSizeException;

    const-string v4, "GVector10"

    invoke-static {v4}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 404
    :cond_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    invoke-virtual {p1}, Ljavax/vecmath/GMatrix;->getNumRow()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 405
    new-instance v3, Ljavax/vecmath/MismatchedSizeException;

    const-string v4, "GVector11"

    invoke-static {v4}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 408
    :cond_1
    if-eq p2, p0, :cond_2

    .line 409
    iget-object v2, p2, Ljavax/vecmath/GVector;->values:[D

    .line 414
    .local v2, "v":[D
    :goto_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v1, v3, -0x1

    .local v1, "j":I
    :goto_1
    if-ltz v1, :cond_4

    .line 415
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v1

    .line 416
    iget v3, p2, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_2
    if-ltz v0, :cond_3

    .line 417
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v3, v1

    iget-object v6, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v1

    aget-wide v6, v6, v0

    aget-wide v8, v2, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v3, v1

    .line 416
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 411
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "v":[D
    :cond_2
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    invoke-virtual {v3}, [D->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [D

    move-object v2, v3

    check-cast v2, [D

    .restart local v2    # "v":[D
    goto :goto_0

    .line 414
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 420
    .end local v0    # "i":I
    :cond_4
    return-void
.end method

.method public final mul(Ljavax/vecmath/GVector;Ljavax/vecmath/GMatrix;)V
    .locals 10
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 433
    invoke-virtual {p2}, Ljavax/vecmath/GMatrix;->getNumRow()I

    move-result v3

    iget v4, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v3, v4, :cond_0

    .line 434
    new-instance v3, Ljavax/vecmath/MismatchedSizeException;

    const-string v4, "GVector12"

    invoke-static {v4}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 436
    :cond_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    invoke-virtual {p2}, Ljavax/vecmath/GMatrix;->getNumCol()I

    move-result v4

    if-eq v3, v4, :cond_1

    .line 437
    new-instance v3, Ljavax/vecmath/MismatchedSizeException;

    const-string v4, "GVector13"

    invoke-static {v4}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 440
    :cond_1
    if-eq p1, p0, :cond_2

    .line 441
    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    .line 446
    .local v2, "v":[D
    :goto_0
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v1, v3, -0x1

    .local v1, "j":I
    :goto_1
    if-ltz v1, :cond_4

    .line 447
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v1

    .line 448
    iget v3, p1, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v0, v3, -0x1

    .local v0, "i":I
    :goto_2
    if-ltz v0, :cond_3

    .line 449
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v3, v1

    iget-object v6, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v0

    aget-wide v6, v6, v1

    aget-wide v8, v2, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v3, v1

    .line 448
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 443
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "v":[D
    :cond_2
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    invoke-virtual {v3}, [D->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [D

    move-object v2, v3

    check-cast v2, [D

    .restart local v2    # "v":[D
    goto :goto_0

    .line 446
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 452
    .end local v0    # "i":I
    :cond_4
    return-void
.end method

.method public final negate()V
    .locals 6

    .prologue
    .line 458
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 459
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v1, v0

    const-wide/high16 v4, -0x4010000000000000L    # -1.0

    mul-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 458
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 461
    :cond_0
    return-void
.end method

.method public final norm()D
    .locals 8

    .prologue
    .line 192
    const-wide/16 v2, 0x0

    .line 195
    .local v2, "sq":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    .line 196
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v1, v0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v1, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 199
    :cond_0
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public final normSquared()D
    .locals 8

    .prologue
    .line 210
    const-wide/16 v2, 0x0

    .line 213
    .local v2, "sq":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    .line 214
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v1, v0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v1, v0

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    return-wide v2
.end method

.method public final normalize()V
    .locals 10

    .prologue
    .line 250
    const-wide/16 v4, 0x0

    .line 253
    .local v4, "sq":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    .line 254
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v1, v0

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v8, v1, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 258
    :cond_0
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    div-double v2, v6, v8

    .line 260
    .local v2, "invMag":D
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 261
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v6, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v6, v0

    mul-double/2addr v6, v2

    aput-wide v6, v1, v0

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 264
    :cond_1
    return-void
.end method

.method public final normalize(Ljavax/vecmath/GVector;)V
    .locals 10
    .param p1, "v1"    # Ljavax/vecmath/GVector;

    .prologue
    .line 226
    const-wide/16 v4, 0x0

    .line 229
    .local v4, "sq":D
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v6, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v6, :cond_0

    .line 230
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v6, "GVector0"

    invoke-static {v6}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 232
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 233
    iget-object v1, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v1, v0

    iget-object v1, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v8, v1, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_1
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    div-double v2, v6, v8

    .line 239
    .local v2, "invMag":D
    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 240
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v6, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v6, v0

    mul-double/2addr v6, v2

    aput-wide v6, v1, v0

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 242
    :cond_2
    return-void
.end method

.method public final scale(D)V
    .locals 5
    .param p1, "s"    # D

    .prologue
    .line 291
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    .line 292
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    mul-double/2addr v2, p1

    aput-wide v2, v1, v0

    .line 291
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 294
    :cond_0
    return-void
.end method

.method public final scale(DLjavax/vecmath/GVector;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "v1"    # Ljavax/vecmath/GVector;

    .prologue
    .line 275
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p3, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 276
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector1"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 278
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 279
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p3, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    mul-double/2addr v2, p1

    aput-wide v2, v1, v0

    .line 278
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 281
    :cond_1
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/GVector;Ljavax/vecmath/GVector;)V
    .locals 7
    .param p1, "s"    # D
    .param p3, "v1"    # Ljavax/vecmath/GVector;
    .param p4, "v2"    # Ljavax/vecmath/GVector;

    .prologue
    .line 308
    iget v1, p4, Ljavax/vecmath/GVector;->length:I

    iget v2, p3, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 309
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector2"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 311
    :cond_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p3, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_1

    .line 312
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector3"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 314
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 315
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p3, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    mul-double/2addr v2, p1

    iget-object v4, p4, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    add-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 314
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 317
    :cond_2
    return-void
.end method

.method public final set(Ljavax/vecmath/GVector;)V
    .locals 4
    .param p1, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 514
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 515
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    iput v1, p0, Ljavax/vecmath/GVector;->length:I

    .line 516
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    new-array v1, v1, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 517
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 518
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 520
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 521
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    .line 520
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 522
    :cond_1
    iget v0, p1, Ljavax/vecmath/GVector;->length:I

    :goto_2
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 523
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 525
    :cond_2
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2f;)V
    .locals 6
    .param p1, "tuple"    # Ljavax/vecmath/Tuple2f;

    .prologue
    const/4 v2, 0x2

    .line 533
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 534
    iput v2, p0, Ljavax/vecmath/GVector;->length:I

    .line 535
    new-array v1, v2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 537
    :cond_0
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x0

    iget v3, p1, Ljavax/vecmath/Tuple2f;->x:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 538
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x1

    iget v3, p1, Ljavax/vecmath/Tuple2f;->y:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 539
    const/4 v0, 0x2

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 541
    :cond_1
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3d;)V
    .locals 6
    .param p1, "tuple"    # Ljavax/vecmath/Tuple3d;

    .prologue
    const/4 v2, 0x3

    .line 565
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 566
    iput v2, p0, Ljavax/vecmath/GVector;->length:I

    .line 567
    new-array v1, v2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 569
    :cond_0
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x0

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->x:D

    aput-wide v4, v1, v2

    .line 570
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x1

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->y:D

    aput-wide v4, v1, v2

    .line 571
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x2

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->z:D

    aput-wide v4, v1, v2

    .line 572
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 573
    :cond_1
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3f;)V
    .locals 6
    .param p1, "tuple"    # Ljavax/vecmath/Tuple3f;

    .prologue
    const/4 v2, 0x3

    .line 549
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 550
    iput v2, p0, Ljavax/vecmath/GVector;->length:I

    .line 551
    new-array v1, v2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 553
    :cond_0
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x0

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 554
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x1

    iget v3, p1, Ljavax/vecmath/Tuple3f;->y:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 555
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x2

    iget v3, p1, Ljavax/vecmath/Tuple3f;->z:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 556
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 557
    :cond_1
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4d;)V
    .locals 6
    .param p1, "tuple"    # Ljavax/vecmath/Tuple4d;

    .prologue
    const/4 v2, 0x4

    .line 598
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 599
    iput v2, p0, Ljavax/vecmath/GVector;->length:I

    .line 600
    new-array v1, v2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 602
    :cond_0
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x0

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->x:D

    aput-wide v4, v1, v2

    .line 603
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x1

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->y:D

    aput-wide v4, v1, v2

    .line 604
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x2

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->z:D

    aput-wide v4, v1, v2

    .line 605
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x3

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->w:D

    aput-wide v4, v1, v2

    .line 606
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 607
    :cond_1
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4f;)V
    .locals 6
    .param p1, "tuple"    # Ljavax/vecmath/Tuple4f;

    .prologue
    const/4 v2, 0x4

    .line 581
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 582
    iput v2, p0, Ljavax/vecmath/GVector;->length:I

    .line 583
    new-array v1, v2, [D

    iput-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    .line 585
    :cond_0
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x0

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 586
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x1

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 587
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x2

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 588
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const/4 v2, 0x3

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v4, v3

    aput-wide v4, v1, v2

    .line 589
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 590
    :cond_1
    return-void
.end method

.method public final set([D)V
    .locals 4
    .param p1, "vector"    # [D

    .prologue
    .line 503
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 504
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, p1, v0

    aput-wide v2, v1, v0

    .line 503
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 505
    :cond_0
    return-void
.end method

.method public final setElement(ID)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # D

    .prologue
    .line 636
    iget-object v0, p0, Ljavax/vecmath/GVector;->values:[D

    aput-wide p2, v0, p1

    .line 637
    return-void
.end method

.method public final setSize(I)V
    .locals 6
    .param p1, "length"    # I

    .prologue
    .line 479
    new-array v2, p1, [D

    .line 482
    .local v2, "tmp":[D
    iget v3, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v3, p1, :cond_0

    .line 483
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    .line 487
    .local v1, "max":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 488
    iget-object v3, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v3, v0

    aput-wide v4, v2, v0

    .line 487
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 485
    .end local v0    # "i":I
    .end local v1    # "max":I
    :cond_0
    move v1, p1

    .restart local v1    # "max":I
    goto :goto_0

    .line 490
    .restart local v0    # "i":I
    :cond_1
    iput p1, p0, Ljavax/vecmath/GVector;->length:I

    .line 492
    iput-object v2, p0, Ljavax/vecmath/GVector;->values:[D

    .line 494
    return-void
.end method

.method public final sub(Ljavax/vecmath/GVector;)V
    .locals 6
    .param p1, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 365
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 366
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector7"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 368
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_1

    .line 369
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v1, v0

    iget-object v4, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    sub-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371
    :cond_1
    return-void
.end method

.method public final sub(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;)V
    .locals 6
    .param p1, "vector1"    # Ljavax/vecmath/GVector;
    .param p2, "vector2"    # Ljavax/vecmath/GVector;

    .prologue
    .line 384
    iget v1, p1, Ljavax/vecmath/GVector;->length:I

    iget v2, p2, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_0

    .line 385
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector8"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 387
    :cond_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    iget v2, p1, Ljavax/vecmath/GVector;->length:I

    if-eq v1, v2, :cond_1

    .line 388
    new-instance v1, Ljavax/vecmath/MismatchedSizeException;

    const-string v2, "GVector9"

    invoke-static {v2}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 390
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_2

    .line 391
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    iget-object v4, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v4, v0

    sub-double/2addr v2, v4

    aput-wide v2, v1, v0

    .line 390
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 392
    :cond_2
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 644
    new-instance v0, Ljava/lang/StringBuffer;

    iget v2, p0, Ljavax/vecmath/GVector;->length:I

    mul-int/lit8 v2, v2, 0x8

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 648
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v1, v2, :cond_0

    .line 649
    iget-object v2, p0, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 648
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 652
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public final zero()V
    .locals 4

    .prologue
    .line 467
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GVector;->length:I

    if-ge v0, v1, :cond_0

    .line 468
    iget-object v1, p0, Ljavax/vecmath/GVector;->values:[D

    const-wide/16 v2, 0x0

    aput-wide v2, v1, v0

    .line 467
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 470
    :cond_0
    return-void
.end method
