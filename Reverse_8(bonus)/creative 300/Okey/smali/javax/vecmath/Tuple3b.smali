.class public abstract Ljavax/vecmath/Tuple3b;
.super Ljava/lang/Object;
.source "Tuple3b.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = -0x6b6bdca0332bc04L


# instance fields
.field public x:B

.field public y:B

.field public z:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 114
    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 115
    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 116
    return-void
.end method

.method public constructor <init>(BBB)V
    .locals 0
    .param p1, "b1"    # B
    .param p2, "b2"    # B
    .param p3, "b3"    # B

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    iput-byte p1, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 79
    iput-byte p2, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 80
    iput-byte p3, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 81
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3b;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->x:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 103
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->y:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 104
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->z:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 105
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "t"    # [B

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 91
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 92
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 93
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 241
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 242
    :catch_0
    move-exception v0

    .line 244
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 208
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple3b;

    move-object v3, v0

    .line 209
    .local v3, "t2":Ljavax/vecmath/Tuple3b;
    iget-byte v5, p0, Ljavax/vecmath/Tuple3b;->x:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple3b;->x:B

    if-ne v5, v6, :cond_0

    iget-byte v5, p0, Ljavax/vecmath/Tuple3b;->y:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple3b;->y:B

    if-ne v5, v6, :cond_0

    iget-byte v5, p0, Ljavax/vecmath/Tuple3b;->z:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple3b;->z:B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    .line 212
    .end local v3    # "t2":Ljavax/vecmath/Tuple3b;
    :cond_0
    :goto_0
    return v4

    .line 211
    :catch_0
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 212
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple3b;)Z
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3b;

    .prologue
    const/4 v1, 0x0

    .line 193
    :try_start_0
    iget-byte v2, p0, Ljavax/vecmath/Tuple3b;->x:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple3b;->x:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Ljavax/vecmath/Tuple3b;->y:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple3b;->y:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Ljavax/vecmath/Tuple3b;->z:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple3b;->z:B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 195
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple3b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3b;

    .prologue
    .line 152
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple3b;->x:B

    .line 153
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple3b;->y:B

    .line 154
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple3b;->z:B

    .line 155
    return-void
.end method

.method public final get([B)V
    .locals 2
    .param p1, "t"    # [B

    .prologue
    .line 139
    const/4 v0, 0x0

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->x:B

    aput-byte v1, p1, v0

    .line 140
    const/4 v0, 0x1

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->y:B

    aput-byte v1, p1, v0

    .line 141
    const/4 v0, 0x2

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->z:B

    aput-byte v1, p1, v0

    .line 142
    return-void
.end method

.method public final getX()B
    .locals 1

    .prologue
    .line 257
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    return v0
.end method

.method public final getY()B
    .locals 1

    .prologue
    .line 281
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    return v0
.end method

.method public final getZ()B
    .locals 1

    .prologue
    .line 304
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 225
    iget-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x0

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->y:B

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->z:B

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method public final set(Ljavax/vecmath/Tuple3b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3b;

    .prologue
    .line 165
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->x:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 166
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->y:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 167
    iget-byte v0, p1, Ljavax/vecmath/Tuple3b;->z:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 168
    return-void
.end method

.method public final set([B)V
    .locals 1
    .param p1, "t"    # [B

    .prologue
    .line 178
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 179
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 180
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 181
    return-void
.end method

.method public final setX(B)V
    .locals 0
    .param p1, "x"    # B

    .prologue
    .line 269
    iput-byte p1, p0, Ljavax/vecmath/Tuple3b;->x:B

    .line 270
    return-void
.end method

.method public final setY(B)V
    .locals 0
    .param p1, "y"    # B

    .prologue
    .line 293
    iput-byte p1, p0, Ljavax/vecmath/Tuple3b;->y:B

    .line 294
    return-void
.end method

.method public final setZ(B)V
    .locals 0
    .param p1, "z"    # B

    .prologue
    .line 316
    iput-byte p1, p0, Ljavax/vecmath/Tuple3b;->z:B

    .line 317
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->x:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->y:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple3b;->z:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
