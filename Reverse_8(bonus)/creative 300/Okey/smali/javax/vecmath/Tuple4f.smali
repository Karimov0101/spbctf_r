.class public abstract Ljavax/vecmath/Tuple4f;
.super Ljava/lang/Object;
.source "Tuple4f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x62183735c5c58bc3L


# instance fields
.field public w:F

.field public x:F

.field public y:F

.field public z:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 127
    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 128
    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 129
    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 130
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 76
    iput p2, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 77
    iput p3, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 78
    iput p4, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 79
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 115
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 116
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 117
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 118
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 102
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 103
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 104
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 105
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 89
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 90
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 91
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 92
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 648
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 649
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 650
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 651
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 652
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 574
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 575
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 576
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 577
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 578
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 235
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 236
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 237
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 238
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->w:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 239
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 222
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 223
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 224
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 225
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->w:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 226
    return-void
.end method

.method public final clamp(FF)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 588
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_4

    .line 589
    iput p2, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 594
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_5

    .line 595
    iput p2, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 600
    :cond_1
    :goto_1
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_6

    .line 601
    iput p2, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 606
    :cond_2
    :goto_2
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_7

    .line 607
    iput p2, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 612
    :cond_3
    :goto_3
    return-void

    .line 590
    :cond_4
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 591
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    goto :goto_0

    .line 596
    :cond_5
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 597
    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    goto :goto_1

    .line 602
    :cond_6
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    .line 603
    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    goto :goto_2

    .line 608
    :cond_7
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    .line 609
    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    goto :goto_3
.end method

.method public final clamp(FFLjavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "t"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 461
    iget v0, p3, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_0

    .line 462
    iput p2, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 469
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_2

    .line 470
    iput p2, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 477
    :goto_1
    iget v0, p3, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_4

    .line 478
    iput p2, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 485
    :goto_2
    iget v0, p3, Ljavax/vecmath/Tuple4f;->w:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_6

    .line 486
    iput p2, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 493
    :goto_3
    return-void

    .line 463
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple4f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 464
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    goto :goto_0

    .line 466
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    goto :goto_0

    .line 471
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple4f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    .line 472
    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    goto :goto_1

    .line 474
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    goto :goto_1

    .line 479
    :cond_4
    iget v0, p3, Ljavax/vecmath/Tuple4f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_5

    .line 480
    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    goto :goto_2

    .line 482
    :cond_5
    iget v0, p3, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    goto :goto_2

    .line 487
    :cond_6
    iget v0, p3, Ljavax/vecmath/Tuple4f;->w:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_7

    .line 488
    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    goto :goto_3

    .line 490
    :cond_7
    iget v0, p3, Ljavax/vecmath/Tuple4f;->w:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    goto :goto_3
.end method

.method public final clampMax(F)V
    .locals 1
    .param p1, "max"    # F

    .prologue
    .line 635
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 636
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 637
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_2

    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 638
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_3

    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 640
    :cond_3
    return-void
.end method

.method public final clampMax(FLjavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "max"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 540
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    .line 541
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 546
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    .line 547
    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 552
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_2

    .line 553
    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 558
    :goto_2
    iget v0, p2, Ljavax/vecmath/Tuple4f;->w:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_3

    .line 559
    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 564
    :goto_3
    return-void

    .line 543
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    goto :goto_0

    .line 549
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    goto :goto_1

    .line 555
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    goto :goto_2

    .line 561
    :cond_3
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    goto :goto_3
.end method

.method public final clampMin(F)V
    .locals 1
    .param p1, "min"    # F

    .prologue
    .line 621
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 622
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 623
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 624
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 626
    :cond_3
    return-void
.end method

.method public final clampMin(FLjavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 504
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 505
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 510
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 511
    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 516
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_2

    .line 517
    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 522
    :goto_2
    iget v0, p2, Ljavax/vecmath/Tuple4f;->w:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    .line 523
    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 529
    :goto_3
    return-void

    .line 507
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    goto :goto_0

    .line 513
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    goto :goto_1

    .line 519
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    goto :goto_2

    .line 525
    :cond_3
    iget v0, p2, Ljavax/vecmath/Tuple4f;->w:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    goto :goto_3
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 698
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 699
    :catch_0
    move-exception v0

    .line 701
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple4f;F)Z
    .locals 5
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p2, "epsilon"    # F

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 414
    iget v2, p0, Ljavax/vecmath/Tuple4f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    sub-float v0, v2, v3

    .line 415
    .local v0, "diff":F
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 430
    :cond_0
    :goto_0
    return v1

    .line 416
    :cond_1
    cmpg-float v2, v0, v4

    if-gez v2, :cond_2

    neg-float v2, v0

    :goto_1
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 418
    iget v2, p0, Ljavax/vecmath/Tuple4f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    sub-float v0, v2, v3

    .line 419
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 420
    cmpg-float v2, v0, v4

    if-gez v2, :cond_3

    neg-float v2, v0

    :goto_2
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 422
    iget v2, p0, Ljavax/vecmath/Tuple4f;->z:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    sub-float v0, v2, v3

    .line 423
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 424
    cmpg-float v2, v0, v4

    if-gez v2, :cond_4

    neg-float v2, v0

    :goto_3
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 426
    iget v2, p0, Ljavax/vecmath/Tuple4f;->w:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    sub-float v0, v2, v3

    .line 427
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 428
    cmpg-float v2, v0, v4

    if-gez v2, :cond_5

    neg-float v2, v0

    :goto_4
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 430
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v2, v0

    .line 416
    goto :goto_1

    :cond_3
    move v2, v0

    .line 420
    goto :goto_2

    :cond_4
    move v2, v0

    .line 424
    goto :goto_3

    :cond_5
    move v2, v0

    .line 428
    goto :goto_4
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 391
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple4f;

    move-object v3, v0

    .line 392
    .local v3, "t2":Ljavax/vecmath/Tuple4f;
    iget v5, p0, Ljavax/vecmath/Tuple4f;->x:F

    iget v6, v3, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4f;->y:F

    iget v6, v3, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4f;->z:F

    iget v6, v3, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4f;->w:F

    iget v6, v3, Ljavax/vecmath/Tuple4f;->w:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 396
    .end local v3    # "t2":Ljavax/vecmath/Tuple4f;
    :cond_0
    :goto_0
    return v4

    .line 395
    :catch_0
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 396
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple4f;)Z
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    const/4 v1, 0x0

    .line 375
    :try_start_0
    iget v2, p0, Ljavax/vecmath/Tuple4f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple4f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple4f;->z:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple4f;->w:F

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 378
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 208
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    .line 209
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    .line 210
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    .line 211
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    iput v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    .line 212
    return-void
.end method

.method public final get([F)V
    .locals 2
    .param p1, "t"    # [F

    .prologue
    .line 195
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple4f;->x:F

    aput v1, p1, v0

    .line 196
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple4f;->y:F

    aput v1, p1, v0

    .line 197
    const/4 v0, 0x2

    iget v1, p0, Ljavax/vecmath/Tuple4f;->z:F

    aput v1, p1, v0

    .line 198
    const/4 v0, 0x3

    iget v1, p0, Ljavax/vecmath/Tuple4f;->w:F

    aput v1, p1, v0

    .line 199
    return-void
.end method

.method public final getW()F
    .locals 1

    .prologue
    .line 784
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    return v0
.end method

.method public final getX()F
    .locals 1

    .prologue
    .line 713
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    return v0
.end method

.method public final getY()F
    .locals 1

    .prologue
    .line 737
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    return v0
.end method

.method public final getZ()F
    .locals 1

    .prologue
    .line 760
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 443
    const-wide/16 v0, 0x1

    .line 444
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4f;->x:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 445
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4f;->y:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 446
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4f;->z:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 447
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4f;->w:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 448
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public interpolate(Ljavax/vecmath/Tuple4f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p2, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 680
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 681
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 682
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 683
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 685
    return-void
.end method

.method public interpolate(Ljavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4f;
    .param p3, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 664
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 665
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 666
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 667
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 669
    return-void
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 290
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 291
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 292
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 293
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 277
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 278
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 279
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 280
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 281
    return-void
.end method

.method public final scale(F)V
    .locals 1
    .param p1, "s"    # F

    .prologue
    .line 318
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 319
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 320
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 321
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 322
    return-void
.end method

.method public final scale(FLjavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 304
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 305
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 306
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 307
    iget v0, p2, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 308
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 349
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 350
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 351
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 352
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4f;->w:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 353
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p3, "t2"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 334
    iget v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 335
    iget v0, p2, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 336
    iget v0, p2, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4f;->z:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 337
    iget v0, p2, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4f;->w:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 338
    return-void
.end method

.method public final set(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 142
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 143
    iput p2, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 144
    iput p3, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 145
    iput p4, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 146
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 182
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 183
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 184
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 185
    iget-wide v0, p1, Ljavax/vecmath/Tuple4d;->w:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 186
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 169
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 170
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 171
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 172
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 173
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 156
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 157
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 158
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 159
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 160
    return-void
.end method

.method public final setW(F)V
    .locals 0
    .param p1, "w"    # F

    .prologue
    .line 796
    iput p1, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 797
    return-void
.end method

.method public final setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 725
    iput p1, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 726
    return-void
.end method

.method public final setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 749
    iput p1, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 750
    return-void
.end method

.method public final setZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 772
    iput p1, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 773
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 264
    iget v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 265
    iget v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 266
    iget v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 267
    iget v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    iget v1, p1, Ljavax/vecmath/Tuple4f;->w:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 268
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 250
    iget v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->x:F

    .line 251
    iget v0, p1, Ljavax/vecmath/Tuple4f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->y:F

    .line 252
    iget v0, p1, Ljavax/vecmath/Tuple4f;->z:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->z:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->z:F

    .line 253
    iget v0, p1, Ljavax/vecmath/Tuple4f;->w:F

    iget v1, p2, Ljavax/vecmath/Tuple4f;->w:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4f;->w:F

    .line 254
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4f;->z:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4f;->w:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
