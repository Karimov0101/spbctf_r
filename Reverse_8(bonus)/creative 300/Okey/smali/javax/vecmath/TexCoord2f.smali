.class public Ljavax/vecmath/TexCoord2f;
.super Ljavax/vecmath/Tuple2f;
.source "TexCoord2f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x6eff7c9c7dc40ae7L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljavax/vecmath/Tuple2f;-><init>()V

    .line 93
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljavax/vecmath/Tuple2f;-><init>(FF)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/TexCoord2f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/TexCoord2f;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 84
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 63
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>([F)V

    .line 64
    return-void
.end method
