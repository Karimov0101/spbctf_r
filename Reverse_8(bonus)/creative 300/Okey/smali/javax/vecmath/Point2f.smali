.class public Ljavax/vecmath/Point2f;
.super Ljavax/vecmath/Tuple2f;
.source "Point2f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x42a1d08023cd66c3L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljavax/vecmath/Tuple2f;-><init>()V

    .line 114
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljavax/vecmath/Tuple2f;-><init>(FF)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point2d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 82
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 83
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point2f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 105
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "p"    # [F

    .prologue
    .line 63
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2f;-><init>([F)V

    .line 64
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point2f;)F
    .locals 4
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 137
    iget v2, p0, Ljavax/vecmath/Point2f;->x:F

    iget v3, p1, Ljavax/vecmath/Point2f;->x:F

    sub-float v0, v2, v3

    .line 138
    .local v0, "dx":F
    iget v2, p0, Ljavax/vecmath/Point2f;->y:F

    iget v3, p1, Ljavax/vecmath/Point2f;->y:F

    sub-float v1, v2, v3

    .line 139
    .local v1, "dy":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public final distanceL1(Ljavax/vecmath/Point2f;)F
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 150
    iget v0, p0, Ljavax/vecmath/Point2f;->x:F

    iget v1, p1, Ljavax/vecmath/Point2f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Ljavax/vecmath/Point2f;->y:F

    iget v2, p1, Ljavax/vecmath/Point2f;->y:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point2f;)F
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 161
    iget v0, p0, Ljavax/vecmath/Point2f;->x:F

    iget v1, p1, Ljavax/vecmath/Point2f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Ljavax/vecmath/Point2f;->y:F

    iget v2, p1, Ljavax/vecmath/Point2f;->y:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method public final distanceSquared(Ljavax/vecmath/Point2f;)F
    .locals 4
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 124
    iget v2, p0, Ljavax/vecmath/Point2f;->x:F

    iget v3, p1, Ljavax/vecmath/Point2f;->x:F

    sub-float v0, v2, v3

    .line 125
    .local v0, "dx":F
    iget v2, p0, Ljavax/vecmath/Point2f;->y:F

    iget v3, p1, Ljavax/vecmath/Point2f;->y:F

    sub-float v1, v2, v3

    .line 126
    .local v1, "dy":F
    mul-float v2, v0, v0

    mul-float v3, v1, v1

    add-float/2addr v2, v3

    return v2
.end method
