.class public Ljavax/vecmath/Matrix4d;
.super Ljava/lang/Object;
.source "Matrix4d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final EPS:D = 1.0E-10

.field static final serialVersionUID:J = 0x72212ca59125c42eL


# instance fields
.field public m00:D

.field public m01:D

.field public m02:D

.field public m03:D

.field public m10:D

.field public m11:D

.field public m12:D

.field public m13:D

.field public m20:D

.field public m21:D

.field public m22:D

.field public m23:D

.field public m30:D

.field public m31:D

.field public m32:D

.field public m33:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 405
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 406
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 407
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 408
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 410
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 411
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 412
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 413
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 415
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 416
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 417
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 418
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 420
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 421
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 422
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 423
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 425
    return-void
.end method

.method public constructor <init>(DDDDDDDDDDDDDDDD)V
    .locals 3
    .param p1, "m00"    # D
    .param p3, "m01"    # D
    .param p5, "m02"    # D
    .param p7, "m03"    # D
    .param p9, "m10"    # D
    .param p11, "m11"    # D
    .param p13, "m12"    # D
    .param p15, "m13"    # D
    .param p17, "m20"    # D
    .param p19, "m21"    # D
    .param p21, "m22"    # D
    .param p23, "m23"    # D
    .param p25, "m30"    # D
    .param p27, "m31"    # D
    .param p29, "m32"    # D
    .param p31, "m33"    # D

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 158
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 159
    iput-wide p5, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 160
    iput-wide p7, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 162
    iput-wide p9, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 163
    iput-wide p11, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 164
    move-wide/from16 v0, p13

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 165
    move-wide/from16 v0, p15

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 167
    move-wide/from16 v0, p17

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 168
    move-wide/from16 v0, p19

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 169
    move-wide/from16 v0, p21

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 170
    move-wide/from16 v0, p23

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 172
    move-wide/from16 v0, p25

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 173
    move-wide/from16 v0, p27

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 174
    move-wide/from16 v0, p29

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 175
    move-wide/from16 v0, p31

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 177
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Vector3d;D)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 378
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 379
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 380
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 381
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 383
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 384
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 385
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 386
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 388
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 389
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 390
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 391
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 393
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 394
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 395
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 396
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 398
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3d;D)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 344
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 345
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 346
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 347
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 348
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 350
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 351
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 352
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 353
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 355
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 356
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 357
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v0, v0

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 358
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 360
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 361
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 362
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 363
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 365
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 282
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 283
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 284
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 285
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 286
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 288
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 289
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 290
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 291
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 293
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 294
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 295
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 296
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 298
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 299
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 300
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 301
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 303
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 313
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 314
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 315
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 317
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 318
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 319
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 320
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 322
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 323
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 324
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 325
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 327
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 328
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 329
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 330
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 332
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Vector3d;D)V
    .locals 9
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    .line 218
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 219
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 220
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 221
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 223
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 224
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 225
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 227
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 228
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 229
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 231
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 232
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 233
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 235
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 236
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 237
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 238
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 240
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Vector3d;D)V
    .locals 7
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    .line 252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 254
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 255
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 257
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 258
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 259
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 261
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 262
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 263
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 265
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 266
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 267
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 269
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 270
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 271
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 272
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 274
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "v"    # [D

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 186
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 187
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 188
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 189
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 191
    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 192
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 193
    const/4 v0, 0x6

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 194
    const/4 v0, 0x7

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 196
    const/16 v0, 0x8

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 197
    const/16 v0, 0x9

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 198
    const/16 v0, 0xa

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 199
    const/16 v0, 0xb

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 201
    const/16 v0, 0xc

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 202
    const/16 v0, 0xd

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 203
    const/16 v0, 0xe

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 204
    const/16 v0, 0xf

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 206
    return-void
.end method

.method private final getScaleRotate([D[D)V
    .locals 4
    .param p1, "scales"    # [D
    .param p2, "rots"    # [D

    .prologue
    .line 3566
    const/16 v1, 0x9

    new-array v0, v1, [D

    .line 3567
    .local v0, "tmp":[D
    const/4 v1, 0x0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    aput-wide v2, v0, v1

    .line 3568
    const/4 v1, 0x1

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    aput-wide v2, v0, v1

    .line 3569
    const/4 v1, 0x2

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    aput-wide v2, v0, v1

    .line 3571
    const/4 v1, 0x3

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    aput-wide v2, v0, v1

    .line 3572
    const/4 v1, 0x4

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    aput-wide v2, v0, v1

    .line 3573
    const/4 v1, 0x5

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    aput-wide v2, v0, v1

    .line 3575
    const/4 v1, 0x6

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    aput-wide v2, v0, v1

    .line 3576
    const/4 v1, 0x7

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    aput-wide v2, v0, v1

    .line 3577
    const/16 v1, 0x8

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    aput-wide v2, v0, v1

    .line 3579
    invoke-static {v0, p1, p2}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 3581
    return-void
.end method

.method static luBacksubstitution([D[I[D)V
    .locals 18
    .param p0, "matrix1"    # [D
    .param p1, "row_perm"    # [I
    .param p2, "matrix2"    # [D

    .prologue
    .line 2222
    const/4 v6, 0x0

    .line 2225
    .local v6, "rp":I
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_0
    const/4 v10, 0x4

    if-ge v5, v10, :cond_3

    .line 2227
    move v0, v5

    .line 2228
    .local v0, "cv":I
    const/4 v2, -0x1

    .line 2231
    .local v2, "ii":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v10, 0x4

    if-ge v1, v10, :cond_2

    .line 2234
    add-int v10, v6, v1

    aget v3, p1, v10

    .line 2235
    .local v3, "ip":I
    mul-int/lit8 v10, v3, 0x4

    add-int/2addr v10, v0

    aget-wide v8, p2, v10

    .line 2236
    .local v8, "sum":D
    mul-int/lit8 v10, v3, 0x4

    add-int/2addr v10, v0

    mul-int/lit8 v11, v1, 0x4

    add-int/2addr v11, v0

    aget-wide v12, p2, v11

    aput-wide v12, p2, v10

    .line 2237
    if-ltz v2, :cond_0

    .line 2239
    mul-int/lit8 v7, v1, 0x4

    .line 2240
    .local v7, "rv":I
    move v4, v2

    .local v4, "j":I
    :goto_2
    add-int/lit8 v10, v1, -0x1

    if-gt v4, v10, :cond_1

    .line 2241
    add-int v10, v7, v4

    aget-wide v10, p0, v10

    mul-int/lit8 v12, v4, 0x4

    add-int/2addr v12, v0

    aget-wide v12, p2, v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    .line 2240
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2244
    .end local v4    # "j":I
    .end local v7    # "rv":I
    :cond_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 2245
    move v2, v1

    .line 2247
    :cond_1
    mul-int/lit8 v10, v1, 0x4

    add-int/2addr v10, v0

    aput-wide v8, p2, v10

    .line 2231
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2252
    .end local v3    # "ip":I
    .end local v8    # "sum":D
    :cond_2
    const/16 v7, 0xc

    .line 2253
    .restart local v7    # "rv":I
    add-int/lit8 v10, v0, 0xc

    aget-wide v12, p2, v10

    const/16 v11, 0xf

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 2255
    add-int/lit8 v7, v7, -0x4

    .line 2256
    add-int/lit8 v10, v0, 0x8

    add-int/lit8 v11, v0, 0x8

    aget-wide v12, p2, v11

    const/16 v11, 0xb

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/16 v11, 0xa

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 2259
    add-int/lit8 v7, v7, -0x4

    .line 2260
    add-int/lit8 v10, v0, 0x4

    add-int/lit8 v11, v0, 0x4

    aget-wide v12, p2, v11

    const/4 v11, 0x6

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x8

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x7

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x5

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 2264
    add-int/lit8 v7, v7, -0x4

    .line 2265
    add-int/lit8 v10, v0, 0x0

    add-int/lit8 v11, v0, 0x0

    aget-wide v12, p2, v11

    const/4 v11, 0x1

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x4

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x2

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x8

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x3

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x0

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 2225
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 2270
    .end local v0    # "cv":I
    .end local v1    # "i":I
    .end local v2    # "ii":I
    .end local v7    # "rv":I
    :cond_3
    return-void
.end method

.method static luDecomposition([D[I)Z
    .locals 30
    .param p0, "matrix0"    # [D
    .param p1, "row_perm"    # [I

    .prologue
    .line 2066
    const/16 v26, 0x4

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 2074
    .local v18, "row_scale":[D
    const/16 v16, 0x0

    .line 2075
    .local v16, "ptr":I
    const/16 v19, 0x0

    .line 2078
    .local v19, "rs":I
    const/4 v4, 0x4

    .local v4, "i":I
    move/from16 v20, v19

    .end local v19    # "rs":I
    .local v20, "rs":I
    move v5, v4

    .line 2079
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_2

    .line 2080
    const-wide/16 v2, 0x0

    .line 2083
    .local v2, "big":D
    const/4 v7, 0x4

    .local v7, "j":I
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .local v17, "ptr":I
    move v8, v7

    .line 2084
    .end local v7    # "j":I
    .local v8, "j":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "j":I
    .restart local v7    # "j":I
    if-eqz v8, :cond_0

    .line 2085
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    aget-wide v24, p0, v17

    .line 2086
    .local v24, "temp":D
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    .line 2087
    cmpl-double v26, v24, v2

    if-lez v26, :cond_e

    .line 2088
    move-wide/from16 v2, v24

    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto :goto_1

    .line 2093
    .end local v8    # "j":I
    .end local v24    # "temp":D
    .restart local v7    # "j":I
    :cond_0
    const-wide/16 v26, 0x0

    cmpl-double v26, v2, v26

    if-nez v26, :cond_1

    .line 2094
    const/16 v26, 0x0

    .line 2192
    .end local v2    # "big":D
    .end local v17    # "ptr":I
    :goto_2
    return v26

    .line 2096
    .restart local v2    # "big":D
    .restart local v17    # "ptr":I
    :cond_1
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "rs":I
    .restart local v19    # "rs":I
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v2

    aput-wide v26, v18, v20

    move/from16 v20, v19

    .end local v19    # "rs":I
    .restart local v20    # "rs":I
    move/from16 v16, v17

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 2104
    .end local v2    # "big":D
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v4    # "i":I
    :cond_2
    const/4 v11, 0x0

    .line 2107
    .local v11, "mtx":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    const/16 v26, 0x4

    move/from16 v0, v26

    if-ge v7, v0, :cond_d

    .line 2113
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_4

    .line 2114
    mul-int/lit8 v26, v4, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 2115
    .local v21, "target":I
    aget-wide v22, p0, v21

    .line 2116
    .local v22, "sum":D
    move v9, v4

    .line 2117
    .local v9, "k":I
    mul-int/lit8 v26, v4, 0x4

    add-int v12, v11, v26

    .line 2118
    .local v12, "p1":I
    add-int v14, v11, v7

    .local v14, "p2":I
    move v10, v9

    .line 2119
    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_3

    .line 2120
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 2121
    add-int/lit8 v12, v12, 0x1

    .line 2122
    add-int/lit8 v14, v14, 0x4

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 2124
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_3
    aput-wide v22, p0, v21

    .line 2113
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 2129
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    :cond_4
    const-wide/16 v2, 0x0

    .line 2130
    .restart local v2    # "big":D
    const/4 v6, -0x1

    .line 2131
    .local v6, "imax":I
    move v4, v7

    :goto_6
    const/16 v26, 0x4

    move/from16 v0, v26

    if-ge v4, v0, :cond_7

    .line 2132
    mul-int/lit8 v26, v4, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 2133
    .restart local v21    # "target":I
    aget-wide v22, p0, v21

    .line 2134
    .restart local v22    # "sum":D
    move v9, v7

    .line 2135
    .restart local v9    # "k":I
    mul-int/lit8 v26, v4, 0x4

    add-int v12, v11, v26

    .line 2136
    .restart local v12    # "p1":I
    add-int v14, v11, v7

    .restart local v14    # "p2":I
    move v10, v9

    .line 2137
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_7
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_5

    .line 2138
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 2139
    add-int/lit8 v12, v12, 0x1

    .line 2140
    add-int/lit8 v14, v14, 0x4

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_7

    .line 2142
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_5
    aput-wide v22, p0, v21

    .line 2145
    aget-wide v26, v18, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    mul-double v24, v26, v28

    .restart local v24    # "temp":D
    cmpl-double v26, v24, v2

    if-ltz v26, :cond_6

    .line 2146
    move-wide/from16 v2, v24

    .line 2147
    move v6, v4

    .line 2131
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 2151
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    .end local v24    # "temp":D
    :cond_7
    if-gez v6, :cond_8

    .line 2152
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "Matrix4d11"

    invoke-static/range {v27 .. v27}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 2156
    :cond_8
    if-eq v7, v6, :cond_a

    .line 2158
    const/4 v9, 0x4

    .line 2159
    .restart local v9    # "k":I
    mul-int/lit8 v26, v6, 0x4

    add-int v12, v11, v26

    .line 2160
    .restart local v12    # "p1":I
    mul-int/lit8 v26, v7, 0x4

    add-int v14, v11, v26

    .restart local v14    # "p2":I
    move v15, v14

    .end local v14    # "p2":I
    .local v15, "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .local v13, "p1":I
    move v10, v9

    .line 2161
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_8
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_9

    .line 2162
    aget-wide v24, p0, v13

    .line 2163
    .restart local v24    # "temp":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "p1":I
    .restart local v12    # "p1":I
    aget-wide v26, p0, v15

    aput-wide v26, p0, v13

    .line 2164
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "p2":I
    .restart local v14    # "p2":I
    aput-wide v24, p0, v15

    move v15, v14

    .end local v14    # "p2":I
    .restart local v15    # "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .restart local v13    # "p1":I
    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_8

    .line 2168
    .end local v10    # "k":I
    .end local v24    # "temp":D
    .restart local v9    # "k":I
    :cond_9
    aget-wide v26, v18, v7

    aput-wide v26, v18, v6

    .line 2172
    .end local v9    # "k":I
    .end local v13    # "p1":I
    .end local v15    # "p2":I
    :cond_a
    aput v6, p1, v7

    .line 2175
    mul-int/lit8 v26, v7, 0x4

    add-int v26, v26, v11

    add-int v26, v26, v7

    aget-wide v26, p0, v26

    const-wide/16 v28, 0x0

    cmpl-double v26, v26, v28

    if-nez v26, :cond_b

    .line 2176
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 2180
    :cond_b
    const/16 v26, 0x3

    move/from16 v0, v26

    if-eq v7, v0, :cond_c

    .line 2181
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-int/lit8 v28, v7, 0x4

    add-int v28, v28, v11

    add-int v28, v28, v7

    aget-wide v28, p0, v28

    div-double v24, v26, v28

    .line 2182
    .restart local v24    # "temp":D
    add-int/lit8 v26, v7, 0x1

    mul-int/lit8 v26, v26, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 2183
    .restart local v21    # "target":I
    rsub-int/lit8 v4, v7, 0x3

    move v5, v4

    .line 2184
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :goto_9
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_c

    .line 2185
    aget-wide v26, p0, v21

    mul-double v26, v26, v24

    aput-wide v26, p0, v21

    .line 2186
    add-int/lit8 v21, v21, 0x4

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_9

    .line 2107
    .end local v5    # "i":I
    .end local v21    # "target":I
    .end local v24    # "temp":D
    .restart local v4    # "i":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 2192
    .end local v2    # "big":D
    .end local v6    # "imax":I
    :cond_d
    const/16 v26, 0x1

    goto/16 :goto_2

    .end local v11    # "mtx":I
    .restart local v2    # "big":D
    .restart local v24    # "temp":D
    :cond_e
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto/16 :goto_1
.end method


# virtual methods
.method public final add(D)V
    .locals 3
    .param p1, "scalar"    # D

    .prologue
    .line 1357
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1358
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1359
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1360
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1361
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1362
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1363
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1364
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1365
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1366
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1367
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1368
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1369
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1370
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1371
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1372
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1373
    return-void
.end method

.method public final add(DLjavax/vecmath/Matrix4d;)V
    .locals 3
    .param p1, "scalar"    # D
    .param p3, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1383
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m00:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1384
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m01:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1385
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m02:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1386
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1387
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m10:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1388
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1389
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m12:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1390
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1391
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m20:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1392
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m21:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1393
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1394
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1395
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m30:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1396
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m31:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1397
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m32:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1398
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1399
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix4d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1435
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m00:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1436
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m01:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1437
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m02:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1438
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1440
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1441
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1442
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m12:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1443
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1445
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m20:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1446
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1447
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1448
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1450
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m30:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1451
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m31:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1452
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m32:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1453
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1454
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1408
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m00:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1409
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m01:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1410
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m02:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1411
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1413
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m10:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1414
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1415
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m12:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1416
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1418
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m20:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1419
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m21:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1420
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1421
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1423
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m30:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1424
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m31:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1425
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m32:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1426
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1427
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3593
    const/4 v1, 0x0

    .line 3595
    .local v1, "m1":Ljavax/vecmath/Matrix4d;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m1":Ljavax/vecmath/Matrix4d;
    check-cast v1, Ljavax/vecmath/Matrix4d;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3601
    .restart local v1    # "m1":Ljavax/vecmath/Matrix4d;
    return-object v1

    .line 3596
    .end local v1    # "m1":Ljavax/vecmath/Matrix4d;
    :catch_0
    move-exception v0

    .line 3598
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/InternalError;

    invoke-direct {v2}, Ljava/lang/InternalError;-><init>()V

    throw v2
.end method

.method public final determinant()D
    .locals 10

    .prologue
    .line 2282
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 2284
    .local v0, "det":D
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    .line 2286
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    .line 2288
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    .line 2291
    return-wide v0
.end method

.method public epsilonEquals(Ljavax/vecmath/Matrix4d;D)Z
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "epsilon"    # D

    .prologue
    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 3021
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m00:D

    sub-double v0, v4, v6

    .line 3022
    .local v0, "diff":D
    cmpg-double v3, v0, v8

    if-gez v3, :cond_1

    neg-double v4, v0

    :goto_0
    cmpl-double v3, v4, p2

    if-lez v3, :cond_2

    .line 3069
    :cond_0
    :goto_1
    return v2

    :cond_1
    move-wide v4, v0

    .line 3022
    goto :goto_0

    .line 3024
    :cond_2
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m01:D

    sub-double v0, v4, v6

    .line 3025
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3027
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m02:D

    sub-double v0, v4, v6

    .line 3028
    cmpg-double v3, v0, v8

    if-gez v3, :cond_4

    neg-double v4, v0

    :goto_3
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3030
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m03:D

    sub-double v0, v4, v6

    .line 3031
    cmpg-double v3, v0, v8

    if-gez v3, :cond_5

    neg-double v4, v0

    :goto_4
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3033
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m10:D

    sub-double v0, v4, v6

    .line 3034
    cmpg-double v3, v0, v8

    if-gez v3, :cond_6

    neg-double v4, v0

    :goto_5
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3036
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m11:D

    sub-double v0, v4, v6

    .line 3037
    cmpg-double v3, v0, v8

    if-gez v3, :cond_7

    neg-double v4, v0

    :goto_6
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3039
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m12:D

    sub-double v0, v4, v6

    .line 3040
    cmpg-double v3, v0, v8

    if-gez v3, :cond_8

    neg-double v4, v0

    :goto_7
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3042
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m13:D

    sub-double v0, v4, v6

    .line 3043
    cmpg-double v3, v0, v8

    if-gez v3, :cond_9

    neg-double v4, v0

    :goto_8
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3045
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m20:D

    sub-double v0, v4, v6

    .line 3046
    cmpg-double v3, v0, v8

    if-gez v3, :cond_a

    neg-double v4, v0

    :goto_9
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3048
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m21:D

    sub-double v0, v4, v6

    .line 3049
    cmpg-double v3, v0, v8

    if-gez v3, :cond_b

    neg-double v4, v0

    :goto_a
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3051
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    sub-double v0, v4, v6

    .line 3052
    cmpg-double v3, v0, v8

    if-gez v3, :cond_c

    neg-double v4, v0

    :goto_b
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3054
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m23:D

    sub-double v0, v4, v6

    .line 3055
    cmpg-double v3, v0, v8

    if-gez v3, :cond_d

    neg-double v4, v0

    :goto_c
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3057
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m30:D

    sub-double v0, v4, v6

    .line 3058
    cmpg-double v3, v0, v8

    if-gez v3, :cond_e

    neg-double v4, v0

    :goto_d
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3060
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m31:D

    sub-double v0, v4, v6

    .line 3061
    cmpg-double v3, v0, v8

    if-gez v3, :cond_f

    neg-double v4, v0

    :goto_e
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3063
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m32:D

    sub-double v0, v4, v6

    .line 3064
    cmpg-double v3, v0, v8

    if-gez v3, :cond_10

    neg-double v4, v0

    :goto_f
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3066
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m33:D

    sub-double v0, v4, v6

    .line 3067
    cmpg-double v3, v0, v8

    if-gez v3, :cond_11

    neg-double v4, v0

    :goto_10
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 3069
    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_3
    move-wide v4, v0

    .line 3025
    goto/16 :goto_2

    :cond_4
    move-wide v4, v0

    .line 3028
    goto/16 :goto_3

    :cond_5
    move-wide v4, v0

    .line 3031
    goto/16 :goto_4

    :cond_6
    move-wide v4, v0

    .line 3034
    goto/16 :goto_5

    :cond_7
    move-wide v4, v0

    .line 3037
    goto/16 :goto_6

    :cond_8
    move-wide v4, v0

    .line 3040
    goto/16 :goto_7

    :cond_9
    move-wide v4, v0

    .line 3043
    goto/16 :goto_8

    :cond_a
    move-wide v4, v0

    .line 3046
    goto/16 :goto_9

    :cond_b
    move-wide v4, v0

    .line 3049
    goto :goto_a

    :cond_c
    move-wide v4, v0

    .line 3052
    goto :goto_b

    :cond_d
    move-wide v4, v0

    .line 3055
    goto :goto_c

    :cond_e
    move-wide v4, v0

    .line 3058
    goto :goto_d

    :cond_f
    move-wide v4, v0

    .line 3061
    goto :goto_e

    :cond_10
    move-wide v4, v0

    .line 3064
    goto :goto_f

    :cond_11
    move-wide v4, v0

    .line 3067
    goto :goto_10
.end method

.method public epsilonEquals(Ljavax/vecmath/Matrix4d;F)Z
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "epsilon"    # F

    .prologue
    .line 3006
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Ljavax/vecmath/Matrix4d;->epsilonEquals(Ljavax/vecmath/Matrix4d;D)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 2990
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Matrix4d;

    move-object v4, v0

    .line 2991
    .local v4, "m2":Ljavax/vecmath/Matrix4d;
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m00:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m01:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m02:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m03:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m10:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m11:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m12:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m13:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m20:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m21:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m22:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m23:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m30:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m31:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m32:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v8, v4, Ljavax/vecmath/Matrix4d;->m33:D
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 2999
    .end local v4    # "m2":Ljavax/vecmath/Matrix4d;
    :cond_0
    :goto_0
    return v5

    .line 2998
    :catch_0
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 2999
    .end local v2    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Matrix4d;)Z
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    const/4 v1, 0x0

    .line 2969
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m00:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m01:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m02:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m03:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m10:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m12:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m13:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m20:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m21:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m22:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m23:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m30:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m31:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m32:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m33:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 2976
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Vector3d;)D
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;

    .prologue
    const/4 v4, 0x3

    .line 841
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 842
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 843
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 845
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    .line 846
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    .line 847
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 849
    aget-wide v2, v0, v4

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    .line 850
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    .line 851
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 853
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    .line 854
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    .line 855
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    .line 857
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 858
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 859
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->z:D

    .line 861
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    return-wide v2
.end method

.method public final get(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3d;)D
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;

    .prologue
    const/4 v4, 0x3

    .line 875
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 876
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 877
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 879
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 880
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 881
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 883
    aget-wide v2, v0, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 884
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 885
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 887
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 888
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 889
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 891
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 892
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 893
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->z:D

    .line 895
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    return-wide v2
.end method

.method public final get(Ljavax/vecmath/Matrix3d;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v4, 0x3

    .line 785
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 786
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 787
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 789
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    .line 790
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    .line 791
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 793
    aget-wide v2, v0, v4

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    .line 794
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    .line 795
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 797
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    .line 798
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    .line 799
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    .line 801
    return-void
.end method

.method public final get(Ljavax/vecmath/Matrix3f;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v4, 0x3

    .line 812
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 813
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 815
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 817
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 818
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 819
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 821
    aget-wide v2, v0, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 822
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 823
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 825
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 826
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 827
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 828
    return-void
.end method

.method public final get(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 955
    const/16 v4, 0x9

    new-array v0, v4, [D

    .line 956
    .local v0, "tmp_rot":[D
    const/4 v4, 0x3

    new-array v1, v4, [D

    .line 958
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 962
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/4 v8, 0x4

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 963
    .local v2, "ww":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_0

    neg-double v4, v2

    :goto_0
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_1

    .line 964
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    .line 965
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    div-double v2, v4, v6

    .line 966
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const/4 v6, 0x5

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    .line 967
    const/4 v4, 0x2

    aget-wide v4, v0, v4

    const/4 v6, 0x6

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    .line 968
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    const/4 v6, 0x1

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    .line 992
    :goto_1
    return-void

    :cond_0
    move-wide v4, v2

    .line 963
    goto :goto_0

    .line 972
    :cond_1
    const-wide/16 v4, 0x0

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    .line 973
    const-wide/high16 v4, -0x4020000000000000L    # -0.5

    const/4 v6, 0x4

    aget-wide v6, v0, v6

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 974
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_2

    neg-double v4, v2

    :goto_2
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 975
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    .line 976
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    div-double v2, v4, v6

    .line 977
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    .line 978
    const/4 v4, 0x6

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_1

    :cond_2
    move-wide v4, v2

    .line 974
    goto :goto_2

    .line 982
    :cond_3
    const-wide/16 v4, 0x0

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    .line 983
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    sub-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 984
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_4

    neg-double v4, v2

    :goto_3
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_5

    .line 985
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    .line 986
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    div-double/2addr v4, v6

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_1

    :cond_4
    move-wide v4, v2

    .line 984
    goto :goto_3

    .line 990
    :cond_5
    const-wide/16 v4, 0x0

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    .line 991
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iput-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_1
.end method

.method public final get(Ljavax/vecmath/Quat4f;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 908
    const/16 v4, 0x9

    new-array v0, v4, [D

    .line 909
    .local v0, "tmp_rot":[D
    const/4 v4, 0x3

    new-array v1, v4, [D

    .line 910
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 914
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/4 v8, 0x4

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 915
    .local v2, "ww":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_0

    neg-double v4, v2

    :goto_0
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_1

    .line 916
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->w:F

    .line 917
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    iget v6, p1, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v6, v6

    div-double v2, v4, v6

    .line 918
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const/4 v6, 0x5

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 919
    const/4 v4, 0x2

    aget-wide v4, v0, v4

    const/4 v6, 0x6

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 920
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    const/4 v6, 0x1

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    .line 945
    :goto_1
    return-void

    :cond_0
    move-wide v4, v2

    .line 915
    goto :goto_0

    .line 924
    :cond_1
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->w:F

    .line 925
    const-wide/high16 v4, -0x4020000000000000L    # -0.5

    const/4 v6, 0x4

    aget-wide v6, v0, v6

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 926
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_2

    neg-double v4, v2

    :goto_2
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 927
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 928
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    div-double v2, v4, v6

    .line 929
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 930
    const/4 v4, 0x6

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1

    :cond_2
    move-wide v4, v2

    .line 926
    goto :goto_2

    .line 934
    :cond_3
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 935
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    sub-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 936
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_4

    neg-double v4, v2

    :goto_3
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_5

    .line 937
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 938
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget v8, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v8, v8

    mul-double/2addr v6, v8

    div-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1

    :cond_4
    move-wide v4, v2

    .line 936
    goto :goto_3

    .line 942
    :cond_5
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 943
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1
.end method

.method public final get(Ljavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "trans"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 1000
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    .line 1001
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    .line 1002
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    .line 1003
    return-void
.end method

.method public final getColumn(ILjavax/vecmath/Vector4d;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 712
    if-nez p1, :cond_0

    .line 713
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 714
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 715
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 716
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    .line 737
    :goto_0
    return-void

    .line 717
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 718
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 719
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 720
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 721
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 722
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 723
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 724
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 725
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 726
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 727
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 728
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 729
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 730
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 731
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 733
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d3"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getColumn(I[D)V
    .locals 6
    .param p1, "column"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 748
    if-nez p1, :cond_0

    .line 749
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    aput-wide v0, p2, v5

    .line 750
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    aput-wide v0, p2, v2

    .line 751
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    aput-wide v0, p2, v3

    .line 752
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    aput-wide v0, p2, v4

    .line 773
    :goto_0
    return-void

    .line 753
    :cond_0
    if-ne p1, v2, :cond_1

    .line 754
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    aput-wide v0, p2, v5

    .line 755
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    aput-wide v0, p2, v2

    .line 756
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    aput-wide v0, p2, v3

    .line 757
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 758
    :cond_1
    if-ne p1, v3, :cond_2

    .line 759
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    aput-wide v0, p2, v5

    .line 760
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    aput-wide v0, p2, v2

    .line 761
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    aput-wide v0, p2, v3

    .line 762
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 763
    :cond_2
    if-ne p1, v4, :cond_3

    .line 764
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    aput-wide v0, p2, v5

    .line 765
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    aput-wide v0, p2, v2

    .line 766
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    aput-wide v0, p2, v3

    .line 767
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 769
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d3"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getElement(II)D
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 568
    packed-switch p1, :pswitch_data_0

    .line 636
    :goto_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d1"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 571
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 574
    :pswitch_1
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 627
    :goto_1
    return-wide v0

    .line 576
    :pswitch_2
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    goto :goto_1

    .line 578
    :pswitch_3
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    goto :goto_1

    .line 580
    :pswitch_4
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    goto :goto_1

    .line 586
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 589
    :pswitch_6
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    goto :goto_1

    .line 591
    :pswitch_7
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    goto :goto_1

    .line 593
    :pswitch_8
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    goto :goto_1

    .line 595
    :pswitch_9
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    goto :goto_1

    .line 602
    :pswitch_a
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 605
    :pswitch_b
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    goto :goto_1

    .line 607
    :pswitch_c
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    goto :goto_1

    .line 609
    :pswitch_d
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    goto :goto_1

    .line 611
    :pswitch_e
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    goto :goto_1

    .line 618
    :pswitch_f
    packed-switch p2, :pswitch_data_4

    goto :goto_0

    .line 621
    :pswitch_10
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    goto :goto_1

    .line 623
    :pswitch_11
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    goto :goto_1

    .line 625
    :pswitch_12
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    goto :goto_1

    .line 627
    :pswitch_13
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_1

    .line 568
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch

    .line 571
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 586
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 602
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 618
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final getM00()D
    .locals 2

    .prologue
    .line 3612
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    return-wide v0
.end method

.method public final getM01()D
    .locals 2

    .prologue
    .line 3635
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    return-wide v0
.end method

.method public final getM02()D
    .locals 2

    .prologue
    .line 3657
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    return-wide v0
.end method

.method public final getM03()D
    .locals 2

    .prologue
    .line 3812
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    return-wide v0
.end method

.method public final getM10()D
    .locals 2

    .prologue
    .line 3679
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    return-wide v0
.end method

.method public final getM11()D
    .locals 2

    .prologue
    .line 3701
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    return-wide v0
.end method

.method public final getM12()D
    .locals 2

    .prologue
    .line 3723
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    return-wide v0
.end method

.method public final getM13()D
    .locals 2

    .prologue
    .line 3834
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    return-wide v0
.end method

.method public final getM20()D
    .locals 2

    .prologue
    .line 3746
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    return-wide v0
.end method

.method public final getM21()D
    .locals 2

    .prologue
    .line 3768
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    return-wide v0
.end method

.method public final getM22()D
    .locals 2

    .prologue
    .line 3790
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    return-wide v0
.end method

.method public final getM23()D
    .locals 2

    .prologue
    .line 3856
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    return-wide v0
.end method

.method public final getM30()D
    .locals 2

    .prologue
    .line 3878
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    return-wide v0
.end method

.method public final getM31()D
    .locals 2

    .prologue
    .line 3900
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    return-wide v0
.end method

.method public final getM32()D
    .locals 2

    .prologue
    .line 3923
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    return-wide v0
.end method

.method public final getM33()D
    .locals 2

    .prologue
    .line 3945
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    return-wide v0
.end method

.method public final getRotationScale(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1024
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 1025
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 1026
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    .line 1027
    return-void
.end method

.method public final getRotationScale(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1012
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1013
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1014
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1015
    return-void
.end method

.method public final getRow(ILjavax/vecmath/Vector4d;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 645
    if-nez p1, :cond_0

    .line 646
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 647
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 648
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 649
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    .line 668
    :goto_0
    return-void

    .line 650
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 651
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 652
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 653
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 654
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 655
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 656
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 657
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 658
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 659
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 660
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 661
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    .line 662
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    .line 663
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    .line 664
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iput-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    goto :goto_0

    .line 666
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRow(I[D)V
    .locals 6
    .param p1, "row"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 677
    if-nez p1, :cond_0

    .line 678
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    aput-wide v0, p2, v5

    .line 679
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    aput-wide v0, p2, v2

    .line 680
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    aput-wide v0, p2, v3

    .line 681
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    aput-wide v0, p2, v4

    .line 701
    :goto_0
    return-void

    .line 682
    :cond_0
    if-ne p1, v2, :cond_1

    .line 683
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    aput-wide v0, p2, v5

    .line 684
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    aput-wide v0, p2, v2

    .line 685
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    aput-wide v0, p2, v3

    .line 686
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 687
    :cond_1
    if-ne p1, v3, :cond_2

    .line 688
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    aput-wide v0, p2, v5

    .line 689
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    aput-wide v0, p2, v2

    .line 690
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    aput-wide v0, p2, v3

    .line 691
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 692
    :cond_2
    if-ne p1, v4, :cond_3

    .line 693
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    aput-wide v0, p2, v5

    .line 694
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    aput-wide v0, p2, v2

    .line 695
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    aput-wide v0, p2, v3

    .line 696
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    aput-wide v0, p2, v4

    goto :goto_0

    .line 699
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getScale()D
    .locals 4

    .prologue
    .line 1039
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 1040
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 1041
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 1043
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    return-wide v2
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 3081
    const-wide/16 v0, 0x1

    .line 3082
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3083
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3084
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3085
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m03:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3086
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3087
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3088
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3089
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3090
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3091
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3092
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3093
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m23:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3094
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m30:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3095
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m31:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3096
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m32:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3097
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m33:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 3098
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final invert()V
    .locals 0

    .prologue
    .line 1970
    invoke-virtual {p0, p0}, Ljavax/vecmath/Matrix4d;->invertGeneral(Ljavax/vecmath/Matrix4d;)V

    .line 1971
    return-void
.end method

.method public final invert(Ljavax/vecmath/Matrix4d;)V
    .locals 0
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1962
    invoke-virtual {p0, p1}, Ljavax/vecmath/Matrix4d;->invertGeneral(Ljavax/vecmath/Matrix4d;)V

    .line 1963
    return-void
.end method

.method final invertGeneral(Ljavax/vecmath/Matrix4d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    const/16 v13, 0xa

    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1982
    const/16 v4, 0x10

    new-array v1, v4, [D

    .line 1983
    .local v1, "result":[D
    new-array v2, v11, [I

    .line 1988
    .local v2, "row_perm":[I
    const/16 v4, 0x10

    new-array v3, v4, [D

    .line 1990
    .local v3, "tmp":[D
    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m00:D

    aput-wide v4, v3, v10

    .line 1991
    const/4 v4, 0x1

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m01:D

    aput-wide v6, v3, v4

    .line 1992
    const/4 v4, 0x2

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m02:D

    aput-wide v6, v3, v4

    .line 1993
    const/4 v4, 0x3

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m03:D

    aput-wide v6, v3, v4

    .line 1995
    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m10:D

    aput-wide v4, v3, v11

    .line 1996
    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    aput-wide v4, v3, v12

    .line 1997
    const/4 v4, 0x6

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m12:D

    aput-wide v6, v3, v4

    .line 1998
    const/4 v4, 0x7

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m13:D

    aput-wide v6, v3, v4

    .line 2000
    const/16 v4, 0x8

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m20:D

    aput-wide v6, v3, v4

    .line 2001
    const/16 v4, 0x9

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m21:D

    aput-wide v6, v3, v4

    .line 2002
    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m22:D

    aput-wide v4, v3, v13

    .line 2003
    const/16 v4, 0xb

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m23:D

    aput-wide v6, v3, v4

    .line 2005
    const/16 v4, 0xc

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m30:D

    aput-wide v6, v3, v4

    .line 2006
    const/16 v4, 0xd

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m31:D

    aput-wide v6, v3, v4

    .line 2007
    const/16 v4, 0xe

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m32:D

    aput-wide v6, v3, v4

    .line 2008
    const/16 v4, 0xf

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m33:D

    aput-wide v6, v3, v4

    .line 2011
    invoke-static {v3, v2}, Ljavax/vecmath/Matrix4d;->luDecomposition([D[I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2013
    new-instance v4, Ljavax/vecmath/SingularMatrixException;

    const-string v5, "Matrix4d10"

    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 2017
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x10

    if-ge v0, v4, :cond_1

    const-wide/16 v4, 0x0

    aput-wide v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2018
    :cond_1
    aput-wide v8, v1, v10

    aput-wide v8, v1, v12

    aput-wide v8, v1, v13

    const/16 v4, 0xf

    aput-wide v8, v1, v4

    .line 2019
    invoke-static {v3, v2, v1}, Ljavax/vecmath/Matrix4d;->luBacksubstitution([D[I[D)V

    .line 2021
    aget-wide v4, v1, v10

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2022
    const/4 v4, 0x1

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2023
    const/4 v4, 0x2

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2024
    const/4 v4, 0x3

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2026
    aget-wide v4, v1, v11

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2027
    aget-wide v4, v1, v12

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2028
    const/4 v4, 0x6

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2029
    const/4 v4, 0x7

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2031
    const/16 v4, 0x8

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2032
    const/16 v4, 0x9

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2033
    aget-wide v4, v1, v13

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2034
    const/16 v4, 0xb

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2036
    const/16 v4, 0xc

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2037
    const/16 v4, 0xd

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2038
    const/16 v4, 0xe

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2039
    const/16 v4, 0xf

    aget-wide v4, v1, v4

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2041
    return-void
.end method

.method public final mul(D)V
    .locals 3
    .param p1, "scalar"    # D

    .prologue
    .line 2591
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2592
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2593
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2594
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2595
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2596
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2597
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2598
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2599
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2600
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2601
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2602
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2603
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2604
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2605
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2606
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2607
    return-void
.end method

.method public final mul(DLjavax/vecmath/Matrix4d;)V
    .locals 3
    .param p1, "scalar"    # D
    .param p3, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2617
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m00:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2618
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m01:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2619
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m02:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2620
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m03:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2621
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m10:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2622
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m11:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2623
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m12:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2624
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m13:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2625
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2626
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m21:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2627
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m22:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2628
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m23:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2629
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m30:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2630
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m31:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2631
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m32:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2632
    iget-wide v0, p3, Ljavax/vecmath/Matrix4d;->m33:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2633
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix4d;)V
    .locals 42
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2647
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v4, v36, v38

    .line 2649
    .local v4, "m00":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v6, v36, v38

    .line 2651
    .local v6, "m01":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v8, v36, v38

    .line 2653
    .local v8, "m02":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v10, v36, v38

    .line 2656
    .local v10, "m03":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v12, v36, v38

    .line 2658
    .local v12, "m10":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v14, v36, v38

    .line 2660
    .local v14, "m11":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v16, v36, v38

    .line 2662
    .local v16, "m12":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v18, v36, v38

    .line 2665
    .local v18, "m13":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v20, v36, v38

    .line 2667
    .local v20, "m20":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v22, v36, v38

    .line 2669
    .local v22, "m21":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v24, v36, v38

    .line 2671
    .local v24, "m22":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v26, v36, v38

    .line 2674
    .local v26, "m23":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v28, v36, v38

    .line 2676
    .local v28, "m30":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v30, v36, v38

    .line 2678
    .local v30, "m31":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v32, v36, v38

    .line 2680
    .local v32, "m32":D
    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p0

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v34, v36, v38

    .line 2683
    .local v34, "m33":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2684
    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2685
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2686
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2687
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 42
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2697
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2699
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2701
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2703
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2705
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2708
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2710
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2712
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2714
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2717
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2719
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2721
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2723
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2726
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2728
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2730
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2732
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2767
    :goto_0
    return-void

    .line 2741
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v4, v36, v38

    .line 2742
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v6, v36, v38

    .line 2743
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v8, v36, v38

    .line 2744
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v10, v36, v38

    .line 2746
    .local v10, "m03":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v12, v36, v38

    .line 2747
    .local v12, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v14, v36, v38

    .line 2748
    .local v14, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v16, v36, v38

    .line 2749
    .local v16, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v18, v36, v38

    .line 2751
    .local v18, "m13":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v20, v36, v38

    .line 2752
    .local v20, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v22, v36, v38

    .line 2753
    .local v22, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v24, v36, v38

    .line 2754
    .local v24, "m22":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v26, v36, v38

    .line 2756
    .local v26, "m23":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v28, v36, v38

    .line 2757
    .local v28, "m30":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v30, v36, v38

    .line 2758
    .local v30, "m31":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v32, v36, v38

    .line 2759
    .local v32, "m32":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v34, v36, v38

    .line 2761
    .local v34, "m33":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2762
    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2763
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2764
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    goto/16 :goto_0
.end method

.method public final mulTransposeBoth(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 42
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2777
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2778
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2779
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2780
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2781
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2783
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2784
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2785
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2786
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2788
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2789
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2790
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2791
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2793
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2794
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2795
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2796
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2829
    :goto_0
    return-void

    .line 2803
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v4, v36, v38

    .line 2804
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v6, v36, v38

    .line 2805
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v8, v36, v38

    .line 2806
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v10, v36, v38

    .line 2808
    .local v10, "m03":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v12, v36, v38

    .line 2809
    .local v12, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v14, v36, v38

    .line 2810
    .local v14, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v16, v36, v38

    .line 2811
    .local v16, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v18, v36, v38

    .line 2813
    .local v18, "m13":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v20, v36, v38

    .line 2814
    .local v20, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v22, v36, v38

    .line 2815
    .local v22, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v24, v36, v38

    .line 2816
    .local v24, "m22":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v26, v36, v38

    .line 2818
    .local v26, "m23":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v28, v36, v38

    .line 2819
    .local v28, "m30":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v30, v36, v38

    .line 2820
    .local v30, "m31":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v32, v36, v38

    .line 2821
    .local v32, "m32":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v34, v36, v38

    .line 2823
    .local v34, "m33":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2824
    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2825
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2826
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    goto/16 :goto_0
.end method

.method public final mulTransposeLeft(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 42
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2903
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2904
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2905
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2906
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2907
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2909
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2910
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2911
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2912
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2914
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2915
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2916
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2917
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2919
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2920
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2921
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2922
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2957
    :goto_0
    return-void

    .line 2931
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v4, v36, v38

    .line 2932
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v6, v36, v38

    .line 2933
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v8, v36, v38

    .line 2934
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v10, v36, v38

    .line 2936
    .local v10, "m03":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v12, v36, v38

    .line 2937
    .local v12, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v14, v36, v38

    .line 2938
    .local v14, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v16, v36, v38

    .line 2939
    .local v16, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v18, v36, v38

    .line 2941
    .local v18, "m13":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v20, v36, v38

    .line 2942
    .local v20, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v22, v36, v38

    .line 2943
    .local v22, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v24, v36, v38

    .line 2944
    .local v24, "m22":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v26, v36, v38

    .line 2946
    .local v26, "m23":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v28, v36, v38

    .line 2947
    .local v28, "m30":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v30, v36, v38

    .line 2948
    .local v30, "m31":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v32, v36, v38

    .line 2949
    .local v32, "m32":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v34, v36, v38

    .line 2951
    .local v34, "m33":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2952
    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2953
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2954
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    goto/16 :goto_0
.end method

.method public final mulTransposeRight(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 42
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 2841
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2842
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2843
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2844
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2845
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2847
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2848
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2849
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2850
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2852
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2853
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2854
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2855
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2857
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2858
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2859
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2860
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-wide/from16 v0, v36

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2892
    :goto_0
    return-void

    .line 2867
    :cond_0
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v4, v36, v38

    .line 2868
    .local v4, "m00":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v6, v36, v38

    .line 2869
    .local v6, "m01":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v8, v36, v38

    .line 2870
    .local v8, "m02":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v10, v36, v38

    .line 2872
    .local v10, "m03":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v12, v36, v38

    .line 2873
    .local v12, "m10":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v14, v36, v38

    .line 2874
    .local v14, "m11":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v16, v36, v38

    .line 2875
    .local v16, "m12":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v18, v36, v38

    .line 2877
    .local v18, "m13":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v20, v36, v38

    .line 2878
    .local v20, "m20":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v22, v36, v38

    .line 2879
    .local v22, "m21":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v24, v36, v38

    .line 2880
    .local v24, "m22":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v26, v36, v38

    .line 2882
    .local v26, "m23":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m03:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v28, v36, v38

    .line 2883
    .local v28, "m30":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m13:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v30, v36, v38

    .line 2884
    .local v30, "m31":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m23:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v32, v36, v38

    .line 2885
    .local v32, "m32":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v36, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v38, v0

    mul-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v36, v36, v38

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v38, v0

    move-object/from16 v0, p2

    iget-wide v0, v0, Ljavax/vecmath/Matrix4d;->m33:D

    move-wide/from16 v40, v0

    mul-double v38, v38, v40

    add-double v34, v36, v38

    .line 2887
    .local v34, "m33":D
    move-object/from16 v0, p0

    iput-wide v4, v0, Ljavax/vecmath/Matrix4d;->m00:D

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljavax/vecmath/Matrix4d;->m01:D

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljavax/vecmath/Matrix4d;->m02:D

    move-object/from16 v0, p0

    iput-wide v10, v0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2888
    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Matrix4d;->m10:D

    move-object/from16 v0, p0

    iput-wide v14, v0, Ljavax/vecmath/Matrix4d;->m11:D

    move-wide/from16 v0, v16

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    move-wide/from16 v0, v18

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2889
    move-wide/from16 v0, v20

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    move-wide/from16 v0, v22

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2890
    move-wide/from16 v0, v28

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    move-wide/from16 v0, v30

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    move-wide/from16 v0, v32

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    move-wide/from16 v0, v34

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    goto/16 :goto_0
.end method

.method public final negate()V
    .locals 2

    .prologue
    .line 3523
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3524
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3525
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3526
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 3527
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3528
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3529
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3530
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 3531
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3532
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3533
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3534
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 3535
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 3536
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 3537
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 3538
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 3539
    return-void
.end method

.method public final negate(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 3548
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3549
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3550
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3551
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 3552
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3553
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3554
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3555
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 3556
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3557
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3558
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3559
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 3560
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 3561
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 3562
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 3563
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 3564
    return-void
.end method

.method public final rotX(D)V
    .locals 11
    .param p1, "angle"    # D

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 2495
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 2496
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 2498
    .local v0, "cosAngle":D
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2499
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2500
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2501
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2503
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2504
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2505
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2506
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2508
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2509
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2510
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2511
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2513
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2514
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2515
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2516
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2517
    return-void
.end method

.method public final rotY(D)V
    .locals 11
    .param p1, "angle"    # D

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 2528
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 2529
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 2531
    .local v0, "cosAngle":D
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2532
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2533
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2534
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2536
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2537
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2538
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2539
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2541
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2542
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2543
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2544
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2546
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2547
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2548
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2549
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2550
    return-void
.end method

.method public final rotZ(D)V
    .locals 11
    .param p1, "angle"    # D

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 2561
    invoke-static {p1, p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 2562
    .local v2, "sinAngle":D
    invoke-static {p1, p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    .line 2564
    .local v0, "cosAngle":D
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2565
    neg-double v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2566
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2567
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2569
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2570
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2571
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2572
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2574
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2575
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2576
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2577
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2579
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2580
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2581
    iput-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2582
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2583
    return-void
.end method

.method public final set(D)V
    .locals 3
    .param p1, "scale"    # D

    .prologue
    const-wide/16 v0, 0x0

    .line 2301
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2302
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2303
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2304
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2306
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2307
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2308
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2309
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2311
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2312
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2313
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2314
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2316
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2317
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2318
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2319
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2320
    return-void
.end method

.method public final set(DLjavax/vecmath/Vector3d;)V
    .locals 5
    .param p1, "scale"    # D
    .param p3, "v1"    # Ljavax/vecmath/Vector3d;

    .prologue
    const-wide/16 v2, 0x0

    .line 2359
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2360
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2361
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2362
    iget-wide v0, p3, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2364
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2365
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2366
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2367
    iget-wide v0, p3, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2369
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2370
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2371
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2372
    iget-wide v0, p3, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2374
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2375
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2376
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2377
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2378
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 30
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 1668
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v28, v0

    mul-double v26, v26, v28

    add-double v24, v24, v26

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v28, v0

    mul-double v26, v26, v28

    add-double v24, v24, v26

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 1670
    .local v12, "mag":D
    const-wide v24, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v24, v12, v24

    if-gez v24, :cond_0

    .line 1671
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1672
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1673
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1675
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1676
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1677
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1679
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1680
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1681
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1709
    :goto_0
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1710
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1711
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1713
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1714
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1715
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1716
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1717
    return-void

    .line 1683
    :cond_0
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    div-double v12, v24, v12

    .line 1684
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v24, v0

    mul-double v4, v24, v12

    .line 1685
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v24, v0

    mul-double v6, v24, v12

    .line 1686
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v24, v0

    mul-double v8, v24, v12

    .line 1688
    .local v8, "az":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 1689
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 1690
    .local v10, "cosTheta":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    sub-double v16, v24, v10

    .line 1692
    .local v16, "t":D
    mul-double v20, v4, v8

    .line 1693
    .local v20, "xz":D
    mul-double v18, v4, v6

    .line 1694
    .local v18, "xy":D
    mul-double v22, v6, v8

    .line 1696
    .local v22, "yz":D
    mul-double v24, v16, v4

    mul-double v24, v24, v4

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1697
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1698
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1700
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1701
    mul-double v24, v16, v6

    mul-double v24, v24, v6

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1702
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1704
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1705
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1706
    mul-double v24, v16, v8

    mul-double v24, v24, v8

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 28
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 1755
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v24, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v25, v0

    mul-float v24, v24, v25

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v25, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v26, v0

    mul-float v25, v25, v26

    add-float v24, v24, v25

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 1757
    .local v12, "mag":D
    const-wide v24, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v24, v12, v24

    if-gez v24, :cond_0

    .line 1758
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1759
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1760
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1762
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1763
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1764
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1766
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1767
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1768
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1795
    :goto_0
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1796
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1797
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1799
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1800
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1801
    const-wide/16 v24, 0x0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1802
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1803
    return-void

    .line 1770
    :cond_0
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    div-double v12, v24, v12

    .line 1771
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v4, v24, v12

    .line 1772
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v6, v24, v12

    .line 1773
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    mul-double v8, v24, v12

    .line 1775
    .local v8, "az":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 1776
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v24, v0

    move/from16 v0, v24

    float-to-double v0, v0

    move-wide/from16 v24, v0

    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 1777
    .local v10, "cosTheta":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    sub-double v16, v24, v10

    .line 1779
    .local v16, "t":D
    mul-double v20, v4, v8

    .line 1780
    .local v20, "xz":D
    mul-double v18, v4, v6

    .line 1781
    .local v18, "xy":D
    mul-double v22, v6, v8

    .line 1783
    .local v22, "yz":D
    mul-double v24, v16, v4

    mul-double v24, v24, v4

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1784
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1785
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1787
    mul-double v24, v16, v18

    mul-double v26, v14, v8

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1788
    mul-double v24, v16, v6

    mul-double v24, v24, v6

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1789
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1791
    mul-double v24, v16, v20

    mul-double v26, v14, v6

    sub-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1792
    mul-double v24, v16, v22

    mul-double v26, v14, v4

    add-double v24, v24, v26

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1793
    mul-double v24, v16, v8

    mul-double v24, v24, v8

    add-double v24, v24, v10

    move-wide/from16 v0, v24

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const-wide/16 v2, 0x0

    .line 1626
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1627
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1628
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1629
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1630
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Vector3d;D)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "scale"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 2452
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2453
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2454
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2455
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2457
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2458
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2459
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2460
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2462
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2463
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2464
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2465
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2467
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2468
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2469
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2470
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2471
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const-wide/16 v2, 0x0

    .line 1611
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1612
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1613
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1614
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1615
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3f;F)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "scale"    # F

    .prologue
    const-wide/16 v2, 0x0

    .line 2420
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2421
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2422
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2423
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2425
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2426
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2427
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2428
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2430
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2431
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2432
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v0, p3

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2433
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2435
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2436
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2437
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2438
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2439
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1933
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1934
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1935
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1936
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1938
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1939
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1940
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1941
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1943
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1944
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1945
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1946
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1948
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1949
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1950
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1951
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1952
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1905
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1906
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1907
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1908
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1910
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1911
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1912
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1913
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1915
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1916
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1917
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1918
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1920
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1921
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1922
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1923
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1924
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1639
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1640
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1641
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1643
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1644
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1645
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1647
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1648
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1649
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1651
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1652
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1653
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1655
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1656
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1657
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1658
    iput-wide v10, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1659
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Vector3d;D)V
    .locals 9
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    .line 1814
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1815
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1816
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1818
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1819
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1820
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1822
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1823
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1824
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1826
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1827
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1828
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1830
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1831
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1832
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1833
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1834
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1726
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1727
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1728
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1730
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1731
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1732
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1734
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1735
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1736
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v0, v0

    mul-double/2addr v0, v6

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v2, v2

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v2, v2

    mul-double/2addr v2, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1738
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1739
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1740
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1742
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1743
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1744
    iput-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1745
    iput-wide v10, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1746
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Vector3d;D)V
    .locals 7
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    .line 1845
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1846
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1847
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1849
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1850
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1851
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1853
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1854
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1855
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1857
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1858
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1859
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1861
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1862
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1863
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1864
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1865
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Vector3f;F)V
    .locals 8
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "s"    # F

    .prologue
    .line 1876
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1877
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1878
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1880
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1881
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1882
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1884
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1885
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1886
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1888
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1889
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1890
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1892
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1893
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1894
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1895
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1896
    return-void
.end method

.method public final set(Ljavax/vecmath/Vector3d;)V
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector3d;

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v2, 0x0

    .line 2329
    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2330
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2331
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2332
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2334
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2335
    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2336
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2337
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2339
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2340
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2341
    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2342
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2344
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2345
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2346
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2347
    iput-wide v4, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2348
    return-void
.end method

.method public final set(Ljavax/vecmath/Vector3d;D)V
    .locals 4
    .param p1, "v1"    # Ljavax/vecmath/Vector3d;
    .param p2, "scale"    # D

    .prologue
    const-wide/16 v2, 0x0

    .line 2389
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 2390
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 2391
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 2392
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v0, p2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2394
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 2395
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 2396
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 2397
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v0, p2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2399
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 2400
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 2401
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 2402
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v0, p2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2404
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 2405
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 2406
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 2407
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 2408
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "m"    # [D

    .prologue
    .line 1584
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1585
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1586
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1587
    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1588
    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1589
    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1590
    const/4 v0, 0x6

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1591
    const/4 v0, 0x7

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1592
    const/16 v0, 0x8

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1593
    const/16 v0, 0x9

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1594
    const/16 v0, 0xa

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1595
    const/16 v0, 0xb

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1596
    const/16 v0, 0xc

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1597
    const/16 v0, 0xd

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1598
    const/16 v0, 0xe

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1599
    const/16 v0, 0xf

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1600
    return-void
.end method

.method public final setColumn(IDDDD)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "x"    # D
    .param p4, "y"    # D
    .param p6, "z"    # D
    .param p8, "w"    # D

    .prologue
    .line 1235
    packed-switch p1, :pswitch_data_0

    .line 1265
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d7"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1237
    :pswitch_0
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1238
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1239
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1240
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1267
    :goto_0
    return-void

    .line 1244
    :pswitch_1
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1245
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1246
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1247
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    goto :goto_0

    .line 1251
    :pswitch_2
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1252
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1253
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1254
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    goto :goto_0

    .line 1258
    :pswitch_3
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1259
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1260
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1261
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1235
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setColumn(ILjavax/vecmath/Vector4d;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 1276
    packed-switch p1, :pswitch_data_0

    .line 1306
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d7"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1278
    :pswitch_0
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1279
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1280
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1281
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1308
    :goto_0
    return-void

    .line 1285
    :pswitch_1
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1286
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1287
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1288
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    goto :goto_0

    .line 1292
    :pswitch_2
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1293
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1294
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1295
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    goto :goto_0

    .line 1299
    :pswitch_3
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1300
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1301
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1302
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1276
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setColumn(I[D)V
    .locals 5
    .param p1, "column"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1317
    packed-switch p1, :pswitch_data_0

    .line 1347
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d7"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1319
    :pswitch_0
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1320
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1321
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1322
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1349
    :goto_0
    return-void

    .line 1326
    :pswitch_1
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1327
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1328
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1329
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    goto :goto_0

    .line 1333
    :pswitch_2
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1334
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1335
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1336
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    goto :goto_0

    .line 1340
    :pswitch_3
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1341
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1342
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1343
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1317
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setElement(IID)V
    .locals 3
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "value"    # D

    .prologue
    .line 473
    packed-switch p1, :pswitch_data_0

    .line 556
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 476
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 491
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :pswitch_1
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 558
    :goto_0
    return-void

    .line 482
    :pswitch_2
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m01:D

    goto :goto_0

    .line 485
    :pswitch_3
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m02:D

    goto :goto_0

    .line 488
    :pswitch_4
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m03:D

    goto :goto_0

    .line 496
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    .line 511
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 499
    :pswitch_6
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m10:D

    goto :goto_0

    .line 502
    :pswitch_7
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m11:D

    goto :goto_0

    .line 505
    :pswitch_8
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m12:D

    goto :goto_0

    .line 508
    :pswitch_9
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m13:D

    goto :goto_0

    .line 516
    :pswitch_a
    packed-switch p2, :pswitch_data_3

    .line 531
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 519
    :pswitch_b
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m20:D

    goto :goto_0

    .line 522
    :pswitch_c
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m21:D

    goto :goto_0

    .line 525
    :pswitch_d
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m22:D

    goto :goto_0

    .line 528
    :pswitch_e
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m23:D

    goto :goto_0

    .line 536
    :pswitch_f
    packed-switch p2, :pswitch_data_4

    .line 551
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 539
    :pswitch_10
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m30:D

    goto :goto_0

    .line 542
    :pswitch_11
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m31:D

    goto :goto_0

    .line 545
    :pswitch_12
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m32:D

    goto :goto_0

    .line 548
    :pswitch_13
    iput-wide p3, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 473
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch

    .line 476
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 496
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 516
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 536
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final setIdentity()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 444
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 445
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 446
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 447
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 449
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 450
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 451
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 452
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 454
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 455
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 456
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 457
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 459
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 460
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 461
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 462
    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 463
    return-void
.end method

.method public final setM00(D)V
    .locals 1
    .param p1, "m00"    # D

    .prologue
    .line 3624
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3625
    return-void
.end method

.method public final setM01(D)V
    .locals 1
    .param p1, "m01"    # D

    .prologue
    .line 3646
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3647
    return-void
.end method

.method public final setM02(D)V
    .locals 1
    .param p1, "m02"    # D

    .prologue
    .line 3668
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3669
    return-void
.end method

.method public final setM03(D)V
    .locals 1
    .param p1, "m03"    # D

    .prologue
    .line 3823
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 3824
    return-void
.end method

.method public final setM10(D)V
    .locals 1
    .param p1, "m10"    # D

    .prologue
    .line 3690
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3691
    return-void
.end method

.method public final setM11(D)V
    .locals 1
    .param p1, "m11"    # D

    .prologue
    .line 3712
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3713
    return-void
.end method

.method public final setM12(D)V
    .locals 1
    .param p1, "m12"    # D

    .prologue
    .line 3735
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3736
    return-void
.end method

.method public final setM13(D)V
    .locals 1
    .param p1, "m13"    # D

    .prologue
    .line 3845
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 3846
    return-void
.end method

.method public final setM20(D)V
    .locals 1
    .param p1, "m20"    # D

    .prologue
    .line 3757
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3758
    return-void
.end method

.method public final setM21(D)V
    .locals 1
    .param p1, "m21"    # D

    .prologue
    .line 3779
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3780
    return-void
.end method

.method public final setM22(D)V
    .locals 1
    .param p1, "m22"    # D

    .prologue
    .line 3801
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3802
    return-void
.end method

.method public final setM23(D)V
    .locals 1
    .param p1, "m23"    # D

    .prologue
    .line 3867
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 3868
    return-void
.end method

.method public final setM30(D)V
    .locals 1
    .param p1, "m30"    # D

    .prologue
    .line 3889
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 3890
    return-void
.end method

.method public final setM31(D)V
    .locals 1
    .param p1, "m31"    # D

    .prologue
    .line 3911
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 3912
    return-void
.end method

.method public final setM32(D)V
    .locals 1
    .param p1, "m32"    # D

    .prologue
    .line 3934
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 3935
    return-void
.end method

.method public final setM33(D)V
    .locals 1
    .param p1, "m33"    # D

    .prologue
    .line 3956
    iput-wide p1, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 3957
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/AxisAngle4d;)V
    .locals 34
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 3463
    const/16 v26, 0x9

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 3464
    .local v18, "tmp_rot":[D
    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v19, v0

    .line 3466
    .local v19, "tmp_scale":[D
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 3468
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v28, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v30, v0

    mul-double v28, v28, v30

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v32, v0

    mul-double v30, v30, v32

    add-double v28, v28, v30

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v30, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v32, v0

    mul-double v30, v30, v32

    add-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    div-double v12, v26, v28

    .line 3469
    .local v12, "mag":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v26, v0

    mul-double v4, v26, v12

    .line 3470
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v26, v0

    mul-double v6, v26, v12

    .line 3471
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v26, v0

    mul-double v8, v26, v12

    .line 3473
    .local v8, "az":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 3474
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 3475
    .local v10, "cosTheta":D
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    sub-double v16, v26, v10

    .line 3477
    .local v16, "t":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v28, v0

    mul-double v22, v26, v28

    .line 3478
    .local v22, "xz":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v28, v0

    mul-double v20, v26, v28

    .line 3479
    .local v20, "xy":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v28, v0

    mul-double v24, v26, v28

    .line 3481
    .local v24, "yz":D
    mul-double v26, v16, v4

    mul-double v26, v26, v4

    add-double v26, v26, v10

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3482
    mul-double v26, v16, v20

    mul-double v28, v14, v8

    sub-double v26, v26, v28

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3483
    mul-double v26, v16, v22

    mul-double v28, v14, v6

    add-double v26, v26, v28

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3485
    mul-double v26, v16, v20

    mul-double v28, v14, v8

    add-double v26, v26, v28

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3486
    mul-double v26, v16, v6

    mul-double v26, v26, v6

    add-double v26, v26, v10

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3487
    mul-double v26, v16, v24

    mul-double v28, v14, v4

    sub-double v26, v26, v28

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3489
    mul-double v26, v16, v22

    mul-double v28, v14, v6

    sub-double v26, v26, v28

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3490
    mul-double v26, v16, v24

    mul-double v28, v14, v4

    add-double v26, v26, v28

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3491
    mul-double v26, v16, v8

    mul-double v26, v26, v8

    add-double v26, v26, v10

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    move-object/from16 v2, p0

    iput-wide v0, v2, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3493
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Matrix3d;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3341
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3342
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3344
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 3346
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3347
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3348
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3350
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3351
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3352
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3354
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3355
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3356
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3358
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Matrix3f;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3374
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3375
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3376
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 3378
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3379
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3380
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3382
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3383
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3384
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3386
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3387
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3388
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3389
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 3433
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3434
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3435
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 3437
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3438
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3439
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3441
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3442
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3443
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3445
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3446
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3447
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3449
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Quat4f;)V
    .locals 13
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/4 v12, 0x1

    const/4 v9, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/high16 v8, 0x40000000    # 2.0f

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 3402
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3403
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3404
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 3406
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v8

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    sub-double v2, v10, v2

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v8

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3407
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3408
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3410
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    aget-wide v4, v1, v12

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3411
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v8

    iget v3, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    sub-double v2, v10, v2

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v8

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    aget-wide v4, v1, v12

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3412
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    aget-wide v4, v1, v12

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3414
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3415
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v6

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3416
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v8

    iget v3, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v3

    float-to-double v2, v2

    sub-double v2, v10, v2

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v8

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    float-to-double v4, v4

    sub-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3418
    return-void
.end method

.method public final setRotationScale(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1054
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1055
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1056
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1057
    return-void
.end method

.method public final setRotationScale(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1066
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1067
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1068
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1069
    return-void
.end method

.method public final setRow(IDDDD)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "x"    # D
    .param p4, "y"    # D
    .param p6, "z"    # D
    .param p8, "w"    # D

    .prologue
    .line 1108
    packed-switch p1, :pswitch_data_0

    .line 1138
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1110
    :pswitch_0
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1111
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1112
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1113
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1141
    :goto_0
    return-void

    .line 1117
    :pswitch_1
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1118
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1119
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1120
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m13:D

    goto :goto_0

    .line 1124
    :pswitch_2
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1125
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1126
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1127
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    goto :goto_0

    .line 1131
    :pswitch_3
    iput-wide p2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1132
    iput-wide p4, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1133
    iput-wide p6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1134
    iput-wide p8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1108
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setRow(ILjavax/vecmath/Vector4d;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 1150
    packed-switch p1, :pswitch_data_0

    .line 1180
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1152
    :pswitch_0
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1153
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1154
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1155
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1182
    :goto_0
    return-void

    .line 1159
    :pswitch_1
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1160
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1161
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1162
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    goto :goto_0

    .line 1166
    :pswitch_2
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1167
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1168
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1169
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    goto :goto_0

    .line 1173
    :pswitch_3
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1174
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1175
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1176
    iget-wide v0, p2, Ljavax/vecmath/Vector4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1150
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setRow(I[D)V
    .locals 5
    .param p1, "row"    # I
    .param p2, "v"    # [D

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 1191
    packed-switch p1, :pswitch_data_0

    .line 1221
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4d4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1193
    :pswitch_0
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1194
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1195
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1196
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1223
    :goto_0
    return-void

    .line 1200
    :pswitch_1
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1201
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1202
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1203
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    goto :goto_0

    .line 1207
    :pswitch_2
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1208
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1209
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1210
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    goto :goto_0

    .line 1214
    :pswitch_3
    aget-wide v0, p2, v0

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1215
    aget-wide v0, p2, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1216
    aget-wide v0, p2, v3

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1217
    aget-wide v0, p2, v4

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    goto :goto_0

    .line 1191
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setScale(D)V
    .locals 5
    .param p1, "scale"    # D

    .prologue
    const/4 v4, 0x3

    .line 1079
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 1080
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 1082
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4d;->getScaleRotate([D[D)V

    .line 1084
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1085
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1086
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1088
    aget-wide v2, v0, v4

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1089
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1090
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1092
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1093
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1094
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    mul-double/2addr v2, p1

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1096
    return-void
.end method

.method public final setTranslation(Ljavax/vecmath/Vector3d;)V
    .locals 2
    .param p1, "trans"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 2481
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 2482
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 2483
    iget-wide v0, p1, Ljavax/vecmath/Vector3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 2484
    return-void
.end method

.method public final setZero()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 3500
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 3501
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 3502
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 3503
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 3504
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 3505
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 3506
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 3507
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 3508
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 3509
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 3510
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 3511
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 3512
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 3513
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 3514
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 3515
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 3516
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix4d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1493
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m00:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1494
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m01:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1495
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m02:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1496
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m03:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1498
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1499
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m11:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1500
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m12:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1501
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m13:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1503
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m20:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1504
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1505
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m22:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1506
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m23:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1508
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m30:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1509
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m31:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1510
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m32:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1511
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m33:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1512
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix4d;Ljavax/vecmath/Matrix4d;)V
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1464
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m00:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1465
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m01:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1466
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m02:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1467
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m03:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1469
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m10:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1470
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m11:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1471
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m12:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1472
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m13:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1474
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m20:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1475
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m21:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1476
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m22:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1477
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m23:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1479
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m30:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1480
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m31:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1481
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m32:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1482
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v2, p2, Ljavax/vecmath/Matrix4d;->m33:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1483
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 432
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m11:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m22:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m33:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljavax/vecmath/Point3d;)V
    .locals 10
    .param p1, "point"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 3219
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    add-double v0, v4, v6

    .line 3220
    .local v0, "x":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    add-double v2, v4, v6

    .line 3221
    .local v2, "y":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v4, v6

    iput-wide v4, p1, Ljavax/vecmath/Point3d;->z:D

    .line 3222
    iput-wide v0, p1, Ljavax/vecmath/Point3d;->x:D

    .line 3223
    iput-wide v2, p1, Ljavax/vecmath/Point3d;->y:D

    .line 3224
    return-void
.end method

.method public final transform(Ljavax/vecmath/Point3d;Ljavax/vecmath/Point3d;)V
    .locals 10
    .param p1, "point"    # Ljavax/vecmath/Point3d;
    .param p2, "pointOut"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 3201
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    add-double v0, v4, v6

    .line 3202
    .local v0, "x":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    add-double v2, v4, v6

    .line 3203
    .local v2, "y":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v4, v6

    iput-wide v4, p2, Ljavax/vecmath/Point3d;->z:D

    .line 3204
    iput-wide v0, p2, Ljavax/vecmath/Point3d;->x:D

    .line 3205
    iput-wide v2, p2, Ljavax/vecmath/Point3d;->y:D

    .line 3207
    return-void
.end method

.method public final transform(Ljavax/vecmath/Point3f;)V
    .locals 8
    .param p1, "point"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 3255
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 3256
    .local v0, "x":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 3257
    .local v1, "y":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Point3f;->z:F

    .line 3258
    iput v0, p1, Ljavax/vecmath/Point3f;->x:F

    .line 3259
    iput v1, p1, Ljavax/vecmath/Point3f;->y:F

    .line 3260
    return-void
.end method

.method public final transform(Ljavax/vecmath/Point3f;Ljavax/vecmath/Point3f;)V
    .locals 8
    .param p1, "point"    # Ljavax/vecmath/Point3f;
    .param p2, "pointOut"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 3238
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m03:D

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 3239
    .local v0, "x":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m13:D

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 3240
    .local v1, "y":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v4, p1, Ljavax/vecmath/Point3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v6, p1, Ljavax/vecmath/Point3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v6, p1, Ljavax/vecmath/Point3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m23:D

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p2, Ljavax/vecmath/Point3f;->z:F

    .line 3241
    iput v0, p2, Ljavax/vecmath/Point3f;->x:F

    .line 3242
    iput v1, p2, Ljavax/vecmath/Point3f;->y:F

    .line 3243
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4d;)V
    .locals 12
    .param p1, "vec"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 3133
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v0, v6, v8

    .line 3135
    .local v0, "x":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v2, v6, v8

    .line 3137
    .local v2, "y":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    .line 3139
    .local v4, "z":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iput-wide v6, p1, Ljavax/vecmath/Tuple4d;->w:D

    .line 3141
    iput-wide v0, p1, Ljavax/vecmath/Tuple4d;->x:D

    .line 3142
    iput-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    .line 3143
    iput-wide v4, p1, Ljavax/vecmath/Tuple4d;->z:D

    .line 3144
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4d;Ljavax/vecmath/Tuple4d;)V
    .locals 12
    .param p1, "vec"    # Ljavax/vecmath/Tuple4d;
    .param p2, "vecOut"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 3111
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v0, v6, v8

    .line 3113
    .local v0, "x":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v2, v6, v8

    .line 3115
    .local v2, "y":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    .line 3117
    .local v4, "z":D
    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget-wide v10, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iput-wide v6, p2, Ljavax/vecmath/Tuple4d;->w:D

    .line 3119
    iput-wide v0, p2, Ljavax/vecmath/Tuple4d;->x:D

    .line 3120
    iput-wide v2, p2, Ljavax/vecmath/Tuple4d;->y:D

    .line 3121
    iput-wide v4, p2, Ljavax/vecmath/Tuple4d;->z:D

    .line 3122
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4f;)V
    .locals 10
    .param p1, "vec"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 3177
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v0, v4

    .line 3179
    .local v0, "x":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v1, v4

    .line 3181
    .local v1, "y":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 3183
    .local v2, "z":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    .line 3185
    iput v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    .line 3186
    iput v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    .line 3187
    iput v2, p1, Ljavax/vecmath/Tuple4f;->z:F

    .line 3188
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;)V
    .locals 10
    .param p1, "vec"    # Ljavax/vecmath/Tuple4f;
    .param p2, "vecOut"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 3155
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v0, v4

    .line 3157
    .local v0, "x":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v1, v4

    .line 3159
    .local v1, "y":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v2, v4

    .line 3161
    .local v2, "z":F
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m30:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v6, v3

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m31:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m32:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m33:D

    iget v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v8, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v3, v4

    iput v3, p2, Ljavax/vecmath/Tuple4f;->w:F

    .line 3163
    iput v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    .line 3164
    iput v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    .line 3165
    iput v2, p2, Ljavax/vecmath/Tuple4f;->z:F

    .line 3166
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3d;)V
    .locals 10
    .param p1, "normal"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 3289
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double v0, v4, v6

    .line 3290
    .local v0, "x":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 3291
    .local v2, "y":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iput-wide v4, p1, Ljavax/vecmath/Vector3d;->z:D

    .line 3292
    iput-wide v0, p1, Ljavax/vecmath/Vector3d;->x:D

    .line 3293
    iput-wide v2, p1, Ljavax/vecmath/Vector3d;->y:D

    .line 3294
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3d;Ljavax/vecmath/Vector3d;)V
    .locals 10
    .param p1, "normal"    # Ljavax/vecmath/Vector3d;
    .param p2, "normalOut"    # Ljavax/vecmath/Vector3d;

    .prologue
    .line 3272
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double v0, v4, v6

    .line 3273
    .local v0, "x":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double v2, v4, v6

    .line 3274
    .local v2, "y":D
    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget-wide v6, p1, Ljavax/vecmath/Vector3d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget-wide v8, p1, Ljavax/vecmath/Vector3d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iput-wide v4, p2, Ljavax/vecmath/Vector3d;->z:D

    .line 3275
    iput-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    .line 3276
    iput-wide v2, p2, Ljavax/vecmath/Vector3d;->y:D

    .line 3277
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3f;)V
    .locals 8
    .param p1, "normal"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 3323
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 3324
    .local v0, "x":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 3325
    .local v1, "y":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Vector3f;->z:F

    .line 3326
    iput v0, p1, Ljavax/vecmath/Vector3f;->x:F

    .line 3327
    iput v1, p1, Ljavax/vecmath/Vector3f;->y:F

    .line 3328
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3f;Ljavax/vecmath/Vector3f;)V
    .locals 8
    .param p1, "normal"    # Ljavax/vecmath/Vector3f;
    .param p2, "normalOut"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 3306
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m00:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 3307
    .local v0, "x":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m11:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v1, v2

    .line 3308
    .local v1, "y":F
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    iget v4, p1, Ljavax/vecmath/Vector3f;->x:F

    float-to-double v4, v4

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m21:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Matrix4d;->m22:D

    iget v6, p1, Ljavax/vecmath/Vector3f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p2, Ljavax/vecmath/Vector3f;->z:F

    .line 3309
    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 3310
    iput v1, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 3311
    return-void
.end method

.method public final transpose()V
    .locals 4

    .prologue
    .line 1521
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1522
    .local v0, "temp":D
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1523
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1525
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1526
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1527
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1529
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1530
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1531
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1533
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1534
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1535
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1537
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1538
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1539
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1541
    iget-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1542
    iget-wide v2, p0, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v2, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1543
    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1544
    return-void
.end method

.method public final transpose(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1552
    if-eq p0, p1, :cond_0

    .line 1553
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m00:D

    .line 1554
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m01:D

    .line 1555
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m02:D

    .line 1556
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m03:D

    .line 1558
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m10:D

    .line 1559
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m11:D

    .line 1560
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m12:D

    .line 1561
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m13:D

    .line 1563
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m20:D

    .line 1564
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m21:D

    .line 1565
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m22:D

    .line 1566
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m23:D

    .line 1568
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m30:D

    .line 1569
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m31:D

    .line 1570
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m32:D

    .line 1571
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    iput-wide v0, p0, Ljavax/vecmath/Matrix4d;->m33:D

    .line 1574
    :goto_0
    return-void

    .line 1573
    :cond_0
    invoke-virtual {p0}, Ljavax/vecmath/Matrix4d;->transpose()V

    goto :goto_0
.end method
