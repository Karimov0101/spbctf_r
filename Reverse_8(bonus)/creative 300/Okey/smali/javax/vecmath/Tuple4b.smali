.class public abstract Ljavax/vecmath/Tuple4b;
.super Ljava/lang/Object;
.source "Tuple4b.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = -0x722b354b4a8cb763L


# instance fields
.field public w:B

.field public x:B

.field public y:B

.field public z:B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 123
    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 124
    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 125
    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 126
    return-void
.end method

.method public constructor <init>(BBBB)V
    .locals 0
    .param p1, "b1"    # B
    .param p2, "b2"    # B
    .param p3, "b3"    # B
    .param p4, "b4"    # B

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-byte p1, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 85
    iput-byte p2, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 86
    iput-byte p3, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 87
    iput-byte p4, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 88
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4b;

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->x:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 111
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->y:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 112
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->z:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 113
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->w:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 114
    return-void
.end method

.method public constructor <init>([B)V
    .locals 1
    .param p1, "t"    # [B

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 98
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 99
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 100
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 101
    return-void
.end method


# virtual methods
.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 258
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 259
    :catch_0
    move-exception v0

    .line 261
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 222
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple4b;

    move-object v3, v0

    .line 223
    .local v3, "t2":Ljavax/vecmath/Tuple4b;
    iget-byte v5, p0, Ljavax/vecmath/Tuple4b;->x:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple4b;->x:B

    if-ne v5, v6, :cond_0

    iget-byte v5, p0, Ljavax/vecmath/Tuple4b;->y:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple4b;->y:B

    if-ne v5, v6, :cond_0

    iget-byte v5, p0, Ljavax/vecmath/Tuple4b;->z:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple4b;->z:B

    if-ne v5, v6, :cond_0

    iget-byte v5, p0, Ljavax/vecmath/Tuple4b;->w:B

    iget-byte v6, v3, Ljavax/vecmath/Tuple4b;->w:B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    .line 227
    .end local v3    # "t2":Ljavax/vecmath/Tuple4b;
    :cond_0
    :goto_0
    return v4

    .line 226
    :catch_0
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 227
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple4b;)Z
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple4b;

    .prologue
    const/4 v1, 0x0

    .line 206
    :try_start_0
    iget-byte v2, p0, Ljavax/vecmath/Tuple4b;->x:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple4b;->x:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Ljavax/vecmath/Tuple4b;->y:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple4b;->y:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Ljavax/vecmath/Tuple4b;->z:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple4b;->z:B

    if-ne v2, v3, :cond_0

    iget-byte v2, p0, Ljavax/vecmath/Tuple4b;->w:B

    iget-byte v3, p1, Ljavax/vecmath/Tuple4b;->w:B
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    .line 209
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple4b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4b;

    .prologue
    .line 163
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple4b;->x:B

    .line 164
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple4b;->y:B

    .line 165
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple4b;->z:B

    .line 166
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    iput-byte v0, p1, Ljavax/vecmath/Tuple4b;->w:B

    .line 167
    return-void
.end method

.method public final get([B)V
    .locals 2
    .param p1, "b"    # [B

    .prologue
    .line 149
    const/4 v0, 0x0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->x:B

    aput-byte v1, p1, v0

    .line 150
    const/4 v0, 0x1

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->y:B

    aput-byte v1, p1, v0

    .line 151
    const/4 v0, 0x2

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->z:B

    aput-byte v1, p1, v0

    .line 152
    const/4 v0, 0x3

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->w:B

    aput-byte v1, p1, v0

    .line 153
    return-void
.end method

.method public final getW()B
    .locals 1

    .prologue
    .line 345
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    return v0
.end method

.method public final getX()B
    .locals 1

    .prologue
    .line 274
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    return v0
.end method

.method public final getY()B
    .locals 1

    .prologue
    .line 298
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    return v0
.end method

.method public final getZ()B
    .locals 1

    .prologue
    .line 321
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 241
    iget-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->y:B

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->z:B

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->w:B

    and-int/lit16 v1, v1, 0xff

    shl-int/lit8 v1, v1, 0x18

    or-int/2addr v0, v1

    return v0
.end method

.method public final set(Ljavax/vecmath/Tuple4b;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4b;

    .prologue
    .line 177
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->x:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 178
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->y:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 179
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->z:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 180
    iget-byte v0, p1, Ljavax/vecmath/Tuple4b;->w:B

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 181
    return-void
.end method

.method public final set([B)V
    .locals 1
    .param p1, "b"    # [B

    .prologue
    .line 191
    const/4 v0, 0x0

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 192
    const/4 v0, 0x1

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 193
    const/4 v0, 0x2

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 194
    const/4 v0, 0x3

    aget-byte v0, p1, v0

    iput-byte v0, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 195
    return-void
.end method

.method public final setW(B)V
    .locals 0
    .param p1, "w"    # B

    .prologue
    .line 357
    iput-byte p1, p0, Ljavax/vecmath/Tuple4b;->w:B

    .line 358
    return-void
.end method

.method public final setX(B)V
    .locals 0
    .param p1, "x"    # B

    .prologue
    .line 286
    iput-byte p1, p0, Ljavax/vecmath/Tuple4b;->x:B

    .line 287
    return-void
.end method

.method public final setY(B)V
    .locals 0
    .param p1, "y"    # B

    .prologue
    .line 310
    iput-byte p1, p0, Ljavax/vecmath/Tuple4b;->y:B

    .line 311
    return-void
.end method

.method public final setZ(B)V
    .locals 0
    .param p1, "z"    # B

    .prologue
    .line 333
    iput-byte p1, p0, Ljavax/vecmath/Tuple4b;->z:B

    .line 334
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->x:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->y:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->z:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-byte v1, p0, Ljavax/vecmath/Tuple4b;->w:B

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
