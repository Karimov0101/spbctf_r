.class public abstract Ljavax/vecmath/Tuple3i;
.super Ljava/lang/Object;
.source "Tuple3i.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = -0xa2b3798545562a8L


# instance fields
.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 104
    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 105
    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 106
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 71
    iput p2, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 72
    iput p3, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 73
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 94
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 95
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 96
    return-void
.end method

.method public constructor <init>([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 82
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 83
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 84
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 498
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 499
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 500
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 501
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 430
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 431
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 432
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 433
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 185
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 186
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 187
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 188
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3i;Ljavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 174
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 175
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 176
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 177
    return-void
.end method

.method public final clamp(II)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 442
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    if-le v0, p2, :cond_3

    .line 443
    iput p2, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 448
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    if-le v0, p2, :cond_4

    .line 449
    iput p2, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 454
    :cond_1
    :goto_1
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    if-le v0, p2, :cond_5

    .line 455
    iput p2, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 459
    :cond_2
    :goto_2
    return-void

    .line 444
    :cond_3
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    if-ge v0, p1, :cond_0

    .line 445
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    goto :goto_0

    .line 450
    :cond_4
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    if-ge v0, p1, :cond_1

    .line 451
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    goto :goto_1

    .line 456
    :cond_5
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    if-ge v0, p1, :cond_2

    .line 457
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    goto :goto_2
.end method

.method public final clamp(IILjavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "t"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 344
    iget v0, p3, Ljavax/vecmath/Tuple3i;->x:I

    if-le v0, p2, :cond_0

    .line 345
    iput p2, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 352
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple3i;->y:I

    if-le v0, p2, :cond_2

    .line 353
    iput p2, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 360
    :goto_1
    iget v0, p3, Ljavax/vecmath/Tuple3i;->z:I

    if-le v0, p2, :cond_4

    .line 361
    iput p2, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 367
    :goto_2
    return-void

    .line 346
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple3i;->x:I

    if-ge v0, p1, :cond_1

    .line 347
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    goto :goto_0

    .line 349
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    goto :goto_0

    .line 354
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple3i;->y:I

    if-ge v0, p1, :cond_3

    .line 355
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    goto :goto_1

    .line 357
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    goto :goto_1

    .line 362
    :cond_4
    iget v0, p3, Ljavax/vecmath/Tuple3i;->z:I

    if-ge v0, p1, :cond_5

    .line 363
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    goto :goto_2

    .line 365
    :cond_5
    iget v0, p3, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    goto :goto_2
.end method

.method public final clampMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 483
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    if-le v0, p1, :cond_0

    .line 484
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 486
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    if-le v0, p1, :cond_1

    .line 487
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 489
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    if-le v0, p1, :cond_2

    .line 490
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 491
    :cond_2
    return-void
.end method

.method public final clampMax(ILjavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "max"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 404
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    if-le v0, p1, :cond_0

    .line 405
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 410
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    if-le v0, p1, :cond_1

    .line 411
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 416
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    if-le v0, p1, :cond_2

    .line 417
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 421
    :goto_2
    return-void

    .line 407
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    goto :goto_0

    .line 413
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    goto :goto_1

    .line 419
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    goto :goto_2
.end method

.method public final clampMin(I)V
    .locals 1
    .param p1, "min"    # I

    .prologue
    .line 467
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    if-ge v0, p1, :cond_0

    .line 468
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 470
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    if-ge v0, p1, :cond_1

    .line 471
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 473
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    if-ge v0, p1, :cond_2

    .line 474
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 475
    :cond_2
    return-void
.end method

.method public final clampMin(ILjavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 377
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    if-ge v0, p1, :cond_0

    .line 378
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 383
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    if-ge v0, p1, :cond_1

    .line 384
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 389
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    if-ge v0, p1, :cond_2

    .line 390
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 394
    :goto_2
    return-void

    .line 380
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    goto :goto_0

    .line 386
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    goto :goto_1

    .line 392
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    goto :goto_2
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 514
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 515
    :catch_0
    move-exception v0

    .line 517
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 307
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple3i;

    move-object v3, v0

    .line 308
    .local v3, "t2":Ljavax/vecmath/Tuple3i;
    iget v5, p0, Ljavax/vecmath/Tuple3i;->x:I

    iget v6, v3, Ljavax/vecmath/Tuple3i;->x:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple3i;->y:I

    iget v6, v3, Ljavax/vecmath/Tuple3i;->y:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple3i;->z:I

    iget v6, v3, Ljavax/vecmath/Tuple3i;->z:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    .line 314
    .end local v3    # "t2":Ljavax/vecmath/Tuple3i;
    :cond_0
    :goto_0
    return v4

    .line 310
    :catch_0
    move-exception v2

    .line 311
    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 313
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 314
    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 162
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    .line 163
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    .line 164
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    .line 165
    return-void
.end method

.method public final get([I)V
    .locals 2
    .param p1, "t"    # [I

    .prologue
    .line 151
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple3i;->x:I

    aput v1, p1, v0

    .line 152
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple3i;->y:I

    aput v1, p1, v0

    .line 153
    const/4 v0, 0x2

    iget v1, p0, Ljavax/vecmath/Tuple3i;->z:I

    aput v1, p1, v0

    .line 154
    return-void
.end method

.method public final getX()I
    .locals 1

    .prologue
    .line 530
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    return v0
.end method

.method public final getY()I
    .locals 1

    .prologue
    .line 554
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    return v0
.end method

.method public final getZ()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 328
    const-wide/16 v0, 0x1

    .line 329
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3i;->x:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 330
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3i;->y:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 331
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple3i;->z:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 332
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 232
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 233
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 234
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 221
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 222
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 223
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 224
    return-void
.end method

.method public final scale(I)V
    .locals 1
    .param p1, "s"    # I

    .prologue
    .line 256
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 257
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 258
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 259
    return-void
.end method

.method public final scale(ILjavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 244
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 245
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 246
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 247
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 283
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 284
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 285
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple3i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 286
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple3i;Ljavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple3i;
    .param p3, "t2"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 270
    iget v0, p2, Ljavax/vecmath/Tuple3i;->x:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 271
    iget v0, p2, Ljavax/vecmath/Tuple3i;->y:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 272
    iget v0, p2, Ljavax/vecmath/Tuple3i;->z:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple3i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 273
    return-void
.end method

.method public final set(III)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 117
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 118
    iput p2, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 119
    iput p3, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 120
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 140
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 141
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 142
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 143
    return-void
.end method

.method public final set([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 129
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 130
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 131
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 132
    return-void
.end method

.method public final setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 542
    iput p1, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 543
    return-void
.end method

.method public final setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 566
    iput p1, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 567
    return-void
.end method

.method public final setZ(I)V
    .locals 0
    .param p1, "z"    # I

    .prologue
    .line 588
    iput p1, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 589
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 210
    iget v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 211
    iget v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 212
    iget v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    iget v1, p1, Ljavax/vecmath/Tuple3i;->z:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 213
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3i;Ljavax/vecmath/Tuple3i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 198
    iget v0, p1, Ljavax/vecmath/Tuple3i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->x:I

    .line 199
    iget v0, p1, Ljavax/vecmath/Tuple3i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->y:I

    .line 200
    iget v0, p1, Ljavax/vecmath/Tuple3i;->z:I

    iget v1, p2, Ljavax/vecmath/Tuple3i;->z:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple3i;->z:I

    .line 201
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3i;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3i;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple3i;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
