.class public Ljavax/vecmath/Matrix3f;
.super Ljava/lang/Object;
.source "Matrix3f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final EPS:D = 1.0E-8

.field static final serialVersionUID:J = 0x49351d1ffefaaeaL


# instance fields
.field public m00:F

.field public m01:F

.field public m02:F

.field public m10:F

.field public m11:F

.field public m12:F

.field public m20:F

.field public m21:F

.field public m22:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 199
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 200
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 201
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 203
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 204
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 205
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 207
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 208
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 209
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 211
    return-void
.end method

.method public constructor <init>(FFFFFFFFF)V
    .locals 0
    .param p1, "m00"    # F
    .param p2, "m01"    # F
    .param p3, "m02"    # F
    .param p4, "m10"    # F
    .param p5, "m11"    # F
    .param p6, "m12"    # F
    .param p7, "m20"    # F
    .param p8, "m21"    # F
    .param p9, "m22"    # F

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 116
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 117
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 119
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 120
    iput p5, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 121
    iput p6, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 123
    iput p7, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 124
    iput p8, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 125
    iput p9, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 127
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 156
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 157
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 158
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 159
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 161
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 162
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 163
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 165
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 166
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 167
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 169
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 180
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 181
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 183
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 184
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 185
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 187
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 188
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 189
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 191
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "v"    # [F

    .prologue
    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 137
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 138
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 140
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 141
    const/4 v0, 0x4

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 142
    const/4 v0, 0x5

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 144
    const/4 v0, 0x6

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 145
    const/4 v0, 0x7

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 146
    const/16 v0, 0x8

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 148
    return-void
.end method

.method private final invertGeneral(Ljavax/vecmath/Matrix3f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/16 v13, 0x8

    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1090
    const/16 v4, 0x9

    new-array v3, v4, [D

    .line 1091
    .local v3, "temp":[D
    const/16 v4, 0x9

    new-array v1, v4, [D

    .line 1092
    .local v1, "result":[D
    new-array v2, v11, [I

    .line 1099
    .local v2, "row_perm":[I
    iget v4, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v4, v4

    aput-wide v4, v3, v10

    .line 1100
    const/4 v4, 0x1

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1101
    const/4 v4, 0x2

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1103
    iget v4, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v4, v4

    aput-wide v4, v3, v11

    .line 1104
    iget v4, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v4, v4

    aput-wide v4, v3, v12

    .line 1105
    const/4 v4, 0x5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1107
    const/4 v4, 0x6

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1108
    const/4 v4, 0x7

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1109
    iget v4, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v4, v4

    aput-wide v4, v3, v13

    .line 1113
    invoke-static {v3, v2}, Ljavax/vecmath/Matrix3f;->luDecomposition([D[I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1115
    new-instance v4, Ljavax/vecmath/SingularMatrixException;

    const-string v5, "Matrix3f12"

    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1119
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x9

    if-ge v0, v4, :cond_1

    const-wide/16 v4, 0x0

    aput-wide v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1120
    :cond_1
    aput-wide v8, v1, v10

    aput-wide v8, v1, v12

    aput-wide v8, v1, v13

    .line 1121
    invoke-static {v3, v2, v1}, Ljavax/vecmath/Matrix3f;->luBacksubstitution([D[I[D)V

    .line 1123
    aget-wide v4, v1, v10

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1124
    const/4 v4, 0x1

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1125
    const/4 v4, 0x2

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1127
    aget-wide v4, v1, v11

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1128
    aget-wide v4, v1, v12

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1129
    const/4 v4, 0x5

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1131
    const/4 v4, 0x6

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1132
    const/4 v4, 0x7

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1133
    aget-wide v4, v1, v13

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1135
    return-void
.end method

.method static luBacksubstitution([D[I[D)V
    .locals 18
    .param p0, "matrix1"    # [D
    .param p1, "row_perm"    # [I
    .param p2, "matrix2"    # [D

    .prologue
    .line 1316
    const/4 v6, 0x0

    .line 1319
    .local v6, "rp":I
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_0
    const/4 v10, 0x3

    if-ge v5, v10, :cond_3

    .line 1321
    move v0, v5

    .line 1322
    .local v0, "cv":I
    const/4 v2, -0x1

    .line 1325
    .local v2, "ii":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v10, 0x3

    if-ge v1, v10, :cond_2

    .line 1328
    add-int v10, v6, v1

    aget v3, p1, v10

    .line 1329
    .local v3, "ip":I
    mul-int/lit8 v10, v3, 0x3

    add-int/2addr v10, v0

    aget-wide v8, p2, v10

    .line 1330
    .local v8, "sum":D
    mul-int/lit8 v10, v3, 0x3

    add-int/2addr v10, v0

    mul-int/lit8 v11, v1, 0x3

    add-int/2addr v11, v0

    aget-wide v12, p2, v11

    aput-wide v12, p2, v10

    .line 1331
    if-ltz v2, :cond_0

    .line 1333
    mul-int/lit8 v7, v1, 0x3

    .line 1334
    .local v7, "rv":I
    move v4, v2

    .local v4, "j":I
    :goto_2
    add-int/lit8 v10, v1, -0x1

    if-gt v4, v10, :cond_1

    .line 1335
    add-int v10, v7, v4

    aget-wide v10, p0, v10

    mul-int/lit8 v12, v4, 0x3

    add-int/2addr v12, v0

    aget-wide v12, p2, v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    .line 1334
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1338
    .end local v4    # "j":I
    .end local v7    # "rv":I
    :cond_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 1339
    move v2, v1

    .line 1341
    :cond_1
    mul-int/lit8 v10, v1, 0x3

    add-int/2addr v10, v0

    aput-wide v8, p2, v10

    .line 1325
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1346
    .end local v3    # "ip":I
    .end local v8    # "sum":D
    :cond_2
    const/4 v7, 0x6

    .line 1347
    .restart local v7    # "rv":I
    add-int/lit8 v10, v0, 0x6

    aget-wide v12, p2, v10

    const/16 v11, 0x8

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1349
    add-int/lit8 v7, v7, -0x3

    .line 1350
    add-int/lit8 v10, v0, 0x3

    add-int/lit8 v11, v0, 0x3

    aget-wide v12, p2, v11

    const/4 v11, 0x5

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x6

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x4

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1353
    add-int/lit8 v7, v7, -0x3

    .line 1354
    add-int/lit8 v10, v0, 0x0

    add-int/lit8 v11, v0, 0x0

    aget-wide v12, p2, v11

    const/4 v11, 0x1

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x3

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x2

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x6

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x0

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1319
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 1359
    .end local v0    # "cv":I
    .end local v1    # "i":I
    .end local v2    # "ii":I
    .end local v7    # "rv":I
    :cond_3
    return-void
.end method

.method static luDecomposition([D[I)Z
    .locals 30
    .param p0, "matrix0"    # [D
    .param p1, "row_perm"    # [I

    .prologue
    .line 1160
    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 1168
    .local v18, "row_scale":[D
    const/16 v16, 0x0

    .line 1169
    .local v16, "ptr":I
    const/16 v19, 0x0

    .line 1172
    .local v19, "rs":I
    const/4 v4, 0x3

    .local v4, "i":I
    move/from16 v20, v19

    .end local v19    # "rs":I
    .local v20, "rs":I
    move v5, v4

    .line 1173
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_2

    .line 1174
    const-wide/16 v2, 0x0

    .line 1177
    .local v2, "big":D
    const/4 v7, 0x3

    .local v7, "j":I
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .local v17, "ptr":I
    move v8, v7

    .line 1178
    .end local v7    # "j":I
    .local v8, "j":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "j":I
    .restart local v7    # "j":I
    if-eqz v8, :cond_0

    .line 1179
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    aget-wide v24, p0, v17

    .line 1180
    .local v24, "temp":D
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    .line 1181
    cmpl-double v26, v24, v2

    if-lez v26, :cond_e

    .line 1182
    move-wide/from16 v2, v24

    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto :goto_1

    .line 1187
    .end local v8    # "j":I
    .end local v24    # "temp":D
    .restart local v7    # "j":I
    :cond_0
    const-wide/16 v26, 0x0

    cmpl-double v26, v2, v26

    if-nez v26, :cond_1

    .line 1188
    const/16 v26, 0x0

    .line 1286
    .end local v2    # "big":D
    .end local v17    # "ptr":I
    :goto_2
    return v26

    .line 1190
    .restart local v2    # "big":D
    .restart local v17    # "ptr":I
    :cond_1
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "rs":I
    .restart local v19    # "rs":I
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v2

    aput-wide v26, v18, v20

    move/from16 v20, v19

    .end local v19    # "rs":I
    .restart local v20    # "rs":I
    move/from16 v16, v17

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 1198
    .end local v2    # "big":D
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v4    # "i":I
    :cond_2
    const/4 v11, 0x0

    .line 1201
    .local v11, "mtx":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v7, v0, :cond_d

    .line 1207
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_4

    .line 1208
    mul-int/lit8 v26, v4, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1209
    .local v21, "target":I
    aget-wide v22, p0, v21

    .line 1210
    .local v22, "sum":D
    move v9, v4

    .line 1211
    .local v9, "k":I
    mul-int/lit8 v26, v4, 0x3

    add-int v12, v11, v26

    .line 1212
    .local v12, "p1":I
    add-int v14, v11, v7

    .local v14, "p2":I
    move v10, v9

    .line 1213
    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_3

    .line 1214
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1215
    add-int/lit8 v12, v12, 0x1

    .line 1216
    add-int/lit8 v14, v14, 0x3

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 1218
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_3
    aput-wide v22, p0, v21

    .line 1207
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1223
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    :cond_4
    const-wide/16 v2, 0x0

    .line 1224
    .restart local v2    # "big":D
    const/4 v6, -0x1

    .line 1225
    .local v6, "imax":I
    move v4, v7

    :goto_6
    const/16 v26, 0x3

    move/from16 v0, v26

    if-ge v4, v0, :cond_7

    .line 1226
    mul-int/lit8 v26, v4, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1227
    .restart local v21    # "target":I
    aget-wide v22, p0, v21

    .line 1228
    .restart local v22    # "sum":D
    move v9, v7

    .line 1229
    .restart local v9    # "k":I
    mul-int/lit8 v26, v4, 0x3

    add-int v12, v11, v26

    .line 1230
    .restart local v12    # "p1":I
    add-int v14, v11, v7

    .restart local v14    # "p2":I
    move v10, v9

    .line 1231
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_7
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_5

    .line 1232
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1233
    add-int/lit8 v12, v12, 0x1

    .line 1234
    add-int/lit8 v14, v14, 0x3

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_7

    .line 1236
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_5
    aput-wide v22, p0, v21

    .line 1239
    aget-wide v26, v18, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    mul-double v24, v26, v28

    .restart local v24    # "temp":D
    cmpl-double v26, v24, v2

    if-ltz v26, :cond_6

    .line 1240
    move-wide/from16 v2, v24

    .line 1241
    move v6, v4

    .line 1225
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1245
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    .end local v24    # "temp":D
    :cond_7
    if-gez v6, :cond_8

    .line 1246
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "Matrix3f13"

    invoke-static/range {v27 .. v27}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 1250
    :cond_8
    if-eq v7, v6, :cond_a

    .line 1252
    const/4 v9, 0x3

    .line 1253
    .restart local v9    # "k":I
    mul-int/lit8 v26, v6, 0x3

    add-int v12, v11, v26

    .line 1254
    .restart local v12    # "p1":I
    mul-int/lit8 v26, v7, 0x3

    add-int v14, v11, v26

    .restart local v14    # "p2":I
    move v15, v14

    .end local v14    # "p2":I
    .local v15, "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .local v13, "p1":I
    move v10, v9

    .line 1255
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_8
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_9

    .line 1256
    aget-wide v24, p0, v13

    .line 1257
    .restart local v24    # "temp":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "p1":I
    .restart local v12    # "p1":I
    aget-wide v26, p0, v15

    aput-wide v26, p0, v13

    .line 1258
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "p2":I
    .restart local v14    # "p2":I
    aput-wide v24, p0, v15

    move v15, v14

    .end local v14    # "p2":I
    .restart local v15    # "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .restart local v13    # "p1":I
    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_8

    .line 1262
    .end local v10    # "k":I
    .end local v24    # "temp":D
    .restart local v9    # "k":I
    :cond_9
    aget-wide v26, v18, v7

    aput-wide v26, v18, v6

    .line 1266
    .end local v9    # "k":I
    .end local v13    # "p1":I
    .end local v15    # "p2":I
    :cond_a
    aput v6, p1, v7

    .line 1269
    mul-int/lit8 v26, v7, 0x3

    add-int v26, v26, v11

    add-int v26, v26, v7

    aget-wide v26, p0, v26

    const-wide/16 v28, 0x0

    cmpl-double v26, v26, v28

    if-nez v26, :cond_b

    .line 1270
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 1274
    :cond_b
    const/16 v26, 0x2

    move/from16 v0, v26

    if-eq v7, v0, :cond_c

    .line 1275
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-int/lit8 v28, v7, 0x3

    add-int v28, v28, v11

    add-int v28, v28, v7

    aget-wide v28, p0, v28

    div-double v24, v26, v28

    .line 1276
    .restart local v24    # "temp":D
    add-int/lit8 v26, v7, 0x1

    mul-int/lit8 v26, v26, 0x3

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1277
    .restart local v21    # "target":I
    rsub-int/lit8 v4, v7, 0x2

    move v5, v4

    .line 1278
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :goto_9
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_c

    .line 1279
    aget-wide v26, p0, v21

    mul-double v26, v26, v24

    aput-wide v26, p0, v21

    .line 1280
    add-int/lit8 v21, v21, 0x3

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_9

    .line 1201
    .end local v5    # "i":I
    .end local v21    # "target":I
    .end local v24    # "temp":D
    .restart local v4    # "i":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 1286
    .end local v2    # "big":D
    .end local v6    # "imax":I
    :cond_d
    const/16 v26, 0x1

    goto/16 :goto_2

    .end local v11    # "mtx":I
    .restart local v2    # "big":D
    .restart local v24    # "temp":D
    :cond_e
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto/16 :goto_1
.end method


# virtual methods
.method public final add(F)V
    .locals 1
    .param p1, "scalar"    # F

    .prologue
    .line 705
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 706
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 707
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 708
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 709
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 710
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 711
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 712
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 713
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 714
    return-void
.end method

.method public final add(FLjavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "scalar"    # F
    .param p2, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 724
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m00:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 725
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m01:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 726
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m02:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 727
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m10:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 728
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 729
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m12:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 730
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m20:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 731
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m21:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 732
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 733
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 762
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m00:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 763
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m01:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 764
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m02:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 766
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m10:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 767
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 768
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m12:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 770
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m20:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 771
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m21:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 772
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 773
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 742
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m00:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 743
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m01:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 744
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m02:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 746
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m10:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 747
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 748
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m12:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 750
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m20:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 751
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m21:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 752
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 753
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2105
    const/4 v1, 0x0

    .line 2107
    .local v1, "m1":Ljavax/vecmath/Matrix3f;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m1":Ljavax/vecmath/Matrix3f;
    check-cast v1, Ljavax/vecmath/Matrix3f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2112
    .restart local v1    # "m1":Ljavax/vecmath/Matrix3f;
    return-object v1

    .line 2108
    .end local v1    # "m1":Ljavax/vecmath/Matrix3f;
    :catch_0
    move-exception v0

    .line 2110
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/InternalError;

    invoke-direct {v2}, Ljava/lang/InternalError;-><init>()V

    throw v2
.end method

.method public final determinant()F
    .locals 6

    .prologue
    .line 1367
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 1370
    .local v0, "total":F
    return v0
.end method

.method public epsilonEquals(Ljavax/vecmath/Matrix3f;F)Z
    .locals 3
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "epsilon"    # F

    .prologue
    .line 1946
    const/4 v0, 0x1

    .line 1948
    .local v0, "status":Z
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    .line 1949
    :cond_0
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_1

    const/4 v0, 0x0

    .line 1950
    :cond_1
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_2

    const/4 v0, 0x0

    .line 1952
    :cond_2
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_3

    const/4 v0, 0x0

    .line 1953
    :cond_3
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_4

    const/4 v0, 0x0

    .line 1954
    :cond_4
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_5

    const/4 v0, 0x0

    .line 1956
    :cond_5
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_6

    const/4 v0, 0x0

    .line 1957
    :cond_6
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_7

    const/4 v0, 0x0

    .line 1958
    :cond_7
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_8

    const/4 v0, 0x0

    .line 1960
    :cond_8
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "o1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 1926
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Matrix3f;

    move-object v3, v0

    .line 1927
    .local v3, "m2":Ljavax/vecmath/Matrix3f;
    iget v5, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m00:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m01:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m02:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m10:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m11:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m12:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m20:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m21:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, v3, Ljavax/vecmath/Matrix3f;->m22:F
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 1932
    .end local v3    # "m2":Ljavax/vecmath/Matrix3f;
    :cond_0
    :goto_0
    return v4

    .line 1931
    :catch_0
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 1932
    .end local v1    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Matrix3f;)Z
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v1, 0x0

    .line 1907
    :try_start_0
    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m01:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m02:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m11:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m12:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m20:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m21:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m22:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 1911
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final getColumn(ILjavax/vecmath/Vector3f;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 391
    if-nez p1, :cond_0

    .line 392
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 393
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 394
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    .line 407
    :goto_0
    return-void

    .line 395
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 396
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 397
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 398
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    goto :goto_0

    .line 399
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 400
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 401
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 402
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    goto :goto_0

    .line 404
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f3"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getColumn(I[F)V
    .locals 4
    .param p1, "column"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 416
    if-nez p1, :cond_0

    .line 417
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    aput v0, p2, v3

    .line 418
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    aput v0, p2, v1

    .line 419
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    aput v0, p2, v2

    .line 431
    :goto_0
    return-void

    .line 420
    :cond_0
    if-ne p1, v1, :cond_1

    .line 421
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    aput v0, p2, v3

    .line 422
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    aput v0, p2, v1

    .line 423
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    aput v0, p2, v2

    goto :goto_0

    .line 424
    :cond_1
    if-ne p1, v2, :cond_2

    .line 425
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    aput v0, p2, v3

    .line 426
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    aput v0, p2, v1

    .line 427
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    aput v0, p2, v2

    goto :goto_0

    .line 429
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f3"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getElement(II)F
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 442
    packed-switch p1, :pswitch_data_0

    .line 488
    :goto_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f5"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 445
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 448
    :pswitch_1
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 479
    :goto_1
    return v0

    .line 450
    :pswitch_2
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    goto :goto_1

    .line 452
    :pswitch_3
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    goto :goto_1

    .line 458
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 461
    :pswitch_5
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    goto :goto_1

    .line 463
    :pswitch_6
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    goto :goto_1

    .line 465
    :pswitch_7
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    goto :goto_1

    .line 472
    :pswitch_8
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 475
    :pswitch_9
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    goto :goto_1

    .line 477
    :pswitch_a
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    goto :goto_1

    .line 479
    :pswitch_b
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_1

    .line 442
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch

    .line 445
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 458
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 472
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final getM00()F
    .locals 1

    .prologue
    .line 2124
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    return v0
.end method

.method public final getM01()F
    .locals 1

    .prologue
    .line 2147
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    return v0
.end method

.method public final getM02()F
    .locals 1

    .prologue
    .line 2169
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    return v0
.end method

.method public final getM10()F
    .locals 1

    .prologue
    .line 2191
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    return v0
.end method

.method public final getM11()F
    .locals 1

    .prologue
    .line 2213
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    return v0
.end method

.method public final getM12()F
    .locals 1

    .prologue
    .line 2235
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    return v0
.end method

.method public final getM20()F
    .locals 1

    .prologue
    .line 2255
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    return v0
.end method

.method public final getM21()F
    .locals 1

    .prologue
    .line 2277
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    return v0
.end method

.method public final getM22()F
    .locals 1

    .prologue
    .line 2299
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    return v0
.end method

.method public final getRow(ILjavax/vecmath/Vector3f;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 342
    if-nez p1, :cond_0

    .line 343
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 344
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 345
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    .line 358
    :goto_0
    return-void

    .line 346
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 347
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 348
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 349
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    goto :goto_0

    .line 350
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 351
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 352
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 353
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p2, Ljavax/vecmath/Vector3f;->z:F

    goto :goto_0

    .line 355
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f1"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRow(I[F)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 366
    if-nez p1, :cond_0

    .line 367
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    aput v0, p2, v3

    .line 368
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    aput v0, p2, v1

    .line 369
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    aput v0, p2, v2

    .line 382
    :goto_0
    return-void

    .line 370
    :cond_0
    if-ne p1, v1, :cond_1

    .line 371
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    aput v0, p2, v3

    .line 372
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    aput v0, p2, v1

    .line 373
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    aput v0, p2, v2

    goto :goto_0

    .line 374
    :cond_1
    if-ne p1, v2, :cond_2

    .line 375
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    aput v0, p2, v3

    .line 376
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    aput v0, p2, v1

    .line 377
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    aput v0, p2, v2

    goto :goto_0

    .line 379
    :cond_2
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f1"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getScale()F
    .locals 4

    .prologue
    .line 691
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 692
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 693
    .local v1, "tmp_scale":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3f;->getScaleRotate([D[D)V

    .line 695
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method getScaleRotate([D[D)V
    .locals 4
    .param p1, "scales"    # [D
    .param p2, "rot"    # [D

    .prologue
    .line 2080
    const/16 v1, 0x9

    new-array v0, v1, [D

    .line 2081
    .local v0, "tmp":[D
    const/4 v1, 0x0

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2082
    const/4 v1, 0x1

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2083
    const/4 v1, 0x2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2084
    const/4 v1, 0x3

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2085
    const/4 v1, 0x4

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2086
    const/4 v1, 0x5

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2087
    const/4 v1, 0x6

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2088
    const/4 v1, 0x7

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2089
    const/16 v1, 0x8

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 2090
    invoke-static {v0, p1, p2}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 2092
    return-void
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 1974
    const-wide/16 v0, 0x1

    .line 1975
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m00:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1976
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1977
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1978
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1979
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1980
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1981
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1982
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1983
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m22:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 1984
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final invert()V
    .locals 0

    .prologue
    .line 1078
    invoke-direct {p0, p0}, Ljavax/vecmath/Matrix3f;->invertGeneral(Ljavax/vecmath/Matrix3f;)V

    .line 1079
    return-void
.end method

.method public final invert(Ljavax/vecmath/Matrix3f;)V
    .locals 0
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1070
    invoke-direct {p0, p1}, Ljavax/vecmath/Matrix3f;->invertGeneral(Ljavax/vecmath/Matrix3f;)V

    .line 1071
    return-void
.end method

.method public final mul(F)V
    .locals 1
    .param p1, "scalar"    # F

    .prologue
    .line 1474
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1475
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1476
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1478
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1479
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1480
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1482
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1483
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1484
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1485
    return-void
.end method

.method public final mul(FLjavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "scalar"    # F
    .param p2, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1495
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1496
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1497
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1499
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1500
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1501
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1503
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1504
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1505
    iget v0, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1507
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1520
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 1521
    .local v0, "m00":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v1, v9, v10

    .line 1522
    .local v1, "m01":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 1524
    .local v2, "m02":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v3, v9, v10

    .line 1525
    .local v3, "m10":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v4, v9, v10

    .line 1526
    .local v4, "m11":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v5, v9, v10

    .line 1528
    .local v5, "m12":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v6, v9, v10

    .line 1529
    .local v6, "m20":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v7, v9, v10

    .line 1530
    .local v7, "m21":F
    iget v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v8, v9, v10

    .line 1532
    .local v8, "m22":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1533
    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1534
    iput v6, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v7, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v8, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1535
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1545
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 1546
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1547
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1548
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1550
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1551
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1552
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1554
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1555
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1556
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1578
    :goto_0
    return-void

    .line 1562
    :cond_0
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 1563
    .local v0, "m00":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v1, v9, v10

    .line 1564
    .local v1, "m01":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 1566
    .local v2, "m02":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v3, v9, v10

    .line 1567
    .local v3, "m10":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v4, v9, v10

    .line 1568
    .local v4, "m11":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v5, v9, v10

    .line 1570
    .local v5, "m12":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v6, v9, v10

    .line 1571
    .local v6, "m20":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v7, v9, v10

    .line 1572
    .local v7, "m21":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v8, v9, v10

    .line 1574
    .local v8, "m22":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1575
    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1576
    iput v6, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v7, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v8, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto/16 :goto_0
.end method

.method public final mulNormalize(Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x3

    .line 1588
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1589
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1590
    .local v1, "tmp_rot":[D
    new-array v2, v7, [D

    .line 1592
    .local v2, "tmp_scale":[D
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v8

    .line 1593
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v9

    .line 1594
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v10

    .line 1596
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v7

    .line 1597
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v11

    .line 1598
    const/4 v3, 0x5

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1600
    const/4 v3, 0x6

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1601
    const/4 v3, 0x7

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1602
    const/16 v3, 0x8

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1604
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1606
    aget-wide v4, v1, v8

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1607
    aget-wide v4, v1, v9

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1608
    aget-wide v4, v1, v10

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1610
    aget-wide v4, v1, v7

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1611
    aget-wide v4, v1, v11

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1612
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1614
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1615
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1616
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1618
    return-void
.end method

.method public final mulNormalize(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x3

    .line 1629
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1630
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1631
    .local v1, "tmp_rot":[D
    new-array v2, v7, [D

    .line 1634
    .local v2, "tmp_scale":[D
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v8

    .line 1635
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v9

    .line 1636
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v10

    .line 1638
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v7

    .line 1639
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-double v4, v3

    aput-wide v4, v0, v11

    .line 1640
    const/4 v3, 0x5

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1642
    const/4 v3, 0x6

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1643
    const/4 v3, 0x7

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1644
    const/16 v3, 0x8

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v5, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v6, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1646
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1648
    aget-wide v4, v1, v8

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1649
    aget-wide v4, v1, v9

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1650
    aget-wide v4, v1, v10

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1652
    aget-wide v4, v1, v7

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1653
    aget-wide v4, v1, v11

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1654
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1656
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1657
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1658
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1659
    return-void
.end method

.method public final mulTransposeBoth(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1669
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 1670
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1671
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1672
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1674
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1675
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1676
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1678
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1679
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1680
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1703
    :goto_0
    return-void

    .line 1686
    :cond_0
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 1687
    .local v0, "m00":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v1, v9, v10

    .line 1688
    .local v1, "m01":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 1690
    .local v2, "m02":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v3, v9, v10

    .line 1691
    .local v3, "m10":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v4, v9, v10

    .line 1692
    .local v4, "m11":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v5, v9, v10

    .line 1694
    .local v5, "m12":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v6, v9, v10

    .line 1695
    .local v6, "m20":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v7, v9, v10

    .line 1696
    .local v7, "m21":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v8, v9, v10

    .line 1698
    .local v8, "m22":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1699
    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1700
    iput v6, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v7, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v8, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto/16 :goto_0
.end method

.method public final mulTransposeLeft(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1757
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 1758
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1759
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1760
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1762
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1763
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1764
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1766
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1767
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1768
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1790
    :goto_0
    return-void

    .line 1774
    :cond_0
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 1775
    .local v0, "m00":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v1, v9, v10

    .line 1776
    .local v1, "m01":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 1778
    .local v2, "m02":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v3, v9, v10

    .line 1779
    .local v3, "m10":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v4, v9, v10

    .line 1780
    .local v4, "m11":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v5, v9, v10

    .line 1782
    .local v5, "m12":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v10, v11

    add-float v6, v9, v10

    .line 1783
    .local v6, "m20":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float v7, v9, v10

    .line 1784
    .local v7, "m21":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v8, v9, v10

    .line 1786
    .local v8, "m22":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1787
    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1788
    iput v6, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v7, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v8, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto/16 :goto_0
.end method

.method public final mulTransposeRight(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1714
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 1715
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1716
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1717
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1719
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1720
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1721
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1723
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1724
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1725
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iput v9, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1747
    :goto_0
    return-void

    .line 1731
    :cond_0
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v0, v9, v10

    .line 1732
    .local v0, "m00":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v1, v9, v10

    .line 1733
    .local v1, "m01":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 1735
    .local v2, "m02":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v3, v9, v10

    .line 1736
    .local v3, "m10":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v4, v9, v10

    .line 1737
    .local v4, "m11":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v5, v9, v10

    .line 1739
    .local v5, "m12":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v10, v11

    add-float v6, v9, v10

    .line 1740
    .local v6, "m20":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v10, v11

    add-float v7, v9, v10

    .line 1741
    .local v7, "m21":F
    iget v9, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v10, p2, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v10, v11

    add-float/2addr v9, v10

    iget v10, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v11, p2, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v10, v11

    add-float v8, v9, v10

    .line 1743
    .local v8, "m22":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1744
    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iput v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iput v5, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1745
    iput v6, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iput v7, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iput v8, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto/16 :goto_0
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 2012
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 2013
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 2014
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 2016
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 2017
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 2018
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 2020
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 2021
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 2022
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 2024
    return-void
.end method

.method public final negate(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 2033
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 2034
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 2035
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 2037
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 2038
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 2039
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 2041
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 2042
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 2043
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 2045
    return-void
.end method

.method public final normalize()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 1797
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 1798
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 1799
    .local v1, "tmp_scale":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3f;->getScaleRotate([D[D)V

    .line 1801
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1802
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1803
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1805
    aget-wide v2, v0, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1806
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1807
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1809
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1810
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1811
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1813
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Matrix3f;)V
    .locals 11
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 1821
    const/16 v3, 0x9

    new-array v0, v3, [D

    .line 1822
    .local v0, "tmp":[D
    const/16 v3, 0x9

    new-array v1, v3, [D

    .line 1823
    .local v1, "tmp_rot":[D
    new-array v2, v6, [D

    .line 1825
    .local v2, "tmp_scale":[D
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v4, v3

    aput-wide v4, v0, v7

    .line 1826
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v4, v3

    aput-wide v4, v0, v8

    .line 1827
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v4, v3

    aput-wide v4, v0, v9

    .line 1829
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v4, v3

    aput-wide v4, v0, v6

    .line 1830
    iget v3, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v4, v3

    aput-wide v4, v0, v10

    .line 1831
    const/4 v3, 0x5

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1833
    const/4 v3, 0x6

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1834
    const/4 v3, 0x7

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1835
    const/16 v3, 0x8

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v4, v4

    aput-wide v4, v0, v3

    .line 1837
    invoke-static {v0, v2, v1}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 1839
    aget-wide v4, v1, v7

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1840
    aget-wide v4, v1, v8

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1841
    aget-wide v4, v1, v9

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1843
    aget-wide v4, v1, v6

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1844
    aget-wide v4, v1, v10

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1845
    const/4 v3, 0x5

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1847
    const/4 v3, 0x6

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1848
    const/4 v3, 0x7

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1849
    const/16 v3, 0x8

    aget-wide v4, v1, v3

    double-to-float v3, v4

    iput v3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1851
    return-void
.end method

.method public final normalizeCP()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1858
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    div-float v0, v4, v1

    .line 1859
    .local v0, "mag":F
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1860
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1861
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1863
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    div-float v0, v4, v1

    .line 1864
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1865
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1866
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1868
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1869
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1870
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1872
    return-void
.end method

.method public final normalizeCP(Ljavax/vecmath/Matrix3f;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 1881
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    div-float v0, v4, v1

    .line 1882
    .local v0, "mag":F
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1883
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1884
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1886
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    div-float v0, v4, v1

    .line 1887
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1888
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1889
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1891
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1892
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1893
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1895
    return-void
.end method

.method public final rotX(F)V
    .locals 5
    .param p1, "angle"    # F

    .prologue
    const/4 v4, 0x0

    .line 1402
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1403
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 1405
    .local v0, "cosAngle":F
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1406
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1407
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1409
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1410
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1411
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1413
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1414
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1415
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1416
    return-void
.end method

.method public final rotY(F)V
    .locals 5
    .param p1, "angle"    # F

    .prologue
    const/4 v4, 0x0

    .line 1427
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1428
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 1430
    .local v0, "cosAngle":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1431
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1432
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1434
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1435
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1436
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1438
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1439
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1440
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1441
    return-void
.end method

.method public final rotZ(F)V
    .locals 5
    .param p1, "angle"    # F

    .prologue
    const/4 v4, 0x0

    .line 1452
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 1453
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 1455
    .local v0, "cosAngle":F
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1456
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1457
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1459
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1460
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1461
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1463
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1464
    iput v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1465
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1466
    return-void
.end method

.method public final set(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    const/4 v0, 0x0

    .line 1380
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1381
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1382
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1384
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1385
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1386
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1388
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1389
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1390
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1391
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 28
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 934
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v24, v0

    mul-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v26, v0

    mul-double v24, v24, v26

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    .line 935
    .local v10, "mag":D
    const-wide v22, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v22, v10, v22

    if-gez v22, :cond_0

    .line 936
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 937
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 938
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 940
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 941
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 942
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 944
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 945
    const/16 v22, 0x0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 946
    const/high16 v22, 0x3f800000    # 1.0f

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 974
    :goto_0
    return-void

    .line 948
    :cond_0
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    div-double v10, v22, v10

    .line 949
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v22, v0

    mul-double v2, v22, v10

    .line 950
    .local v2, "ax":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v22, v0

    mul-double v4, v22, v10

    .line 951
    .local v4, "ay":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v22, v0

    mul-double v6, v22, v10

    .line 953
    .local v6, "az":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    .line 954
    .local v12, "sinTheta":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v22, v0

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->cos(D)D

    move-result-wide v8

    .line 955
    .local v8, "cosTheta":D
    const-wide/high16 v22, 0x3ff0000000000000L    # 1.0

    sub-double v14, v22, v8

    .line 957
    .local v14, "t":D
    mul-double v18, v2, v6

    .line 958
    .local v18, "xz":D
    mul-double v16, v2, v4

    .line 959
    .local v16, "xy":D
    mul-double v20, v4, v6

    .line 961
    .local v20, "yz":D
    mul-double v22, v14, v2

    mul-double v22, v22, v2

    add-double v22, v22, v8

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 962
    mul-double v22, v14, v16

    mul-double v24, v12, v6

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 963
    mul-double v22, v14, v18

    mul-double v24, v12, v4

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 965
    mul-double v22, v14, v16

    mul-double v24, v12, v6

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 966
    mul-double v22, v14, v4

    mul-double v22, v22, v4

    add-double v22, v22, v8

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 967
    mul-double v22, v14, v20

    mul-double v24, v12, v2

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 969
    mul-double v22, v14, v18

    mul-double v24, v12, v4

    sub-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 970
    mul-double v22, v14, v20

    mul-double v24, v12, v2

    add-double v22, v22, v24

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 971
    mul-double v22, v14, v6

    mul-double v22, v22, v6

    add-double v22, v22, v8

    move-wide/from16 v0, v22

    double-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix3f;->m22:F

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 14
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 885
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v12, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v12, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    .line 886
    .local v4, "mag":F
    float-to-double v10, v4

    const-wide v12, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v10, v10, v12

    if-gez v10, :cond_0

    .line 887
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 888
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 889
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 891
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 892
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 893
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 895
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 896
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 897
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 925
    :goto_0
    return-void

    .line 899
    :cond_0
    const/high16 v10, 0x3f800000    # 1.0f

    div-float v4, v10, v4

    .line 900
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float v0, v10, v4

    .line 901
    .local v0, "ax":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float v1, v10, v4

    .line 902
    .local v1, "ay":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float v2, v10, v4

    .line 904
    .local v2, "az":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v5, v10

    .line 905
    .local v5, "sinTheta":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v3, v10

    .line 906
    .local v3, "cosTheta":F
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float v6, v10, v3

    .line 908
    .local v6, "t":F
    mul-float v8, v0, v2

    .line 909
    .local v8, "xz":F
    mul-float v7, v0, v1

    .line 910
    .local v7, "xy":F
    mul-float v9, v1, v2

    .line 912
    .local v9, "yz":F
    mul-float v10, v6, v0

    mul-float/2addr v10, v0

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 913
    mul-float v10, v6, v7

    mul-float v11, v5, v2

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 914
    mul-float v10, v6, v8

    mul-float v11, v5, v1

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 916
    mul-float v10, v6, v7

    mul-float v11, v5, v2

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 917
    mul-float v10, v6, v1

    mul-float/2addr v10, v1

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 918
    mul-float v10, v6, v9

    mul-float v11, v5, v0

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 920
    mul-float v10, v6, v8

    mul-float v11, v5, v1

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 921
    mul-float v10, v6, v9

    mul-float v11, v5, v0

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 922
    mul-float v10, v6, v2

    mul-float/2addr v10, v2

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 1048
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1049
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1050
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1052
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1053
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1054
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1056
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1057
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1058
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1060
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 1026
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1027
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1028
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1030
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1031
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1032
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1034
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1035
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1036
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1038
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 983
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 984
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 985
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 987
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 988
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 989
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 991
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 992
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 993
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 994
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 5
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x40000000    # 2.0f

    .line 865
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 866
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 867
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 869
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 870
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 871
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 873
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 874
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 875
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 876
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "m"    # [F

    .prologue
    .line 1004
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1005
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1006
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1008
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1009
    const/4 v0, 0x4

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1010
    const/4 v0, 0x5

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 1012
    const/4 v0, 0x6

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 1013
    const/4 v0, 0x7

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 1014
    const/16 v0, 0x8

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 1017
    return-void
.end method

.method public final setColumn(IFFF)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 595
    packed-switch p1, :pswitch_data_0

    .line 615
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 597
    :pswitch_0
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 598
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 599
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 617
    :goto_0
    return-void

    .line 603
    :pswitch_1
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 604
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 605
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    goto :goto_0

    .line 609
    :pswitch_2
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 610
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 611
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setColumn(ILjavax/vecmath/Vector3f;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 626
    packed-switch p1, :pswitch_data_0

    .line 646
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 628
    :pswitch_0
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 629
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 630
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 648
    :goto_0
    return-void

    .line 634
    :pswitch_1
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 635
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 636
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    goto :goto_0

    .line 640
    :pswitch_2
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 641
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 642
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 626
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setColumn(I[F)V
    .locals 3
    .param p1, "column"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 657
    packed-switch p1, :pswitch_data_0

    .line 677
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 659
    :pswitch_0
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 660
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 661
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 679
    :goto_0
    return-void

    .line 665
    :pswitch_1
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 666
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 667
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    goto :goto_0

    .line 671
    :pswitch_2
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 672
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 673
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 657
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setElement(IIF)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "value"    # F

    .prologue
    .line 277
    packed-switch p1, :pswitch_data_0

    .line 332
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 292
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :pswitch_1
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 334
    :goto_0
    return-void

    .line 286
    :pswitch_2
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    goto :goto_0

    .line 289
    :pswitch_3
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    goto :goto_0

    .line 297
    :pswitch_4
    packed-switch p2, :pswitch_data_2

    .line 309
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 300
    :pswitch_5
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    goto :goto_0

    .line 303
    :pswitch_6
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    goto :goto_0

    .line 306
    :pswitch_7
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    goto :goto_0

    .line 314
    :pswitch_8
    packed-switch p2, :pswitch_data_3

    .line 327
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :pswitch_9
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    goto :goto_0

    .line 320
    :pswitch_a
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    goto :goto_0

    .line 323
    :pswitch_b
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 277
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_8
    .end packed-switch

    .line 280
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 297
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    .line 314
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final setIdentity()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 229
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 230
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 231
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 233
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 234
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 235
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 237
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 238
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 239
    iput v1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 240
    return-void
.end method

.method public final setM00(F)V
    .locals 0
    .param p1, "m00"    # F

    .prologue
    .line 2135
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 2136
    return-void
.end method

.method public final setM01(F)V
    .locals 0
    .param p1, "m01"    # F

    .prologue
    .line 2158
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 2159
    return-void
.end method

.method public final setM02(F)V
    .locals 0
    .param p1, "m02"    # F

    .prologue
    .line 2180
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 2181
    return-void
.end method

.method public final setM10(F)V
    .locals 0
    .param p1, "m10"    # F

    .prologue
    .line 2202
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 2203
    return-void
.end method

.method public final setM11(F)V
    .locals 0
    .param p1, "m11"    # F

    .prologue
    .line 2224
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 2225
    return-void
.end method

.method public final setM12(F)V
    .locals 0
    .param p1, "m12"    # F

    .prologue
    .line 2244
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 2245
    return-void
.end method

.method public final setM20(F)V
    .locals 0
    .param p1, "m20"    # F

    .prologue
    .line 2266
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 2267
    return-void
.end method

.method public final setM21(F)V
    .locals 0
    .param p1, "m21"    # F

    .prologue
    .line 2288
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 2289
    return-void
.end method

.method public final setM22(F)V
    .locals 0
    .param p1, "m22"    # F

    .prologue
    .line 2310
    iput p1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 2311
    return-void
.end method

.method public final setRow(IFFF)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F

    .prologue
    .line 500
    packed-switch p1, :pswitch_data_0

    .line 520
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 502
    :pswitch_0
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 503
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 504
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 522
    :goto_0
    return-void

    .line 508
    :pswitch_1
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 509
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 510
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    goto :goto_0

    .line 514
    :pswitch_2
    iput p2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 515
    iput p3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 516
    iput p4, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 500
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setRow(ILjavax/vecmath/Vector3f;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 531
    packed-switch p1, :pswitch_data_0

    .line 551
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 533
    :pswitch_0
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 534
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 535
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 553
    :goto_0
    return-void

    .line 539
    :pswitch_1
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 540
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 541
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    goto :goto_0

    .line 545
    :pswitch_2
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 546
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 547
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 531
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setRow(I[F)V
    .locals 3
    .param p1, "row"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 562
    packed-switch p1, :pswitch_data_0

    .line 582
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix3f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 564
    :pswitch_0
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 565
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 566
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 584
    :goto_0
    return-void

    .line 570
    :pswitch_1
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 571
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 572
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    goto :goto_0

    .line 576
    :pswitch_2
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 577
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 578
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0

    .line 562
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setScale(F)V
    .locals 7
    .param p1, "scale"    # F

    .prologue
    const/4 v6, 0x3

    .line 250
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 251
    .local v0, "tmp_rot":[D
    new-array v1, v6, [D

    .line 253
    .local v1, "tmp_scale":[D
    invoke-virtual {p0, v1, v0}, Ljavax/vecmath/Matrix3f;->getScaleRotate([D[D)V

    .line 255
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 256
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 257
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 259
    aget-wide v2, v0, v6

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 260
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 261
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 263
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 264
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 265
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 267
    return-void
.end method

.method public final setZero()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1993
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 1994
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 1995
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 1997
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 1998
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 1999
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 2001
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 2002
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 2003
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 2005
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 803
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m00:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 804
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 805
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m02:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 807
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m10:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 808
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m11:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 809
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 811
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 812
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m21:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 813
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v1, p1, Ljavax/vecmath/Matrix3f;->m22:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 814
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 783
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m00:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 784
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 785
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m02:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 787
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m10:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 788
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m11:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 789
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 791
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 792
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m21:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 793
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iget v1, p2, Ljavax/vecmath/Matrix3f;->m22:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 794
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m00:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m11:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix3f;->m22:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljavax/vecmath/Tuple3f;)V
    .locals 6
    .param p1, "t"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 2054
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v4, v5

    add-float v0, v3, v4

    .line 2055
    .local v0, "x":F
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v4, v5

    add-float v1, v3, v4

    .line 2056
    .local v1, "y":F
    iget v3, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v5, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 2057
    .local v2, "z":F
    invoke-virtual {p1, v0, v1, v2}, Ljavax/vecmath/Tuple3f;->set(FFF)V

    .line 2058
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple3f;Ljavax/vecmath/Tuple3f;)V
    .locals 5
    .param p1, "t"    # Ljavax/vecmath/Tuple3f;
    .param p2, "result"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 2068
    iget v2, p0, Ljavax/vecmath/Matrix3f;->m00:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v3, v4

    add-float v0, v2, v3

    .line 2069
    .local v0, "x":F
    iget v2, p0, Ljavax/vecmath/Matrix3f;->m10:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m11:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v3, v4

    add-float v1, v2, v3

    .line 2070
    .local v1, "y":F
    iget v2, p0, Ljavax/vecmath/Matrix3f;->m20:F

    iget v3, p1, Ljavax/vecmath/Tuple3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m21:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix3f;->m22:F

    iget v4, p1, Ljavax/vecmath/Tuple3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p2, Ljavax/vecmath/Tuple3f;->z:F

    .line 2071
    iput v0, p2, Ljavax/vecmath/Tuple3f;->x:F

    .line 2072
    iput v1, p2, Ljavax/vecmath/Tuple3f;->y:F

    .line 2073
    return-void
.end method

.method public final transpose()V
    .locals 2

    .prologue
    .line 823
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 824
    .local v0, "temp":F
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m01:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 825
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 827
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 828
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m02:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 829
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 831
    iget v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 832
    iget v1, p0, Ljavax/vecmath/Matrix3f;->m12:F

    iput v1, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 833
    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 834
    return-void
.end method

.method public final transpose(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 842
    if-eq p0, p1, :cond_0

    .line 843
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m00:F

    .line 844
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m01:F

    .line 845
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m02:F

    .line 847
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m10:F

    .line 848
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m11:F

    .line 849
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m12:F

    .line 851
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m20:F

    .line 852
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m21:F

    .line 853
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix3f;->m22:F

    .line 856
    :goto_0
    return-void

    .line 855
    :cond_0
    invoke-virtual {p0}, Ljavax/vecmath/Matrix3f;->transpose()V

    goto :goto_0
.end method
