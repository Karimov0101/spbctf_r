.class public Ljavax/vecmath/Color3f;
.super Ljavax/vecmath/Tuple3f;
.source "Color3f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x19d66ada4e8c089bL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljavax/vecmath/Tuple3f;-><init>()V

    .line 122
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3f;-><init>(FFF)V

    .line 60
    return-void
.end method

.method public constructor <init>(Ljava/awt/Color;)V
    .locals 4
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    .line 111
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    .line 112
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    .line 113
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    .line 111
    invoke-direct {p0, v0, v1, v2}, Ljavax/vecmath/Tuple3f;-><init>(FFF)V

    .line 114
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Color3f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Color3f;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 78
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 87
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 68
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>([F)V

    .line 69
    return-void
.end method


# virtual methods
.method public final get()Ljava/awt/Color;
    .locals 5

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 151
    iget v3, p0, Ljavax/vecmath/Color3f;->x:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 152
    .local v2, "r":I
    iget v3, p0, Ljavax/vecmath/Color3f;->y:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 153
    .local v1, "g":I
    iget v3, p0, Ljavax/vecmath/Color3f;->z:F

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 155
    .local v0, "b":I
    new-instance v3, Ljava/awt/Color;

    invoke-direct {v3, v2, v1, v0}, Ljava/awt/Color;-><init>(III)V

    return-object v3
.end method

.method public final set(Ljava/awt/Color;)V
    .locals 2
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 136
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color3f;->x:F

    .line 137
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color3f;->y:F

    .line 138
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color3f;->z:F

    .line 139
    return-void
.end method
