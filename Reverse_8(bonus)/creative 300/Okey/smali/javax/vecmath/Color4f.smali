.class public Ljavax/vecmath/Color4f;
.super Ljavax/vecmath/Tuple4f;
.source "Color4f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x770a0aa46bcc7554L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 125
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 61
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 62
    return-void
.end method

.method public constructor <init>(Ljava/awt/Color;)V
    .locals 5
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    const/high16 v4, 0x437f0000    # 255.0f

    .line 113
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v4

    .line 114
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    .line 115
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    .line 116
    invoke-virtual {p1}, Ljava/awt/Color;->getAlpha()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    .line 113
    invoke-direct {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 117
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Color4f;)V
    .locals 0
    .param p1, "c1"    # Ljavax/vecmath/Color4f;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 80
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 89
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "c"    # [F

    .prologue
    .line 70
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>([F)V

    .line 71
    return-void
.end method


# virtual methods
.method public final get()Ljava/awt/Color;
    .locals 6

    .prologue
    const/high16 v5, 0x437f0000    # 255.0f

    .line 155
    iget v4, p0, Ljavax/vecmath/Color4f;->x:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 156
    .local v3, "r":I
    iget v4, p0, Ljavax/vecmath/Color4f;->y:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 157
    .local v2, "g":I
    iget v4, p0, Ljavax/vecmath/Color4f;->z:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 158
    .local v1, "b":I
    iget v4, p0, Ljavax/vecmath/Color4f;->w:F

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 160
    .local v0, "a":I
    new-instance v4, Ljava/awt/Color;

    invoke-direct {v4, v3, v2, v1, v0}, Ljava/awt/Color;-><init>(IIII)V

    return-object v4
.end method

.method public final set(Ljava/awt/Color;)V
    .locals 2
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    const/high16 v1, 0x437f0000    # 255.0f

    .line 139
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color4f;->x:F

    .line 140
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color4f;->y:F

    .line 141
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color4f;->z:F

    .line 142
    invoke-virtual {p1}, Ljava/awt/Color;->getAlpha()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Color4f;->w:F

    .line 143
    return-void
.end method
