.class public abstract Ljavax/vecmath/Tuple4i;
.super Ljava/lang/Object;
.source "Tuple4i.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x6feb43eeddfcd490L


# instance fields
.field public w:I

.field public x:I

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 113
    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 114
    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 115
    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 116
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I
    .param p4, "w"    # I

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 77
    iput p2, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 78
    iput p3, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 79
    iput p4, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 80
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 102
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 103
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 104
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 105
    return-void
.end method

.method public constructor <init>([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 89
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 90
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 91
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 92
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 563
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 564
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 565
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 566
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 481
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 482
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 483
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 484
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 485
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 202
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 203
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 204
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 205
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 206
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple4i;Ljavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 190
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 191
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 192
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 193
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 194
    return-void
.end method

.method public final clamp(II)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I

    .prologue
    .line 494
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    if-le v0, p2, :cond_4

    .line 495
    iput p2, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 500
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    if-le v0, p2, :cond_5

    .line 501
    iput p2, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 506
    :cond_1
    :goto_1
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    if-le v0, p2, :cond_6

    .line 507
    iput p2, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 512
    :cond_2
    :goto_2
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    if-le v0, p2, :cond_7

    .line 513
    iput p2, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 517
    :cond_3
    :goto_3
    return-void

    .line 496
    :cond_4
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    if-ge v0, p1, :cond_0

    .line 497
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    goto :goto_0

    .line 502
    :cond_5
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    if-ge v0, p1, :cond_1

    .line 503
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    goto :goto_1

    .line 508
    :cond_6
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    if-ge v0, p1, :cond_2

    .line 509
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    goto :goto_2

    .line 514
    :cond_7
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    if-ge v0, p1, :cond_3

    .line 515
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    goto :goto_3
.end method

.method public final clamp(IILjavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "t"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 373
    iget v0, p3, Ljavax/vecmath/Tuple4i;->x:I

    if-le v0, p2, :cond_0

    .line 374
    iput p2, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 381
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple4i;->y:I

    if-le v0, p2, :cond_2

    .line 382
    iput p2, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 389
    :goto_1
    iget v0, p3, Ljavax/vecmath/Tuple4i;->z:I

    if-le v0, p2, :cond_4

    .line 390
    iput p2, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 397
    :goto_2
    iget v0, p3, Ljavax/vecmath/Tuple4i;->w:I

    if-le v0, p2, :cond_6

    .line 398
    iput p2, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 404
    :goto_3
    return-void

    .line 375
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple4i;->x:I

    if-ge v0, p1, :cond_1

    .line 376
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    goto :goto_0

    .line 378
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    goto :goto_0

    .line 383
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple4i;->y:I

    if-ge v0, p1, :cond_3

    .line 384
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    goto :goto_1

    .line 386
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    goto :goto_1

    .line 391
    :cond_4
    iget v0, p3, Ljavax/vecmath/Tuple4i;->z:I

    if-ge v0, p1, :cond_5

    .line 392
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    goto :goto_2

    .line 394
    :cond_5
    iget v0, p3, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    goto :goto_2

    .line 399
    :cond_6
    iget v0, p3, Ljavax/vecmath/Tuple4i;->w:I

    if-ge v0, p1, :cond_7

    .line 400
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    goto :goto_3

    .line 402
    :cond_7
    iget v0, p3, Ljavax/vecmath/Tuple4i;->w:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    goto :goto_3
.end method

.method public final clampMax(I)V
    .locals 1
    .param p1, "max"    # I

    .prologue
    .line 544
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    if-le v0, p1, :cond_0

    .line 545
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 547
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    if-le v0, p1, :cond_1

    .line 548
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 550
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    if-le v0, p1, :cond_2

    .line 551
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 553
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    if-le v0, p1, :cond_3

    .line 554
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 555
    :cond_3
    return-void
.end method

.method public final clampMax(ILjavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "max"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 449
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    if-le v0, p1, :cond_0

    .line 450
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 455
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    if-le v0, p1, :cond_1

    .line 456
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 461
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    if-le v0, p1, :cond_2

    .line 462
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 467
    :goto_2
    iget v0, p2, Ljavax/vecmath/Tuple4i;->w:I

    if-le v0, p1, :cond_3

    .line 468
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 472
    :goto_3
    return-void

    .line 452
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    goto :goto_0

    .line 458
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    goto :goto_1

    .line 464
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    goto :goto_2

    .line 470
    :cond_3
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    goto :goto_3
.end method

.method public final clampMin(I)V
    .locals 1
    .param p1, "min"    # I

    .prologue
    .line 525
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    if-ge v0, p1, :cond_0

    .line 526
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 528
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    if-ge v0, p1, :cond_1

    .line 529
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 531
    :cond_1
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    if-ge v0, p1, :cond_2

    .line 532
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 534
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    if-ge v0, p1, :cond_3

    .line 535
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 536
    :cond_3
    return-void
.end method

.method public final clampMin(ILjavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "min"    # I
    .param p2, "t"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 414
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    if-ge v0, p1, :cond_0

    .line 415
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 420
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    if-ge v0, p1, :cond_1

    .line 421
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 426
    :goto_1
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    if-ge v0, p1, :cond_2

    .line 427
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 432
    :goto_2
    iget v0, p2, Ljavax/vecmath/Tuple4i;->w:I

    if-ge v0, p1, :cond_3

    .line 433
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 439
    :goto_3
    return-void

    .line 417
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    goto :goto_0

    .line 423
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    goto :goto_1

    .line 429
    :cond_2
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    goto :goto_2

    .line 435
    :cond_3
    iget v0, p2, Ljavax/vecmath/Tuple4i;->w:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    goto :goto_3
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 579
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 580
    :catch_0
    move-exception v0

    .line 582
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 334
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple4i;

    move-object v3, v0

    .line 335
    .local v3, "t2":Ljavax/vecmath/Tuple4i;
    iget v5, p0, Ljavax/vecmath/Tuple4i;->x:I

    iget v6, v3, Ljavax/vecmath/Tuple4i;->x:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4i;->y:I

    iget v6, v3, Ljavax/vecmath/Tuple4i;->y:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4i;->z:I

    iget v6, v3, Ljavax/vecmath/Tuple4i;->z:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple4i;->w:I

    iget v6, v3, Ljavax/vecmath/Tuple4i;->w:I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    if-ne v5, v6, :cond_0

    const/4 v4, 0x1

    .line 342
    .end local v3    # "t2":Ljavax/vecmath/Tuple4i;
    :cond_0
    :goto_0
    return v4

    .line 338
    :catch_0
    move-exception v2

    .line 339
    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 341
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 342
    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 177
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    .line 178
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    .line 179
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    .line 180
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    iput v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    .line 181
    return-void
.end method

.method public final get([I)V
    .locals 2
    .param p1, "t"    # [I

    .prologue
    .line 165
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple4i;->x:I

    aput v1, p1, v0

    .line 166
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple4i;->y:I

    aput v1, p1, v0

    .line 167
    const/4 v0, 0x2

    iget v1, p0, Ljavax/vecmath/Tuple4i;->z:I

    aput v1, p1, v0

    .line 168
    const/4 v0, 0x3

    iget v1, p0, Ljavax/vecmath/Tuple4i;->w:I

    aput v1, p1, v0

    .line 169
    return-void
.end method

.method public final getW()I
    .locals 1

    .prologue
    .line 665
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    return v0
.end method

.method public final getX()I
    .locals 1

    .prologue
    .line 596
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    return v0
.end method

.method public final getY()I
    .locals 1

    .prologue
    .line 620
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    return v0
.end method

.method public final getZ()I
    .locals 1

    .prologue
    .line 643
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 356
    const-wide/16 v0, 0x1

    .line 357
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4i;->x:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 358
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4i;->y:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 359
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4i;->z:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 360
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple4i;->w:I

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 361
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 252
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 253
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 254
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 255
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 256
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 241
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 242
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 243
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 244
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    neg-int v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 245
    return-void
.end method

.method public final scale(I)V
    .locals 1
    .param p1, "s"    # I

    .prologue
    .line 279
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 280
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 281
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 282
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 283
    return-void
.end method

.method public final scale(ILjavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 266
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 267
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 268
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 269
    iget v0, p2, Ljavax/vecmath/Tuple4i;->w:I

    mul-int/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 270
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 308
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 309
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 310
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 311
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    mul-int/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple4i;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 312
    return-void
.end method

.method public final scaleAdd(ILjavax/vecmath/Tuple4i;Ljavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "s"    # I
    .param p2, "t1"    # Ljavax/vecmath/Tuple4i;
    .param p3, "t2"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 294
    iget v0, p2, Ljavax/vecmath/Tuple4i;->x:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4i;->x:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 295
    iget v0, p2, Ljavax/vecmath/Tuple4i;->y:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4i;->y:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 296
    iget v0, p2, Ljavax/vecmath/Tuple4i;->z:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4i;->z:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 297
    iget v0, p2, Ljavax/vecmath/Tuple4i;->w:I

    mul-int/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple4i;->w:I

    add-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 298
    return-void
.end method

.method public final set(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I
    .param p4, "w"    # I

    .prologue
    .line 128
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 129
    iput p2, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 130
    iput p3, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 131
    iput p4, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 132
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple4i;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 153
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 154
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 155
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 156
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 157
    return-void
.end method

.method public final set([I)V
    .locals 1
    .param p1, "t"    # [I

    .prologue
    .line 141
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 142
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 143
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 144
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 145
    return-void
.end method

.method public final setW(I)V
    .locals 0
    .param p1, "w"    # I

    .prologue
    .line 677
    iput p1, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 678
    return-void
.end method

.method public final setX(I)V
    .locals 0
    .param p1, "x"    # I

    .prologue
    .line 608
    iput p1, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 609
    return-void
.end method

.method public final setY(I)V
    .locals 0
    .param p1, "y"    # I

    .prologue
    .line 632
    iput p1, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 633
    return-void
.end method

.method public final setZ(I)V
    .locals 0
    .param p1, "z"    # I

    .prologue
    .line 655
    iput p1, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 656
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 229
    iget v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 230
    iget v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 231
    iget v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->z:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 232
    iget v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    iget v1, p1, Ljavax/vecmath/Tuple4i;->w:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 233
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple4i;Ljavax/vecmath/Tuple4i;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;
    .param p2, "t2"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 216
    iget v0, p1, Ljavax/vecmath/Tuple4i;->x:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->x:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->x:I

    .line 217
    iget v0, p1, Ljavax/vecmath/Tuple4i;->y:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->y:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->y:I

    .line 218
    iget v0, p1, Ljavax/vecmath/Tuple4i;->z:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->z:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->z:I

    .line 219
    iget v0, p1, Ljavax/vecmath/Tuple4i;->w:I

    iget v1, p2, Ljavax/vecmath/Tuple4i;->w:I

    sub-int/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple4i;->w:I

    .line 220
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 321
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4i;->x:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4i;->y:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4i;->z:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple4i;->w:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
