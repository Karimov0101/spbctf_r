.class public abstract Ljavax/vecmath/Tuple3d;
.super Ljava/lang/Object;
.source "Tuple3d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x4ce97b820b91b15fL


# instance fields
.field public x:D

.field public y:D

.field public z:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 113
    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 114
    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 115
    return-void
.end method

.method public constructor <init>(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 70
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 71
    iput-wide p5, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 72
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 92
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 93
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 94
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 103
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 104
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 105
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 81
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 82
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 83
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 2

    .prologue
    .line 623
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 624
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 625
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 626
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 537
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 538
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 539
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 541
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 208
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 209
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 210
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 211
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 196
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 197
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 198
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 199
    return-void
.end method

.method public final clamp(DD)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 558
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_3

    .line 559
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 564
    :cond_0
    :goto_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_4

    .line 565
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 570
    :cond_1
    :goto_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_5

    .line 571
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 576
    :cond_2
    :goto_2
    return-void

    .line 560
    :cond_3
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 561
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    goto :goto_0

    .line 566
    :cond_4
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 567
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    goto :goto_1

    .line 572
    :cond_5
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    .line 573
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    goto :goto_2
.end method

.method public final clamp(DDLjavax/vecmath/Tuple3d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 431
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_0

    .line 432
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 439
    :goto_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_2

    .line 440
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 447
    :goto_1
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->z:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_4

    .line 448
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 455
    :goto_2
    return-void

    .line 433
    :cond_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 434
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    goto :goto_0

    .line 436
    :cond_1
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    goto :goto_0

    .line 441
    :cond_2
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    .line 442
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    goto :goto_1

    .line 444
    :cond_3
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    goto :goto_1

    .line 449
    :cond_4
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_5

    .line 450
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    goto :goto_2

    .line 452
    :cond_5
    iget-wide v0, p5, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    goto :goto_2
.end method

.method public final clamp(FF)V
    .locals 4
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 548
    float-to-double v0, p1

    float-to-double v2, p2

    invoke-virtual {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple3d;->clamp(DD)V

    .line 549
    return-void
.end method

.method public final clamp(FFLjavax/vecmath/Tuple3d;)V
    .locals 7
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 419
    float-to-double v2, p1

    float-to-double v4, p2

    move-object v1, p0

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Ljavax/vecmath/Tuple3d;->clamp(DDLjavax/vecmath/Tuple3d;)V

    .line 420
    return-void
.end method

.method public final clampMax(D)V
    .locals 3
    .param p1, "max"    # D

    .prologue
    .line 612
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 613
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 614
    :cond_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_2

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 615
    :cond_2
    return-void
.end method

.method public final clampMax(DLjavax/vecmath/Tuple3d;)V
    .locals 3
    .param p1, "max"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 509
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    .line 510
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 515
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    .line 516
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 521
    :goto_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_2

    .line 522
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 527
    :goto_2
    return-void

    .line 512
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    goto :goto_0

    .line 518
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    goto :goto_1

    .line 524
    :cond_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    goto :goto_2
.end method

.method public final clampMax(F)V
    .locals 2
    .param p1, "max"    # F

    .prologue
    .line 603
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Ljavax/vecmath/Tuple3d;->clampMax(D)V

    .line 604
    return-void
.end method

.method public final clampMax(FLjavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "max"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 498
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1, p2}, Ljavax/vecmath/Tuple3d;->clampMax(DLjavax/vecmath/Tuple3d;)V

    .line 499
    return-void
.end method

.method public final clampMin(D)V
    .locals 3
    .param p1, "min"    # D

    .prologue
    .line 592
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 593
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 594
    :cond_1
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 596
    :cond_2
    return-void
.end method

.method public final clampMin(DLjavax/vecmath/Tuple3d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 473
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 474
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 479
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 480
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 485
    :goto_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_2

    .line 486
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 491
    :goto_2
    return-void

    .line 476
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    goto :goto_0

    .line 482
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    goto :goto_1

    .line 488
    :cond_2
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    goto :goto_2
.end method

.method public final clampMin(F)V
    .locals 2
    .param p1, "min"    # F

    .prologue
    .line 583
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Ljavax/vecmath/Tuple3d;->clampMin(D)V

    .line 584
    return-void
.end method

.method public final clampMin(FLjavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "min"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 462
    float-to-double v0, p1

    invoke-virtual {p0, v0, v1, p2}, Ljavax/vecmath/Tuple3d;->clampMin(DLjavax/vecmath/Tuple3d;)V

    .line 463
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 682
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 683
    :catch_0
    move-exception v0

    .line 685
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple3d;D)Z
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "epsilon"    # D

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 398
    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->x:D

    sub-double v0, v4, v6

    .line 399
    .local v0, "diff":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 410
    :cond_0
    :goto_0
    return v2

    .line 400
    :cond_1
    cmpg-double v3, v0, v8

    if-gez v3, :cond_2

    neg-double v4, v0

    :goto_1
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 402
    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->y:D

    sub-double v0, v4, v6

    .line 403
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 404
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 406
    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->z:D

    sub-double v0, v4, v6

    .line 407
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 408
    cmpg-double v3, v0, v8

    if-gez v3, :cond_4

    neg-double v4, v0

    :goto_3
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 410
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move-wide v4, v0

    .line 400
    goto :goto_1

    :cond_3
    move-wide v4, v0

    .line 404
    goto :goto_2

    :cond_4
    move-wide v4, v0

    .line 408
    goto :goto_3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 377
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple3d;

    move-object v4, v0

    .line 378
    .local v4, "t2":Ljavax/vecmath/Tuple3d;
    iget-wide v6, p0, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple3d;->z:D
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 381
    .end local v4    # "t2":Ljavax/vecmath/Tuple3d;
    :cond_0
    :goto_0
    return v5

    .line 380
    :catch_0
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 381
    .end local v2    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple3d;)Z
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    const/4 v1, 0x0

    .line 362
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->y:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->z:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 364
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 183
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    .line 184
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    .line 185
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    .line 186
    return-void
.end method

.method public final get([D)V
    .locals 4
    .param p1, "t"    # [D

    .prologue
    .line 171
    const/4 v0, 0x0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->x:D

    aput-wide v2, p1, v0

    .line 172
    const/4 v0, 0x1

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->y:D

    aput-wide v2, p1, v0

    .line 173
    const/4 v0, 0x2

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->z:D

    aput-wide v2, p1, v0

    .line 174
    return-void
.end method

.method public final getX()D
    .locals 2

    .prologue
    .line 697
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    return-wide v0
.end method

.method public final getY()D
    .locals 2

    .prologue
    .line 721
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    return-wide v0
.end method

.method public final getZ()D
    .locals 2

    .prologue
    .line 744
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 345
    const-wide/16 v0, 0x1

    .line 346
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->x:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 347
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->y:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 348
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple3d;->z:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 349
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3d;D)V
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 666
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 667
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 668
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 669
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3d;F)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "alpha"    # F

    .prologue
    .line 655
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Ljavax/vecmath/Tuple3d;->interpolate(Ljavax/vecmath/Tuple3d;D)V

    .line 656
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;D)V
    .locals 7
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3d;
    .param p3, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 645
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 646
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 647
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 648
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;F)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3d;
    .param p3, "alpha"    # F

    .prologue
    .line 633
    float-to-double v0, p3

    invoke-virtual {p0, p1, p2, v0, v1}, Ljavax/vecmath/Tuple3d;->interpolate(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;D)V

    .line 634
    return-void
.end method

.method public final negate()V
    .locals 2

    .prologue
    .line 256
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 257
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 258
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 259
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 245
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 246
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 247
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 248
    return-void
.end method

.method public final scale(D)V
    .locals 3
    .param p1, "s"    # D

    .prologue
    .line 283
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 284
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 285
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 286
    return-void
.end method

.method public final scale(DLjavax/vecmath/Tuple3d;)V
    .locals 3
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 270
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 271
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 272
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 273
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple3d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 319
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple3d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 320
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple3d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 321
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple3d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 322
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p4, "t2"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 298
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple3d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 299
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple3d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 300
    iget-wide v0, p3, Ljavax/vecmath/Tuple3d;->z:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple3d;->z:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 301
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 308
    new-instance v0, Ljavax/vecmath/Point3d;

    invoke-direct {v0, p3}, Ljavax/vecmath/Point3d;-><init>(Ljavax/vecmath/Tuple3f;)V

    invoke-virtual {p0, p1, p2, v0}, Ljavax/vecmath/Tuple3d;->scaleAdd(DLjavax/vecmath/Tuple3d;)V

    .line 309
    return-void
.end method

.method public final set(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 125
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 126
    iput-wide p3, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 127
    iput-wide p5, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 128
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 148
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 149
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 150
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 151
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 159
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 160
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 161
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 162
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 137
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 138
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 139
    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 140
    return-void
.end method

.method public final setX(D)V
    .locals 1
    .param p1, "x"    # D

    .prologue
    .line 709
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 710
    return-void
.end method

.method public final setY(D)V
    .locals 1
    .param p1, "y"    # D

    .prologue
    .line 733
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 734
    return-void
.end method

.method public final setZ(D)V
    .locals 1
    .param p1, "z"    # D

    .prologue
    .line 756
    iput-wide p1, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 757
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 233
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 234
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 235
    iget-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->z:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 236
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple3d;Ljavax/vecmath/Tuple3d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 221
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->x:D

    .line 222
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->y:D

    .line 223
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple3d;->z:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple3d;->z:D

    .line 224
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 332
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple3d;->z:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
