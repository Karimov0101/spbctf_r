.class public Ljavax/vecmath/Point3d;
.super Ljavax/vecmath/Tuple3d;
.source "Point3d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x4f5aa35a84b40eefL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 113
    invoke-direct {p0}, Ljavax/vecmath/Tuple3d;-><init>()V

    .line 114
    return-void
.end method

.method public constructor <init>(DDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D

    .prologue
    .line 54
    invoke-direct/range {p0 .. p6}, Ljavax/vecmath/Tuple3d;-><init>(DDD)V

    .line 55
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point3d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3d;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point3f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3d;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 104
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3d;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 105
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3d;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 95
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .param p1, "p"    # [D

    .prologue
    .line 64
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3d;-><init>([D)V

    .line 65
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point3d;)D
    .locals 10
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 142
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->x:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->x:D

    sub-double v0, v6, v8

    .line 143
    .local v0, "dx":D
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    sub-double v2, v6, v8

    .line 144
    .local v2, "dy":D
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    sub-double v4, v6, v8

    .line 145
    .local v4, "dz":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    mul-double v8, v4, v4

    add-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    return-wide v6
.end method

.method public final distanceL1(Ljavax/vecmath/Point3d;)D
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 157
    iget-wide v0, p0, Ljavax/vecmath/Point3d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Point3d;->x:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Ljavax/vecmath/Point3d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Point3d;->y:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Point3d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Point3d;->z:D

    sub-double/2addr v2, v4

    .line 158
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point3d;)D
    .locals 8
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 171
    iget-wide v2, p0, Ljavax/vecmath/Point3d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Point3d;->x:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    iget-wide v4, p0, Ljavax/vecmath/Point3d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Point3d;->y:D

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    .line 173
    .local v0, "tmp":D
    iget-wide v2, p0, Ljavax/vecmath/Point3d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Point3d;->z:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v2

    return-wide v2
.end method

.method public final distanceSquared(Ljavax/vecmath/Point3d;)D
    .locals 10
    .param p1, "p1"    # Ljavax/vecmath/Point3d;

    .prologue
    .line 126
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->x:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->x:D

    sub-double v0, v6, v8

    .line 127
    .local v0, "dx":D
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->y:D

    sub-double v2, v6, v8

    .line 128
    .local v2, "dy":D
    iget-wide v6, p0, Ljavax/vecmath/Point3d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Point3d;->z:D

    sub-double v4, v6, v8

    .line 129
    .local v4, "dz":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    mul-double v8, v4, v4

    add-double/2addr v6, v8

    return-wide v6
.end method

.method public final project(Ljavax/vecmath/Point4d;)V
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 186
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Point4d;->w:D

    div-double v0, v2, v4

    .line 187
    .local v0, "oneOw":D
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point3d;->x:D

    .line 188
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point3d;->y:D

    .line 189
    iget-wide v2, p1, Ljavax/vecmath/Point4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Point3d;->z:D

    .line 191
    return-void
.end method
