.class public Ljavax/vecmath/Point4i;
.super Ljavax/vecmath/Tuple4i;
.source "Point4i.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x89b2033bae5caffL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljavax/vecmath/Tuple4i;-><init>()V

    .line 84
    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I
    .param p4, "w"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4i;-><init>(IIII)V

    .line 57
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4i;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4i;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4i;-><init>(Ljavax/vecmath/Tuple4i;)V

    .line 76
    return-void
.end method

.method public constructor <init>([I)V
    .locals 0
    .param p1, "t"    # [I

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4i;-><init>([I)V

    .line 66
    return-void
.end method
