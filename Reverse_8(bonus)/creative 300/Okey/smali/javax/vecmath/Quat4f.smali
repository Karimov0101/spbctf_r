.class public Ljavax/vecmath/Quat4f;
.super Ljavax/vecmath/Tuple4f;
.source "Quat4f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final EPS:D = 1.0E-6

.field static final EPS2:D = 1.0E-30

.field static final PIO2:D = 1.57079632679

.field static final serialVersionUID:J = 0x2522d3980053434fL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 140
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 58
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 60
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-float v1, p1, p1

    mul-float v4, p2, p2

    add-float/2addr v1, v4

    mul-float v4, p3, p3

    add-float/2addr v1, v4

    mul-float v4, p4, p4

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 61
    .local v0, "mag":F
    mul-float v1, p1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 62
    mul-float v1, p2, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 63
    mul-float v1, p3, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 64
    mul-float v1, p4, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 66
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4d;)V
    .locals 0
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 99
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 100
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4f;)V
    .locals 0
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 90
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 91
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 124
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 126
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 127
    .local v0, "mag":D
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 128
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 129
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 130
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 131
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 108
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 110
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Tuple4f;->y:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Tuple4f;->z:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    iget v4, p1, Ljavax/vecmath/Tuple4f;->w:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 111
    .local v0, "mag":F
    iget v1, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 112
    iget v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 113
    iget v1, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 114
    iget v1, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 116
    return-void
.end method

.method public constructor <init>([F)V
    .locals 10
    .param p1, "q"    # [F

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 73
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 75
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    aget v1, p1, v6

    aget v4, p1, v6

    mul-float/2addr v1, v4

    aget v4, p1, v7

    aget v5, p1, v7

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    aget v4, p1, v8

    aget v5, p1, v8

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    aget v4, p1, v9

    aget v5, p1, v9

    mul-float/2addr v4, v5

    add-float/2addr v1, v4

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 76
    .local v0, "mag":F
    aget v1, p1, v6

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 77
    aget v1, p1, v7

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 78
    aget v1, p1, v8

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 79
    aget v1, p1, v9

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 81
    return-void
.end method


# virtual methods
.method public final conjugate()V
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Ljavax/vecmath/Quat4f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 161
    iget v0, p0, Ljavax/vecmath/Quat4f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 162
    iget v0, p0, Ljavax/vecmath/Quat4f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 163
    return-void
.end method

.method public final conjugate(Ljavax/vecmath/Quat4f;)V
    .locals 1
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 149
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 150
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 151
    iget v0, p1, Ljavax/vecmath/Quat4f;->z:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 152
    iget v0, p1, Ljavax/vecmath/Quat4f;->w:F

    iput v0, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 153
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Quat4f;F)V
    .locals 16
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "alpha"    # F

    .prologue
    .line 625
    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    move-object/from16 v0, p1

    iget v13, v0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/Quat4f;->y:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/Quat4f;->z:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    move-object/from16 v0, p0

    iget v13, v0, Ljavax/vecmath/Quat4f;->w:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    float-to-double v2, v12

    .line 627
    .local v2, "dot":D
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_0

    .line 629
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->x:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->y:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->y:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->z:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->z:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->w:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->w:F

    .line 630
    neg-double v2, v2

    .line 633
    :cond_0
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v2

    const-wide v14, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v12, v12, v14

    if-lez v12, :cond_1

    .line 634
    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    .line 635
    .local v4, "om":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 636
    .local v10, "sinom":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move/from16 v0, p2

    float-to-double v14, v0

    sub-double/2addr v12, v14

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v6, v12, v10

    .line 637
    .local v6, "s1":D
    move/from16 v0, p2

    float-to-double v12, v0

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v8, v12, v10

    .line 643
    .end local v4    # "om":D
    .end local v10    # "sinom":D
    .local v8, "s2":D
    :goto_0
    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->w:F

    .line 644
    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->x:F

    .line 645
    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->y:F

    .line 646
    move-object/from16 v0, p0

    iget v12, v0, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->z:F

    .line 647
    return-void

    .line 639
    .end local v6    # "s1":D
    .end local v8    # "s2":D
    :cond_1
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move/from16 v0, p2

    float-to-double v14, v0

    sub-double v6, v12, v14

    .line 640
    .restart local v6    # "s1":D
    move/from16 v0, p2

    float-to-double v8, v0

    .restart local v8    # "s2":D
    goto :goto_0
.end method

.method public final interpolate(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Quat4f;F)V
    .locals 16
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "q2"    # Ljavax/vecmath/Quat4f;
    .param p3, "alpha"    # F

    .prologue
    .line 668
    move-object/from16 v0, p2

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    move-object/from16 v0, p1

    iget v13, v0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v12, v13

    move-object/from16 v0, p2

    iget v13, v0, Ljavax/vecmath/Quat4f;->y:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    move-object/from16 v0, p2

    iget v13, v0, Ljavax/vecmath/Quat4f;->z:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    move-object/from16 v0, p2

    iget v13, v0, Ljavax/vecmath/Quat4f;->w:F

    move-object/from16 v0, p1

    iget v14, v0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    float-to-double v2, v12

    .line 670
    .local v2, "dot":D
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_0

    .line 672
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->x:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->y:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->y:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->z:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->z:F

    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->w:F

    neg-float v12, v12

    move-object/from16 v0, p1

    iput v12, v0, Ljavax/vecmath/Quat4f;->w:F

    .line 673
    neg-double v2, v2

    .line 676
    :cond_0
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v2

    const-wide v14, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpl-double v12, v12, v14

    if-lez v12, :cond_1

    .line 677
    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    .line 678
    .local v4, "om":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 679
    .local v10, "sinom":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move/from16 v0, p3

    float-to-double v14, v0

    sub-double/2addr v12, v14

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v6, v12, v10

    .line 680
    .local v6, "s1":D
    move/from16 v0, p3

    float-to-double v12, v0

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v8, v12, v10

    .line 685
    .end local v4    # "om":D
    .end local v10    # "sinom":D
    .local v8, "s2":D
    :goto_0
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget v14, v0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->w:F

    .line 686
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget v14, v0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->x:F

    .line 687
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget v14, v0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->y:F

    .line 688
    move-object/from16 v0, p1

    iget v12, v0, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v12, v12

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget v14, v0, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v14, v14

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    double-to-float v12, v12

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Quat4f;->z:F

    .line 689
    return-void

    .line 682
    .end local v6    # "s1":D
    .end local v8    # "s2":D
    :cond_1
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    move/from16 v0, p3

    float-to-double v14, v0

    sub-double v6, v12, v14

    .line 683
    .restart local v6    # "s1":D
    move/from16 v0, p3

    float-to-double v8, v0

    .restart local v8    # "s2":D
    goto :goto_0
.end method

.method public final inverse()V
    .locals 5

    .prologue
    .line 269
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Quat4f;->x:F

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Quat4f;->y:F

    iget v4, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Quat4f;->z:F

    iget v4, p0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    div-float v0, v1, v2

    .line 270
    .local v0, "norm":F
    iget v1, p0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 271
    iget v1, p0, Ljavax/vecmath/Quat4f;->x:F

    neg-float v2, v0

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 272
    iget v1, p0, Ljavax/vecmath/Quat4f;->y:F

    neg-float v2, v0

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 273
    iget v1, p0, Ljavax/vecmath/Quat4f;->z:F

    neg-float v2, v0

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 274
    return-void
.end method

.method public final inverse(Ljavax/vecmath/Quat4f;)V
    .locals 5
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 254
    const/high16 v1, 0x3f800000    # 1.0f

    iget v2, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    div-float v0, v1, v2

    .line 255
    .local v0, "norm":F
    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 256
    neg-float v1, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 257
    neg-float v1, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 258
    neg-float v1, v0

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 259
    return-void
.end method

.method public final mul(Ljavax/vecmath/Quat4f;)V
    .locals 6
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 203
    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float v0, v3, v4

    .line 204
    .local v0, "w":F
    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float v1, v3, v4

    .line 205
    .local v1, "x":F
    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 206
    .local v2, "y":F
    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 207
    iput v0, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 208
    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 209
    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 210
    return-void
.end method

.method public final mul(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Quat4f;)V
    .locals 6
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "q2"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 175
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 176
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 177
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 178
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 179
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 191
    :goto_0
    return-void

    .line 183
    :cond_0
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float v0, v3, v4

    .line 184
    .local v0, "w":F
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    sub-float v1, v3, v4

    .line 185
    .local v1, "x":F
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 186
    .local v2, "y":F
    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p2, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    iget v4, p2, Ljavax/vecmath/Quat4f;->w:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p2, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 187
    iput v0, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 188
    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 189
    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    goto :goto_0
.end method

.method public final mulInverse(Ljavax/vecmath/Quat4f;)V
    .locals 1
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 238
    new-instance v0, Ljavax/vecmath/Quat4f;

    invoke-direct {v0, p1}, Ljavax/vecmath/Quat4f;-><init>(Ljavax/vecmath/Quat4f;)V

    .line 240
    .local v0, "tempQuat":Ljavax/vecmath/Quat4f;
    invoke-virtual {v0}, Ljavax/vecmath/Quat4f;->inverse()V

    .line 241
    invoke-virtual {p0, v0}, Ljavax/vecmath/Quat4f;->mul(Ljavax/vecmath/Quat4f;)V

    .line 242
    return-void
.end method

.method public final mulInverse(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Quat4f;)V
    .locals 1
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "q2"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 222
    new-instance v0, Ljavax/vecmath/Quat4f;

    invoke-direct {v0, p2}, Ljavax/vecmath/Quat4f;-><init>(Ljavax/vecmath/Quat4f;)V

    .line 224
    .local v0, "tempQuat":Ljavax/vecmath/Quat4f;
    invoke-virtual {v0}, Ljavax/vecmath/Quat4f;->inverse()V

    .line 225
    invoke-virtual {p0, p1, v0}, Ljavax/vecmath/Quat4f;->mul(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Quat4f;)V

    .line 226
    return-void
.end method

.method public final normalize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 310
    iget v1, p0, Ljavax/vecmath/Quat4f;->x:F

    iget v2, p0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Quat4f;->z:F

    iget v3, p0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p0, Ljavax/vecmath/Quat4f;->w:F

    iget v3, p0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 312
    .local v0, "norm":F
    cmpl-float v1, v0, v4

    if-lez v1, :cond_0

    .line 313
    const/high16 v1, 0x3f800000    # 1.0f

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v1, v2

    .line 314
    iget v1, p0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 315
    iget v1, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 316
    iget v1, p0, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 317
    iget v1, p0, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 324
    :goto_0
    return-void

    .line 319
    :cond_0
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 320
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 321
    iput v4, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 322
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    goto :goto_0
.end method

.method public final normalize(Ljavax/vecmath/Quat4f;)V
    .locals 5
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/4 v4, 0x0

    .line 286
    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v2, v3

    add-float v0, v1, v2

    .line 288
    .local v0, "norm":F
    cmpl-float v1, v0, v4

    if-lez v1, :cond_0

    .line 289
    const/high16 v1, 0x3f800000    # 1.0f

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v0, v1, v2

    .line 290
    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 291
    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 292
    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 293
    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 300
    :goto_0
    return-void

    .line 295
    :cond_0
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 296
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 297
    iput v4, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 298
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 14
    .param p1, "a"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v10, 0x0

    .line 589
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    double-to-float v0, v2

    .line 591
    .local v0, "amag":F
    float-to-double v2, v0

    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 592
    iput v10, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 593
    iput v10, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 594
    iput v10, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 595
    iput v10, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 605
    :goto_0
    return-void

    .line 597
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v0, v2, v0

    .line 598
    iget-wide v2, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    div-double/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 599
    .local v1, "mag":F
    iget-wide v2, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    div-double/2addr v2, v12

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 600
    iget-wide v2, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    double-to-float v2, v2

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 601
    iget-wide v2, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    double-to-float v2, v2

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 602
    iget-wide v2, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    double-to-float v2, v2

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 10
    .param p1, "a"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/4 v6, 0x0

    .line 562
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 563
    .local v0, "amag":F
    float-to-double v2, v0

    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 564
    iput v6, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 565
    iput v6, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 566
    iput v6, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 567
    iput v6, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 576
    :goto_0
    return-void

    .line 569
    :cond_0
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v0, v2, v0

    .line 570
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v2, v2

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 571
    .local v1, "mag":F
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v2, v2

    div-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 572
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 573
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 574
    iget v2, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v2, v0

    mul-float/2addr v2, v1

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    const-wide/16 v12, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide v10, 0x39b4484bfeebc2a0L    # 1.0E-30

    const/4 v8, 0x0

    .line 504
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v2, v4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    mul-double v0, v6, v2

    .line 506
    .local v0, "ww":D
    cmpl-double v2, v0, v12

    if-ltz v2, :cond_0

    .line 507
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_1

    .line 508
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 509
    iget v2, p0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v2, v2

    div-double v0, v6, v2

    .line 510
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 511
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 512
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 550
    :goto_0
    return-void

    .line 516
    :cond_0
    iput v8, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 517
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 518
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 519
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 523
    :cond_1
    iput v8, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 524
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 525
    cmpl-double v2, v0, v12

    if-ltz v2, :cond_2

    .line 526
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_3

    .line 527
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 528
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    div-double v0, v2, v4

    .line 529
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 530
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 534
    :cond_2
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 535
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 536
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 540
    :cond_3
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 541
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    sub-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 542
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_4

    .line 543
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 544
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 548
    :cond_4
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 549
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v8, 0x3e800000    # 0.25f

    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 448
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v1, v2

    add-float/2addr v1, v5

    mul-float v0, v8, v1

    .line 450
    .local v0, "ww":F
    cmpl-float v1, v0, v4

    if-ltz v1, :cond_0

    .line 451
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_1

    .line 452
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 453
    iget v1, p0, Ljavax/vecmath/Quat4f;->w:F

    div-float v0, v8, v1

    .line 454
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 455
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 456
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 494
    :goto_0
    return-void

    .line 460
    :cond_0
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 461
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 462
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 463
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 467
    :cond_1
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 468
    const/high16 v1, -0x41000000    # -0.5f

    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v2, v3

    mul-float v0, v1, v2

    .line 469
    cmpl-float v1, v0, v4

    if-ltz v1, :cond_2

    .line 470
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_3

    .line 471
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 472
    iget v1, p0, Ljavax/vecmath/Quat4f;->x:F

    div-float v0, v9, v1

    .line 473
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 474
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 478
    :cond_2
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 479
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 480
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 484
    :cond_3
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 485
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m22:F

    sub-float v1, v5, v1

    mul-float v0, v9, v1

    .line 486
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_4

    .line 487
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 488
    iget v1, p1, Ljavax/vecmath/Matrix3f;->m21:F

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    div-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 492
    :cond_4
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 493
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    const-wide/high16 v6, 0x3fd0000000000000L    # 0.25

    const-wide/16 v12, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const-wide v10, 0x39b4484bfeebc2a0L    # 1.0E-30

    const/4 v8, 0x0

    .line 392
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v2, v4

    mul-double v0, v6, v2

    .line 394
    .local v0, "ww":D
    cmpl-double v2, v0, v12

    if-ltz v2, :cond_0

    .line 395
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_1

    .line 396
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 397
    iget v2, p0, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v2, v2

    div-double v0, v6, v2

    .line 398
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m12:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 399
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m20:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 400
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m01:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 438
    :goto_0
    return-void

    .line 404
    :cond_0
    iput v8, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 405
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 406
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 407
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 411
    :cond_1
    iput v8, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 412
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 413
    cmpl-double v2, v0, v12

    if-ltz v2, :cond_2

    .line 414
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_3

    .line 415
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 416
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget v4, p0, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v4, v4

    div-double v0, v2, v4

    .line 417
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 418
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v2, v0

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 422
    :cond_2
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 423
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 424
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 428
    :cond_3
    iput v8, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 429
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    sub-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 430
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_4

    .line 431
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 432
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p0, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 436
    :cond_4
    iput v8, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 437
    iput v9, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3e800000    # 0.25f

    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 334
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v1, v2

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v1, v2

    mul-float v0, v8, v1

    .line 336
    .local v0, "ww":F
    cmpl-float v1, v0, v4

    if-ltz v1, :cond_0

    .line 337
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_1

    .line 338
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 339
    iget v1, p0, Ljavax/vecmath/Quat4f;->w:F

    div-float v0, v8, v1

    .line 340
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m12:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 341
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m20:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 342
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m01:F

    sub-float/2addr v1, v2

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    .line 382
    :goto_0
    return-void

    .line 346
    :cond_0
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 347
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 348
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 349
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 353
    :cond_1
    iput v4, p0, Ljavax/vecmath/Quat4f;->w:F

    .line 354
    const/high16 v1, -0x41000000    # -0.5f

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v2, v3

    mul-float v0, v1, v2

    .line 356
    cmpl-float v1, v0, v4

    if-ltz v1, :cond_2

    .line 357
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_3

    .line 358
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 359
    iget v1, p0, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v9

    div-float v0, v5, v1

    .line 360
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m10:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 361
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 365
    :cond_2
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 366
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 367
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 371
    :cond_3
    iput v4, p0, Ljavax/vecmath/Quat4f;->x:F

    .line 372
    const/high16 v1, 0x3f000000    # 0.5f

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m22:F

    sub-float v2, v5, v2

    mul-float v0, v1, v2

    .line 374
    float-to-double v2, v0

    cmpl-double v1, v2, v6

    if-ltz v1, :cond_4

    .line 375
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 376
    iget v1, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iget v2, p0, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v9

    div-float/2addr v1, v2

    iput v1, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0

    .line 380
    :cond_4
    iput v4, p0, Ljavax/vecmath/Quat4f;->y:F

    .line 381
    iput v5, p0, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_0
.end method
