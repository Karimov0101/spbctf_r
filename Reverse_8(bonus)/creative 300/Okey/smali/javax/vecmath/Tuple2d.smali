.class public abstract Ljavax/vecmath/Tuple2d;
.super Ljava/lang/Object;
.source "Tuple2d.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x561f4c19391f878eL


# instance fields
.field public x:D

.field public y:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 106
    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 107
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 64
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 65
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 86
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 87
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 97
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 98
    return-void
.end method

.method public constructor <init>([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 75
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 76
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 2

    .prologue
    .line 508
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 509
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 510
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 453
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 454
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 455
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 184
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 185
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 186
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2d;Ljavax/vecmath/Tuple2d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 173
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 174
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 175
    return-void
.end method

.method public final clamp(DD)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D

    .prologue
    .line 466
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_2

    .line 467
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 472
    :cond_0
    :goto_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_3

    .line 473
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 478
    :cond_1
    :goto_1
    return-void

    .line 468
    :cond_2
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 469
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    goto :goto_0

    .line 474
    :cond_3
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 475
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    goto :goto_1
.end method

.method public final clamp(DDLjavax/vecmath/Tuple2d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "max"    # D
    .param p5, "t"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 381
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_0

    .line 382
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 389
    :goto_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->y:D

    cmpl-double v0, v0, p3

    if-lez v0, :cond_2

    .line 390
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 397
    :goto_1
    return-void

    .line 383
    :cond_0
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 384
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    goto :goto_0

    .line 386
    :cond_1
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    goto :goto_0

    .line 391
    :cond_2
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_3

    .line 392
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    goto :goto_1

    .line 394
    :cond_3
    iget-wide v0, p5, Ljavax/vecmath/Tuple2d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    goto :goto_1
.end method

.method public final clampMax(D)V
    .locals 3
    .param p1, "max"    # D

    .prologue
    .line 498
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 499
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 500
    :cond_1
    return-void
.end method

.method public final clampMax(DLjavax/vecmath/Tuple2d;)V
    .locals 3
    .param p1, "max"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 431
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_0

    .line 432
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 437
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    cmpl-double v0, v0, p1

    if-lez v0, :cond_1

    .line 438
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 443
    :goto_1
    return-void

    .line 434
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    goto :goto_0

    .line 440
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    goto :goto_1
.end method

.method public final clampMin(D)V
    .locals 3
    .param p1, "min"    # D

    .prologue
    .line 487
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 488
    :cond_0
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 489
    :cond_1
    return-void
.end method

.method public final clampMin(DLjavax/vecmath/Tuple2d;)V
    .locals 3
    .param p1, "min"    # D
    .param p3, "t"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 408
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_0

    .line 409
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 414
    :goto_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    cmpg-double v0, v0, p1

    if-gez v0, :cond_1

    .line 415
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 420
    :goto_1
    return-void

    .line 411
    :cond_0
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    goto :goto_0

    .line 417
    :cond_1
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    goto :goto_1
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 551
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 552
    :catch_0
    move-exception v0

    .line 554
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple2d;D)Z
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p2, "epsilon"    # D

    .prologue
    const-wide/16 v8, 0x0

    const/4 v2, 0x0

    .line 350
    iget-wide v4, p0, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple2d;->x:D

    sub-double v0, v4, v6

    .line 351
    .local v0, "diff":D
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 358
    :cond_0
    :goto_0
    return v2

    .line 352
    :cond_1
    cmpg-double v3, v0, v8

    if-gez v3, :cond_2

    neg-double v4, v0

    :goto_1
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 354
    iget-wide v4, p0, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple2d;->y:D

    sub-double v0, v4, v6

    .line 355
    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v3

    if-nez v3, :cond_0

    .line 356
    cmpg-double v3, v0, v8

    if-gez v3, :cond_3

    neg-double v4, v0

    :goto_2
    cmpl-double v3, v4, p2

    if-gtz v3, :cond_0

    .line 358
    const/4 v2, 0x1

    goto :goto_0

    :cond_2
    move-wide v4, v0

    .line 352
    goto :goto_1

    :cond_3
    move-wide v4, v0

    .line 356
    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 10
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x0

    .line 329
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple2d;

    move-object v4, v0

    .line 330
    .local v4, "t2":Ljavax/vecmath/Tuple2d;
    iget-wide v6, p0, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    iget-wide v6, p0, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v8, v4, Ljavax/vecmath/Tuple2d;->y:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v6, v6, v8

    if-nez v6, :cond_0

    const/4 v5, 0x1

    .line 333
    .end local v4    # "t2":Ljavax/vecmath/Tuple2d;
    :cond_0
    :goto_0
    return v5

    .line 332
    :catch_0
    move-exception v3

    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 333
    .end local v3    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v2

    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple2d;)Z
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    const/4 v1, 0x0

    .line 313
    :try_start_0
    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple2d;->x:D

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple2d;->y:D
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 315
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get([D)V
    .locals 4
    .param p1, "t"    # [D

    .prologue
    .line 161
    const/4 v0, 0x0

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->x:D

    aput-wide v2, p1, v0

    .line 162
    const/4 v0, 0x1

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->y:D

    aput-wide v2, p1, v0

    .line 163
    return-void
.end method

.method public final getX()D
    .locals 2

    .prologue
    .line 567
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    return-wide v0
.end method

.method public final getY()D
    .locals 2

    .prologue
    .line 591
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 297
    const-wide/16 v0, 0x1

    .line 298
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple2d;->x:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 299
    mul-long v2, v6, v0

    iget-wide v4, p0, Ljavax/vecmath/Tuple2d;->y:D

    invoke-static {v4, v5}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v4

    add-long v0, v2, v4

    .line 300
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final interpolate(Ljavax/vecmath/Tuple2d;D)V
    .locals 6
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p2, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 535
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 536
    sub-double v0, v4, p2

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v2, p2

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 538
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple2d;Ljavax/vecmath/Tuple2d;D)V
    .locals 7
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2d;
    .param p3, "alpha"    # D

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 522
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 523
    sub-double v0, v4, p3

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 524
    return-void
.end method

.method public final negate()V
    .locals 2

    .prologue
    .line 230
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 231
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 232
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 220
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 221
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 222
    return-void
.end method

.method public final scale(D)V
    .locals 3
    .param p1, "s"    # D

    .prologue
    .line 255
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 256
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 257
    return-void
.end method

.method public final scale(DLjavax/vecmath/Tuple2d;)V
    .locals 3
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 243
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 244
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 245
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple2d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 282
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple2d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 283
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p3, Ljavax/vecmath/Tuple2d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 284
    return-void
.end method

.method public final scaleAdd(DLjavax/vecmath/Tuple2d;Ljavax/vecmath/Tuple2d;)V
    .locals 5
    .param p1, "s"    # D
    .param p3, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p4, "t2"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 269
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->x:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple2d;->x:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 270
    iget-wide v0, p3, Ljavax/vecmath/Tuple2d;->y:D

    mul-double/2addr v0, p1

    iget-wide v2, p4, Ljavax/vecmath/Tuple2d;->y:D

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 271
    return-void
.end method

.method public final set(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 117
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 118
    iput-wide p3, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 119
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 140
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 141
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 142
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 151
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 152
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    float-to-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 153
    return-void
.end method

.method public final set([D)V
    .locals 2
    .param p1, "t"    # [D

    .prologue
    .line 129
    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 130
    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 131
    return-void
.end method

.method public final setX(D)V
    .locals 1
    .param p1, "x"    # D

    .prologue
    .line 579
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 580
    return-void
.end method

.method public final setY(D)V
    .locals 1
    .param p1, "y"    # D

    .prologue
    .line 603
    iput-wide p1, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 604
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 209
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 210
    iget-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Tuple2d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 211
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2d;Ljavax/vecmath/Tuple2d;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 197
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->x:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->x:D

    .line 198
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    iget-wide v2, p2, Ljavax/vecmath/Tuple2d;->y:D

    sub-double/2addr v0, v2

    iput-wide v0, p0, Ljavax/vecmath/Tuple2d;->y:D

    .line 199
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 368
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->x:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Ljavax/vecmath/Tuple2d;->y:D

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
