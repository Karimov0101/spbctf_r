.class public Ljavax/vecmath/TexCoord3f;
.super Ljavax/vecmath/Tuple3f;
.source "TexCoord3f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x30d182bfea20bcf1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljavax/vecmath/Tuple3f;-><init>()V

    .line 105
    return-void
.end method

.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3f;-><init>(FFF)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/TexCoord3f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/TexCoord3f;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 76
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3d;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 85
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>(Ljavax/vecmath/Tuple3f;)V

    .line 86
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "v"    # [F

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3f;-><init>([F)V

    .line 66
    return-void
.end method
