.class public abstract Ljavax/vecmath/Tuple2f;
.super Ljava/lang/Object;
.source "Tuple2f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field static final serialVersionUID:J = 0x7d0e24d20c6ee2c4L


# instance fields
.field public x:F

.field public y:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 107
    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 108
    return-void
.end method

.method public constructor <init>(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 64
    iput p2, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 65
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 97
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 98
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 86
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 87
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 75
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 76
    return-void
.end method


# virtual methods
.method public final absolute()V
    .locals 1

    .prologue
    .line 510
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 511
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 512
    return-void
.end method

.method public final absolute(Ljavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "t"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 455
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 456
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 457
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 186
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple2f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 187
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple2f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 188
    return-void
.end method

.method public final add(Ljavax/vecmath/Tuple2f;Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 175
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple2f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 176
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple2f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 177
    return-void
.end method

.method public final clamp(FF)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 468
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_2

    .line 469
    iput p2, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 474
    :cond_0
    :goto_0
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_3

    .line 475
    iput p2, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 480
    :cond_1
    :goto_1
    return-void

    .line 470
    :cond_2
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 471
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    goto :goto_0

    .line 476
    :cond_3
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 477
    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    goto :goto_1
.end method

.method public final clamp(FFLjavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F
    .param p3, "t"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 383
    iget v0, p3, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_0

    .line 384
    iput p2, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 391
    :goto_0
    iget v0, p3, Ljavax/vecmath/Tuple2f;->y:F

    cmpl-float v0, v0, p2

    if-lez v0, :cond_2

    .line 392
    iput p2, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 399
    :goto_1
    return-void

    .line 385
    :cond_0
    iget v0, p3, Ljavax/vecmath/Tuple2f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 386
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    goto :goto_0

    .line 388
    :cond_1
    iget v0, p3, Ljavax/vecmath/Tuple2f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    goto :goto_0

    .line 393
    :cond_2
    iget v0, p3, Ljavax/vecmath/Tuple2f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_3

    .line 394
    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    goto :goto_1

    .line 396
    :cond_3
    iget v0, p3, Ljavax/vecmath/Tuple2f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    goto :goto_1
.end method

.method public final clampMax(F)V
    .locals 1
    .param p1, "max"    # F

    .prologue
    .line 500
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 501
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 502
    :cond_1
    return-void
.end method

.method public final clampMax(FLjavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "max"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 433
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_0

    .line 434
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 439
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    cmpl-float v0, v0, p1

    if-lez v0, :cond_1

    .line 440
    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 445
    :goto_1
    return-void

    .line 436
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    goto :goto_0

    .line 442
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    goto :goto_1
.end method

.method public final clampMin(F)V
    .locals 1
    .param p1, "min"    # F

    .prologue
    .line 489
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 490
    :cond_0
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 491
    :cond_1
    return-void
.end method

.method public final clampMin(FLjavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "min"    # F
    .param p2, "t"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 410
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 411
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 416
    :goto_0
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_1

    .line 417
    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 422
    :goto_1
    return-void

    .line 413
    :cond_0
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    goto :goto_0

    .line 419
    :cond_1
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    goto :goto_1
.end method

.method public clone()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 555
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    return-object v1

    .line 556
    :catch_0
    move-exception v0

    .line 558
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v1, Ljava/lang/InternalError;

    invoke-direct {v1}, Ljava/lang/InternalError;-><init>()V

    throw v1
.end method

.method public epsilonEquals(Ljavax/vecmath/Tuple2f;F)Z
    .locals 5
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p2, "epsilon"    # F

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 352
    iget v2, p0, Ljavax/vecmath/Tuple2f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple2f;->x:F

    sub-float v0, v2, v3

    .line 353
    .local v0, "diff":F
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v1

    .line 354
    :cond_1
    cmpg-float v2, v0, v4

    if-gez v2, :cond_2

    neg-float v2, v0

    :goto_1
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 356
    iget v2, p0, Ljavax/vecmath/Tuple2f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple2f;->y:F

    sub-float v0, v2, v3

    .line 357
    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v2

    if-nez v2, :cond_0

    .line 358
    cmpg-float v2, v0, v4

    if-gez v2, :cond_3

    neg-float v2, v0

    :goto_2
    cmpl-float v2, v2, p2

    if-gtz v2, :cond_0

    .line 360
    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    move v2, v0

    .line 354
    goto :goto_1

    :cond_3
    move v2, v0

    .line 358
    goto :goto_2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 331
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Tuple2f;

    move-object v3, v0

    .line 332
    .local v3, "t2":Ljavax/vecmath/Tuple2f;
    iget v5, p0, Ljavax/vecmath/Tuple2f;->x:F

    iget v6, v3, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Tuple2f;->y:F

    iget v6, v3, Ljavax/vecmath/Tuple2f;->y:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 335
    .end local v3    # "t2":Ljavax/vecmath/Tuple2f;
    :cond_0
    :goto_0
    return v4

    .line 334
    :catch_0
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0

    .line 335
    .end local v2    # "e2":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Tuple2f;)Z
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    const/4 v1, 0x0

    .line 315
    :try_start_0
    iget v2, p0, Ljavax/vecmath/Tuple2f;->x:F

    iget v3, p1, Ljavax/vecmath/Tuple2f;->x:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Tuple2f;->y:F

    iget v3, p1, Ljavax/vecmath/Tuple2f;->y:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 317
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get([F)V
    .locals 2
    .param p1, "t"    # [F

    .prologue
    .line 163
    const/4 v0, 0x0

    iget v1, p0, Ljavax/vecmath/Tuple2f;->x:F

    aput v1, p1, v0

    .line 164
    const/4 v0, 0x1

    iget v1, p0, Ljavax/vecmath/Tuple2f;->y:F

    aput v1, p1, v0

    .line 165
    return-void
.end method

.method public final getX()F
    .locals 1

    .prologue
    .line 571
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    return v0
.end method

.method public final getY()F
    .locals 1

    .prologue
    .line 595
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 299
    const-wide/16 v0, 0x1

    .line 300
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple2f;->x:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 301
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Tuple2f;->y:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 302
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final interpolate(Ljavax/vecmath/Tuple2f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p2, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 539
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 540
    sub-float v0, v2, p2

    iget v1, p0, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 542
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Tuple2f;Ljavax/vecmath/Tuple2f;F)V
    .locals 3
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2f;
    .param p3, "alpha"    # F

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 524
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 525
    sub-float v0, v2, p3

    iget v1, p1, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, v1

    iget v1, p2, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v1, p3

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 527
    return-void
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 233
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 234
    return-void
.end method

.method public final negate(Ljavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 222
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 223
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 224
    return-void
.end method

.method public final scale(F)V
    .locals 1
    .param p1, "s"    # F

    .prologue
    .line 257
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 258
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 259
    return-void
.end method

.method public final scale(FLjavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 245
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 246
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 247
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 284
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple2f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 285
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, p1

    iget v1, p2, Ljavax/vecmath/Tuple2f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 286
    return-void
.end method

.method public final scaleAdd(FLjavax/vecmath/Tuple2f;Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "s"    # F
    .param p2, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p3, "t2"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 271
    iget v0, p2, Ljavax/vecmath/Tuple2f;->x:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple2f;->x:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 272
    iget v0, p2, Ljavax/vecmath/Tuple2f;->y:F

    mul-float/2addr v0, p1

    iget v1, p3, Ljavax/vecmath/Tuple2f;->y:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 273
    return-void
.end method

.method public final set(FF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 118
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 119
    iput p2, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 120
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 152
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 153
    iget-wide v0, p1, Ljavax/vecmath/Tuple2d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 154
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple2f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 141
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 142
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 143
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "t"    # [F

    .prologue
    .line 130
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 131
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 132
    return-void
.end method

.method public final setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 583
    iput p1, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 584
    return-void
.end method

.method public final setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 607
    iput p1, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 608
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 211
    iget v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple2f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 212
    iget v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    iget v1, p1, Ljavax/vecmath/Tuple2f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 213
    return-void
.end method

.method public final sub(Ljavax/vecmath/Tuple2f;Ljavax/vecmath/Tuple2f;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;
    .param p2, "t2"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 199
    iget v0, p1, Ljavax/vecmath/Tuple2f;->x:F

    iget v1, p2, Ljavax/vecmath/Tuple2f;->x:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->x:F

    .line 200
    iget v0, p1, Ljavax/vecmath/Tuple2f;->y:F

    iget v1, p2, Ljavax/vecmath/Tuple2f;->y:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Tuple2f;->y:F

    .line 201
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 370
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple2f;->x:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Tuple2f;->y:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
