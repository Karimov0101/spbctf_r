.class public Ljavax/vecmath/Vector4d;
.super Ljavax/vecmath/Tuple4d;
.source "Vector4d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x36a70663a02a5ffcL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 125
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "w"    # D

    .prologue
    .line 55
    invoke-direct/range {p0 .. p8}, Ljavax/vecmath/Tuple4d;-><init>(DDDD)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3d;)V
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 115
    iget-wide v2, p1, Ljavax/vecmath/Tuple3d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Tuple3d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple3d;->z:D

    const-wide/16 v8, 0x0

    move-object v1, p0

    invoke-direct/range {v1 .. v9}, Ljavax/vecmath/Tuple4d;-><init>(DDDD)V

    .line 116
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 93
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector4d;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 75
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Vector4f;)V
    .locals 0
    .param p1, "v1"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 84
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .param p1, "v"    # [D

    .prologue
    .line 65
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>([D)V

    .line 66
    return-void
.end method


# virtual methods
.method public final angle(Ljavax/vecmath/Vector4d;)D
    .locals 8
    .param p1, "v1"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 218
    invoke-virtual {p0, p1}, Ljavax/vecmath/Vector4d;->dot(Ljavax/vecmath/Vector4d;)D

    move-result-wide v2

    invoke-virtual {p0}, Ljavax/vecmath/Vector4d;->length()D

    move-result-wide v4

    invoke-virtual {p1}, Ljavax/vecmath/Vector4d;->length()D

    move-result-wide v6

    mul-double/2addr v4, v6

    div-double v0, v2, v4

    .line 219
    .local v0, "vDot":D
    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    cmpg-double v2, v0, v2

    if-gez v2, :cond_0

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    .line 220
    :cond_0
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v2, v0, v2

    if-lez v2, :cond_1

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 221
    :cond_1
    invoke-static {v0, v1}, Ljava/lang/Math;->acos(D)D

    move-result-wide v2

    return-wide v2
.end method

.method public final dot(Ljavax/vecmath/Vector4d;)D
    .locals 6
    .param p1, "v1"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 173
    iget-wide v0, p0, Ljavax/vecmath/Vector4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    iget-wide v4, p1, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final length()D
    .locals 6

    .prologue
    .line 150
    iget-wide v0, p0, Ljavax/vecmath/Vector4d;->x:D

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public final lengthSquared()D
    .locals 6

    .prologue
    .line 161
    iget-wide v0, p0, Ljavax/vecmath/Vector4d;->x:D

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final normalize()V
    .locals 10

    .prologue
    .line 200
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, Ljavax/vecmath/Vector4d;->x:D

    iget-wide v6, p0, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Vector4d;->y:D

    iget-wide v8, p0, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Vector4d;->z:D

    iget-wide v8, p0, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Vector4d;->w:D

    iget-wide v8, p0, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 202
    .local v0, "norm":D
    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->x:D

    .line 203
    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    .line 204
    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    .line 205
    iget-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    .line 206
    return-void
.end method

.method public final normalize(Ljavax/vecmath/Vector4d;)V
    .locals 10
    .param p1, "v1"    # Ljavax/vecmath/Vector4d;

    .prologue
    .line 185
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Vector4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Vector4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Vector4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Vector4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 186
    .local v0, "norm":D
    iget-wide v2, p1, Ljavax/vecmath/Vector4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->x:D

    .line 187
    iget-wide v2, p1, Ljavax/vecmath/Vector4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->y:D

    .line 188
    iget-wide v2, p1, Ljavax/vecmath/Vector4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->z:D

    .line 189
    iget-wide v2, p1, Ljavax/vecmath/Vector4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Vector4d;->w:D

    .line 190
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3d;)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Tuple3d;

    .prologue
    .line 137
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->x:D

    iput-wide v0, p0, Ljavax/vecmath/Vector4d;->x:D

    .line 138
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->y:D

    iput-wide v0, p0, Ljavax/vecmath/Vector4d;->y:D

    .line 139
    iget-wide v0, p1, Ljavax/vecmath/Tuple3d;->z:D

    iput-wide v0, p0, Ljavax/vecmath/Vector4d;->z:D

    .line 140
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljavax/vecmath/Vector4d;->w:D

    .line 141
    return-void
.end method
