.class public Ljavax/vecmath/Point2d;
.super Ljavax/vecmath/Tuple2d;
.source "Point2d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0xfbbe284dd717332L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljavax/vecmath/Tuple2d;-><init>()V

    .line 113
    return-void
.end method

.method public constructor <init>(DD)V
    .locals 1
    .param p1, "x"    # D
    .param p3, "y"    # D

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple2d;-><init>(DD)V

    .line 54
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point2d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 74
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point2f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point2f;

    .prologue
    .line 83
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 84
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2d;

    .prologue
    .line 93
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2d;)V

    .line 94
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple2f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple2f;

    .prologue
    .line 103
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>(Ljavax/vecmath/Tuple2f;)V

    .line 104
    return-void
.end method

.method public constructor <init>([D)V
    .locals 0
    .param p1, "p"    # [D

    .prologue
    .line 63
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple2d;-><init>([D)V

    .line 64
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point2d;)D
    .locals 8
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 136
    iget-wide v4, p0, Ljavax/vecmath/Point2d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Point2d;->x:D

    sub-double v0, v4, v6

    .line 137
    .local v0, "dx":D
    iget-wide v4, p0, Ljavax/vecmath/Point2d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Point2d;->y:D

    sub-double v2, v4, v6

    .line 138
    .local v2, "dy":D
    mul-double v4, v0, v0

    mul-double v6, v2, v2

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    return-wide v4
.end method

.method public final distanceL1(Ljavax/vecmath/Point2d;)D
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 149
    iget-wide v0, p0, Ljavax/vecmath/Point2d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Point2d;->x:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Ljavax/vecmath/Point2d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Point2d;->y:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    add-double/2addr v0, v2

    return-wide v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point2d;)D
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 160
    iget-wide v0, p0, Ljavax/vecmath/Point2d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Point2d;->x:D

    sub-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v0

    iget-wide v2, p0, Ljavax/vecmath/Point2d;->y:D

    iget-wide v4, p1, Ljavax/vecmath/Point2d;->y:D

    sub-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0
.end method

.method public final distanceSquared(Ljavax/vecmath/Point2d;)D
    .locals 8
    .param p1, "p1"    # Ljavax/vecmath/Point2d;

    .prologue
    .line 123
    iget-wide v4, p0, Ljavax/vecmath/Point2d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Point2d;->x:D

    sub-double v0, v4, v6

    .line 124
    .local v0, "dx":D
    iget-wide v4, p0, Ljavax/vecmath/Point2d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Point2d;->y:D

    sub-double v2, v4, v6

    .line 125
    .local v2, "dy":D
    mul-double v4, v0, v0

    mul-double v6, v2, v2

    add-double/2addr v4, v6

    return-wide v4
.end method
