.class public Ljavax/vecmath/Point3i;
.super Ljavax/vecmath/Tuple3i;
.source "Point3i.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x5556a9d5e486ba41L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljavax/vecmath/Tuple3i;-><init>()V

    .line 83
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "z"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Ljavax/vecmath/Tuple3i;-><init>(III)V

    .line 56
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3i;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple3i;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3i;-><init>(Ljavax/vecmath/Tuple3i;)V

    .line 75
    return-void
.end method

.method public constructor <init>([I)V
    .locals 0
    .param p1, "t"    # [I

    .prologue
    .line 64
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple3i;-><init>([I)V

    .line 65
    return-void
.end method
