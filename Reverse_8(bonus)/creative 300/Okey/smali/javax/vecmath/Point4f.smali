.class public Ljavax/vecmath/Point4f;
.super Ljavax/vecmath/Tuple4f;
.source "Point4f.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = 0x406fb9ddd0e3b06bL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljavax/vecmath/Tuple4f;-><init>()V

    .line 130
    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F
    .param p4, "w"    # F

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 57
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point4d;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point4d;

    .prologue
    .line 86
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Point4f;)V
    .locals 0
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple3f;)V
    .locals 4
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 120
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iget v1, p1, Ljavax/vecmath/Tuple3f;->y:F

    iget v2, p1, Ljavax/vecmath/Tuple3f;->z:F

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple4f;-><init>(FFFF)V

    .line 121
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 106
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 107
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 96
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 97
    return-void
.end method

.method public constructor <init>([F)V
    .locals 0
    .param p1, "p"    # [F

    .prologue
    .line 66
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4f;-><init>([F)V

    .line 67
    return-void
.end method


# virtual methods
.method public final distance(Ljavax/vecmath/Point4f;)F
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 175
    iget v4, p0, Ljavax/vecmath/Point4f;->x:F

    iget v5, p1, Ljavax/vecmath/Point4f;->x:F

    sub-float v1, v4, v5

    .line 176
    .local v1, "dx":F
    iget v4, p0, Ljavax/vecmath/Point4f;->y:F

    iget v5, p1, Ljavax/vecmath/Point4f;->y:F

    sub-float v2, v4, v5

    .line 177
    .local v2, "dy":F
    iget v4, p0, Ljavax/vecmath/Point4f;->z:F

    iget v5, p1, Ljavax/vecmath/Point4f;->z:F

    sub-float v3, v4, v5

    .line 178
    .local v3, "dz":F
    iget v4, p0, Ljavax/vecmath/Point4f;->w:F

    iget v5, p1, Ljavax/vecmath/Point4f;->w:F

    sub-float v0, v4, v5

    .line 179
    .local v0, "dw":F
    mul-float v4, v1, v1

    mul-float v5, v2, v2

    add-float/2addr v4, v5

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    mul-float v5, v0, v0

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    return v4
.end method

.method public final distanceL1(Ljavax/vecmath/Point4f;)F
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 192
    iget v0, p0, Ljavax/vecmath/Point4f;->x:F

    iget v1, p1, Ljavax/vecmath/Point4f;->x:F

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Ljavax/vecmath/Point4f;->y:F

    iget v2, p1, Ljavax/vecmath/Point4f;->y:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Point4f;->z:F

    iget v2, p1, Ljavax/vecmath/Point4f;->z:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    iget v1, p0, Ljavax/vecmath/Point4f;->w:F

    iget v2, p1, Ljavax/vecmath/Point4f;->w:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    add-float/2addr v0, v1

    return v0
.end method

.method public final distanceLinf(Ljavax/vecmath/Point4f;)F
    .locals 5
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 206
    iget v2, p0, Ljavax/vecmath/Point4f;->x:F

    iget v3, p1, Ljavax/vecmath/Point4f;->x:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Ljavax/vecmath/Point4f;->y:F

    iget v4, p1, Ljavax/vecmath/Point4f;->y:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 207
    .local v0, "t1":F
    iget v2, p0, Ljavax/vecmath/Point4f;->z:F

    iget v3, p1, Ljavax/vecmath/Point4f;->z:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Ljavax/vecmath/Point4f;->w:F

    iget v4, p1, Ljavax/vecmath/Point4f;->w:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 209
    .local v1, "t2":F
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v2

    return v2
.end method

.method public final distanceSquared(Ljavax/vecmath/Point4f;)F
    .locals 6
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    .line 158
    iget v4, p0, Ljavax/vecmath/Point4f;->x:F

    iget v5, p1, Ljavax/vecmath/Point4f;->x:F

    sub-float v1, v4, v5

    .line 159
    .local v1, "dx":F
    iget v4, p0, Ljavax/vecmath/Point4f;->y:F

    iget v5, p1, Ljavax/vecmath/Point4f;->y:F

    sub-float v2, v4, v5

    .line 160
    .local v2, "dy":F
    iget v4, p0, Ljavax/vecmath/Point4f;->z:F

    iget v5, p1, Ljavax/vecmath/Point4f;->z:F

    sub-float v3, v4, v5

    .line 161
    .local v3, "dz":F
    iget v4, p0, Ljavax/vecmath/Point4f;->w:F

    iget v5, p1, Ljavax/vecmath/Point4f;->w:F

    sub-float v0, v4, v5

    .line 162
    .local v0, "dw":F
    mul-float v4, v1, v1

    mul-float v5, v2, v2

    add-float/2addr v4, v5

    mul-float v5, v3, v3

    add-float/2addr v4, v5

    mul-float v5, v0, v0

    add-float/2addr v4, v5

    return v4
.end method

.method public final project(Ljavax/vecmath/Point4f;)V
    .locals 3
    .param p1, "p1"    # Ljavax/vecmath/Point4f;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 223
    iget v1, p1, Ljavax/vecmath/Point4f;->w:F

    div-float v0, v2, v1

    .line 224
    .local v0, "oneOw":F
    iget v1, p1, Ljavax/vecmath/Point4f;->x:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point4f;->x:F

    .line 225
    iget v1, p1, Ljavax/vecmath/Point4f;->y:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point4f;->y:F

    .line 226
    iget v1, p1, Ljavax/vecmath/Point4f;->z:F

    mul-float/2addr v1, v0

    iput v1, p0, Ljavax/vecmath/Point4f;->z:F

    .line 227
    iput v2, p0, Ljavax/vecmath/Point4f;->w:F

    .line 229
    return-void
.end method

.method public final set(Ljavax/vecmath/Tuple3f;)V
    .locals 1
    .param p1, "t1"    # Ljavax/vecmath/Tuple3f;

    .prologue
    .line 142
    iget v0, p1, Ljavax/vecmath/Tuple3f;->x:F

    iput v0, p0, Ljavax/vecmath/Point4f;->x:F

    .line 143
    iget v0, p1, Ljavax/vecmath/Tuple3f;->y:F

    iput v0, p0, Ljavax/vecmath/Point4f;->y:F

    .line 144
    iget v0, p1, Ljavax/vecmath/Tuple3f;->z:F

    iput v0, p0, Ljavax/vecmath/Point4f;->z:F

    .line 145
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Point4f;->w:F

    .line 146
    return-void
.end method
