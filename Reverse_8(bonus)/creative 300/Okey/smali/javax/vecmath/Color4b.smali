.class public Ljavax/vecmath/Color4b;
.super Ljavax/vecmath/Tuple4b;
.source "Color4b.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final serialVersionUID:J = -0x175523a9f9cb28bL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljavax/vecmath/Tuple4b;-><init>()V

    .line 122
    return-void
.end method

.method public constructor <init>(BBBB)V
    .locals 0
    .param p1, "b1"    # B
    .param p2, "b2"    # B
    .param p3, "b3"    # B
    .param p4, "b4"    # B

    .prologue
    .line 65
    invoke-direct {p0, p1, p2, p3, p4}, Ljavax/vecmath/Tuple4b;-><init>(BBBB)V

    .line 66
    return-void
.end method

.method public constructor <init>(Ljava/awt/Color;)V
    .locals 4
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    .line 110
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-byte v0, v0

    .line 111
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v1

    int-to-byte v1, v1

    .line 112
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v2

    int-to-byte v2, v2

    .line 113
    invoke-virtual {p1}, Ljava/awt/Color;->getAlpha()I

    move-result v3

    int-to-byte v3, v3

    .line 110
    invoke-direct {p0, v0, v1, v2, v3}, Ljavax/vecmath/Tuple4b;-><init>(BBBB)V

    .line 114
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Color4b;)V
    .locals 0
    .param p1, "c1"    # Ljavax/vecmath/Color4b;

    .prologue
    .line 84
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4b;-><init>(Ljavax/vecmath/Tuple4b;)V

    .line 85
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4b;)V
    .locals 0
    .param p1, "t1"    # Ljavax/vecmath/Tuple4b;

    .prologue
    .line 94
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4b;-><init>(Ljavax/vecmath/Tuple4b;)V

    .line 95
    return-void
.end method

.method public constructor <init>([B)V
    .locals 0
    .param p1, "c"    # [B

    .prologue
    .line 74
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4b;-><init>([B)V

    .line 75
    return-void
.end method


# virtual methods
.method public final get()Ljava/awt/Color;
    .locals 5

    .prologue
    .line 152
    iget-byte v4, p0, Ljavax/vecmath/Color4b;->x:B

    and-int/lit16 v3, v4, 0xff

    .line 153
    .local v3, "r":I
    iget-byte v4, p0, Ljavax/vecmath/Color4b;->y:B

    and-int/lit16 v2, v4, 0xff

    .line 154
    .local v2, "g":I
    iget-byte v4, p0, Ljavax/vecmath/Color4b;->z:B

    and-int/lit16 v1, v4, 0xff

    .line 155
    .local v1, "b":I
    iget-byte v4, p0, Ljavax/vecmath/Color4b;->w:B

    and-int/lit16 v0, v4, 0xff

    .line 157
    .local v0, "a":I
    new-instance v4, Ljava/awt/Color;

    invoke-direct {v4, v3, v2, v1, v0}, Ljava/awt/Color;-><init>(IIII)V

    return-object v4
.end method

.method public final set(Ljava/awt/Color;)V
    .locals 1
    .param p1, "color"    # Ljava/awt/Color;

    .prologue
    .line 136
    invoke-virtual {p1}, Ljava/awt/Color;->getRed()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color4b;->x:B

    .line 137
    invoke-virtual {p1}, Ljava/awt/Color;->getGreen()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color4b;->y:B

    .line 138
    invoke-virtual {p1}, Ljava/awt/Color;->getBlue()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color4b;->z:B

    .line 139
    invoke-virtual {p1}, Ljava/awt/Color;->getAlpha()I

    move-result v0

    int-to-byte v0, v0

    iput-byte v0, p0, Ljavax/vecmath/Color4b;->w:B

    .line 140
    return-void
.end method
