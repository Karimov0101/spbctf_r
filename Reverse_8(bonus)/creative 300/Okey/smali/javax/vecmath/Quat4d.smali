.class public Ljavax/vecmath/Quat4d;
.super Ljavax/vecmath/Tuple4d;
.source "Quat4d.java"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field static final EPS:D = 1.0E-12

.field static final EPS2:D = 1.0E-30

.field static final PIO2:D = 1.57079632679

.field static final serialVersionUID:J = 0x69289dcfc9cb668bL


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 138
    return-void
.end method

.method public constructor <init>(DDDD)V
    .locals 9
    .param p1, "x"    # D
    .param p3, "y"    # D
    .param p5, "z"    # D
    .param p7, "w"    # D

    .prologue
    .line 57
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 59
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    mul-double v4, p1, p1

    mul-double v6, p3, p3

    add-double/2addr v4, v6

    mul-double v6, p5, p5

    add-double/2addr v4, v6

    mul-double v6, p7, p7

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 60
    .local v0, "mag":D
    mul-double v2, p1, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 61
    mul-double v2, p3, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 62
    mul-double v2, p5, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 63
    mul-double v2, p7, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 65
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4d;)V
    .locals 0
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4d;)V

    .line 89
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4f;)V
    .locals 0
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 97
    invoke-direct {p0, p1}, Ljavax/vecmath/Tuple4d;-><init>(Ljavax/vecmath/Tuple4f;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4d;)V
    .locals 10
    .param p1, "t1"    # Ljavax/vecmath/Tuple4d;

    .prologue
    .line 122
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 124
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Tuple4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Tuple4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 125
    .local v0, "mag":D
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 126
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 127
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 128
    iget-wide v2, p1, Ljavax/vecmath/Tuple4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 129
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Tuple4f;)V
    .locals 7
    .param p1, "t1"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 106
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 108
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    iget v6, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    iget v6, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    iget v6, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 109
    .local v0, "mag":D
    iget v2, p1, Ljavax/vecmath/Tuple4f;->x:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 110
    iget v2, p1, Ljavax/vecmath/Tuple4f;->y:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 111
    iget v2, p1, Ljavax/vecmath/Tuple4f;->z:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 112
    iget v2, p1, Ljavax/vecmath/Tuple4f;->w:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 114
    return-void
.end method

.method public constructor <init>([D)V
    .locals 14
    .param p1, "q"    # [D

    .prologue
    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 72
    invoke-direct {p0}, Ljavax/vecmath/Tuple4d;-><init>()V

    .line 74
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    aget-wide v4, p1, v10

    aget-wide v6, p1, v10

    mul-double/2addr v4, v6

    aget-wide v6, p1, v11

    aget-wide v8, p1, v11

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aget-wide v6, p1, v12

    aget-wide v8, p1, v12

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aget-wide v6, p1, v13

    aget-wide v8, p1, v13

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 75
    .local v0, "mag":D
    aget-wide v2, p1, v10

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 76
    aget-wide v2, p1, v11

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 77
    aget-wide v2, p1, v12

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 78
    aget-wide v2, p1, v13

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 80
    return-void
.end method


# virtual methods
.method public final conjugate()V
    .locals 2

    .prologue
    .line 160
    iget-wide v0, p0, Ljavax/vecmath/Quat4d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 161
    iget-wide v0, p0, Ljavax/vecmath/Quat4d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 162
    iget-wide v0, p0, Ljavax/vecmath/Quat4d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 163
    return-void
.end method

.method public final conjugate(Ljavax/vecmath/Quat4d;)V
    .locals 2
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 147
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 148
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 149
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->z:D

    neg-double v0, v0

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 150
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->w:D

    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 151
    return-void
.end method

.method public final interpolate(Ljavax/vecmath/Quat4d;D)V
    .locals 18
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "alpha"    # D

    .prologue
    .line 619
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->y:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->y:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->z:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->z:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->w:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->w:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double v2, v12, v14

    .line 621
    .local v2, "dot":D
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_0

    .line 623
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    .line 624
    neg-double v2, v2

    .line 627
    :cond_0
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v2

    const-wide v14, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v12, v12, v14

    if-lez v12, :cond_1

    .line 628
    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    .line 629
    .local v4, "om":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 630
    .local v10, "sinom":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v12, p2

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v6, v12, v10

    .line 631
    .local v6, "s1":D
    mul-double v12, p2, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v8, v12, v10

    .line 637
    .end local v4    # "om":D
    .end local v10    # "sinom":D
    .local v8, "s2":D
    :goto_0
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    .line 638
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    .line 639
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    .line 640
    move-object/from16 v0, p0

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    .line 641
    return-void

    .line 633
    .end local v6    # "s1":D
    .end local v8    # "s2":D
    :cond_1
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v6, v12, p2

    .line 634
    .restart local v6    # "s1":D
    move-wide/from16 v8, p2

    .restart local v8    # "s2":D
    goto :goto_0
.end method

.method public final interpolate(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Quat4d;D)V
    .locals 19
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "q2"    # Ljavax/vecmath/Quat4d;
    .param p3, "alpha"    # D

    .prologue
    .line 659
    move-object/from16 v0, p2

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    move-object/from16 v0, p1

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v12, v14

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->y:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->y:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->z:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->z:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double/2addr v12, v14

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->w:D

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/Quat4d;->w:D

    move-wide/from16 v16, v0

    mul-double v14, v14, v16

    add-double v2, v12, v14

    .line 661
    .local v2, "dot":D
    const-wide/16 v12, 0x0

    cmpg-double v12, v2, v12

    if-gez v12, :cond_0

    .line 663
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    neg-double v12, v12

    move-object/from16 v0, p1

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    .line 664
    neg-double v2, v2

    .line 667
    :cond_0
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v12, v2

    const-wide v14, 0x3d719799812dea11L    # 1.0E-12

    cmpl-double v12, v12, v14

    if-lez v12, :cond_1

    .line 668
    invoke-static {v2, v3}, Ljava/lang/Math;->acos(D)D

    move-result-wide v4

    .line 669
    .local v4, "om":D
    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    .line 670
    .local v10, "sinom":D
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v12, v12, p3

    mul-double/2addr v12, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v6, v12, v10

    .line 671
    .local v6, "s1":D
    mul-double v12, p3, v4

    invoke-static {v12, v13}, Ljava/lang/Math;->sin(D)D

    move-result-wide v12

    div-double v8, v12, v10

    .line 676
    .end local v4    # "om":D
    .end local v10    # "sinom":D
    .local v8, "s2":D
    :goto_0
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->w:D

    .line 677
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->x:D

    .line 678
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->y:D

    .line 679
    move-object/from16 v0, p1

    iget-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v12, v6

    move-object/from16 v0, p2

    iget-wide v14, v0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v14, v8

    add-double/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Ljavax/vecmath/Quat4d;->z:D

    .line 680
    return-void

    .line 673
    .end local v6    # "s1":D
    .end local v8    # "s2":D
    :cond_1
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    sub-double v6, v12, p3

    .line 674
    .restart local v6    # "s1":D
    move-wide/from16 v8, p3

    .restart local v8    # "s2":D
    goto :goto_0
.end method

.method public final inverse()V
    .locals 10

    .prologue
    .line 268
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    div-double v0, v2, v4

    .line 269
    .local v0, "norm":D
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 270
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    neg-double v4, v0

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 271
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    neg-double v4, v0

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 272
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    neg-double v4, v0

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 273
    return-void
.end method

.method public final inverse(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 253
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    div-double v0, v2, v4

    .line 254
    .local v0, "norm":D
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 255
    neg-double v2, v0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 256
    neg-double v2, v0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 257
    neg-double v2, v0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 258
    return-void
.end method

.method public final mul(Ljavax/vecmath/Quat4d;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 203
    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double v0, v6, v8

    .line 204
    .local v0, "w":D
    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double v2, v6, v8

    .line 205
    .local v2, "x":D
    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    .line 206
    .local v4, "y":D
    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 207
    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 208
    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 209
    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 210
    return-void
.end method

.method public final mul(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Quat4d;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "q2"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 175
    if-eq p0, p1, :cond_0

    if-eq p0, p2, :cond_0

    .line 176
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 177
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 178
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 179
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 191
    :goto_0
    return-void

    .line 183
    :cond_0
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double v0, v6, v8

    .line 184
    .local v0, "w":D
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    sub-double v2, v6, v8

    .line 185
    .local v2, "x":D
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    add-double v4, v6, v8

    .line 186
    .local v4, "y":D
    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    iget-wide v8, p2, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v10, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v10, p2, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v8, v10

    sub-double/2addr v6, v8

    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 187
    iput-wide v0, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 188
    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 189
    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    goto :goto_0
.end method

.method public final mulInverse(Ljavax/vecmath/Quat4d;)V
    .locals 1
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 238
    new-instance v0, Ljavax/vecmath/Quat4d;

    invoke-direct {v0, p1}, Ljavax/vecmath/Quat4d;-><init>(Ljavax/vecmath/Quat4d;)V

    .line 240
    .local v0, "tempQuat":Ljavax/vecmath/Quat4d;
    invoke-virtual {v0}, Ljavax/vecmath/Quat4d;->inverse()V

    .line 241
    invoke-virtual {p0, v0}, Ljavax/vecmath/Quat4d;->mul(Ljavax/vecmath/Quat4d;)V

    .line 242
    return-void
.end method

.method public final mulInverse(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Quat4d;)V
    .locals 1
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "q2"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 222
    new-instance v0, Ljavax/vecmath/Quat4d;

    invoke-direct {v0, p2}, Ljavax/vecmath/Quat4d;-><init>(Ljavax/vecmath/Quat4d;)V

    .line 224
    .local v0, "tempQuat":Ljavax/vecmath/Quat4d;
    invoke-virtual {v0}, Ljavax/vecmath/Quat4d;->inverse()V

    .line 225
    invoke-virtual {p0, p1, v0}, Ljavax/vecmath/Quat4d;->mul(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Quat4d;)V

    .line 226
    return-void
.end method

.method public final normalize()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 309
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v4, v6

    add-double v0, v2, v4

    .line 311
    .local v0, "norm":D
    cmpl-double v2, v0, v8

    if-lez v2, :cond_0

    .line 312
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 313
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 314
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 315
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 316
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 323
    :goto_0
    return-void

    .line 318
    :cond_0
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 319
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 320
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 321
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->w:D

    goto :goto_0
.end method

.method public final normalize(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/16 v8, 0x0

    .line 285
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v4, v6

    add-double v0, v2, v4

    .line 287
    .local v0, "norm":D
    cmpl-double v2, v0, v8

    if-lez v2, :cond_0

    .line 288
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    div-double v0, v2, v4

    .line 289
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 290
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 291
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 292
    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 299
    :goto_0
    return-void

    .line 294
    :cond_0
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 295
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 296
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 297
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->w:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 14
    .param p1, "a"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const-wide/16 v10, 0x0

    .line 586
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    iget-wide v8, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    iget-wide v8, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 587
    .local v0, "amag":D
    const-wide v4, 0x3d719799812dea11L    # 1.0E-12

    cmpg-double v4, v0, v4

    if-gez v4, :cond_0

    .line 588
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 589
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 590
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 591
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 601
    :goto_0
    return-void

    .line 593
    :cond_0
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, v0

    .line 594
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    div-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 595
    .local v2, "mag":D
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->angle:D

    div-double/2addr v4, v12

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 596
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->x:D

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 597
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->y:D

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 598
    iget-wide v4, p1, Ljavax/vecmath/AxisAngle4d;->z:D

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 12
    .param p1, "a"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    const-wide/16 v8, 0x0

    .line 559
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v5, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v6, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v6, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    .line 560
    .local v0, "amag":D
    const-wide v4, 0x3d719799812dea11L    # 1.0E-12

    cmpg-double v4, v0, v4

    if-gez v4, :cond_0

    .line 561
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 562
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 563
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 564
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 574
    :goto_0
    return-void

    .line 566
    :cond_0
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    .line 567
    .local v2, "mag":D
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v0, v4, v0

    .line 568
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v4, v4

    div-double/2addr v4, v10

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 569
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 570
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 571
    iget v4, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    float-to-double v4, v4

    mul-double/2addr v4, v0

    mul-double/2addr v4, v2

    iput-wide v4, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    .line 500
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m11:D

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 502
    .local v0, "ww":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 503
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_1

    .line 504
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 505
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    div-double v0, v2, v4

    .line 506
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m12:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 507
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 508
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 546
    :goto_0
    return-void

    .line 512
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 513
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 514
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 515
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 519
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 520
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 521
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_2

    .line 522
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_3

    .line 523
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 524
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->x:D

    div-double v0, v2, v4

    .line 525
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 526
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 530
    :cond_2
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 531
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 532
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 536
    :cond_3
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 537
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-wide v6, p1, Ljavax/vecmath/Matrix3d;->m22:D

    sub-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 538
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_4

    .line 539
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 540
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 544
    :cond_4
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 545
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const-wide/high16 v12, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/16 v8, 0x0

    .line 445
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m11:F

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v2, v3

    float-to-double v2, v2

    add-double/2addr v2, v10

    mul-double v0, v4, v2

    .line 447
    .local v0, "ww":D
    cmpl-double v2, v0, v8

    if-ltz v2, :cond_0

    .line 448
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_1

    .line 449
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 450
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    div-double v0, v4, v2

    .line 451
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m12:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 452
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m20:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 453
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m01:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 490
    :goto_0
    return-void

    .line 457
    :cond_0
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 458
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 459
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 460
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 464
    :cond_1
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 465
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget v4, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v5, p1, Ljavax/vecmath/Matrix3f;->m22:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v0, v2, v4

    .line 466
    cmpl-double v2, v0, v8

    if-ltz v2, :cond_2

    .line 467
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_3

    .line 468
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 469
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    div-double v0, v12, v2

    .line 470
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 471
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 475
    :cond_2
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 476
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 477
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 481
    :cond_3
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 482
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v2, v2

    sub-double v2, v10, v2

    mul-double v0, v12, v2

    .line 483
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_4

    .line 484
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 485
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v2, v2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 488
    :cond_4
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 489
    iput-wide v10, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 389
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m00:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m11:D

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m33:D

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 391
    .local v0, "ww":D
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_0

    .line 392
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_1

    .line 393
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 394
    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->w:D

    div-double v0, v2, v4

    .line 395
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m12:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 396
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m02:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m20:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 397
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m01:D

    sub-double/2addr v2, v4

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 435
    :goto_0
    return-void

    .line 401
    :cond_0
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 402
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 403
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 404
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 408
    :cond_1
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 409
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    add-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 410
    const-wide/16 v2, 0x0

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_2

    .line 411
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_3

    .line 412
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 413
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->x:D

    div-double v0, v2, v4

    .line 414
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m10:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 415
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m20:D

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 419
    :cond_2
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 420
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 421
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 425
    :cond_3
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 426
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    iget-wide v6, p1, Ljavax/vecmath/Matrix4d;->m22:D

    sub-double/2addr v4, v6

    mul-double v0, v2, v4

    .line 427
    const-wide v2, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpl-double v2, v0, v2

    if-ltz v2, :cond_4

    .line 428
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 429
    iget-wide v2, p1, Ljavax/vecmath/Matrix4d;->m21:D

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    div-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 433
    :cond_4
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 434
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    const-wide v10, 0x39b4484bfeebc2a0L    # 1.0E-30

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    .line 333
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double v0, v4, v2

    .line 335
    .local v0, "ww":D
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_0

    .line 336
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_1

    .line 337
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 338
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->w:D

    div-double v0, v4, v2

    .line 339
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m12:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 340
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m20:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 341
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m01:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    .line 379
    :goto_0
    return-void

    .line 345
    :cond_0
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 346
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 347
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 348
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 352
    :cond_1
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->w:D

    .line 353
    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    iget v4, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double v0, v2, v4

    .line 354
    cmpl-double v2, v0, v6

    if-ltz v2, :cond_2

    .line 355
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_3

    .line 356
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 357
    iget-wide v2, p0, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v12

    div-double v0, v8, v2

    .line 358
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 359
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v2, v2

    mul-double/2addr v2, v0

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 363
    :cond_2
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 364
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 365
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 369
    :cond_3
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->x:D

    .line 370
    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    iget v4, p1, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v4, v4

    sub-double v4, v8, v4

    mul-double v0, v2, v4

    .line 371
    cmpl-double v2, v0, v10

    if-ltz v2, :cond_4

    .line 372
    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 373
    iget v2, p1, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v2, v2

    iget-wide v4, p0, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v12

    div-double/2addr v2, v4

    iput-wide v2, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0

    .line 377
    :cond_4
    iput-wide v6, p0, Ljavax/vecmath/Quat4d;->y:D

    .line 378
    iput-wide v8, p0, Ljavax/vecmath/Quat4d;->z:D

    goto :goto_0
.end method
