.class public Ljavax/vecmath/Matrix4f;
.super Ljava/lang/Object;
.source "Matrix4f.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final EPS:D = 1.0E-8

.field static final serialVersionUID:J = -0x74a4afbe0efa23a9L


# instance fields
.field public m00:F

.field public m01:F

.field public m02:F

.field public m03:F

.field public m10:F

.field public m11:F

.field public m12:F

.field public m13:F

.field public m20:F

.field public m21:F

.field public m22:F

.field public m23:F

.field public m30:F

.field public m31:F

.field public m32:F

.field public m33:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 340
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 341
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 342
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 343
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 345
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 346
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 347
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 348
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 350
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 351
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 352
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 353
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 355
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 356
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 357
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 358
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 360
    return-void
.end method

.method public constructor <init>(FFFFFFFFFFFFFFFF)V
    .locals 1
    .param p1, "m00"    # F
    .param p2, "m01"    # F
    .param p3, "m02"    # F
    .param p4, "m03"    # F
    .param p5, "m10"    # F
    .param p6, "m11"    # F
    .param p7, "m12"    # F
    .param p8, "m13"    # F
    .param p9, "m20"    # F
    .param p10, "m21"    # F
    .param p11, "m22"    # F
    .param p12, "m23"    # F
    .param p13, "m30"    # F
    .param p14, "m31"    # F
    .param p15, "m32"    # F
    .param p16, "m33"    # F

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 157
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 158
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 159
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 161
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 162
    iput p6, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 163
    iput p7, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 164
    iput p8, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 166
    iput p9, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 167
    iput p10, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 168
    iput p11, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 169
    iput p12, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 171
    iput p13, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 172
    iput p14, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 173
    move/from16 v0, p15

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 174
    move/from16 v0, p16

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 176
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3f;F)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "s"    # F

    .prologue
    const/4 v1, 0x0

    .line 311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 312
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 313
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 314
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 315
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 317
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 318
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 319
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 320
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 322
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 323
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 324
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 325
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 327
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 328
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 329
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 330
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 332
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 247
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 248
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 249
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 250
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 251
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 253
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 254
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 255
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 256
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 258
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 259
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 260
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 261
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 263
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 264
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 265
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 266
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 268
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 279
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 280
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 281
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 283
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 284
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 285
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 286
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 288
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 289
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 290
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 291
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 293
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 294
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 295
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 296
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 298
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Vector3f;F)V
    .locals 8
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "s"    # F

    .prologue
    .line 217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 219
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 220
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 222
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 223
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->z:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 224
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 226
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 227
    float-to-double v0, p3

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v5, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v4, v5

    iget v5, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    float-to-double v4, v4

    mul-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 228
    float-to-double v0, p3

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    iget v6, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v6, v6

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    mul-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 230
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 231
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 232
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 234
    const/4 v0, 0x0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 235
    const/4 v0, 0x0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 236
    const/4 v0, 0x0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 237
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 239
    return-void
.end method

.method public constructor <init>([F)V
    .locals 1
    .param p1, "v"    # [F

    .prologue
    .line 184
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 186
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 187
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 188
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 190
    const/4 v0, 0x4

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 191
    const/4 v0, 0x5

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 192
    const/4 v0, 0x6

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 193
    const/4 v0, 0x7

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 195
    const/16 v0, 0x8

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 196
    const/16 v0, 0x9

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 197
    const/16 v0, 0xa

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 198
    const/16 v0, 0xb

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 200
    const/16 v0, 0xc

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 201
    const/16 v0, 0xd

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 202
    const/16 v0, 0xe

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 203
    const/16 v0, 0xf

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 205
    return-void
.end method

.method private final getScaleRotate([D[D)V
    .locals 4
    .param p1, "scales"    # [D
    .param p2, "rots"    # [D

    .prologue
    .line 3226
    const/16 v1, 0x9

    new-array v0, v1, [D

    .line 3227
    .local v0, "tmp":[D
    const/4 v1, 0x0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3228
    const/4 v1, 0x1

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3229
    const/4 v1, 0x2

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3231
    const/4 v1, 0x3

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3232
    const/4 v1, 0x4

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3233
    const/4 v1, 0x5

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3235
    const/4 v1, 0x6

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3236
    const/4 v1, 0x7

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3237
    const/16 v1, 0x8

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v2, v2

    aput-wide v2, v0, v1

    .line 3239
    invoke-static {v0, p1, p2}, Ljavax/vecmath/Matrix3d;->compute_svd([D[D[D)V

    .line 3241
    return-void
.end method

.method static luBacksubstitution([D[I[D)V
    .locals 18
    .param p0, "matrix1"    # [D
    .param p1, "row_perm"    # [I
    .param p2, "matrix2"    # [D

    .prologue
    .line 1961
    const/4 v6, 0x0

    .line 1964
    .local v6, "rp":I
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_0
    const/4 v10, 0x4

    if-ge v5, v10, :cond_3

    .line 1966
    move v0, v5

    .line 1967
    .local v0, "cv":I
    const/4 v2, -0x1

    .line 1970
    .local v2, "ii":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v10, 0x4

    if-ge v1, v10, :cond_2

    .line 1973
    add-int v10, v6, v1

    aget v3, p1, v10

    .line 1974
    .local v3, "ip":I
    mul-int/lit8 v10, v3, 0x4

    add-int/2addr v10, v0

    aget-wide v8, p2, v10

    .line 1975
    .local v8, "sum":D
    mul-int/lit8 v10, v3, 0x4

    add-int/2addr v10, v0

    mul-int/lit8 v11, v1, 0x4

    add-int/2addr v11, v0

    aget-wide v12, p2, v11

    aput-wide v12, p2, v10

    .line 1976
    if-ltz v2, :cond_0

    .line 1978
    mul-int/lit8 v7, v1, 0x4

    .line 1979
    .local v7, "rv":I
    move v4, v2

    .local v4, "j":I
    :goto_2
    add-int/lit8 v10, v1, -0x1

    if-gt v4, v10, :cond_1

    .line 1980
    add-int v10, v7, v4

    aget-wide v10, p0, v10

    mul-int/lit8 v12, v4, 0x4

    add-int/2addr v12, v0

    aget-wide v12, p2, v12

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    .line 1979
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 1983
    .end local v4    # "j":I
    .end local v7    # "rv":I
    :cond_0
    const-wide/16 v10, 0x0

    cmpl-double v10, v8, v10

    if-eqz v10, :cond_1

    .line 1984
    move v2, v1

    .line 1986
    :cond_1
    mul-int/lit8 v10, v1, 0x4

    add-int/2addr v10, v0

    aput-wide v8, p2, v10

    .line 1970
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1991
    .end local v3    # "ip":I
    .end local v8    # "sum":D
    :cond_2
    const/16 v7, 0xc

    .line 1992
    .restart local v7    # "rv":I
    add-int/lit8 v10, v0, 0xc

    aget-wide v12, p2, v10

    const/16 v11, 0xf

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1994
    add-int/lit8 v7, v7, -0x4

    .line 1995
    add-int/lit8 v10, v0, 0x8

    add-int/lit8 v11, v0, 0x8

    aget-wide v12, p2, v11

    const/16 v11, 0xb

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/16 v11, 0xa

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1998
    add-int/lit8 v7, v7, -0x4

    .line 1999
    add-int/lit8 v10, v0, 0x4

    add-int/lit8 v11, v0, 0x4

    aget-wide v12, p2, v11

    const/4 v11, 0x6

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x8

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x7

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x5

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 2003
    add-int/lit8 v7, v7, -0x4

    .line 2004
    add-int/lit8 v10, v0, 0x0

    add-int/lit8 v11, v0, 0x0

    aget-wide v12, p2, v11

    const/4 v11, 0x1

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x4

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x2

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0x8

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x3

    aget-wide v14, p0, v11

    add-int/lit8 v11, v0, 0xc

    aget-wide v16, p2, v11

    mul-double v14, v14, v16

    sub-double/2addr v12, v14

    const/4 v11, 0x0

    aget-wide v14, p0, v11

    div-double/2addr v12, v14

    aput-wide v12, p2, v10

    .line 1964
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    .line 2009
    .end local v0    # "cv":I
    .end local v1    # "i":I
    .end local v2    # "ii":I
    .end local v7    # "rv":I
    :cond_3
    return-void
.end method

.method static luDecomposition([D[I)Z
    .locals 30
    .param p0, "matrix0"    # [D
    .param p1, "row_perm"    # [I

    .prologue
    .line 1805
    const/16 v26, 0x4

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 1813
    .local v18, "row_scale":[D
    const/16 v16, 0x0

    .line 1814
    .local v16, "ptr":I
    const/16 v19, 0x0

    .line 1817
    .local v19, "rs":I
    const/4 v4, 0x4

    .local v4, "i":I
    move/from16 v20, v19

    .end local v19    # "rs":I
    .local v20, "rs":I
    move v5, v4

    .line 1818
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_2

    .line 1819
    const-wide/16 v2, 0x0

    .line 1822
    .local v2, "big":D
    const/4 v7, 0x4

    .local v7, "j":I
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .local v17, "ptr":I
    move v8, v7

    .line 1823
    .end local v7    # "j":I
    .local v8, "j":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "j":I
    .restart local v7    # "j":I
    if-eqz v8, :cond_0

    .line 1824
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    aget-wide v24, p0, v17

    .line 1825
    .local v24, "temp":D
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    .line 1826
    cmpl-double v26, v24, v2

    if-lez v26, :cond_e

    .line 1827
    move-wide/from16 v2, v24

    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto :goto_1

    .line 1832
    .end local v8    # "j":I
    .end local v24    # "temp":D
    .restart local v7    # "j":I
    :cond_0
    const-wide/16 v26, 0x0

    cmpl-double v26, v2, v26

    if-nez v26, :cond_1

    .line 1833
    const/16 v26, 0x0

    .line 1931
    .end local v2    # "big":D
    .end local v17    # "ptr":I
    :goto_2
    return v26

    .line 1835
    .restart local v2    # "big":D
    .restart local v17    # "ptr":I
    :cond_1
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "rs":I
    .restart local v19    # "rs":I
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v2

    aput-wide v26, v18, v20

    move/from16 v20, v19

    .end local v19    # "rs":I
    .restart local v20    # "rs":I
    move/from16 v16, v17

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 1843
    .end local v2    # "big":D
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v4    # "i":I
    :cond_2
    const/4 v11, 0x0

    .line 1846
    .local v11, "mtx":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    const/16 v26, 0x4

    move/from16 v0, v26

    if-ge v7, v0, :cond_d

    .line 1852
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_4

    .line 1853
    mul-int/lit8 v26, v4, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1854
    .local v21, "target":I
    aget-wide v22, p0, v21

    .line 1855
    .local v22, "sum":D
    move v9, v4

    .line 1856
    .local v9, "k":I
    mul-int/lit8 v26, v4, 0x4

    add-int v12, v11, v26

    .line 1857
    .local v12, "p1":I
    add-int v14, v11, v7

    .local v14, "p2":I
    move v10, v9

    .line 1858
    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_3

    .line 1859
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1860
    add-int/lit8 v12, v12, 0x1

    .line 1861
    add-int/lit8 v14, v14, 0x4

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 1863
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_3
    aput-wide v22, p0, v21

    .line 1852
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1868
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    :cond_4
    const-wide/16 v2, 0x0

    .line 1869
    .restart local v2    # "big":D
    const/4 v6, -0x1

    .line 1870
    .local v6, "imax":I
    move v4, v7

    :goto_6
    const/16 v26, 0x4

    move/from16 v0, v26

    if-ge v4, v0, :cond_7

    .line 1871
    mul-int/lit8 v26, v4, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1872
    .restart local v21    # "target":I
    aget-wide v22, p0, v21

    .line 1873
    .restart local v22    # "sum":D
    move v9, v7

    .line 1874
    .restart local v9    # "k":I
    mul-int/lit8 v26, v4, 0x4

    add-int v12, v11, v26

    .line 1875
    .restart local v12    # "p1":I
    add-int v14, v11, v7

    .restart local v14    # "p2":I
    move v10, v9

    .line 1876
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_7
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_5

    .line 1877
    aget-wide v26, p0, v12

    aget-wide v28, p0, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1878
    add-int/lit8 v12, v12, 0x1

    .line 1879
    add-int/lit8 v14, v14, 0x4

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_7

    .line 1881
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_5
    aput-wide v22, p0, v21

    .line 1884
    aget-wide v26, v18, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    mul-double v24, v26, v28

    .restart local v24    # "temp":D
    cmpl-double v26, v24, v2

    if-ltz v26, :cond_6

    .line 1885
    move-wide/from16 v2, v24

    .line 1886
    move v6, v4

    .line 1870
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1890
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    .end local v24    # "temp":D
    :cond_7
    if-gez v6, :cond_8

    .line 1891
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "Matrix4f13"

    invoke-static/range {v27 .. v27}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 1895
    :cond_8
    if-eq v7, v6, :cond_a

    .line 1897
    const/4 v9, 0x4

    .line 1898
    .restart local v9    # "k":I
    mul-int/lit8 v26, v6, 0x4

    add-int v12, v11, v26

    .line 1899
    .restart local v12    # "p1":I
    mul-int/lit8 v26, v7, 0x4

    add-int v14, v11, v26

    .restart local v14    # "p2":I
    move v15, v14

    .end local v14    # "p2":I
    .local v15, "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .local v13, "p1":I
    move v10, v9

    .line 1900
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_8
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_9

    .line 1901
    aget-wide v24, p0, v13

    .line 1902
    .restart local v24    # "temp":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "p1":I
    .restart local v12    # "p1":I
    aget-wide v26, p0, v15

    aput-wide v26, p0, v13

    .line 1903
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "p2":I
    .restart local v14    # "p2":I
    aput-wide v24, p0, v15

    move v15, v14

    .end local v14    # "p2":I
    .restart local v15    # "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .restart local v13    # "p1":I
    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_8

    .line 1907
    .end local v10    # "k":I
    .end local v24    # "temp":D
    .restart local v9    # "k":I
    :cond_9
    aget-wide v26, v18, v7

    aput-wide v26, v18, v6

    .line 1911
    .end local v9    # "k":I
    .end local v13    # "p1":I
    .end local v15    # "p2":I
    :cond_a
    aput v6, p1, v7

    .line 1914
    mul-int/lit8 v26, v7, 0x4

    add-int v26, v26, v11

    add-int v26, v26, v7

    aget-wide v26, p0, v26

    const-wide/16 v28, 0x0

    cmpl-double v26, v26, v28

    if-nez v26, :cond_b

    .line 1915
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 1919
    :cond_b
    const/16 v26, 0x3

    move/from16 v0, v26

    if-eq v7, v0, :cond_c

    .line 1920
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-int/lit8 v28, v7, 0x4

    add-int v28, v28, v11

    add-int v28, v28, v7

    aget-wide v28, p0, v28

    div-double v24, v26, v28

    .line 1921
    .restart local v24    # "temp":D
    add-int/lit8 v26, v7, 0x1

    mul-int/lit8 v26, v26, 0x4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1922
    .restart local v21    # "target":I
    rsub-int/lit8 v4, v7, 0x3

    move v5, v4

    .line 1923
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :goto_9
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_c

    .line 1924
    aget-wide v26, p0, v21

    mul-double v26, v26, v24

    aput-wide v26, p0, v21

    .line 1925
    add-int/lit8 v21, v21, 0x4

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_9

    .line 1846
    .end local v5    # "i":I
    .end local v21    # "target":I
    .end local v24    # "temp":D
    .restart local v4    # "i":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 1931
    .end local v2    # "big":D
    .end local v6    # "imax":I
    :cond_d
    const/16 v26, 0x1

    goto/16 :goto_2

    .end local v11    # "mtx":I
    .restart local v2    # "big":D
    .restart local v24    # "temp":D
    :cond_e
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto/16 :goto_1
.end method


# virtual methods
.method public final add(F)V
    .locals 1
    .param p1, "scalar"    # F

    .prologue
    .line 1183
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1184
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1185
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1186
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1187
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1188
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1189
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1190
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1191
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1192
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1193
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1194
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1195
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1196
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1197
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1198
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1199
    return-void
.end method

.method public final add(FLjavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "scalar"    # F
    .param p2, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1209
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m00:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1210
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m01:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1211
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m02:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1212
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m03:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1213
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m10:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1214
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1215
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m12:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1216
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m13:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1217
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m20:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1218
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m21:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1219
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1220
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1221
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m30:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1222
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m31:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1223
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m32:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1224
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1225
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1262
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m00:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1263
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m01:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1264
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m02:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1265
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m03:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1267
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m10:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1268
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1269
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m12:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1270
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m13:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1272
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m20:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1273
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m21:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1274
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1275
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1277
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m30:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1278
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m31:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1279
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m32:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1280
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1281
    return-void
.end method

.method public final add(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1234
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m00:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1235
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m01:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1236
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m02:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1237
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m03:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1239
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m10:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1240
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m11:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1241
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m12:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1242
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m13:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1244
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m20:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1245
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m21:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1246
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m22:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1247
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1249
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m30:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1250
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m31:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1251
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m32:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1252
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m33:F

    add-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1253
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 3253
    const/4 v1, 0x0

    .line 3255
    .local v1, "m1":Ljavax/vecmath/Matrix4f;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "m1":Ljavax/vecmath/Matrix4f;
    check-cast v1, Ljavax/vecmath/Matrix4f;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3261
    .restart local v1    # "m1":Ljavax/vecmath/Matrix4f;
    return-object v1

    .line 3256
    .end local v1    # "m1":Ljavax/vecmath/Matrix4f;
    :catch_0
    move-exception v0

    .line 3258
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v2, Ljava/lang/InternalError;

    invoke-direct {v2}, Ljava/lang/InternalError;-><init>()V

    throw v2
.end method

.method public final determinant()F
    .locals 5

    .prologue
    .line 2021
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float v0, v1, v2

    .line 2023
    .local v0, "det":F
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 2025
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 2027
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 2030
    return v0
.end method

.method public epsilonEquals(Ljavax/vecmath/Matrix4f;F)Z
    .locals 3
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "epsilon"    # F

    .prologue
    .line 2805
    const/4 v0, 0x1

    .line 2807
    .local v0, "status":Z
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m00:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_0

    const/4 v0, 0x0

    .line 2808
    :cond_0
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m01:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_1

    const/4 v0, 0x0

    .line 2809
    :cond_1
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m02:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_2

    const/4 v0, 0x0

    .line 2810
    :cond_2
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m03:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_3

    const/4 v0, 0x0

    .line 2812
    :cond_3
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m10:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_4

    const/4 v0, 0x0

    .line 2813
    :cond_4
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m11:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_5

    const/4 v0, 0x0

    .line 2814
    :cond_5
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m12:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_6

    const/4 v0, 0x0

    .line 2815
    :cond_6
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m13:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_7

    const/4 v0, 0x0

    .line 2817
    :cond_7
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m20:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_8

    const/4 v0, 0x0

    .line 2818
    :cond_8
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m21:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_9

    const/4 v0, 0x0

    .line 2819
    :cond_9
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m22:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_a

    const/4 v0, 0x0

    .line 2820
    :cond_a
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m23:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_b

    const/4 v0, 0x0

    .line 2822
    :cond_b
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m30:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_c

    const/4 v0, 0x0

    .line 2823
    :cond_c
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m31:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_d

    const/4 v0, 0x0

    .line 2824
    :cond_d
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m32:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_e

    const/4 v0, 0x0

    .line 2825
    :cond_e
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v2, p1, Ljavax/vecmath/Matrix4f;->m33:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, p2

    if-lez v1, :cond_f

    const/4 v0, 0x0

    .line 2827
    :cond_f
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "t1"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x0

    .line 2781
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/Matrix4f;

    move-object v3, v0

    .line 2782
    .local v3, "m2":Ljavax/vecmath/Matrix4f;
    iget v5, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m00:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m01:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m02:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m03:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m10:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m11:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m12:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m13:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m20:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m21:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m22:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m23:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m30:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m31:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m32:F

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    iget v5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v6, v3, Ljavax/vecmath/Matrix4f;->m33:F
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-float v5, v5, v6

    if-nez v5, :cond_0

    const/4 v4, 0x1

    .line 2790
    .end local v3    # "m2":Ljavax/vecmath/Matrix4f;
    :cond_0
    :goto_0
    return v4

    .line 2789
    :catch_0
    move-exception v1

    .local v1, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 2790
    .end local v1    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v2

    .local v2, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/Matrix4f;)Z
    .locals 4
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const/4 v1, 0x0

    .line 2760
    :try_start_0
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m00:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m01:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m02:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m03:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m10:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m11:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m12:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m13:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m20:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m21:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m22:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m23:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m30:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m31:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m32:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    iget v2, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m33:F
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    const/4 v1, 0x1

    .line 2767
    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3f;)F
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;

    .prologue
    const/4 v4, 0x3

    .line 796
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 797
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 799
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 801
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 802
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 803
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 805
    aget-wide v2, v0, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 806
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 807
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 809
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 810
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 811
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 813
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iput v2, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 814
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iput v2, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 815
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iput v2, p2, Ljavax/vecmath/Vector3f;->z:F

    .line 817
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public final get(Ljavax/vecmath/Matrix3d;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v4, 0x3

    .line 739
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 740
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 742
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 744
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    .line 745
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    .line 746
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 748
    aget-wide v2, v0, v4

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    .line 749
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    .line 750
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 752
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    .line 753
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    .line 754
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    iput-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    .line 756
    return-void
.end method

.method public final get(Ljavax/vecmath/Matrix3f;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v4, 0x3

    .line 766
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 767
    .local v0, "tmp_rot":[D
    new-array v1, v4, [D

    .line 769
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 771
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 772
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 773
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 775
    aget-wide v2, v0, v4

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 776
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 777
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 779
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 780
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 781
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    double-to-float v2, v2

    iput v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 783
    return-void
.end method

.method public final get(Ljavax/vecmath/Quat4f;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    .line 829
    const/16 v4, 0x9

    new-array v0, v4, [D

    .line 830
    .local v0, "tmp_rot":[D
    const/4 v4, 0x3

    new-array v1, v4, [D

    .line 831
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 835
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v8, 0x0

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/4 v8, 0x4

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 836
    .local v2, "ww":D
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_0

    neg-double v4, v2

    :goto_0
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_1

    .line 837
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->w:F

    .line 838
    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    iget v6, p1, Ljavax/vecmath/Quat4f;->w:F

    float-to-double v6, v6

    div-double v2, v4, v6

    .line 839
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const/4 v6, 0x5

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 840
    const/4 v4, 0x2

    aget-wide v4, v0, v4

    const/4 v6, 0x6

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 841
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    const/4 v6, 0x1

    aget-wide v6, v0, v6

    sub-double/2addr v4, v6

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    .line 866
    :goto_1
    return-void

    :cond_0
    move-wide v4, v2

    .line 836
    goto :goto_0

    .line 845
    :cond_1
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->w:F

    .line 846
    const-wide/high16 v4, -0x4020000000000000L    # -0.5

    const/4 v6, 0x4

    aget-wide v6, v0, v6

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    add-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 847
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_2

    neg-double v4, v2

    :goto_2
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_3

    .line 848
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 849
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    iget v6, p1, Ljavax/vecmath/Quat4f;->x:F

    float-to-double v6, v6

    div-double v2, v4, v6

    .line 850
    const/4 v4, 0x3

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 851
    const/4 v4, 0x6

    aget-wide v4, v0, v4

    mul-double/2addr v4, v2

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1

    :cond_2
    move-wide v4, v2

    .line 847
    goto :goto_2

    .line 855
    :cond_3
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->x:F

    .line 856
    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/16 v8, 0x8

    aget-wide v8, v0, v8

    sub-double/2addr v6, v8

    mul-double v2, v4, v6

    .line 857
    const-wide/16 v4, 0x0

    cmpg-double v4, v2, v4

    if-gez v4, :cond_4

    neg-double v4, v2

    :goto_3
    const-wide v6, 0x39b4484bfeebc2a0L    # 1.0E-30

    cmpg-double v4, v4, v6

    if-ltz v4, :cond_5

    .line 858
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 859
    const/4 v4, 0x7

    aget-wide v4, v0, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    iget v8, p1, Ljavax/vecmath/Quat4f;->y:F

    float-to-double v8, v8

    mul-double/2addr v6, v8

    div-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1

    :cond_4
    move-wide v4, v2

    .line 857
    goto :goto_3

    .line 863
    :cond_5
    const/4 v4, 0x0

    iput v4, p1, Ljavax/vecmath/Quat4f;->y:F

    .line 864
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p1, Ljavax/vecmath/Quat4f;->z:F

    goto :goto_1
.end method

.method public final get(Ljavax/vecmath/Vector3f;)V
    .locals 1
    .param p1, "trans"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 875
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p1, Ljavax/vecmath/Vector3f;->x:F

    .line 876
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p1, Ljavax/vecmath/Vector3f;->y:F

    .line 877
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p1, Ljavax/vecmath/Vector3f;->z:F

    .line 878
    return-void
.end method

.method public final getColumn(ILjavax/vecmath/Vector4f;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 645
    if-nez p1, :cond_0

    .line 646
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 647
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 648
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 649
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    .line 669
    :goto_0
    return-void

    .line 650
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 651
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 652
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 653
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 654
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 655
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 656
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 657
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 658
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 659
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 660
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 661
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 662
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 663
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 664
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 666
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getColumn(I[F)V
    .locals 5
    .param p1, "column"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 678
    if-nez p1, :cond_0

    .line 679
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    aput v0, p2, v4

    .line 680
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    aput v0, p2, v1

    .line 681
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    aput v0, p2, v2

    .line 682
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    aput v0, p2, v3

    .line 702
    :goto_0
    return-void

    .line 683
    :cond_0
    if-ne p1, v1, :cond_1

    .line 684
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    aput v0, p2, v4

    .line 685
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    aput v0, p2, v1

    .line 686
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    aput v0, p2, v2

    .line 687
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    aput v0, p2, v3

    goto :goto_0

    .line 688
    :cond_1
    if-ne p1, v2, :cond_2

    .line 689
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    aput v0, p2, v4

    .line 690
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    aput v0, p2, v1

    .line 691
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    aput v0, p2, v2

    .line 692
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    aput v0, p2, v3

    goto :goto_0

    .line 693
    :cond_2
    if-ne p1, v3, :cond_3

    .line 694
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    aput v0, p2, v4

    .line 695
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    aput v0, p2, v1

    .line 696
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    aput v0, p2, v2

    .line 697
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    aput v0, p2, v3

    goto :goto_0

    .line 699
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f4"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getElement(II)F
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 503
    packed-switch p1, :pswitch_data_0

    .line 571
    :goto_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f1"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 509
    :pswitch_1
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 562
    :goto_1
    return v0

    .line 511
    :pswitch_2
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    goto :goto_1

    .line 513
    :pswitch_3
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    goto :goto_1

    .line 515
    :pswitch_4
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    goto :goto_1

    .line 521
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    goto :goto_0

    .line 524
    :pswitch_6
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    goto :goto_1

    .line 526
    :pswitch_7
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    goto :goto_1

    .line 528
    :pswitch_8
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    goto :goto_1

    .line 530
    :pswitch_9
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    goto :goto_1

    .line 537
    :pswitch_a
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 540
    :pswitch_b
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    goto :goto_1

    .line 542
    :pswitch_c
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    goto :goto_1

    .line 544
    :pswitch_d
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    goto :goto_1

    .line 546
    :pswitch_e
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    goto :goto_1

    .line 553
    :pswitch_f
    packed-switch p2, :pswitch_data_4

    goto :goto_0

    .line 556
    :pswitch_10
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    goto :goto_1

    .line 558
    :pswitch_11
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    goto :goto_1

    .line 560
    :pswitch_12
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    goto :goto_1

    .line 562
    :pswitch_13
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_1

    .line 503
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch

    .line 506
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 521
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 537
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 553
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final getM00()F
    .locals 1

    .prologue
    .line 3272
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    return v0
.end method

.method public final getM01()F
    .locals 1

    .prologue
    .line 3294
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    return v0
.end method

.method public final getM02()F
    .locals 1

    .prologue
    .line 3316
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    return v0
.end method

.method public final getM03()F
    .locals 1

    .prologue
    .line 3470
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    return v0
.end method

.method public final getM10()F
    .locals 1

    .prologue
    .line 3338
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    return v0
.end method

.method public final getM11()F
    .locals 1

    .prologue
    .line 3360
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    return v0
.end method

.method public final getM12()F
    .locals 1

    .prologue
    .line 3382
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    return v0
.end method

.method public final getM13()F
    .locals 1

    .prologue
    .line 3492
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    return v0
.end method

.method public final getM20()F
    .locals 1

    .prologue
    .line 3404
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    return v0
.end method

.method public final getM21()F
    .locals 1

    .prologue
    .line 3426
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    return v0
.end method

.method public final getM22()F
    .locals 1

    .prologue
    .line 3448
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    return v0
.end method

.method public final getM23()F
    .locals 1

    .prologue
    .line 3514
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    return v0
.end method

.method public final getM30()F
    .locals 1

    .prologue
    .line 3536
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    return v0
.end method

.method public final getM31()F
    .locals 1

    .prologue
    .line 3559
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    return v0
.end method

.method public final getM32()F
    .locals 1

    .prologue
    .line 3581
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    return v0
.end method

.method public final getM33()F
    .locals 1

    .prologue
    .line 3604
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    return v0
.end method

.method public final getRotationScale(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 887
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 888
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 889
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 890
    return-void
.end method

.method public final getRow(ILjavax/vecmath/Vector4f;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 580
    if-nez p1, :cond_0

    .line 581
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 582
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 583
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 584
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    .line 604
    :goto_0
    return-void

    .line 585
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 586
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 587
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 588
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 589
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 590
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 591
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 592
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 593
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 594
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 595
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 596
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->x:F

    .line 597
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->y:F

    .line 598
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->z:F

    .line 599
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iput v0, p2, Ljavax/vecmath/Vector4f;->w:F

    goto :goto_0

    .line 601
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getRow(I[F)V
    .locals 5
    .param p1, "row"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 612
    if-nez p1, :cond_0

    .line 613
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    aput v0, p2, v4

    .line 614
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    aput v0, p2, v1

    .line 615
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    aput v0, p2, v2

    .line 616
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    aput v0, p2, v3

    .line 636
    :goto_0
    return-void

    .line 617
    :cond_0
    if-ne p1, v1, :cond_1

    .line 618
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    aput v0, p2, v4

    .line 619
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    aput v0, p2, v1

    .line 620
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    aput v0, p2, v2

    .line 621
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    aput v0, p2, v3

    goto :goto_0

    .line 622
    :cond_1
    if-ne p1, v2, :cond_2

    .line 623
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    aput v0, p2, v4

    .line 624
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    aput v0, p2, v1

    .line 625
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    aput v0, p2, v2

    .line 626
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    aput v0, p2, v3

    goto :goto_0

    .line 627
    :cond_2
    if-ne p1, v3, :cond_3

    .line 628
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    aput v0, p2, v4

    .line 629
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    aput v0, p2, v1

    .line 630
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    aput v0, p2, v2

    .line 631
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    aput v0, p2, v3

    goto :goto_0

    .line 633
    :cond_3
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f2"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getScale()F
    .locals 4

    .prologue
    .line 901
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 902
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 904
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 906
    invoke-static {v1}, Ljavax/vecmath/Matrix3d;->max3([D)D

    move-result-wide v2

    double-to-float v2, v2

    return v2
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f

    .line 2841
    const-wide/16 v0, 0x1

    .line 2842
    .local v0, "bits":J
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m00:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2843
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2844
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2845
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2846
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m10:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2847
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m11:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2848
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2849
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2850
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2851
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2852
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2853
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2854
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2855
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2856
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2857
    mul-long v2, v6, v0

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    invoke-static {v4}, Ljavax/vecmath/VecMathUtil;->floatToIntBits(F)I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 2858
    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v2, v0

    long-to-int v2, v2

    return v2
.end method

.method public final invert()V
    .locals 0

    .prologue
    .line 1708
    invoke-virtual {p0, p0}, Ljavax/vecmath/Matrix4f;->invertGeneral(Ljavax/vecmath/Matrix4f;)V

    .line 1709
    return-void
.end method

.method public final invert(Ljavax/vecmath/Matrix4f;)V
    .locals 0
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1700
    invoke-virtual {p0, p1}, Ljavax/vecmath/Matrix4f;->invertGeneral(Ljavax/vecmath/Matrix4f;)V

    .line 1701
    return-void
.end method

.method final invertGeneral(Ljavax/vecmath/Matrix4f;)V
    .locals 14
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const/16 v13, 0xa

    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 1720
    const/16 v4, 0x10

    new-array v3, v4, [D

    .line 1721
    .local v3, "temp":[D
    const/16 v4, 0x10

    new-array v1, v4, [D

    .line 1722
    .local v1, "result":[D
    new-array v2, v11, [I

    .line 1729
    .local v2, "row_perm":[I
    iget v4, p1, Ljavax/vecmath/Matrix4f;->m00:F

    float-to-double v4, v4

    aput-wide v4, v3, v10

    .line 1730
    const/4 v4, 0x1

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m01:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1731
    const/4 v4, 0x2

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m02:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1732
    const/4 v4, 0x3

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m03:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1734
    iget v4, p1, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v4, v4

    aput-wide v4, v3, v11

    .line 1735
    iget v4, p1, Ljavax/vecmath/Matrix4f;->m11:F

    float-to-double v4, v4

    aput-wide v4, v3, v12

    .line 1736
    const/4 v4, 0x6

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m12:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1737
    const/4 v4, 0x7

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m13:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1739
    const/16 v4, 0x8

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1740
    const/16 v4, 0x9

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1741
    iget v4, p1, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v4, v4

    aput-wide v4, v3, v13

    .line 1742
    const/16 v4, 0xb

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m23:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1744
    const/16 v4, 0xc

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m30:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1745
    const/16 v4, 0xd

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m31:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1746
    const/16 v4, 0xe

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m32:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1747
    const/16 v4, 0xf

    iget v5, p1, Ljavax/vecmath/Matrix4f;->m33:F

    float-to-double v6, v5

    aput-wide v6, v3, v4

    .line 1750
    invoke-static {v3, v2}, Ljavax/vecmath/Matrix4f;->luDecomposition([D[I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1752
    new-instance v4, Ljavax/vecmath/SingularMatrixException;

    const-string v5, "Matrix4f12"

    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1756
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v4, 0x10

    if-ge v0, v4, :cond_1

    const-wide/16 v4, 0x0

    aput-wide v4, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1757
    :cond_1
    aput-wide v8, v1, v10

    aput-wide v8, v1, v12

    aput-wide v8, v1, v13

    const/16 v4, 0xf

    aput-wide v8, v1, v4

    .line 1758
    invoke-static {v3, v2, v1}, Ljavax/vecmath/Matrix4f;->luBacksubstitution([D[I[D)V

    .line 1760
    aget-wide v4, v1, v10

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1761
    const/4 v4, 0x1

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1762
    const/4 v4, 0x2

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1763
    const/4 v4, 0x3

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1765
    aget-wide v4, v1, v11

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1766
    aget-wide v4, v1, v12

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1767
    const/4 v4, 0x6

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1768
    const/4 v4, 0x7

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1770
    const/16 v4, 0x8

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1771
    const/16 v4, 0x9

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1772
    aget-wide v4, v1, v13

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1773
    const/16 v4, 0xb

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1775
    const/16 v4, 0xc

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1776
    const/16 v4, 0xd

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1777
    const/16 v4, 0xe

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1778
    const/16 v4, 0xf

    aget-wide v4, v1, v4

    double-to-float v4, v4

    iput v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1780
    return-void
.end method

.method public final mul(F)V
    .locals 1
    .param p1, "scalar"    # F

    .prologue
    .line 2386
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2387
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2388
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2389
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2390
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2391
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2392
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2393
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2394
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2395
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2396
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2397
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2398
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2399
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2400
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2401
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2402
    return-void
.end method

.method public final mul(FLjavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "scalar"    # F
    .param p2, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2412
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m00:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2413
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m01:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2414
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m02:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2415
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m03:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2416
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m10:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2417
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m11:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2418
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m12:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2419
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m13:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2420
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m20:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2421
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m21:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2422
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m22:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2423
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m23:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2424
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m30:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2425
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m31:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2426
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m32:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2427
    iget v0, p2, Ljavax/vecmath/Matrix4f;->m33:F

    mul-float/2addr v0, p1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2428
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix4f;)V
    .locals 21
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2442
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    .line 2444
    .local v2, "m00":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    .line 2446
    .local v3, "m01":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    .line 2448
    .local v4, "m02":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    .line 2451
    .local v5, "m03":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    .line 2453
    .local v6, "m10":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    .line 2455
    .local v7, "m11":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    .line 2457
    .local v8, "m12":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    .line 2460
    .local v9, "m13":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    .line 2462
    .local v10, "m20":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    .line 2464
    .local v11, "m21":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    .line 2466
    .local v12, "m22":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    .line 2469
    .local v13, "m23":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    .line 2471
    .local v14, "m30":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    .line 2473
    .local v15, "m31":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    .line 2475
    .local v16, "m32":F
    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p0

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    .line 2478
    .local v17, "m33":F
    move-object/from16 v0, p0

    iput v2, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2479
    move-object/from16 v0, p0

    iput v6, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2480
    move-object/from16 v0, p0

    iput v10, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2481
    move-object/from16 v0, p0

    iput v14, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2482
    return-void
.end method

.method public final mul(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 21
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2492
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2494
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2496
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2498
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2500
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2503
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2505
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2507
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2509
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2512
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2514
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2516
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2518
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2521
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2523
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2525
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2527
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2559
    :goto_0
    return-void

    .line 2534
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    .line 2535
    .local v2, "m00":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    .line 2536
    .local v3, "m01":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    .line 2537
    .local v4, "m02":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    .line 2539
    .local v5, "m03":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    .line 2540
    .local v6, "m10":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    .line 2541
    .local v7, "m11":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    .line 2542
    .local v8, "m12":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    .line 2544
    .local v9, "m13":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    .line 2545
    .local v10, "m20":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    .line 2546
    .local v11, "m21":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    .line 2547
    .local v12, "m22":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    .line 2549
    .local v13, "m23":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    .line 2550
    .local v14, "m30":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    .line 2551
    .local v15, "m31":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    .line 2552
    .local v16, "m32":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    .line 2554
    .local v17, "m33":F
    move-object/from16 v0, p0

    iput v2, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2555
    move-object/from16 v0, p0

    iput v6, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2556
    move-object/from16 v0, p0

    iput v10, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2557
    move-object/from16 v0, p0

    iput v14, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    goto/16 :goto_0
.end method

.method public final mulTransposeBoth(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 21
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2569
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2570
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2571
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2572
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2573
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2575
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2576
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2577
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2578
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2580
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2581
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2582
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2583
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2585
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2586
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2587
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2588
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2621
    :goto_0
    return-void

    .line 2595
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    .line 2596
    .local v2, "m00":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    .line 2597
    .local v3, "m01":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    .line 2598
    .local v4, "m02":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    .line 2600
    .local v5, "m03":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    .line 2601
    .local v6, "m10":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    .line 2602
    .local v7, "m11":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    .line 2603
    .local v8, "m12":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    .line 2605
    .local v9, "m13":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    .line 2606
    .local v10, "m20":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    .line 2607
    .local v11, "m21":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    .line 2608
    .local v12, "m22":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    .line 2610
    .local v13, "m23":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    .line 2611
    .local v14, "m30":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    .line 2612
    .local v15, "m31":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    .line 2613
    .local v16, "m32":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    .line 2615
    .local v17, "m33":F
    move-object/from16 v0, p0

    iput v2, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2616
    move-object/from16 v0, p0

    iput v6, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2617
    move-object/from16 v0, p0

    iput v10, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2618
    move-object/from16 v0, p0

    iput v14, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    goto/16 :goto_0
.end method

.method public final mulTransposeLeft(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 21
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2694
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2695
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2696
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2697
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2698
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2700
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2701
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2702
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2703
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2705
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2706
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2707
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2708
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2710
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2711
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2712
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2713
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2748
    :goto_0
    return-void

    .line 2722
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    .line 2723
    .local v2, "m00":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    .line 2724
    .local v3, "m01":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    .line 2725
    .local v4, "m02":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    .line 2727
    .local v5, "m03":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    .line 2728
    .local v6, "m10":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    .line 2729
    .local v7, "m11":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    .line 2730
    .local v8, "m12":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    .line 2732
    .local v9, "m13":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    .line 2733
    .local v10, "m20":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    .line 2734
    .local v11, "m21":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    .line 2735
    .local v12, "m22":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    .line 2737
    .local v13, "m23":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    .line 2738
    .local v14, "m30":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    .line 2739
    .local v15, "m31":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    .line 2740
    .local v16, "m32":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    .line 2742
    .local v17, "m33":F
    move-object/from16 v0, p0

    iput v2, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2743
    move-object/from16 v0, p0

    iput v6, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2744
    move-object/from16 v0, p0

    iput v10, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2745
    move-object/from16 v0, p0

    iput v14, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    goto/16 :goto_0
.end method

.method public final mulTransposeRight(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 21
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 2631
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    if-eq v0, v1, :cond_0

    .line 2632
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2633
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2634
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2635
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2637
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2638
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2639
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2640
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2642
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2643
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2644
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2645
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2647
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2648
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2649
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2650
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2683
    :goto_0
    return-void

    .line 2657
    :cond_0
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v2, v18, v19

    .line 2658
    .local v2, "m00":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v3, v18, v19

    .line 2659
    .local v3, "m01":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v4, v18, v19

    .line 2660
    .local v4, "m02":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v5, v18, v19

    .line 2662
    .local v5, "m03":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v6, v18, v19

    .line 2663
    .local v6, "m10":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v7, v18, v19

    .line 2664
    .local v7, "m11":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v8, v18, v19

    .line 2665
    .local v8, "m12":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v9, v18, v19

    .line 2667
    .local v9, "m13":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v10, v18, v19

    .line 2668
    .local v10, "m20":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v11, v18, v19

    .line 2669
    .local v11, "m21":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v12, v18, v19

    .line 2670
    .local v12, "m22":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v13, v18, v19

    .line 2672
    .local v13, "m23":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m03:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v14, v18, v19

    .line 2673
    .local v14, "m30":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m13:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v15, v18, v19

    .line 2674
    .local v15, "m31":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m23:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v16, v18, v19

    .line 2675
    .local v16, "m32":F
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move/from16 v19, v0

    mul-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v18, v18, v19

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Ljavax/vecmath/Matrix4f;->m33:F

    move/from16 v20, v0

    mul-float v19, v19, v20

    add-float v17, v18, v19

    .line 2677
    .local v17, "m33":F
    move-object/from16 v0, p0

    iput v2, v0, Ljavax/vecmath/Matrix4f;->m00:F

    move-object/from16 v0, p0

    iput v3, v0, Ljavax/vecmath/Matrix4f;->m01:F

    move-object/from16 v0, p0

    iput v4, v0, Ljavax/vecmath/Matrix4f;->m02:F

    move-object/from16 v0, p0

    iput v5, v0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2678
    move-object/from16 v0, p0

    iput v6, v0, Ljavax/vecmath/Matrix4f;->m10:F

    move-object/from16 v0, p0

    iput v7, v0, Ljavax/vecmath/Matrix4f;->m11:F

    move-object/from16 v0, p0

    iput v8, v0, Ljavax/vecmath/Matrix4f;->m12:F

    move-object/from16 v0, p0

    iput v9, v0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2679
    move-object/from16 v0, p0

    iput v10, v0, Ljavax/vecmath/Matrix4f;->m20:F

    move-object/from16 v0, p0

    iput v11, v0, Ljavax/vecmath/Matrix4f;->m21:F

    move-object/from16 v0, p0

    iput v12, v0, Ljavax/vecmath/Matrix4f;->m22:F

    move-object/from16 v0, p0

    iput v13, v0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2680
    move-object/from16 v0, p0

    iput v14, v0, Ljavax/vecmath/Matrix4f;->m30:F

    move-object/from16 v0, p0

    iput v15, v0, Ljavax/vecmath/Matrix4f;->m31:F

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    goto/16 :goto_0
.end method

.method public final negate()V
    .locals 1

    .prologue
    .line 3182
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3183
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3184
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3185
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 3186
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3187
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3188
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3189
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 3190
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3191
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3192
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3193
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 3194
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 3195
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 3196
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 3197
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 3198
    return-void
.end method

.method public final negate(Ljavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 3207
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3208
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3209
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3210
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 3211
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3212
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3213
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3214
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 3215
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3216
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3217
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3218
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 3219
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 3220
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 3221
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 3222
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    neg-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 3223
    return-void
.end method

.method public final rotX(F)V
    .locals 6
    .param p1, "angle"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2290
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 2291
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 2293
    .local v0, "cosAngle":F
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2294
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2295
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2296
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2298
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2299
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2300
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2301
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2303
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2304
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2305
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2306
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2308
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2309
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2310
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2311
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2312
    return-void
.end method

.method public final rotY(F)V
    .locals 6
    .param p1, "angle"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2323
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 2324
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 2326
    .local v0, "cosAngle":F
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2327
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2328
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2329
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2331
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2332
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2333
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2334
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2336
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2337
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2338
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2339
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2341
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2342
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2343
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2344
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2345
    return-void
.end method

.method public final rotZ(F)V
    .locals 6
    .param p1, "angle"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 2356
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 2357
    .local v1, "sinAngle":F
    float-to-double v2, p1

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    double-to-float v0, v2

    .line 2359
    .local v0, "cosAngle":F
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2360
    neg-float v2, v1

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2361
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2362
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2364
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2365
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2366
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2367
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2369
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2370
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2371
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2372
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2374
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2375
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2376
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2377
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2378
    return-void
.end method

.method public final set(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    const/4 v0, 0x0

    .line 2070
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2071
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2072
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2073
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2075
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2076
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2077
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2078
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2080
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2081
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2082
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2083
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2085
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2086
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2087
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2088
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2089
    return-void
.end method

.method public final set(FLjavax/vecmath/Vector3f;)V
    .locals 2
    .param p1, "scale"    # F
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;

    .prologue
    const/4 v1, 0x0

    .line 2154
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2155
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2156
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2157
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2159
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2160
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2161
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2162
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2164
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2165
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2166
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2167
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2169
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2170
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2171
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2172
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2173
    return-void
.end method

.method public final set(Ljavax/vecmath/AxisAngle4d;)V
    .locals 22
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4d;

    .prologue
    .line 1524
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v16, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v18, v0

    mul-double v16, v16, v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    add-double v16, v16, v18

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v18, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v20, v0

    mul-double v18, v18, v20

    add-double v16, v16, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    .line 1526
    .local v10, "mag":D
    const-wide v16, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v16, v10, v16

    if-gez v16, :cond_0

    .line 1527
    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1528
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1529
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1531
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1532
    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1533
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1535
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1536
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1537
    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1564
    :goto_0
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1565
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1566
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1568
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1569
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1570
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1571
    const/high16 v16, 0x3f800000    # 1.0f

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1572
    return-void

    .line 1539
    :cond_0
    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    div-double v10, v16, v10

    .line 1540
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->x:D

    move-wide/from16 v16, v0

    mul-double v2, v16, v10

    .line 1541
    .local v2, "ax":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->y:D

    move-wide/from16 v16, v0

    mul-double v4, v16, v10

    .line 1542
    .local v4, "ay":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->z:D

    move-wide/from16 v16, v0

    mul-double v6, v16, v10

    .line 1544
    .local v6, "az":D
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->sin(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v9, v0

    .line 1545
    .local v9, "sinTheta":F
    move-object/from16 v0, p1

    iget-wide v0, v0, Ljavax/vecmath/AxisAngle4d;->angle:D

    move-wide/from16 v16, v0

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->cos(D)D

    move-result-wide v16

    move-wide/from16 v0, v16

    double-to-float v8, v0

    .line 1546
    .local v8, "cosTheta":F
    const/high16 v16, 0x3f800000    # 1.0f

    sub-float v12, v16, v8

    .line 1548
    .local v12, "t":F
    mul-double v16, v2, v6

    move-wide/from16 v0, v16

    double-to-float v14, v0

    .line 1549
    .local v14, "xz":F
    mul-double v16, v2, v4

    move-wide/from16 v0, v16

    double-to-float v13, v0

    .line 1550
    .local v13, "xy":F
    mul-double v16, v4, v6

    move-wide/from16 v0, v16

    double-to-float v15, v0

    .line 1552
    .local v15, "yz":F
    mul-double v16, v2, v2

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v16, v16, v8

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1553
    mul-float v16, v12, v13

    double-to-float v0, v6

    move/from16 v17, v0

    mul-float v17, v17, v9

    sub-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1554
    mul-float v16, v12, v14

    double-to-float v0, v4

    move/from16 v17, v0

    mul-float v17, v17, v9

    add-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1556
    mul-float v16, v12, v13

    double-to-float v0, v6

    move/from16 v17, v0

    mul-float v17, v17, v9

    add-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1557
    mul-double v16, v4, v4

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v16, v16, v8

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1558
    mul-float v16, v12, v15

    double-to-float v0, v2

    move/from16 v17, v0

    mul-float v17, v17, v9

    sub-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1560
    mul-float v16, v12, v14

    double-to-float v0, v4

    move/from16 v17, v0

    mul-float v17, v17, v9

    sub-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1561
    mul-float v16, v12, v15

    double-to-float v0, v2

    move/from16 v17, v0

    mul-float v17, v17, v9

    add-float v16, v16, v17

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1562
    mul-double v16, v6, v6

    move-wide/from16 v0, v16

    double-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v12

    add-float v16, v16, v8

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    goto/16 :goto_0
.end method

.method public final set(Ljavax/vecmath/AxisAngle4f;)V
    .locals 14
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 1439
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    iget v12, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    iget v11, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    iget v12, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v4, v10

    .line 1440
    .local v4, "mag":F
    float-to-double v10, v4

    const-wide v12, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v10, v10, v12

    if-gez v10, :cond_0

    .line 1441
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1442
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1443
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1445
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1446
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1447
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1449
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1450
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1451
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1478
    :goto_0
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1479
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1480
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1482
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1483
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1484
    const/4 v10, 0x0

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1485
    const/high16 v10, 0x3f800000    # 1.0f

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1486
    return-void

    .line 1453
    :cond_0
    const/high16 v10, 0x3f800000    # 1.0f

    div-float v4, v10, v4

    .line 1454
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->x:F

    mul-float v0, v10, v4

    .line 1455
    .local v0, "ax":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->y:F

    mul-float v1, v10, v4

    .line 1456
    .local v1, "ay":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->z:F

    mul-float v2, v10, v4

    .line 1458
    .local v2, "az":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->sin(D)D

    move-result-wide v10

    double-to-float v5, v10

    .line 1459
    .local v5, "sinTheta":F
    iget v10, p1, Ljavax/vecmath/AxisAngle4f;->angle:F

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    double-to-float v3, v10

    .line 1460
    .local v3, "cosTheta":F
    const/high16 v10, 0x3f800000    # 1.0f

    sub-float v6, v10, v3

    .line 1462
    .local v6, "t":F
    mul-float v8, v0, v2

    .line 1463
    .local v8, "xz":F
    mul-float v7, v0, v1

    .line 1464
    .local v7, "xy":F
    mul-float v9, v1, v2

    .line 1466
    .local v9, "yz":F
    mul-float v10, v6, v0

    mul-float/2addr v10, v0

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1467
    mul-float v10, v6, v7

    mul-float v11, v5, v2

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1468
    mul-float v10, v6, v8

    mul-float v11, v5, v1

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1470
    mul-float v10, v6, v7

    mul-float v11, v5, v2

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1471
    mul-float v10, v6, v1

    mul-float/2addr v10, v1

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1472
    mul-float v10, v6, v9

    mul-float v11, v5, v0

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1474
    mul-float v10, v6, v8

    mul-float v11, v5, v1

    sub-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1475
    mul-float v10, v6, v9

    mul-float v11, v5, v0

    add-float/2addr v10, v11

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1476
    mul-float v10, v6, v2

    mul-float/2addr v10, v2

    add-float/2addr v10, v3

    iput v10, p0, Ljavax/vecmath/Matrix4f;->m22:F

    goto :goto_0
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 3
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v2, 0x0

    .line 2057
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2058
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2059
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2060
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m32:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2061
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3d;Ljavax/vecmath/Vector3d;D)V
    .locals 3
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "scale"    # D

    .prologue
    const/4 v2, 0x0

    .line 2246
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2247
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2248
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2249
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2251
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2252
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2253
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2254
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2256
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2257
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2258
    iget-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2259
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2261
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2262
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2263
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2264
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2265
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v1, 0x0

    .line 2042
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2043
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2044
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2045
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2046
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;Ljavax/vecmath/Vector3f;F)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "scale"    # F

    .prologue
    const/4 v1, 0x0

    .line 2215
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2216
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2217
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2218
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2220
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2221
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2222
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2223
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2225
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2226
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2227
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2228
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2230
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2231
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2232
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2233
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2234
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    .line 1643
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1644
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1645
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1646
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1648
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1649
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1650
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1651
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1653
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1654
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1655
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1656
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1658
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1659
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1660
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1661
    iget-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1662
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1671
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1672
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1673
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1674
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1676
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1677
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1678
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1679
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1681
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1682
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1683
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1684
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1686
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1687
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1688
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1689
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1690
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;)V
    .locals 12
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/4 v8, 0x0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1495
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1496
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1497
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1499
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1500
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1501
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1503
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1504
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1505
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v10, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1507
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1508
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1509
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1511
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1512
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1513
    iput v8, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1514
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1515
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4d;Ljavax/vecmath/Vector3d;D)V
    .locals 11
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;
    .param p2, "t1"    # Ljavax/vecmath/Vector3d;
    .param p3, "s"    # D

    .prologue
    const/4 v10, 0x0

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 1583
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1584
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1585
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1587
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1588
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1589
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1591
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1592
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v0, v2

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, v6

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1593
    iget-wide v0, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v6

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v0, v2

    sub-double v0, v8, v0

    iget-wide v2, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v2, v4

    sub-double/2addr v0, v2

    mul-double/2addr v0, p3

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1595
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->x:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1596
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->y:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1597
    iget-wide v0, p2, Ljavax/vecmath/Vector3d;->z:D

    double-to-float v0, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1599
    iput v10, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1600
    iput v10, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1601
    iput v10, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1602
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1603
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;)V
    .locals 6
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    .line 1410
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    sub-float v0, v5, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1411
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1412
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1414
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1415
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v5, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1416
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1418
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1419
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1420
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v5, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1422
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1423
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1424
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1426
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1427
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1428
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1429
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1430
    return-void
.end method

.method public final set(Ljavax/vecmath/Quat4f;Ljavax/vecmath/Vector3f;F)V
    .locals 6
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;
    .param p2, "t1"    # Ljavax/vecmath/Vector3f;
    .param p3, "s"    # F

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v3, 0x40000000    # 2.0f

    .line 1614
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1615
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1616
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1618
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1619
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1620
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1622
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1623
    iget v0, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v1, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v0, v1

    iget v1, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, v3

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1624
    iget v0, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v3

    iget v1, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v0, v1

    sub-float v0, v4, v0

    iget v1, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v3

    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    mul-float/2addr v0, p3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1626
    iget v0, p2, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1627
    iget v0, p2, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1628
    iget v0, p2, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1630
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1631
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1632
    iput v5, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1633
    iput v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1634
    return-void
.end method

.method public final set(Ljavax/vecmath/Vector3f;)V
    .locals 3
    .param p1, "v1"    # Ljavax/vecmath/Vector3f;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 2124
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2125
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2126
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2127
    iget v0, p1, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2129
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2130
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2131
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2132
    iget v0, p1, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2134
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2135
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2136
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2137
    iget v0, p1, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2139
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2140
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2141
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2142
    iput v2, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2143
    return-void
.end method

.method public final set(Ljavax/vecmath/Vector3f;F)V
    .locals 2
    .param p1, "t1"    # Ljavax/vecmath/Vector3f;
    .param p2, "scale"    # F

    .prologue
    const/4 v1, 0x0

    .line 2184
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2185
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2186
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2187
    iget v0, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v0, p2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2189
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2190
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2191
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2192
    iget v0, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v0, p2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2194
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2195
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2196
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2197
    iget v0, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v0, p2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2199
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2200
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2201
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2202
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2203
    return-void
.end method

.method public final set([F)V
    .locals 1
    .param p1, "m"    # [F

    .prologue
    .line 2099
    const/4 v0, 0x0

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2100
    const/4 v0, 0x1

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2101
    const/4 v0, 0x2

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2102
    const/4 v0, 0x3

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2103
    const/4 v0, 0x4

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2104
    const/4 v0, 0x5

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2105
    const/4 v0, 0x6

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 2106
    const/4 v0, 0x7

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2107
    const/16 v0, 0x8

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 2108
    const/16 v0, 0x9

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 2109
    const/16 v0, 0xa

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 2110
    const/16 v0, 0xb

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2111
    const/16 v0, 0xc

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 2112
    const/16 v0, 0xd

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 2113
    const/16 v0, 0xe

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 2114
    const/16 v0, 0xf

    aget v0, p1, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 2115
    return-void
.end method

.method public final setColumn(IFFFF)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F
    .param p5, "w"    # F

    .prologue
    .line 1061
    packed-switch p1, :pswitch_data_0

    .line 1091
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1063
    :pswitch_0
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1064
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1065
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1066
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1093
    :goto_0
    return-void

    .line 1070
    :pswitch_1
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1071
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1072
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1073
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m31:F

    goto :goto_0

    .line 1077
    :pswitch_2
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1078
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1079
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1080
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m32:F

    goto :goto_0

    .line 1084
    :pswitch_3
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1085
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1086
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1087
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 1061
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setColumn(ILjavax/vecmath/Vector4f;)V
    .locals 2
    .param p1, "column"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 1102
    packed-switch p1, :pswitch_data_0

    .line 1132
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1104
    :pswitch_0
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1105
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1106
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1107
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1134
    :goto_0
    return-void

    .line 1111
    :pswitch_1
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1112
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1113
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1114
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    goto :goto_0

    .line 1118
    :pswitch_2
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1119
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1120
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1121
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    goto :goto_0

    .line 1125
    :pswitch_3
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1126
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1127
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1128
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 1102
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setColumn(I[F)V
    .locals 4
    .param p1, "column"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1143
    packed-switch p1, :pswitch_data_0

    .line 1173
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f9"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1145
    :pswitch_0
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1146
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1147
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1148
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1175
    :goto_0
    return-void

    .line 1152
    :pswitch_1
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1153
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1154
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1155
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    goto :goto_0

    .line 1159
    :pswitch_2
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1160
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1161
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1162
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    goto :goto_0

    .line 1166
    :pswitch_3
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1167
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1168
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1169
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 1143
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setElement(IIF)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "value"    # F

    .prologue
    .line 408
    packed-switch p1, :pswitch_data_0

    .line 491
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 411
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 426
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :pswitch_1
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 493
    :goto_0
    return-void

    .line 417
    :pswitch_2
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    goto :goto_0

    .line 420
    :pswitch_3
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    goto :goto_0

    .line 423
    :pswitch_4
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m03:F

    goto :goto_0

    .line 431
    :pswitch_5
    packed-switch p2, :pswitch_data_2

    .line 446
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :pswitch_6
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    goto :goto_0

    .line 437
    :pswitch_7
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    goto :goto_0

    .line 440
    :pswitch_8
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    goto :goto_0

    .line 443
    :pswitch_9
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    goto :goto_0

    .line 451
    :pswitch_a
    packed-switch p2, :pswitch_data_3

    .line 466
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 454
    :pswitch_b
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m20:F

    goto :goto_0

    .line 457
    :pswitch_c
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    goto :goto_0

    .line 460
    :pswitch_d
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    goto :goto_0

    .line 463
    :pswitch_e
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m23:F

    goto :goto_0

    .line 471
    :pswitch_f
    packed-switch p2, :pswitch_data_4

    .line 486
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f0"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :pswitch_10
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m30:F

    goto :goto_0

    .line 477
    :pswitch_11
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m31:F

    goto :goto_0

    .line 480
    :pswitch_12
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m32:F

    goto :goto_0

    .line 483
    :pswitch_13
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 408
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_5
        :pswitch_a
        :pswitch_f
    .end packed-switch

    .line 411
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 431
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch

    .line 451
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 471
    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method

.method public final setIdentity()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 379
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 380
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 381
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 382
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 384
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 385
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 386
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 387
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 389
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 390
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 391
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 392
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 394
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 395
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 396
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 397
    iput v1, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 398
    return-void
.end method

.method public final setM00(F)V
    .locals 0
    .param p1, "m00"    # F

    .prologue
    .line 3283
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3284
    return-void
.end method

.method public final setM01(F)V
    .locals 0
    .param p1, "m01"    # F

    .prologue
    .line 3305
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3306
    return-void
.end method

.method public final setM02(F)V
    .locals 0
    .param p1, "m02"    # F

    .prologue
    .line 3327
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3328
    return-void
.end method

.method public final setM03(F)V
    .locals 0
    .param p1, "m03"    # F

    .prologue
    .line 3481
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 3482
    return-void
.end method

.method public final setM10(F)V
    .locals 0
    .param p1, "m10"    # F

    .prologue
    .line 3349
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3350
    return-void
.end method

.method public final setM11(F)V
    .locals 0
    .param p1, "m11"    # F

    .prologue
    .line 3371
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3372
    return-void
.end method

.method public final setM12(F)V
    .locals 0
    .param p1, "m12"    # F

    .prologue
    .line 3393
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3394
    return-void
.end method

.method public final setM13(F)V
    .locals 0
    .param p1, "m13"    # F

    .prologue
    .line 3503
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 3504
    return-void
.end method

.method public final setM20(F)V
    .locals 0
    .param p1, "m20"    # F

    .prologue
    .line 3415
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3416
    return-void
.end method

.method public final setM21(F)V
    .locals 0
    .param p1, "m21"    # F

    .prologue
    .line 3437
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3438
    return-void
.end method

.method public final setM22(F)V
    .locals 0
    .param p1, "m22"    # F

    .prologue
    .line 3459
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3460
    return-void
.end method

.method public final setM23(F)V
    .locals 0
    .param p1, "m23"    # F

    .prologue
    .line 3525
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 3526
    return-void
.end method

.method public final setM30(F)V
    .locals 0
    .param p1, "m30"    # F

    .prologue
    .line 3548
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 3549
    return-void
.end method

.method public final setM31(F)V
    .locals 0
    .param p1, "m31"    # F

    .prologue
    .line 3570
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 3571
    return-void
.end method

.method public final setM32(F)V
    .locals 0
    .param p1, "m32"    # F

    .prologue
    .line 3593
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 3594
    return-void
.end method

.method public final setM33(F)V
    .locals 0
    .param p1, "m33"    # F

    .prologue
    .line 3615
    iput p1, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 3616
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/AxisAngle4f;)V
    .locals 30
    .param p1, "a1"    # Ljavax/vecmath/AxisAngle4f;

    .prologue
    .line 3106
    const/16 v26, 0x9

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 3107
    .local v18, "tmp_rot":[D
    const/16 v26, 0x3

    move/from16 v0, v26

    new-array v0, v0, [D

    move-object/from16 v19, v0

    .line 3109
    .local v19, "tmp_scale":[D
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 3111
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v28, v0

    mul-float v27, v27, v28

    add-float v26, v26, v27

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v27, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v28, v0

    mul-float v27, v27, v28

    add-float v26, v26, v27

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 3112
    .local v12, "mag":D
    const-wide v26, 0x3e45798ee2308c3aL    # 1.0E-8

    cmpg-double v26, v12, v26

    if-gez v26, :cond_0

    .line 3113
    const/high16 v26, 0x3f800000    # 1.0f

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3114
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3115
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3117
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3118
    const/high16 v26, 0x3f800000    # 1.0f

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3119
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3121
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3122
    const/16 v26, 0x0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3123
    const/high16 v26, 0x3f800000    # 1.0f

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3152
    :goto_0
    return-void

    .line 3125
    :cond_0
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v12, v26, v12

    .line 3126
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    mul-double v4, v26, v12

    .line 3127
    .local v4, "ax":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    mul-double v6, v26, v12

    .line 3128
    .local v6, "ay":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    mul-double v8, v26, v12

    .line 3130
    .local v8, "az":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->sin(D)D

    move-result-wide v14

    .line 3131
    .local v14, "sinTheta":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->angle:F

    move/from16 v26, v0

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v26, v0

    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->cos(D)D

    move-result-wide v10

    .line 3132
    .local v10, "cosTheta":D
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    sub-double v16, v26, v10

    .line 3134
    .local v16, "t":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v22, v0

    .line 3135
    .local v22, "xz":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->x:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v20, v0

    .line 3136
    .local v20, "xy":D
    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->y:F

    move/from16 v26, v0

    move-object/from16 v0, p1

    iget v0, v0, Ljavax/vecmath/AxisAngle4f;->z:F

    move/from16 v27, v0

    mul-float v26, v26, v27

    move/from16 v0, v26

    float-to-double v0, v0

    move-wide/from16 v24, v0

    .line 3138
    .local v24, "yz":D
    mul-double v26, v16, v4

    mul-double v26, v26, v4

    add-double v26, v26, v10

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3139
    mul-double v26, v16, v20

    mul-double v28, v14, v8

    sub-double v26, v26, v28

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3140
    mul-double v26, v16, v22

    mul-double v28, v14, v6

    add-double v26, v26, v28

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3142
    mul-double v26, v16, v20

    mul-double v28, v14, v8

    add-double v26, v26, v28

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3143
    mul-double v26, v16, v6

    mul-double v26, v26, v6

    add-double v26, v26, v10

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3144
    mul-double v26, v16, v24

    mul-double v28, v14, v4

    sub-double v26, v26, v28

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3146
    mul-double v26, v16, v22

    mul-double v28, v14, v6

    sub-double v26, v26, v28

    const/16 v28, 0x0

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3147
    mul-double v26, v16, v24

    mul-double v28, v14, v4

    add-double v26, v26, v28

    const/16 v28, 0x1

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3148
    mul-double v26, v16, v8

    mul-double v26, v26, v8

    add-double v26, v26, v10

    const/16 v28, 0x2

    aget-wide v28, v19, v28

    mul-double v26, v26, v28

    move-wide/from16 v0, v26

    double-to-float v0, v0

    move/from16 v26, v0

    move/from16 v0, v26

    move-object/from16 v1, p0

    iput v0, v1, Ljavax/vecmath/Matrix4f;->m22:F

    goto/16 :goto_0
.end method

.method public final setRotation(Ljavax/vecmath/Matrix3d;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2988
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 2989
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 2991
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 2993
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m00:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 2994
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m01:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 2995
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m02:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 2997
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m10:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 2998
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m11:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 2999
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m12:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3001
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m20:D

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3002
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m21:D

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3003
    iget-wide v2, p1, Ljavax/vecmath/Matrix3d;->m22:D

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3005
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Matrix3f;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3018
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3019
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3021
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 3023
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3024
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3025
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3027
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3028
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3029
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3031
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v2, v2

    aget-wide v4, v1, v6

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3032
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v2, v2

    aget-wide v4, v1, v7

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3033
    iget v2, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3034
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Quat4d;)V
    .locals 10
    .param p1, "q1"    # Ljavax/vecmath/Quat4d;

    .prologue
    .line 3077
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3078
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3080
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 3082
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3083
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3084
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x0

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3086
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3087
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3088
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x1

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3090
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->x:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3091
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-wide v4, p1, Ljavax/vecmath/Quat4d;->y:D

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->z:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->w:D

    iget-wide v8, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3092
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->x:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    iget-wide v6, p1, Ljavax/vecmath/Quat4d;->y:D

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    const/4 v4, 0x2

    aget-wide v4, v1, v4

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3093
    return-void
.end method

.method public final setRotation(Ljavax/vecmath/Quat4f;)V
    .locals 11
    .param p1, "q1"    # Ljavax/vecmath/Quat4f;

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x40000000    # 2.0f

    .line 3047
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 3048
    .local v0, "tmp_rot":[D
    const/4 v2, 0x3

    new-array v1, v2, [D

    .line 3049
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 3051
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v6

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    sub-float v2, v7, v2

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3052
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3053
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3055
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3056
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v6

    iget v3, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v3

    sub-float v2, v7, v2

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3057
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v9

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3059
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v10

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3060
    iget v2, p1, Ljavax/vecmath/Quat4f;->y:F

    iget v3, p1, Ljavax/vecmath/Quat4f;->z:F

    mul-float/2addr v2, v3

    iget v3, p1, Ljavax/vecmath/Quat4f;->w:F

    iget v4, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    mul-float/2addr v2, v6

    float-to-double v2, v2

    aget-wide v4, v1, v10

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3061
    iget v2, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v6

    iget v3, p1, Ljavax/vecmath/Quat4f;->x:F

    mul-float/2addr v2, v3

    sub-float v2, v7, v2

    iget v3, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v6

    iget v4, p1, Ljavax/vecmath/Quat4f;->y:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-double v2, v2

    aget-wide v4, v1, v10

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3063
    return-void
.end method

.method public final setRotationScale(Ljavax/vecmath/Matrix3f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    .line 918
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 919
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 920
    iget v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 921
    return-void
.end method

.method public final setRow(IFFFF)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "z"    # F
    .param p5, "w"    # F

    .prologue
    .line 934
    packed-switch p1, :pswitch_data_0

    .line 964
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 936
    :pswitch_0
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 937
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 938
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 939
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 966
    :goto_0
    return-void

    .line 943
    :pswitch_1
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 944
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 945
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 946
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m13:F

    goto :goto_0

    .line 950
    :pswitch_2
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 951
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 952
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 953
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m23:F

    goto :goto_0

    .line 957
    :pswitch_3
    iput p2, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 958
    iput p3, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 959
    iput p4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 960
    iput p5, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 934
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setRow(ILjavax/vecmath/Vector4f;)V
    .locals 2
    .param p1, "row"    # I
    .param p2, "v"    # Ljavax/vecmath/Vector4f;

    .prologue
    .line 975
    packed-switch p1, :pswitch_data_0

    .line 1005
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 977
    :pswitch_0
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 978
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 979
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 980
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1007
    :goto_0
    return-void

    .line 984
    :pswitch_1
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 985
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 986
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 987
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    goto :goto_0

    .line 991
    :pswitch_2
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 992
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 993
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 994
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    goto :goto_0

    .line 998
    :pswitch_3
    iget v0, p2, Ljavax/vecmath/Vector4f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 999
    iget v0, p2, Ljavax/vecmath/Vector4f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1000
    iget v0, p2, Ljavax/vecmath/Vector4f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1001
    iget v0, p2, Ljavax/vecmath/Vector4f;->w:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 975
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setRow(I[F)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "v"    # [F

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1017
    packed-switch p1, :pswitch_data_0

    .line 1047
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    const-string v1, "Matrix4f6"

    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1019
    :pswitch_0
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1020
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1021
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1022
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1049
    :goto_0
    return-void

    .line 1026
    :pswitch_1
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1027
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1028
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1029
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    goto :goto_0

    .line 1033
    :pswitch_2
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1034
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1035
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1036
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    goto :goto_0

    .line 1040
    :pswitch_3
    aget v0, p2, v0

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1041
    aget v0, p2, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1042
    aget v0, p2, v2

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1043
    aget v0, p2, v3

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    goto :goto_0

    .line 1017
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final setScale(F)V
    .locals 7
    .param p1, "scale"    # F

    .prologue
    const/4 v6, 0x3

    .line 713
    const/16 v2, 0x9

    new-array v0, v2, [D

    .line 714
    .local v0, "tmp_rot":[D
    new-array v1, v6, [D

    .line 715
    .local v1, "tmp_scale":[D
    invoke-direct {p0, v1, v0}, Ljavax/vecmath/Matrix4f;->getScaleRotate([D[D)V

    .line 717
    const/4 v2, 0x0

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 718
    const/4 v2, 0x1

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 719
    const/4 v2, 0x2

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 721
    aget-wide v2, v0, v6

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 722
    const/4 v2, 0x4

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 723
    const/4 v2, 0x5

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 725
    const/4 v2, 0x6

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 726
    const/4 v2, 0x7

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 727
    const/16 v2, 0x8

    aget-wide v2, v0, v2

    float-to-double v4, p1

    mul-double/2addr v2, v4

    double-to-float v2, v2

    iput v2, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 729
    return-void
.end method

.method public final setTranslation(Ljavax/vecmath/Vector3f;)V
    .locals 1
    .param p1, "trans"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 2275
    iget v0, p1, Ljavax/vecmath/Vector3f;->x:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 2276
    iget v0, p1, Ljavax/vecmath/Vector3f;->y:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 2277
    iget v0, p1, Ljavax/vecmath/Vector3f;->z:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 2278
    return-void
.end method

.method public final setZero()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3159
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 3160
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 3161
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 3162
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 3163
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 3164
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 3165
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 3166
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 3167
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 3168
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 3169
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 3170
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 3171
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 3172
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 3173
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 3174
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 3175
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1320
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m00:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1321
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m01:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1322
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m02:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1323
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m03:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1325
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m10:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1326
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m11:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1327
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m12:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1328
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m13:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1330
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m20:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1331
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m21:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1332
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m22:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1333
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m23:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1335
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m30:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1336
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m31:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1337
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m32:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1338
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v1, p1, Ljavax/vecmath/Matrix4f;->m33:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1339
    return-void
.end method

.method public final sub(Ljavax/vecmath/Matrix4f;Ljavax/vecmath/Matrix4f;)V
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;
    .param p2, "m2"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1292
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m00:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1293
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m01:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1294
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m02:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1295
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m03:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1297
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m10:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1298
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m11:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1299
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m12:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1300
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m13:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1302
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m20:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1303
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m21:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1304
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m22:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1305
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m23:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1307
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m30:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1308
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m31:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1309
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m32:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1310
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    iget v1, p2, Ljavax/vecmath/Matrix4f;->m33:F

    sub-float/2addr v0, v1

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1311
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 367
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m00:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m11:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m13:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m22:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m23:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Ljavax/vecmath/Matrix4f;->m33:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final transform(Ljavax/vecmath/Point3f;)V
    .locals 5
    .param p1, "point"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 2934
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m03:F

    add-float v0, v2, v3

    .line 2935
    .local v0, "x":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    add-float v1, v2, v3

    .line 2936
    .local v1, "y":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v2, v3

    iput v2, p1, Ljavax/vecmath/Point3f;->z:F

    .line 2937
    iput v0, p1, Ljavax/vecmath/Point3f;->x:F

    .line 2938
    iput v1, p1, Ljavax/vecmath/Point3f;->y:F

    .line 2939
    return-void
.end method

.method public final transform(Ljavax/vecmath/Point3f;Ljavax/vecmath/Point3f;)V
    .locals 5
    .param p1, "point"    # Ljavax/vecmath/Point3f;
    .param p2, "pointOut"    # Ljavax/vecmath/Point3f;

    .prologue
    .line 2917
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m03:F

    add-float v0, v2, v3

    .line 2918
    .local v0, "x":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m13:F

    add-float v1, v2, v3

    .line 2919
    .local v1, "y":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v3, p1, Ljavax/vecmath/Point3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v4, p1, Ljavax/vecmath/Point3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v4, p1, Ljavax/vecmath/Point3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m23:F

    add-float/2addr v2, v3

    iput v2, p2, Ljavax/vecmath/Point3f;->z:F

    .line 2920
    iput v0, p2, Ljavax/vecmath/Point3f;->x:F

    .line 2921
    iput v1, p2, Ljavax/vecmath/Point3f;->y:F

    .line 2922
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4f;)V
    .locals 6
    .param p1, "vec"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 2894
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v0, v3, v4

    .line 2896
    .local v0, "x":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v1, v3, v4

    .line 2898
    .local v1, "y":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 2900
    .local v2, "z":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p1, Ljavax/vecmath/Tuple4f;->w:F

    .line 2902
    iput v0, p1, Ljavax/vecmath/Tuple4f;->x:F

    .line 2903
    iput v1, p1, Ljavax/vecmath/Tuple4f;->y:F

    .line 2904
    iput v2, p1, Ljavax/vecmath/Tuple4f;->z:F

    .line 2905
    return-void
.end method

.method public final transform(Ljavax/vecmath/Tuple4f;Ljavax/vecmath/Tuple4f;)V
    .locals 6
    .param p1, "vec"    # Ljavax/vecmath/Tuple4f;
    .param p2, "vecOut"    # Ljavax/vecmath/Tuple4f;

    .prologue
    .line 2871
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v0, v3, v4

    .line 2873
    .local v0, "x":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v1, v3, v4

    .line 2875
    .local v1, "y":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float v2, v3, v4

    .line 2877
    .local v2, "z":F
    iget v3, p0, Ljavax/vecmath/Matrix4f;->m30:F

    iget v4, p1, Ljavax/vecmath/Tuple4f;->x:F

    mul-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m31:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->y:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m32:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->z:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iget v4, p0, Ljavax/vecmath/Matrix4f;->m33:F

    iget v5, p1, Ljavax/vecmath/Tuple4f;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    iput v3, p2, Ljavax/vecmath/Tuple4f;->w:F

    .line 2879
    iput v0, p2, Ljavax/vecmath/Tuple4f;->x:F

    .line 2880
    iput v1, p2, Ljavax/vecmath/Tuple4f;->y:F

    .line 2881
    iput v2, p2, Ljavax/vecmath/Tuple4f;->z:F

    .line 2882
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3f;)V
    .locals 5
    .param p1, "normal"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 2968
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float v0, v2, v3

    .line 2969
    .local v0, "x":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float v1, v2, v3

    .line 2970
    .local v1, "y":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p1, Ljavax/vecmath/Vector3f;->z:F

    .line 2971
    iput v0, p1, Ljavax/vecmath/Vector3f;->x:F

    .line 2972
    iput v1, p1, Ljavax/vecmath/Vector3f;->y:F

    .line 2973
    return-void
.end method

.method public final transform(Ljavax/vecmath/Vector3f;Ljavax/vecmath/Vector3f;)V
    .locals 5
    .param p1, "normal"    # Ljavax/vecmath/Vector3f;
    .param p2, "normalOut"    # Ljavax/vecmath/Vector3f;

    .prologue
    .line 2951
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m00:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float v0, v2, v3

    .line 2952
    .local v0, "x":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m10:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m11:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float v1, v2, v3

    .line 2953
    .local v1, "y":F
    iget v2, p0, Ljavax/vecmath/Matrix4f;->m20:F

    iget v3, p1, Ljavax/vecmath/Vector3f;->x:F

    mul-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m21:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->y:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iget v3, p0, Ljavax/vecmath/Matrix4f;->m22:F

    iget v4, p1, Ljavax/vecmath/Vector3f;->z:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    iput v2, p2, Ljavax/vecmath/Vector3f;->z:F

    .line 2954
    iput v0, p2, Ljavax/vecmath/Vector3f;->x:F

    .line 2955
    iput v1, p2, Ljavax/vecmath/Vector3f;->y:F

    .line 2956
    return-void
.end method

.method public final transpose()V
    .locals 2

    .prologue
    .line 1348
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1349
    .local v0, "temp":F
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m01:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1350
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1352
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1353
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m02:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1354
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1356
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1357
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m03:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1358
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1360
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1361
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m12:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1362
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1364
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1365
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m13:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1366
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1368
    iget v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1369
    iget v1, p0, Ljavax/vecmath/Matrix4f;->m23:F

    iput v1, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1370
    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1371
    return-void
.end method

.method public final transpose(Ljavax/vecmath/Matrix4f;)V
    .locals 1
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    .line 1379
    if-eq p0, p1, :cond_0

    .line 1380
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1381
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1382
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1383
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1385
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1386
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1387
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1388
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1390
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1391
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1392
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1393
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1395
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1396
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1397
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1398
    iget v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    iput v0, p0, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1401
    :goto_0
    return-void

    .line 1400
    :cond_0
    invoke-virtual {p0}, Ljavax/vecmath/Matrix4f;->transpose()V

    goto :goto_0
.end method
