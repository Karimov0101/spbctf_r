.class public Ljavax/vecmath/GMatrix;
.super Ljava/lang/Object;
.source "GMatrix.java"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/lang/Cloneable;


# static fields
.field private static final EPS:D = 1.0E-10

.field private static final debug:Z = false

.field static final serialVersionUID:J = 0x268a3b4aa638cc3dL


# instance fields
.field nCol:I

.field nRow:I

.field values:[[D


# direct methods
.method public constructor <init>(II)V
    .locals 6
    .param p1, "nRow"    # I
    .param p2, "nCol"    # I

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    filled-new-array {p1, p2}, [I

    move-result-object v3

    sget-object v4, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 67
    iput p1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 68
    iput p2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 71
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 72
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, p2, :cond_0

    .line 73
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v1

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 71
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    .end local v1    # "j":I
    :cond_1
    if-ge p1, p2, :cond_2

    .line 79
    move v2, p1

    .line 83
    .local v2, "l":I
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_3

    .line 84
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    aput-wide v4, v3, v0

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 81
    .end local v2    # "l":I
    :cond_2
    move v2, p2

    .restart local v2    # "l":I
    goto :goto_2

    .line 86
    :cond_3
    return-void
.end method

.method public constructor <init>(II[D)V
    .locals 6
    .param p1, "nRow"    # I
    .param p2, "nCol"    # I
    .param p3, "matrix"    # [D

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    filled-new-array {p1, p2}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 103
    iput p1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 104
    iput p2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 107
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 108
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, p2, :cond_0

    .line 109
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    mul-int v3, v0, p2

    add-int/2addr v3, v1

    aget-wide v4, p3, v3

    aput-wide v4, v2, v1

    .line 108
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 107
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public constructor <init>(Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p1, "matrix"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    iget v2, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iput v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 122
    iget v2, p1, Ljavax/vecmath/GMatrix;->nCol:I

    iput v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 123
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 126
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_1

    .line 127
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_0

    .line 128
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    aput-wide v4, v2, v1

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 126
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 131
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method private static chase_across([D[DILjavax/vecmath/GMatrix;)V
    .locals 20
    .param p0, "s"    # [D
    .param p1, "e"    # [D
    .param p2, "k"    # I
    .param p3, "u"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2483
    const/4 v8, 0x1

    new-array v7, v8, [D

    .line 2484
    .local v7, "cosl":[D
    const/4 v8, 0x1

    new-array v6, v8, [D

    .line 2486
    .local v6, "sinl":[D
    new-instance v13, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p3

    iget v8, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p3

    iget v9, v0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-direct {v13, v8, v9}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2487
    .local v13, "t":Ljavax/vecmath/GMatrix;
    new-instance v14, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p3

    iget v8, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p3

    iget v9, v0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-direct {v14, v8, v9}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2499
    .local v14, "m":Ljavax/vecmath/GMatrix;
    aget-wide v4, p1, p2

    .line 2500
    .local v4, "g":D
    add-int/lit8 v8, p2, 0x1

    aget-wide v2, p0, v8

    .line 2502
    .local v2, "f":D
    move/from16 v15, p2

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p3

    iget v8, v0, Ljavax/vecmath/GMatrix;->nCol:I

    add-int/lit8 v8, v8, -0x2

    if-ge v15, v8, :cond_0

    .line 2503
    invoke-static/range {v2 .. v7}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v16

    .line 2504
    .local v16, "r":D
    add-int/lit8 v8, v15, 0x1

    aget-wide v8, p1, v8

    neg-double v8, v8

    const/4 v10, 0x0

    aget-wide v10, v6, v10

    mul-double v4, v8, v10

    .line 2505
    add-int/lit8 v8, v15, 0x2

    aget-wide v2, p0, v8

    .line 2506
    add-int/lit8 v8, v15, 0x1

    aput-wide v16, p0, v8

    .line 2507
    add-int/lit8 v8, v15, 0x1

    add-int/lit8 v9, v15, 0x1

    aget-wide v10, p1, v9

    const/4 v9, 0x0

    aget-wide v18, v7, v9

    mul-double v10, v10, v18

    aput-wide v10, p1, v8

    .line 2508
    add-int/lit8 v9, v15, 0x1

    move/from16 v8, p2

    move-object/from16 v10, p3

    move-object v11, v7

    move-object v12, v6

    invoke-static/range {v8 .. v14}, Ljavax/vecmath/GMatrix;->update_u_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2502
    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    .line 2511
    .end local v16    # "r":D
    :cond_0
    add-int/lit8 v8, v15, 0x1

    invoke-static/range {v2 .. v7}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v10

    aput-wide v10, p0, v8

    .line 2512
    add-int/lit8 v9, v15, 0x1

    move/from16 v8, p2

    move-object/from16 v10, p3

    move-object v11, v7

    move-object v12, v6

    invoke-static/range {v8 .. v14}, Ljavax/vecmath/GMatrix;->update_u_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2513
    return-void
.end method

.method private static chase_up([D[DILjavax/vecmath/GMatrix;)V
    .locals 20
    .param p0, "s"    # [D
    .param p1, "e"    # [D
    .param p2, "k"    # I
    .param p3, "v"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2449
    const/4 v9, 0x1

    new-array v7, v9, [D

    .line 2450
    .local v7, "cosr":[D
    const/4 v9, 0x1

    new-array v6, v9, [D

    .line 2452
    .local v6, "sinr":[D
    new-instance v13, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p3

    iget v9, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p3

    iget v10, v0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-direct {v13, v9, v10}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2453
    .local v13, "t":Ljavax/vecmath/GMatrix;
    new-instance v14, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p3

    iget v9, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p3

    iget v10, v0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-direct {v14, v9, v10}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2465
    .local v14, "m":Ljavax/vecmath/GMatrix;
    aget-wide v2, p1, p2

    .line 2466
    .local v2, "f":D
    aget-wide v4, p0, p2

    .line 2468
    .local v4, "g":D
    move/from16 v8, p2

    .local v8, "i":I
    :goto_0
    if-lez v8, :cond_0

    .line 2469
    invoke-static/range {v2 .. v7}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v16

    .line 2470
    .local v16, "r":D
    add-int/lit8 v9, v8, -0x1

    aget-wide v10, p1, v9

    neg-double v10, v10

    const/4 v9, 0x0

    aget-wide v18, v6, v9

    mul-double v2, v10, v18

    .line 2471
    add-int/lit8 v9, v8, -0x1

    aget-wide v4, p0, v9

    .line 2472
    aput-wide v16, p0, v8

    .line 2473
    add-int/lit8 v9, v8, -0x1

    add-int/lit8 v10, v8, -0x1

    aget-wide v10, p1, v10

    const/4 v12, 0x0

    aget-wide v18, v7, v12

    mul-double v10, v10, v18

    aput-wide v10, p1, v9

    .line 2474
    add-int/lit8 v9, p2, 0x1

    move-object/from16 v10, p3

    move-object v11, v7

    move-object v12, v6

    invoke-static/range {v8 .. v14}, Ljavax/vecmath/GMatrix;->update_v_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2468
    add-int/lit8 v8, v8, -0x1

    goto :goto_0

    .line 2477
    .end local v16    # "r":D
    :cond_0
    add-int/lit8 v9, v8, 0x1

    invoke-static/range {v2 .. v7}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v10

    aput-wide v10, p0, v9

    .line 2478
    add-int/lit8 v9, p2, 0x1

    move-object/from16 v10, p3

    move-object v11, v7

    move-object v12, v6

    invoke-static/range {v8 .. v14}, Ljavax/vecmath/GMatrix;->update_v_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2479
    return-void
.end method

.method private static checkMatrix(Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p0, "m"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1370
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_2

    .line 1371
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_1

    .line 1372
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    aget-wide v2, v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v2

    const-wide v4, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 1373
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, " 0.0     "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1371
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1375
    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v4, v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    goto :goto_2

    .line 1378
    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    .line 1370
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1380
    .end local v1    # "j":I
    :cond_2
    return-void
.end method

.method static computeSVD(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)I
    .locals 42
    .param p0, "mat"    # Ljavax/vecmath/GMatrix;
    .param p1, "U"    # Ljavax/vecmath/GMatrix;
    .param p2, "W"    # Ljavax/vecmath/GMatrix;
    .param p3, "V"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1969
    new-instance v32, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p0

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p0

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move-object/from16 v0, v32

    invoke-direct {v0, v4, v5}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 1970
    .local v32, "tmp":Ljavax/vecmath/GMatrix;
    new-instance v33, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p0

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p0

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move-object/from16 v0, v33

    invoke-direct {v0, v4, v5}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 1971
    .local v33, "u":Ljavax/vecmath/GMatrix;
    new-instance v34, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p0

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, p0

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move-object/from16 v0, v34

    invoke-direct {v0, v4, v5}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 1972
    .local v34, "v":Ljavax/vecmath/GMatrix;
    new-instance v20, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Ljavax/vecmath/GMatrix;-><init>(Ljavax/vecmath/GMatrix;)V

    .line 1975
    .local v20, "m":Ljavax/vecmath/GMatrix;
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, v20

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lt v4, v5, :cond_0

    .line 1976
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v26, v0

    .line 1977
    .local v26, "sLength":I
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    add-int/lit8 v16, v4, -0x1

    .line 1983
    .local v16, "eLength":I
    :goto_0
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, v20

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v4, v5, :cond_1

    .line 1984
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v36, v0

    .line 1988
    .local v36, "vecLength":I
    :goto_1
    move/from16 v0, v36

    new-array v0, v0, [D

    move-object/from16 v35, v0

    .line 1989
    .local v35, "vec":[D
    move/from16 v0, v26

    new-array v10, v0, [D

    .line 1990
    .local v10, "single_values":[D
    move/from16 v0, v16

    new-array v7, v0, [D

    .line 1996
    .local v7, "e":[D
    const/16 v25, 0x0

    .line 1998
    .local v25, "rank":I
    invoke-virtual/range {p1 .. p1}, Ljavax/vecmath/GMatrix;->setIdentity()V

    .line 1999
    invoke-virtual/range {p3 .. p3}, Ljavax/vecmath/GMatrix;->setIdentity()V

    .line 2001
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v24, v0

    .line 2002
    .local v24, "nr":I
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v21, v0

    .line 2005
    .local v21, "nc":I
    const/16 v27, 0x0

    .local v27, "si":I
    :goto_2
    move/from16 v0, v27

    move/from16 v1, v26

    if-ge v0, v1, :cond_28

    .line 2008
    const/4 v4, 0x1

    move/from16 v0, v24

    if-le v0, v4, :cond_14

    .line 2015
    const-wide/16 v22, 0x0

    .line 2016
    .local v22, "mag":D
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_3
    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_2

    .line 2017
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v5, v17, v27

    aget-object v4, v4, v5

    aget-wide v4, v4, v27

    move-object/from16 v0, v20

    iget-object v6, v0, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v8, v17, v27

    aget-object v6, v6, v8

    aget-wide v8, v6, v27

    mul-double/2addr v4, v8

    add-double v22, v22, v4

    .line 2016
    add-int/lit8 v17, v17, 0x1

    goto :goto_3

    .line 1979
    .end local v7    # "e":[D
    .end local v10    # "single_values":[D
    .end local v16    # "eLength":I
    .end local v17    # "i":I
    .end local v21    # "nc":I
    .end local v22    # "mag":D
    .end local v24    # "nr":I
    .end local v25    # "rank":I
    .end local v26    # "sLength":I
    .end local v27    # "si":I
    .end local v35    # "vec":[D
    .end local v36    # "vecLength":I
    :cond_0
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v26, v0

    .line 1980
    .restart local v26    # "sLength":I
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v16, v0

    .restart local v16    # "eLength":I
    goto :goto_0

    .line 1986
    :cond_1
    move-object/from16 v0, v20

    iget v0, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v36, v0

    .restart local v36    # "vecLength":I
    goto :goto_1

    .line 2024
    .restart local v7    # "e":[D
    .restart local v10    # "single_values":[D
    .restart local v17    # "i":I
    .restart local v21    # "nc":I
    .restart local v22    # "mag":D
    .restart local v24    # "nr":I
    .restart local v25    # "rank":I
    .restart local v27    # "si":I
    .restart local v35    # "vec":[D
    :cond_2
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    .line 2025
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    aget-wide v4, v4, v27

    const-wide/16 v8, 0x0

    cmpl-double v4, v4, v8

    if-nez v4, :cond_3

    .line 2026
    const/4 v4, 0x0

    aput-wide v22, v35, v4

    .line 2031
    :goto_4
    const/16 v17, 0x1

    :goto_5
    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_4

    .line 2032
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v5, v27, v17

    aget-object v4, v4, v5

    aget-wide v4, v4, v27

    aput-wide v4, v35, v17

    .line 2031
    add-int/lit8 v17, v17, 0x1

    goto :goto_5

    .line 2028
    :cond_3
    const/4 v4, 0x0

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v27

    aget-wide v8, v5, v27

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v27

    aget-wide v38, v5, v27

    move-wide/from16 v0, v22

    move-wide/from16 v2, v38

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v38

    add-double v8, v8, v38

    aput-wide v8, v35, v4

    goto :goto_4

    .line 2035
    :cond_4
    const-wide/16 v28, 0x0

    .line 2036
    .local v28, "scale":D
    const/16 v17, 0x0

    :goto_6
    move/from16 v0, v17

    move/from16 v1, v24

    if-ge v0, v1, :cond_5

    .line 2040
    aget-wide v4, v35, v17

    aget-wide v8, v35, v17

    mul-double/2addr v4, v8

    add-double v28, v28, v4

    .line 2036
    add-int/lit8 v17, v17, 0x1

    goto :goto_6

    .line 2043
    :cond_5
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v28, v4, v28

    .line 2047
    move/from16 v18, v27

    .local v18, "j":I
    :goto_7
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_7

    .line 2048
    move/from16 v19, v27

    .local v19, "k":I
    :goto_8
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_6

    .line 2049
    move-object/from16 v0, v33

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-wide/from16 v0, v28

    neg-double v8, v0

    sub-int v5, v18, v27

    aget-wide v38, v35, v5

    mul-double v8, v8, v38

    sub-int v5, v19, v27

    aget-wide v38, v35, v5

    mul-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2048
    add-int/lit8 v19, v19, 0x1

    goto :goto_8

    .line 2047
    :cond_6
    add-int/lit8 v18, v18, 0x1

    goto :goto_7

    .line 2053
    .end local v19    # "k":I
    :cond_7
    move/from16 v17, v27

    :goto_9
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_8

    .line 2054
    move-object/from16 v0, v33

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v17

    aget-wide v8, v4, v17

    const-wide/high16 v38, 0x3ff0000000000000L    # 1.0

    add-double v8, v8, v38

    aput-wide v8, v4, v17

    .line 2053
    add-int/lit8 v17, v17, 0x1

    goto :goto_9

    .line 2058
    :cond_8
    const-wide/16 v30, 0x0

    .line 2059
    .local v30, "t":D
    move/from16 v17, v27

    :goto_a
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_9

    .line 2060
    move-object/from16 v0, v33

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    aget-wide v4, v4, v17

    move-object/from16 v0, v20

    iget-object v6, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v17

    aget-wide v8, v6, v27

    mul-double/2addr v4, v8

    add-double v30, v30, v4

    .line 2059
    add-int/lit8 v17, v17, 0x1

    goto :goto_a

    .line 2062
    :cond_9
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    aput-wide v30, v4, v27

    .line 2065
    move/from16 v18, v27

    :goto_b
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_c

    .line 2066
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_c
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_b

    .line 2067
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v19

    .line 2068
    move/from16 v17, v27

    :goto_d
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_a

    .line 2069
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    aget-wide v8, v4, v19

    move-object/from16 v0, v33

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v38, v5, v17

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v17

    aget-wide v40, v5, v19

    mul-double v38, v38, v40

    add-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2068
    add-int/lit8 v17, v17, 0x1

    goto :goto_d

    .line 2066
    :cond_a
    add-int/lit8 v19, v19, 0x1

    goto :goto_c

    .line 2065
    :cond_b
    add-int/lit8 v18, v18, 0x1

    goto :goto_b

    .line 2074
    .end local v19    # "k":I
    :cond_c
    move/from16 v18, v27

    :goto_e
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_e

    .line 2075
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_f
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_d

    .line 2076
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-object/from16 v0, v32

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v8, v5, v19

    aput-wide v8, v4, v19

    .line 2075
    add-int/lit8 v19, v19, 0x1

    goto :goto_f

    .line 2074
    :cond_d
    add-int/lit8 v18, v18, 0x1

    goto :goto_e

    .line 2086
    .end local v19    # "k":I
    :cond_e
    move/from16 v18, v27

    :goto_10
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_11

    .line 2087
    const/16 v19, 0x0

    .restart local v19    # "k":I
    :goto_11
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_10

    .line 2088
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v19

    .line 2089
    move/from16 v17, v27

    :goto_12
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_f

    .line 2090
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    aget-wide v8, v4, v19

    move-object/from16 v0, v33

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v38, v5, v17

    move-object/from16 v0, p1

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v17

    aget-wide v40, v5, v19

    mul-double v38, v38, v40

    add-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2089
    add-int/lit8 v17, v17, 0x1

    goto :goto_12

    .line 2087
    :cond_f
    add-int/lit8 v19, v19, 0x1

    goto :goto_11

    .line 2086
    :cond_10
    add-int/lit8 v18, v18, 0x1

    goto :goto_10

    .line 2095
    .end local v19    # "k":I
    :cond_11
    move/from16 v18, v27

    :goto_13
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_13

    .line 2096
    const/16 v19, 0x0

    .restart local v19    # "k":I
    :goto_14
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_12

    .line 2097
    move-object/from16 v0, p1

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-object/from16 v0, v32

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v8, v5, v19

    aput-wide v8, v4, v19

    .line 2096
    add-int/lit8 v19, v19, 0x1

    goto :goto_14

    .line 2095
    :cond_12
    add-int/lit8 v18, v18, 0x1

    goto :goto_13

    .line 2108
    .end local v19    # "k":I
    :cond_13
    add-int/lit8 v24, v24, -0x1

    .line 2111
    .end local v17    # "i":I
    .end local v18    # "j":I
    .end local v22    # "mag":D
    .end local v28    # "scale":D
    .end local v30    # "t":D
    :cond_14
    const/4 v4, 0x2

    move/from16 v0, v21

    if-le v0, v4, :cond_27

    .line 2117
    const-wide/16 v22, 0x0

    .line 2118
    .restart local v22    # "mag":D
    const/16 v17, 0x1

    .restart local v17    # "i":I
    :goto_15
    move/from16 v0, v17

    move/from16 v1, v21

    if-ge v0, v1, :cond_15

    .line 2119
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    add-int v5, v27, v17

    aget-wide v4, v4, v5

    move-object/from16 v0, v20

    iget-object v6, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v27

    add-int v8, v27, v17

    aget-wide v8, v6, v8

    mul-double/2addr v4, v8

    add-double v22, v22, v4

    .line 2118
    add-int/lit8 v17, v17, 0x1

    goto :goto_15

    .line 2127
    :cond_15
    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v22

    .line 2128
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    add-int/lit8 v5, v27, 0x1

    aget-wide v4, v4, v5

    const-wide/16 v8, 0x0

    cmpl-double v4, v4, v8

    if-nez v4, :cond_16

    .line 2129
    const/4 v4, 0x0

    aput-wide v22, v35, v4

    .line 2135
    :goto_16
    const/16 v17, 0x1

    :goto_17
    add-int/lit8 v4, v21, -0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_17

    .line 2136
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    add-int v5, v27, v17

    add-int/lit8 v5, v5, 0x1

    aget-wide v4, v4, v5

    aput-wide v4, v35, v17

    .line 2135
    add-int/lit8 v17, v17, 0x1

    goto :goto_17

    .line 2131
    :cond_16
    const/4 v4, 0x0

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v27

    add-int/lit8 v6, v27, 0x1

    aget-wide v8, v5, v6

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v27

    add-int/lit8 v6, v27, 0x1

    aget-wide v38, v5, v6

    .line 2132
    move-wide/from16 v0, v22

    move-wide/from16 v2, v38

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v38

    add-double v8, v8, v38

    aput-wide v8, v35, v4

    goto :goto_16

    .line 2140
    :cond_17
    const-wide/16 v28, 0x0

    .line 2141
    .restart local v28    # "scale":D
    const/16 v17, 0x0

    :goto_18
    add-int/lit8 v4, v21, -0x1

    move/from16 v0, v17

    if-ge v0, v4, :cond_18

    .line 2143
    aget-wide v4, v35, v17

    aget-wide v8, v35, v17

    mul-double/2addr v4, v8

    add-double v28, v28, v4

    .line 2141
    add-int/lit8 v17, v17, 0x1

    goto :goto_18

    .line 2146
    :cond_18
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v28, v4, v28

    .line 2150
    add-int/lit8 v18, v27, 0x1

    .restart local v18    # "j":I
    :goto_19
    move/from16 v0, v18

    move/from16 v1, v21

    if-ge v0, v1, :cond_1a

    .line 2151
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_1a
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_19

    .line 2152
    move-object/from16 v0, v34

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-wide/from16 v0, v28

    neg-double v8, v0

    sub-int v5, v18, v27

    add-int/lit8 v5, v5, -0x1

    aget-wide v38, v35, v5

    mul-double v8, v8, v38

    sub-int v5, v19, v27

    add-int/lit8 v5, v5, -0x1

    aget-wide v38, v35, v5

    mul-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2151
    add-int/lit8 v19, v19, 0x1

    goto :goto_1a

    .line 2150
    :cond_19
    add-int/lit8 v18, v18, 0x1

    goto :goto_19

    .line 2156
    .end local v19    # "k":I
    :cond_1a
    add-int/lit8 v17, v27, 0x1

    :goto_1b
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_1b

    .line 2157
    move-object/from16 v0, v34

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v17

    aget-wide v8, v4, v17

    const-wide/high16 v38, 0x3ff0000000000000L    # 1.0

    add-double v8, v8, v38

    aput-wide v8, v4, v17

    .line 2156
    add-int/lit8 v17, v17, 0x1

    goto :goto_1b

    .line 2160
    :cond_1b
    const-wide/16 v30, 0x0

    .line 2161
    .restart local v30    # "t":D
    move/from16 v17, v27

    :goto_1c
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_1c

    .line 2162
    move-object/from16 v0, v34

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v17

    add-int/lit8 v5, v27, 0x1

    aget-wide v4, v4, v5

    move-object/from16 v0, v20

    iget-object v6, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v27

    aget-wide v8, v6, v17

    mul-double/2addr v4, v8

    add-double v30, v30, v4

    .line 2161
    add-int/lit8 v17, v17, 0x1

    goto :goto_1c

    .line 2164
    :cond_1c
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v27

    add-int/lit8 v5, v27, 0x1

    aput-wide v30, v4, v5

    .line 2167
    add-int/lit8 v18, v27, 0x1

    :goto_1d
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_1f

    .line 2168
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_1e
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_1e

    .line 2169
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v19

    .line 2170
    add-int/lit8 v17, v27, 0x1

    :goto_1f
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_1d

    .line 2171
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    aget-wide v8, v4, v19

    move-object/from16 v0, v34

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v17

    aget-wide v38, v5, v19

    move-object/from16 v0, v20

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v40, v5, v17

    mul-double v38, v38, v40

    add-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2170
    add-int/lit8 v17, v17, 0x1

    goto :goto_1f

    .line 2168
    :cond_1d
    add-int/lit8 v19, v19, 0x1

    goto :goto_1e

    .line 2167
    :cond_1e
    add-int/lit8 v18, v18, 0x1

    goto :goto_1d

    .line 2176
    .end local v19    # "k":I
    :cond_1f
    add-int/lit8 v18, v27, 0x1

    :goto_20
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_21

    .line 2177
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_21
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_20

    .line 2178
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-object/from16 v0, v32

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v8, v5, v19

    aput-wide v8, v4, v19

    .line 2177
    add-int/lit8 v19, v19, 0x1

    goto :goto_21

    .line 2176
    :cond_20
    add-int/lit8 v18, v18, 0x1

    goto :goto_20

    .line 2189
    .end local v19    # "k":I
    :cond_21
    const/16 v18, 0x0

    :goto_22
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_24

    .line 2190
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_23
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_23

    .line 2191
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    const-wide/16 v8, 0x0

    aput-wide v8, v4, v19

    .line 2192
    add-int/lit8 v17, v27, 0x1

    :goto_24
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v17

    if-ge v0, v4, :cond_22

    .line 2193
    move-object/from16 v0, v32

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    aget-wide v8, v4, v19

    move-object/from16 v0, v34

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v17

    aget-wide v38, v5, v19

    move-object/from16 v0, p3

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v40, v5, v17

    mul-double v38, v38, v40

    add-double v8, v8, v38

    aput-wide v8, v4, v19

    .line 2192
    add-int/lit8 v17, v17, 0x1

    goto :goto_24

    .line 2190
    :cond_22
    add-int/lit8 v19, v19, 0x1

    goto :goto_23

    .line 2189
    :cond_23
    add-int/lit8 v18, v18, 0x1

    goto :goto_22

    .line 2201
    .end local v19    # "k":I
    :cond_24
    const/16 v18, 0x0

    :goto_25
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move/from16 v0, v18

    if-ge v0, v4, :cond_26

    .line 2202
    add-int/lit8 v19, v27, 0x1

    .restart local v19    # "k":I
    :goto_26
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move/from16 v0, v19

    if-ge v0, v4, :cond_25

    .line 2203
    move-object/from16 v0, p3

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v18

    move-object/from16 v0, v32

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v18

    aget-wide v8, v5, v19

    aput-wide v8, v4, v19

    .line 2202
    add-int/lit8 v19, v19, 0x1

    goto :goto_26

    .line 2201
    :cond_25
    add-int/lit8 v18, v18, 0x1

    goto :goto_25

    .line 2212
    .end local v19    # "k":I
    :cond_26
    add-int/lit8 v21, v21, -0x1

    .line 2005
    .end local v17    # "i":I
    .end local v18    # "j":I
    .end local v22    # "mag":D
    .end local v28    # "scale":D
    .end local v30    # "t":D
    :cond_27
    add-int/lit8 v27, v27, 0x1

    goto/16 :goto_2

    .line 2216
    :cond_28
    const/16 v17, 0x0

    .restart local v17    # "i":I
    :goto_27
    move/from16 v0, v17

    move/from16 v1, v26

    if-ge v0, v1, :cond_29

    .line 2217
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v17

    aget-wide v4, v4, v17

    aput-wide v4, v10, v17

    .line 2216
    add-int/lit8 v17, v17, 0x1

    goto :goto_27

    .line 2220
    :cond_29
    const/16 v17, 0x0

    :goto_28
    move/from16 v0, v17

    move/from16 v1, v16

    if-ge v0, v1, :cond_2a

    .line 2221
    move-object/from16 v0, v20

    iget-object v4, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v17

    add-int/lit8 v5, v17, 0x1

    aget-wide v4, v4, v5

    aput-wide v4, v7, v17

    .line 2220
    add-int/lit8 v17, v17, 0x1

    goto :goto_28

    .line 2234
    :cond_2a
    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nRow:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2b

    move-object/from16 v0, v20

    iget v4, v0, Ljavax/vecmath/GMatrix;->nCol:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2b

    .line 2235
    const/4 v4, 0x1

    new-array v12, v4, [D

    .line 2236
    .local v12, "cosl":[D
    const/4 v4, 0x1

    new-array v14, v4, [D

    .line 2237
    .local v14, "cosr":[D
    const/4 v4, 0x1

    new-array v11, v4, [D

    .line 2238
    .local v11, "sinl":[D
    const/4 v4, 0x1

    new-array v13, v4, [D

    .line 2240
    .local v13, "sinr":[D
    const/4 v4, 0x0

    aget-wide v4, v10, v4

    const/4 v6, 0x0

    aget-wide v6, v7, v6

    const/4 v8, 0x1

    aget-wide v8, v10, v8

    const/4 v15, 0x0

    invoke-static/range {v4 .. v15}, Ljavax/vecmath/GMatrix;->compute_2X2(DDD[D[D[D[D[DI)I

    .line 2243
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v4, v0, v12, v11}, Ljavax/vecmath/GMatrix;->update_u(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2244
    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v4, v0, v14, v13}, Ljavax/vecmath/GMatrix;->update_v(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2246
    const/4 v4, 0x2

    .line 2257
    .end local v11    # "sinl":[D
    .end local v12    # "cosl":[D
    .end local v13    # "sinr":[D
    .end local v14    # "cosr":[D
    :goto_29
    return v4

    .line 2250
    :cond_2b
    const/4 v4, 0x0

    array-length v5, v7

    add-int/lit8 v5, v5, -0x1

    move-object v6, v10

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    invoke-static/range {v4 .. v9}, Ljavax/vecmath/GMatrix;->compute_qr(II[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2253
    array-length v0, v10

    move/from16 v25, v0

    move/from16 v4, v25

    .line 2257
    goto :goto_29
.end method

.method static compute_2X2(DDD[D[D[D[D[DI)I
    .locals 64
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "h"    # D
    .param p6, "single_values"    # [D
    .param p7, "snl"    # [D
    .param p8, "csl"    # [D
    .param p9, "snr"    # [D
    .param p10, "csr"    # [D
    .param p11, "index"    # I

    .prologue
    .line 2720
    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    .line 2721
    .local v6, "c_b3":D
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 2733
    .local v8, "c_b4":D
    const/16 v51, 0x0

    aget-wide v46, p6, v51

    .line 2734
    .local v46, "ssmax":D
    const/16 v51, 0x1

    aget-wide v48, p6, v51

    .line 2735
    .local v48, "ssmin":D
    const-wide/16 v10, 0x0

    .line 2736
    .local v10, "clt":D
    const-wide/16 v12, 0x0

    .line 2737
    .local v12, "crt":D
    const-wide/16 v42, 0x0

    .line 2738
    .local v42, "slt":D
    const-wide/16 v44, 0x0

    .line 2739
    .local v44, "srt":D
    const-wide/16 v56, 0x0

    .line 2741
    .local v56, "tsign":D
    move-wide/from16 v20, p0

    .line 2742
    .local v20, "ft":D
    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->abs(D)D

    move-result-wide v18

    .line 2743
    .local v18, "fa":D
    move-wide/from16 v30, p4

    .line 2744
    .local v30, "ht":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    .line 2746
    .local v28, "ha":D
    const/16 v25, 0x1

    .line 2747
    .local v25, "pmax":I
    cmpl-double v51, v28, v18

    if-lez v51, :cond_1

    .line 2748
    const/16 v50, 0x1

    .line 2752
    .local v50, "swap":Z
    :goto_0
    if-eqz v50, :cond_0

    .line 2753
    const/16 v25, 0x3

    .line 2754
    move-wide/from16 v54, v20

    .line 2755
    .local v54, "temp":D
    move-wide/from16 v20, v30

    .line 2756
    move-wide/from16 v30, v54

    .line 2757
    move-wide/from16 v54, v18

    .line 2758
    move-wide/from16 v18, v28

    .line 2759
    move-wide/from16 v28, v54

    .line 2763
    .end local v54    # "temp":D
    :cond_0
    move-wide/from16 v26, p2

    .line 2764
    .local v26, "gt":D
    invoke-static/range {v26 .. v27}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    .line 2765
    .local v22, "ga":D
    const-wide/16 v60, 0x0

    cmpl-double v51, v22, v60

    if-nez v51, :cond_2

    .line 2766
    const/16 v51, 0x1

    aput-wide v28, p6, v51

    .line 2767
    const/16 v51, 0x0

    aput-wide v18, p6, v51

    .line 2768
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2769
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    .line 2770
    const-wide/16 v42, 0x0

    .line 2771
    const-wide/16 v44, 0x0

    .line 2901
    :goto_1
    const/16 v51, 0x0

    return v51

    .line 2750
    .end local v22    # "ga":D
    .end local v26    # "gt":D
    .end local v50    # "swap":Z
    :cond_1
    const/16 v50, 0x0

    .restart local v50    # "swap":Z
    goto :goto_0

    .line 2773
    .restart local v22    # "ga":D
    .restart local v26    # "gt":D
    :cond_2
    const/16 v24, 0x1

    .line 2774
    .local v24, "gasmal":Z
    cmpl-double v51, v22, v18

    if-lez v51, :cond_3

    .line 2775
    const/16 v25, 0x2

    .line 2776
    div-double v60, v18, v22

    const-wide v62, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v51, v60, v62

    if-gez v51, :cond_3

    .line 2777
    const/16 v24, 0x0

    .line 2778
    move-wide/from16 v46, v22

    .line 2780
    const-wide/high16 v60, 0x3ff0000000000000L    # 1.0

    cmpl-double v51, v28, v60

    if-lez v51, :cond_9

    .line 2781
    div-double v60, v22, v28

    div-double v48, v18, v60

    .line 2785
    :goto_2
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2786
    div-double v42, v30, v26

    .line 2787
    const-wide/high16 v44, 0x3ff0000000000000L    # 1.0

    .line 2788
    div-double v12, v20, v26

    .line 2791
    :cond_3
    if-eqz v24, :cond_5

    .line 2792
    sub-double v14, v18, v28

    .line 2793
    .local v14, "d":D
    cmpl-double v51, v14, v18

    if-nez v51, :cond_a

    .line 2795
    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    .line 2800
    .local v32, "l":D
    :goto_3
    div-double v34, v26, v20

    .line 2801
    .local v34, "m":D
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    sub-double v52, v60, v32

    .line 2802
    .local v52, "t":D
    mul-double v36, v34, v34

    .line 2803
    .local v36, "mm":D
    mul-double v58, v52, v52

    .line 2804
    .local v58, "tt":D
    add-double v60, v58, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v40

    .line 2806
    .local v40, "s":D
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_b

    .line 2807
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v38

    .line 2812
    .local v38, "r":D
    :goto_4
    add-double v60, v40, v38

    const-wide/high16 v62, 0x3fe0000000000000L    # 0.5

    mul-double v4, v60, v62

    .line 2813
    .local v4, "a":D
    cmpl-double v51, v22, v18

    if-lez v51, :cond_4

    .line 2814
    const/16 v25, 0x2

    .line 2815
    div-double v60, v18, v22

    const-wide v62, 0x3ddb7cdfd9d7bdbbL    # 1.0E-10

    cmpg-double v51, v60, v62

    if-gez v51, :cond_4

    .line 2816
    const/16 v24, 0x0

    .line 2817
    move-wide/from16 v46, v22

    .line 2818
    const-wide/high16 v60, 0x3ff0000000000000L    # 1.0

    cmpl-double v51, v28, v60

    if-lez v51, :cond_c

    .line 2819
    div-double v60, v22, v28

    div-double v48, v18, v60

    .line 2823
    :goto_5
    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    .line 2824
    div-double v42, v30, v26

    .line 2825
    const-wide/high16 v44, 0x3ff0000000000000L    # 1.0

    .line 2826
    div-double v12, v20, v26

    .line 2829
    :cond_4
    if-eqz v24, :cond_5

    .line 2830
    sub-double v14, v18, v28

    .line 2831
    cmpl-double v51, v14, v18

    if-nez v51, :cond_d

    .line 2832
    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    .line 2837
    :goto_6
    div-double v34, v26, v20

    .line 2838
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    sub-double v52, v60, v32

    .line 2840
    mul-double v36, v34, v34

    .line 2841
    mul-double v58, v52, v52

    .line 2842
    add-double v60, v58, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v40

    .line 2844
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_e

    .line 2845
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->abs(D)D

    move-result-wide v38

    .line 2850
    :goto_7
    add-double v60, v40, v38

    const-wide/high16 v62, 0x3fe0000000000000L    # 0.5

    mul-double v4, v60, v62

    .line 2851
    div-double v48, v28, v4

    .line 2852
    mul-double v46, v18, v4

    .line 2854
    const-wide/16 v60, 0x0

    cmpl-double v51, v36, v60

    if-nez v51, :cond_10

    .line 2855
    const-wide/16 v60, 0x0

    cmpl-double v51, v32, v60

    if-nez v51, :cond_f

    .line 2856
    move-wide/from16 v0, v20

    invoke-static {v6, v7, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    move-wide/from16 v0, v26

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v52, v60, v62

    .line 2864
    :goto_8
    mul-double v60, v52, v52

    const-wide/high16 v62, 0x4010000000000000L    # 4.0

    add-double v60, v60, v62

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v32

    .line 2865
    const-wide/high16 v60, 0x4000000000000000L    # 2.0

    div-double v12, v60, v32

    .line 2866
    div-double v44, v52, v32

    .line 2867
    mul-double v60, v44, v34

    add-double v60, v60, v12

    div-double v10, v60, v4

    .line 2868
    div-double v60, v30, v20

    mul-double v60, v60, v44

    div-double v42, v60, v4

    .line 2871
    .end local v4    # "a":D
    .end local v14    # "d":D
    .end local v32    # "l":D
    .end local v34    # "m":D
    .end local v36    # "mm":D
    .end local v38    # "r":D
    .end local v40    # "s":D
    .end local v52    # "t":D
    .end local v58    # "tt":D
    :cond_5
    if-eqz v50, :cond_11

    .line 2872
    const/16 v51, 0x0

    aput-wide v44, p8, v51

    .line 2873
    const/16 v51, 0x0

    aput-wide v12, p7, v51

    .line 2874
    const/16 v51, 0x0

    aput-wide v42, p10, v51

    .line 2875
    const/16 v51, 0x0

    aput-wide v10, p9, v51

    .line 2883
    :goto_9
    const/16 v51, 0x1

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_6

    .line 2884
    const/16 v51, 0x0

    aget-wide v60, p10, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p8, v51

    .line 2885
    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p0

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2887
    :cond_6
    const/16 v51, 0x2

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_7

    .line 2888
    const/16 v51, 0x0

    aget-wide v60, p9, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p8, v51

    .line 2889
    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p2

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2891
    :cond_7
    const/16 v51, 0x3

    move/from16 v0, v25

    move/from16 v1, v51

    if-ne v0, v1, :cond_8

    .line 2892
    const/16 v51, 0x0

    aget-wide v60, p9, v51

    move-wide/from16 v0, v60

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    const/16 v51, 0x0

    aget-wide v62, p7, v51

    .line 2893
    move-wide/from16 v0, v62

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v60, v60, v62

    move-wide/from16 v0, p4

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v56, v60, v62

    .line 2896
    :cond_8
    move-wide/from16 v0, v46

    move-wide/from16 v2, v56

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    aput-wide v60, p6, p11

    .line 2897
    move-wide/from16 v0, p0

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    mul-double v60, v60, v56

    move-wide/from16 v0, p4

    invoke-static {v8, v9, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v62

    mul-double v16, v60, v62

    .line 2898
    .local v16, "d__1":D
    add-int/lit8 v51, p11, 0x1

    move-wide/from16 v0, v48

    move-wide/from16 v2, v16

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    aput-wide v60, p6, v51

    goto/16 :goto_1

    .line 2783
    .end local v16    # "d__1":D
    :cond_9
    div-double v60, v18, v22

    mul-double v48, v60, v28

    goto/16 :goto_2

    .line 2797
    .restart local v14    # "d":D
    :cond_a
    div-double v32, v14, v18

    .restart local v32    # "l":D
    goto/16 :goto_3

    .line 2809
    .restart local v34    # "m":D
    .restart local v36    # "mm":D
    .restart local v40    # "s":D
    .restart local v52    # "t":D
    .restart local v58    # "tt":D
    :cond_b
    mul-double v60, v32, v32

    add-double v60, v60, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    .restart local v38    # "r":D
    goto/16 :goto_4

    .line 2821
    .restart local v4    # "a":D
    :cond_c
    div-double v60, v18, v22

    mul-double v48, v60, v28

    goto/16 :goto_5

    .line 2834
    :cond_d
    div-double v32, v14, v18

    goto/16 :goto_6

    .line 2847
    :cond_e
    mul-double v60, v32, v32

    add-double v60, v60, v36

    invoke-static/range {v60 .. v61}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v38

    goto/16 :goto_7

    .line 2858
    :cond_f
    move-wide/from16 v0, v20

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v60

    div-double v60, v26, v60

    div-double v62, v34, v52

    add-double v52, v60, v62

    goto/16 :goto_8

    .line 2861
    :cond_10
    add-double v60, v40, v52

    div-double v60, v34, v60

    add-double v62, v38, v32

    div-double v62, v34, v62

    add-double v60, v60, v62

    const-wide/high16 v62, 0x3ff0000000000000L    # 1.0

    add-double v62, v62, v4

    mul-double v52, v60, v62

    goto/16 :goto_8

    .line 2877
    .end local v4    # "a":D
    .end local v14    # "d":D
    .end local v32    # "l":D
    .end local v34    # "m":D
    .end local v36    # "mm":D
    .end local v38    # "r":D
    .end local v40    # "s":D
    .end local v52    # "t":D
    .end local v58    # "tt":D
    :cond_11
    const/16 v51, 0x0

    aput-wide v10, p8, v51

    .line 2878
    const/16 v51, 0x0

    aput-wide v42, p7, v51

    .line 2879
    const/16 v51, 0x0

    aput-wide v12, p10, v51

    .line 2880
    const/16 v51, 0x0

    aput-wide v44, p9, v51

    goto/16 :goto_9
.end method

.method static compute_qr(II[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 40
    .param p0, "start"    # I
    .param p1, "end"    # I
    .param p2, "s"    # [D
    .param p3, "e"    # [D
    .param p4, "u"    # Ljavax/vecmath/GMatrix;
    .param p5, "v"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2266
    const/4 v8, 0x1

    new-array v0, v8, [D

    move-object/from16 v16, v0

    .line 2267
    .local v16, "cosl":[D
    const/4 v8, 0x1

    new-array v0, v8, [D

    move-object/from16 v18, v0

    .line 2268
    .local v18, "cosr":[D
    const/4 v8, 0x1

    new-array v15, v8, [D

    .line 2269
    .local v15, "sinl":[D
    const/4 v8, 0x1

    new-array v0, v8, [D

    move-object/from16 v17, v0

    .line 2270
    .local v17, "sinr":[D
    new-instance v30, Ljavax/vecmath/GMatrix;

    move-object/from16 v0, p4

    iget v8, v0, Ljavax/vecmath/GMatrix;->nCol:I

    move-object/from16 v0, p5

    iget v9, v0, Ljavax/vecmath/GMatrix;->nRow:I

    move-object/from16 v0, v30

    invoke-direct {v0, v8, v9}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2272
    .local v30, "m":Ljavax/vecmath/GMatrix;
    const/16 v22, 0x2

    .line 2273
    .local v22, "MAX_INTERATIONS":I
    const-wide v20, 0x3cf605c9419ea60aL    # 4.89E-15

    .line 2297
    .local v20, "CONVERGE_TOL":D
    const-wide/high16 v24, 0x3ff0000000000000L    # 1.0

    .line 2298
    .local v24, "c_b48":D
    const-wide/high16 v26, -0x4010000000000000L    # -1.0

    .line 2299
    .local v26, "c_b71":D
    const/16 v23, 0x0

    .line 2304
    .local v23, "converged":Z
    const-wide/16 v4, 0x0

    .line 2305
    .local v4, "f":D
    const-wide/16 v6, 0x0

    .line 2307
    .local v6, "g":D
    const/16 v29, 0x0

    .local v29, "k":I
    :goto_0
    const/4 v8, 0x2

    move/from16 v0, v29

    if-ge v0, v8, :cond_a

    if-nez v23, :cond_a

    .line 2308
    move/from16 v28, p0

    .local v28, "i":I
    :goto_1
    move/from16 v0, v28

    move/from16 v1, p1

    if-gt v0, v1, :cond_4

    .line 2311
    move/from16 v0, v28

    move/from16 v1, p0

    if-ne v0, v1, :cond_0

    .line 2312
    move-object/from16 v0, p3

    array-length v8, v0

    move-object/from16 v0, p2

    array-length v9, v0

    if-ne v8, v9, :cond_3

    .line 2313
    move/from16 v36, p1

    .line 2317
    .local v36, "sl":I
    :goto_2
    add-int/lit8 v8, v36, -0x1

    aget-wide v4, p2, v8

    .end local v4    # "f":D
    aget-wide v6, p3, p1

    .end local v6    # "g":D
    aget-wide v8, p2, v36

    invoke-static/range {v4 .. v9}, Ljavax/vecmath/GMatrix;->compute_shift(DDD)D

    move-result-wide v34

    .line 2319
    .local v34, "shift":D
    aget-wide v8, p2, v28

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    sub-double v8, v8, v34

    aget-wide v10, p2, v28

    .line 2320
    move-wide/from16 v0, v24

    invoke-static {v0, v1, v10, v11}, Ljavax/vecmath/GMatrix;->d_sign(DD)D

    move-result-wide v10

    aget-wide v12, p2, v28

    div-double v12, v34, v12

    add-double/2addr v10, v12

    mul-double v4, v8, v10

    .line 2321
    .restart local v4    # "f":D
    aget-wide v6, p3, v28

    .end local v34    # "shift":D
    .end local v36    # "sl":I
    .restart local v6    # "g":D
    :cond_0
    move-object/from16 v8, v17

    move-object/from16 v9, v18

    .line 2324
    invoke-static/range {v4 .. v9}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v32

    .line 2325
    .local v32, "r":D
    move/from16 v0, v28

    move/from16 v1, p0

    if-eq v0, v1, :cond_1

    .line 2326
    add-int/lit8 v8, v28, -0x1

    aput-wide v32, p3, v8

    .line 2328
    :cond_1
    const/4 v8, 0x0

    aget-wide v8, v18, v8

    aget-wide v10, p2, v28

    mul-double/2addr v8, v10

    const/4 v10, 0x0

    aget-wide v10, v17, v10

    aget-wide v12, p3, v28

    mul-double/2addr v10, v12

    add-double v4, v8, v10

    .line 2329
    const/4 v8, 0x0

    aget-wide v8, v18, v8

    aget-wide v10, p3, v28

    mul-double/2addr v8, v10

    const/4 v10, 0x0

    aget-wide v10, v17, v10

    aget-wide v12, p2, v28

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, p3, v28

    .line 2330
    const/4 v8, 0x0

    aget-wide v8, v17, v8

    add-int/lit8 v10, v28, 0x1

    aget-wide v10, p2, v10

    mul-double v6, v8, v10

    .line 2331
    add-int/lit8 v8, v28, 0x1

    const/4 v9, 0x0

    aget-wide v10, v18, v9

    add-int/lit8 v9, v28, 0x1

    aget-wide v12, p2, v9

    mul-double/2addr v10, v12

    aput-wide v10, p2, v8

    .line 2334
    move/from16 v0, v28

    move-object/from16 v1, p5

    move-object/from16 v2, v18

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->update_v(ILjavax/vecmath/GMatrix;[D[D)V

    move-object v8, v15

    move-object/from16 v9, v16

    .line 2338
    invoke-static/range {v4 .. v9}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v32

    .line 2339
    aput-wide v32, p2, v28

    .line 2340
    const/4 v8, 0x0

    aget-wide v8, v16, v8

    aget-wide v10, p3, v28

    mul-double/2addr v8, v10

    const/4 v10, 0x0

    aget-wide v10, v15, v10

    add-int/lit8 v12, v28, 0x1

    aget-wide v12, p2, v12

    mul-double/2addr v10, v12

    add-double v4, v8, v10

    .line 2341
    add-int/lit8 v8, v28, 0x1

    const/4 v9, 0x0

    aget-wide v10, v16, v9

    add-int/lit8 v9, v28, 0x1

    aget-wide v12, p2, v9

    mul-double/2addr v10, v12

    const/4 v9, 0x0

    aget-wide v12, v15, v9

    aget-wide v38, p3, v28

    mul-double v12, v12, v38

    sub-double/2addr v10, v12

    aput-wide v10, p2, v8

    .line 2343
    move/from16 v0, v28

    move/from16 v1, p1

    if-ge v0, v1, :cond_2

    .line 2345
    const/4 v8, 0x0

    aget-wide v8, v15, v8

    add-int/lit8 v10, v28, 0x1

    aget-wide v10, p3, v10

    mul-double v6, v8, v10

    .line 2346
    add-int/lit8 v8, v28, 0x1

    const/4 v9, 0x0

    aget-wide v10, v16, v9

    add-int/lit8 v9, v28, 0x1

    aget-wide v12, p3, v9

    mul-double/2addr v10, v12

    aput-wide v10, p3, v8

    .line 2350
    :cond_2
    move/from16 v0, v28

    move-object/from16 v1, p4

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2, v15}, Ljavax/vecmath/GMatrix;->update_u(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2308
    add-int/lit8 v28, v28, 0x1

    goto/16 :goto_1

    .line 2315
    .end local v32    # "r":D
    :cond_3
    add-int/lit8 v36, p1, 0x1

    .restart local v36    # "sl":I
    goto/16 :goto_2

    .line 2356
    .end local v36    # "sl":I
    :cond_4
    move-object/from16 v0, p2

    array-length v8, v0

    move-object/from16 v0, p3

    array-length v9, v0

    if-ne v8, v9, :cond_5

    move-object/from16 v8, v17

    move-object/from16 v9, v18

    .line 2357
    invoke-static/range {v4 .. v9}, Ljavax/vecmath/GMatrix;->compute_rot(DD[D[D)D

    move-result-wide v32

    .line 2358
    .restart local v32    # "r":D
    const/4 v8, 0x0

    aget-wide v8, v18, v8

    aget-wide v10, p2, v28

    mul-double/2addr v8, v10

    const/4 v10, 0x0

    aget-wide v10, v17, v10

    aget-wide v12, p3, v28

    mul-double/2addr v10, v12

    add-double v4, v8, v10

    .line 2359
    const/4 v8, 0x0

    aget-wide v8, v18, v8

    aget-wide v10, p3, v28

    mul-double/2addr v8, v10

    const/4 v10, 0x0

    aget-wide v10, v17, v10

    aget-wide v12, p2, v28

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, p3, v28

    .line 2360
    add-int/lit8 v8, v28, 0x1

    const/4 v9, 0x0

    aget-wide v10, v18, v9

    add-int/lit8 v9, v28, 0x1

    aget-wide v12, p2, v9

    mul-double/2addr v10, v12

    aput-wide v10, p2, v8

    .line 2362
    move/from16 v0, v28

    move-object/from16 v1, p5

    move-object/from16 v2, v18

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->update_v(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2375
    .end local v32    # "r":D
    :cond_5
    :goto_3
    sub-int v8, p1, p0

    const/4 v9, 0x1

    if-le v8, v9, :cond_6

    aget-wide v8, p3, p1

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v8, v8, v10

    if-gez v8, :cond_6

    .line 2376
    add-int/lit8 p1, p1, -0x1

    goto :goto_3

    .line 2380
    :cond_6
    add-int/lit8 v31, p1, -0x2

    .local v31, "n":I
    :goto_4
    move/from16 v0, v31

    move/from16 v1, p0

    if-le v0, v1, :cond_8

    .line 2381
    aget-wide v8, p3, v31

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v8, v8, v10

    if-gez v8, :cond_7

    .line 2382
    add-int/lit8 v8, v31, 0x1

    move/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v12, p4

    move-object/from16 v13, p5

    invoke-static/range {v8 .. v13}, Ljavax/vecmath/GMatrix;->compute_qr(II[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2383
    add-int/lit8 p1, v31, -0x1

    .line 2386
    :goto_5
    sub-int v8, p1, p0

    const/4 v9, 0x1

    if-le v8, v9, :cond_7

    aget-wide v8, p3, p1

    .line 2387
    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v8, v8, v10

    if-gez v8, :cond_7

    .line 2388
    add-int/lit8 p1, p1, -0x1

    goto :goto_5

    .line 2380
    :cond_7
    add-int/lit8 v31, v31, -0x1

    goto :goto_4

    .line 2396
    :cond_8
    sub-int v8, p1, p0

    const/4 v9, 0x1

    if-gt v8, v9, :cond_9

    add-int/lit8 v8, p0, 0x1

    aget-wide v8, p3, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v8, v8, v10

    if-gez v8, :cond_9

    .line 2397
    const/16 v23, 0x1

    .line 2307
    :cond_9
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_0

    .line 2407
    .end local v28    # "i":I
    .end local v31    # "n":I
    :cond_a
    const/4 v8, 0x1

    aget-wide v8, p3, v8

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v8

    const-wide v10, 0x3cf605c9419ea60aL    # 4.89E-15

    cmpg-double v8, v8, v10

    if-gez v8, :cond_b

    .line 2408
    aget-wide v8, p2, p0

    aget-wide v10, p3, p0

    add-int/lit8 v12, p0, 0x1

    aget-wide v12, p2, v12

    const/16 v19, 0x0

    move-object/from16 v14, p2

    invoke-static/range {v8 .. v19}, Ljavax/vecmath/GMatrix;->compute_2X2(DDD[D[D[D[D[DI)I

    .line 2410
    const-wide/16 v8, 0x0

    aput-wide v8, p3, p0

    .line 2411
    add-int/lit8 v8, p0, 0x1

    const-wide/16 v10, 0x0

    aput-wide v10, p3, v8

    .line 2415
    :cond_b
    move/from16 v28, p0

    .line 2416
    .restart local v28    # "i":I
    move/from16 v0, v28

    move-object/from16 v1, p4

    move-object/from16 v2, v16

    invoke-static {v0, v1, v2, v15}, Ljavax/vecmath/GMatrix;->update_u(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2417
    move/from16 v0, v28

    move-object/from16 v1, p5

    move-object/from16 v2, v18

    move-object/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Ljavax/vecmath/GMatrix;->update_v(ILjavax/vecmath/GMatrix;[D[D)V

    .line 2425
    return-void
.end method

.method static compute_rot(DD[D[D)D
    .locals 26
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "sin"    # [D
    .param p5, "cos"    # [D

    .prologue
    .line 2913
    const-wide/high16 v14, 0x21b0000000000000L    # 2.002083095183101E-146

    .line 2914
    .local v14, "safmn2":D
    const-wide/high16 v16, 0x5e30000000000000L    # 4.9947976805055876E145

    .line 2916
    .local v16, "safmx2":D
    const-wide/16 v22, 0x0

    cmpl-double v11, p2, v22

    if-nez v11, :cond_1

    .line 2917
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 2918
    .local v4, "cs":D
    const-wide/16 v20, 0x0

    .line 2919
    .local v20, "sn":D
    move-wide/from16 v12, p0

    .line 2969
    .local v12, "r":D
    :cond_0
    :goto_0
    const/4 v11, 0x0

    aput-wide v20, p4, v11

    .line 2970
    const/4 v11, 0x0

    aput-wide v4, p5, v11

    .line 2971
    return-wide v12

    .line 2920
    .end local v4    # "cs":D
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_1
    const-wide/16 v22, 0x0

    cmpl-double v11, p0, v22

    if-nez v11, :cond_2

    .line 2921
    const-wide/16 v4, 0x0

    .line 2922
    .restart local v4    # "cs":D
    const-wide/high16 v20, 0x3ff0000000000000L    # 1.0

    .line 2923
    .restart local v20    # "sn":D
    move-wide/from16 v12, p2

    .restart local v12    # "r":D
    goto :goto_0

    .line 2925
    .end local v4    # "cs":D
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_2
    move-wide/from16 v6, p0

    .line 2926
    .local v6, "f1":D
    move-wide/from16 v8, p2

    .line 2927
    .local v8, "g1":D
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/GMatrix;->max(DD)D

    move-result-wide v18

    .line 2928
    .local v18, "scale":D
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    cmpl-double v11, v18, v22

    if-ltz v11, :cond_4

    .line 2929
    const/4 v2, 0x0

    .line 2930
    .local v2, "count":I
    :goto_1
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    cmpl-double v11, v18, v22

    if-ltz v11, :cond_3

    .line 2931
    add-int/lit8 v2, v2, 0x1

    .line 2932
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v6, v6, v22

    .line 2933
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v8, v8, v22

    .line 2934
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/GMatrix;->max(DD)D

    move-result-wide v18

    goto :goto_1

    .line 2936
    :cond_3
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2937
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 2938
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 2939
    .restart local v20    # "sn":D
    move v10, v2

    .line 2940
    .local v10, "i__1":I
    const/4 v3, 0x1

    .local v3, "i":I
    :goto_2
    if-gt v3, v2, :cond_7

    .line 2941
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v12, v12, v22

    .line 2940
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 2943
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "cs":D
    .end local v10    # "i__1":I
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_4
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    cmpg-double v11, v18, v22

    if-gtz v11, :cond_6

    .line 2944
    const/4 v2, 0x0

    .line 2945
    .restart local v2    # "count":I
    :goto_3
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    cmpg-double v11, v18, v22

    if-gtz v11, :cond_5

    .line 2946
    add-int/lit8 v2, v2, 0x1

    .line 2947
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v6, v6, v22

    .line 2948
    const-wide/high16 v22, 0x5e30000000000000L    # 4.9947976805055876E145

    mul-double v8, v8, v22

    .line 2949
    invoke-static {v6, v7}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    invoke-static/range {v22 .. v25}, Ljavax/vecmath/GMatrix;->max(DD)D

    move-result-wide v18

    goto :goto_3

    .line 2951
    :cond_5
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2952
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 2953
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 2954
    .restart local v20    # "sn":D
    move v10, v2

    .line 2955
    .restart local v10    # "i__1":I
    const/4 v3, 0x1

    .restart local v3    # "i":I
    :goto_4
    if-gt v3, v2, :cond_7

    .line 2956
    const-wide/high16 v22, 0x21b0000000000000L    # 2.002083095183101E-146

    mul-double v12, v12, v22

    .line 2955
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 2959
    .end local v2    # "count":I
    .end local v3    # "i":I
    .end local v4    # "cs":D
    .end local v10    # "i__1":I
    .end local v12    # "r":D
    .end local v20    # "sn":D
    :cond_6
    mul-double v22, v6, v6

    mul-double v24, v8, v8

    add-double v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v12

    .line 2960
    .restart local v12    # "r":D
    div-double v4, v6, v12

    .line 2961
    .restart local v4    # "cs":D
    div-double v20, v8, v12

    .line 2963
    .restart local v20    # "sn":D
    :cond_7
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    cmpl-double v11, v22, v24

    if-lez v11, :cond_0

    const-wide/16 v22, 0x0

    cmpg-double v11, v4, v22

    if-gez v11, :cond_0

    .line 2964
    neg-double v4, v4

    .line 2965
    move-wide/from16 v0, v20

    neg-double v0, v0

    move-wide/from16 v20, v0

    .line 2966
    neg-double v12, v12

    goto/16 :goto_0
.end method

.method static compute_shift(DDD)D
    .locals 34
    .param p0, "f"    # D
    .param p2, "g"    # D
    .param p4, "h"    # D

    .prologue
    .line 2676
    invoke-static/range {p0 .. p1}, Ljava/lang/Math;->abs(D)D

    move-result-wide v14

    .line 2677
    .local v14, "fa":D
    invoke-static/range {p2 .. p3}, Ljava/lang/Math;->abs(D)D

    move-result-wide v20

    .line 2678
    .local v20, "ga":D
    invoke-static/range {p4 .. p5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v22

    .line 2679
    .local v22, "ha":D
    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/GMatrix;->min(DD)D

    move-result-wide v16

    .line 2680
    .local v16, "fhmn":D
    move-wide/from16 v0, v22

    invoke-static {v14, v15, v0, v1}, Ljavax/vecmath/GMatrix;->max(DD)D

    move-result-wide v18

    .line 2682
    .local v18, "fhmx":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v16, v26

    if-nez v26, :cond_1

    .line 2683
    const-wide/16 v24, 0x0

    .line 2684
    .local v24, "ssmin":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v18, v26

    if-nez v26, :cond_0

    .line 2713
    :goto_0
    return-wide v24

    .line 2686
    :cond_0
    invoke-static/range {v18 .. v21}, Ljavax/vecmath/GMatrix;->min(DD)D

    move-result-wide v26

    invoke-static/range {v18 .. v21}, Ljavax/vecmath/GMatrix;->max(DD)D

    move-result-wide v28

    div-double v26, v26, v28

    goto :goto_0

    .line 2689
    .end local v24    # "ssmin":D
    :cond_1
    cmpg-double v26, v20, v18

    if-gez v26, :cond_2

    .line 2690
    div-double v26, v16, v18

    const-wide/high16 v28, 0x3ff0000000000000L    # 1.0

    add-double v2, v26, v28

    .line 2691
    .local v2, "as":D
    sub-double v26, v18, v16

    div-double v4, v26, v18

    .line 2692
    .local v4, "at":D
    div-double v10, v20, v18

    .line 2693
    .local v10, "d__1":D
    mul-double v6, v10, v10

    .line 2694
    .local v6, "au":D
    const-wide/high16 v26, 0x4000000000000000L    # 2.0

    mul-double v28, v2, v2

    add-double v28, v28, v6

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    mul-double v30, v4, v4

    add-double v30, v30, v6

    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    add-double v28, v28, v30

    div-double v8, v26, v28

    .line 2695
    .local v8, "c":D
    mul-double v24, v16, v8

    .restart local v24    # "ssmin":D
    goto :goto_0

    .line 2697
    .end local v2    # "as":D
    .end local v4    # "at":D
    .end local v6    # "au":D
    .end local v8    # "c":D
    .end local v10    # "d__1":D
    .end local v24    # "ssmin":D
    :cond_2
    div-double v6, v18, v20

    .line 2698
    .restart local v6    # "au":D
    const-wide/16 v26, 0x0

    cmpl-double v26, v6, v26

    if-nez v26, :cond_3

    .line 2699
    mul-double v26, v16, v18

    div-double v24, v26, v20

    .restart local v24    # "ssmin":D
    goto :goto_0

    .line 2701
    .end local v24    # "ssmin":D
    :cond_3
    div-double v26, v16, v18

    const-wide/high16 v28, 0x3ff0000000000000L    # 1.0

    add-double v2, v26, v28

    .line 2702
    .restart local v2    # "as":D
    sub-double v26, v18, v16

    div-double v4, v26, v18

    .line 2703
    .restart local v4    # "at":D
    mul-double v10, v2, v6

    .line 2704
    .restart local v10    # "d__1":D
    mul-double v12, v4, v6

    .line 2705
    .local v12, "d__2":D
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-double v28, v10, v10

    const-wide/high16 v30, 0x3ff0000000000000L    # 1.0

    add-double v28, v28, v30

    invoke-static/range {v28 .. v29}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v28

    mul-double v30, v12, v12

    const-wide/high16 v32, 0x3ff0000000000000L    # 1.0

    add-double v30, v30, v32

    .line 2706
    invoke-static/range {v30 .. v31}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v30

    add-double v28, v28, v30

    div-double v8, v26, v28

    .line 2707
    .restart local v8    # "c":D
    mul-double v26, v16, v8

    mul-double v24, v26, v6

    .line 2708
    .restart local v24    # "ssmin":D
    add-double v24, v24, v24

    goto :goto_0
.end method

.method static d_sign(DD)D
    .locals 6
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    const-wide/16 v4, 0x0

    .line 2976
    cmpl-double v2, p0, v4

    if-ltz v2, :cond_0

    move-wide v0, p0

    .line 2977
    .local v0, "x":D
    :goto_0
    cmpl-double v2, p2, v4

    if-ltz v2, :cond_1

    .end local v0    # "x":D
    :goto_1
    return-wide v0

    .line 2976
    :cond_0
    neg-double v0, p0

    goto :goto_0

    .line 2977
    .restart local v0    # "x":D
    :cond_1
    neg-double v0, v0

    goto :goto_1
.end method

.method static luBacksubstitution(I[D[I[D)V
    .locals 20
    .param p0, "dim"    # I
    .param p1, "matrix1"    # [D
    .param p2, "row_perm"    # [I
    .param p3, "matrix2"    # [D

    .prologue
    .line 1920
    const/4 v9, 0x0

    .line 1923
    .local v9, "rp":I
    const/4 v7, 0x0

    .local v7, "k":I
    :goto_0
    move/from16 v0, p0

    if-ge v7, v0, :cond_5

    .line 1925
    move v2, v7

    .line 1926
    .local v2, "cv":I
    const/4 v4, -0x1

    .line 1929
    .local v4, "ii":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    move/from16 v0, p0

    if-ge v3, v0, :cond_2

    .line 1932
    add-int v11, v9, v3

    aget v5, p2, v11

    .line 1933
    .local v5, "ip":I
    mul-int v11, p0, v5

    add-int/2addr v11, v2

    aget-wide v12, p3, v11

    .line 1934
    .local v12, "sum":D
    mul-int v11, p0, v5

    add-int/2addr v11, v2

    mul-int v16, p0, v3

    add-int v16, v16, v2

    aget-wide v16, p3, v16

    aput-wide v16, p3, v11

    .line 1935
    if-ltz v4, :cond_0

    .line 1937
    mul-int v10, v3, p0

    .line 1938
    .local v10, "rv":I
    move v6, v4

    .local v6, "j":I
    :goto_2
    add-int/lit8 v11, v3, -0x1

    if-gt v6, v11, :cond_1

    .line 1939
    add-int v11, v10, v6

    aget-wide v16, p1, v11

    mul-int v11, p0, v6

    add-int/2addr v11, v2

    aget-wide v18, p3, v11

    mul-double v16, v16, v18

    sub-double v12, v12, v16

    .line 1938
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 1942
    .end local v6    # "j":I
    .end local v10    # "rv":I
    :cond_0
    const-wide/16 v16, 0x0

    cmpl-double v11, v12, v16

    if-eqz v11, :cond_1

    .line 1943
    move v4, v3

    .line 1945
    :cond_1
    mul-int v11, p0, v3

    add-int/2addr v11, v2

    aput-wide v12, p3, v11

    .line 1929
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1949
    .end local v5    # "ip":I
    .end local v12    # "sum":D
    :cond_2
    const/4 v3, 0x0

    :goto_3
    move/from16 v0, p0

    if-ge v3, v0, :cond_4

    .line 1950
    add-int/lit8 v11, p0, -0x1

    sub-int v8, v11, v3

    .line 1951
    .local v8, "ri":I
    mul-int v10, p0, v8

    .line 1952
    .restart local v10    # "rv":I
    const-wide/16 v14, 0x0

    .line 1953
    .local v14, "tt":D
    const/4 v6, 0x1

    .restart local v6    # "j":I
    :goto_4
    if-gt v6, v3, :cond_3

    .line 1954
    add-int v11, v10, p0

    sub-int/2addr v11, v6

    aget-wide v16, p1, v11

    sub-int v11, p0, v6

    mul-int v11, v11, p0

    add-int/2addr v11, v2

    aget-wide v18, p3, v11

    mul-double v16, v16, v18

    add-double v14, v14, v16

    .line 1953
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 1956
    :cond_3
    mul-int v11, p0, v8

    add-int/2addr v11, v2

    mul-int v16, p0, v8

    add-int v16, v16, v2

    aget-wide v16, p3, v16

    sub-double v16, v16, v14

    add-int v18, v10, v8

    aget-wide v18, p1, v18

    div-double v16, v16, v18

    aput-wide v16, p3, v11

    .line 1949
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1923
    .end local v6    # "j":I
    .end local v8    # "ri":I
    .end local v10    # "rv":I
    .end local v14    # "tt":D
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    .line 1959
    .end local v2    # "cv":I
    .end local v3    # "i":I
    .end local v4    # "ii":I
    :cond_5
    return-void
.end method

.method static luDecomposition(I[D[I[I)Z
    .locals 30
    .param p0, "dim"    # I
    .param p1, "matrix0"    # [D
    .param p2, "row_perm"    # [I
    .param p3, "even_row_xchg"    # [I

    .prologue
    .line 1768
    move/from16 v0, p0

    new-array v0, v0, [D

    move-object/from16 v18, v0

    .line 1775
    .local v18, "row_scale":[D
    const/16 v16, 0x0

    .line 1776
    .local v16, "ptr":I
    const/16 v19, 0x0

    .line 1777
    .local v19, "rs":I
    const/16 v26, 0x0

    const/16 v27, 0x1

    aput v27, p3, v26

    .line 1780
    move/from16 v4, p0

    .local v4, "i":I
    move/from16 v20, v19

    .end local v19    # "rs":I
    .local v20, "rs":I
    move v5, v4

    .line 1781
    .end local v4    # "i":I
    .local v5, "i":I
    :goto_0
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_2

    .line 1782
    const-wide/16 v2, 0x0

    .line 1785
    .local v2, "big":D
    move/from16 v7, p0

    .local v7, "j":I
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .local v17, "ptr":I
    move v8, v7

    .line 1786
    .end local v7    # "j":I
    .local v8, "j":I
    :goto_1
    add-int/lit8 v7, v8, -0x1

    .end local v8    # "j":I
    .restart local v7    # "j":I
    if-eqz v8, :cond_0

    .line 1787
    add-int/lit8 v16, v17, 0x1

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    aget-wide v24, p1, v17

    .line 1788
    .local v24, "temp":D
    invoke-static/range {v24 .. v25}, Ljava/lang/Math;->abs(D)D

    move-result-wide v24

    .line 1789
    cmpl-double v26, v24, v2

    if-lez v26, :cond_e

    .line 1790
    move-wide/from16 v2, v24

    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto :goto_1

    .line 1795
    .end local v8    # "j":I
    .end local v24    # "temp":D
    .restart local v7    # "j":I
    :cond_0
    const-wide/16 v26, 0x0

    cmpl-double v26, v2, v26

    if-nez v26, :cond_1

    .line 1796
    const/16 v26, 0x0

    move/from16 v16, v17

    .line 1889
    .end local v2    # "big":D
    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    :goto_2
    return v26

    .line 1798
    .end local v16    # "ptr":I
    .restart local v2    # "big":D
    .restart local v17    # "ptr":I
    :cond_1
    add-int/lit8 v19, v20, 0x1

    .end local v20    # "rs":I
    .restart local v19    # "rs":I
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    div-double v26, v26, v2

    aput-wide v26, v18, v20

    move/from16 v20, v19

    .end local v19    # "rs":I
    .restart local v20    # "rs":I
    move/from16 v16, v17

    .end local v17    # "ptr":I
    .restart local v16    # "ptr":I
    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_0

    .line 1802
    .end local v2    # "big":D
    .end local v5    # "i":I
    .end local v7    # "j":I
    .restart local v4    # "i":I
    :cond_2
    const/4 v11, 0x0

    .line 1803
    .local v11, "mtx":I
    const/4 v7, 0x0

    .restart local v7    # "j":I
    :goto_3
    move/from16 v0, p0

    if-ge v7, v0, :cond_d

    .line 1809
    const/4 v4, 0x0

    :goto_4
    if-ge v4, v7, :cond_4

    .line 1810
    mul-int v26, p0, v4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1811
    .local v21, "target":I
    aget-wide v22, p1, v21

    .line 1812
    .local v22, "sum":D
    move v9, v4

    .line 1813
    .local v9, "k":I
    mul-int v26, p0, v4

    add-int v12, v11, v26

    .line 1814
    .local v12, "p1":I
    add-int v14, v11, v7

    .local v14, "p2":I
    move v10, v9

    .line 1815
    .end local v9    # "k":I
    .local v10, "k":I
    :goto_5
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_3

    .line 1816
    aget-wide v26, p1, v12

    aget-wide v28, p1, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1817
    add-int/lit8 v12, v12, 0x1

    .line 1818
    add-int v14, v14, p0

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_5

    .line 1820
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_3
    aput-wide v22, p1, v21

    .line 1809
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1825
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    :cond_4
    const-wide/16 v2, 0x0

    .line 1826
    .restart local v2    # "big":D
    const/4 v6, -0x1

    .line 1827
    .local v6, "imax":I
    move v4, v7

    :goto_6
    move/from16 v0, p0

    if-ge v4, v0, :cond_7

    .line 1828
    mul-int v26, p0, v4

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1829
    .restart local v21    # "target":I
    aget-wide v22, p1, v21

    .line 1830
    .restart local v22    # "sum":D
    move v9, v7

    .line 1831
    .restart local v9    # "k":I
    mul-int v26, p0, v4

    add-int v12, v11, v26

    .line 1832
    .restart local v12    # "p1":I
    add-int v14, v11, v7

    .restart local v14    # "p2":I
    move v10, v9

    .line 1833
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_7
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_5

    .line 1834
    aget-wide v26, p1, v12

    aget-wide v28, p1, v14

    mul-double v26, v26, v28

    sub-double v22, v22, v26

    .line 1835
    add-int/lit8 v12, v12, 0x1

    .line 1836
    add-int v14, v14, p0

    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_7

    .line 1838
    .end local v10    # "k":I
    .restart local v9    # "k":I
    :cond_5
    aput-wide v22, p1, v21

    .line 1841
    aget-wide v26, v18, v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Math;->abs(D)D

    move-result-wide v28

    mul-double v24, v26, v28

    .restart local v24    # "temp":D
    cmpl-double v26, v24, v2

    if-ltz v26, :cond_6

    .line 1842
    move-wide/from16 v2, v24

    .line 1843
    move v6, v4

    .line 1827
    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 1847
    .end local v9    # "k":I
    .end local v12    # "p1":I
    .end local v14    # "p2":I
    .end local v21    # "target":I
    .end local v22    # "sum":D
    .end local v24    # "temp":D
    :cond_7
    if-gez v6, :cond_8

    .line 1848
    new-instance v26, Ljava/lang/RuntimeException;

    const-string v27, "GMatrix24"

    invoke-static/range {v27 .. v27}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    invoke-direct/range {v26 .. v27}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v26

    .line 1852
    :cond_8
    if-eq v7, v6, :cond_a

    .line 1854
    move/from16 v9, p0

    .line 1855
    .restart local v9    # "k":I
    mul-int v26, p0, v6

    add-int v12, v11, v26

    .line 1856
    .restart local v12    # "p1":I
    mul-int v26, p0, v7

    add-int v14, v11, v26

    .restart local v14    # "p2":I
    move v15, v14

    .end local v14    # "p2":I
    .local v15, "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .local v13, "p1":I
    move v10, v9

    .line 1857
    .end local v9    # "k":I
    .restart local v10    # "k":I
    :goto_8
    add-int/lit8 v9, v10, -0x1

    .end local v10    # "k":I
    .restart local v9    # "k":I
    if-eqz v10, :cond_9

    .line 1858
    aget-wide v24, p1, v13

    .line 1859
    .restart local v24    # "temp":D
    add-int/lit8 v12, v13, 0x1

    .end local v13    # "p1":I
    .restart local v12    # "p1":I
    aget-wide v26, p1, v15

    aput-wide v26, p1, v13

    .line 1860
    add-int/lit8 v14, v15, 0x1

    .end local v15    # "p2":I
    .restart local v14    # "p2":I
    aput-wide v24, p1, v15

    move v15, v14

    .end local v14    # "p2":I
    .restart local v15    # "p2":I
    move v13, v12

    .end local v12    # "p1":I
    .restart local v13    # "p1":I
    move v10, v9

    .end local v9    # "k":I
    .restart local v10    # "k":I
    goto :goto_8

    .line 1864
    .end local v10    # "k":I
    .end local v24    # "temp":D
    .restart local v9    # "k":I
    :cond_9
    aget-wide v26, v18, v7

    aput-wide v26, v18, v6

    .line 1865
    const/16 v26, 0x0

    const/16 v27, 0x0

    aget v27, p3, v27

    move/from16 v0, v27

    neg-int v0, v0

    move/from16 v27, v0

    aput v27, p3, v26

    .line 1869
    .end local v9    # "k":I
    .end local v13    # "p1":I
    .end local v15    # "p2":I
    :cond_a
    aput v6, p2, v7

    .line 1872
    mul-int v26, p0, v7

    add-int v26, v26, v11

    add-int v26, v26, v7

    aget-wide v26, p1, v26

    const-wide/16 v28, 0x0

    cmpl-double v26, v26, v28

    if-nez v26, :cond_b

    .line 1873
    const/16 v26, 0x0

    goto/16 :goto_2

    .line 1877
    :cond_b
    add-int/lit8 v26, p0, -0x1

    move/from16 v0, v26

    if-eq v7, v0, :cond_c

    .line 1878
    const-wide/high16 v26, 0x3ff0000000000000L    # 1.0

    mul-int v28, p0, v7

    add-int v28, v28, v11

    add-int v28, v28, v7

    aget-wide v28, p1, v28

    div-double v24, v26, v28

    .line 1879
    .restart local v24    # "temp":D
    add-int/lit8 v26, v7, 0x1

    mul-int v26, v26, p0

    add-int v26, v26, v11

    add-int v21, v26, v7

    .line 1880
    .restart local v21    # "target":I
    add-int/lit8 v26, p0, -0x1

    sub-int v4, v26, v7

    move v5, v4

    .line 1881
    .end local v4    # "i":I
    .restart local v5    # "i":I
    :goto_9
    add-int/lit8 v4, v5, -0x1

    .end local v5    # "i":I
    .restart local v4    # "i":I
    if-eqz v5, :cond_c

    .line 1882
    aget-wide v26, p1, v21

    mul-double v26, v26, v24

    aput-wide v26, p1, v21

    .line 1883
    add-int v21, v21, p0

    move v5, v4

    .end local v4    # "i":I
    .restart local v5    # "i":I
    goto :goto_9

    .line 1803
    .end local v5    # "i":I
    .end local v21    # "target":I
    .end local v24    # "temp":D
    .restart local v4    # "i":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    .line 1889
    .end local v2    # "big":D
    .end local v6    # "imax":I
    :cond_d
    const/16 v26, 0x1

    goto/16 :goto_2

    .end local v11    # "mtx":I
    .restart local v2    # "big":D
    .restart local v24    # "temp":D
    :cond_e
    move/from16 v17, v16

    .end local v16    # "ptr":I
    .restart local v17    # "ptr":I
    move v8, v7

    .end local v7    # "j":I
    .restart local v8    # "j":I
    goto/16 :goto_1
.end method

.method static max(DD)D
    .locals 2
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 2658
    cmpl-double v0, p0, p2

    if-lez v0, :cond_0

    .line 2661
    .end local p0    # "a":D
    :goto_0
    return-wide p0

    .restart local p0    # "a":D
    :cond_0
    move-wide p0, p2

    goto :goto_0
.end method

.method static min(DD)D
    .locals 2
    .param p0, "a"    # D
    .param p2, "b"    # D

    .prologue
    .line 2665
    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    .line 2668
    .end local p0    # "a":D
    :goto_0
    return-wide p0

    .restart local p0    # "a":D
    :cond_0
    move-wide p0, p2

    goto :goto_0
.end method

.method private static print_m(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 4
    .param p0, "m"    # Ljavax/vecmath/GMatrix;
    .param p1, "u"    # Ljavax/vecmath/GMatrix;
    .param p2, "v"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2597
    new-instance v0, Ljavax/vecmath/GMatrix;

    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-direct {v0, v1, v2}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2599
    .local v0, "mtmp":Ljavax/vecmath/GMatrix;
    invoke-virtual {v0, p1, v0}, Ljavax/vecmath/GMatrix;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2600
    invoke-virtual {v0, v0, p2}, Ljavax/vecmath/GMatrix;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2601
    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "\n m = \n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljavax/vecmath/GMatrix;->toString(Ljavax/vecmath/GMatrix;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2603
    return-void
.end method

.method private static print_se([D[D)V
    .locals 6
    .param p0, "s"    # [D
    .param p1, "e"    # [D

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2429
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "\ns ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-wide v2, p0, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-wide v2, p0, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x2

    aget-wide v2, p0, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2430
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "e ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-wide v2, p1, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget-wide v2, p1, v5

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2431
    return-void
.end method

.method private static print_svd([D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p0, "s"    # [D
    .param p1, "e"    # [D
    .param p2, "u"    # Ljavax/vecmath/GMatrix;
    .param p3, "v"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2626
    new-instance v1, Ljavax/vecmath/GMatrix;

    iget v2, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p3, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-direct {v1, v2, v3}, Ljavax/vecmath/GMatrix;-><init>(II)V

    .line 2628
    .local v1, "mtmp":Ljavax/vecmath/GMatrix;
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, " \ns = "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2629
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 2630
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, p0, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2629
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2633
    :cond_0
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v3, " \ne = "

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2634
    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 2635
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-wide v4, p1, v0

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2634
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2638
    :cond_1
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " \nu  = \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Ljavax/vecmath/GMatrix;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2639
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " \nv  = \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p3}, Ljavax/vecmath/GMatrix;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2641
    invoke-virtual {v1}, Ljavax/vecmath/GMatrix;->setIdentity()V

    .line 2642
    const/4 v0, 0x0

    :goto_2
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2643
    iget-object v2, v1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    aget-wide v4, p0, v0

    aput-wide v4, v2, v0

    .line 2642
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2645
    :cond_2
    const/4 v0, 0x0

    :goto_3
    array-length v2, p1

    if-ge v0, v2, :cond_3

    .line 2646
    iget-object v2, v1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    add-int/lit8 v3, v0, 0x1

    aget-wide v4, p1, v0

    aput-wide v4, v2, v3

    .line 2645
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2648
    :cond_3
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " \nm  = \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljavax/vecmath/GMatrix;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2650
    invoke-virtual {v1, p2, v1}, Ljavax/vecmath/GMatrix;->mulTransposeLeft(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2651
    invoke-virtual {v1, v1, p3}, Ljavax/vecmath/GMatrix;->mulTransposeRight(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2653
    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, " \n u.transpose*m*v.transpose  = \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 2654
    invoke-virtual {v1}, Ljavax/vecmath/GMatrix;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2653
    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2655
    return-void
.end method

.method private static toString(Ljavax/vecmath/GMatrix;)Ljava/lang/String;
    .locals 8
    .param p0, "m"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2607
    new-instance v0, Ljava/lang/StringBuffer;

    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x8

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 2610
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v3, :cond_2

    .line 2611
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v3, :cond_1

    .line 2612
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v1

    aget-wide v4, v3, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3e112e0be826d695L    # 1.0E-9

    cmpg-double v3, v4, v6

    if-gez v3, :cond_0

    .line 2613
    const-string v3, "0.0000 "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2611
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2615
    :cond_0
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v1

    aget-wide v4, v3, v2

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 2618
    :cond_1
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 2610
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2620
    .end local v2    # "j":I
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method private static update_u(ILjavax/vecmath/GMatrix;[D[D)V
    .locals 11
    .param p0, "index"    # I
    .param p1, "u"    # Ljavax/vecmath/GMatrix;
    .param p2, "cosl"    # [D
    .param p3, "sinl"    # [D

    .prologue
    const/4 v10, 0x0

    .line 2587
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    iget v1, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_0

    .line 2588
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, p0

    aget-wide v2, v1, v0

    .line 2589
    .local v2, "utemp":D
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, p0

    aget-wide v4, p2, v10

    mul-double/2addr v4, v2

    aget-wide v6, p3, v10

    iget-object v8, p1, Ljavax/vecmath/GMatrix;->values:[[D

    add-int/lit8 v9, p0, 0x1

    aget-object v8, v8, v9

    aget-wide v8, v8, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v0

    .line 2591
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    add-int/lit8 v4, p0, 0x1

    aget-object v1, v1, v4

    aget-wide v4, p3, v10

    neg-double v4, v4

    mul-double/2addr v4, v2

    aget-wide v6, p2, v10

    iget-object v8, p1, Ljavax/vecmath/GMatrix;->values:[[D

    add-int/lit8 v9, p0, 0x1

    aget-object v8, v8, v9

    aget-wide v8, v8, v0

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, v0

    .line 2587
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2594
    .end local v2    # "utemp":D
    :cond_0
    return-void
.end method

.method private static update_u_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 14
    .param p0, "topr"    # I
    .param p1, "bottomr"    # I
    .param p2, "u"    # Ljavax/vecmath/GMatrix;
    .param p3, "cosl"    # [D
    .param p4, "sinl"    # [D
    .param p5, "t"    # Ljavax/vecmath/GMatrix;
    .param p6, "m"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2557
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    move-object/from16 v0, p2

    iget v5, v0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v4, v5, :cond_0

    .line 2558
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, p0

    aget-wide v6, v5, v4

    .line 2559
    .local v6, "utemp":D
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, p0

    const/4 v8, 0x0

    aget-wide v8, p3, v8

    mul-double/2addr v8, v6

    const/4 v10, 0x0

    aget-wide v10, p4, v10

    move-object/from16 v0, p2

    iget-object v12, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v12, v12, p1

    aget-wide v12, v12, v4

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, v5, v4

    .line 2560
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, p1

    const/4 v8, 0x0

    aget-wide v8, p4, v8

    mul-double/2addr v8, v6

    const/4 v10, 0x0

    aget-wide v10, p3, v10

    move-object/from16 v0, p2

    iget-object v12, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v12, v12, p1

    aget-wide v12, v12, v4

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    aput-wide v8, v5, v4

    .line 2557
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2573
    .end local v6    # "utemp":D
    :cond_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nm="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2574
    invoke-static/range {p6 .. p6}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2575
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nu="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2576
    invoke-static/range {p5 .. p5}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2577
    move-object/from16 v0, p6

    move-object/from16 v1, p5

    move-object/from16 v2, p6

    invoke-virtual {v0, v1, v2}, Ljavax/vecmath/GMatrix;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2578
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nt*m="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2579
    invoke-static/range {p6 .. p6}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2580
    return-void
.end method

.method private static update_v(ILjavax/vecmath/GMatrix;[D[D)V
    .locals 12
    .param p0, "index"    # I
    .param p1, "v"    # Ljavax/vecmath/GMatrix;
    .param p2, "cosr"    # [D
    .param p3, "sinr"    # [D

    .prologue
    .line 2438
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v1, :cond_0

    .line 2439
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    aget-wide v2, v1, p0

    .line 2440
    .local v2, "vtemp":D
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    const/4 v4, 0x0

    aget-wide v4, p2, v4

    mul-double/2addr v4, v2

    const/4 v6, 0x0

    aget-wide v6, p3, v6

    iget-object v8, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v8, v8, v0

    add-int/lit8 v9, p0, 0x1

    aget-wide v8, v8, v9

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    aput-wide v4, v1, p0

    .line 2442
    iget-object v1, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    add-int/lit8 v4, p0, 0x1

    const/4 v5, 0x0

    aget-wide v6, p3, v5

    neg-double v6, v6

    mul-double/2addr v6, v2

    const/4 v5, 0x0

    aget-wide v8, p2, v5

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    add-int/lit8 v10, p0, 0x1

    aget-wide v10, v5, v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v1, v4

    .line 2438
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2445
    .end local v2    # "vtemp":D
    :cond_0
    return-void
.end method

.method private static update_v_split(IILjavax/vecmath/GMatrix;[D[DLjavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 14
    .param p0, "topr"    # I
    .param p1, "bottomr"    # I
    .param p2, "v"    # Ljavax/vecmath/GMatrix;
    .param p3, "cosr"    # [D
    .param p4, "sinr"    # [D
    .param p5, "t"    # Ljavax/vecmath/GMatrix;
    .param p6, "m"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 2521
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_0
    move-object/from16 v0, p2

    iget v5, v0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v4, v5, :cond_0

    .line 2522
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v4

    aget-wide v6, v5, p0

    .line 2523
    .local v6, "vtemp":D
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x0

    aget-wide v8, p3, v8

    mul-double/2addr v8, v6

    const/4 v10, 0x0

    aget-wide v10, p4, v10

    move-object/from16 v0, p2

    iget-object v12, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v12, v12, v4

    aget-wide v12, v12, p1

    mul-double/2addr v10, v12

    sub-double/2addr v8, v10

    aput-wide v8, v5, p0

    .line 2524
    move-object/from16 v0, p2

    iget-object v5, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v4

    const/4 v8, 0x0

    aget-wide v8, p4, v8

    mul-double/2addr v8, v6

    const/4 v10, 0x0

    aget-wide v10, p3, v10

    move-object/from16 v0, p2

    iget-object v12, v0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v12, v12, v4

    aget-wide v12, v12, p1

    mul-double/2addr v10, v12

    add-double/2addr v8, v10

    aput-wide v8, v5, p1

    .line 2521
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2538
    .end local v6    # "vtemp":D
    :cond_0
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "topr    ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2539
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "bottomr ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2540
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "cosr ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-wide v10, p3, v9

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2541
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "sinr ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-wide v10, p4, v9

    invoke-virtual {v8, v10, v11}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2542
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nm ="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2543
    invoke-static/range {p6 .. p6}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2544
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nv ="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2545
    invoke-static/range {p5 .. p5}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2546
    move-object/from16 v0, p6

    move-object/from16 v1, p6

    move-object/from16 v2, p5

    invoke-virtual {v0, v1, v2}, Ljavax/vecmath/GMatrix;->mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V

    .line 2547
    sget-object v5, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v8, "\nt*m ="

    invoke-virtual {v5, v8}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 2548
    invoke-static/range {p6 .. p6}, Ljavax/vecmath/GMatrix;->checkMatrix(Ljavax/vecmath/GMatrix;)V

    .line 2549
    return-void
.end method


# virtual methods
.method public final LUD(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GVector;)I
    .locals 10
    .param p1, "LU"    # Ljavax/vecmath/GMatrix;
    .param p2, "permutation"    # Ljavax/vecmath/GVector;

    .prologue
    .line 1617
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v7, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int v4, v6, v7

    .line 1618
    .local v4, "size":I
    new-array v5, v4, [D

    .line 1619
    .local v5, "temp":[D
    const/4 v6, 0x1

    new-array v0, v6, [I

    .line 1620
    .local v0, "even_row_exchange":[I
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    new-array v3, v6, [I

    .line 1623
    .local v3, "row_perm":[I
    iget v6, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v6, v7, :cond_0

    .line 1624
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GMatrix19"

    .line 1625
    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1628
    :cond_0
    iget v6, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v6, v7, :cond_1

    .line 1629
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GMatrix27"

    .line 1630
    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1633
    :cond_1
    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v7, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v6, v7, :cond_2

    .line 1634
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GMatrix27"

    .line 1635
    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1638
    :cond_2
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v7

    if-eq v6, v7, :cond_3

    .line 1639
    new-instance v6, Ljavax/vecmath/MismatchedSizeException;

    const-string v7, "GMatrix20"

    .line 1640
    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1643
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v6, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v6, :cond_5

    .line 1644
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v6, :cond_4

    .line 1645
    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v6, v1

    add-int/2addr v6, v2

    iget-object v7, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v7, v7, v1

    aget-wide v8, v7, v2

    aput-wide v8, v5, v6

    .line 1644
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1643
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1650
    .end local v2    # "j":I
    :cond_5
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-static {v6, v5, v3, v0}, Ljavax/vecmath/GMatrix;->luDecomposition(I[D[I[I)Z

    move-result v6

    if-nez v6, :cond_6

    .line 1652
    new-instance v6, Ljavax/vecmath/SingularMatrixException;

    const-string v7, "GMatrix21"

    .line 1653
    invoke-static {v7}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 1656
    :cond_6
    const/4 v1, 0x0

    :goto_2
    iget v6, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v6, :cond_8

    .line 1657
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_3
    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v6, :cond_7

    .line 1658
    iget-object v6, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v1

    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v7, v1

    add-int/2addr v7, v2

    aget-wide v8, v5, v7

    aput-wide v8, v6, v2

    .line 1657
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1656
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1662
    .end local v2    # "j":I
    :cond_8
    const/4 v1, 0x0

    :goto_4
    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v6, :cond_9

    .line 1663
    iget-object v6, p2, Ljavax/vecmath/GVector;->values:[D

    aget v7, v3, v1

    int-to-double v8, v7

    aput-wide v8, v6, v1

    .line 1662
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1666
    :cond_9
    const/4 v6, 0x0

    aget v6, v0, v6

    return v6
.end method

.method public final SVD(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)I
    .locals 12
    .param p1, "U"    # Ljavax/vecmath/GMatrix;
    .param p2, "W"    # Ljavax/vecmath/GMatrix;
    .param p3, "V"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1539
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v1, p3, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v1, p3, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v0, v1, :cond_1

    .line 1540
    :cond_0
    new-instance v0, Ljavax/vecmath/MismatchedSizeException;

    const-string v1, "GMatrix18"

    .line 1541
    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1544
    :cond_1
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v1, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v0, v1, :cond_2

    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v1, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v0, v1, :cond_3

    .line 1545
    :cond_2
    new-instance v0, Ljavax/vecmath/MismatchedSizeException;

    const-string v1, "GMatrix25"

    .line 1546
    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1549
    :cond_3
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v1, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v1, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v0, v1, :cond_5

    .line 1550
    :cond_4
    new-instance v0, Ljavax/vecmath/MismatchedSizeException;

    const-string v1, "GMatrix26"

    .line 1551
    invoke-static {v1}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1564
    :cond_5
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 1565
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v1, 0x1

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_7

    .line 1566
    invoke-virtual {p1}, Ljavax/vecmath/GMatrix;->setIdentity()V

    .line 1567
    invoke-virtual {p3}, Ljavax/vecmath/GMatrix;->setIdentity()V

    .line 1569
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x1

    aget-wide v0, v0, v1

    const-wide/16 v2, 0x0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_6

    .line 1570
    const/4 v0, 0x2

    .line 1593
    :goto_0
    return v0

    .line 1573
    :cond_6
    const/4 v0, 0x1

    new-array v7, v0, [D

    .line 1574
    .local v7, "sinl":[D
    const/4 v0, 0x1

    new-array v9, v0, [D

    .line 1575
    .local v9, "sinr":[D
    const/4 v0, 0x1

    new-array v8, v0, [D

    .line 1576
    .local v8, "cosl":[D
    const/4 v0, 0x1

    new-array v10, v0, [D

    .line 1577
    .local v10, "cosr":[D
    const/4 v0, 0x2

    new-array v6, v0, [D

    .line 1579
    .local v6, "single_values":[D
    const/4 v0, 0x0

    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v2, 0x0

    aget-object v1, v1, v2

    const/4 v2, 0x0

    aget-wide v2, v1, v2

    aput-wide v2, v6, v0

    .line 1580
    const/4 v0, 0x1

    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/4 v2, 0x1

    aget-wide v2, v1, v2

    aput-wide v2, v6, v0

    .line 1582
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/4 v1, 0x0

    aget-wide v0, v0, v1

    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    aget-wide v2, v2, v3

    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    const/4 v5, 0x1

    aget-object v4, v4, v5

    const/4 v5, 0x1

    aget-wide v4, v4, v5

    const/4 v11, 0x0

    invoke-static/range {v0 .. v11}, Ljavax/vecmath/GMatrix;->compute_2X2(DDD[D[D[D[D[DI)I

    .line 1585
    const/4 v0, 0x0

    invoke-static {v0, p1, v8, v7}, Ljavax/vecmath/GMatrix;->update_u(ILjavax/vecmath/GMatrix;[D[D)V

    .line 1586
    const/4 v0, 0x0

    invoke-static {v0, p3, v10, v9}, Ljavax/vecmath/GMatrix;->update_v(ILjavax/vecmath/GMatrix;[D[D)V

    .line 1588
    const/4 v0, 0x2

    goto :goto_0

    .line 1593
    .end local v6    # "single_values":[D
    .end local v7    # "sinl":[D
    .end local v8    # "cosl":[D
    .end local v9    # "sinr":[D
    .end local v10    # "cosr":[D
    :cond_7
    invoke-static {p0, p1, p2, p3}, Ljavax/vecmath/GMatrix;->computeSVD(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)I

    move-result v0

    goto :goto_0
.end method

.method public final add(Ljavax/vecmath/GMatrix;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 223
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_0

    .line 224
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix4"

    .line 225
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 227
    :cond_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_1

    .line 228
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix5"

    .line 229
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 231
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 232
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 233
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v6, v3, v1

    add-double/2addr v4, v6

    aput-wide v4, v2, v1

    .line 232
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 231
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final add(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 247
    iget v2, p2, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_0

    .line 248
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix6"

    .line 249
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 251
    :cond_0
    iget v2, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_1

    .line 252
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix7"

    .line 253
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 255
    :cond_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_3

    .line 256
    :cond_2
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix8"

    .line 257
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 259
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_5

    .line 260
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_4

    .line 261
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    iget-object v3, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v6, v3, v1

    add-double/2addr v4, v6

    aput-wide v4, v2, v1

    .line 260
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 259
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    .end local v1    # "j":I
    :cond_5
    return-void
.end method

.method public clone()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2989
    const/4 v3, 0x0

    .line 2991
    .local v3, "m1":Ljavax/vecmath/GMatrix;
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "m1":Ljavax/vecmath/GMatrix;
    check-cast v3, Ljavax/vecmath/GMatrix;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2998
    .restart local v3    # "m1":Ljavax/vecmath/GMatrix;
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[D

    iput-object v4, v3, Ljavax/vecmath/GMatrix;->values:[[D

    .line 2999
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v4, :cond_1

    .line 3000
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_0

    .line 3001
    iget-object v4, v3, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v1

    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aget-wide v6, v5, v2

    aput-wide v6, v4, v2

    .line 3000
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2992
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "m1":Ljavax/vecmath/GMatrix;
    :catch_0
    move-exception v0

    .line 2994
    .local v0, "e":Ljava/lang/CloneNotSupportedException;
    new-instance v4, Ljava/lang/InternalError;

    invoke-direct {v4}, Ljava/lang/InternalError;-><init>()V

    throw v4

    .line 2999
    .end local v0    # "e":Ljava/lang/CloneNotSupportedException;
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "m1":Ljavax/vecmath/GMatrix;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3005
    .end local v2    # "j":I
    :cond_1
    return-object v3
.end method

.method public final copySubMatrix(IIIIIILjavax/vecmath/GMatrix;)V
    .locals 8
    .param p1, "rowSource"    # I
    .param p2, "colSource"    # I
    .param p3, "numRow"    # I
    .param p4, "numCol"    # I
    .param p5, "rowDest"    # I
    .param p6, "colDest"    # I
    .param p7, "target"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 451
    if-eq p0, p7, :cond_1

    .line 452
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_5

    .line 453
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    if-ge v1, p4, :cond_0

    .line 454
    iget-object v3, p7, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v4, p5, v0

    aget-object v3, v3, v4

    add-int v4, p6, v1

    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v6, p1, v0

    aget-object v5, v5, v6

    add-int v6, p2, v1

    aget-wide v6, v5, v6

    aput-wide v6, v3, v4

    .line 453
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 452
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 459
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_1
    filled-new-array {p3, p4}, [I

    move-result-object v3

    sget-object v4, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v4, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    .line 460
    .local v2, "tmp":[[D
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    if-ge v0, p3, :cond_3

    .line 461
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_3
    if-ge v1, p4, :cond_2

    .line 462
    aget-object v3, v2, v0

    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v5, p1, v0

    aget-object v4, v4, v5

    add-int v5, p2, v1

    aget-wide v4, v4, v5

    aput-wide v4, v3, v1

    .line 461
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 460
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 465
    .end local v1    # "j":I
    :cond_3
    const/4 v0, 0x0

    :goto_4
    if-ge v0, p3, :cond_5

    .line 466
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_5
    if-ge v1, p4, :cond_4

    .line 467
    iget-object v3, p7, Ljavax/vecmath/GMatrix;->values:[[D

    add-int v4, p5, v0

    aget-object v3, v3, v4

    add-int v4, p6, v1

    aget-object v5, v2, v0

    aget-wide v6, v5, v1

    aput-wide v6, v3, v4

    .line 466
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 465
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 471
    .end local v1    # "j":I
    .end local v2    # "tmp":[[D
    :cond_5
    return-void
.end method

.method public epsilonEquals(Ljavax/vecmath/GMatrix;D)Z
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "epsilon"    # D

    .prologue
    const/4 v4, 0x0

    .line 1486
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v6, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v5, v6, :cond_0

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v6, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v5, v6, :cond_1

    .line 1496
    :cond_0
    :goto_0
    return v4

    .line 1489
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v5, :cond_4

    .line 1490
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v3, v5, :cond_3

    .line 1491
    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v6, v5, v3

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v8, v5, v3

    sub-double v0, v6, v8

    .line 1492
    .local v0, "diff":D
    const-wide/16 v6, 0x0

    cmpg-double v5, v0, v6

    if-gez v5, :cond_2

    neg-double v0, v0

    .end local v0    # "diff":D
    :cond_2
    cmpl-double v5, v0, p2

    if-gtz v5, :cond_0

    .line 1490
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1489
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1496
    .end local v3    # "j":I
    :cond_4
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public epsilonEquals(Ljavax/vecmath/GMatrix;F)Z
    .locals 2
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "epsilon"    # F

    .prologue
    .line 1470
    float-to-double v0, p2

    invoke-virtual {p0, p1, v0, v1}, Ljavax/vecmath/GMatrix;->epsilonEquals(Ljavax/vecmath/GMatrix;D)Z

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o1"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x0

    .line 1445
    :try_start_0
    move-object v0, p1

    check-cast v0, Ljavax/vecmath/GMatrix;

    move-object v6, v0

    .line 1447
    .local v6, "m2":Ljavax/vecmath/GMatrix;
    iget v8, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v9, v6, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v8, v9, :cond_0

    iget v8, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v9, v6, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v8, v9, :cond_1

    .line 1462
    .end local v6    # "m2":Ljavax/vecmath/GMatrix;
    :cond_0
    :goto_0
    return v7

    .line 1450
    .restart local v6    # "m2":Ljavax/vecmath/GMatrix;
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget v8, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v4, v8, :cond_3

    .line 1451
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_2
    iget v8, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v5, v8, :cond_2

    .line 1452
    iget-object v8, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v8, v8, v4

    aget-wide v8, v8, v5

    iget-object v10, v6, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v10, v10, v4

    aget-wide v10, v10, v5
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1

    cmpl-double v8, v8, v10

    if-nez v8, :cond_0

    .line 1451
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1450
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1456
    .end local v5    # "j":I
    :cond_3
    const/4 v7, 0x1

    goto :goto_0

    .line 1458
    .end local v4    # "i":I
    .end local v6    # "m2":Ljavax/vecmath/GMatrix;
    :catch_0
    move-exception v2

    .line 1459
    .local v2, "e1":Ljava/lang/ClassCastException;
    goto :goto_0

    .line 1461
    .end local v2    # "e1":Ljava/lang/ClassCastException;
    :catch_1
    move-exception v3

    .line 1462
    .local v3, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public equals(Ljavax/vecmath/GMatrix;)Z
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    const/4 v3, 0x0

    .line 1419
    :try_start_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v4, v5, :cond_1

    .line 1431
    :cond_0
    :goto_0
    return v3

    .line 1422
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v4, :cond_3

    .line 1423
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_2

    .line 1424
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v1

    aget-wide v4, v4, v2

    iget-object v6, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v1

    aget-wide v6, v6, v2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_0

    .line 1423
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1422
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1428
    .end local v2    # "j":I
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 1430
    .end local v1    # "i":I
    :catch_0
    move-exception v0

    .line 1431
    .local v0, "e2":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/GMatrix;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    const-wide/16 v8, 0x0

    .line 1094
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v4, v5, :cond_0

    .line 1095
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 1099
    .local v2, "nc":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v4, v5, :cond_1

    .line 1100
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 1104
    .local v3, "nr":I
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v3, :cond_3

    .line 1105
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_3
    if-ge v1, v2, :cond_2

    .line 1106
    iget-object v4, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v6, v5, v1

    aput-wide v6, v4, v1

    .line 1105
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1097
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "nc":I
    .end local v3    # "nr":I
    :cond_0
    iget v2, p1, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v2    # "nc":I
    goto :goto_0

    .line 1102
    :cond_1
    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    .restart local v3    # "nr":I
    goto :goto_1

    .line 1104
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1109
    .end local v1    # "j":I
    :cond_3
    move v0, v3

    :goto_4
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_5

    .line 1110
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_5
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_4

    .line 1111
    iget-object v4, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aput-wide v8, v4, v1

    .line 1110
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1109
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1114
    .end local v1    # "j":I
    :cond_5
    move v1, v2

    .restart local v1    # "j":I
    :goto_6
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_7

    .line 1115
    const/4 v0, 0x0

    :goto_7
    if-ge v0, v3, :cond_6

    .line 1116
    iget-object v4, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aput-wide v8, v4, v1

    .line 1115
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 1114
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 1119
    :cond_7
    return-void
.end method

.method public final get(Ljavax/vecmath/Matrix3d;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 799
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_4

    .line 800
    :cond_0
    invoke-virtual {p1}, Ljavax/vecmath/Matrix3d;->setZero()V

    .line 801
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lez v0, :cond_3

    .line 802
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_1

    .line 803
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    .line 804
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_1

    .line 805
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    .line 806
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_1

    .line 807
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    .line 811
    :cond_1
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v2, :cond_3

    .line 812
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_2

    .line 813
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    .line 814
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_2

    .line 815
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    .line 816
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_2

    .line 817
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    .line 821
    :cond_2
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v3, :cond_3

    .line 822
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_3

    .line 823
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 824
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_3

    .line 825
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 826
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_3

    .line 827
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    .line 847
    :cond_3
    :goto_0
    return-void

    .line 835
    :cond_4
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m00:D

    .line 836
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m01:D

    .line 837
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m02:D

    .line 839
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m10:D

    .line 840
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m11:D

    .line 841
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m12:D

    .line 843
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m20:D

    .line 844
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m21:D

    .line 845
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix3d;->m22:D

    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Matrix3f;)V
    .locals 5
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v1, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 857
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_4

    .line 858
    :cond_0
    invoke-virtual {p1}, Ljavax/vecmath/Matrix3f;->setZero()V

    .line 859
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lez v0, :cond_3

    .line 860
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_1

    .line 861
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 862
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_1

    .line 863
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 864
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_1

    .line 865
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 869
    :cond_1
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v2, :cond_3

    .line 870
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_2

    .line 871
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 872
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_2

    .line 873
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 874
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_2

    .line 875
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 879
    :cond_2
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v3, :cond_3

    .line 880
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_3

    .line 881
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 882
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_3

    .line 883
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 884
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_3

    .line 885
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    .line 905
    :cond_3
    :goto_0
    return-void

    .line 893
    :cond_4
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m00:F

    .line 894
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m01:F

    .line 895
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m02:F

    .line 897
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m10:F

    .line 898
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m11:F

    .line 899
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m12:F

    .line 901
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m20:F

    .line 902
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m21:F

    .line 903
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix3f;->m22:F

    goto :goto_0
.end method

.method public final get(Ljavax/vecmath/Matrix4d;)V
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 914
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_5

    .line 915
    :cond_0
    invoke-virtual {p1}, Ljavax/vecmath/Matrix4d;->setZero()V

    .line 916
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lez v0, :cond_4

    .line 917
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_1

    .line 918
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    .line 919
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_1

    .line 920
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    .line 921
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_1

    .line 922
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    .line 923
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_1

    .line 924
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    .line 929
    :cond_1
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v2, :cond_4

    .line 930
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_2

    .line 931
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    .line 932
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_2

    .line 933
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    .line 934
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_2

    .line 935
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    .line 936
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_2

    .line 937
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    .line 942
    :cond_2
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v3, :cond_4

    .line 943
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_3

    .line 944
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    .line 945
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_3

    .line 946
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    .line 947
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_3

    .line 948
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    .line 949
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_3

    .line 950
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    .line 955
    :cond_3
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v4, :cond_4

    .line 956
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_4

    .line 957
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    .line 958
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_4

    .line 959
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    .line 960
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_4

    .line 961
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    .line 962
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_4

    .line 963
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    .line 994
    :cond_4
    :goto_0
    return-void

    .line 973
    :cond_5
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m00:D

    .line 974
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m01:D

    .line 975
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m02:D

    .line 976
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m03:D

    .line 978
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m10:D

    .line 979
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m11:D

    .line 980
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m12:D

    .line 981
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m13:D

    .line 983
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m20:D

    .line 984
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m21:D

    .line 985
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m22:D

    .line 986
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m23:D

    .line 988
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v5

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m30:D

    .line 989
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m31:D

    .line 990
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m32:D

    .line 991
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    iput-wide v0, p1, Ljavax/vecmath/Matrix4d;->m33:D

    goto/16 :goto_0
.end method

.method public final get(Ljavax/vecmath/Matrix4f;)V
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const/4 v1, 0x4

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 1004
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v0, v1, :cond_0

    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_5

    .line 1005
    :cond_0
    invoke-virtual {p1}, Ljavax/vecmath/Matrix4f;->setZero()V

    .line 1006
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lez v0, :cond_4

    .line 1007
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_1

    .line 1008
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1009
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_1

    .line 1010
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1011
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_1

    .line 1012
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1013
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_1

    .line 1014
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1019
    :cond_1
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v2, :cond_4

    .line 1020
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_2

    .line 1021
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1022
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_2

    .line 1023
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1024
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_2

    .line 1025
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1026
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_2

    .line 1027
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1032
    :cond_2
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v3, :cond_4

    .line 1033
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_3

    .line 1034
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1035
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_3

    .line 1036
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1037
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_3

    .line 1038
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1039
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_3

    .line 1040
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1045
    :cond_3
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-le v0, v4, :cond_4

    .line 1046
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lez v0, :cond_4

    .line 1047
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1048
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v2, :cond_4

    .line 1049
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1050
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v3, :cond_4

    .line 1051
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1052
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-le v0, v4, :cond_4

    .line 1053
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    .line 1083
    :cond_4
    :goto_0
    return-void

    .line 1063
    :cond_5
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m00:F

    .line 1064
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m01:F

    .line 1065
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m02:F

    .line 1066
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v5

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m03:F

    .line 1068
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m10:F

    .line 1069
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m11:F

    .line 1070
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m12:F

    .line 1071
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v2

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m13:F

    .line 1073
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m20:F

    .line 1074
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m21:F

    .line 1075
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m22:F

    .line 1076
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v3

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m23:F

    .line 1078
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v5

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m30:F

    .line 1079
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v2

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m31:F

    .line 1080
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v3

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m32:F

    .line 1081
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, v4

    aget-wide v0, v0, v4

    double-to-float v0, v0

    iput v0, p1, Ljavax/vecmath/Matrix4f;->m33:F

    goto/16 :goto_0
.end method

.method public final getColumn(ILjavax/vecmath/GVector;)V
    .locals 4
    .param p1, "col"    # I
    .param p2, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 784
    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v1

    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v2, :cond_0

    .line 785
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p2, v1}, Ljavax/vecmath/GVector;->setSize(I)V

    .line 787
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v1, :cond_1

    .line 788
    iget-object v1, p2, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    aget-wide v2, v2, p1

    aput-wide v2, v1, v0

    .line 787
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 790
    :cond_1
    return-void
.end method

.method public final getColumn(I[D)V
    .locals 4
    .param p1, "col"    # I
    .param p2, "array"    # [D

    .prologue
    .line 771
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v1, :cond_0

    .line 772
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    aget-wide v2, v1, p1

    aput-wide v2, p2, v0

    .line 771
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 775
    :cond_0
    return-void
.end method

.method public final getElement(II)D
    .locals 2
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 722
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, p1

    aget-wide v0, v0, p2

    return-wide v0
.end method

.method public final getNumCol()I
    .locals 1

    .prologue
    .line 711
    iget v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    return v0
.end method

.method public final getNumRow()I
    .locals 1

    .prologue
    .line 702
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    return v0
.end method

.method public final getRow(ILjavax/vecmath/GVector;)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 756
    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v1

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_0

    .line 757
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-virtual {p2, v1}, Ljavax/vecmath/GVector;->setSize(I)V

    .line 759
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_1

    .line 760
    iget-object v1, p2, Ljavax/vecmath/GVector;->values:[D

    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, p1

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    .line 759
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 762
    :cond_1
    return-void
.end method

.method public final getRow(I[D)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "array"    # [D

    .prologue
    .line 744
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_0

    .line 745
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, p1

    aget-wide v2, v1, v0

    aput-wide v2, p2, v0

    .line 744
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 747
    :cond_0
    return-void
.end method

.method public hashCode()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x1f

    .line 1393
    const-wide/16 v0, 0x1

    .line 1395
    .local v0, "bits":J
    mul-long v4, v8, v0

    iget v6, p0, Ljavax/vecmath/GMatrix;->nRow:I

    int-to-long v6, v6

    add-long v0, v4, v6

    .line 1396
    mul-long v4, v8, v0

    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    int-to-long v6, v6

    add-long v0, v4, v6

    .line 1398
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v4, :cond_1

    .line 1399
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v3, v4, :cond_0

    .line 1400
    mul-long v4, v8, v0

    iget-object v6, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v2

    aget-wide v6, v6, v3

    invoke-static {v6, v7}, Ljavax/vecmath/VecMathUtil;->doubleToLongBits(D)J

    move-result-wide v6

    add-long v0, v4, v6

    .line 1399
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1398
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1404
    .end local v3    # "j":I
    :cond_1
    const/16 v4, 0x20

    shr-long v4, v0, v4

    xor-long/2addr v4, v0

    long-to-int v4, v4

    return v4
.end method

.method public final identityMinus()V
    .locals 8

    .prologue
    .line 393
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v3, :cond_1

    .line 394
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v3, :cond_0

    .line 395
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v4, v4, v1

    neg-double v4, v4

    aput-wide v4, v3, v1

    .line 394
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 393
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 400
    .end local v1    # "j":I
    :cond_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v3, v4, :cond_2

    .line 401
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 405
    .local v2, "l":I
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_3

    .line 406
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    aput-wide v4, v3, v0

    .line 405
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 403
    .end local v2    # "l":I
    :cond_2
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v2    # "l":I
    goto :goto_2

    .line 408
    :cond_3
    return-void
.end method

.method public final invert()V
    .locals 0

    .prologue
    .line 416
    invoke-virtual {p0, p0}, Ljavax/vecmath/GMatrix;->invertGeneral(Ljavax/vecmath/GMatrix;)V

    .line 417
    return-void
.end method

.method public final invert(Ljavax/vecmath/GMatrix;)V
    .locals 0
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 426
    invoke-virtual {p0, p1}, Ljavax/vecmath/GMatrix;->invertGeneral(Ljavax/vecmath/GMatrix;)V

    .line 427
    return-void
.end method

.method final invertGeneral(Ljavax/vecmath/GMatrix;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1703
    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v8, p1, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int v5, v7, v8

    .line 1704
    .local v5, "size":I
    new-array v6, v5, [D

    .line 1705
    .local v6, "temp":[D
    new-array v3, v5, [D

    .line 1706
    .local v3, "result":[D
    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    new-array v4, v7, [I

    .line 1707
    .local v4, "row_perm":[I
    const/4 v7, 0x1

    new-array v0, v7, [I

    .line 1712
    .local v0, "even_row_exchange":[I
    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v8, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v7, v8, :cond_0

    .line 1714
    new-instance v7, Ljavax/vecmath/MismatchedSizeException;

    const-string v8, "GMatrix22"

    .line 1715
    invoke-static {v8}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1719
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v7, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v7, :cond_2

    .line 1720
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v7, :cond_1

    .line 1721
    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v7, v1

    add-int/2addr v7, v2

    iget-object v8, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v8, v8, v1

    aget-wide v8, v8, v2

    aput-wide v8, v6, v7

    .line 1720
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1719
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1726
    .end local v2    # "j":I
    :cond_2
    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-static {v7, v6, v4, v0}, Ljavax/vecmath/GMatrix;->luDecomposition(I[D[I[I)Z

    move-result v7

    if-nez v7, :cond_3

    .line 1728
    new-instance v7, Ljavax/vecmath/SingularMatrixException;

    const-string v8, "GMatrix21"

    .line 1729
    invoke-static {v8}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljavax/vecmath/SingularMatrixException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1733
    :cond_3
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v5, :cond_4

    .line 1734
    const-wide/16 v8, 0x0

    aput-wide v8, v3, v1

    .line 1733
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1736
    :cond_4
    const/4 v1, 0x0

    :goto_3
    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v7, :cond_5

    .line 1737
    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v7, v1

    add-int/2addr v7, v1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    aput-wide v8, v3, v7

    .line 1736
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1739
    :cond_5
    iget v7, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-static {v7, v6, v4, v3}, Ljavax/vecmath/GMatrix;->luBacksubstitution(I[D[I[D)V

    .line 1741
    const/4 v1, 0x0

    :goto_4
    iget v7, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v7, :cond_7

    .line 1742
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_5
    iget v7, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v7, :cond_6

    .line 1743
    iget-object v7, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v7, v7, v1

    iget v8, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v8, v1

    add-int/2addr v8, v2

    aget-wide v8, v3, v8

    aput-wide v8, v7, v2

    .line 1742
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1741
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1746
    .end local v2    # "j":I
    :cond_7
    return-void
.end method

.method public final mul(Ljavax/vecmath/GMatrix;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 142
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v4, v5, :cond_1

    .line 143
    :cond_0
    new-instance v4, Ljavax/vecmath/MismatchedSizeException;

    const-string v5, "GMatrix0"

    .line 144
    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 146
    :cond_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    .line 148
    .local v3, "tmp":[[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_4

    .line 149
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_3

    .line 150
    aget-object v4, v3, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 151
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_2

    .line 152
    aget-object v4, v3, v0

    aget-wide v6, v4, v1

    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v8, v5, v2

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v10, v5, v1

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 149
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 148
    .end local v2    # "k":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 157
    .end local v1    # "j":I
    :cond_4
    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 158
    return-void
.end method

.method public final mul(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 170
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v4, v5, :cond_1

    .line 171
    :cond_0
    new-instance v4, Ljavax/vecmath/MismatchedSizeException;

    const-string v5, "GMatrix1"

    .line 172
    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 174
    :cond_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    .line 176
    .local v3, "tmp":[[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_4

    .line 177
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_3

    .line 178
    aget-object v4, v3, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 179
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_2

    .line 180
    aget-object v4, v3, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v8, v5, v2

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v10, v5, v1

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 179
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 177
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 176
    .end local v2    # "k":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    .end local v1    # "j":I
    :cond_4
    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 186
    return-void
.end method

.method public final mul(Ljavax/vecmath/GVector;Ljavax/vecmath/GVector;)V
    .locals 8
    .param p1, "v1"    # Ljavax/vecmath/GVector;
    .param p2, "v2"    # Ljavax/vecmath/GVector;

    .prologue
    .line 200
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-virtual {p1}, Ljavax/vecmath/GVector;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 201
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix2"

    .line 202
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 204
    :cond_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 205
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix3"

    .line 206
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 208
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Ljavax/vecmath/GVector;->getSize()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 209
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    invoke-virtual {p2}, Ljavax/vecmath/GVector;->getSize()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 210
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GVector;->values:[D

    aget-wide v4, v3, v0

    iget-object v3, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v6, v3, v1

    mul-double/2addr v4, v6

    aput-wide v4, v2, v1

    .line 209
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 208
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final mulTransposeBoth(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1188
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v4, v5, :cond_1

    .line 1189
    :cond_0
    new-instance v4, Ljavax/vecmath/MismatchedSizeException;

    const-string v5, "GMatrix14"

    .line 1190
    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1192
    :cond_1
    if-eq p1, p0, :cond_2

    if-ne p2, p0, :cond_7

    .line 1193
    :cond_2
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    .line 1194
    .local v3, "tmp":[[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_5

    .line 1195
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_4

    .line 1196
    aget-object v4, v3, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1197
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v4, :cond_3

    .line 1198
    aget-object v4, v3, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v8, v5, v0

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aget-wide v10, v5, v2

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1197
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1195
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1194
    .end local v2    # "k":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1202
    .end local v1    # "j":I
    :cond_5
    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 1213
    .end local v3    # "tmp":[[D
    :cond_6
    return-void

    .line 1204
    .end local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_6

    .line 1205
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_4
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_9

    .line 1206
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1207
    const/4 v2, 0x0

    .restart local v2    # "k":I
    :goto_5
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v4, :cond_8

    .line 1208
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v8, v5, v0

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aget-wide v10, v5, v2

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1207
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1205
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1204
    .end local v2    # "k":I
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public final mulTransposeLeft(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1264
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v4, v5, :cond_1

    .line 1265
    :cond_0
    new-instance v4, Ljavax/vecmath/MismatchedSizeException;

    const-string v5, "GMatrix16"

    .line 1266
    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1268
    :cond_1
    if-eq p1, p0, :cond_2

    if-ne p2, p0, :cond_7

    .line 1269
    :cond_2
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    .line 1270
    .local v3, "tmp":[[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_5

    .line 1271
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_4

    .line 1272
    aget-object v4, v3, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1273
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v4, :cond_3

    .line 1274
    aget-object v4, v3, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v8, v5, v0

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v10, v5, v1

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1273
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1271
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1270
    .end local v2    # "k":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1278
    .end local v1    # "j":I
    :cond_5
    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 1289
    .end local v3    # "tmp":[[D
    :cond_6
    return-void

    .line 1280
    .end local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_6

    .line 1281
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_4
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_9

    .line 1282
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1283
    const/4 v2, 0x0

    .restart local v2    # "k":I
    :goto_5
    iget v4, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v4, :cond_8

    .line 1284
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v8, v5, v0

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v2

    aget-wide v10, v5, v1

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1283
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1281
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1280
    .end local v2    # "k":I
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public final mulTransposeRight(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 12
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1225
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v5, p2, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v4, v5, :cond_0

    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v4, v5, :cond_1

    .line 1226
    :cond_0
    new-instance v4, Ljavax/vecmath/MismatchedSizeException;

    const-string v5, "GMatrix15"

    .line 1227
    invoke-static {v5}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1229
    :cond_1
    if-eq p1, p0, :cond_2

    if-ne p2, p0, :cond_7

    .line 1230
    :cond_2
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v4, v5}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [[D

    .line 1231
    .local v3, "tmp":[[D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_5

    .line 1232
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_4

    .line 1233
    aget-object v4, v3, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1234
    const/4 v2, 0x0

    .local v2, "k":I
    :goto_2
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_3

    .line 1235
    aget-object v4, v3, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v8, v5, v2

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aget-wide v10, v5, v2

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1234
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1232
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1231
    .end local v2    # "k":I
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1239
    .end local v1    # "j":I
    :cond_5
    iput-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 1251
    .end local v3    # "tmp":[[D
    :cond_6
    return-void

    .line 1241
    .end local v0    # "i":I
    :cond_7
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_3
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v4, :cond_6

    .line 1242
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_4
    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v4, :cond_9

    .line 1243
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    const-wide/16 v6, 0x0

    aput-wide v6, v4, v1

    .line 1244
    const/4 v2, 0x0

    .restart local v2    # "k":I
    :goto_5
    iget v4, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_8

    .line 1245
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v6, v4, v1

    iget-object v5, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v8, v5, v2

    iget-object v5, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aget-wide v10, v5, v2

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    aput-wide v6, v4, v1

    .line 1244
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1242
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1241
    .end local v2    # "k":I
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method public final negate()V
    .locals 6

    .prologue
    .line 323
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_1

    .line 324
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_0

    .line 325
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    neg-double v4, v4

    aput-wide v4, v2, v1

    .line 324
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public final negate(Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 338
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_1

    .line 339
    :cond_0
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix13"

    .line 340
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 342
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 343
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 344
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    neg-double v4, v4

    aput-wide v4, v2, v1

    .line 343
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 342
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 347
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final set(Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 677
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v2, v3, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v3, :cond_1

    .line 678
    :cond_0
    iget v2, p1, Ljavax/vecmath/GMatrix;->nRow:I

    iput v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 679
    iget v2, p1, Ljavax/vecmath/GMatrix;->nCol:I

    iput v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 680
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 683
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 684
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 685
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    aput-wide v4, v2, v1

    .line 684
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 683
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 689
    .end local v1    # "j":I
    :cond_3
    iget v0, p1, Ljavax/vecmath/GMatrix;->nRow:I

    :goto_2
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_5

    .line 690
    iget v1, p1, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v1    # "j":I
    :goto_3
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_4

    .line 691
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 690
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 689
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 694
    .end local v1    # "j":I
    :cond_5
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3d;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3d;

    .prologue
    const/4 v4, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 565
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v2, v4, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_1

    .line 566
    :cond_0
    filled-new-array {v4, v4}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 567
    iput v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 568
    iput v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 571
    :cond_1
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m00:D

    aput-wide v4, v2, v6

    .line 572
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m01:D

    aput-wide v4, v2, v7

    .line 573
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m02:D

    aput-wide v4, v2, v8

    .line 575
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m10:D

    aput-wide v4, v2, v6

    .line 576
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m11:D

    aput-wide v4, v2, v7

    .line 577
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m12:D

    aput-wide v4, v2, v8

    .line 579
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m20:D

    aput-wide v4, v2, v6

    .line 580
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m21:D

    aput-wide v4, v2, v7

    .line 581
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix3d;->m22:D

    aput-wide v4, v2, v8

    .line 583
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 584
    const/4 v1, 0x3

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 585
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 584
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 583
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 589
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix3f;)V
    .locals 9
    .param p1, "m1"    # Ljavax/vecmath/Matrix3f;

    .prologue
    const/4 v3, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 534
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-lt v2, v3, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v2, v3, :cond_1

    .line 535
    :cond_0
    iput v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 536
    iput v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 537
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v2, v3}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 540
    :cond_1
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m00:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 541
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m01:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 542
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m02:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 544
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m10:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 545
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m11:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 546
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m12:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 548
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m20:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 549
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m21:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 550
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix3f;->m22:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 552
    const/4 v0, 0x3

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 553
    const/4 v1, 0x3

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 554
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 553
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 552
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 557
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4d;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix4d;

    .prologue
    const/4 v4, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 636
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v2, v4, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_1

    .line 637
    :cond_0
    filled-new-array {v4, v4}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 638
    iput v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 639
    iput v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 642
    :cond_1
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m00:D

    aput-wide v4, v2, v6

    .line 643
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m01:D

    aput-wide v4, v2, v7

    .line 644
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m02:D

    aput-wide v4, v2, v8

    .line 645
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m03:D

    aput-wide v4, v2, v9

    .line 647
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m10:D

    aput-wide v4, v2, v6

    .line 648
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m11:D

    aput-wide v4, v2, v7

    .line 649
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m12:D

    aput-wide v4, v2, v8

    .line 650
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m13:D

    aput-wide v4, v2, v9

    .line 652
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m20:D

    aput-wide v4, v2, v6

    .line 653
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m21:D

    aput-wide v4, v2, v7

    .line 654
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m22:D

    aput-wide v4, v2, v8

    .line 655
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m23:D

    aput-wide v4, v2, v9

    .line 657
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m30:D

    aput-wide v4, v2, v6

    .line 658
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m31:D

    aput-wide v4, v2, v7

    .line 659
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m32:D

    aput-wide v4, v2, v8

    .line 660
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget-wide v4, p1, Ljavax/vecmath/Matrix4d;->m33:D

    aput-wide v4, v2, v9

    .line 662
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 663
    const/4 v1, 0x4

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 664
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 663
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 662
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 667
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final set(Ljavax/vecmath/Matrix4f;)V
    .locals 10
    .param p1, "m1"    # Ljavax/vecmath/Matrix4f;

    .prologue
    const/4 v4, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 597
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-lt v2, v4, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v4, :cond_1

    .line 598
    :cond_0
    filled-new-array {v4, v4}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[D

    iput-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 599
    iput v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 600
    iput v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 603
    :cond_1
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m00:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 604
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m01:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 605
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m02:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 606
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v6

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m03:F

    float-to-double v4, v3

    aput-wide v4, v2, v9

    .line 608
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m10:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 609
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m11:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 610
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m12:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 611
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v7

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m13:F

    float-to-double v4, v3

    aput-wide v4, v2, v9

    .line 613
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m20:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 614
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m21:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 615
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m22:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 616
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v8

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m23:F

    float-to-double v4, v3

    aput-wide v4, v2, v9

    .line 618
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m30:F

    float-to-double v4, v3

    aput-wide v4, v2, v6

    .line 619
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m31:F

    float-to-double v4, v3

    aput-wide v4, v2, v7

    .line 620
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m32:F

    float-to-double v4, v3

    aput-wide v4, v2, v8

    .line 621
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v9

    iget v3, p1, Ljavax/vecmath/Matrix4f;->m33:F

    float-to-double v4, v3

    aput-wide v4, v2, v9

    .line 623
    const/4 v0, 0x4

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 624
    const/4 v1, 0x4

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 625
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 624
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 623
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 628
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final set([D)V
    .locals 6
    .param p1, "matrix"    # [D

    .prologue
    .line 519
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_1

    .line 520
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_0

    .line 521
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v3, v0

    add-int/2addr v3, v1

    aget-wide v4, p1, v3

    aput-wide v4, v2, v1

    .line 520
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 519
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 524
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public final setColumn(ILjavax/vecmath/GVector;)V
    .locals 4
    .param p1, "col"    # I
    .param p2, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 1172
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v1, :cond_0

    .line 1173
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    iget-object v2, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, p1

    .line 1172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1176
    :cond_0
    return-void
.end method

.method public final setColumn(I[D)V
    .locals 4
    .param p1, "col"    # I
    .param p2, "array"    # [D

    .prologue
    .line 1158
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v1, :cond_0

    .line 1159
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, v0

    aget-wide v2, p2, v0

    aput-wide v2, v1, p1

    .line 1158
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1161
    :cond_0
    return-void
.end method

.method public final setElement(IID)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "value"    # D

    .prologue
    .line 734
    iget-object v0, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v0, v0, p1

    aput-wide p3, v0, p2

    .line 735
    return-void
.end method

.method public final setIdentity()V
    .locals 6

    .prologue
    .line 355
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v3, :cond_1

    .line 356
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v3, :cond_0

    .line 357
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v1

    .line 356
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 355
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 362
    .end local v1    # "j":I
    :cond_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v3, v4, :cond_2

    .line 363
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 367
    .local v2, "l":I
    :goto_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_3

    .line 368
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    aput-wide v4, v3, v0

    .line 367
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 365
    .end local v2    # "l":I
    :cond_2
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v2    # "l":I
    goto :goto_2

    .line 370
    :cond_3
    return-void
.end method

.method public final setRow(ILjavax/vecmath/GVector;)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "vector"    # Ljavax/vecmath/GVector;

    .prologue
    .line 1144
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_0

    .line 1145
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, p1

    iget-object v2, p2, Ljavax/vecmath/GVector;->values:[D

    aget-wide v2, v2, v0

    aput-wide v2, v1, v0

    .line 1144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1147
    :cond_0
    return-void
.end method

.method public final setRow(I[D)V
    .locals 4
    .param p1, "row"    # I
    .param p2, "array"    # [D

    .prologue
    .line 1130
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v0, v1, :cond_0

    .line 1131
    iget-object v1, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v1, v1, p1

    aget-wide v2, p2, v0

    aput-wide v2, v1, v0

    .line 1130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1133
    :cond_0
    return-void
.end method

.method public final setScale(D)V
    .locals 7
    .param p1, "scale"    # D

    .prologue
    .line 1678
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v3, v4, :cond_0

    .line 1679
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 1683
    .local v2, "l":I
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v3, :cond_2

    .line 1684
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v3, :cond_1

    .line 1685
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v3, v1

    .line 1684
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1681
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "l":I
    :cond_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v2    # "l":I
    goto :goto_0

    .line 1683
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1689
    .end local v1    # "j":I
    :cond_2
    const/4 v0, 0x0

    :goto_3
    if-ge v0, v2, :cond_3

    .line 1690
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aput-wide p1, v3, v0

    .line 1689
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1692
    :cond_3
    return-void
.end method

.method public final setSize(II)V
    .locals 8
    .param p1, "nRow"    # I
    .param p2, "nCol"    # I

    .prologue
    .line 482
    filled-new-array {p1, p2}, [I

    move-result-object v5

    sget-object v6, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[D

    .line 485
    .local v4, "tmp":[[D
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v5, p1, :cond_0

    .line 486
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 490
    .local v3, "maxRow":I
    :goto_0
    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v5, p2, :cond_1

    .line 491
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 495
    .local v2, "maxCol":I
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    if-ge v0, v3, :cond_3

    .line 496
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_3
    if-ge v1, v2, :cond_2

    .line 497
    aget-object v5, v4, v0

    iget-object v6, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v0

    aget-wide v6, v6, v1

    aput-wide v6, v5, v1

    .line 496
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 488
    .end local v0    # "i":I
    .end local v1    # "j":I
    .end local v2    # "maxCol":I
    .end local v3    # "maxRow":I
    :cond_0
    move v3, p1

    .restart local v3    # "maxRow":I
    goto :goto_0

    .line 493
    :cond_1
    move v2, p2

    .restart local v2    # "maxCol":I
    goto :goto_1

    .line 495
    .restart local v0    # "i":I
    .restart local v1    # "j":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 501
    .end local v1    # "j":I
    :cond_3
    iput p1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 502
    iput p2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 504
    iput-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 505
    return-void
.end method

.method public final setZero()V
    .locals 6

    .prologue
    .line 378
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_1

    .line 379
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_0

    .line 380
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    const-wide/16 v4, 0x0

    aput-wide v4, v2, v1

    .line 379
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 378
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 383
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public final sub(Ljavax/vecmath/GMatrix;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 274
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_0

    .line 275
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix9"

    .line 276
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 278
    :cond_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_1

    .line 279
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix28"

    .line 280
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 282
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_3

    .line 283
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 284
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v6, v3, v1

    sub-double/2addr v4, v6

    aput-wide v4, v2, v1

    .line 283
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 282
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 287
    .end local v1    # "j":I
    :cond_3
    return-void
.end method

.method public final sub(Ljavax/vecmath/GMatrix;Ljavax/vecmath/GMatrix;)V
    .locals 8
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;
    .param p2, "m2"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 298
    iget v2, p2, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_0

    .line 299
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix10"

    .line 300
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 302
    :cond_0
    iget v2, p2, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_1

    .line 303
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix11"

    .line 304
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 306
    :cond_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-ne v2, v3, :cond_2

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v2, v3, :cond_3

    .line 307
    :cond_2
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix12"

    .line 308
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 310
    :cond_3
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_5

    .line 311
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_4

    .line 312
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v4, v3, v1

    iget-object v3, p2, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v0

    aget-wide v6, v3, v1

    sub-double/2addr v4, v6

    aput-wide v4, v2, v1

    .line 311
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 310
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 315
    .end local v1    # "j":I
    :cond_5
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1352
    new-instance v0, Ljava/lang/StringBuffer;

    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v4, p0, Ljavax/vecmath/GMatrix;->nCol:I

    mul-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x8

    invoke-direct {v0, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    .line 1356
    .local v0, "buffer":Ljava/lang/StringBuffer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v1, v3, :cond_1

    .line 1357
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    iget v3, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v2, v3, :cond_0

    .line 1358
    iget-object v3, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v1

    aget-wide v4, v3, v2

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1357
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1360
    :cond_0
    const-string v3, "\n"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1356
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1363
    .end local v2    # "j":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public final trace()D
    .locals 6

    .prologue
    .line 1508
    iget v4, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v4, v5, :cond_0

    .line 1509
    iget v1, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 1513
    .local v1, "l":I
    :goto_0
    const-wide/16 v2, 0x0

    .line 1514
    .local v2, "t":D
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_1

    .line 1515
    iget-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v4, v4, v0

    aget-wide v4, v4, v0

    add-double/2addr v2, v4

    .line 1514
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1511
    .end local v0    # "i":I
    .end local v1    # "l":I
    .end local v2    # "t":D
    :cond_0
    iget v1, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .restart local v1    # "l":I
    goto :goto_0

    .line 1517
    .restart local v0    # "i":I
    .restart local v2    # "t":D
    :cond_1
    return-wide v2
.end method

.method public final transpose()V
    .locals 8

    .prologue
    .line 1299
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-eq v5, v6, :cond_3

    .line 1301
    iget v0, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 1302
    .local v0, "i":I
    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iput v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    .line 1303
    iput v0, p0, Ljavax/vecmath/GMatrix;->nCol:I

    .line 1304
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v6, p0, Ljavax/vecmath/GMatrix;->nCol:I

    filled-new-array {v5, v6}, [I

    move-result-object v5

    sget-object v6, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v6, v5}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[D

    .line 1305
    .local v4, "tmp":[[D
    const/4 v0, 0x0

    :goto_0
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v5, :cond_1

    .line 1306
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v5, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v5, :cond_0

    .line 1307
    aget-object v5, v4, v0

    iget-object v6, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v1

    aget-wide v6, v6, v0

    aput-wide v6, v5, v1

    .line 1306
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1305
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1310
    .end local v1    # "j":I
    :cond_1
    iput-object v4, p0, Ljavax/vecmath/GMatrix;->values:[[D

    .line 1321
    .end local v4    # "tmp":[[D
    :cond_2
    return-void

    .line 1313
    .end local v0    # "i":I
    :cond_3
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_2
    iget v5, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v5, :cond_2

    .line 1314
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_3
    if-ge v1, v0, :cond_4

    .line 1315
    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    aget-wide v2, v5, v1

    .line 1316
    .local v2, "swap":D
    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v0

    iget-object v6, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v6, v6, v1

    aget-wide v6, v6, v0

    aput-wide v6, v5, v1

    .line 1317
    iget-object v5, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v5, v5, v1

    aput-wide v2, v5, v0

    .line 1314
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1313
    .end local v2    # "swap":D
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final transpose(Ljavax/vecmath/GMatrix;)V
    .locals 6
    .param p1, "m1"    # Ljavax/vecmath/GMatrix;

    .prologue
    .line 1331
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nCol:I

    if-ne v2, v3, :cond_0

    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    iget v3, p1, Ljavax/vecmath/GMatrix;->nRow:I

    if-eq v2, v3, :cond_1

    .line 1332
    :cond_0
    new-instance v2, Ljavax/vecmath/MismatchedSizeException;

    const-string v3, "GMatrix17"

    .line 1333
    invoke-static {v3}, Ljavax/vecmath/VecMathI18N;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljavax/vecmath/MismatchedSizeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1335
    :cond_1
    if-eq p1, p0, :cond_3

    .line 1336
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Ljavax/vecmath/GMatrix;->nRow:I

    if-ge v0, v2, :cond_4

    .line 1337
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v2, p0, Ljavax/vecmath/GMatrix;->nCol:I

    if-ge v1, v2, :cond_2

    .line 1338
    iget-object v2, p0, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v2, v2, v0

    iget-object v3, p1, Ljavax/vecmath/GMatrix;->values:[[D

    aget-object v3, v3, v1

    aget-wide v4, v3, v0

    aput-wide v4, v2, v1

    .line 1337
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1336
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1342
    .end local v0    # "i":I
    .end local v1    # "j":I
    :cond_3
    invoke-virtual {p0}, Ljavax/vecmath/GMatrix;->transpose()V

    .line 1344
    :cond_4
    return-void
.end method
