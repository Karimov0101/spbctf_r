.class final Lio/realm/RealmCache;
.super Ljava/lang/Object;
.source "RealmCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RealmCache$RealmCacheType;,
        Lio/realm/RealmCache$RefAndCount;,
        Lio/realm/RealmCache$Callback0;,
        Lio/realm/RealmCache$Callback;
    }
.end annotation


# static fields
.field private static final DIFFERENT_KEY_MESSAGE:Ljava/lang/String; = "Wrong key used to decrypt Realm."

.field private static final MAX_ENTRIES_IN_TYPED_COLUMN_INDICES_ARRAY:I = 0x4

.field private static final WRONG_REALM_CLASS_MESSAGE:Ljava/lang/String; = "The type of Realm class must be Realm or DynamicRealm."

.field private static cachesMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lio/realm/RealmCache;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final configuration:Lio/realm/RealmConfiguration;

.field private final refAndCountMap:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Lio/realm/RealmCache$RealmCacheType;",
            "Lio/realm/RealmCache$RefAndCount;",
            ">;"
        }
    .end annotation
.end field

.field private final typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Lio/realm/RealmConfiguration;)V
    .locals 7
    .param p1, "config"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v1, 0x4

    new-array v1, v1, [Lio/realm/internal/ColumnIndices;

    iput-object v1, p0, Lio/realm/RealmCache;->typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;

    .line 91
    iput-object p1, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    .line 92
    new-instance v1, Ljava/util/EnumMap;

    const-class v2, Lio/realm/RealmCache$RealmCacheType;

    invoke-direct {v1, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v1, p0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    .line 93
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v0, v2, v1

    .line 94
    .local v0, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v4, p0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    new-instance v5, Lio/realm/RealmCache$RefAndCount;

    const/4 v6, 0x0

    invoke-direct {v5, v6}, Lio/realm/RealmCache$RefAndCount;-><init>(Lio/realm/RealmCache$1;)V

    invoke-virtual {v4, v0, v5}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 96
    .end local v0    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_0
    return-void
.end method

.method private static copyAssetFileIfNeeded(Lio/realm/RealmConfiguration;)V
    .locals 11
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 346
    const/4 v3, 0x0

    .line 347
    .local v3, "exceptionWhenClose":Ljava/io/IOException;
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->hasAssetFile()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 348
    new-instance v7, Ljava/io/File;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getRealmDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getRealmFileName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 349
    .local v7, "realmFile":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 396
    .end local v7    # "realmFile":Ljava/io/File;
    :cond_0
    return-void

    .line 353
    .restart local v7    # "realmFile":Ljava/io/File;
    :cond_1
    const/4 v4, 0x0

    .line 354
    .local v4, "inputStream":Ljava/io/InputStream;
    const/4 v5, 0x0

    .line 356
    .local v5, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getAssetFile()Ljava/io/InputStream;

    move-result-object v4

    .line 357
    if-nez v4, :cond_4

    .line 358
    new-instance v8, Lio/realm/exceptions/RealmFileException;

    sget-object v9, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    const-string v10, "Invalid input stream to asset file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/String;)V

    throw v8
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 368
    :catch_0
    move-exception v2

    .line 369
    .local v2, "e":Ljava/io/IOException;
    :goto_0
    :try_start_1
    new-instance v8, Lio/realm/exceptions/RealmFileException;

    sget-object v9, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    const-string v10, "Could not resolve the path to the Realm asset file."

    invoke-direct {v8, v9, v10, v2}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 372
    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    :goto_1
    if-eqz v4, :cond_2

    .line 374
    :try_start_2
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4

    .line 379
    :cond_2
    :goto_2
    if-eqz v5, :cond_3

    .line 381
    :try_start_3
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 387
    :cond_3
    :goto_3
    throw v8

    .line 362
    :cond_4
    :try_start_4
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 363
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .local v6, "outputStream":Ljava/io/FileOutputStream;
    const/16 v8, 0x1000

    :try_start_5
    new-array v0, v8, [B

    .line 365
    .local v0, "buf":[B
    :goto_4
    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    .local v1, "bytesRead":I
    const/4 v8, -0x1

    if-le v1, v8, :cond_5

    .line 366
    const/4 v8, 0x0

    invoke-virtual {v6, v0, v8, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_4

    .line 368
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    :catch_1
    move-exception v2

    move-object v5, v6

    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_0

    .line 372
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v0    # "buf":[B
    .restart local v1    # "bytesRead":I
    .restart local v6    # "outputStream":Ljava/io/FileOutputStream;
    :cond_5
    if-eqz v4, :cond_6

    .line 374
    :try_start_6
    invoke-virtual {v4}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    .line 379
    :cond_6
    :goto_5
    if-eqz v6, :cond_7

    .line 381
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    .line 392
    :cond_7
    :goto_6
    if-eqz v3, :cond_0

    .line 393
    new-instance v8, Lio/realm/exceptions/RealmFileException;

    sget-object v9, Lio/realm/exceptions/RealmFileException$Kind;->ACCESS_ERROR:Lio/realm/exceptions/RealmFileException$Kind;

    invoke-direct {v8, v9, v3}, Lio/realm/exceptions/RealmFileException;-><init>(Lio/realm/exceptions/RealmFileException$Kind;Ljava/lang/Throwable;)V

    throw v8

    .line 375
    :catch_2
    move-exception v2

    .line 376
    .restart local v2    # "e":Ljava/io/IOException;
    move-object v3, v2

    goto :goto_5

    .line 382
    .end local v2    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v2

    .line 384
    .restart local v2    # "e":Ljava/io/IOException;
    if-nez v3, :cond_7

    .line 385
    move-object v3, v2

    goto :goto_6

    .line 375
    .end local v0    # "buf":[B
    .end local v1    # "bytesRead":I
    .end local v2    # "e":Ljava/io/IOException;
    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    :catch_4
    move-exception v2

    .line 376
    .restart local v2    # "e":Ljava/io/IOException;
    move-object v3, v2

    goto :goto_2

    .line 382
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 384
    .restart local v2    # "e":Ljava/io/IOException;
    if-nez v3, :cond_3

    .line 385
    move-object v3, v2

    goto :goto_3

    .line 372
    .end local v2    # "e":Ljava/io/IOException;
    .end local v5    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v6    # "outputStream":Ljava/io/FileOutputStream;
    :catchall_1
    move-exception v8

    move-object v5, v6

    .end local v6    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v5    # "outputStream":Ljava/io/FileOutputStream;
    goto :goto_1
.end method

.method static declared-synchronized createRealmOrGetFromCache(Lio/realm/RealmConfiguration;Ljava/lang/Class;)Lio/realm/BaseRealm;
    .locals 10
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lio/realm/BaseRealm;",
            ">(",
            "Lio/realm/RealmConfiguration;",
            "Ljava/lang/Class",
            "<TE;>;)TE;"
        }
    .end annotation

    .prologue
    .line 107
    .local p1, "realmClass":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const-class v8, Lio/realm/RealmCache;

    monitor-enter v8

    const/4 v1, 0x1

    .line 108
    .local v1, "isCacheInMap":Z
    :try_start_0
    sget-object v7, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 109
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_7

    .line 111
    new-instance v0, Lio/realm/RealmCache;

    .end local v0    # "cache":Lio/realm/RealmCache;
    invoke-direct {v0, p0}, Lio/realm/RealmCache;-><init>(Lio/realm/RealmConfiguration;)V

    .line 113
    .restart local v0    # "cache":Lio/realm/RealmCache;
    const/4 v1, 0x0

    .line 115
    invoke-static {p0}, Lio/realm/RealmCache;->copyAssetFileIfNeeded(Lio/realm/RealmConfiguration;)V

    .line 121
    :goto_0
    iget-object v7, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-static {p1}, Lio/realm/RealmCache$RealmCacheType;->valueOf(Ljava/lang/Class;)Lio/realm/RealmCache$RealmCacheType;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/RealmCache$RefAndCount;

    .line 123
    .local v4, "refAndCount":Lio/realm/RealmCache$RefAndCount;
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-nez v7, :cond_1

    .line 124
    invoke-static {p0}, Lio/realm/internal/SharedRealm;->getInstance(Lio/realm/RealmConfiguration;)Lio/realm/internal/SharedRealm;

    move-result-object v6

    .line 125
    .local v6, "sharedRealm":Lio/realm/internal/SharedRealm;
    invoke-static {v6}, Lio/realm/internal/Table;->primaryKeyTableNeedsMigration(Lio/realm/internal/SharedRealm;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 126
    invoke-virtual {v6}, Lio/realm/internal/SharedRealm;->beginTransaction()V

    .line 127
    invoke-static {v6}, Lio/realm/internal/Table;->migratePrimaryKeyTableIfNeeded(Lio/realm/internal/SharedRealm;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 128
    invoke-virtual {v6}, Lio/realm/internal/SharedRealm;->commitTransaction()V

    .line 133
    :cond_0
    :goto_1
    invoke-virtual {v6}, Lio/realm/internal/SharedRealm;->close()V

    .line 136
    .end local v6    # "sharedRealm":Lio/realm/internal/SharedRealm;
    :cond_1
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v7

    if-nez v7, :cond_3

    .line 141
    const-class v7, Lio/realm/Realm;

    if-ne p1, v7, :cond_9

    .line 143
    iget-object v7, v0, Lio/realm/RealmCache;->typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;

    invoke-static {p0, v7}, Lio/realm/Realm;->createInstance(Lio/realm/RealmConfiguration;[Lio/realm/internal/ColumnIndices;)Lio/realm/Realm;

    move-result-object v2

    .line 153
    .local v2, "realm":Lio/realm/BaseRealm;
    :goto_2
    if-nez v1, :cond_2

    .line 154
    sget-object v7, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v9, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    :cond_2
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 157
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 160
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_3
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    .line 161
    .local v5, "refCount":Ljava/lang/Integer;
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_5

    .line 162
    const-class v7, Lio/realm/Realm;

    if-ne p1, v7, :cond_4

    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-nez v7, :cond_4

    .line 163
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/BaseRealm;

    .line 165
    .restart local v2    # "realm":Lio/realm/BaseRealm;
    iget-object v7, v0, Lio/realm/RealmCache;->typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;

    iget-object v9, v2, Lio/realm/BaseRealm;->schema:Lio/realm/RealmSchema;

    iget-object v9, v9, Lio/realm/RealmSchema;->columnIndices:Lio/realm/internal/ColumnIndices;

    invoke-virtual {v9}, Lio/realm/internal/ColumnIndices;->clone()Lio/realm/internal/ColumnIndices;

    move-result-object v9

    invoke-static {v7, v9}, Lio/realm/RealmCache;->storeColumnIndices([Lio/realm/internal/ColumnIndices;Lio/realm/internal/ColumnIndices;)I

    .line 168
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_4
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$108(Lio/realm/RealmCache$RefAndCount;)I

    .line 170
    :cond_5
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 173
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/BaseRealm;

    .line 176
    .local v3, "realm":Lio/realm/BaseRealm;, "TE;"
    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    const/4 v9, 0x1

    if-ne v7, v9, :cond_6

    .line 177
    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->isSyncConfiguration()Z

    move-result v7

    invoke-static {v7}, Lio/realm/internal/ObjectServerFacade;->getFacade(Z)Lio/realm/internal/ObjectServerFacade;

    move-result-object v7

    invoke-virtual {v7, p0}, Lio/realm/internal/ObjectServerFacade;->realmOpened(Lio/realm/RealmConfiguration;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    :cond_6
    monitor-exit v8

    return-object v3

    .line 118
    .end local v3    # "realm":Lio/realm/BaseRealm;, "TE;"
    .end local v4    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .end local v5    # "refCount":Ljava/lang/Integer;
    :cond_7
    :try_start_1
    invoke-direct {v0, p0}, Lio/realm/RealmCache;->validateConfiguration(Lio/realm/RealmConfiguration;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 107
    .end local v0    # "cache":Lio/realm/RealmCache;
    :catchall_0
    move-exception v7

    monitor-exit v8

    throw v7

    .line 130
    .restart local v0    # "cache":Lio/realm/RealmCache;
    .restart local v4    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .restart local v6    # "sharedRealm":Lio/realm/internal/SharedRealm;
    :cond_8
    :try_start_2
    invoke-virtual {v6}, Lio/realm/internal/SharedRealm;->cancelTransaction()V

    goto/16 :goto_1

    .line 144
    .end local v6    # "sharedRealm":Lio/realm/internal/SharedRealm;
    :cond_9
    const-class v7, Lio/realm/DynamicRealm;

    if-ne p1, v7, :cond_a

    .line 145
    invoke-static {p0}, Lio/realm/DynamicRealm;->createInstance(Lio/realm/RealmConfiguration;)Lio/realm/DynamicRealm;

    move-result-object v2

    .restart local v2    # "realm":Lio/realm/BaseRealm;
    goto/16 :goto_2

    .line 147
    .end local v2    # "realm":Lio/realm/BaseRealm;
    :cond_a
    new-instance v7, Ljava/lang/IllegalArgumentException;

    const-string v9, "The type of Realm class must be Realm or DynamicRealm."

    invoke-direct {v7, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method public static findColumnIndices([Lio/realm/internal/ColumnIndices;J)Lio/realm/internal/ColumnIndices;
    .locals 5
    .param p0, "array"    # [Lio/realm/internal/ColumnIndices;
    .param p1, "schemaVersion"    # J

    .prologue
    .line 420
    array-length v2, p0

    add-int/lit8 v1, v2, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 421
    aget-object v0, p0, v1

    .line 422
    .local v0, "candidate":Lio/realm/internal/ColumnIndices;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lio/realm/internal/ColumnIndices;->getSchemaVersion()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 426
    .end local v0    # "candidate":Lio/realm/internal/ColumnIndices;
    :goto_1
    return-object v0

    .line 420
    .restart local v0    # "candidate":Lio/realm/internal/ColumnIndices;
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 426
    .end local v0    # "candidate":Lio/realm/internal/ColumnIndices;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static getLocalThreadCount(Lio/realm/RealmConfiguration;)I
    .locals 9
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;

    .prologue
    const/4 v5, 0x0

    .line 399
    sget-object v4, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 400
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_0

    .line 408
    :goto_0
    return v5

    .line 403
    :cond_0
    const/4 v2, 0x0

    .line 404
    .local v2, "totalRefCount":I
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v7

    array-length v8, v7

    move v6, v5

    :goto_1
    if-ge v6, v8, :cond_2

    aget-object v3, v7, v6

    .line 405
    .local v3, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v4, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {v4, v3}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/RealmCache$RefAndCount;

    invoke-static {v4}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 406
    .local v1, "localCount":Ljava/lang/Integer;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    :goto_2
    add-int/2addr v2, v4

    .line 404
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_1

    :cond_1
    move v4, v5

    .line 406
    goto :goto_2

    .end local v1    # "localCount":Ljava/lang/Integer;
    .end local v3    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_2
    move v5, v2

    .line 408
    goto :goto_0
.end method

.method static declared-synchronized invokeWithGlobalRefCount(Lio/realm/RealmConfiguration;Lio/realm/RealmCache$Callback;)V
    .locals 8
    .param p0, "configuration"    # Lio/realm/RealmConfiguration;
    .param p1, "callback"    # Lio/realm/RealmCache$Callback;

    .prologue
    const/4 v3, 0x0

    .line 293
    const-class v5, Lio/realm/RealmCache;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/RealmConfiguration;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 294
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_0

    .line 295
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Lio/realm/RealmCache$Callback;->onResult(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    :goto_0
    monitor-exit v5

    return-void

    .line 298
    :cond_0
    const/4 v1, 0x0

    .line 299
    .local v1, "totalRefCount":I
    :try_start_1
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v6

    array-length v7, v6

    move v4, v3

    :goto_1
    if-ge v4, v7, :cond_1

    aget-object v2, v6, v4

    .line 300
    .local v2, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v3, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {v3, v2}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmCache$RefAndCount;

    invoke-static {v3}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v3

    add-int/2addr v1, v3

    .line 299
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 302
    .end local v2    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_1
    invoke-interface {p1, v1}, Lio/realm/RealmCache$Callback;->onResult(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 293
    .end local v0    # "cache":Lio/realm/RealmCache;
    .end local v1    # "totalRefCount":I
    :catchall_0
    move-exception v3

    monitor-exit v5

    throw v3
.end method

.method static declared-synchronized invokeWithLock(Lio/realm/RealmCache$Callback0;)V
    .locals 2
    .param p0, "callback"    # Lio/realm/RealmCache$Callback0;

    .prologue
    .line 335
    const-class v0, Lio/realm/RealmCache;

    monitor-enter v0

    :try_start_0
    invoke-interface {p0}, Lio/realm/RealmCache$Callback0;->onCall()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    monitor-exit v0

    return-void

    .line 335
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method static declared-synchronized release(Lio/realm/BaseRealm;)V
    .locals 11
    .param p0, "realm"    # Lio/realm/BaseRealm;

    .prologue
    const/4 v6, 0x0

    .line 189
    const-class v8, Lio/realm/RealmCache;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 190
    .local v1, "canonicalPath":Ljava/lang/String;
    sget-object v7, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-interface {v7, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;

    .line 191
    .local v0, "cache":Lio/realm/RealmCache;
    const/4 v3, 0x0

    .line 192
    .local v3, "refCount":Ljava/lang/Integer;
    const/4 v2, 0x0

    .line 194
    .local v2, "refAndCount":Lio/realm/RealmCache$RefAndCount;
    if-eqz v0, :cond_0

    .line 195
    iget-object v7, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9}, Lio/realm/RealmCache$RealmCacheType;->valueOf(Ljava/lang/Class;)Lio/realm/RealmCache$RealmCacheType;

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    check-cast v2, Lio/realm/RealmCache$RefAndCount;

    .line 196
    .restart local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "refCount":Ljava/lang/Integer;
    check-cast v3, Ljava/lang/Integer;

    .line 198
    .restart local v3    # "refCount":Ljava/lang/Integer;
    :cond_0
    if-nez v3, :cond_1

    .line 199
    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 202
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-gtz v7, :cond_3

    .line 203
    const-string v6, "%s has been closed already."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v1, v7, v9

    invoke-static {v6, v7}, Lio/realm/log/RealmLog;->warn(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    :cond_2
    :goto_0
    monitor-exit v8

    return-void

    .line 208
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 210
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_7

    .line 213
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 214
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v7

    const/4 v9, 0x0

    invoke-virtual {v7, v9}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 217
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$110(Lio/realm/RealmCache$RefAndCount;)I

    .line 218
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-gez v7, :cond_4

    .line 220
    new-instance v6, Ljava/lang/IllegalStateException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Global reference counter of Realm"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, " got corrupted."

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 189
    .end local v0    # "cache":Lio/realm/RealmCache;
    .end local v1    # "canonicalPath":Ljava/lang/String;
    .end local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .end local v3    # "refCount":Ljava/lang/Integer;
    :catchall_0
    move-exception v6

    monitor-exit v8

    throw v6

    .line 225
    .restart local v0    # "cache":Lio/realm/RealmCache;
    .restart local v1    # "canonicalPath":Ljava/lang/String;
    .restart local v2    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    .restart local v3    # "refCount":Ljava/lang/Integer;
    :cond_4
    :try_start_2
    instance-of v7, p0, Lio/realm/Realm;

    if-eqz v7, :cond_5

    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v7

    if-nez v7, :cond_5

    .line 227
    iget-object v7, v0, Lio/realm/RealmCache;->typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;

    const/4 v9, 0x0

    invoke-static {v7, v9}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 230
    :cond_5
    const/4 v4, 0x0

    .line 231
    .local v4, "totalRefCount":I
    invoke-static {}, Lio/realm/RealmCache$RealmCacheType;->values()[Lio/realm/RealmCache$RealmCacheType;

    move-result-object v9

    array-length v10, v9

    move v7, v6

    :goto_1
    if-ge v7, v10, :cond_6

    aget-object v5, v9, v7

    .line 232
    .local v5, "type":Lio/realm/RealmCache$RealmCacheType;
    iget-object v6, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    invoke-virtual {v6, v5}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/realm/RealmCache$RefAndCount;

    invoke-static {v6}, Lio/realm/RealmCache$RefAndCount;->access$100(Lio/realm/RealmCache$RefAndCount;)I

    move-result v6

    add-int/2addr v4, v6

    .line 231
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    goto :goto_1

    .line 236
    .end local v5    # "type":Lio/realm/RealmCache$RealmCacheType;
    :cond_6
    invoke-virtual {p0}, Lio/realm/BaseRealm;->doClose()V

    .line 239
    if-nez v4, :cond_2

    .line 240
    sget-object v6, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-interface {v6, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    invoke-virtual {p0}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/RealmConfiguration;->isSyncConfiguration()Z

    move-result v6

    invoke-static {v6}, Lio/realm/internal/ObjectServerFacade;->getFacade(Z)Lio/realm/internal/ObjectServerFacade;

    move-result-object v6

    .line 242
    invoke-virtual {p0}, Lio/realm/BaseRealm;->getConfiguration()Lio/realm/RealmConfiguration;

    move-result-object v7

    invoke-virtual {v6, v7}, Lio/realm/internal/ObjectServerFacade;->realmClosed(Lio/realm/RealmConfiguration;)V

    goto/16 :goto_0

    .line 246
    .end local v4    # "totalRefCount":I
    :cond_7
    invoke-static {v2}, Lio/realm/RealmCache$RefAndCount;->access$300(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method private static storeColumnIndices([Lio/realm/internal/ColumnIndices;Lio/realm/internal/ColumnIndices;)I
    .locals 8
    .param p0, "array"    # [Lio/realm/internal/ColumnIndices;
    .param p1, "columnIndices"    # Lio/realm/internal/ColumnIndices;

    .prologue
    .line 441
    const-wide v2, 0x7fffffffffffffffL

    .line 442
    .local v2, "oldestSchemaVersion":J
    const/4 v0, -0x1

    .line 443
    .local v0, "candidateIndex":I
    array-length v5, p0

    add-int/lit8 v1, v5, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_2

    .line 444
    aget-object v5, p0, v1

    if-nez v5, :cond_0

    .line 445
    aput-object p1, p0, v1

    .line 456
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 449
    .restart local v1    # "i":I
    :cond_0
    aget-object v4, p0, v1

    .line 450
    .local v4, "target":Lio/realm/internal/ColumnIndices;
    invoke-virtual {v4}, Lio/realm/internal/ColumnIndices;->getSchemaVersion()J

    move-result-wide v6

    cmp-long v5, v6, v2

    if-gtz v5, :cond_1

    .line 451
    invoke-virtual {v4}, Lio/realm/internal/ColumnIndices;->getSchemaVersion()J

    move-result-wide v2

    .line 452
    move v0, v1

    .line 443
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 455
    .end local v4    # "target":Lio/realm/internal/ColumnIndices;
    :cond_2
    aput-object p1, p0, v0

    move v1, v0

    .line 456
    goto :goto_1
.end method

.method static declared-synchronized updateSchemaCache(Lio/realm/Realm;)V
    .locals 7
    .param p0, "realm"    # Lio/realm/Realm;

    .prologue
    .line 311
    const-class v5, Lio/realm/RealmCache;

    monitor-enter v5

    :try_start_0
    sget-object v4, Lio/realm/RealmCache;->cachesMap:Ljava/util/Map;

    invoke-virtual {p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmCache;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 312
    .local v0, "cache":Lio/realm/RealmCache;
    if-nez v0, :cond_1

    .line 327
    :cond_0
    :goto_0
    monitor-exit v5

    return-void

    .line 316
    :cond_1
    :try_start_1
    iget-object v4, v0, Lio/realm/RealmCache;->refAndCountMap:Ljava/util/EnumMap;

    sget-object v6, Lio/realm/RealmCache$RealmCacheType;->TYPED_REALM:Lio/realm/RealmCache$RealmCacheType;

    invoke-virtual {v4, v6}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lio/realm/RealmCache$RefAndCount;

    .line 317
    .local v3, "refAndCount":Lio/realm/RealmCache$RefAndCount;
    invoke-static {v3}, Lio/realm/RealmCache$RefAndCount;->access$200(Lio/realm/RealmCache$RefAndCount;)Ljava/lang/ThreadLocal;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 322
    iget-object v2, v0, Lio/realm/RealmCache;->typedColumnIndicesArray:[Lio/realm/internal/ColumnIndices;

    .line 323
    .local v2, "globalCacheArray":[Lio/realm/internal/ColumnIndices;
    invoke-virtual {p0, v2}, Lio/realm/Realm;->updateSchemaCache([Lio/realm/internal/ColumnIndices;)Lio/realm/internal/ColumnIndices;

    move-result-object v1

    .line 324
    .local v1, "createdCacheEntry":Lio/realm/internal/ColumnIndices;
    if-eqz v1, :cond_0

    .line 325
    invoke-static {v2, v1}, Lio/realm/RealmCache;->storeColumnIndices([Lio/realm/internal/ColumnIndices;Lio/realm/internal/ColumnIndices;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    .end local v0    # "cache":Lio/realm/RealmCache;
    .end local v1    # "createdCacheEntry":Lio/realm/internal/ColumnIndices;
    .end local v2    # "globalCacheArray":[Lio/realm/internal/ColumnIndices;
    .end local v3    # "refAndCount":Lio/realm/RealmCache$RefAndCount;
    :catchall_0
    move-exception v4

    monitor-exit v5

    throw v4
.end method

.method private validateConfiguration(Lio/realm/RealmConfiguration;)V
    .locals 5
    .param p1, "newConfiguration"    # Lio/realm/RealmConfiguration;

    .prologue
    .line 257
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2, p1}, Lio/realm/RealmConfiguration;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 259
    return-void

    .line 263
    :cond_0
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v2

    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getEncryptionKey()[B

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_1

    .line 264
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Wrong key used to decrypt Realm."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 268
    :cond_1
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v0

    .line 269
    .local v0, "newMigration":Lio/realm/RealmMigration;
    iget-object v2, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v2}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v1

    .line 270
    .local v1, "oldMigration":Lio/realm/RealmMigration;
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    .line 272
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 273
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 274
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Configurations cannot be different if used to open the same file. The most likely cause is that equals() and hashCode() are not overridden in the migration class: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 276
    invoke-virtual {p1}, Lio/realm/RealmConfiguration;->getMigration()Lio/realm/RealmMigration;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 279
    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Configurations cannot be different if used to open the same file. \nCached configuration: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lio/realm/RealmCache;->configuration:Lio/realm/RealmConfiguration;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\n\nNew configuration: \n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method
