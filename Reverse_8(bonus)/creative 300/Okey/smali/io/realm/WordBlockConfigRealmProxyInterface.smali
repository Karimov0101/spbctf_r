.class public interface abstract Lio/realm/WordBlockConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "WordBlockConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$speed()I
.end method

.method public abstract realmGet$trainingDuration()J
.end method

.method public abstract realmGet$wordCount()I
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$speed(I)V
.end method

.method public abstract realmSet$trainingDuration(J)V
.end method

.method public abstract realmSet$wordCount(I)V
.end method
