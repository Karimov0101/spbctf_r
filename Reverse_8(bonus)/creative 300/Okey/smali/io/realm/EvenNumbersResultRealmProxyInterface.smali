.class public interface abstract Lio/realm/EvenNumbersResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "EvenNumbersResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
