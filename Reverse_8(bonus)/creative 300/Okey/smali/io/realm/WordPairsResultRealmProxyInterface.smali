.class public interface abstract Lio/realm/WordPairsResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "WordPairsResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
