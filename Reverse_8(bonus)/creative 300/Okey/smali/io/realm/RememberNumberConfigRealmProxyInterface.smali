.class public interface abstract Lio/realm/RememberNumberConfigRealmProxyInterface;
.super Ljava/lang/Object;
.source "RememberNumberConfigRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$answersToComplexityDown()I
.end method

.method public abstract realmGet$answersToComplexityUp()I
.end method

.method public abstract realmGet$complexity()I
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$maxComplexity()I
.end method

.method public abstract realmGet$minComplexity()I
.end method

.method public abstract realmGet$trainingShowCount()I
.end method

.method public abstract realmSet$answersToComplexityDown(I)V
.end method

.method public abstract realmSet$answersToComplexityUp(I)V
.end method

.method public abstract realmSet$complexity(I)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$maxComplexity(I)V
.end method

.method public abstract realmSet$minComplexity(I)V
.end method

.method public abstract realmSet$trainingShowCount(I)V
.end method
