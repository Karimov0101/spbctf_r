.class public interface abstract Lio/realm/GreenDotResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "GreenDotResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$unixTime()J
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$unixTime(J)V
.end method
