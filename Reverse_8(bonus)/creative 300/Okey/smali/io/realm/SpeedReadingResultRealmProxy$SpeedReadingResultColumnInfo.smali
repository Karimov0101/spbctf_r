.class final Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "SpeedReadingResultRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/SpeedReadingResultRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SpeedReadingResultColumnInfo"
.end annotation


# instance fields
.field public averageSpeedIndex:J

.field public configIndex:J

.field public idIndex:J

.field public maxSpeedIndex:J

.field public unixTimeIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 43
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 45
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "SpeedReadingResult"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->idIndex:J

    .line 46
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 47
    const-string v1, "SpeedReadingResult"

    const-string v2, "maxSpeed"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->maxSpeedIndex:J

    .line 48
    const-string v1, "maxSpeed"

    iget-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->maxSpeedIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "SpeedReadingResult"

    const-string v2, "averageSpeed"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->averageSpeedIndex:J

    .line 50
    const-string v1, "averageSpeed"

    iget-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->averageSpeedIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "SpeedReadingResult"

    const-string v2, "unixTime"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->unixTimeIndex:J

    .line 52
    const-string v1, "unixTime"

    iget-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->unixTimeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "SpeedReadingResult"

    const-string v2, "config"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->configIndex:J

    .line 54
    const-string v1, "config"

    iget-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->configIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    invoke-virtual {p0, v0}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;
    .locals 1

    .prologue
    .line 73
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->clone()Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->clone()Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 61
    move-object v0, p1

    check-cast v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;

    .line 62
    .local v0, "otherInfo":Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;
    iget-wide v2, v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->idIndex:J

    .line 63
    iget-wide v2, v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->maxSpeedIndex:J

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->maxSpeedIndex:J

    .line 64
    iget-wide v2, v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->averageSpeedIndex:J

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->averageSpeedIndex:J

    .line 65
    iget-wide v2, v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->unixTimeIndex:J

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->unixTimeIndex:J

    .line 66
    iget-wide v2, v0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->configIndex:J

    iput-wide v2, p0, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->configIndex:J

    .line 68
    invoke-virtual {v0}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 69
    return-void
.end method
