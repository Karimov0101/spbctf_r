.class Lio/realm/internal/PendingRow$1;
.super Ljava/lang/Object;
.source "PendingRow.java"

# interfaces
.implements Lio/realm/RealmChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/realm/internal/PendingRow;-><init>(Lio/realm/internal/SharedRealm;Lio/realm/internal/TableQuery;Lio/realm/internal/SortDescriptor;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lio/realm/RealmChangeListener",
        "<",
        "Lio/realm/internal/PendingRow;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lio/realm/internal/PendingRow;

.field final synthetic val$returnCheckedRow:Z


# direct methods
.method constructor <init>(Lio/realm/internal/PendingRow;Z)V
    .locals 0
    .param p1, "this$0"    # Lio/realm/internal/PendingRow;

    .prologue
    .line 40
    iput-object p1, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    iput-boolean p2, p0, Lio/realm/internal/PendingRow$1;->val$returnCheckedRow:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChange(Lio/realm/internal/PendingRow;)V
    .locals 5
    .param p1, "pendingRow"    # Lio/realm/internal/PendingRow;

    .prologue
    .line 43
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$000(Lio/realm/internal/PendingRow;)Ljava/lang/ref/WeakReference;

    move-result-object v3

    if-nez v3, :cond_0

    .line 44
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "The \'frontEnd\' has not been set."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 46
    :cond_0
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$000(Lio/realm/internal/PendingRow;)Ljava/lang/ref/WeakReference;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/PendingRow$FrontEnd;

    .line 47
    .local v0, "frontEnd":Lio/realm/internal/PendingRow$FrontEnd;
    if-nez v0, :cond_2

    .line 49
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$100(Lio/realm/internal/PendingRow;)V

    .line 67
    :cond_1
    :goto_0
    return-void

    .line 53
    :cond_2
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$200(Lio/realm/internal/PendingRow;)Lio/realm/internal/Collection;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/internal/Collection;->isValid()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 55
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$200(Lio/realm/internal/PendingRow;)Lio/realm/internal/Collection;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/internal/Collection;->firstUncheckedRow()Lio/realm/internal/UncheckedRow;

    move-result-object v2

    .line 57
    .local v2, "uncheckedRow":Lio/realm/internal/UncheckedRow;
    if-eqz v2, :cond_1

    .line 58
    iget-boolean v3, p0, Lio/realm/internal/PendingRow$1;->val$returnCheckedRow:Z

    if-eqz v3, :cond_3

    invoke-static {v2}, Lio/realm/internal/CheckedRow;->getFromRow(Lio/realm/internal/UncheckedRow;)Lio/realm/internal/CheckedRow;

    move-result-object v1

    .line 60
    .local v1, "row":Lio/realm/internal/Row;
    :goto_1
    invoke-interface {v0, v1}, Lio/realm/internal/PendingRow$FrontEnd;->onQueryFinished(Lio/realm/internal/Row;)V

    .line 61
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$100(Lio/realm/internal/PendingRow;)V

    goto :goto_0

    .end local v1    # "row":Lio/realm/internal/Row;
    :cond_3
    move-object v1, v2

    .line 58
    goto :goto_1

    .line 65
    .end local v2    # "uncheckedRow":Lio/realm/internal/UncheckedRow;
    :cond_4
    iget-object v3, p0, Lio/realm/internal/PendingRow$1;->this$0:Lio/realm/internal/PendingRow;

    invoke-static {v3}, Lio/realm/internal/PendingRow;->access$100(Lio/realm/internal/PendingRow;)V

    goto :goto_0
.end method

.method public bridge synthetic onChange(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 40
    check-cast p1, Lio/realm/internal/PendingRow;

    invoke-virtual {p0, p1}, Lio/realm/internal/PendingRow$1;->onChange(Lio/realm/internal/PendingRow;)V

    return-void
.end method
