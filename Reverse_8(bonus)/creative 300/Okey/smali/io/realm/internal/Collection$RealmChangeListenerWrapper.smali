.class Lio/realm/internal/Collection$RealmChangeListenerWrapper;
.super Ljava/lang/Object;
.source "Collection.java"

# interfaces
.implements Lio/realm/OrderedRealmCollectionChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/internal/Collection;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RealmChangeListenerWrapper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lio/realm/OrderedRealmCollectionChangeListener",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final listener:Lio/realm/RealmChangeListener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/RealmChangeListener",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lio/realm/RealmChangeListener;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/RealmChangeListener",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p0, "this":Lio/realm/internal/Collection$RealmChangeListenerWrapper;, "Lio/realm/internal/Collection$RealmChangeListenerWrapper<TT;>;"
    .local p1, "listener":Lio/realm/RealmChangeListener;, "Lio/realm/RealmChangeListener<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lio/realm/internal/Collection$RealmChangeListenerWrapper;->listener:Lio/realm/RealmChangeListener;

    .line 57
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 2
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 66
    .local p0, "this":Lio/realm/internal/Collection$RealmChangeListenerWrapper;, "Lio/realm/internal/Collection$RealmChangeListenerWrapper<TT;>;"
    instance-of v0, p1, Lio/realm/internal/Collection$RealmChangeListenerWrapper;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/Collection$RealmChangeListenerWrapper;->listener:Lio/realm/RealmChangeListener;

    check-cast p1, Lio/realm/internal/Collection$RealmChangeListenerWrapper;

    .end local p1    # "obj":Ljava/lang/Object;
    iget-object v1, p1, Lio/realm/internal/Collection$RealmChangeListenerWrapper;->listener:Lio/realm/RealmChangeListener;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 72
    .local p0, "this":Lio/realm/internal/Collection$RealmChangeListenerWrapper;, "Lio/realm/internal/Collection$RealmChangeListenerWrapper<TT;>;"
    iget-object v0, p0, Lio/realm/internal/Collection$RealmChangeListenerWrapper;->listener:Lio/realm/RealmChangeListener;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public onChange(Ljava/lang/Object;Lio/realm/OrderedCollectionChangeSet;)V
    .locals 1
    .param p2, "changes"    # Lio/realm/OrderedCollectionChangeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lio/realm/OrderedCollectionChangeSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    .local p0, "this":Lio/realm/internal/Collection$RealmChangeListenerWrapper;, "Lio/realm/internal/Collection$RealmChangeListenerWrapper<TT;>;"
    .local p1, "collection":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lio/realm/internal/Collection$RealmChangeListenerWrapper;->listener:Lio/realm/RealmChangeListener;

    invoke-interface {v0, p1}, Lio/realm/RealmChangeListener;->onChange(Ljava/lang/Object;)V

    .line 62
    return-void
.end method
