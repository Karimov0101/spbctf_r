.class public Lio/realm/internal/Table;
.super Ljava/lang/Object;
.source "Table.java"

# interfaces
.implements Lio/realm/internal/TableSchema;
.implements Lio/realm/internal/NativeObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/Table$PivotType;
    }
.end annotation


# static fields
.field public static final INFINITE:J = -0x1L

.field public static final NOT_NULLABLE:Z = false

.field public static final NO_MATCH:I = -0x1

.field private static final NO_PRIMARY_KEY:J = -0x2L

.field public static final NULLABLE:Z = true

.field private static final PRIMARY_KEY_CLASS_COLUMN_INDEX:J = 0x0L

.field private static final PRIMARY_KEY_CLASS_COLUMN_NAME:Ljava/lang/String; = "pk_table"

.field private static final PRIMARY_KEY_FIELD_COLUMN_INDEX:J = 0x1L

.field private static final PRIMARY_KEY_FIELD_COLUMN_NAME:Ljava/lang/String; = "pk_property"

.field private static final PRIMARY_KEY_TABLE_NAME:Ljava/lang/String; = "pk"

.field public static final TABLE_MAX_LENGTH:I = 0x38

.field public static final TABLE_PREFIX:Ljava/lang/String;

.field private static final nativeFinalizerPtr:J


# instance fields
.field private cachedPrimaryKeyColumnIndex:J

.field final context:Lio/realm/internal/Context;

.field private nativePtr:J

.field private final sharedRealm:Lio/realm/internal/SharedRealm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    invoke-static {}, Lio/realm/internal/Util;->getTablePrefix()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    .line 62
    invoke-static {}, Lio/realm/internal/Table;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/Table;->nativeFinalizerPtr:J

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 72
    new-instance v0, Lio/realm/internal/Context;

    invoke-direct {v0}, Lio/realm/internal/Context;-><init>()V

    iput-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    .line 76
    invoke-virtual {p0}, Lio/realm/internal/Table;->createNative()J

    move-result-wide v0

    iput-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 77
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 78
    new-instance v0, Ljava/lang/OutOfMemoryError;

    const-string v1, "Out of native memory."

    invoke-direct {v0, v1}, Ljava/lang/OutOfMemoryError;-><init>(Ljava/lang/String;)V

    throw v0

    .line 80
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    .line 81
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v0, p0}, Lio/realm/internal/Context;->addReference(Lio/realm/internal/NativeObject;)V

    .line 82
    return-void
.end method

.method constructor <init>(Lio/realm/internal/SharedRealm;J)V
    .locals 2
    .param p1, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p2, "nativePointer"    # J

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 89
    iget-object v0, p1, Lio/realm/internal/SharedRealm;->context:Lio/realm/internal/Context;

    iput-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    .line 90
    iput-object p1, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    .line 91
    iput-wide p2, p0, Lio/realm/internal/Table;->nativePtr:J

    .line 92
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-virtual {v0, p0}, Lio/realm/internal/Context;->addReference(Lio/realm/internal/NativeObject;)V

    .line 93
    return-void
.end method

.method constructor <init>(Lio/realm/internal/Table;J)V
    .locals 2
    .param p1, "parent"    # Lio/realm/internal/Table;
    .param p2, "nativePointer"    # J

    .prologue
    .line 85
    iget-object v0, p1, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    invoke-direct {p0, v0, p2, p3}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    .line 86
    return-void
.end method

.method private checkHasPrimaryKey()V
    .locals 3

    .prologue
    .line 931
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 932
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has no primary key defined"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 934
    :cond_0
    return-void
.end method

.method private getPrimaryKeyTable()Lio/realm/internal/Table;
    .locals 8

    .prologue
    .line 858
    iget-object v3, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    if-nez v3, :cond_1

    .line 859
    const/4 v2, 0x0

    .line 869
    :cond_0
    :goto_0
    return-object v2

    .line 861
    :cond_1
    iget-object v3, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    const-string v4, "pk"

    invoke-virtual {v3, v4}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v2

    .line 862
    .local v2, "pkTable":Lio/realm/internal/Table;
    invoke-virtual {v2}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 863
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 864
    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v4, "pk_table"

    invoke-virtual {v2, v3, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    move-result-wide v0

    .line 865
    .local v0, "columnIndex":J
    invoke-virtual {v2, v0, v1}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 866
    sget-object v3, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    const-string v4, "pk_property"

    invoke-virtual {v2, v3, v4}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J

    goto :goto_0
.end method

.method private invalidateCachedPrimaryKeyIndex()V
    .locals 2

    .prologue
    .line 876
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 877
    return-void
.end method

.method public static isModelTable(Ljava/lang/String;)Z
    .locals 1
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 1156
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private isPrimaryKey(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 613
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPrimaryKeyColumn(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 576
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static migratePrimaryKeyTableIfNeeded(Lio/realm/internal/SharedRealm;)Z
    .locals 6
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    .line 889
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->isInTransaction()Z

    move-result v1

    if-nez v1, :cond_1

    .line 890
    :cond_0
    invoke-static {}, Lio/realm/internal/Table;->throwImmutable()V

    .line 892
    :cond_1
    const-string v1, "pk"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 893
    const/4 v1, 0x0

    .line 896
    :goto_0
    return v1

    .line 895
    :cond_2
    const-string v1, "pk"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 896
    .local v0, "pkTable":Lio/realm/internal/Table;
    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getGroupNative()J

    move-result-wide v2

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v2, v3, v4, v5}, Lio/realm/internal/Table;->nativeMigratePrimaryKeyTableIfNeeded(JJ)Z

    move-result v1

    goto :goto_0
.end method

.method private native nativeAddColumn(JILjava/lang/String;Z)J
.end method

.method private native nativeAddColumnLink(JILjava/lang/String;J)J
.end method

.method public static native nativeAddEmptyRow(JJ)J
.end method

.method private native nativeAddSearchIndex(JJ)V
.end method

.method private native nativeAverageDouble(JJ)D
.end method

.method private native nativeAverageFloat(JJ)D
.end method

.method private native nativeAverageInt(JJ)D
.end method

.method private native nativeClear(J)V
.end method

.method private native nativeConvertColumnToNotNullable(JJ)V
.end method

.method private native nativeConvertColumnToNullable(JJ)V
.end method

.method private native nativeCountDouble(JJD)J
.end method

.method private native nativeCountFloat(JJF)J
.end method

.method private native nativeCountLong(JJJ)J
.end method

.method private native nativeCountString(JJLjava/lang/String;)J
.end method

.method private native nativeFindFirstBool(JJZ)J
.end method

.method private native nativeFindFirstDouble(JJD)J
.end method

.method private native nativeFindFirstFloat(JJF)J
.end method

.method public static native nativeFindFirstInt(JJJ)J
.end method

.method public static native nativeFindFirstNull(JJ)J
.end method

.method public static native nativeFindFirstString(JJLjava/lang/String;)J
.end method

.method private native nativeFindFirstTimestamp(JJJ)J
.end method

.method private native nativeGetBoolean(JJJ)Z
.end method

.method private native nativeGetByteArray(JJJ)[B
.end method

.method private native nativeGetColumnCount(J)J
.end method

.method private native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method private native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method private native nativeGetColumnType(JJ)I
.end method

.method private native nativeGetDouble(JJJ)D
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeGetFloat(JJJ)F
.end method

.method private native nativeGetLink(JJJ)J
.end method

.method private native nativeGetLinkTarget(JJ)J
.end method

.method public static native nativeGetLinkView(JJJ)J
.end method

.method private native nativeGetLong(JJJ)J
.end method

.method private native nativeGetName(J)Ljava/lang/String;
.end method

.method private native nativeGetSortedViewMulti(J[J[Z)J
.end method

.method private native nativeGetString(JJJ)Ljava/lang/String;
.end method

.method private native nativeGetTimestamp(JJJ)J
.end method

.method private native nativeHasSameSchema(JJ)Z
.end method

.method private native nativeHasSearchIndex(JJ)Z
.end method

.method private native nativeIsColumnNullable(JJ)Z
.end method

.method private native nativeIsNull(JJJ)Z
.end method

.method private native nativeIsNullLink(JJJ)Z
.end method

.method private native nativeIsValid(J)Z
.end method

.method private native nativeLowerBoundInt(JJJ)J
.end method

.method private native nativeMaximumDouble(JJ)D
.end method

.method private native nativeMaximumFloat(JJ)F
.end method

.method private native nativeMaximumInt(JJ)J
.end method

.method private native nativeMaximumTimestamp(JJ)J
.end method

.method private static native nativeMigratePrimaryKeyTableIfNeeded(JJ)Z
.end method

.method private native nativeMinimumDouble(JJ)D
.end method

.method private native nativeMinimumFloat(JJ)F
.end method

.method private native nativeMinimumInt(JJ)J
.end method

.method private native nativeMinimumTimestamp(JJ)J
.end method

.method private native nativeMoveLastOver(JJ)V
.end method

.method public static native nativeNullifyLink(JJJ)V
.end method

.method private native nativePivot(JJJIJ)V
.end method

.method private static native nativePrimaryKeyTableNeedsMigration(J)Z
.end method

.method private native nativeRemove(JJ)V
.end method

.method private native nativeRemoveColumn(JJ)V
.end method

.method private native nativeRemoveLast(J)V
.end method

.method private native nativeRemoveSearchIndex(JJ)V
.end method

.method private native nativeRenameColumn(JJLjava/lang/String;)V
.end method

.method public static native nativeSetBoolean(JJJZZ)V
.end method

.method public static native nativeSetByteArray(JJJ[BZ)V
.end method

.method public static native nativeSetDouble(JJJDZ)V
.end method

.method public static native nativeSetFloat(JJJFZ)V
.end method

.method public static native nativeSetLink(JJJJZ)V
.end method

.method public static native nativeSetLong(JJJJZ)V
.end method

.method public static native nativeSetLongUnique(JJJJ)V
.end method

.method public static native nativeSetNull(JJJZ)V
.end method

.method public static native nativeSetNullUnique(JJJ)V
.end method

.method private native nativeSetPrimaryKey(JJLjava/lang/String;)J
.end method

.method public static native nativeSetString(JJJLjava/lang/String;Z)V
.end method

.method public static native nativeSetStringUnique(JJJLjava/lang/String;)V
.end method

.method public static native nativeSetTimestamp(JJJJZ)V
.end method

.method private native nativeSize(J)J
.end method

.method private native nativeSumDouble(JJ)D
.end method

.method private native nativeSumFloat(JJ)D
.end method

.method private native nativeSumInt(JJ)J
.end method

.method private native nativeToJson(J)Ljava/lang/String;
.end method

.method private native nativeUpperBoundInt(JJJ)J
.end method

.method private native nativeVersion(J)J
.end method

.method private native nativeWhere(J)J
.end method

.method public static primaryKeyTableNeedsMigration(Lio/realm/internal/SharedRealm;)Z
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    .line 900
    const-string v1, "pk"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 901
    const/4 v1, 0x0

    .line 904
    :goto_0
    return v1

    .line 903
    :cond_0
    const-string v1, "pk"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 904
    .local v0, "pkTable":Lio/realm/internal/Table;
    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v2, v3}, Lio/realm/internal/Table;->nativePrimaryKeyTableNeedsMigration(J)Z

    move-result v1

    goto :goto_0
.end method

.method public static tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "tableName"    # Ljava/lang/String;

    .prologue
    .line 1171
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1174
    .end local p0    # "tableName":Ljava/lang/String;
    :goto_0
    return-object p0

    .restart local p0    # "tableName":Ljava/lang/String;
    :cond_0
    sget-object v0, Lio/realm/internal/Table;->TABLE_PREFIX:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V
    .locals 3
    .param p0, "value"    # Ljava/lang/Object;

    .prologue
    .line 669
    new-instance v0, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value already exists: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmPrimaryKeyConstraintException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static throwImmutable()V
    .locals 2

    .prologue
    .line 1136
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Changing Realm data can only be done from inside a transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private verifyColumnName(Ljava/lang/String;)V
    .locals 2
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 124
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x3f

    if-le v0, v1, :cond_0

    .line 125
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column names are currently limited to max 63 characters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs add([Ljava/lang/Object;)J
    .locals 32
    .param p1, "values"    # [Ljava/lang/Object;

    .prologue
    .line 492
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->addEmptyRow()J

    move-result-wide v6

    .line 494
    .local v6, "rowIndex":J
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 497
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v2

    long-to-int v0, v2

    move/from16 v29, v0

    .line 498
    .local v29, "columns":I
    move-object/from16 v0, p1

    array-length v2, v0

    move/from16 v0, v29

    if-eq v0, v2, :cond_0

    .line 499
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "The number of value parameters ("

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p1

    array-length v10, v0

    .line 500
    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, ") does not match the number of columns in the table ("

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    .line 502
    invoke-static/range {v29 .. v29}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, ")."

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 504
    :cond_0
    move/from16 v0, v29

    new-array v0, v0, [Lio/realm/RealmFieldType;

    move-object/from16 v28, v0

    .line 505
    .local v28, "colTypes":[Lio/realm/RealmFieldType;
    const/4 v4, 0x0

    .local v4, "columnIndex":I
    :goto_0
    move/from16 v0, v29

    if-ge v4, v0, :cond_3

    .line 506
    aget-object v31, p1, v4

    .line 507
    .local v31, "value":Ljava/lang/Object;
    int-to-long v2, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v27

    .line 508
    .local v27, "colType":Lio/realm/RealmFieldType;
    aput-object v27, v28, v4

    .line 509
    move-object/from16 v0, v27

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lio/realm/RealmFieldType;->isValid(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 512
    if-nez v31, :cond_1

    .line 513
    const-string v30, "null"

    .line 518
    .local v30, "providedType":Ljava/lang/String;
    :goto_1
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Invalid argument no "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v10, v4, 0x1

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, ". Expected a value compatible with column type "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, ", but got "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, v30

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v10, "."

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 515
    .end local v30    # "providedType":Ljava/lang/String;
    :cond_1
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v30

    .restart local v30    # "providedType":Ljava/lang/String;
    goto :goto_1

    .line 505
    .end local v30    # "providedType":Ljava/lang/String;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 524
    .end local v27    # "colType":Lio/realm/RealmFieldType;
    .end local v31    # "value":Ljava/lang/Object;
    :cond_3
    const-wide/16 v4, 0x0

    .local v4, "columnIndex":J
    :goto_2
    move/from16 v0, v29

    int-to-long v2, v0

    cmp-long v2, v4, v2

    if-gez v2, :cond_8

    .line 525
    long-to-int v2, v4

    aget-object v31, p1, v2

    .line 526
    .restart local v31    # "value":Ljava/lang/Object;
    sget-object v2, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    long-to-int v3, v4

    aget-object v3, v28, v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 569
    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unexpected columnType: "

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    long-to-int v10, v4

    aget-object v10, v28, v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 528
    :pswitch_0
    move-object/from16 v0, p0

    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v31, Ljava/lang/Boolean;

    .end local v31    # "value":Ljava/lang/Object;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 524
    :goto_3
    const-wide/16 v2, 0x1

    add-long/2addr v4, v2

    goto :goto_2

    .line 531
    .restart local v31    # "value":Ljava/lang/Object;
    :pswitch_1
    if-nez v31, :cond_4

    .line 532
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6, v7}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 533
    move-object/from16 v0, p0

    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v8}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_3

    .line 535
    :cond_4
    check-cast v31, Ljava/lang/Number;

    .end local v31    # "value":Ljava/lang/Object;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Number;->longValue()J

    move-result-wide v8

    .local v8, "intValue":J
    move-object/from16 v3, p0

    .line 536
    invoke-virtual/range {v3 .. v9}, Lio/realm/internal/Table;->checkIntValueIsLegal(JJJ)V

    .line 537
    move-object/from16 v0, p0

    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    const/4 v10, 0x0

    invoke-static/range {v2 .. v10}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto :goto_3

    .line 541
    .end local v8    # "intValue":J
    .restart local v31    # "value":Ljava/lang/Object;
    :pswitch_2
    move-object/from16 v0, p0

    iget-wide v10, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v31, Ljava/lang/Float;

    .end local v31    # "value":Ljava/lang/Object;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Float;->floatValue()F

    move-result v16

    const/16 v17, 0x0

    move-wide v12, v4

    move-wide v14, v6

    invoke-static/range {v10 .. v17}, Lio/realm/internal/Table;->nativeSetFloat(JJJFZ)V

    goto :goto_3

    .line 544
    .restart local v31    # "value":Ljava/lang/Object;
    :pswitch_3
    move-object/from16 v0, p0

    iget-wide v10, v0, Lio/realm/internal/Table;->nativePtr:J

    check-cast v31, Ljava/lang/Double;

    .end local v31    # "value":Ljava/lang/Object;
    invoke-virtual/range {v31 .. v31}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v16

    const/16 v18, 0x0

    move-wide v12, v4

    move-wide v14, v6

    invoke-static/range {v10 .. v18}, Lio/realm/internal/Table;->nativeSetDouble(JJJDZ)V

    goto :goto_3

    .line 547
    .restart local v31    # "value":Ljava/lang/Object;
    :pswitch_4
    if-nez v31, :cond_5

    .line 548
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v5, v6, v7}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 549
    move-object/from16 v0, p0

    iget-wide v10, v0, Lio/realm/internal/Table;->nativePtr:J

    const/16 v16, 0x0

    move-wide v12, v4

    move-wide v14, v6

    invoke-static/range {v10 .. v16}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    goto :goto_3

    :cond_5
    move-object/from16 v16, v31

    .line 551
    check-cast v16, Ljava/lang/String;

    .local v16, "stringValue":Ljava/lang/String;
    move-object/from16 v11, p0

    move-wide v12, v4

    move-wide v14, v6

    .line 552
    invoke-virtual/range {v11 .. v16}, Lio/realm/internal/Table;->checkStringValueIsLegal(JJLjava/lang/String;)V

    .line 553
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v18, v0

    move-object/from16 v24, v31

    check-cast v24, Ljava/lang/String;

    const/16 v25, 0x0

    move-wide/from16 v20, v4

    move-wide/from16 v22, v6

    invoke-static/range {v18 .. v25}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto :goto_3

    .line 557
    .end local v16    # "stringValue":Ljava/lang/String;
    :pswitch_5
    if-nez v31, :cond_6

    .line 558
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Null Date is not allowed."

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 559
    :cond_6
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v18, v0

    check-cast v31, Ljava/util/Date;

    .end local v31    # "value":Ljava/lang/Object;
    invoke-virtual/range {v31 .. v31}, Ljava/util/Date;->getTime()J

    move-result-wide v24

    const/16 v26, 0x0

    move-wide/from16 v20, v4

    move-wide/from16 v22, v6

    invoke-static/range {v18 .. v26}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    goto/16 :goto_3

    .line 562
    .restart local v31    # "value":Ljava/lang/Object;
    :pswitch_6
    if-nez v31, :cond_7

    .line 563
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Null Array is not allowed"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 564
    :cond_7
    move-object/from16 v0, p0

    iget-wide v0, v0, Lio/realm/internal/Table;->nativePtr:J

    move-wide/from16 v18, v0

    check-cast v31, [B

    .end local v31    # "value":Ljava/lang/Object;
    move-object/from16 v24, v31

    check-cast v24, [B

    const/16 v25, 0x0

    move-wide/from16 v20, v4

    move-wide/from16 v22, v6

    invoke-static/range {v18 .. v25}, Lio/realm/internal/Table;->nativeSetByteArray(JJJ[BZ)V

    goto/16 :goto_3

    .line 572
    :cond_8
    return-wide v6

    .line 526
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;)J
    .locals 2
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J
    .locals 7
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "isNullable"    # Z

    .prologue
    .line 138
    invoke-direct {p0, p2}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 139
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v4

    move-object v1, p0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeAddColumn(JILjava/lang/String;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J
    .locals 8
    .param p1, "type"    # Lio/realm/RealmFieldType;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 158
    invoke-direct {p0, p2}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 159
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p1}, Lio/realm/RealmFieldType;->getNativeValue()I

    move-result v4

    iget-wide v6, p3, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-object v5, p2

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeAddColumnLink(JILjava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public addEmptyRow()J
    .locals 4

    .prologue
    .line 380
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 381
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, 0x1

    invoke-static {v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public addEmptyRowWithPrimaryKey(Ljava/lang/Object;)J
    .locals 2
    .param p1, "primaryKeyValue"    # Ljava/lang/Object;

    .prologue
    .line 392
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J
    .locals 12
    .param p1, "primaryKeyValue"    # Ljava/lang/Object;
    .param p2, "validation"    # Z

    .prologue
    .line 404
    if-eqz p2, :cond_0

    .line 405
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 406
    invoke-direct {p0}, Lio/realm/internal/Table;->checkHasPrimaryKey()V

    .line 409
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v2

    .line 410
    .local v2, "primaryKeyColumnIndex":J
    invoke-virtual {p0, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v9

    .line 414
    .local v9, "type":Lio/realm/RealmFieldType;
    if-nez p1, :cond_3

    .line 415
    sget-object v0, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v9}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 430
    new-instance v0, Lio/realm/exceptions/RealmException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 418
    :pswitch_0
    if-eqz p2, :cond_1

    invoke-virtual {p0, v2, v3}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v0

    const-wide/16 v10, -0x1

    cmp-long v0, v0, v10

    if-eqz v0, :cond_1

    .line 419
    const-string v0, "null"

    invoke-static {v0}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 421
    :cond_1
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v10, 0x1

    invoke-static {v0, v1, v10, v11}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v4

    .line 422
    .local v4, "rowIndex":J
    sget-object v0, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    if-ne v9, v0, :cond_2

    .line 423
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetStringUnique(JJJLjava/lang/String;)V

    .line 464
    :goto_0
    return-wide v4

    .line 425
    :cond_2
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeSetNullUnique(JJJ)V

    goto :goto_0

    .line 434
    .end local v4    # "rowIndex":J
    :cond_3
    sget-object v0, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v9}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 461
    new-instance v0, Lio/realm/exceptions/RealmException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Cannot check for duplicate rows for unsupported primary key type: "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 436
    :pswitch_1
    instance-of v0, p1, Ljava/lang/String;

    if-nez v0, :cond_4

    .line 437
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary key value is not a String: "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 439
    :cond_4
    if-eqz p2, :cond_5

    move-object v0, p1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v2, v3, v0}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v0

    const-wide/16 v10, -0x1

    cmp-long v0, v0, v10

    if-eqz v0, :cond_5

    .line 440
    invoke-static {p1}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 442
    :cond_5
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v10, 0x1

    invoke-static {v0, v1, v10, v11}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v4

    .line 443
    .restart local v4    # "rowIndex":J
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v6, p1

    check-cast v6, Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetStringUnique(JJJLjava/lang/String;)V

    goto :goto_0

    .line 449
    .end local v4    # "rowIndex":J
    :pswitch_2
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    .line 453
    .local v6, "pkValue":J
    if-eqz p2, :cond_6

    invoke-virtual {p0, v2, v3, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v0

    const-wide/16 v10, -0x1

    cmp-long v0, v0, v10

    if-eqz v0, :cond_6

    .line 454
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 456
    :cond_6
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v10, 0x1

    invoke-static {v0, v1, v10, v11}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v4

    .line 457
    .restart local v4    # "rowIndex":J
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetLongUnique(JJJJ)V

    goto/16 :goto_0

    .line 450
    .end local v4    # "rowIndex":J
    .end local v6    # "pkValue":J
    :catch_0
    move-exception v8

    .line 451
    .local v8, "e":Ljava/lang/RuntimeException;
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary key value is not a long: "

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 415
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 434
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public addEmptyRows(J)J
    .locals 5
    .param p1, "rows"    # J

    .prologue
    const-wide/16 v2, 0x1

    .line 469
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 470
    cmp-long v0, p1, v2

    if-gez v0, :cond_0

    .line 471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "\'rows\' must be > 0."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 474
    cmp-long v0, p1, v2

    if-lez v0, :cond_1

    .line 475
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Multiple empty rows cannot be created if a primary key is defined for the table."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 477
    :cond_1
    invoke-virtual {p0}, Lio/realm/internal/Table;->addEmptyRow()J

    move-result-wide v0

    .line 479
    :goto_0
    return-wide v0

    :cond_2
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAddEmptyRow(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method public addSearchIndex(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 828
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 829
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAddSearchIndex(JJ)V

    .line 830
    return-void
.end method

.method public averageDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 988
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 971
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageLong(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 954
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeAverageInt(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method checkDuplicatedNullForPrimaryKeyValue(JJ)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J

    .prologue
    .line 645
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKeyColumn(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 646
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v2

    .line 647
    .local v2, "type":Lio/realm/RealmFieldType;
    sget-object v3, Lio/realm/internal/Table$1;->$SwitchMap$io$realm$RealmFieldType:[I

    invoke-virtual {v2}, Lio/realm/RealmFieldType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 660
    .end local v2    # "type":Lio/realm/RealmFieldType;
    :cond_0
    :goto_0
    return-void

    .line 650
    .restart local v2    # "type":Lio/realm/RealmFieldType;
    :pswitch_0
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v0

    .line 651
    .local v0, "rowIndex":J
    cmp-long v3, v0, p3

    if-eqz v3, :cond_0

    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 652
    const-string v3, "null"

    invoke-static {v3}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_0

    .line 647
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method checkImmutable()V
    .locals 1

    .prologue
    .line 925
    invoke-virtual {p0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 926
    invoke-static {}, Lio/realm/internal/Table;->throwImmutable()V

    .line 928
    :cond_0
    return-void
.end method

.method checkIntValueIsLegal(JJJ)V
    .locals 5
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J
    .param p5, "value"    # J

    .prologue
    .line 635
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKeyColumn(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 636
    invoke-virtual {p0, p1, p2, p5, p6}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v0

    .line 637
    .local v0, "rowIndex":J
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 638
    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 641
    .end local v0    # "rowIndex":J
    :cond_0
    return-void
.end method

.method checkStringValueIsLegal(JJLjava/lang/String;)V
    .locals 5
    .param p1, "columnIndex"    # J
    .param p3, "rowToUpdate"    # J
    .param p5, "value"    # Ljava/lang/String;

    .prologue
    .line 626
    invoke-direct {p0, p1, p2}, Lio/realm/internal/Table;->isPrimaryKey(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 627
    invoke-virtual {p0, p1, p2, p5}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v0

    .line 628
    .local v0, "rowIndex":J
    cmp-long v2, v0, p3

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 629
    invoke-static {p5}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    .line 632
    .end local v0    # "rowIndex":J
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 297
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 298
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeClear(J)V

    .line 299
    return-void
.end method

.method public convertColumnToNotNullable(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 270
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeConvertColumnToNotNullable(JJ)V

    .line 271
    return-void
.end method

.method public convertColumnToNullable(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 261
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeConvertColumnToNullable(JJ)V

    .line 262
    return-void
.end method

.method public count(JD)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 1014
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeCountDouble(JJD)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JF)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 1010
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeCountFloat(JJF)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1006
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeCountLong(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JLjava/lang/String;)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1018
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-object v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeCountString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method protected native createNative()J
.end method

.method public findFirstBoolean(JZ)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Z

    .prologue
    .line 1036
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindFirstBool(JJZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstDate(JLjava/util/Date;)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "date"    # Ljava/util/Date;

    .prologue
    .line 1048
    if-nez p3, :cond_0

    .line 1049
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1051
    :cond_0
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindFirstTimestamp(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstDouble(JD)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # D

    .prologue
    .line 1044
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeFindFirstDouble(JJD)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstFloat(JF)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # F

    .prologue
    .line 1040
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeFindFirstFloat(JJF)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstLong(JJ)J
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1032
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstNull(J)J
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 1068
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeFindFirstNull(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public findFirstString(JLjava/lang/String;)J
    .locals 3
    .param p1, "columnIndex"    # J
    .param p3, "value"    # Ljava/lang/String;

    .prologue
    .line 1055
    if-nez p3, :cond_0

    .line 1056
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null is not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1058
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-static {v0, v1, p1, p2, p3}, Lio/realm/internal/Table;->nativeFindFirstString(JJLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getBinaryByteArray(JJ)[B
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 708
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetByteArray(JJJ)[B

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 681
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetBoolean(JJJ)Z

    move-result v0

    return v0
.end method

.method public getCheckedRow(J)Lio/realm/internal/CheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 758
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/CheckedRow;->get(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/CheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()J
    .locals 2

    .prologue
    .line 308
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetColumnCount(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnIndex(Ljava/lang/String;)J
    .locals 2
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 328
    if-nez p1, :cond_0

    .line 329
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Column name can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/Table;->nativeGetColumnIndex(JLjava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public getColumnName(J)Ljava/lang/String;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 318
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnType(J)Lio/realm/RealmFieldType;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 341
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnType(JJ)I

    move-result v0

    invoke-static {v0}, Lio/realm/RealmFieldType;->fromNativeValue(I)Lio/realm/RealmFieldType;

    move-result-object v0

    return-object v0
.end method

.method public getDate(JJ)Ljava/util/Date;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 693
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetTimestamp(JJJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public getDouble(JJ)D
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 689
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetDouble(JJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(JJ)F
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 685
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetFloat(JJJ)F

    move-result v0

    return v0
.end method

.method public getLink(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 712
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetLink(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLinkTarget(J)Lio/realm/internal/Table;
    .locals 7
    .param p1, "columnIndex"    # J

    .prologue
    .line 716
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v4, v5, p1, p2}, Lio/realm/internal/Table;->nativeGetLinkTarget(JJ)J

    move-result-wide v0

    .line 718
    .local v0, "nativeTablePointer":J
    new-instance v2, Lio/realm/internal/Table;

    iget-object v3, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    invoke-direct {v2, v3, v0, v1}, Lio/realm/internal/Table;-><init>(Lio/realm/internal/SharedRealm;J)V

    .line 719
    .local v2, "table":Lio/realm/internal/Table;
    return-object v2
.end method

.method public getLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 677
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetLong(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1097
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeGetName(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 102
    sget-wide v0, Lio/realm/internal/Table;->nativeFinalizerPtr:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    return-wide v0
.end method

.method public getNativeTablePointer()J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    return-wide v0
.end method

.method public getPrimaryKey()J
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const-wide/16 v6, -0x2

    .line 585
    iget-wide v8, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    cmp-long v3, v8, v10

    if-gez v3, :cond_0

    iget-wide v8, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    cmp-long v3, v8, v6

    if-nez v3, :cond_2

    .line 586
    :cond_0
    iget-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 602
    :cond_1
    :goto_0
    return-wide v6

    .line 588
    :cond_2
    invoke-direct {p0}, Lio/realm/internal/Table;->getPrimaryKeyTable()Lio/realm/internal/Table;

    move-result-object v2

    .line 589
    .local v2, "pkTable":Lio/realm/internal/Table;
    if-eqz v2, :cond_1

    .line 593
    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lio/realm/internal/Table;->tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 594
    .local v0, "className":Ljava/lang/String;
    invoke-virtual {v2, v10, v11, v0}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v4

    .line 595
    .local v4, "rowIndex":J
    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-eqz v3, :cond_3

    .line 596
    invoke-virtual {v2, v4, v5}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v3

    const-wide/16 v6, 0x1

    invoke-virtual {v3, v6, v7}, Lio/realm/internal/UncheckedRow;->getString(J)Ljava/lang/String;

    move-result-object v1

    .line 597
    .local v1, "pkColumnName":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 602
    .end local v1    # "pkColumnName":Ljava/lang/String;
    :goto_1
    iget-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    goto :goto_0

    .line 599
    :cond_3
    iput-wide v6, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    goto :goto_1
.end method

.method public getString(JJ)Ljava/lang/String;
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 704
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeGetString(JJJ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 0

    .prologue
    .line 106
    return-object p0
.end method

.method public getUncheckedRow(J)Lio/realm/internal/UncheckedRow;
    .locals 1
    .param p1, "index"    # J

    .prologue
    .line 734
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->getByRowIndex(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getUncheckedRowByPointer(J)Lio/realm/internal/UncheckedRow;
    .locals 1
    .param p1, "nativeRowPointer"    # J

    .prologue
    .line 745
    iget-object v0, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-static {v0, p0, p1, p2}, Lio/realm/internal/UncheckedRow;->getByRowPointer(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/UncheckedRow;

    move-result-object v0

    return-object v0
.end method

.method public getVersion()J
    .locals 2

    .prologue
    .line 1167
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeVersion(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public hasPrimaryKey()Z
    .locals 4

    .prologue
    .line 622
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasSameSchema(Lio/realm/internal/Table;)Z
    .locals 4
    .param p1, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 1146
    if-nez p1, :cond_0

    .line 1147
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The argument cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1149
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    iget-wide v2, p1, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/Table;->nativeHasSameSchema(JJ)Z

    move-result v0

    return v0
.end method

.method public hasSearchIndex(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 908
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeHasSearchIndex(JJ)Z

    move-result v0

    return v0
.end method

.method public isColumnNullable(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 252
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeIsColumnNullable(JJ)Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 4

    .prologue
    .line 290
    invoke-virtual {p0}, Lio/realm/internal/Table;->size()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isImmutable()Z
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/realm/internal/Table;->sharedRealm:Lio/realm/internal/SharedRealm;

    invoke-virtual {v0}, Lio/realm/internal/SharedRealm;->isInTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 723
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeIsNull(JJJ)Z

    move-result v0

    return v0
.end method

.method public isNullLink(JJ)Z
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 912
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeIsNullLink(JJJ)Z

    move-result v0

    return v0
.end method

.method public isValid()Z
    .locals 4

    .prologue
    .line 120
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeIsValid(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public lowerBoundLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1073
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeLowerBoundInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public maximumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 994
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/Table;->nativeMaximumTimestamp(JJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public maximumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 980
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumDouble(JJ)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 963
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumFloat(JJ)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 946
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMaximumInt(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumDate(J)Ljava/util/Date;
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 998
    new-instance v0, Ljava/util/Date;

    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/Table;->nativeMinimumTimestamp(JJ)J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    return-object v0
.end method

.method public minimumDouble(J)Ljava/lang/Double;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 984
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumDouble(JJ)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(J)Ljava/lang/Float;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 967
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumFloat(JJ)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumLong(J)Ljava/lang/Long;
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 950
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMinimumInt(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public moveLastOver(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 367
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 368
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeMoveLastOver(JJ)V

    .line 369
    return-void
.end method

.method native nativeGetRowPtr(JJ)J
.end method

.method public nullifyLink(JJ)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J

    .prologue
    .line 916
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    .line 917
    return-void
.end method

.method public pivot(JJLio/realm/internal/Table$PivotType;)Lio/realm/internal/Table;
    .locals 13
    .param p1, "stringCol"    # J
    .param p3, "intCol"    # J
    .param p5, "pivotType"    # Lio/realm/internal/Table$PivotType;

    .prologue
    .line 1080
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1081
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Group by column must be of type String"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1082
    :cond_0
    move-wide/from16 v0, p3

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v3

    sget-object v4, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    invoke-virtual {v3, v4}, Lio/realm/RealmFieldType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1083
    new-instance v3, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Aggregation column must be of type Int"

    invoke-direct {v3, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 1084
    :cond_1
    new-instance v2, Lio/realm/internal/Table;

    invoke-direct {v2}, Lio/realm/internal/Table;-><init>()V

    .line 1085
    .local v2, "result":Lio/realm/internal/Table;
    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v0, p5

    iget v10, v0, Lio/realm/internal/Table$PivotType;->value:I

    iget-wide v11, v2, Lio/realm/internal/Table;->nativePtr:J

    move-object v3, p0

    move-wide v6, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v3 .. v12}, Lio/realm/internal/Table;->nativePivot(JJJIJ)V

    .line 1086
    return-object v2
.end method

.method public remove(J)V
    .locals 3
    .param p1, "rowIndex"    # J

    .prologue
    .line 352
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 353
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRemove(JJ)V

    .line 354
    return-void
.end method

.method public removeColumn(J)V
    .locals 5
    .param p1, "columnIndex"    # J

    .prologue
    .line 177
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v0

    .line 180
    .local v0, "oldPkColumnIndex":J
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3, p1, p2}, Lio/realm/internal/Table;->nativeRemoveColumn(JJ)V

    .line 184
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    .line 189
    cmp-long v2, v0, p1

    if-nez v2, :cond_1

    .line 190
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    cmp-long v2, v0, p1

    if-lez v2, :cond_0

    .line 195
    invoke-direct {p0}, Lio/realm/internal/Table;->invalidateCachedPrimaryKeyIndex()V

    goto :goto_0
.end method

.method public removeFirst()V
    .locals 2

    .prologue
    .line 357
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 358
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lio/realm/internal/Table;->remove(J)V

    .line 359
    return-void
.end method

.method public removeLast()V
    .locals 2

    .prologue
    .line 362
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 363
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeRemoveLast(J)V

    .line 364
    return-void
.end method

.method public removeSearchIndex(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 833
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 834
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeRemoveSearchIndex(JJ)V

    .line 835
    return-void
.end method

.method public renameColumn(JLjava/lang/String;)V
    .locals 23
    .param p1, "columnIndex"    # J
    .param p3, "newName"    # Ljava/lang/String;

    .prologue
    .line 211
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1}, Lio/realm/internal/Table;->verifyColumnName(Ljava/lang/String;)V

    .line 213
    move-object/from16 v0, p0

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    invoke-direct {v0, v4, v5, v1, v2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v16

    .line 216
    .local v16, "oldName":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v20

    .line 219
    .local v20, "oldPkColumnIndex":J
    move-object/from16 v0, p0

    iget-wide v6, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v5, p0

    move-wide/from16 v8, p1

    move-object/from16 v10, p3

    invoke-direct/range {v5 .. v10}, Lio/realm/internal/Table;->nativeRenameColumn(JJLjava/lang/String;)V

    .line 222
    cmp-long v4, v20, p1

    if-nez v4, :cond_1

    .line 224
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lio/realm/internal/Table;->tableNameToClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 225
    .local v17, "className":Ljava/lang/String;
    invoke-direct/range {p0 .. p0}, Lio/realm/internal/Table;->getPrimaryKeyTable()Lio/realm/internal/Table;

    move-result-object v19

    .line 226
    .local v19, "pkTable":Lio/realm/internal/Table;
    if-nez v19, :cond_0

    .line 227
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Table is not created from a SharedRealm, primary key is not available"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    .end local v17    # "className":Ljava/lang/String;
    .end local v19    # "pkTable":Lio/realm/internal/Table;
    :catch_0
    move-exception v18

    .line 239
    .local v18, "e":Ljava/lang/Exception;
    move-object/from16 v0, p0

    iget-wide v12, v0, Lio/realm/internal/Table;->nativePtr:J

    move-object/from16 v11, p0

    move-wide/from16 v14, p1

    invoke-direct/range {v11 .. v16}, Lio/realm/internal/Table;->nativeRenameColumn(JJLjava/lang/String;)V

    .line 240
    throw v18

    .line 230
    .end local v18    # "e":Ljava/lang/Exception;
    .restart local v17    # "className":Ljava/lang/String;
    .restart local v19    # "pkTable":Lio/realm/internal/Table;
    :cond_0
    const-wide/16 v4, 0x0

    :try_start_1
    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v4, v5, v1}, Lio/realm/internal/Table;->findFirstString(JLjava/lang/String;)J

    move-result-wide v8

    .line 231
    .local v8, "pkRowIndex":J
    const-wide/16 v4, -0x1

    cmp-long v4, v8, v4

    if-eqz v4, :cond_2

    .line 232
    move-object/from16 v0, v19

    iget-wide v4, v0, Lio/realm/internal/Table;->nativePtr:J

    const-wide/16 v6, 0x1

    const/4 v11, 0x0

    move-object/from16 v10, p3

    invoke-static/range {v4 .. v11}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    .line 243
    .end local v8    # "pkRowIndex":J
    .end local v17    # "className":Ljava/lang/String;
    .end local v19    # "pkTable":Lio/realm/internal/Table;
    :cond_1
    return-void

    .line 234
    .restart local v8    # "pkRowIndex":J
    .restart local v17    # "className":Ljava/lang/String;
    .restart local v19    # "pkTable":Lio/realm/internal/Table;
    :cond_2
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Non-existent PrimaryKey column cannot be renamed"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method public setBinaryByteArray(JJ[BZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "data"    # [B
    .param p6, "isDefault"    # Z

    .prologue
    .line 812
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 813
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetByteArray(JJJ[BZ)V

    .line 814
    return-void
.end method

.method public setBoolean(JJZZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Z
    .param p6, "isDefault"    # Z

    .prologue
    .line 772
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 773
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetBoolean(JJJZZ)V

    .line 774
    return-void
.end method

.method public setDate(JJLjava/util/Date;Z)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "date"    # Ljava/util/Date;
    .param p6, "isDefault"    # Z

    .prologue
    .line 787
    if-nez p5, :cond_0

    .line 788
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null Date is not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 789
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 790
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-virtual {p5}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-wide v2, p1

    move-wide v4, p3

    move v8, p6

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetTimestamp(JJJJZ)V

    .line 791
    return-void
.end method

.method public setDouble(JJDZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # D
    .param p7, "isDefault"    # Z

    .prologue
    .line 782
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 783
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetDouble(JJJDZ)V

    .line 784
    return-void
.end method

.method public setFloat(JJFZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # F
    .param p6, "isDefault"    # Z

    .prologue
    .line 777
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 778
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetFloat(JJJFZ)V

    .line 779
    return-void
.end method

.method public setLink(JJJZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J
    .param p7, "isDefault"    # Z

    .prologue
    .line 817
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 818
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    .line 819
    return-void
.end method

.method public setLong(JJJZ)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # J
    .param p7, "isDefault"    # Z

    .prologue
    .line 766
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 767
    invoke-virtual/range {p0 .. p6}, Lio/realm/internal/Table;->checkIntValueIsLegal(JJJ)V

    .line 768
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move-wide v6, p5

    move/from16 v8, p7

    invoke-static/range {v0 .. v8}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 769
    return-void
.end method

.method public setNull(JJZ)V
    .locals 7
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "isDefault"    # Z

    .prologue
    .line 822
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 823
    invoke-virtual {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 824
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    .line 825
    return-void
.end method

.method public setPrimaryKey(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 854
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeGetColumnName(JJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 855
    return-void
.end method

.method public setPrimaryKey(Ljava/lang/String;)V
    .locals 7
    .param p1, "columnName"    # Ljava/lang/String;

    .prologue
    .line 846
    invoke-direct {p0}, Lio/realm/internal/Table;->getPrimaryKeyTable()Lio/realm/internal/Table;

    move-result-object v0

    .line 847
    .local v0, "pkTable":Lio/realm/internal/Table;
    if-nez v0, :cond_0

    .line 848
    new-instance v1, Lio/realm/exceptions/RealmException;

    const-string v2, "Primary keys are only supported if Table is part of a Group"

    invoke-direct {v1, v2}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 850
    :cond_0
    iget-wide v2, v0, Lio/realm/internal/Table;->nativePtr:J

    iget-wide v4, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/Table;->nativeSetPrimaryKey(JJLjava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/internal/Table;->cachedPrimaryKeyColumnIndex:J

    .line 851
    return-void
.end method

.method public setString(JJLjava/lang/String;Z)V
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "rowIndex"    # J
    .param p5, "value"    # Ljava/lang/String;
    .param p6, "isDefault"    # Z

    .prologue
    .line 801
    invoke-virtual {p0}, Lio/realm/internal/Table;->checkImmutable()V

    .line 802
    if-nez p5, :cond_0

    .line 803
    invoke-virtual {p0, p1, p2, p3, p4}, Lio/realm/internal/Table;->checkDuplicatedNullForPrimaryKeyValue(JJ)V

    .line 804
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move v6, p6

    invoke-static/range {v0 .. v6}, Lio/realm/internal/Table;->nativeSetNull(JJJZ)V

    .line 809
    :goto_0
    return-void

    .line 806
    :cond_0
    invoke-virtual/range {p0 .. p5}, Lio/realm/internal/Table;->checkStringValueIsLegal(JJLjava/lang/String;)V

    .line 807
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    move-wide v2, p1

    move-wide v4, p3

    move-object v6, p5

    move v7, p6

    invoke-static/range {v0 .. v7}, Lio/realm/internal/Table;->nativeSetString(JJJLjava/lang/String;Z)V

    goto :goto_0
.end method

.method public size()J
    .locals 2

    .prologue
    .line 281
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeSize(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sumDouble(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 976
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumDouble(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(J)D
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 959
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumFloat(JJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumLong(J)J
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 942
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/Table;->nativeSumInt(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public toJson()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1101
    iget-wide v0, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/Table;->nativeToJson(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 1105
    invoke-virtual {p0}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 1106
    .local v0, "columnCount":J
    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v3

    .line 1107
    .local v3, "name":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "The Table "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1108
    .local v5, "stringBuilder":Ljava/lang/StringBuilder;
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1109
    invoke-virtual {p0}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1110
    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1112
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1113
    invoke-virtual {p0}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    invoke-virtual {p0, v6, v7}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v4

    .line 1114
    .local v4, "pkFieldName":Ljava/lang/String;
    const-string v6, "has \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\' field as a PrimaryKey, and "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1116
    .end local v4    # "pkFieldName":Ljava/lang/String;
    :cond_1
    const-string v6, "contains "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1117
    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1118
    const-string v6, " columns: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1120
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    int-to-long v6, v2

    cmp-long v6, v6, v0

    if-gez v6, :cond_3

    .line 1121
    if-eqz v2, :cond_2

    .line 1122
    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1124
    :cond_2
    int-to-long v6, v2

    invoke-virtual {p0, v6, v7}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1120
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1126
    :cond_3
    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1128
    const-string v6, " And "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1129
    invoke-virtual {p0}, Lio/realm/internal/Table;->size()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1130
    const-string v6, " rows."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1132
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    return-object v6
.end method

.method public upperBoundLong(JJ)J
    .locals 9
    .param p1, "columnIndex"    # J
    .param p3, "value"    # J

    .prologue
    .line 1076
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v1 .. v7}, Lio/realm/internal/Table;->nativeUpperBoundInt(JJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public where()Lio/realm/internal/TableQuery;
    .locals 4

    .prologue
    .line 1026
    iget-wide v2, p0, Lio/realm/internal/Table;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/Table;->nativeWhere(J)J

    move-result-wide v0

    .line 1028
    .local v0, "nativeQueryPtr":J
    new-instance v2, Lio/realm/internal/TableQuery;

    iget-object v3, p0, Lio/realm/internal/Table;->context:Lio/realm/internal/Context;

    invoke-direct {v2, v3, p0, v0, v1}, Lio/realm/internal/TableQuery;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    return-object v2
.end method
