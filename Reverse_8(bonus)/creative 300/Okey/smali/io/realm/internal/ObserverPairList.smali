.class Lio/realm/internal/ObserverPairList;
.super Ljava/lang/Object;
.source "ObserverPairList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/internal/ObserverPairList$Callback;,
        Lio/realm/internal/ObserverPairList$ObserverPair;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lio/realm/internal/ObserverPairList$ObserverPair;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private cleared:Z

.field private pairs:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 35
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/ObserverPairList;->cleared:Z

    return-void
.end method


# virtual methods
.method public add(Lio/realm/internal/ObserverPairList$ObserverPair;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    .local p1, "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    const/4 v1, 0x0

    .line 123
    iget-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iput-boolean v1, p1, Lio/realm/internal/ObserverPairList$ObserverPair;->removed:Z

    .line 127
    :cond_0
    iget-boolean v0, p0, Lio/realm/internal/ObserverPairList;->cleared:Z

    if-eqz v0, :cond_1

    .line 128
    iput-boolean v1, p0, Lio/realm/internal/ObserverPairList;->cleared:Z

    .line 130
    :cond_1
    return-void
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 118
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/ObserverPairList;->cleared:Z

    .line 119
    iget-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 120
    return-void
.end method

.method foreach(Lio/realm/internal/ObserverPairList$Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/internal/ObserverPairList$Callback",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    .local p1, "callback":Lio/realm/internal/ObserverPairList$Callback;, "Lio/realm/internal/ObserverPairList$Callback<TT;>;"
    iget-object v2, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/ObserverPairList$ObserverPair;

    .line 100
    .local v1, "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    iget-boolean v3, p0, Lio/realm/internal/ObserverPairList;->cleared:Z

    if-eqz v3, :cond_2

    .line 111
    .end local v1    # "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    :cond_1
    return-void

    .line 103
    .restart local v1    # "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    :cond_2
    iget-object v3, v1, Lio/realm/internal/ObserverPairList$ObserverPair;->observerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 104
    .local v0, "observer":Ljava/lang/Object;
    if-nez v0, :cond_3

    .line 105
    iget-object v3, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 106
    :cond_3
    iget-boolean v3, v1, Lio/realm/internal/ObserverPairList$ObserverPair;->removed:Z

    if-nez v3, :cond_0

    .line 107
    invoke-interface {p1, v1, v0}, Lio/realm/internal/ObserverPairList$Callback;->onCalled(Lio/realm/internal/ObserverPairList$ObserverPair;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 114
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    iget-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public remove(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<S:",
            "Ljava/lang/Object;",
            "U:",
            "Ljava/lang/Object;",
            ">(TS;TU;)V"
        }
    .end annotation

    .prologue
    .line 133
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    .local p1, "observer":Ljava/lang/Object;, "TS;"
    .local p2, "listener":Ljava/lang/Object;, "TU;"
    iget-object v1, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/ObserverPairList$ObserverPair;

    .line 134
    .local v0, "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    iget-object v2, v0, Lio/realm/internal/ObserverPairList$ObserverPair;->observerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_0

    iget-object v2, v0, Lio/realm/internal/ObserverPairList$ObserverPair;->listener:Ljava/lang/Object;

    invoke-virtual {p2, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    const/4 v1, 0x1

    iput-boolean v1, v0, Lio/realm/internal/ObserverPairList$ObserverPair;->removed:Z

    .line 136
    iget-object v1, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 140
    .end local v0    # "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    :cond_1
    return-void
.end method

.method removeByObserver(Ljava/lang/Object;)V
    .locals 4
    .param p1, "observer"    # Ljava/lang/Object;

    .prologue
    .line 143
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    iget-object v2, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/ObserverPairList$ObserverPair;

    .line 144
    .local v1, "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    iget-object v3, v1, Lio/realm/internal/ObserverPairList$ObserverPair;->observerRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .line 145
    .local v0, "object":Ljava/lang/Object;
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    .line 146
    :cond_1
    const/4 v3, 0x1

    iput-boolean v3, v1, Lio/realm/internal/ObserverPairList$ObserverPair;->removed:Z

    .line 147
    iget-object v3, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 150
    .end local v0    # "object":Ljava/lang/Object;
    .end local v1    # "pair":Lio/realm/internal/ObserverPairList$ObserverPair;, "TT;"
    :cond_2
    return-void
.end method

.method public size()I
    .locals 1

    .prologue
    .line 153
    .local p0, "this":Lio/realm/internal/ObserverPairList;, "Lio/realm/internal/ObserverPairList<TT;>;"
    iget-object v0, p0, Lio/realm/internal/ObserverPairList;->pairs:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
