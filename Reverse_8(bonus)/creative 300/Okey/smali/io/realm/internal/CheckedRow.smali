.class public Lio/realm/internal/CheckedRow;
.super Lio/realm/internal/UncheckedRow;
.source "CheckedRow.java"


# instance fields
.field private originalRow:Lio/realm/internal/UncheckedRow;


# direct methods
.method private constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    .locals 1
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "parent"    # Lio/realm/internal/Table;
    .param p3, "nativePtr"    # J

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    .line 37
    return-void
.end method

.method private constructor <init>(Lio/realm/internal/UncheckedRow;)V
    .locals 0
    .param p1, "row"    # Lio/realm/internal/UncheckedRow;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lio/realm/internal/UncheckedRow;-><init>(Lio/realm/internal/UncheckedRow;)V

    .line 41
    iput-object p1, p0, Lio/realm/internal/CheckedRow;->originalRow:Lio/realm/internal/UncheckedRow;

    .line 42
    return-void
.end method

.method public static get(Lio/realm/internal/Context;Lio/realm/internal/LinkView;J)Lio/realm/internal/CheckedRow;
    .locals 4
    .param p0, "context"    # Lio/realm/internal/Context;
    .param p1, "linkView"    # Lio/realm/internal/LinkView;
    .param p2, "index"    # J

    .prologue
    .line 66
    invoke-virtual {p1}, Lio/realm/internal/LinkView;->getNativePtr()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3, p2, p3}, Lio/realm/internal/LinkView;->nativeGetRow(JJ)J

    move-result-wide v0

    .line 67
    .local v0, "nativeRowPointer":J
    new-instance v2, Lio/realm/internal/CheckedRow;

    invoke-virtual {p1}, Lio/realm/internal/LinkView;->getTargetTable()Lio/realm/internal/Table;

    move-result-object v3

    invoke-direct {v2, p0, v3, v0, v1}, Lio/realm/internal/CheckedRow;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    return-object v2
.end method

.method public static get(Lio/realm/internal/Context;Lio/realm/internal/Table;J)Lio/realm/internal/CheckedRow;
    .locals 4
    .param p0, "context"    # Lio/realm/internal/Context;
    .param p1, "table"    # Lio/realm/internal/Table;
    .param p2, "index"    # J

    .prologue
    .line 53
    invoke-virtual {p1}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3, p2, p3}, Lio/realm/internal/Table;->nativeGetRowPtr(JJ)J

    move-result-wide v0

    .line 54
    .local v0, "nativeRowPointer":J
    new-instance v2, Lio/realm/internal/CheckedRow;

    invoke-direct {v2, p0, p1, v0, v1}, Lio/realm/internal/CheckedRow;-><init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V

    return-object v2
.end method

.method public static getFromRow(Lio/realm/internal/UncheckedRow;)Lio/realm/internal/CheckedRow;
    .locals 1
    .param p0, "row"    # Lio/realm/internal/UncheckedRow;

    .prologue
    .line 76
    new-instance v0, Lio/realm/internal/CheckedRow;

    invoke-direct {v0, p0}, Lio/realm/internal/CheckedRow;-><init>(Lio/realm/internal/UncheckedRow;)V

    return-object v0
.end method


# virtual methods
.method public isNull(J)Z
    .locals 1
    .param p1, "columnIndex"    # J

    .prologue
    .line 91
    invoke-super {p0, p1, p2}, Lio/realm/internal/UncheckedRow;->isNull(J)Z

    move-result v0

    return v0
.end method

.method public isNullLink(J)Z
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 81
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/CheckedRow;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v0

    .line 82
    .local v0, "columnType":Lio/realm/RealmFieldType;
    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-ne v0, v1, :cond_1

    .line 83
    :cond_0
    invoke-super {p0, p1, p2}, Lio/realm/internal/UncheckedRow;->isNullLink(J)Z

    move-result v1

    .line 85
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected native nativeGetBoolean(JJ)Z
.end method

.method protected native nativeGetByteArray(JJ)[B
.end method

.method protected native nativeGetColumnCount(J)J
.end method

.method protected native nativeGetColumnIndex(JLjava/lang/String;)J
.end method

.method protected native nativeGetColumnName(JJ)Ljava/lang/String;
.end method

.method protected native nativeGetColumnType(JJ)I
.end method

.method protected native nativeGetDouble(JJ)D
.end method

.method protected native nativeGetFloat(JJ)F
.end method

.method protected native nativeGetLink(JJ)J
.end method

.method protected native nativeGetLinkView(JJ)J
.end method

.method protected native nativeGetLong(JJ)J
.end method

.method protected native nativeGetString(JJ)Ljava/lang/String;
.end method

.method protected native nativeGetTimestamp(JJ)J
.end method

.method protected native nativeIsNullLink(JJ)Z
.end method

.method protected native nativeNullifyLink(JJ)V
.end method

.method protected native nativeSetBoolean(JJZ)V
.end method

.method protected native nativeSetByteArray(JJ[B)V
.end method

.method protected native nativeSetDouble(JJD)V
.end method

.method protected native nativeSetFloat(JJF)V
.end method

.method protected native nativeSetLink(JJJ)V
.end method

.method protected native nativeSetLong(JJJ)V
.end method

.method protected native nativeSetString(JJLjava/lang/String;)V
.end method

.method protected native nativeSetTimestamp(JJJ)V
.end method

.method public setNull(J)V
    .locals 3
    .param p1, "columnIndex"    # J

    .prologue
    .line 102
    invoke-virtual {p0, p1, p2}, Lio/realm/internal/CheckedRow;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v0

    .line 103
    .local v0, "columnType":Lio/realm/RealmFieldType;
    sget-object v1, Lio/realm/RealmFieldType;->BINARY:Lio/realm/RealmFieldType;

    if-ne v0, v1, :cond_0

    .line 104
    const/4 v1, 0x0

    invoke-super {p0, p1, p2, v1}, Lio/realm/internal/UncheckedRow;->setBinaryByteArray(J[B)V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    invoke-super {p0, p1, p2}, Lio/realm/internal/UncheckedRow;->setNull(J)V

    goto :goto_0
.end method
