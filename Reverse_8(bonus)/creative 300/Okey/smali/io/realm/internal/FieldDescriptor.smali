.class public Lio/realm/internal/FieldDescriptor;
.super Ljava/lang/Object;
.source "FieldDescriptor.java"


# instance fields
.field private columnIndices:[J

.field private fieldName:Ljava/lang/String;

.field private fieldType:Lio/realm/RealmFieldType;

.field private searchIndex:Z


# direct methods
.method public constructor <init>(Lio/realm/internal/Table;Ljava/lang/String;ZZ)V
    .locals 18
    .param p1, "table"    # Lio/realm/internal/Table;
    .param p2, "fieldDescription"    # Ljava/lang/String;
    .param p3, "allowLink"    # Z
    .param p4, "allowList"    # Z

    .prologue
    .line 33
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 34
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 35
    :cond_0
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Non-empty field name must be provided"

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 37
    :cond_1
    const-string v13, "."

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "."

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 38
    :cond_2
    new-instance v13, Ljava/lang/IllegalArgumentException;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Illegal field name. It cannot start or end with a \'.\': "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 40
    :cond_3
    const-string v13, "."

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v13

    if-eqz v13, :cond_b

    .line 42
    const-string v13, "\\."

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 43
    .local v9, "names":[Ljava/lang/String;
    array-length v13, v9

    new-array v4, v13, [J

    .line 44
    .local v4, "columnIndices":[J
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    array-length v13, v9

    add-int/lit8 v13, v13, -0x1

    if-ge v8, v13, :cond_9

    .line 45
    aget-object v13, v9, v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v10

    .line 46
    .local v10, "index":J
    const-wide/16 v14, -0x1

    cmp-long v13, v10, v14

    if-nez v13, :cond_4

    .line 47
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Invalid field name: \'%s\' does not refer to a class."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aget-object v17, v9, v8

    aput-object v17, v15, v16

    .line 48
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 50
    :cond_4
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v12

    .line 51
    .local v12, "type":Lio/realm/RealmFieldType;
    if-nez p3, :cond_5

    sget-object v13, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-ne v12, v13, :cond_5

    .line 52
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "\'RealmObject\' field \'%s\' is not a supported link field here."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aget-object v17, v9, v8

    aput-object v17, v15, v16

    .line 53
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 54
    :cond_5
    if-nez p4, :cond_6

    sget-object v13, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-ne v12, v13, :cond_6

    .line 55
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "\'RealmList\' field \'%s\' is not a supported link field here."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aget-object v17, v9, v8

    aput-object v17, v15, v16

    .line 56
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 57
    :cond_6
    sget-object v13, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v12, v13, :cond_7

    sget-object v13, Lio/realm/RealmFieldType;->LIST:Lio/realm/RealmFieldType;

    if-ne v12, v13, :cond_8

    .line 58
    :cond_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v11}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object p1

    .line 59
    aput-wide v10, v4, v8

    .line 44
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 61
    :cond_8
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Invalid field name: \'%s\' does not refer to a class."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aget-object v17, v9, v8

    aput-object v17, v15, v16

    .line 62
    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 67
    .end local v10    # "index":J
    .end local v12    # "type":Lio/realm/RealmFieldType;
    :cond_9
    array-length v13, v9

    add-int/lit8 v13, v13, -0x1

    aget-object v5, v9, v13

    .line 68
    .local v5, "columnName":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    .line 69
    .local v2, "columnIndex":J
    array-length v13, v9

    add-int/lit8 v13, v13, -0x1

    aput-wide v2, v4, v13

    .line 70
    const-wide/16 v14, -0x1

    cmp-long v13, v2, v14

    if-nez v13, :cond_a

    .line 71
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "\'%s\' is not a field name in class \'%s\'."

    const/4 v15, 0x2

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object v5, v15, v16

    const/16 v16, 0x1

    .line 72
    invoke-virtual/range {p1 .. p1}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 75
    :cond_a
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lio/realm/internal/FieldDescriptor;->fieldType:Lio/realm/RealmFieldType;

    .line 76
    move-object/from16 v0, p0

    iput-object v5, v0, Lio/realm/internal/FieldDescriptor;->fieldName:Ljava/lang/String;

    .line 77
    move-object/from16 v0, p0

    iput-object v4, v0, Lio/realm/internal/FieldDescriptor;->columnIndices:[J

    .line 78
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lio/realm/internal/FieldDescriptor;->searchIndex:Z

    .line 89
    .end local v2    # "columnIndex":J
    .end local v4    # "columnIndices":[J
    .end local v5    # "columnName":Ljava/lang/String;
    .end local v8    # "i":I
    .end local v9    # "names":[Ljava/lang/String;
    :goto_1
    return-void

    .line 80
    :cond_b
    invoke-virtual/range {p1 .. p2}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v6

    .line 81
    .local v6, "fieldIndex":J
    const-wide/16 v14, -0x1

    cmp-long v13, v6, v14

    if-nez v13, :cond_c

    .line 82
    new-instance v13, Ljava/lang/IllegalArgumentException;

    const-string v14, "Field \'%s\' does not exist."

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p2, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v13

    .line 84
    :cond_c
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lio/realm/internal/FieldDescriptor;->fieldType:Lio/realm/RealmFieldType;

    .line 85
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lio/realm/internal/FieldDescriptor;->fieldName:Ljava/lang/String;

    .line 86
    const/4 v13, 0x1

    new-array v13, v13, [J

    const/4 v14, 0x0

    aput-wide v6, v13, v14

    move-object/from16 v0, p0

    iput-object v13, v0, Lio/realm/internal/FieldDescriptor;->columnIndices:[J

    .line 87
    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lio/realm/internal/FieldDescriptor;->searchIndex:Z

    goto :goto_1
.end method


# virtual methods
.method public getColumnIndices()[J
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lio/realm/internal/FieldDescriptor;->columnIndices:[J

    iget-object v1, p0, Lio/realm/internal/FieldDescriptor;->columnIndices:[J

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    return-object v0
.end method

.method public getFieldName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/internal/FieldDescriptor;->fieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getFieldType()Lio/realm/RealmFieldType;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lio/realm/internal/FieldDescriptor;->fieldType:Lio/realm/RealmFieldType;

    return-object v0
.end method

.method public hasSearchIndex()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lio/realm/internal/FieldDescriptor;->searchIndex:Z

    return v0
.end method
