.class public Lio/realm/internal/TableQuery;
.super Ljava/lang/Object;
.source "TableQuery.java"

# interfaces
.implements Lio/realm/internal/NativeObject;


# static fields
.field private static final DATE_NULL_ERROR_MESSAGE:Ljava/lang/String; = "Date value in query criteria must not be null."

.field private static final STRING_NULL_ERROR_MESSAGE:Ljava/lang/String; = "String value in query criteria must not be null."

.field private static final nativeFinalizerPtr:J


# instance fields
.field protected DEBUG:Z

.field private final context:Lio/realm/internal/Context;

.field protected nativePtr:J

.field private queryValidated:Z

.field protected final table:Lio/realm/internal/Table;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    invoke-static {}, Lio/realm/internal/TableQuery;->nativeGetFinalizerPtr()J

    move-result-wide v0

    sput-wide v0, Lio/realm/internal/TableQuery;->nativeFinalizerPtr:J

    return-void
.end method

.method public constructor <init>(Lio/realm/internal/Context;Lio/realm/internal/Table;J)V
    .locals 3
    .param p1, "context"    # Lio/realm/internal/Context;
    .param p2, "table"    # Lio/realm/internal/Table;
    .param p3, "nativeQueryPtr"    # J

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 40
    iget-boolean v0, p0, Lio/realm/internal/TableQuery;->DEBUG:Z

    if-eqz v0, :cond_0

    .line 41
    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "++++++ new TableQuery, ptr= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    .line 43
    :cond_0
    iput-object p1, p0, Lio/realm/internal/TableQuery;->context:Lio/realm/internal/Context;

    .line 44
    iput-object p2, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    .line 45
    iput-wide p3, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    .line 46
    invoke-virtual {p1, p0}, Lio/realm/internal/Context;->addReference(Lio/realm/internal/NativeObject;)V

    .line 47
    return-void
.end method

.method public static getNativeSortOrderValues([Lio/realm/Sort;)[Z
    .locals 3
    .param p0, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 632
    array-length v2, p0

    new-array v1, v2, [Z

    .line 633
    .local v1, "nativeValues":[Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 634
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lio/realm/Sort;->getValue()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 633
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 636
    :cond_0
    return-object v1
.end method

.method public static importHandoverRow(JLio/realm/internal/SharedRealm;)J
    .locals 2
    .param p0, "handoverRowPtr"    # J
    .param p2, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    .line 429
    invoke-virtual {p2}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    invoke-static {p0, p1, v0, v1}, Lio/realm/internal/TableQuery;->nativeImportHandoverRowIntoSharedGroup(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private native nativeAverageDouble(JJJJJ)D
.end method

.method private native nativeAverageFloat(JJJJJ)D
.end method

.method private native nativeAverageInt(JJJJJ)D
.end method

.method private native nativeBeginsWith(J[JLjava/lang/String;Z)V
.end method

.method private native nativeBetween(J[JDD)V
.end method

.method private native nativeBetween(J[JFF)V
.end method

.method private native nativeBetween(J[JJJ)V
.end method

.method private native nativeBetweenTimestamp(J[JJJ)V
.end method

.method private native nativeContains(J[JLjava/lang/String;Z)V
.end method

.method private native nativeCount(JJJJ)J
.end method

.method private native nativeEndGroup(J)V
.end method

.method private native nativeEndsWith(J[JLjava/lang/String;Z)V
.end method

.method private native nativeEqual(J[JD)V
.end method

.method private native nativeEqual(J[JF)V
.end method

.method private native nativeEqual(J[JJ)V
.end method

.method private native nativeEqual(J[JLjava/lang/String;Z)V
.end method

.method private native nativeEqual(J[JZ)V
.end method

.method private native nativeEqual(J[J[B)V
.end method

.method private native nativeEqualTimestamp(J[JJ)V
.end method

.method private native nativeFind(JJ)J
.end method

.method private native nativeFindAll(JJJJ)J
.end method

.method private static native nativeGetFinalizerPtr()J
.end method

.method private native nativeGreater(J[JD)V
.end method

.method private native nativeGreater(J[JF)V
.end method

.method private native nativeGreater(J[JJ)V
.end method

.method private native nativeGreaterEqual(J[JD)V
.end method

.method private native nativeGreaterEqual(J[JF)V
.end method

.method private native nativeGreaterEqual(J[JJ)V
.end method

.method private native nativeGreaterEqualTimestamp(J[JJ)V
.end method

.method private native nativeGreaterTimestamp(J[JJ)V
.end method

.method private native nativeGroup(J)V
.end method

.method private native nativeHandoverQuery(JJ)J
.end method

.method private static native nativeImportHandoverRowIntoSharedGroup(JJ)J
.end method

.method private native nativeIsEmpty(J[J)V
.end method

.method private native nativeIsNotNull(J[J)V
.end method

.method private native nativeIsNull(J[J)V
.end method

.method private native nativeLess(J[JD)V
.end method

.method private native nativeLess(J[JF)V
.end method

.method private native nativeLess(J[JJ)V
.end method

.method private native nativeLessEqual(J[JD)V
.end method

.method private native nativeLessEqual(J[JF)V
.end method

.method private native nativeLessEqual(J[JJ)V
.end method

.method private native nativeLessEqualTimestamp(J[JJ)V
.end method

.method private native nativeLessTimestamp(J[JJ)V
.end method

.method private native nativeLike(J[JLjava/lang/String;Z)V
.end method

.method private native nativeMaximumDouble(JJJJJ)Ljava/lang/Double;
.end method

.method private native nativeMaximumFloat(JJJJJ)Ljava/lang/Float;
.end method

.method private native nativeMaximumInt(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumDouble(JJJJJ)Ljava/lang/Double;
.end method

.method private native nativeMinimumFloat(JJJJJ)Ljava/lang/Float;
.end method

.method private native nativeMinimumInt(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;
.end method

.method private native nativeNot(J)V
.end method

.method private native nativeNotEqual(J[JD)V
.end method

.method private native nativeNotEqual(J[JF)V
.end method

.method private native nativeNotEqual(J[JJ)V
.end method

.method private native nativeNotEqual(J[JLjava/lang/String;Z)V
.end method

.method private native nativeNotEqual(J[J[B)V
.end method

.method private native nativeNotEqualTimestamp(J[JJ)V
.end method

.method private native nativeOr(J)V
.end method

.method private native nativeRemove(J)J
.end method

.method private native nativeSumDouble(JJJJJ)D
.end method

.method private native nativeSumFloat(JJJJJ)D
.end method

.method private native nativeSumInt(JJJJJ)J
.end method

.method private native nativeValidateQuery(J)Ljava/lang/String;
.end method

.method private throwImmutable()V
    .locals 2

    .prologue
    .line 640
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Mutable method call during read transaction."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public averageDouble(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 556
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 557
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageDouble(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 552
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 553
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 518
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 519
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageFloat(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 514
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 515
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageInt(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 480
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 481
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageInt(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public averageInt(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 476
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 477
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeAverageInt(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public beginsWith([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 354
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBeginsWith(J[JLjava/lang/String;Z)V

    .line 355
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 356
    return-object p0
.end method

.method public beginsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 348
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBeginsWith(J[JLjava/lang/String;Z)V

    .line 349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 350
    return-object p0
.end method

.method public between([JDD)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # D
    .param p4, "value2"    # D

    .prologue
    .line 229
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetween(J[JDD)V

    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 231
    return-object p0
.end method

.method public between([JFF)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # F
    .param p3, "value2"    # F

    .prologue
    .line 185
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeBetween(J[JFF)V

    .line 186
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 187
    return-object p0
.end method

.method public between([JJJ)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # J
    .param p4, "value2"    # J

    .prologue
    .line 141
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetween(J[JJJ)V

    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 143
    return-object p0
.end method

.method public between([JLjava/util/Date;Ljava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 8
    .param p1, "columnIndex"    # [J
    .param p2, "value1"    # Ljava/util/Date;
    .param p3, "value2"    # Ljava/util/Date;

    .prologue
    .line 297
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 298
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date values in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 299
    :cond_1
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-virtual {p3}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v7}, Lio/realm/internal/TableQuery;->nativeBetweenTimestamp(J[JJJ)V

    .line 300
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 301
    return-object p0
.end method

.method public contains([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 390
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeContains(J[JLjava/lang/String;Z)V

    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 392
    return-object p0
.end method

.method public contains([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 384
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeContains(J[JLjava/lang/String;Z)V

    .line 385
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 386
    return-object p0
.end method

.method public count()J
    .locals 10

    .prologue
    const-wide/16 v6, -0x1

    .line 618
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 619
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move-wide v8, v6

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeCount(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public count(JJJ)J
    .locals 11
    .param p1, "start"    # J
    .param p3, "end"    # J
    .param p5, "limit"    # J

    .prologue
    .line 613
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 614
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide v6, p3

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lio/realm/internal/TableQuery;->nativeCount(JJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public endGroup()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 85
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeEndGroup(J)V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 87
    return-object p0
.end method

.method public endsWith([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 366
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEndsWith(J[JLjava/lang/String;Z)V

    .line 367
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 368
    return-object p0
.end method

.method public endsWith([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 360
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEndsWith(J[JLjava/lang/String;Z)V

    .line 361
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 362
    return-object p0
.end method

.method public equalTo([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 193
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqual(J[JD)V

    .line 194
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 195
    return-object p0
.end method

.method public equalTo([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 149
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeEqual(J[JF)V

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 151
    return-object p0
.end method

.method public equalTo([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # J

    .prologue
    .line 105
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqual(J[JJ)V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 107
    return-object p0
.end method

.method public equalTo([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 330
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEqual(J[JLjava/lang/String;Z)V

    .line 331
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 332
    return-object p0
.end method

.method public equalTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndexes"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 324
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeEqual(J[JLjava/lang/String;Z)V

    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 326
    return-object p0
.end method

.method public equalTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 247
    if-nez p2, :cond_0

    .line 248
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNull(J[J)V

    .line 252
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 253
    return-object p0

    .line 250
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeEqualTimestamp(J[JJ)V

    goto :goto_0
.end method

.method public equalTo([JZ)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Z

    .prologue
    .line 237
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeEqual(J[JZ)V

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 239
    return-object p0
.end method

.method public equalTo([J[B)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # [B

    .prologue
    .line 307
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeEqual(J[J[B)V

    .line 308
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 309
    return-object p0
.end method

.method public find()J
    .locals 4

    .prologue
    .line 417
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 418
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v2, 0x0

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableQuery;->nativeFind(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public find(J)J
    .locals 3
    .param p1, "fromTableRow"    # J
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 409
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 410
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeFind(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNativeFinalizerPtr()J
    .locals 2

    .prologue
    .line 56
    sget-wide v0, Lio/realm/internal/TableQuery;->nativeFinalizerPtr:J

    return-wide v0
.end method

.method public getNativePtr()J
    .locals 2

    .prologue
    .line 51
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    return-wide v0
.end method

.method public getTable()Lio/realm/internal/Table;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    return-object v0
.end method

.method public greaterThan([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 205
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreater(J[JD)V

    .line 206
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 207
    return-object p0
.end method

.method public greaterThan([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 161
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeGreater(J[JF)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 163
    return-object p0
.end method

.method public greaterThan([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 117
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreater(J[JJ)V

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 119
    return-object p0
.end method

.method public greaterThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 265
    if-nez p2, :cond_0

    .line 266
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterTimestamp(J[JJ)V

    .line 268
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 269
    return-object p0
.end method

.method public greaterThanOrEqual([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 211
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JD)V

    .line 212
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 213
    return-object p0
.end method

.method public greaterThanOrEqual([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 167
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JF)V

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 169
    return-object p0
.end method

.method public greaterThanOrEqual([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 123
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqual(J[JJ)V

    .line 124
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 125
    return-object p0
.end method

.method public greaterThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 273
    if-nez p2, :cond_0

    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 275
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeGreaterEqualTimestamp(J[JJ)V

    .line 276
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 277
    return-object p0
.end method

.method public group()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 79
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeGroup(J)V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 81
    return-object p0
.end method

.method public handoverQuery(Lio/realm/internal/SharedRealm;)J
    .locals 4
    .param p1, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    .line 439
    invoke-virtual {p1}, Lio/realm/internal/SharedRealm;->getNativePtr()J

    move-result-wide v0

    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, v2, v3}, Lio/realm/internal/TableQuery;->nativeHandoverQuery(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public isEmpty([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 396
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsEmpty(J[J)V

    .line 397
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 398
    return-object p0
.end method

.method public isNotEmpty([J)Lio/realm/internal/TableQuery;
    .locals 1
    .param p1, "columnIndices"    # [J

    .prologue
    .line 402
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->not()Lio/realm/internal/TableQuery;

    move-result-object v0

    invoke-virtual {v0, p1}, Lio/realm/internal/TableQuery;->isEmpty([J)Lio/realm/internal/TableQuery;

    move-result-object v0

    return-object v0
.end method

.method public isNotNull([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 604
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNotNull(J[J)V

    .line 605
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 606
    return-object p0
.end method

.method public isNull([J)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J

    .prologue
    .line 598
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1}, Lio/realm/internal/TableQuery;->nativeIsNull(J[J)V

    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 600
    return-object p0
.end method

.method public lessThan([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 217
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLess(J[JD)V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 219
    return-object p0
.end method

.method public lessThan([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 173
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeLess(J[JF)V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 175
    return-object p0
.end method

.method public lessThan([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 129
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLess(J[JJ)V

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 131
    return-object p0
.end method

.method public lessThan([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 281
    if-nez p2, :cond_0

    .line 282
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 283
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessTimestamp(J[JJ)V

    .line 284
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 285
    return-object p0
.end method

.method public lessThanOrEqual([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 223
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JD)V

    .line 224
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 225
    return-object p0
.end method

.method public lessThanOrEqual([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 179
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JF)V

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 181
    return-object p0
.end method

.method public lessThanOrEqual([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 135
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqual(J[JJ)V

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 137
    return-object p0
.end method

.method public lessThanOrEqual([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 289
    if-nez p2, :cond_0

    .line 290
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeLessEqualTimestamp(J[JJ)V

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 293
    return-object p0
.end method

.method public like([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 378
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeLike(J[JLjava/lang/String;Z)V

    .line 379
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 380
    return-object p0
.end method

.method public like([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 372
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeLike(J[JLjava/lang/String;Z)V

    .line 373
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 374
    return-object p0
.end method

.method public maximumDate(J)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 571
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 572
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 573
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 574
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 576
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public maximumDate(JJJJ)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 563
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 564
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 565
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 566
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 568
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public maximumDouble(J)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 538
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 539
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumDouble(JJJJ)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 534
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 535
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(J)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 500
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 501
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumFloat(JJJJ)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 496
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 497
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public maximumInt(J)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 462
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 463
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public maximumInt(JJJJ)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 458
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 459
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMaximumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumDate(J)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 588
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 589
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 590
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 591
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 593
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public minimumDate(JJJJ)Ljava/util/Date;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 580
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 581
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumTimestamp(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    .line 582
    .local v0, "result":Ljava/lang/Long;
    if-eqz v0, :cond_0

    .line 583
    new-instance v1, Ljava/util/Date;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 585
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public minimumDouble(J)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 547
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 548
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumDouble(JJJJ)Ljava/lang/Double;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 543
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 544
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumDouble(JJJJJ)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(J)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 509
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 510
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumFloat(JJJJ)Ljava/lang/Float;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 505
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 506
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumFloat(JJJJJ)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public minimumInt(J)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 471
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 472
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public minimumInt(JJJJ)Ljava/lang/Long;
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 467
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 468
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeMinimumInt(JJJJJ)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public not()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeNot(J)V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 99
    return-object p0
.end method

.method public notEqualTo([JD)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # D

    .prologue
    .line 199
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JD)V

    .line 200
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 201
    return-object p0
.end method

.method public notEqualTo([JF)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # F

    .prologue
    .line 155
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JF)V

    .line 156
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 157
    return-object p0
.end method

.method public notEqualTo([JJ)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # J

    .prologue
    .line 111
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v0, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JJ)V

    .line 112
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 113
    return-object p0
.end method

.method public notEqualTo([JLjava/lang/String;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 342
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const/4 v6, 0x1

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JLjava/lang/String;Z)V

    .line 343
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 344
    return-object p0
.end method

.method public notEqualTo([JLjava/lang/String;Lio/realm/Case;)Lio/realm/internal/TableQuery;
    .locals 7
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/lang/String;
    .param p3, "caseSensitive"    # Lio/realm/Case;

    .prologue
    .line 337
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p3}, Lio/realm/Case;->getValue()Z

    move-result v6

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v1 .. v6}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[JLjava/lang/String;Z)V

    .line 338
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 339
    return-object p0
.end method

.method public notEqualTo([JLjava/util/Date;)Lio/realm/internal/TableQuery;
    .locals 6
    .param p1, "columnIndex"    # [J
    .param p2, "value"    # Ljava/util/Date;

    .prologue
    .line 257
    if-nez p2, :cond_0

    .line 258
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Date value in query criteria must not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_0
    iget-wide v1, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-virtual {p2}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    move-object v0, p0

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lio/realm/internal/TableQuery;->nativeNotEqualTimestamp(J[JJ)V

    .line 260
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 261
    return-object p0
.end method

.method public notEqualTo([J[B)Lio/realm/internal/TableQuery;
    .locals 2
    .param p1, "columnIndices"    # [J
    .param p2, "value"    # [B

    .prologue
    .line 313
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1, p1, p2}, Lio/realm/internal/TableQuery;->nativeNotEqual(J[J[B)V

    .line 314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 315
    return-object p0
.end method

.method public or()Lio/realm/internal/TableQuery;
    .locals 2

    .prologue
    .line 91
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeOr(J)V

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 93
    return-object p0
.end method

.method public remove()J
    .locals 2

    .prologue
    .line 623
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 624
    iget-object v0, p0, Lio/realm/internal/TableQuery;->table:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->isImmutable()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lio/realm/internal/TableQuery;->throwImmutable()V

    .line 625
    :cond_0
    iget-wide v0, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v0, v1}, Lio/realm/internal/TableQuery;->nativeRemove(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public sumDouble(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 529
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 530
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumDouble(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 525
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 526
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumDouble(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(J)D
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 491
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 492
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumFloat(JJJJ)D
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 487
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 488
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumFloat(JJJJJ)D

    move-result-wide v0

    return-wide v0
.end method

.method public sumInt(J)J
    .locals 13
    .param p1, "columnIndex"    # J

    .prologue
    const-wide/16 v8, -0x1

    .line 453
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 454
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    const-wide/16 v6, 0x0

    move-object v1, p0

    move-wide v4, p1

    move-wide v10, v8

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumInt(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public sumInt(JJJJ)J
    .locals 13
    .param p1, "columnIndex"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J
    .param p7, "limit"    # J

    .prologue
    .line 449
    invoke-virtual {p0}, Lio/realm/internal/TableQuery;->validateQuery()V

    .line 450
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    invoke-direct/range {v1 .. v11}, Lio/realm/internal/TableQuery;->nativeSumInt(JJJJJ)J

    move-result-wide v0

    return-wide v0
.end method

.method validateQuery()V
    .locals 4

    .prologue
    .line 67
    iget-boolean v1, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    if-nez v1, :cond_0

    .line 68
    iget-wide v2, p0, Lio/realm/internal/TableQuery;->nativePtr:J

    invoke-direct {p0, v2, v3}, Lio/realm/internal/TableQuery;->nativeValidateQuery(J)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "invalidMessage":Ljava/lang/String;
    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x1

    iput-boolean v1, p0, Lio/realm/internal/TableQuery;->queryValidated:Z

    .line 74
    .end local v0    # "invalidMessage":Ljava/lang/String;
    :cond_0
    return-void

    .line 72
    .restart local v0    # "invalidMessage":Ljava/lang/String;
    :cond_1
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
