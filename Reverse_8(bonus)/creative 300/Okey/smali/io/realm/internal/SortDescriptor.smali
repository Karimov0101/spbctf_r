.class public Lio/realm/internal/SortDescriptor;
.super Ljava/lang/Object;
.source "SortDescriptor.java"


# annotations
.annotation build Lio/realm/internal/KeepMember;
.end annotation


# static fields
.field static final validFieldTypesForDistinct:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;"
        }
    .end annotation
.end field

.field static final validFieldTypesForSort:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lio/realm/RealmFieldType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final ascendings:[Z

.field private final columnIndices:[[J

.field private final table:Lio/realm/internal/Table;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    const/4 v0, 0x6

    new-array v0, v0, [Lio/realm/RealmFieldType;

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/RealmFieldType;->FLOAT:Lio/realm/RealmFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/RealmFieldType;->DOUBLE:Lio/realm/RealmFieldType;

    aput-object v1, v0, v6

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/internal/SortDescriptor;->validFieldTypesForSort:Ljava/util/List;

    .line 43
    new-array v0, v7, [Lio/realm/RealmFieldType;

    sget-object v1, Lio/realm/RealmFieldType;->BOOLEAN:Lio/realm/RealmFieldType;

    aput-object v1, v0, v3

    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    aput-object v1, v0, v4

    sget-object v1, Lio/realm/RealmFieldType;->STRING:Lio/realm/RealmFieldType;

    aput-object v1, v0, v5

    sget-object v1, Lio/realm/RealmFieldType;->DATE:Lio/realm/RealmFieldType;

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lio/realm/internal/SortDescriptor;->validFieldTypesForDistinct:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Lio/realm/internal/Table;[J)V
    .locals 2
    .param p1, "table"    # Lio/realm/internal/Table;
    .param p2, "columnIndices"    # [J

    .prologue
    .line 48
    const/4 v0, 0x1

    new-array v0, v0, [[J

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lio/realm/internal/SortDescriptor;-><init>(Lio/realm/internal/Table;[[J[Lio/realm/Sort;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Lio/realm/internal/Table;[[J[Lio/realm/Sort;)V
    .locals 3
    .param p1, "table"    # Lio/realm/internal/Table;
    .param p2, "columnIndices"    # [[J
    .param p3, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    if-eqz p3, :cond_0

    .line 53
    array-length v1, p3

    new-array v1, v1, [Z

    iput-object v1, p0, Lio/realm/internal/SortDescriptor;->ascendings:[Z

    .line 54
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_1

    .line 55
    iget-object v1, p0, Lio/realm/internal/SortDescriptor;->ascendings:[Z

    aget-object v2, p3, v0

    invoke-virtual {v2}, Lio/realm/Sort;->getValue()Z

    move-result v2

    aput-boolean v2, v1, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    .end local v0    # "i":I
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lio/realm/internal/SortDescriptor;->ascendings:[Z

    .line 61
    :cond_1
    iput-object p2, p0, Lio/realm/internal/SortDescriptor;->columnIndices:[[J

    .line 62
    iput-object p1, p0, Lio/realm/internal/SortDescriptor;->table:Lio/realm/internal/Table;

    .line 63
    return-void
.end method

.method private static checkFieldTypeForDistinct(Lio/realm/internal/FieldDescriptor;Ljava/lang/String;)V
    .locals 7
    .param p0, "descriptor"    # Lio/realm/internal/FieldDescriptor;
    .param p1, "fieldDescriptions"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    sget-object v0, Lio/realm/internal/SortDescriptor;->validFieldTypesForDistinct:Ljava/util/List;

    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldType()Lio/realm/RealmFieldType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Distinct is not supported on \'%s\' field \'%s\' in \'%s\'."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    .line 121
    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldType()Lio/realm/RealmFieldType;

    move-result-object v3

    invoke-virtual {v3}, Lio/realm/RealmFieldType;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    aput-object p1, v2, v6

    .line 119
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->hasSearchIndex()Z

    move-result v0

    if-nez v0, :cond_1

    .line 124
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Field \'%s\' in \'%s\' must be indexed in order to use it for distinct queries."

    new-array v2, v6, [Ljava/lang/Object;

    .line 126
    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    .line 124
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_1
    return-void
.end method

.method private static checkFieldTypeForSort(Lio/realm/internal/FieldDescriptor;Ljava/lang/String;)V
    .locals 5
    .param p0, "descriptor"    # Lio/realm/internal/FieldDescriptor;
    .param p1, "fieldDescriptions"    # Ljava/lang/String;

    .prologue
    .line 110
    sget-object v0, Lio/realm/internal/SortDescriptor;->validFieldTypesForSort:Ljava/util/List;

    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldType()Lio/realm/RealmFieldType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Sort is not supported on \'%s\' field \'%s\' in \'%s\'."

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 112
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lio/realm/internal/FieldDescriptor;->getFieldName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p1, v2, v3

    .line 111
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    return-void
.end method

.method public static getInstanceForDistinct(Lio/realm/internal/Table;Ljava/lang/String;)Lio/realm/internal/SortDescriptor;
    .locals 2
    .param p0, "table"    # Lio/realm/internal/Table;
    .param p1, "fieldDescription"    # Ljava/lang/String;

    .prologue
    .line 91
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-static {p0, v0}, Lio/realm/internal/SortDescriptor;->getInstanceForDistinct(Lio/realm/internal/Table;[Ljava/lang/String;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static getInstanceForDistinct(Lio/realm/internal/Table;[Ljava/lang/String;)Lio/realm/internal/SortDescriptor;
    .locals 5
    .param p0, "table"    # Lio/realm/internal/Table;
    .param p1, "fieldDescriptions"    # [Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 95
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 96
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "You must provide at least one field name."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 99
    :cond_1
    array-length v3, p1

    new-array v0, v3, [[J

    .line 100
    .local v0, "columnIndices":[[J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    .line 101
    new-instance v1, Lio/realm/internal/FieldDescriptor;

    aget-object v3, p1, v2

    invoke-direct {v1, p0, v3, v4, v4}, Lio/realm/internal/FieldDescriptor;-><init>(Lio/realm/internal/Table;Ljava/lang/String;ZZ)V

    .line 102
    .local v1, "descriptor":Lio/realm/internal/FieldDescriptor;
    aget-object v3, p1, v2

    invoke-static {v1, v3}, Lio/realm/internal/SortDescriptor;->checkFieldTypeForDistinct(Lio/realm/internal/FieldDescriptor;Ljava/lang/String;)V

    .line 103
    invoke-virtual {v1}, Lio/realm/internal/FieldDescriptor;->getColumnIndices()[J

    move-result-object v3

    aput-object v3, v0, v2

    .line 100
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 106
    .end local v1    # "descriptor":Lio/realm/internal/FieldDescriptor;
    :cond_2
    new-instance v3, Lio/realm/internal/SortDescriptor;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v0, v4}, Lio/realm/internal/SortDescriptor;-><init>(Lio/realm/internal/Table;[[J[Lio/realm/Sort;)V

    return-object v3
.end method

.method public static getInstanceForSort(Lio/realm/internal/Table;Ljava/lang/String;Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;
    .locals 3
    .param p0, "table"    # Lio/realm/internal/Table;
    .param p1, "fieldDescription"    # Ljava/lang/String;
    .param p2, "sortOrder"    # Lio/realm/Sort;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    new-array v0, v1, [Ljava/lang/String;

    aput-object p1, v0, v2

    new-array v1, v1, [Lio/realm/Sort;

    aput-object p2, v1, v2

    invoke-static {p0, v0, v1}, Lio/realm/internal/SortDescriptor;->getInstanceForSort(Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public static getInstanceForSort(Lio/realm/internal/Table;[Ljava/lang/String;[Lio/realm/Sort;)Lio/realm/internal/SortDescriptor;
    .locals 6
    .param p0, "table"    # Lio/realm/internal/Table;
    .param p1, "fieldDescriptions"    # [Ljava/lang/String;
    .param p2, "sortOrders"    # [Lio/realm/Sort;

    .prologue
    .line 70
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 71
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "You must provide at least one field name."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 73
    :cond_1
    if-eqz p2, :cond_2

    array-length v3, p2

    if-nez v3, :cond_3

    .line 74
    :cond_2
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "You must provide at least one sort order."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 76
    :cond_3
    array-length v3, p1

    array-length v4, p2

    if-eq v3, v4, :cond_4

    .line 77
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Number of fields and sort orders do not match."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 80
    :cond_4
    array-length v3, p1

    new-array v0, v3, [[J

    .line 81
    .local v0, "columnIndices":[[J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_5

    .line 82
    new-instance v1, Lio/realm/internal/FieldDescriptor;

    aget-object v3, p1, v2

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct {v1, p0, v3, v4, v5}, Lio/realm/internal/FieldDescriptor;-><init>(Lio/realm/internal/Table;Ljava/lang/String;ZZ)V

    .line 83
    .local v1, "descriptor":Lio/realm/internal/FieldDescriptor;
    aget-object v3, p1, v2

    invoke-static {v1, v3}, Lio/realm/internal/SortDescriptor;->checkFieldTypeForSort(Lio/realm/internal/FieldDescriptor;Ljava/lang/String;)V

    .line 84
    invoke-virtual {v1}, Lio/realm/internal/FieldDescriptor;->getColumnIndices()[J

    move-result-object v3

    aput-object v3, v0, v2

    .line 81
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 87
    .end local v1    # "descriptor":Lio/realm/internal/FieldDescriptor;
    :cond_5
    new-instance v3, Lio/realm/internal/SortDescriptor;

    invoke-direct {v3, p0, v0, p2}, Lio/realm/internal/SortDescriptor;-><init>(Lio/realm/internal/Table;[[J[Lio/realm/Sort;)V

    return-object v3
.end method

.method private getTablePtr()J
    .locals 2
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->table:Lio/realm/internal/Table;

    invoke-virtual {v0}, Lio/realm/internal/Table;->getNativePtr()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method getAscendings()[Z
    .locals 1
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 139
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->ascendings:[Z

    return-object v0
.end method

.method getColumnIndices()[[J
    .locals 1
    .annotation build Lio/realm/internal/KeepMember;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lio/realm/internal/SortDescriptor;->columnIndices:[[J

    return-object v0
.end method
