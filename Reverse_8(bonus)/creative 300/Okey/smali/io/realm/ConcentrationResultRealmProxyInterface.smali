.class public interface abstract Lio/realm/ConcentrationResultRealmProxyInterface;
.super Ljava/lang/Object;
.source "ConcentrationResultRealmProxyInterface.java"


# virtual methods
.method public abstract realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
.end method

.method public abstract realmGet$id()I
.end method

.method public abstract realmGet$score()I
.end method

.method public abstract realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V
.end method

.method public abstract realmSet$id(I)V
.end method

.method public abstract realmSet$score(I)V
.end method
