.class final Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "ConcentrationConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/ConcentrationConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ConcentrationConfigColumnInfo"
.end annotation


# instance fields
.field public circlesCountCustomIndex:J

.field public circlesCountIndex:J

.field public circlesRadiusIndex:J

.field public circlesSizeCustomIndex:J

.field public circlesSpeedCustomIndex:J

.field public circlesSpeedIndex:J

.field public complexityIndex:J

.field public grayTimeIndex:J

.field public idIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 47
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 48
    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 49
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "ConcentrationConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->idIndex:J

    .line 50
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "ConcentrationConfig"

    const-string v2, "grayTime"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->grayTimeIndex:J

    .line 52
    const-string v1, "grayTime"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->grayTimeIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "ConcentrationConfig"

    const-string v2, "complexity"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->complexityIndex:J

    .line 54
    const-string v1, "complexity"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->complexityIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountIndex:J

    .line 56
    const-string v1, "circlesCount"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesRadius"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesRadiusIndex:J

    .line 58
    const-string v1, "circlesRadius"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesRadiusIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesSpeed"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedIndex:J

    .line 60
    const-string v1, "circlesSpeed"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesSpeedCustom"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedCustomIndex:J

    .line 62
    const-string v1, "circlesSpeedCustom"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedCustomIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesSizeCustom"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSizeCustomIndex:J

    .line 64
    const-string v1, "circlesSizeCustom"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSizeCustomIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    const-string v1, "ConcentrationConfig"

    const-string v2, "circlesCountCustom"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountCustomIndex:J

    .line 66
    const-string v1, "circlesCountCustom"

    iget-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountCustomIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    invoke-virtual {p0, v0}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 69
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;
    .locals 1

    .prologue
    .line 89
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->clone()Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->clone()Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 73
    move-object v0, p1

    check-cast v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;

    .line 74
    .local v0, "otherInfo":Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->idIndex:J

    .line 75
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->grayTimeIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->grayTimeIndex:J

    .line 76
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->complexityIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->complexityIndex:J

    .line 77
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountIndex:J

    .line 78
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesRadiusIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesRadiusIndex:J

    .line 79
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedIndex:J

    .line 80
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedCustomIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSpeedCustomIndex:J

    .line 81
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSizeCustomIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesSizeCustomIndex:J

    .line 82
    iget-wide v2, v0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountCustomIndex:J

    iput-wide v2, p0, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->circlesCountCustomIndex:J

    .line 84
    invoke-virtual {v0}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 85
    return-void
.end method
