.class public Lio/realm/RememberNumberConfigRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
.source "RememberNumberConfigRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/RememberNumberConfigRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 90
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    const-string v1, "trainingShowCount"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    const-string v1, "minComplexity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    const-string v1, "maxComplexity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    const-string v1, "complexity"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    const-string v1, "answersToComplexityUp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    const-string v1, "answersToComplexityDown"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/RememberNumberConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 98
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;-><init>()V

    .line 101
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 102
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;"
        }
    .end annotation

    .prologue
    .line 584
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    .line 585
    .local v0, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v0, :cond_0

    .line 586
    check-cast v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 597
    .end local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :goto_0
    return-object v0

    .line 589
    .restart local v0    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object v2, p1

    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual {p0, v3, v2, v4, v5}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v1

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .local v1, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move-object v2, v1

    .line 590
    check-cast v2, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v1

    .line 591
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$trainingShowCount(I)V

    move-object v2, v1

    .line 592
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$minComplexity(I)V

    move-object v2, v1

    .line 593
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$maxComplexity(I)V

    move-object v2, v1

    .line 594
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v2, v1

    .line 595
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p1

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityUp(I)V

    move-object v2, v1

    .line 596
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    check-cast p1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface {p1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityDown(I)V

    move-object v0, v1

    .line 597
    goto :goto_0
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;"
        }
    .end annotation

    .prologue
    .line 545
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 546
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 548
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 578
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :goto_0
    return-object p1

    .line 551
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 552
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 553
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 554
    check-cast v10, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 556
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 557
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move/from16 v11, p2

    .line 558
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 559
    const-class v5, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 560
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 561
    check-cast v5, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 562
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 564
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 565
    new-instance v15, Lio/realm/RememberNumberConfigRealmProxy;

    invoke-direct {v15}, Lio/realm/RememberNumberConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 566
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 568
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 575
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 576
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object p1

    goto :goto_0

    .line 568
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 571
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 578
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/RememberNumberConfigRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object p1

    goto/16 :goto_0

    .line 568
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 4
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;"
        }
    .end annotation

    .prologue
    .line 722
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 723
    :cond_0
    const/4 v2, 0x0

    .line 746
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :goto_0
    return-object v2

    .line 725
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 727
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 729
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 730
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    goto :goto_0

    .line 732
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 733
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 739
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 740
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$trainingShowCount(I)V

    move-object v2, v1

    .line 741
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$minComplexity(I)V

    move-object v2, v1

    .line 742
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$maxComplexity(I)V

    move-object v2, v1

    .line 743
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v2, v1

    .line 744
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityUp(I)V

    move-object v2, v1

    .line 745
    check-cast v2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    check-cast p0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface {p0}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityDown(I)V

    move-object v2, v1

    .line 746
    goto :goto_0

    .line 736
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;-><init>()V

    .line 737
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 13
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 398
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v6

    .line 399
    .local v6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .line 400
    .local v7, "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    if-eqz p2, :cond_1

    .line 401
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p0, v1}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v12

    .line 402
    .local v12, "table":Lio/realm/internal/Table;
    invoke-virtual {v12}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    .line 403
    .local v8, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .line 404
    .local v10, "rowIndex":J
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 405
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v12, v8, v9, v2, v3}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v10

    .line 407
    :cond_0
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-eqz v1, :cond_1

    .line 408
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 410
    .local v0, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v12, v10, v11}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v2

    iget-object v1, p0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v3, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1, v3}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 411
    new-instance v7, Lio/realm/RememberNumberConfigRealmProxy;

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-direct {v7}, Lio/realm/RememberNumberConfigRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 417
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    :cond_1
    if-nez v7, :cond_2

    .line 418
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 419
    const-string v1, "id"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 420
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast v7, Lio/realm/RememberNumberConfigRealmProxy;

    .line 428
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_2
    :goto_0
    const-string v1, "trainingShowCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 429
    const-string v1, "trainingShowCount"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 430
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'trainingShowCount\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 413
    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .restart local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v8    # "pkColumnIndex":J
    .restart local v10    # "rowIndex":J
    .restart local v12    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v1

    .line 422
    .end local v0    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v8    # "pkColumnIndex":J
    .end local v10    # "rowIndex":J
    .end local v12    # "table":Lio/realm/internal/Table;
    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    const-string v2, "id"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v1, v2, v3, v6}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v7

    .end local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast v7, Lio/realm/RememberNumberConfigRealmProxy;

    .restart local v7    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    goto :goto_0

    .line 425
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-object v1, v7

    .line 432
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "trainingShowCount"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$trainingShowCount(I)V

    .line 435
    :cond_6
    const-string v1, "minComplexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 436
    const-string v1, "minComplexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 437
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'minComplexity\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    move-object v1, v7

    .line 439
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "minComplexity"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$minComplexity(I)V

    .line 442
    :cond_8
    const-string v1, "maxComplexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 443
    const-string v1, "maxComplexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 444
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'maxComplexity\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, v7

    .line 446
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "maxComplexity"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$maxComplexity(I)V

    .line 449
    :cond_a
    const-string v1, "complexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 450
    const-string v1, "complexity"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 451
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'complexity\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    move-object v1, v7

    .line 453
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "complexity"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$complexity(I)V

    .line 456
    :cond_c
    const-string v1, "answersToComplexityUp"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 457
    const-string v1, "answersToComplexityUp"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 458
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'answersToComplexityUp\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_d
    move-object v1, v7

    .line 460
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "answersToComplexityUp"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityUp(I)V

    .line 463
    :cond_e
    const-string v1, "answersToComplexityDown"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 464
    const-string v1, "answersToComplexityDown"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 465
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Trying to set non-nullable field \'answersToComplexityDown\' to null."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_f
    move-object v1, v7

    .line 467
    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    const-string v2, "answersToComplexityDown"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityDown(I)V

    .line 470
    :cond_10
    return-object v7
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v3, 0x1

    const/4 v7, 0x0

    .line 255
    const-string v0, "RememberNumberConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const-string v0, "RememberNumberConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 257
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 258
    new-instance v4, Lio/realm/Property;

    const-string v5, "trainingShowCount"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 259
    new-instance v4, Lio/realm/Property;

    const-string v5, "minComplexity"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 260
    new-instance v4, Lio/realm/Property;

    const-string v5, "maxComplexity"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 261
    new-instance v4, Lio/realm/Property;

    const-string v5, "complexity"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 262
    new-instance v4, Lio/realm/Property;

    const-string v5, "answersToComplexityUp"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 263
    new-instance v4, Lio/realm/Property;

    const-string v5, "answersToComplexityDown"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 266
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_0
    const-string v0, "RememberNumberConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 5
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 477
    const/4 v0, 0x0

    .line 478
    .local v0, "jsonHasPrimaryKey":Z
    new-instance v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-direct {v2}, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;-><init>()V

    .line 479
    .local v2, "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 480
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 481
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v1

    .line 482
    .local v1, "name":Ljava/lang/String;
    const-string v3, "id"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 483
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_0

    .line 484
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 485
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_0
    move-object v3, v2

    .line 487
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$id(I)V

    .line 489
    const/4 v0, 0x1

    goto :goto_0

    .line 490
    :cond_1
    const-string v3, "trainingShowCount"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 491
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_2

    .line 492
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 493
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'trainingShowCount\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_2
    move-object v3, v2

    .line 495
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$trainingShowCount(I)V

    goto :goto_0

    .line 497
    :cond_3
    const-string v3, "minComplexity"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 498
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_4

    .line 499
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 500
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'minComplexity\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_4
    move-object v3, v2

    .line 502
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$minComplexity(I)V

    goto :goto_0

    .line 504
    :cond_5
    const-string v3, "maxComplexity"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 505
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_6

    .line 506
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 507
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'maxComplexity\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v2

    .line 509
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$maxComplexity(I)V

    goto/16 :goto_0

    .line 511
    :cond_7
    const-string v3, "complexity"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 512
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_8

    .line 513
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 514
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'complexity\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_8
    move-object v3, v2

    .line 516
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$complexity(I)V

    goto/16 :goto_0

    .line 518
    :cond_9
    const-string v3, "answersToComplexityUp"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 519
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_a

    .line 520
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 521
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'answersToComplexityUp\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_a
    move-object v3, v2

    .line 523
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityUp(I)V

    goto/16 :goto_0

    .line 525
    :cond_b
    const-string v3, "answersToComplexityDown"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 526
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v3

    sget-object v4, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v3, v4, :cond_c

    .line 527
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 528
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'answersToComplexityDown\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_c
    move-object v3, v2

    .line 530
    check-cast v3, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityDown(I)V

    goto/16 :goto_0

    .line 533
    :cond_d
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto/16 :goto_0

    .line 536
    .end local v1    # "name":Ljava/lang/String;
    :cond_e
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 537
    if-nez v0, :cond_f

    .line 538
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 540
    :cond_f
    invoke-virtual {p0, v2}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v2

    .end local v2    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 541
    .restart local v2    # "obj":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    return-object v2
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 392
    sget-object v0, Lio/realm/RememberNumberConfigRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 388
    const-string v0, "class_RememberNumberConfig"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 270
    const-string v1, "class_RememberNumberConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 271
    const-string v1, "class_RememberNumberConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 272
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 273
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "trainingShowCount"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 274
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "minComplexity"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 275
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "maxComplexity"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 276
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "complexity"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 277
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "answersToComplexityUp"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 278
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "answersToComplexityDown"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 279
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 280
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 283
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "class_RememberNumberConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 602
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 603
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 626
    :goto_0
    return-wide v10

    .line 605
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 606
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 607
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 608
    .local v15, "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 609
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 610
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 611
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 612
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 614
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 615
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 619
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 620
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 621
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 622
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 623
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 624
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 625
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    check-cast p1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 617
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_2
    invoke-static/range {v16 .. v16}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 630
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 631
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 632
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 633
    .local v15, "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 634
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 635
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 636
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 637
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 638
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 639
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 642
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 643
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 644
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 645
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 647
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 648
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 652
    :goto_1
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 653
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 654
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 655
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 656
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 657
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 658
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 650
    :cond_3
    invoke-static/range {v17 .. v17}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 661
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J
    .locals 18
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 664
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_0

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 665
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    .line 686
    :goto_0
    return-wide v10

    .line 667
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    const-class v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v17

    .line 668
    .local v17, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 669
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 670
    .local v15, "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    invoke-virtual/range {v17 .. v17}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 671
    .local v4, "pkColumnIndex":J
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, p1

    .line 672
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    .line 673
    .local v16, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v16, :cond_1

    move-object/from16 v6, p1

    .line 674
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 676
    :cond_1
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_2

    move-object/from16 v6, p1

    .line 677
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 679
    :cond_2
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 680
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 681
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 682
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 683
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 684
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    move-object/from16 v6, p1

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 685
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    check-cast p1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface/range {p1 .. p1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 690
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v6, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 691
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v2

    .line 692
    .local v2, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v6, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v6, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v15

    check-cast v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 693
    .local v15, "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v4

    .line 694
    .local v4, "pkColumnIndex":J
    const/16 v16, 0x0

    .line 695
    .local v16, "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_4

    .line 696
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    check-cast v16, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .line 697
    .restart local v16    # "object":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 698
    move-object/from16 v0, v16

    instance-of v6, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move-object/from16 v6, v16

    .line 699
    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 702
    :cond_1
    const-wide/16 v10, -0x1

    .local v10, "rowIndex":J
    move-object/from16 v6, v16

    .line 703
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    .line 704
    .local v17, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v17, :cond_2

    move-object/from16 v6, v16

    .line 705
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    int-to-long v6, v6

    invoke-static/range {v2 .. v7}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v10

    .line 707
    :cond_2
    const-wide/16 v6, -0x1

    cmp-long v6, v10, v6

    if-nez v6, :cond_3

    move-object/from16 v6, v16

    .line 708
    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$id()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v10

    .line 710
    :cond_3
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 711
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 712
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 713
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 714
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 715
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 716
    iget-wide v8, v15, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    move-object/from16 v6, v16

    check-cast v6, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v6}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v6

    int-to-long v12, v6

    const/4 v14, 0x0

    move-wide v6, v2

    invoke-static/range {v6 .. v14}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    goto/16 :goto_0

    .line 719
    .end local v10    # "rowIndex":J
    .end local v17    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_4
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .locals 2
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;"
        }
    .end annotation

    .prologue
    .line 750
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v0, p1

    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$trainingShowCount()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$trainingShowCount(I)V

    move-object v0, p1

    .line 751
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$minComplexity()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$minComplexity(I)V

    move-object v0, p1

    .line 752
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$maxComplexity()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$maxComplexity(I)V

    move-object v0, p1

    .line 753
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$complexity()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$complexity(I)V

    move-object v0, p1

    .line 754
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    move-object v1, p2

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxyInterface;

    invoke-interface {v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityUp()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityUp(I)V

    move-object v0, p1

    .line 755
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxyInterface;

    check-cast p2, Lio/realm/RememberNumberConfigRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;
    invoke-interface {p2}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmGet$answersToComplexityDown()I

    move-result v1

    invoke-interface {v0, v1}, Lio/realm/RememberNumberConfigRealmProxyInterface;->realmSet$answersToComplexityDown(I)V

    .line 756
    return-object p1
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    .locals 12
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v8, 0x7

    .line 287
    const-string v7, "class_RememberNumberConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1b

    .line 288
    const-string v7, "class_RememberNumberConfig"

    invoke-virtual {p0, v7}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 289
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 290
    .local v0, "columnCount":J
    cmp-long v7, v0, v8

    if-eqz v7, :cond_1

    .line 291
    cmp-long v7, v0, v8

    if-gez v7, :cond_0

    .line 292
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is less than expected - expected 7 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 294
    :cond_0
    if-eqz p1, :cond_2

    .line 295
    const-string v7, "Field count is more than expected - expected 7 but was %1$d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 300
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 301
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v7, v4, v0

    if-gez v7, :cond_3

    .line 302
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v8

    invoke-interface {v3, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 297
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Field count is more than expected - expected 7 but was "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 305
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7, v6}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 307
    .local v2, "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v7

    if-nez v7, :cond_4

    .line 308
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 310
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    cmp-long v7, v8, v10

    if-eqz v7, :cond_5

    .line 311
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v10

    invoke-virtual {v6, v10, v11}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " to field id"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 315
    :cond_5
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 316
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 318
    :cond_6
    const-string v7, "id"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_7

    .line 319
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 321
    :cond_7
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v7, v8, v10

    if-eqz v7, :cond_8

    .line 322
    new-instance v7, Ljava/lang/IllegalStateException;

    const-string v8, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v7, v8}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 324
    :cond_8
    const-string v7, "id"

    invoke-virtual {v6, v7}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v7

    if-nez v7, :cond_9

    .line 325
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 327
    :cond_9
    const-string v7, "trainingShowCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_a

    .line 328
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'trainingShowCount\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 330
    :cond_a
    const-string v7, "trainingShowCount"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_b

    .line 331
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'trainingShowCount\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 333
    :cond_b
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_c

    .line 334
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'trainingShowCount\' does support null values in the existing Realm file. Use corresponding boxed type for field \'trainingShowCount\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 336
    :cond_c
    const-string v7, "minComplexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_d

    .line 337
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'minComplexity\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 339
    :cond_d
    const-string v7, "minComplexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_e

    .line 340
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'minComplexity\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 342
    :cond_e
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_f

    .line 343
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'minComplexity\' does support null values in the existing Realm file. Use corresponding boxed type for field \'minComplexity\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 345
    :cond_f
    const-string v7, "maxComplexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_10

    .line 346
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'maxComplexity\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 348
    :cond_10
    const-string v7, "maxComplexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_11

    .line 349
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'maxComplexity\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 351
    :cond_11
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_12

    .line 352
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'maxComplexity\' does support null values in the existing Realm file. Use corresponding boxed type for field \'maxComplexity\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 354
    :cond_12
    const-string v7, "complexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_13

    .line 355
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'complexity\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 357
    :cond_13
    const-string v7, "complexity"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_14

    .line 358
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'complexity\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 360
    :cond_14
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_15

    .line 361
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'complexity\' does support null values in the existing Realm file. Use corresponding boxed type for field \'complexity\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 363
    :cond_15
    const-string v7, "answersToComplexityUp"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_16

    .line 364
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'answersToComplexityUp\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 366
    :cond_16
    const-string v7, "answersToComplexityUp"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_17

    .line 367
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'answersToComplexityUp\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 369
    :cond_17
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_18

    .line 370
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'answersToComplexityUp\' does support null values in the existing Realm file. Use corresponding boxed type for field \'answersToComplexityUp\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 372
    :cond_18
    const-string v7, "answersToComplexityDown"

    invoke-interface {v3, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_19

    .line 373
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Missing field \'answersToComplexityDown\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 375
    :cond_19
    const-string v7, "answersToComplexityDown"

    invoke-interface {v3, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    sget-object v8, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v7, v8, :cond_1a

    .line 376
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Invalid type \'int\' for field \'answersToComplexityDown\' in existing Realm file."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 378
    :cond_1a
    iget-wide v8, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v7

    if-eqz v7, :cond_1c

    .line 379
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Field \'answersToComplexityDown\' does support null values in the existing Realm file. Use corresponding boxed type for field \'answersToComplexityDown\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 383
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    :cond_1b
    new-instance v7, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    const-string v9, "The \'RememberNumberConfig\' class is missing from the schema for this Realm."

    invoke-direct {v7, v8, v9}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v7

    .line 381
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    :cond_1c
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 816
    if-ne p0, p1, :cond_1

    .line 830
    :cond_0
    :goto_0
    return v5

    .line 817
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 818
    check-cast v0, Lio/realm/RememberNumberConfigRealmProxy;

    .line 820
    .local v0, "aRememberNumberConfig":Lio/realm/RememberNumberConfigRealmProxy;
    iget-object v7, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 821
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 822
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 824
    :cond_6
    iget-object v7, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 825
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 826
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 828
    :cond_9
    iget-object v7, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 803
    iget-object v6, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 804
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 805
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 807
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 808
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 809
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 810
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 811
    return v1

    :cond_1
    move v6, v5

    .line 808
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 106
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 116
    :goto_0
    return-void

    .line 109
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 110
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iput-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 111
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 112
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 113
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 114
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 115
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$answersToComplexityDown()I
    .locals 4

    .prologue
    .line 236
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 237
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$answersToComplexityUp()I
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 217
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$complexity()I
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 197
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 121
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$maxComplexity()I
    .locals 4

    .prologue
    .line 176
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 177
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$minComplexity()I
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 157
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 798
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$trainingShowCount()I
    .locals 4

    .prologue
    .line 136
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 137
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v1, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmSet$answersToComplexityDown(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 241
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 242
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 252
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 246
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 250
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 251
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$answersToComplexityUp(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 221
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 222
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 232
    :goto_0
    return-void

    .line 225
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 226
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 230
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 231
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$complexity(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 201
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 202
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 212
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 206
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 210
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 211
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    return-void

    .line 130
    :cond_0
    iget-object v0, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 131
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$maxComplexity(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 181
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 182
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    :goto_0
    return-void

    .line 185
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 186
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 190
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 191
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$minComplexity(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 161
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 162
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 172
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 166
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 170
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 171
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public realmSet$trainingShowCount(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 141
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 152
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 146
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 150
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 151
    iget-object v1, p0, Lio/realm/RememberNumberConfigRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/RememberNumberConfigRealmProxy;->columnInfo:Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    iget-wide v2, v2, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 761
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 762
    const-string v1, "Invalid object"

    .line 793
    :goto_0
    return-object v1

    .line 764
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RememberNumberConfig = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 765
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 766
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 767
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 768
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 769
    const-string v1, "{trainingShowCount:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 770
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$trainingShowCount()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 771
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 772
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 773
    const-string v1, "{minComplexity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 774
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$minComplexity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 775
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 776
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 777
    const-string v1, "{maxComplexity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 778
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$maxComplexity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 779
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 780
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 781
    const-string v1, "{complexity:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 782
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$complexity()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 783
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 784
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 785
    const-string v1, "{answersToComplexityUp:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 786
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$answersToComplexityUp()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 787
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 788
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 789
    const-string v1, "{answersToComplexityDown:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 790
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy;->realmGet$answersToComplexityDown()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 791
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 792
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 793
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method
