.class Lio/realm/DefaultRealmModuleMediator;
.super Lio/realm/internal/RealmProxyMediator;
.source "DefaultRealmModuleMediator.java"


# annotations
.annotation runtime Lio/realm/annotations/RealmModule;
.end annotation


# static fields
.field private static final MODEL_CLASSES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 30
    .local v0, "modelClasses":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Class<+Lio/realm/RealmModel;>;>;"
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 32
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 33
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 34
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 35
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 36
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 37
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 38
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 39
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 40
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 41
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 42
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 43
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 45
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 46
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 48
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 49
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 50
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 51
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 52
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 53
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 54
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 55
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 56
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 57
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 59
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 60
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 61
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v1

    sput-object v1, Lio/realm/DefaultRealmModuleMediator;->MODEL_CLASSES:Ljava/util/Set;

    .line 62
    return-void
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lio/realm/internal/RealmProxyMediator;-><init>()V

    return-void
.end method


# virtual methods
.method public copyOrUpdate(Lio/realm/Realm;Lio/realm/RealmModel;ZLjava/util/Map;)Lio/realm/RealmModel;
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .param p3, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Lio/realm/Realm;",
            "TE;Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 505
    .local p2, "obj":Lio/realm/RealmModel;, "TE;"
    .local p4, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    instance-of v1, p2, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 507
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    :goto_0
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 508
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/MathConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 568
    :goto_1
    return-object v1

    .line 505
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 509
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    :cond_1
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 510
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/SpeedReadingResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 511
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_2
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 512
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/LineOfSightConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 513
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 514
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/CupTimerResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 515
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_4
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 516
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/EvenNumbersConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 517
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_5
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 518
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordPairsResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_1

    .line 519
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_6
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 520
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/GreenDotResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 521
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_7
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 522
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/ConcentrationConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 523
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_8
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 524
    check-cast p2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/PassCourseResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 525
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_9
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 526
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/RememberNumberConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 527
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_a
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 528
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordBlockConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 529
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_b
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 530
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/FlashWordsConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 531
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_c
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 532
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/GreenDotConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 533
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_d
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 534
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/RememberNumberResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 535
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 536
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordPairsConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 537
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_f
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 538
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/CupTimerConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 539
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_10
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 540
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/LineOfSightResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 541
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_11
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 542
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/EvenNumbersResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 543
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_12
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 544
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/SpeedReadingConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 545
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_13
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 546
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordsColumnsResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 547
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_14
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 548
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/MathResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 549
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_15
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 550
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordsColumnsConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 551
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_16
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 552
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/ConcentrationResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 553
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_17
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 554
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/SchulteTableConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 555
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_18
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 556
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/FlashWordResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 557
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_19
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 558
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/SchulteTableResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 559
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1a
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 560
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/RememberWordsResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 561
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1b
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 562
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/TrueColorsResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 563
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1c
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 564
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/RememberWordsConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 565
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1d
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 566
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/TrueColorsConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 567
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 568
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    .end local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, p2, p3, p4}, Lio/realm/WordBlockResultRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_1

    .line 570
    .restart local p2    # "obj":Lio/realm/RealmModel;, "TE;"
    :cond_1f
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public createDetachedCopy(Lio/realm/RealmModel;ILjava/util/Map;)Lio/realm/RealmModel;
    .locals 3
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(TE;I",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)TE;"
        }
    .end annotation

    .prologue
    .local p1, "realmObject":Lio/realm/RealmModel;, "TE;"
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    const/4 v2, 0x0

    .line 1162
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 1164
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1165
    check-cast p1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/MathConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    .line 1225
    :goto_0
    return-object v1

    .line 1166
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_0
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1167
    check-cast p1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/SpeedReadingResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1168
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1169
    check-cast p1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1170
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_2
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1171
    check-cast p1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/CupTimerResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1172
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1173
    check-cast p1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/EvenNumbersConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1174
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_4
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1175
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordPairsResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1176
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_5
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 1177
    check-cast p1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/GreenDotResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto :goto_0

    .line 1178
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_6
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1179
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1180
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_7
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 1181
    check-cast p1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/PassCourseResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1182
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_8
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 1183
    check-cast p1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1184
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_9
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1185
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordBlockConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1186
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_a
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1187
    check-cast p1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/FlashWordsConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1188
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_b
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 1189
    check-cast p1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/GreenDotConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1190
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_c
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 1191
    check-cast p1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/RememberNumberResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1192
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_d
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 1193
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordPairsConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1194
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_e
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1195
    check-cast p1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/CupTimerConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1196
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_f
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 1197
    check-cast p1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/LineOfSightResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1198
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_10
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 1199
    check-cast p1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/EvenNumbersResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1200
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_11
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 1201
    check-cast p1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/SpeedReadingConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1202
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_12
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 1203
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordsColumnsResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1204
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_13
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 1205
    check-cast p1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/MathResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1206
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_14
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 1207
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordsColumnsConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1208
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_15
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 1209
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/ConcentrationResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1210
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_16
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 1211
    check-cast p1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/SchulteTableConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1212
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_17
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 1213
    check-cast p1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/FlashWordResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1214
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_18
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 1215
    check-cast p1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/SchulteTableResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1216
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_19
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 1217
    check-cast p1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/RememberWordsResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1218
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1a
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 1219
    check-cast p1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/TrueColorsResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1220
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1b
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 1221
    check-cast p1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/RememberWordsConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1222
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1c
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 1223
    check-cast p1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/TrueColorsConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1224
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1d
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 1225
    check-cast p1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    .end local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    invoke-static {p1, v2, p2, p3}, Lio/realm/WordBlockResultRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1227
    .restart local p1    # "realmObject":Lio/realm/RealmModel;, "TE;"
    :cond_1e
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public createOrUpdateUsingJsonObject(Ljava/lang/Class;Lio/realm/Realm;Lorg/json/JSONObject;Z)Lio/realm/RealmModel;
    .locals 1
    .param p2, "realm"    # Lio/realm/Realm;
    .param p3, "json"    # Lorg/json/JSONObject;
    .param p4, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/Realm;",
            "Lorg/json/JSONObject;",
            "Z)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 1017
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 1019
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1020
    invoke-static {p2, p3, p4}, Lio/realm/MathConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 1080
    :goto_0
    return-object v0

    .line 1021
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1022
    invoke-static {p2, p3, p4}, Lio/realm/SpeedReadingResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1023
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1024
    invoke-static {p2, p3, p4}, Lio/realm/LineOfSightConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1025
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1026
    invoke-static {p2, p3, p4}, Lio/realm/CupTimerResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1027
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1028
    invoke-static {p2, p3, p4}, Lio/realm/EvenNumbersConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1029
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1030
    invoke-static {p2, p3, p4}, Lio/realm/WordPairsResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1031
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1032
    invoke-static {p2, p3, p4}, Lio/realm/GreenDotResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1033
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1034
    invoke-static {p2, p3, p4}, Lio/realm/ConcentrationConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1035
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1036
    invoke-static {p2, p3, p4}, Lio/realm/PassCourseResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1037
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1038
    invoke-static {p2, p3, p4}, Lio/realm/RememberNumberConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1039
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1040
    invoke-static {p2, p3, p4}, Lio/realm/WordBlockConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1041
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1042
    invoke-static {p2, p3, p4}, Lio/realm/FlashWordsConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1043
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1044
    invoke-static {p2, p3, p4}, Lio/realm/GreenDotConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1045
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1046
    invoke-static {p2, p3, p4}, Lio/realm/RememberNumberResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1047
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1048
    invoke-static {p2, p3, p4}, Lio/realm/WordPairsConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1049
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1050
    invoke-static {p2, p3, p4}, Lio/realm/CupTimerConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1051
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1052
    invoke-static {p2, p3, p4}, Lio/realm/LineOfSightResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1053
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1054
    invoke-static {p2, p3, p4}, Lio/realm/EvenNumbersResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1055
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1056
    invoke-static {p2, p3, p4}, Lio/realm/SpeedReadingConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1057
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1058
    invoke-static {p2, p3, p4}, Lio/realm/WordsColumnsResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1059
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1060
    invoke-static {p2, p3, p4}, Lio/realm/MathResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1061
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1062
    invoke-static {p2, p3, p4}, Lio/realm/WordsColumnsConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1063
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1064
    invoke-static {p2, p3, p4}, Lio/realm/ConcentrationResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1065
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1066
    invoke-static {p2, p3, p4}, Lio/realm/SchulteTableConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1067
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1068
    invoke-static {p2, p3, p4}, Lio/realm/FlashWordResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1069
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1070
    invoke-static {p2, p3, p4}, Lio/realm/SchulteTableResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1071
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1072
    invoke-static {p2, p3, p4}, Lio/realm/RememberWordsResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1073
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1074
    invoke-static {p2, p3, p4}, Lio/realm/TrueColorsResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1075
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1076
    invoke-static {p2, p3, p4}, Lio/realm/RememberWordsConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1077
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1078
    invoke-static {p2, p3, p4}, Lio/realm/TrueColorsConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1079
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1080
    invoke-static {p2, p3, p4}, Lio/realm/WordBlockResultRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1082
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public createRealmObjectSchema(Ljava/lang/Class;Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 1
    .param p2, "realmSchema"    # Lio/realm/RealmSchema;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/RealmSchema;",
            ")",
            "Lio/realm/RealmObjectSchema;"
        }
    .end annotation

    .prologue
    .line 137
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 139
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    invoke-static {p2}, Lio/realm/MathConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    .line 200
    :goto_0
    return-object v0

    .line 141
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    invoke-static {p2}, Lio/realm/SpeedReadingResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 143
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    invoke-static {p2}, Lio/realm/LineOfSightConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 145
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    invoke-static {p2}, Lio/realm/CupTimerResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 148
    invoke-static {p2}, Lio/realm/EvenNumbersConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 149
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 150
    invoke-static {p2}, Lio/realm/WordPairsResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 151
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 152
    invoke-static {p2}, Lio/realm/GreenDotResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 153
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 154
    invoke-static {p2}, Lio/realm/ConcentrationConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 155
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 156
    invoke-static {p2}, Lio/realm/PassCourseResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 157
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 158
    invoke-static {p2}, Lio/realm/RememberNumberConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto :goto_0

    .line 159
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 160
    invoke-static {p2}, Lio/realm/WordBlockConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 161
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 162
    invoke-static {p2}, Lio/realm/FlashWordsConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 163
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 164
    invoke-static {p2}, Lio/realm/GreenDotConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 165
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 166
    invoke-static {p2}, Lio/realm/RememberNumberResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 167
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 168
    invoke-static {p2}, Lio/realm/WordPairsConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 169
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 170
    invoke-static {p2}, Lio/realm/CupTimerConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 171
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 172
    invoke-static {p2}, Lio/realm/LineOfSightResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 173
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 174
    invoke-static {p2}, Lio/realm/EvenNumbersResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 175
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 176
    invoke-static {p2}, Lio/realm/SpeedReadingConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 177
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 178
    invoke-static {p2}, Lio/realm/WordsColumnsResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 179
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 180
    invoke-static {p2}, Lio/realm/MathResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 181
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 182
    invoke-static {p2}, Lio/realm/WordsColumnsConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 183
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 184
    invoke-static {p2}, Lio/realm/ConcentrationResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 185
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 186
    invoke-static {p2}, Lio/realm/SchulteTableConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 187
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 188
    invoke-static {p2}, Lio/realm/FlashWordResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 189
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 190
    invoke-static {p2}, Lio/realm/SchulteTableResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 191
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 192
    invoke-static {p2}, Lio/realm/RememberWordsResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 193
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 194
    invoke-static {p2}, Lio/realm/TrueColorsResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 195
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 196
    invoke-static {p2}, Lio/realm/RememberWordsConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 197
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 198
    invoke-static {p2}, Lio/realm/TrueColorsConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 199
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 200
    invoke-static {p2}, Lio/realm/WordBlockResultRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    move-result-object v0

    goto/16 :goto_0

    .line 202
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public createTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 1
    .param p2, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/internal/SharedRealm;",
            ")",
            "Lio/realm/internal/Table;"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 68
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-static {p2}, Lio/realm/MathConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 70
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    invoke-static {p2}, Lio/realm/SpeedReadingResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 72
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    invoke-static {p2}, Lio/realm/LineOfSightConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 74
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 75
    invoke-static {p2}, Lio/realm/CupTimerResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 76
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    invoke-static {p2}, Lio/realm/EvenNumbersConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 78
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 79
    invoke-static {p2}, Lio/realm/WordPairsResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 80
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 81
    invoke-static {p2}, Lio/realm/GreenDotResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 82
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 83
    invoke-static {p2}, Lio/realm/ConcentrationConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 84
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 85
    invoke-static {p2}, Lio/realm/PassCourseResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 87
    invoke-static {p2}, Lio/realm/RememberNumberConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 89
    invoke-static {p2}, Lio/realm/WordBlockConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 90
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 91
    invoke-static {p2}, Lio/realm/FlashWordsConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 92
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 93
    invoke-static {p2}, Lio/realm/GreenDotConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 94
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 95
    invoke-static {p2}, Lio/realm/RememberNumberResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 96
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 97
    invoke-static {p2}, Lio/realm/WordPairsConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 98
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 99
    invoke-static {p2}, Lio/realm/CupTimerConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 100
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 101
    invoke-static {p2}, Lio/realm/LineOfSightResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 102
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 103
    invoke-static {p2}, Lio/realm/EvenNumbersResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 104
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 105
    invoke-static {p2}, Lio/realm/SpeedReadingConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 106
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 107
    invoke-static {p2}, Lio/realm/WordsColumnsResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 108
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 109
    invoke-static {p2}, Lio/realm/MathResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 110
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 111
    invoke-static {p2}, Lio/realm/WordsColumnsConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 112
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 113
    invoke-static {p2}, Lio/realm/ConcentrationResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 114
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 115
    invoke-static {p2}, Lio/realm/SchulteTableConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 116
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 117
    invoke-static {p2}, Lio/realm/FlashWordResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 118
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 119
    invoke-static {p2}, Lio/realm/SchulteTableResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 120
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 121
    invoke-static {p2}, Lio/realm/RememberWordsResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 122
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 123
    invoke-static {p2}, Lio/realm/TrueColorsResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 124
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 125
    invoke-static {p2}, Lio/realm/RememberWordsConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 126
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 127
    invoke-static {p2}, Lio/realm/TrueColorsConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 128
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 129
    invoke-static {p2}, Lio/realm/WordBlockResultRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    move-result-object v0

    goto/16 :goto_0

    .line 131
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public createUsingJsonStream(Ljava/lang/Class;Lio/realm/Realm;Landroid/util/JsonReader;)Lio/realm/RealmModel;
    .locals 1
    .param p2, "realm"    # Lio/realm/Realm;
    .param p3, "reader"    # Landroid/util/JsonReader;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Lio/realm/Realm;",
            "Landroid/util/JsonReader;",
            ")TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1089
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 1091
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1092
    invoke-static {p2, p3}, Lio/realm/MathConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    .line 1152
    :goto_0
    return-object v0

    .line 1093
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1094
    invoke-static {p2, p3}, Lio/realm/SpeedReadingResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1095
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1096
    invoke-static {p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1097
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1098
    invoke-static {p2, p3}, Lio/realm/CupTimerResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1099
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1100
    invoke-static {p2, p3}, Lio/realm/EvenNumbersConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1101
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1102
    invoke-static {p2, p3}, Lio/realm/WordPairsResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1103
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1104
    invoke-static {p2, p3}, Lio/realm/GreenDotResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto :goto_0

    .line 1105
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1106
    invoke-static {p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1107
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 1108
    invoke-static {p2, p3}, Lio/realm/PassCourseResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1109
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1110
    invoke-static {p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1111
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1112
    invoke-static {p2, p3}, Lio/realm/WordBlockConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1113
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 1114
    invoke-static {p2, p3}, Lio/realm/FlashWordsConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1115
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 1116
    invoke-static {p2, p3}, Lio/realm/GreenDotConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1117
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1118
    invoke-static {p2, p3}, Lio/realm/RememberNumberResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1119
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1120
    invoke-static {p2, p3}, Lio/realm/WordPairsConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1121
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 1122
    invoke-static {p2, p3}, Lio/realm/CupTimerConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1123
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1124
    invoke-static {p2, p3}, Lio/realm/LineOfSightResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1125
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1126
    invoke-static {p2, p3}, Lio/realm/EvenNumbersResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1127
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 1128
    invoke-static {p2, p3}, Lio/realm/SpeedReadingConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1129
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 1130
    invoke-static {p2, p3}, Lio/realm/WordsColumnsResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1131
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 1132
    invoke-static {p2, p3}, Lio/realm/MathResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1133
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 1134
    invoke-static {p2, p3}, Lio/realm/WordsColumnsConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1135
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1136
    invoke-static {p2, p3}, Lio/realm/ConcentrationResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1137
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1138
    invoke-static {p2, p3}, Lio/realm/SchulteTableConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1139
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 1140
    invoke-static {p2, p3}, Lio/realm/FlashWordResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1141
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 1142
    invoke-static {p2, p3}, Lio/realm/SchulteTableResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1143
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 1144
    invoke-static {p2, p3}, Lio/realm/RememberWordsResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1145
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 1146
    invoke-static {p2, p3}, Lio/realm/TrueColorsResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1147
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1148
    invoke-static {p2, p3}, Lio/realm/RememberWordsConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1149
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 1150
    invoke-static {p2, p3}, Lio/realm/TrueColorsConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1151
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 1152
    invoke-static {p2, p3}, Lio/realm/WordBlockResultRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/RealmModel;

    goto/16 :goto_0

    .line 1154
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public getFieldNames(Ljava/lang/Class;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 279
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 281
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 282
    invoke-static {}, Lio/realm/MathConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    .line 342
    :goto_0
    return-object v0

    .line 283
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    invoke-static {}, Lio/realm/SpeedReadingResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 285
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 286
    invoke-static {}, Lio/realm/LineOfSightConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 287
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 288
    invoke-static {}, Lio/realm/CupTimerResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 289
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 290
    invoke-static {}, Lio/realm/EvenNumbersConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 291
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 292
    invoke-static {}, Lio/realm/WordPairsResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 293
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 294
    invoke-static {}, Lio/realm/GreenDotResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 295
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 296
    invoke-static {}, Lio/realm/ConcentrationConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 297
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 298
    invoke-static {}, Lio/realm/PassCourseResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 299
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 300
    invoke-static {}, Lio/realm/RememberNumberConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 301
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 302
    invoke-static {}, Lio/realm/WordBlockConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 303
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 304
    invoke-static {}, Lio/realm/FlashWordsConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 305
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 306
    invoke-static {}, Lio/realm/GreenDotConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 307
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 308
    invoke-static {}, Lio/realm/RememberNumberResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 309
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 310
    invoke-static {}, Lio/realm/WordPairsConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 311
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 312
    invoke-static {}, Lio/realm/CupTimerConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 313
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 314
    invoke-static {}, Lio/realm/LineOfSightResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 315
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 316
    invoke-static {}, Lio/realm/EvenNumbersResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 317
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 318
    invoke-static {}, Lio/realm/SpeedReadingConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 319
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 320
    invoke-static {}, Lio/realm/WordsColumnsResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 321
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 322
    invoke-static {}, Lio/realm/MathResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 323
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 324
    invoke-static {}, Lio/realm/WordsColumnsConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 325
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 326
    invoke-static {}, Lio/realm/ConcentrationResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 327
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 328
    invoke-static {}, Lio/realm/SchulteTableConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 329
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 330
    invoke-static {}, Lio/realm/FlashWordResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 331
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 332
    invoke-static {}, Lio/realm/SchulteTableResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 333
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 334
    invoke-static {}, Lio/realm/RememberWordsResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 335
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 336
    invoke-static {}, Lio/realm/TrueColorsResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 337
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 338
    invoke-static {}, Lio/realm/RememberWordsConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 339
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 340
    invoke-static {}, Lio/realm/TrueColorsConfigRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 341
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 342
    invoke-static {}, Lio/realm/WordBlockResultRealmProxy;->getFieldNames()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 344
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public getModelClasses()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 498
    sget-object v0, Lio/realm/DefaultRealmModuleMediator;->MODEL_CLASSES:Ljava/util/Set;

    return-object v0
.end method

.method public getTableName(Ljava/lang/Class;)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 350
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 352
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    invoke-static {}, Lio/realm/MathConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    .line 413
    :goto_0
    return-object v0

    .line 354
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 355
    invoke-static {}, Lio/realm/SpeedReadingResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 356
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 357
    invoke-static {}, Lio/realm/LineOfSightConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 359
    invoke-static {}, Lio/realm/CupTimerResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 360
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 361
    invoke-static {}, Lio/realm/EvenNumbersConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 362
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 363
    invoke-static {}, Lio/realm/WordPairsResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 364
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 365
    invoke-static {}, Lio/realm/GreenDotResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 366
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 367
    invoke-static {}, Lio/realm/ConcentrationConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 368
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 369
    invoke-static {}, Lio/realm/PassCourseResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 370
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 371
    invoke-static {}, Lio/realm/RememberNumberConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 372
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 373
    invoke-static {}, Lio/realm/WordBlockConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 374
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 375
    invoke-static {}, Lio/realm/FlashWordsConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 376
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 377
    invoke-static {}, Lio/realm/GreenDotConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 378
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 379
    invoke-static {}, Lio/realm/RememberNumberResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 380
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 381
    invoke-static {}, Lio/realm/WordPairsConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 382
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 383
    invoke-static {}, Lio/realm/CupTimerConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 384
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 385
    invoke-static {}, Lio/realm/LineOfSightResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 386
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 387
    invoke-static {}, Lio/realm/EvenNumbersResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 388
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 389
    invoke-static {}, Lio/realm/SpeedReadingConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 390
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 391
    invoke-static {}, Lio/realm/WordsColumnsResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 392
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 393
    invoke-static {}, Lio/realm/MathResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 394
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 395
    invoke-static {}, Lio/realm/WordsColumnsConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 396
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 397
    invoke-static {}, Lio/realm/ConcentrationResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 398
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 399
    invoke-static {}, Lio/realm/SchulteTableConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 401
    invoke-static {}, Lio/realm/FlashWordResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 402
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 403
    invoke-static {}, Lio/realm/SchulteTableResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 404
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 405
    invoke-static {}, Lio/realm/RememberWordsResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 406
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 407
    invoke-static {}, Lio/realm/TrueColorsResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 408
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 409
    invoke-static {}, Lio/realm/RememberWordsConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 410
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 411
    invoke-static {}, Lio/realm/TrueColorsConfigRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 412
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 413
    invoke-static {}, Lio/realm/WordBlockResultRealmProxy;->getTableName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 415
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method

.method public insert(Lio/realm/Realm;Lio/realm/RealmModel;Ljava/util/Map;)V
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "object"    # Lio/realm/RealmModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lio/realm/RealmModel;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 578
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    instance-of v1, p2, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 580
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :goto_0
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 581
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/MathConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J

    .line 645
    :goto_1
    return-void

    .line 578
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 582
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_1
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 583
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SpeedReadingResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/Map;)J

    goto :goto_1

    .line 584
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_2
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 585
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 586
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 587
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/CupTimerResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;Ljava/util/Map;)J

    goto :goto_1

    .line 588
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_4
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 589
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/EvenNumbersConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 590
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_5
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 591
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordPairsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/Map;)J

    goto :goto_1

    .line 592
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_6
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 593
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/GreenDotResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;Ljava/util/Map;)J

    goto :goto_1

    .line 594
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_7
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 595
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 596
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_8
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 597
    check-cast p2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/PassCourseResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J

    goto :goto_1

    .line 598
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_9
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 599
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 600
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_a
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 601
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordBlockConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 602
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_b
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 603
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/FlashWordsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 604
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_c
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 605
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/GreenDotConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 606
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_d
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 607
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberNumberResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 608
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 609
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordPairsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 610
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_f
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 611
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/CupTimerConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 612
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_10
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 613
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/LineOfSightResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 614
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_11
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 615
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/EvenNumbersResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 616
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_12
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 617
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SpeedReadingConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 618
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_13
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 619
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordsColumnsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 620
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_14
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 621
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/MathResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 622
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_15
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 623
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordsColumnsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 624
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_16
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 625
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/ConcentrationResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 626
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_17
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 627
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SchulteTableConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 628
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_18
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 629
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/FlashWordResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 630
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_19
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 631
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SchulteTableResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 632
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1a
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 633
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberWordsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 634
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1b
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 635
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/TrueColorsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 636
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1c
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 637
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberWordsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 638
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1d
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 639
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/TrueColorsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 640
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 641
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    .end local p2    # "object":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordBlockResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 643
    .restart local p2    # "object":Lio/realm/RealmModel;
    :cond_1f
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public insert(Lio/realm/Realm;Ljava/util/Collection;)V
    .locals 5
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 649
    .local p2, "objects":Ljava/util/Collection;, "Ljava/util/Collection<+Lio/realm/RealmModel;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 650
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    const/4 v3, 0x0

    .line 651
    .local v3, "object":Lio/realm/RealmModel;
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 652
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 654
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "object":Lio/realm/RealmModel;
    check-cast v3, Lio/realm/RealmModel;

    .line 657
    .restart local v3    # "object":Lio/realm/RealmModel;
    instance-of v4, v3, Lio/realm/internal/RealmObjectProxy;

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 659
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :goto_0
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, v3

    .line 660
    check-cast v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-static {p1, v4, v0}, Lio/realm/MathConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J

    .line 724
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 725
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 726
    invoke-static {p1, v2, v0}, Lio/realm/MathConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    .line 792
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_0
    :goto_2
    return-void

    .line 657
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0

    .line 661
    .restart local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_2
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v4, v3

    .line 662
    check-cast v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-static {p1, v4, v0}, Lio/realm/SpeedReadingResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/Map;)J

    goto :goto_1

    .line 663
    :cond_3
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v4, v3

    .line 664
    check-cast v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-static {p1, v4, v0}, Lio/realm/LineOfSightConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 665
    :cond_4
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v4, v3

    .line 666
    check-cast v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-static {p1, v4, v0}, Lio/realm/CupTimerResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;Ljava/util/Map;)J

    goto :goto_1

    .line 667
    :cond_5
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v4, v3

    .line 668
    check-cast v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-static {p1, v4, v0}, Lio/realm/EvenNumbersConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 669
    :cond_6
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object v4, v3

    .line 670
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordPairsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/Map;)J

    goto :goto_1

    .line 671
    :cond_7
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move-object v4, v3

    .line 672
    check-cast v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-static {p1, v4, v0}, Lio/realm/GreenDotResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;Ljava/util/Map;)J

    goto :goto_1

    .line 673
    :cond_8
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    move-object v4, v3

    .line 674
    check-cast v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-static {p1, v4, v0}, Lio/realm/ConcentrationConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 675
    :cond_9
    const-class v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object v4, v3

    .line 676
    check-cast v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-static {p1, v4, v0}, Lio/realm/PassCourseResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 677
    :cond_a
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object v4, v3

    .line 678
    check-cast v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-static {p1, v4, v0}, Lio/realm/RememberNumberConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 679
    :cond_b
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move-object v4, v3

    .line 680
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordBlockConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 681
    :cond_c
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object v4, v3

    .line 682
    check-cast v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/FlashWordsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 683
    :cond_d
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object v4, v3

    .line 684
    check-cast v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-static {p1, v4, v0}, Lio/realm/GreenDotConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 685
    :cond_e
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    move-object v4, v3

    .line 686
    check-cast v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-static {p1, v4, v0}, Lio/realm/RememberNumberResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 687
    :cond_f
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    move-object v4, v3

    .line 688
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordPairsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 689
    :cond_10
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    move-object v4, v3

    .line 690
    check-cast v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-static {p1, v4, v0}, Lio/realm/CupTimerConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 691
    :cond_11
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    move-object v4, v3

    .line 692
    check-cast v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-static {p1, v4, v0}, Lio/realm/LineOfSightResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 693
    :cond_12
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    move-object v4, v3

    .line 694
    check-cast v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-static {p1, v4, v0}, Lio/realm/EvenNumbersResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 695
    :cond_13
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    move-object v4, v3

    .line 696
    check-cast v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-static {p1, v4, v0}, Lio/realm/SpeedReadingConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 697
    :cond_14
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object v4, v3

    .line 698
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordsColumnsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 699
    :cond_15
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    move-object v4, v3

    .line 700
    check-cast v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-static {p1, v4, v0}, Lio/realm/MathResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 701
    :cond_16
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    move-object v4, v3

    .line 702
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordsColumnsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 703
    :cond_17
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    move-object v4, v3

    .line 704
    check-cast v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-static {p1, v4, v0}, Lio/realm/ConcentrationResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 705
    :cond_18
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    move-object v4, v3

    .line 706
    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-static {p1, v4, v0}, Lio/realm/SchulteTableConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 707
    :cond_19
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object v4, v3

    .line 708
    check-cast v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-static {p1, v4, v0}, Lio/realm/FlashWordResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 709
    :cond_1a
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    move-object v4, v3

    .line 710
    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-static {p1, v4, v0}, Lio/realm/SchulteTableResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 711
    :cond_1b
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    move-object v4, v3

    .line 712
    check-cast v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-static {p1, v4, v0}, Lio/realm/RememberWordsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 713
    :cond_1c
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    move-object v4, v3

    .line 714
    check-cast v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-static {p1, v4, v0}, Lio/realm/TrueColorsResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 715
    :cond_1d
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    move-object v4, v3

    .line 716
    check-cast v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/RememberWordsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 717
    :cond_1e
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    move-object v4, v3

    .line 718
    check-cast v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/TrueColorsConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 719
    :cond_1f
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    move-object v4, v3

    .line 720
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordBlockResultRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 722
    :cond_20
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v4

    throw v4

    .line 727
    :cond_21
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 728
    invoke-static {p1, v2, v0}, Lio/realm/SpeedReadingResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 729
    :cond_22
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 730
    invoke-static {p1, v2, v0}, Lio/realm/LineOfSightConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 731
    :cond_23
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 732
    invoke-static {p1, v2, v0}, Lio/realm/CupTimerResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 733
    :cond_24
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 734
    invoke-static {p1, v2, v0}, Lio/realm/EvenNumbersConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 735
    :cond_25
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 736
    invoke-static {p1, v2, v0}, Lio/realm/WordPairsResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 737
    :cond_26
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 738
    invoke-static {p1, v2, v0}, Lio/realm/GreenDotResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 739
    :cond_27
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 740
    invoke-static {p1, v2, v0}, Lio/realm/ConcentrationConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 741
    :cond_28
    const-class v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 742
    invoke-static {p1, v2, v0}, Lio/realm/PassCourseResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 743
    :cond_29
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 744
    invoke-static {p1, v2, v0}, Lio/realm/RememberNumberConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 745
    :cond_2a
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 746
    invoke-static {p1, v2, v0}, Lio/realm/WordBlockConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 747
    :cond_2b
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 748
    invoke-static {p1, v2, v0}, Lio/realm/FlashWordsConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 749
    :cond_2c
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 750
    invoke-static {p1, v2, v0}, Lio/realm/GreenDotConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 751
    :cond_2d
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 752
    invoke-static {p1, v2, v0}, Lio/realm/RememberNumberResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 753
    :cond_2e
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 754
    invoke-static {p1, v2, v0}, Lio/realm/WordPairsConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 755
    :cond_2f
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 756
    invoke-static {p1, v2, v0}, Lio/realm/CupTimerConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 757
    :cond_30
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_31

    .line 758
    invoke-static {p1, v2, v0}, Lio/realm/LineOfSightResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 759
    :cond_31
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 760
    invoke-static {p1, v2, v0}, Lio/realm/EvenNumbersResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 761
    :cond_32
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_33

    .line 762
    invoke-static {p1, v2, v0}, Lio/realm/SpeedReadingConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 763
    :cond_33
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 764
    invoke-static {p1, v2, v0}, Lio/realm/WordsColumnsResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 765
    :cond_34
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 766
    invoke-static {p1, v2, v0}, Lio/realm/MathResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 767
    :cond_35
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 768
    invoke-static {p1, v2, v0}, Lio/realm/WordsColumnsConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 769
    :cond_36
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 770
    invoke-static {p1, v2, v0}, Lio/realm/ConcentrationResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 771
    :cond_37
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_38

    .line 772
    invoke-static {p1, v2, v0}, Lio/realm/SchulteTableConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 773
    :cond_38
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_39

    .line 774
    invoke-static {p1, v2, v0}, Lio/realm/FlashWordResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 775
    :cond_39
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a

    .line 776
    invoke-static {p1, v2, v0}, Lio/realm/SchulteTableResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 777
    :cond_3a
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 778
    invoke-static {p1, v2, v0}, Lio/realm/RememberWordsResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 779
    :cond_3b
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 780
    invoke-static {p1, v2, v0}, Lio/realm/TrueColorsResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 781
    :cond_3c
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 782
    invoke-static {p1, v2, v0}, Lio/realm/RememberWordsConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 783
    :cond_3d
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 784
    invoke-static {p1, v2, v0}, Lio/realm/TrueColorsConfigRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 785
    :cond_3e
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3f

    .line 786
    invoke-static {p1, v2, v0}, Lio/realm/WordBlockResultRealmProxy;->insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 788
    :cond_3f
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v4

    throw v4
.end method

.method public insertOrUpdate(Lio/realm/Realm;Lio/realm/RealmModel;Ljava/util/Map;)V
    .locals 2
    .param p1, "realm"    # Lio/realm/Realm;
    .param p2, "obj"    # Lio/realm/RealmModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lio/realm/RealmModel;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 798
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    instance-of v1, p2, Lio/realm/internal/RealmObjectProxy;

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    .line 800
    .local v0, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :goto_0
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 801
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/MathConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J

    .line 865
    :goto_1
    return-void

    .line 798
    .end local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    goto :goto_0

    .line 802
    .restart local v0    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_1
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 803
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SpeedReadingResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/Map;)J

    goto :goto_1

    .line 804
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_2
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 805
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 806
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_3
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 807
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/CupTimerResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;Ljava/util/Map;)J

    goto :goto_1

    .line 808
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_4
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 809
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/EvenNumbersConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 810
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_5
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 811
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordPairsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/Map;)J

    goto :goto_1

    .line 812
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_6
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 813
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/GreenDotResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;Ljava/util/Map;)J

    goto :goto_1

    .line 814
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_7
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 815
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 816
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_8
    const-class v1, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 817
    check-cast p2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/PassCourseResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J

    goto :goto_1

    .line 818
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_9
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 819
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 820
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_a
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 821
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordBlockConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 822
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_b
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 823
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/FlashWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 824
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_c
    const-class v1, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 825
    check-cast p2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/GreenDotConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 826
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_d
    const-class v1, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 827
    check-cast p2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberNumberResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 828
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 829
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordPairsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 830
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_f
    const-class v1, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 831
    check-cast p2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/CupTimerConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 832
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_10
    const-class v1, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 833
    check-cast p2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/LineOfSightResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 834
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_11
    const-class v1, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 835
    check-cast p2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/EvenNumbersResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 836
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_12
    const-class v1, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_13

    .line 837
    check-cast p2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SpeedReadingConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 838
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_13
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 839
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordsColumnsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 840
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_14
    const-class v1, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 841
    check-cast p2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/MathResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 842
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_15
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 843
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordsColumnsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 844
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_16
    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_17

    .line 845
    check-cast p2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/ConcentrationResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 846
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_17
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_18

    .line 847
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SchulteTableConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 848
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_18
    const-class v1, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 849
    check-cast p2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/FlashWordResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 850
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_19
    const-class v1, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1a

    .line 851
    check-cast p2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/SchulteTableResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 852
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1a
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 853
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberWordsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 854
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1b
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1c

    .line 855
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/TrueColorsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 856
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1c
    const-class v1, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 857
    check-cast p2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/RememberWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 858
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1d
    const-class v1, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1e

    .line 859
    check-cast p2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/TrueColorsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 860
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1e
    const-class v1, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 861
    check-cast p2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    .end local p2    # "obj":Lio/realm/RealmModel;
    invoke-static {p1, p2, p3}, Lio/realm/WordBlockResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 863
    .restart local p2    # "obj":Lio/realm/RealmModel;
    :cond_1f
    invoke-static {v0}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v1

    throw v1
.end method

.method public insertOrUpdate(Lio/realm/Realm;Ljava/util/Collection;)V
    .locals 5
    .param p1, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Collection",
            "<+",
            "Lio/realm/RealmModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 869
    .local p2, "objects":Ljava/util/Collection;, "Ljava/util/Collection<+Lio/realm/RealmModel;>;"
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 870
    .local v2, "iterator":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    const/4 v3, 0x0

    .line 871
    .local v3, "object":Lio/realm/RealmModel;
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 872
    .local v0, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 874
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "object":Lio/realm/RealmModel;
    check-cast v3, Lio/realm/RealmModel;

    .line 877
    .restart local v3    # "object":Lio/realm/RealmModel;
    instance-of v4, v3, Lio/realm/internal/RealmObjectProxy;

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v1

    .line 879
    .local v1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :goto_0
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v4, v3

    .line 880
    check-cast v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-static {p1, v4, v0}, Lio/realm/MathConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;Ljava/util/Map;)J

    .line 944
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 945
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 946
    invoke-static {p1, v2, v0}, Lio/realm/MathConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    .line 1012
    .end local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_0
    :goto_2
    return-void

    .line 877
    :cond_1
    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    goto :goto_0

    .line 881
    .restart local v1    # "clazz":Ljava/lang/Class;, "Ljava/lang/Class<Lio/realm/RealmModel;>;"
    :cond_2
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move-object v4, v3

    .line 882
    check-cast v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-static {p1, v4, v0}, Lio/realm/SpeedReadingResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;Ljava/util/Map;)J

    goto :goto_1

    .line 883
    :cond_3
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    move-object v4, v3

    .line 884
    check-cast v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-static {p1, v4, v0}, Lio/realm/LineOfSightConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 885
    :cond_4
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    move-object v4, v3

    .line 886
    check-cast v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-static {p1, v4, v0}, Lio/realm/CupTimerResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;Ljava/util/Map;)J

    goto :goto_1

    .line 887
    :cond_5
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    move-object v4, v3

    .line 888
    check-cast v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-static {p1, v4, v0}, Lio/realm/EvenNumbersConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 889
    :cond_6
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    move-object v4, v3

    .line 890
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordPairsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;Ljava/util/Map;)J

    goto :goto_1

    .line 891
    :cond_7
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    move-object v4, v3

    .line 892
    check-cast v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-static {p1, v4, v0}, Lio/realm/GreenDotResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;Ljava/util/Map;)J

    goto :goto_1

    .line 893
    :cond_8
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_9

    move-object v4, v3

    .line 894
    check-cast v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-static {p1, v4, v0}, Lio/realm/ConcentrationConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    goto :goto_1

    .line 895
    :cond_9
    const-class v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_a

    move-object v4, v3

    .line 896
    check-cast v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-static {p1, v4, v0}, Lio/realm/PassCourseResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 897
    :cond_a
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    move-object v4, v3

    .line 898
    check-cast v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-static {p1, v4, v0}, Lio/realm/RememberNumberConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 899
    :cond_b
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move-object v4, v3

    .line 900
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordBlockConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 901
    :cond_c
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    move-object v4, v3

    .line 902
    check-cast v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/FlashWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 903
    :cond_d
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object v4, v3

    .line 904
    check-cast v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-static {p1, v4, v0}, Lio/realm/GreenDotConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 905
    :cond_e
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_f

    move-object v4, v3

    .line 906
    check-cast v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-static {p1, v4, v0}, Lio/realm/RememberNumberResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 907
    :cond_f
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_10

    move-object v4, v3

    .line 908
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordPairsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 909
    :cond_10
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_11

    move-object v4, v3

    .line 910
    check-cast v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-static {p1, v4, v0}, Lio/realm/CupTimerConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 911
    :cond_11
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    move-object v4, v3

    .line 912
    check-cast v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-static {p1, v4, v0}, Lio/realm/LineOfSightResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 913
    :cond_12
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    move-object v4, v3

    .line 914
    check-cast v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-static {p1, v4, v0}, Lio/realm/EvenNumbersResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 915
    :cond_13
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    move-object v4, v3

    .line 916
    check-cast v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-static {p1, v4, v0}, Lio/realm/SpeedReadingConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 917
    :cond_14
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    move-object v4, v3

    .line 918
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordsColumnsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 919
    :cond_15
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    move-object v4, v3

    .line 920
    check-cast v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-static {p1, v4, v0}, Lio/realm/MathResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 921
    :cond_16
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_17

    move-object v4, v3

    .line 922
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/WordsColumnsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 923
    :cond_17
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    move-object v4, v3

    .line 924
    check-cast v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-static {p1, v4, v0}, Lio/realm/ConcentrationResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 925
    :cond_18
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_19

    move-object v4, v3

    .line 926
    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-static {p1, v4, v0}, Lio/realm/SchulteTableConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 927
    :cond_19
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1a

    move-object v4, v3

    .line 928
    check-cast v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-static {p1, v4, v0}, Lio/realm/FlashWordResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 929
    :cond_1a
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1b

    move-object v4, v3

    .line 930
    check-cast v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-static {p1, v4, v0}, Lio/realm/SchulteTableResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 931
    :cond_1b
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1c

    move-object v4, v3

    .line 932
    check-cast v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-static {p1, v4, v0}, Lio/realm/RememberWordsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 933
    :cond_1c
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1d

    move-object v4, v3

    .line 934
    check-cast v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-static {p1, v4, v0}, Lio/realm/TrueColorsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 935
    :cond_1d
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1e

    move-object v4, v3

    .line 936
    check-cast v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/RememberWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 937
    :cond_1e
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    move-object v4, v3

    .line 938
    check-cast v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-static {p1, v4, v0}, Lio/realm/TrueColorsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 939
    :cond_1f
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_20

    move-object v4, v3

    .line 940
    check-cast v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-static {p1, v4, v0}, Lio/realm/WordBlockResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;Ljava/util/Map;)J

    goto/16 :goto_1

    .line 942
    :cond_20
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v4

    throw v4

    .line 947
    :cond_21
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_22

    .line 948
    invoke-static {p1, v2, v0}, Lio/realm/SpeedReadingResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 949
    :cond_22
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 950
    invoke-static {p1, v2, v0}, Lio/realm/LineOfSightConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 951
    :cond_23
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_24

    .line 952
    invoke-static {p1, v2, v0}, Lio/realm/CupTimerResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 953
    :cond_24
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 954
    invoke-static {p1, v2, v0}, Lio/realm/EvenNumbersConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 955
    :cond_25
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_26

    .line 956
    invoke-static {p1, v2, v0}, Lio/realm/WordPairsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 957
    :cond_26
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 958
    invoke-static {p1, v2, v0}, Lio/realm/GreenDotResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 959
    :cond_27
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 960
    invoke-static {p1, v2, v0}, Lio/realm/ConcentrationConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 961
    :cond_28
    const-class v4, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_29

    .line 962
    invoke-static {p1, v2, v0}, Lio/realm/PassCourseResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 963
    :cond_29
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2a

    .line 964
    invoke-static {p1, v2, v0}, Lio/realm/RememberNumberConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 965
    :cond_2a
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2b

    .line 966
    invoke-static {p1, v2, v0}, Lio/realm/WordBlockConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 967
    :cond_2b
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2c

    .line 968
    invoke-static {p1, v2, v0}, Lio/realm/FlashWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 969
    :cond_2c
    const-class v4, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2d

    .line 970
    invoke-static {p1, v2, v0}, Lio/realm/GreenDotConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 971
    :cond_2d
    const-class v4, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2e

    .line 972
    invoke-static {p1, v2, v0}, Lio/realm/RememberNumberResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 973
    :cond_2e
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2f

    .line 974
    invoke-static {p1, v2, v0}, Lio/realm/WordPairsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 975
    :cond_2f
    const-class v4, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_30

    .line 976
    invoke-static {p1, v2, v0}, Lio/realm/CupTimerConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 977
    :cond_30
    const-class v4, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_31

    .line 978
    invoke-static {p1, v2, v0}, Lio/realm/LineOfSightResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 979
    :cond_31
    const-class v4, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_32

    .line 980
    invoke-static {p1, v2, v0}, Lio/realm/EvenNumbersResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 981
    :cond_32
    const-class v4, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_33

    .line 982
    invoke-static {p1, v2, v0}, Lio/realm/SpeedReadingConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 983
    :cond_33
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_34

    .line 984
    invoke-static {p1, v2, v0}, Lio/realm/WordsColumnsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 985
    :cond_34
    const-class v4, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_35

    .line 986
    invoke-static {p1, v2, v0}, Lio/realm/MathResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 987
    :cond_35
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_36

    .line 988
    invoke-static {p1, v2, v0}, Lio/realm/WordsColumnsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 989
    :cond_36
    const-class v4, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_37

    .line 990
    invoke-static {p1, v2, v0}, Lio/realm/ConcentrationResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 991
    :cond_37
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_38

    .line 992
    invoke-static {p1, v2, v0}, Lio/realm/SchulteTableConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 993
    :cond_38
    const-class v4, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_39

    .line 994
    invoke-static {p1, v2, v0}, Lio/realm/FlashWordResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 995
    :cond_39
    const-class v4, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3a

    .line 996
    invoke-static {p1, v2, v0}, Lio/realm/SchulteTableResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 997
    :cond_3a
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3b

    .line 998
    invoke-static {p1, v2, v0}, Lio/realm/RememberWordsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 999
    :cond_3b
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3c

    .line 1000
    invoke-static {p1, v2, v0}, Lio/realm/TrueColorsResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 1001
    :cond_3c
    const-class v4, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3d

    .line 1002
    invoke-static {p1, v2, v0}, Lio/realm/RememberWordsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 1003
    :cond_3d
    const-class v4, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3e

    .line 1004
    invoke-static {p1, v2, v0}, Lio/realm/TrueColorsConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 1005
    :cond_3e
    const-class v4, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3f

    .line 1006
    invoke-static {p1, v2, v0}, Lio/realm/WordBlockResultRealmProxy;->insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V

    goto/16 :goto_2

    .line 1008
    :cond_3f
    invoke-static {v1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v4

    throw v4
.end method

.method public newInstance(Ljava/lang/Class;Ljava/lang/Object;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)Lio/realm/RealmModel;
    .locals 7
    .param p2, "baseRealm"    # Ljava/lang/Object;
    .param p3, "row"    # Lio/realm/internal/Row;
    .param p4, "columnInfo"    # Lio/realm/internal/ColumnInfo;
    .param p5, "acceptDefaultValue"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E::",
            "Lio/realm/RealmModel;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;",
            "Ljava/lang/Object;",
            "Lio/realm/internal/Row;",
            "Lio/realm/internal/ColumnInfo;",
            "Z",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)TE;"
        }
    .end annotation

    .prologue
    .line 421
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    .local p6, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v2, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v2}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/BaseRealm$RealmObjectContext;

    .line 423
    .local v1, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    move-object v0, p2

    check-cast v0, Lio/realm/BaseRealm;

    move-object v2, v0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v1 .. v6}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 424
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 426
    const-class v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 427
    new-instance v2, Lio/realm/MathConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/MathConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 487
    :goto_0
    return-object v2

    .line 428
    :cond_0
    :try_start_1
    const-class v2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 429
    new-instance v2, Lio/realm/SpeedReadingResultRealmProxy;

    invoke-direct {v2}, Lio/realm/SpeedReadingResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto :goto_0

    .line 430
    :cond_1
    :try_start_2
    const-class v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 431
    new-instance v2, Lio/realm/LineOfSightConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/LineOfSightConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto :goto_0

    .line 432
    :cond_2
    :try_start_3
    const-class v2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 433
    new-instance v2, Lio/realm/CupTimerResultRealmProxy;

    invoke-direct {v2}, Lio/realm/CupTimerResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto :goto_0

    .line 434
    :cond_3
    :try_start_4
    const-class v2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 435
    new-instance v2, Lio/realm/EvenNumbersConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/EvenNumbersConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto :goto_0

    .line 436
    :cond_4
    :try_start_5
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 437
    new-instance v2, Lio/realm/WordPairsResultRealmProxy;

    invoke-direct {v2}, Lio/realm/WordPairsResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto :goto_0

    .line 438
    :cond_5
    :try_start_6
    const-class v2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 439
    new-instance v2, Lio/realm/GreenDotResultRealmProxy;

    invoke-direct {v2}, Lio/realm/GreenDotResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 440
    :cond_6
    :try_start_7
    const-class v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 441
    new-instance v2, Lio/realm/ConcentrationConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/ConcentrationConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 442
    :cond_7
    :try_start_8
    const-class v2, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 443
    new-instance v2, Lio/realm/PassCourseResultRealmProxy;

    invoke-direct {v2}, Lio/realm/PassCourseResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 444
    :cond_8
    :try_start_9
    const-class v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 445
    new-instance v2, Lio/realm/RememberNumberConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/RememberNumberConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 446
    :cond_9
    :try_start_a
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 447
    new-instance v2, Lio/realm/WordBlockConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/WordBlockConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 448
    :cond_a
    :try_start_b
    const-class v2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 449
    new-instance v2, Lio/realm/FlashWordsConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/FlashWordsConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 450
    :cond_b
    :try_start_c
    const-class v2, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 451
    new-instance v2, Lio/realm/GreenDotConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/GreenDotConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 452
    :cond_c
    :try_start_d
    const-class v2, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 453
    new-instance v2, Lio/realm/RememberNumberResultRealmProxy;

    invoke-direct {v2}, Lio/realm/RememberNumberResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 454
    :cond_d
    :try_start_e
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 455
    new-instance v2, Lio/realm/WordPairsConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/WordPairsConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 456
    :cond_e
    :try_start_f
    const-class v2, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 457
    new-instance v2, Lio/realm/CupTimerConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/CupTimerConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 458
    :cond_f
    :try_start_10
    const-class v2, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 459
    new-instance v2, Lio/realm/LineOfSightResultRealmProxy;

    invoke-direct {v2}, Lio/realm/LineOfSightResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 460
    :cond_10
    :try_start_11
    const-class v2, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 461
    new-instance v2, Lio/realm/EvenNumbersResultRealmProxy;

    invoke-direct {v2}, Lio/realm/EvenNumbersResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 462
    :cond_11
    :try_start_12
    const-class v2, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 463
    new-instance v2, Lio/realm/SpeedReadingConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/SpeedReadingConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 464
    :cond_12
    :try_start_13
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 465
    new-instance v2, Lio/realm/WordsColumnsResultRealmProxy;

    invoke-direct {v2}, Lio/realm/WordsColumnsResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 466
    :cond_13
    :try_start_14
    const-class v2, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 467
    new-instance v2, Lio/realm/MathResultRealmProxy;

    invoke-direct {v2}, Lio/realm/MathResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 468
    :cond_14
    :try_start_15
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 469
    new-instance v2, Lio/realm/WordsColumnsConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/WordsColumnsConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 470
    :cond_15
    :try_start_16
    const-class v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 471
    new-instance v2, Lio/realm/ConcentrationResultRealmProxy;

    invoke-direct {v2}, Lio/realm/ConcentrationResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 472
    :cond_16
    :try_start_17
    const-class v2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 473
    new-instance v2, Lio/realm/SchulteTableConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/SchulteTableConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 474
    :cond_17
    :try_start_18
    const-class v2, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 475
    new-instance v2, Lio/realm/FlashWordResultRealmProxy;

    invoke-direct {v2}, Lio/realm/FlashWordResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 476
    :cond_18
    :try_start_19
    const-class v2, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 477
    new-instance v2, Lio/realm/SchulteTableResultRealmProxy;

    invoke-direct {v2}, Lio/realm/SchulteTableResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 478
    :cond_19
    :try_start_1a
    const-class v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 479
    new-instance v2, Lio/realm/RememberWordsResultRealmProxy;

    invoke-direct {v2}, Lio/realm/RememberWordsResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 480
    :cond_1a
    :try_start_1b
    const-class v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 481
    new-instance v2, Lio/realm/TrueColorsResultRealmProxy;

    invoke-direct {v2}, Lio/realm/TrueColorsResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1b
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 482
    :cond_1b
    :try_start_1c
    const-class v2, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 483
    new-instance v2, Lio/realm/RememberWordsConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/RememberWordsConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 484
    :cond_1c
    :try_start_1d
    const-class v2, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 485
    new-instance v2, Lio/realm/TrueColorsConfigRealmProxy;

    invoke-direct {v2}, Lio/realm/TrueColorsConfigRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1d
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 486
    :cond_1d
    :try_start_1e
    const-class v2, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 487
    new-instance v2, Lio/realm/WordBlockResultRealmProxy;

    invoke-direct {v2}, Lio/realm/WordBlockResultRealmProxy;-><init>()V

    invoke-virtual {p1, v2}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/RealmModel;
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    .line 492
    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    goto/16 :goto_0

    .line 489
    :cond_1e
    :try_start_1f
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v2

    throw v2
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_0

    .line 492
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v2
.end method

.method public transformerApplied()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public validateTable(Ljava/lang/Class;Lio/realm/internal/SharedRealm;Z)Lio/realm/internal/ColumnInfo;
    .locals 1
    .param p2, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p3, "allowExtraColumns"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Lio/realm/internal/SharedRealm;",
            "Z)",
            "Lio/realm/internal/ColumnInfo;"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<+Lio/realm/RealmModel;>;"
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->checkClass(Ljava/lang/Class;)V

    .line 210
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-static {p2, p3}, Lio/realm/MathConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/MathConfigRealmProxy$MathConfigColumnInfo;

    move-result-object v0

    .line 271
    :goto_0
    return-object v0

    .line 212
    :cond_0
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 213
    invoke-static {p2, p3}, Lio/realm/SpeedReadingResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/SpeedReadingResultRealmProxy$SpeedReadingResultColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_1
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    invoke-static {p2, p3}, Lio/realm/LineOfSightConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/LineOfSightConfigRealmProxy$LineOfSightConfigColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_2
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 217
    invoke-static {p2, p3}, Lio/realm/CupTimerResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/CupTimerResultRealmProxy$CupTimerResultColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 218
    :cond_3
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 219
    invoke-static {p2, p3}, Lio/realm/EvenNumbersConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/EvenNumbersConfigRealmProxy$EvenNumbersConfigColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 220
    :cond_4
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 221
    invoke-static {p2, p3}, Lio/realm/WordPairsResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordPairsResultRealmProxy$WordPairsResultColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 222
    :cond_5
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 223
    invoke-static {p2, p3}, Lio/realm/GreenDotResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/GreenDotResultRealmProxy$GreenDotResultColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 224
    :cond_6
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 225
    invoke-static {p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/ConcentrationConfigRealmProxy$ConcentrationConfigColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 226
    :cond_7
    const-class v0, Lcom/safonov/speedreading/training/fragment/passcource/repository/etity/PassCourseResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 227
    invoke-static {p2, p3}, Lio/realm/PassCourseResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/PassCourseResultRealmProxy$PassCourseResultColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 228
    :cond_8
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 229
    invoke-static {p2, p3}, Lio/realm/RememberNumberConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    move-result-object v0

    goto :goto_0

    .line 230
    :cond_9
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 231
    invoke-static {p2, p3}, Lio/realm/WordBlockConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordBlockConfigRealmProxy$WordBlockConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 232
    :cond_a
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 233
    invoke-static {p2, p3}, Lio/realm/FlashWordsConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/FlashWordsConfigRealmProxy$FlashWordsConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 234
    :cond_b
    const-class v0, Lcom/safonov/speedreading/training/fragment/greendot/repository/entity/GreenDotConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 235
    invoke-static {p2, p3}, Lio/realm/GreenDotConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/GreenDotConfigRealmProxy$GreenDotConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 236
    :cond_c
    const-class v0, Lcom/safonov/speedreading/training/fragment/remembernumber/repository/entity/RememberNumberResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 237
    invoke-static {p2, p3}, Lio/realm/RememberNumberResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberNumberResultRealmProxy$RememberNumberResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 238
    :cond_d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordpairs/repository/entity/WordPairsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 239
    invoke-static {p2, p3}, Lio/realm/WordPairsConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordPairsConfigRealmProxy$WordPairsConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 240
    :cond_e
    const-class v0, Lcom/safonov/speedreading/training/fragment/cuptimer/repository/entity/CupTimerConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 241
    invoke-static {p2, p3}, Lio/realm/CupTimerConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/CupTimerConfigRealmProxy$CupTimerConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 242
    :cond_f
    const-class v0, Lcom/safonov/speedreading/training/fragment/lineofsight/repository/entity/LineOfSightResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 243
    invoke-static {p2, p3}, Lio/realm/LineOfSightResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/LineOfSightResultRealmProxy$LineOfSightResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 244
    :cond_10
    const-class v0, Lcom/safonov/speedreading/training/fragment/evennumbers/repository/entity/EvenNumbersResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 245
    invoke-static {p2, p3}, Lio/realm/EvenNumbersResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/EvenNumbersResultRealmProxy$EvenNumbersResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 246
    :cond_11
    const-class v0, Lcom/safonov/speedreading/training/fragment/speedreading/repository/entity/SpeedReadingConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 247
    invoke-static {p2, p3}, Lio/realm/SpeedReadingConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/SpeedReadingConfigRealmProxy$SpeedReadingConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 248
    :cond_12
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 249
    invoke-static {p2, p3}, Lio/realm/WordsColumnsResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordsColumnsResultRealmProxy$WordsColumnsResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 250
    :cond_13
    const-class v0, Lcom/safonov/speedreading/training/fragment/math/repository/entity/MathResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 251
    invoke-static {p2, p3}, Lio/realm/MathResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/MathResultRealmProxy$MathResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 252
    :cond_14
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordscolumns/repository/entity/WordsColumnsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 253
    invoke-static {p2, p3}, Lio/realm/WordsColumnsConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordsColumnsConfigRealmProxy$WordsColumnsConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 254
    :cond_15
    const-class v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 255
    invoke-static {p2, p3}, Lio/realm/ConcentrationResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 256
    :cond_16
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 257
    invoke-static {p2, p3}, Lio/realm/SchulteTableConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/SchulteTableConfigRealmProxy$SchulteTableConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 258
    :cond_17
    const-class v0, Lcom/safonov/speedreading/training/fragment/flashword/repository/entity/FlashWordResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 259
    invoke-static {p2, p3}, Lio/realm/FlashWordResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/FlashWordResultRealmProxy$FlashWordResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 260
    :cond_18
    const-class v0, Lcom/safonov/speedreading/training/fragment/schultetable/repository/entity/SchulteTableResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 261
    invoke-static {p2, p3}, Lio/realm/SchulteTableResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/SchulteTableResultRealmProxy$SchulteTableResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 262
    :cond_19
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 263
    invoke-static {p2, p3}, Lio/realm/RememberWordsResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberWordsResultRealmProxy$RememberWordsResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 264
    :cond_1a
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 265
    invoke-static {p2, p3}, Lio/realm/TrueColorsResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/TrueColorsResultRealmProxy$TrueColorsResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 266
    :cond_1b
    const-class v0, Lcom/safonov/speedreading/training/fragment/rememberwords/repository/entity/RememberWordsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 267
    invoke-static {p2, p3}, Lio/realm/RememberWordsConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/RememberWordsConfigRealmProxy$RememberWordsConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 268
    :cond_1c
    const-class v0, Lcom/safonov/speedreading/training/fragment/truecolors/repository/entity/TrueColorsConfig;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 269
    invoke-static {p2, p3}, Lio/realm/TrueColorsConfigRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/TrueColorsConfigRealmProxy$TrueColorsConfigColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 270
    :cond_1d
    const-class v0, Lcom/safonov/speedreading/training/fragment/wordsblock/repository/entity/WordBlockResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 271
    invoke-static {p2, p3}, Lio/realm/WordBlockResultRealmProxy;->validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/WordBlockResultRealmProxy$WordBlockResultColumnInfo;

    move-result-object v0

    goto/16 :goto_0

    .line 273
    :cond_1e
    invoke-static {p1}, Lio/realm/DefaultRealmModuleMediator;->getMissingProxyClassException(Ljava/lang/Class;)Lio/realm/exceptions/RealmException;

    move-result-object v0

    throw v0
.end method
