.class public Lio/realm/ConcentrationResultRealmProxy;
.super Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
.source "ConcentrationResultRealmProxy.java"

# interfaces
.implements Lio/realm/internal/RealmObjectProxy;
.implements Lio/realm/ConcentrationResultRealmProxyInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    }
.end annotation


# static fields
.field private static final FIELD_NAMES:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

.field private proxyState:Lio/realm/ProxyState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/realm/ProxyState",
            "<",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 74
    .local v0, "fieldNames":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-string v1, "id"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 75
    const-string v1, "score"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    const-string v1, "config"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    sput-object v1, Lio/realm/ConcentrationResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    .line 78
    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;-><init>()V

    .line 81
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->setConstructionFinished()V

    .line 82
    return-void
.end method

.method public static copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 8
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "newObject"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;"
        }
    .end annotation

    .prologue
    .line 428
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    .line 429
    .local v1, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v1, :cond_0

    .line 430
    check-cast v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 448
    .end local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :goto_0
    return-object v1

    .line 433
    .restart local v1    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    .restart local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_0
    const-class v5, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object v4, p1

    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v4}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    invoke-virtual {p0, v5, v4, v6, v7}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v3

    check-cast v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .local v3, "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    move-object v4, v3

    .line 434
    check-cast v4, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {p3, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v3

    .line 435
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    move-object v5, p1

    check-cast v5, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$score(I)V

    .line 437
    check-cast p1, Lio/realm/ConcentrationResultRealmProxyInterface;

    .end local p1    # "newObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface {p1}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v2

    .line 438
    .local v2, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v2, :cond_2

    .line 439
    invoke-interface {p3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 440
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v0, :cond_1

    move-object v4, v3

    .line 441
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :goto_1
    move-object v1, v3

    .line 448
    goto :goto_0

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_1
    move-object v4, v3

    .line 443
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-static {p0, v2, p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v5

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_1

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_2
    move-object v4, v3

    .line 446
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_1
.end method

.method public static copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 19
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Z",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;"
        }
    .end annotation

    .prologue
    .line 389
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_0

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    iget-wide v6, v5, Lio/realm/BaseRealm;->threadId:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lio/realm/Realm;->threadId:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 390
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "Objects which belong to Realm instances in other threads cannot be copied into this Realm instance."

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 392
    :cond_0
    move-object/from16 v0, p1

    instance-of v5, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    if-eqz v5, :cond_1

    move-object/from16 v5, p1

    check-cast v5, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v5}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v5

    invoke-virtual {v5}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 422
    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :goto_0
    return-object p1

    .line 395
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_1
    sget-object v5, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v5}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/realm/BaseRealm$RealmObjectContext;

    .line 396
    .local v4, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lio/realm/internal/RealmObjectProxy;

    .line 397
    .local v10, "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    if-eqz v10, :cond_2

    .line 398
    check-cast v10, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .end local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    move-object/from16 p1, v10

    goto :goto_0

    .line 400
    .restart local v10    # "cachedRealmObject":Lio/realm/internal/RealmObjectProxy;
    :cond_2
    const/4 v14, 0x0

    .line 401
    .local v14, "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    move/from16 v11, p2

    .line 402
    .local v11, "canUpdate":Z
    if-eqz v11, :cond_3

    .line 403
    const-class v5, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v18

    .line 404
    .local v18, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v18 .. v18}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .local v12, "pkColumnIndex":J
    move-object/from16 v5, p1

    .line 405
    check-cast v5, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v5

    int-to-long v6, v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v12, v13, v6, v7}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v16

    .line 406
    .local v16, "rowIndex":J
    const-wide/16 v6, -0x1

    cmp-long v5, v16, v6

    if-eqz v5, :cond_4

    .line 408
    :try_start_0
    move-object/from16 v0, v18

    move-wide/from16 v1, v16

    invoke-virtual {v0, v1, v2}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v5, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v7, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v5, v7}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v9

    move-object/from16 v5, p0

    invoke-virtual/range {v4 .. v9}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 409
    new-instance v15, Lio/realm/ConcentrationResultRealmProxy;

    invoke-direct {v15}, Lio/realm/ConcentrationResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 410
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .local v15, "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :try_start_1
    move-object v0, v15

    check-cast v0, Lio/realm/internal/RealmObjectProxy;

    move-object v5, v0

    move-object/from16 v0, p3

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 412
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    move-object v14, v15

    .line 419
    .end local v12    # "pkColumnIndex":J
    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_3
    :goto_1
    if-eqz v11, :cond_5

    .line 420
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v14, v1, v2}, Lio/realm/ConcentrationResultRealmProxy;->update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object p1

    goto :goto_0

    .line 412
    .restart local v12    # "pkColumnIndex":J
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_0
    move-exception v5

    :goto_2
    invoke-virtual {v4}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v5

    .line 415
    :cond_4
    const/4 v11, 0x0

    goto :goto_1

    .line 422
    .end local v12    # "pkColumnIndex":J
    .end local v16    # "rowIndex":J
    .end local v18    # "table":Lio/realm/internal/Table;
    :cond_5
    invoke-static/range {p0 .. p3}, Lio/realm/ConcentrationResultRealmProxy;->copy(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-result-object p1

    goto/16 :goto_0

    .line 412
    .end local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .restart local v12    # "pkColumnIndex":J
    .restart local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .restart local v16    # "rowIndex":J
    .restart local v18    # "table":Lio/realm/internal/Table;
    :catchall_1
    move-exception v5

    move-object v14, v15

    .end local v15    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .restart local v14    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    goto :goto_2
.end method

.method public static createDetachedCopy(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 5
    .param p0, "realmObject"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .param p1, "currentDepth"    # I
    .param p2, "maxDepth"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "II",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy$CacheData",
            "<",
            "Lio/realm/RealmModel;",
            ">;>;)",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;"
        }
    .end annotation

    .prologue
    .line 593
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;>;"
    if-gt p1, p2, :cond_0

    if-nez p0, :cond_1

    .line 594
    :cond_0
    const/4 v2, 0x0

    .line 615
    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :goto_0
    return-object v2

    .line 596
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_1
    invoke-interface {p3, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/internal/RealmObjectProxy$CacheData;

    .line 598
    .local v0, "cachedObject":Lio/realm/internal/RealmObjectProxy$CacheData;, "Lio/realm/internal/RealmObjectProxy$CacheData<Lio/realm/RealmModel;>;"
    if-eqz v0, :cond_3

    .line 600
    iget v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    if-lt p1, v2, :cond_2

    .line 601
    iget-object v2, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v2, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    goto :goto_0

    .line 603
    :cond_2
    iget-object v1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->object:Lio/realm/RealmModel;

    check-cast v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 604
    .local v1, "unmanagedObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    iput p1, v0, Lio/realm/internal/RealmObjectProxy$CacheData;->minDepth:I

    :goto_1
    move-object v2, v1

    .line 610
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$id(I)V

    move-object v2, v1

    .line 611
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    move-object v3, p0

    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$score(I)V

    move-object v2, v1

    .line 614
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    check-cast p0, Lio/realm/ConcentrationResultRealmProxyInterface;

    .end local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface {p0}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v3

    add-int/lit8 v4, p1, 0x1

    invoke-static {v3, v4, p2, p3}, Lio/realm/ConcentrationConfigRealmProxy;->createDetachedCopy(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;IILjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    move-object v2, v1

    .line 615
    goto :goto_0

    .line 607
    .end local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .restart local p0    # "realmObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_3
    new-instance v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-direct {v1}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;-><init>()V

    .line 608
    .restart local v1    # "unmanagedObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    new-instance v2, Lio/realm/internal/RealmObjectProxy$CacheData;

    invoke-direct {v2, p1, v1}, Lio/realm/internal/RealmObjectProxy$CacheData;-><init>(ILio/realm/RealmModel;)V

    invoke-interface {p3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 16
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "json"    # Lorg/json/JSONObject;
    .param p2, "update"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 293
    new-instance v9, Ljava/util/ArrayList;

    const/4 v3, 0x1

    invoke-direct {v9, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 294
    .local v9, "excludeFields":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v10, 0x0

    .line 295
    .local v10, "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    if-eqz p2, :cond_1

    .line 296
    const-class v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v11

    .line 297
    .local v11, "table":Lio/realm/internal/Table;
    invoke-virtual {v11}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    .line 298
    .local v12, "pkColumnIndex":J
    const-wide/16 v14, -0x1

    .line 299
    .local v14, "rowIndex":J
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 300
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v11, v12, v13, v4, v5}, Lio/realm/internal/Table;->findFirstLong(JJ)J

    move-result-wide v14

    .line 302
    :cond_0
    const-wide/16 v4, -0x1

    cmp-long v3, v14, v4

    if-eqz v3, :cond_1

    .line 303
    sget-object v3, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v3}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/realm/BaseRealm$RealmObjectContext;

    .line 305
    .local v2, "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    :try_start_0
    invoke-virtual {v11, v14, v15}, Lio/realm/internal/Table;->getUncheckedRow(J)Lio/realm/internal/UncheckedRow;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v5, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v3, v5}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v7

    move-object/from16 v3, p0

    invoke-virtual/range {v2 .. v7}, Lio/realm/BaseRealm$RealmObjectContext;->set(Lio/realm/BaseRealm;Lio/realm/internal/Row;Lio/realm/internal/ColumnInfo;ZLjava/util/List;)V

    .line 306
    new-instance v10, Lio/realm/ConcentrationResultRealmProxy;

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-direct {v10}, Lio/realm/ConcentrationResultRealmProxy;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    .line 312
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    :cond_1
    if-nez v10, :cond_3

    .line 313
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 314
    const-string v3, "config"

    invoke-interface {v9, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    :cond_2
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 317
    const-string v3, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 318
    const-class v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    check-cast v10, Lio/realm/ConcentrationResultRealmProxy;

    .line 326
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_3
    :goto_0
    const-string v3, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 327
    const-string v3, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 328
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 308
    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .restart local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .restart local v11    # "table":Lio/realm/internal/Table;
    .restart local v12    # "pkColumnIndex":J
    .restart local v14    # "rowIndex":J
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Lio/realm/BaseRealm$RealmObjectContext;->clear()V

    throw v3

    .line 320
    .end local v2    # "objectContext":Lio/realm/BaseRealm$RealmObjectContext;
    .end local v11    # "table":Lio/realm/internal/Table;
    .end local v12    # "pkColumnIndex":J
    .end local v14    # "rowIndex":J
    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_4
    const-class v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    const-string v4, "id"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v4, v5, v9}, Lio/realm/Realm;->createObjectInternal(Ljava/lang/Class;Ljava/lang/Object;ZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v10

    .end local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    check-cast v10, Lio/realm/ConcentrationResultRealmProxy;

    .restart local v10    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    goto :goto_0

    .line 323
    :cond_5
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_6
    move-object v3, v10

    .line 330
    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    const-string v4, "score"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$score(I)V

    .line 333
    :cond_7
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 334
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_9

    move-object v3, v10

    .line 335
    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    .line 341
    :cond_8
    :goto_1
    return-object v10

    .line 337
    :cond_9
    const-string v3, "config"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-static {v0, v3, v1}, Lio/realm/ConcentrationConfigRealmProxy;->createOrUpdateUsingJsonObject(Lio/realm/Realm;Lorg/json/JSONObject;Z)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v8

    .local v8, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    move-object v3, v10

    .line 338
    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v3, v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_1
.end method

.method public static createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;
    .locals 11
    .param p0, "realmSchema"    # Lio/realm/RealmSchema;

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 184
    const-string v0, "ConcentrationResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 185
    const-string v0, "ConcentrationResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->create(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    .line 186
    .local v10, "realmObjectSchema":Lio/realm/RealmObjectSchema;
    new-instance v0, Lio/realm/Property;

    const-string v1, "id"

    sget-object v2, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v4, v3

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 187
    new-instance v4, Lio/realm/Property;

    const-string v5, "score"

    sget-object v6, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    move v8, v7

    move v9, v3

    invoke-direct/range {v4 .. v9}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;ZZZ)V

    invoke-virtual {v10, v4}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 188
    const-string v0, "ConcentrationConfig"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 189
    invoke-static {p0}, Lio/realm/ConcentrationConfigRealmProxy;->createRealmObjectSchema(Lio/realm/RealmSchema;)Lio/realm/RealmObjectSchema;

    .line 191
    :cond_0
    new-instance v0, Lio/realm/Property;

    const-string v1, "config"

    sget-object v2, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v3, "ConcentrationConfig"

    invoke-virtual {p0, v3}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lio/realm/Property;-><init>(Ljava/lang/String;Lio/realm/RealmFieldType;Lio/realm/RealmObjectSchema;)V

    invoke-virtual {v10, v0}, Lio/realm/RealmObjectSchema;->add(Lio/realm/Property;)Lio/realm/RealmObjectSchema;

    .line 194
    .end local v10    # "realmObjectSchema":Lio/realm/RealmObjectSchema;
    :goto_0
    return-object v10

    :cond_1
    const-string v0, "ConcentrationResult"

    invoke-virtual {p0, v0}, Lio/realm/RealmSchema;->get(Ljava/lang/String;)Lio/realm/RealmObjectSchema;

    move-result-object v10

    goto :goto_0
.end method

.method public static createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 6
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "reader"    # Landroid/util/JsonReader;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 348
    const/4 v1, 0x0

    .line 349
    .local v1, "jsonHasPrimaryKey":Z
    new-instance v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-direct {v3}, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;-><init>()V

    .line 350
    .local v3, "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-virtual {p1}, Landroid/util/JsonReader;->beginObject()V

    .line 351
    :goto_0
    invoke-virtual {p1}, Landroid/util/JsonReader;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 352
    invoke-virtual {p1}, Landroid/util/JsonReader;->nextName()Ljava/lang/String;

    move-result-object v2

    .line 353
    .local v2, "name":Ljava/lang/String;
    const-string v4, "id"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 354
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_0

    .line 355
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 356
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'id\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_0
    move-object v4, v3

    .line 358
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$id(I)V

    .line 360
    const/4 v1, 0x1

    goto :goto_0

    .line 361
    :cond_1
    const-string v4, "score"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 362
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_2

    .line 363
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    .line 364
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "Trying to set non-nullable field \'score\' to null."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_2
    move-object v4, v3

    .line 366
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-virtual {p1}, Landroid/util/JsonReader;->nextInt()I

    move-result v5

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$score(I)V

    goto :goto_0

    .line 368
    :cond_3
    const-string v4, "config"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 369
    invoke-virtual {p1}, Landroid/util/JsonReader;->peek()Landroid/util/JsonToken;

    move-result-object v4

    sget-object v5, Landroid/util/JsonToken;->NULL:Landroid/util/JsonToken;

    if-ne v4, v5, :cond_4

    .line 370
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    move-object v4, v3

    .line 371
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    const/4 v5, 0x0

    invoke-interface {v4, v5}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_0

    .line 373
    :cond_4
    invoke-static {p0, p1}, Lio/realm/ConcentrationConfigRealmProxy;->createUsingJsonStream(Lio/realm/Realm;Landroid/util/JsonReader;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v0

    .local v0, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    move-object v4, v3

    .line 374
    check-cast v4, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v4, v0}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_0

    .line 377
    .end local v0    # "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_5
    invoke-virtual {p1}, Landroid/util/JsonReader;->skipValue()V

    goto :goto_0

    .line 380
    .end local v2    # "name":Ljava/lang/String;
    :cond_6
    invoke-virtual {p1}, Landroid/util/JsonReader;->endObject()V

    .line 381
    if-nez v1, :cond_7

    .line 382
    new-instance v4, Ljava/lang/IllegalArgumentException;

    const-string v5, "JSON object doesn\'t have the primary key field \'id\'."

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 384
    :cond_7
    invoke-virtual {p0, v3}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object v3

    .end local v3    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    check-cast v3, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 385
    .restart local v3    # "obj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    return-object v3
.end method

.method public static getFieldNames()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 287
    sget-object v0, Lio/realm/ConcentrationResultRealmProxy;->FIELD_NAMES:Ljava/util/List;

    return-object v0
.end method

.method public static getTableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 283
    const-string v0, "class_ConcentrationResult"

    return-object v0
.end method

.method public static initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;
    .locals 4
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;

    .prologue
    const/4 v3, 0x0

    .line 198
    const-string v1, "class_ConcentrationResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 199
    const-string v1, "class_ConcentrationResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    .line 200
    .local v0, "table":Lio/realm/internal/Table;
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "id"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 201
    sget-object v1, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    const-string v2, "score"

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumn(Lio/realm/RealmFieldType;Ljava/lang/String;Z)J

    .line 202
    const-string v1, "class_ConcentrationConfig"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 203
    invoke-static {p0}, Lio/realm/ConcentrationConfigRealmProxy;->initTable(Lio/realm/internal/SharedRealm;)Lio/realm/internal/Table;

    .line 205
    :cond_0
    sget-object v1, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    const-string v2, "config"

    const-string v3, "class_ConcentrationConfig"

    invoke-virtual {p0, v3}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lio/realm/internal/Table;->addColumnLink(Lio/realm/RealmFieldType;Ljava/lang/String;Lio/realm/internal/Table;)J

    .line 206
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lio/realm/internal/Table;->addSearchIndex(J)V

    .line 207
    const-string v1, "id"

    invoke-virtual {v0, v1}, Lio/realm/internal/Table;->setPrimaryKey(Ljava/lang/String;)V

    .line 210
    .end local v0    # "table":Lio/realm/internal/Table;
    :goto_0
    return-object v0

    :cond_1
    const-string v1, "class_ConcentrationResult"

    invoke-virtual {p0, v1}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v0

    goto :goto_0
.end method

.method public static insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 453
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 454
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 481
    :cond_0
    :goto_0
    return-wide v12

    .line 456
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_1
    const-class v8, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 457
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 458
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    .line 459
    .local v18, "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 460
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 461
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 462
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_2

    move-object/from16 v8, p1

    .line 463
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 465
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, p1

    .line 466
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 470
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 471
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 473
    check-cast p1, Lio/realm/ConcentrationResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v19

    .line 474
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v19, :cond_0

    .line 475
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 476
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 477
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 479
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 468
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_4
    invoke-static/range {v20 .. v20}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public static insert(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 485
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 486
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 487
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    .line 488
    .local v18, "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 489
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 490
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 491
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 492
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 493
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 494
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 497
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 498
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 499
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 500
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 502
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_4

    move-object/from16 v8, v20

    .line 503
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 507
    :goto_1
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 508
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 510
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v19

    .line 511
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v19, :cond_0

    .line 512
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 513
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 514
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy;->insert(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 516
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-object/from16 v9, v22

    invoke-virtual/range {v9 .. v16}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto/16 :goto_0

    .line 505
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_4
    invoke-static/range {v21 .. v21}, Lio/realm/internal/Table;->throwDuplicatePrimaryKeyException(Ljava/lang/Object;)V

    goto :goto_1

    .line 520
    .end local v12    # "rowIndex":J
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_5
    return-void
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)J
    .locals 22
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "object"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 523
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    move-object/from16 v0, p1

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_0

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 524
    check-cast p1, Lio/realm/internal/RealmObjectProxy;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v12

    .line 551
    :goto_0
    return-wide v12

    .line 526
    .restart local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_0
    const-class v8, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v21

    .line 527
    .local v21, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 528
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    .line 529
    .local v18, "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    invoke-virtual/range {v21 .. v21}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 530
    .local v6, "pkColumnIndex":J
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, p1

    .line 531
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    .line 532
    .local v20, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v20, :cond_1

    move-object/from16 v8, p1

    .line 533
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 535
    :cond_1
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_2

    move-object/from16 v8, p1

    .line 536
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 538
    :cond_2
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 539
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, p1

    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    .line 541
    check-cast p1, Lio/realm/ConcentrationResultRealmProxyInterface;

    .end local p1    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface/range {p1 .. p1}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v19

    .line 542
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v19, :cond_4

    .line 543
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 544
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_3

    .line 545
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 547
    :cond_3
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 549
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0
.end method

.method public static insertOrUpdate(Lio/realm/Realm;Ljava/util/Iterator;Ljava/util/Map;)V
    .locals 23
    .param p0, "realm"    # Lio/realm/Realm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Ljava/util/Iterator",
            "<+",
            "Lio/realm/RealmModel;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 555
    .local p1, "objects":Ljava/util/Iterator;, "Ljava/util/Iterator<+Lio/realm/RealmModel;>;"
    .local p2, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Ljava/lang/Long;>;"
    const-class v8, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lio/realm/Realm;->getTable(Ljava/lang/Class;)Lio/realm/internal/Table;

    move-result-object v22

    .line 556
    .local v22, "table":Lio/realm/internal/Table;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getNativeTablePointer()J

    move-result-wide v4

    .line 557
    .local v4, "tableNativePtr":J
    move-object/from16 v0, p0

    iget-object v8, v0, Lio/realm/Realm;->schema:Lio/realm/RealmSchema;

    const-class v9, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    invoke-virtual {v8, v9}, Lio/realm/RealmSchema;->getColumnInfo(Ljava/lang/Class;)Lio/realm/internal/ColumnInfo;

    move-result-object v18

    check-cast v18, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    .line 558
    .local v18, "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    invoke-virtual/range {v22 .. v22}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v6

    .line 559
    .local v6, "pkColumnIndex":J
    const/16 v20, 0x0

    .line 560
    .local v20, "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    :cond_0
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 561
    invoke-interface/range {p1 .. p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v20

    .end local v20    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    check-cast v20, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;

    .line 562
    .restart local v20    # "object":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 563
    move-object/from16 v0, v20

    instance-of v8, v0, Lio/realm/internal/RealmObjectProxy;

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {p0 .. p0}, Lio/realm/Realm;->getPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    move-object/from16 v8, v20

    .line 564
    check-cast v8, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v8}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v8

    invoke-virtual {v8}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v8

    invoke-interface {v8}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 567
    :cond_1
    const-wide/16 v12, -0x1

    .local v12, "rowIndex":J
    move-object/from16 v8, v20

    .line 568
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    .line 569
    .local v21, "primaryKeyValue":Ljava/lang/Integer;
    if-eqz v21, :cond_2

    move-object/from16 v8, v20

    .line 570
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v4 .. v9}, Lio/realm/internal/Table;->nativeFindFirstInt(JJJ)J

    move-result-wide v12

    .line 572
    :cond_2
    const-wide/16 v8, -0x1

    cmp-long v8, v12, v8

    if-nez v8, :cond_3

    move-object/from16 v8, v20

    .line 573
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$id()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v8, v9}, Lio/realm/internal/Table;->addEmptyRowWithPrimaryKey(Ljava/lang/Object;Z)J

    move-result-wide v12

    .line 575
    :cond_3
    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 576
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    move-object/from16 v8, v20

    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v8

    int-to-long v14, v8

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLong(JJJJZ)V

    move-object/from16 v8, v20

    .line 578
    check-cast v8, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v8}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v19

    .line 579
    .local v19, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v19, :cond_5

    .line 580
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Long;

    .line 581
    .local v17, "cacheconfig":Ljava/lang/Long;
    if-nez v17, :cond_4

    .line 582
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lio/realm/ConcentrationConfigRealmProxy;->insertOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;Ljava/util/Map;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    .line 584
    :cond_4
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    const/16 v16, 0x0

    move-wide v8, v4

    invoke-static/range {v8 .. v16}, Lio/realm/internal/Table;->nativeSetLink(JJJJZ)V

    goto/16 :goto_0

    .line 586
    .end local v17    # "cacheconfig":Ljava/lang/Long;
    :cond_5
    move-object/from16 v0, v18

    iget-wide v10, v0, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    move-wide v8, v4

    invoke-static/range {v8 .. v13}, Lio/realm/internal/Table;->nativeNullifyLink(JJJ)V

    goto/16 :goto_0

    .line 590
    .end local v12    # "rowIndex":J
    .end local v19    # "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .end local v21    # "primaryKeyValue":Ljava/lang/Integer;
    :cond_6
    return-void
.end method

.method static update(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;Ljava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .locals 4
    .param p0, "realm"    # Lio/realm/Realm;
    .param p1, "realmObject"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .param p2, "newObject"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/realm/Realm;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;",
            "Ljava/util/Map",
            "<",
            "Lio/realm/RealmModel;",
            "Lio/realm/internal/RealmObjectProxy;",
            ">;)",
            "Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;"
        }
    .end annotation

    .prologue
    .line 619
    .local p3, "cache":Ljava/util/Map;, "Ljava/util/Map<Lio/realm/RealmModel;Lio/realm/internal/RealmObjectProxy;>;"
    move-object v2, p1

    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    move-object v3, p2

    check-cast v3, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$score()I

    move-result v3

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$score(I)V

    .line 620
    check-cast p2, Lio/realm/ConcentrationResultRealmProxyInterface;

    .end local p2    # "newObject":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationResult;
    invoke-interface {p2}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v1

    .line 621
    .local v1, "configObj":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v1, :cond_1

    .line 622
    invoke-interface {p3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 623
    .local v0, "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    if-eqz v0, :cond_0

    move-object v2, p1

    .line 624
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    invoke-interface {v2, v0}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    .line 631
    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :goto_0
    return-object p1

    .restart local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_0
    move-object v2, p1

    .line 626
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    const/4 v3, 0x1

    invoke-static {p0, v1, v3, p3}, Lio/realm/ConcentrationConfigRealmProxy;->copyOrUpdate(Lio/realm/Realm;Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;ZLjava/util/Map;)Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v3

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_0

    .end local v0    # "cacheconfig":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_1
    move-object v2, p1

    .line 629
    check-cast v2, Lio/realm/ConcentrationResultRealmProxyInterface;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lio/realm/ConcentrationResultRealmProxyInterface;->realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V

    goto :goto_0
.end method

.method public static validateTable(Lio/realm/internal/SharedRealm;Z)Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    .locals 14
    .param p0, "sharedRealm"    # Lio/realm/internal/SharedRealm;
    .param p1, "allowExtraColumns"    # Z

    .prologue
    const-wide/16 v10, 0x3

    .line 214
    const-string v8, "class_ConcentrationResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 215
    const-string v8, "class_ConcentrationResult"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v6

    .line 216
    .local v6, "table":Lio/realm/internal/Table;
    invoke-virtual {v6}, Lio/realm/internal/Table;->getColumnCount()J

    move-result-wide v0

    .line 217
    .local v0, "columnCount":J
    cmp-long v8, v0, v10

    if-eqz v8, :cond_1

    .line 218
    cmp-long v8, v0, v10

    if-gez v8, :cond_0

    .line 219
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is less than expected - expected 3 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 221
    :cond_0
    if-eqz p1, :cond_2

    .line 222
    const-string v8, "Field count is more than expected - expected 3 but was %1$d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v8, v9}, Lio/realm/log/RealmLog;->debug(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 228
    .local v3, "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    const-wide/16 v4, 0x0

    .local v4, "i":J
    :goto_0
    cmp-long v8, v4, v0

    if-gez v8, :cond_3

    .line 229
    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v4, v5}, Lio/realm/internal/Table;->getColumnType(J)Lio/realm/RealmFieldType;

    move-result-object v9

    invoke-interface {v3, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 228
    const-wide/16 v8, 0x1

    add-long/2addr v4, v8

    goto :goto_0

    .line 224
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    :cond_2
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Field count is more than expected - expected 3 but was "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 232
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    :cond_3
    new-instance v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8, v6}, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;-><init>(Ljava/lang/String;Lio/realm/internal/Table;)V

    .line 234
    .local v2, "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    invoke-virtual {v6}, Lio/realm/internal/Table;->hasPrimaryKey()Z

    move-result v8

    if-nez v8, :cond_4

    .line 235
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Primary key not defined for field \'id\' in existing Realm file. @PrimaryKey was added."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 237
    :cond_4
    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v8

    iget-wide v10, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->idIndex:J

    cmp-long v8, v8, v10

    if-eqz v8, :cond_5

    .line 238
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Primary Key annotation definition was changed, from field "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v6}, Lio/realm/internal/Table;->getPrimaryKey()J

    move-result-wide v12

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getColumnName(J)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " to field id"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 242
    :cond_5
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    .line 243
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'id\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 245
    :cond_6
    const-string v8, "id"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_7

    .line 246
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'id\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 248
    :cond_7
    iget-wide v8, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_8

    iget-wide v8, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->idIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->findFirstNull(J)J

    move-result-wide v8

    const-wide/16 v10, -0x1

    cmp-long v8, v8, v10

    if-eqz v8, :cond_8

    .line 249
    new-instance v8, Ljava/lang/IllegalStateException;

    const-string v9, "Cannot migrate an object with null value in field \'id\'. Either maintain the same type for primary key field \'id\', or remove the object with null value before migration."

    invoke-direct {v8, v9}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 251
    :cond_8
    const-string v8, "id"

    invoke-virtual {v6, v8}, Lio/realm/internal/Table;->getColumnIndex(Ljava/lang/String;)J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->hasSearchIndex(J)Z

    move-result v8

    if-nez v8, :cond_9

    .line 252
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Index not defined for field \'id\' in existing Realm file. Either set @Index or migrate using io.realm.internal.Table.removeSearchIndex()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 254
    :cond_9
    const-string v8, "score"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 255
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'score\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 257
    :cond_a
    const-string v8, "score"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->INTEGER:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_b

    .line 258
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'int\' for field \'score\' in existing Realm file."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 260
    :cond_b
    iget-wide v8, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->isColumnNullable(J)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 261
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Field \'score\' does support null values in the existing Realm file. Use corresponding boxed type for field \'score\' or migrate using RealmObjectSchema.setNullable()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 263
    :cond_c
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_d

    .line 264
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing field \'config\' in existing Realm file. Either remove field or migrate using io.realm.internal.Table.addColumn()."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 266
    :cond_d
    const-string v8, "config"

    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    sget-object v9, Lio/realm/RealmFieldType;->OBJECT:Lio/realm/RealmFieldType;

    if-eq v8, v9, :cond_e

    .line 267
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Invalid type \'ConcentrationConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 269
    :cond_e
    const-string v8, "class_ConcentrationConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->hasTable(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_f

    .line 270
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Missing class \'class_ConcentrationConfig\' for field \'config\'"

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 272
    :cond_f
    const-string v8, "class_ConcentrationConfig"

    invoke-virtual {p0, v8}, Lio/realm/internal/SharedRealm;->getTable(Ljava/lang/String;)Lio/realm/internal/Table;

    move-result-object v7

    .line 273
    .local v7, "table_2":Lio/realm/internal/Table;
    iget-wide v8, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v8, v9}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v8

    invoke-virtual {v8, v7}, Lio/realm/internal/Table;->hasSameSchema(Lio/realm/internal/Table;)Z

    move-result v8

    if-nez v8, :cond_11

    .line 274
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Invalid RealmObject for field \'config\': \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-virtual {v6, v12, v13}, Lio/realm/internal/Table;->getLinkTarget(J)Lio/realm/internal/Table;

    move-result-object v11

    invoke-virtual {v11}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\' expected - was \'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\'"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 278
    .end local v0    # "columnCount":J
    .end local v2    # "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    .end local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .end local v4    # "i":J
    .end local v6    # "table":Lio/realm/internal/Table;
    .end local v7    # "table_2":Lio/realm/internal/Table;
    :cond_10
    new-instance v8, Lio/realm/exceptions/RealmMigrationNeededException;

    invoke-virtual {p0}, Lio/realm/internal/SharedRealm;->getPath()Ljava/lang/String;

    move-result-object v9

    const-string v10, "The \'ConcentrationResult\' class is missing from the schema for this Realm."

    invoke-direct {v8, v9, v10}, Lio/realm/exceptions/RealmMigrationNeededException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    throw v8

    .line 276
    .restart local v0    # "columnCount":J
    .restart local v2    # "columnInfo":Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;
    .restart local v3    # "columnTypes":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lio/realm/RealmFieldType;>;"
    .restart local v4    # "i":J
    .restart local v6    # "table":Lio/realm/internal/Table;
    .restart local v7    # "table_2":Lio/realm/internal/Table;
    :cond_11
    return-object v2
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 12
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 675
    if-ne p0, p1, :cond_1

    .line 689
    :cond_0
    :goto_0
    return v5

    .line 676
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    if-eq v7, v8, :cond_3

    :cond_2
    move v5, v6

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 677
    check-cast v0, Lio/realm/ConcentrationResultRealmProxy;

    .line 679
    .local v0, "aConcentrationResult":Lio/realm/ConcentrationResultRealmProxy;
    iget-object v7, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v3

    .line 680
    .local v3, "path":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 681
    .local v1, "otherPath":Ljava/lang/String;
    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_6

    :cond_4
    move v5, v6

    goto :goto_0

    :cond_5
    if-nez v1, :cond_4

    .line 683
    :cond_6
    iget-object v7, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 684
    .local v4, "tableName":Ljava/lang/String;
    iget-object v7, v0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v7

    invoke-virtual {v7}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v2

    .line 685
    .local v2, "otherTableName":Ljava/lang/String;
    if-eqz v4, :cond_8

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_9

    :cond_7
    move v5, v6

    goto :goto_0

    :cond_8
    if-nez v2, :cond_7

    .line 687
    :cond_9
    iget-object v7, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v8

    iget-object v7, v0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v7}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v7

    invoke-interface {v7}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    move v5, v6

    goto :goto_0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 662
    iget-object v6, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/BaseRealm;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 663
    .local v0, "realmName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/internal/Table;->getName()Ljava/lang/String;

    move-result-object v4

    .line 664
    .local v4, "tableName":Ljava/lang/String;
    iget-object v6, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v2

    .line 666
    .local v2, "rowIndex":J
    const/16 v1, 0x11

    .line 667
    .local v1, "result":I
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v6

    :goto_0
    add-int/lit16 v1, v6, 0x20f

    .line 668
    mul-int/lit8 v6, v1, 0x1f

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    :cond_0
    add-int v1, v6, v5

    .line 669
    mul-int/lit8 v5, v1, 0x1f

    const/16 v6, 0x20

    ushr-long v6, v2, v6

    xor-long/2addr v6, v2

    long-to-int v6, v6

    add-int v1, v5, v6

    .line 670
    return v1

    :cond_1
    move v6, v5

    .line 667
    goto :goto_0
.end method

.method public realm$injectObjectContext()V
    .locals 3

    .prologue
    .line 86
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    if-eqz v1, :cond_0

    .line 96
    :goto_0
    return-void

    .line 89
    :cond_0
    sget-object v1, Lio/realm/BaseRealm;->objectContext:Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;

    invoke-virtual {v1}, Lio/realm/BaseRealm$ThreadLocalRealmObjectContext;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/realm/BaseRealm$RealmObjectContext;

    .line 90
    .local v0, "context":Lio/realm/BaseRealm$RealmObjectContext;
    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getColumnInfo()Lio/realm/internal/ColumnInfo;

    move-result-object v1

    check-cast v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iput-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    .line 91
    new-instance v1, Lio/realm/ProxyState;

    invoke-direct {v1, p0}, Lio/realm/ProxyState;-><init>(Lio/realm/RealmModel;)V

    iput-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    .line 92
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRealm()Lio/realm/BaseRealm;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRealm$realm(Lio/realm/BaseRealm;)V

    .line 93
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getRow()Lio/realm/internal/Row;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setRow$realm(Lio/realm/internal/Row;)V

    .line 94
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getAcceptDefaultValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setAcceptDefaultValue$realm(Z)V

    .line 95
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/BaseRealm$RealmObjectContext;->getExcludeFields()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/realm/ProxyState;->setExcludeFields$realm(Ljava/util/List;)V

    goto :goto_0
.end method

.method public realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    .locals 6

    .prologue
    .line 135
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 136
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->isNullLink(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 139
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    const-class v1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v3, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v4, v3, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-interface {v2, v4, v5}, Lio/realm/internal/Row;->getLink(J)J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lio/realm/BaseRealm;->get(Ljava/lang/Class;JZLjava/util/List;)Lio/realm/RealmModel;

    move-result-object v0

    check-cast v0, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    goto :goto_0
.end method

.method public realmGet$id()I
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 101
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->idIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmGet$proxyState()Lio/realm/ProxyState;
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    return-object v0
.end method

.method public realmGet$score()I
    .locals 4

    .prologue
    .line 116
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 117
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->getLong(J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public realmSet$config(Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;)V
    .locals 9
    .param p1, "value"    # Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .prologue
    .line 143
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 144
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_1

    .line 181
    :cond_0
    :goto_0
    return-void

    .line 147
    :cond_1
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getExcludeFields$realm()Ljava/util/List;

    move-result-object v1

    const-string v2, "config"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    if-eqz p1, :cond_2

    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 151
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    check-cast v1, Lio/realm/Realm;

    invoke-virtual {v1, p1}, Lio/realm/Realm;->copyToRealm(Lio/realm/RealmModel;)Lio/realm/RealmModel;

    move-result-object p1

    .end local p1    # "value":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    check-cast p1, Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    .line 153
    .restart local p1    # "value":Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;
    :cond_2
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 154
    .local v0, "row":Lio/realm/internal/Row;
    if-nez p1, :cond_3

    .line 156
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-interface {v0, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto :goto_0

    .line 159
    :cond_3
    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 160
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-object v1, p1

    .line 162
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_5

    .line 163
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 165
    :cond_5
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    move-object v6, p1

    check-cast v6, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v6}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v6

    invoke-virtual {v6}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v6

    invoke-interface {v6}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLink(JJJZ)V

    goto :goto_0

    .line 169
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_6
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 170
    if-nez p1, :cond_7

    .line 171
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    invoke-interface {v1, v2, v3}, Lio/realm/internal/Row;->nullifyLink(J)V

    goto/16 :goto_0

    .line 174
    :cond_7
    invoke-static {p1}, Lio/realm/RealmObject;->isManaged(Lio/realm/RealmModel;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 175
    :cond_8
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' is not a valid managed object."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    move-object v1, p1

    .line 177
    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v2}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v2

    if-eq v1, v2, :cond_a

    .line 178
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "\'value\' belongs to a different Realm."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 180
    :cond_a
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v2

    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v4, v1, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->configIndex:J

    move-object v1, p1

    check-cast v1, Lio/realm/internal/RealmObjectProxy;

    invoke-interface {v1}, Lio/realm/internal/RealmObjectProxy;->realmGet$proxyState()Lio/realm/ProxyState;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    invoke-interface {v1}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v6

    invoke-interface {v2, v4, v5, v6, v7}, Lio/realm/internal/Row;->setLink(JJ)V

    goto/16 :goto_0
.end method

.method public realmSet$id(I)V
    .locals 2
    .param p1, "value"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v0}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v0

    invoke-virtual {v0}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 111
    new-instance v0, Lio/realm/exceptions/RealmException;

    const-string v1, "Primary key field \'id\' cannot be changed after object was created."

    invoke-direct {v0, v1}, Lio/realm/exceptions/RealmException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public realmSet$score(I)V
    .locals 9
    .param p1, "value"    # I

    .prologue
    .line 121
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->isUnderConstruction()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 122
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getAcceptDefaultValue$realm()Z

    move-result v1

    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 125
    :cond_0
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v0

    .line 126
    .local v0, "row":Lio/realm/internal/Row;
    invoke-interface {v0}, Lio/realm/internal/Row;->getTable()Lio/realm/internal/Table;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    invoke-interface {v0}, Lio/realm/internal/Row;->getIndex()J

    move-result-wide v4

    int-to-long v6, p1

    const/4 v8, 0x1

    invoke-virtual/range {v1 .. v8}, Lio/realm/internal/Table;->setLong(JJJZ)V

    goto :goto_0

    .line 130
    .end local v0    # "row":Lio/realm/internal/Row;
    :cond_1
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRealm$realm()Lio/realm/BaseRealm;

    move-result-object v1

    invoke-virtual {v1}, Lio/realm/BaseRealm;->checkIfValid()V

    .line 131
    iget-object v1, p0, Lio/realm/ConcentrationResultRealmProxy;->proxyState:Lio/realm/ProxyState;

    invoke-virtual {v1}, Lio/realm/ProxyState;->getRow$realm()Lio/realm/internal/Row;

    move-result-object v1

    iget-object v2, p0, Lio/realm/ConcentrationResultRealmProxy;->columnInfo:Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;

    iget-wide v2, v2, Lio/realm/ConcentrationResultRealmProxy$ConcentrationResultColumnInfo;->scoreIndex:J

    int-to-long v4, p1

    invoke-interface {v1, v2, v3, v4, v5}, Lio/realm/internal/Row;->setLong(JJ)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 636
    invoke-static {p0}, Lio/realm/RealmObject;->isValid(Lio/realm/RealmModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 637
    const-string v1, "Invalid object"

    .line 652
    :goto_0
    return-object v1

    .line 639
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConcentrationResult = ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 640
    .local v0, "stringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "{id:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 641
    invoke-virtual {p0}, Lio/realm/ConcentrationResultRealmProxy;->realmGet$id()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 642
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 643
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 644
    const-string v1, "{score:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 645
    invoke-virtual {p0}, Lio/realm/ConcentrationResultRealmProxy;->realmGet$score()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 646
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 647
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 648
    const-string v1, "{config:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 649
    invoke-virtual {p0}, Lio/realm/ConcentrationResultRealmProxy;->realmGet$config()Lcom/safonov/speedreading/training/fragment/concentration/repository/entity/ConcentrationConfig;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string v1, "ConcentrationConfig"

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 650
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 651
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 652
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 649
    :cond_1
    const-string v1, "null"

    goto :goto_1
.end method
