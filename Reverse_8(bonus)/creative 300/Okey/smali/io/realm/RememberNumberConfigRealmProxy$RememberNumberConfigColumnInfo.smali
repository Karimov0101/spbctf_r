.class final Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
.super Lio/realm/internal/ColumnInfo;
.source "RememberNumberConfigRealmProxy.java"

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/realm/RememberNumberConfigRealmProxy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "RememberNumberConfigColumnInfo"
.end annotation


# instance fields
.field public answersToComplexityDownIndex:J

.field public answersToComplexityUpIndex:J

.field public complexityIndex:J

.field public idIndex:J

.field public maxComplexityIndex:J

.field public minComplexityIndex:J

.field public trainingShowCountIndex:J


# direct methods
.method constructor <init>(Ljava/lang/String;Lio/realm/internal/Table;)V
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "table"    # Lio/realm/internal/Table;

    .prologue
    .line 45
    invoke-direct {p0}, Lio/realm/internal/ColumnInfo;-><init>()V

    .line 46
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 47
    .local v0, "indicesMap":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Long;>;"
    const-string v1, "RememberNumberConfig"

    const-string v2, "id"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    .line 48
    const-string v1, "id"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "RememberNumberConfig"

    const-string v2, "trainingShowCount"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    .line 50
    const-string v1, "trainingShowCount"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v1, "RememberNumberConfig"

    const-string v2, "minComplexity"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    .line 52
    const-string v1, "minComplexity"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    const-string v1, "RememberNumberConfig"

    const-string v2, "maxComplexity"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    .line 54
    const-string v1, "maxComplexity"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    const-string v1, "RememberNumberConfig"

    const-string v2, "complexity"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    .line 56
    const-string v1, "complexity"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v1, "RememberNumberConfig"

    const-string v2, "answersToComplexityUp"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    .line 58
    const-string v1, "answersToComplexityUp"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v1, "RememberNumberConfig"

    const-string v2, "answersToComplexityDown"

    invoke-virtual {p0, p1, p2, v1, v2}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getValidColumnIndex(Ljava/lang/String;Lio/realm/internal/Table;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    .line 60
    const-string v1, "answersToComplexityDown"

    iget-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    invoke-virtual {p0, v0}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 63
    return-void
.end method


# virtual methods
.method public final clone()Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Lio/realm/internal/ColumnInfo;->clone()Lio/realm/internal/ColumnInfo;

    move-result-object v0

    check-cast v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    return-object v0
.end method

.method public bridge synthetic clone()Lio/realm/internal/ColumnInfo;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->clone()Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/CloneNotSupportedException;
        }
    .end annotation

    .prologue
    .line 34
    invoke-virtual {p0}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->clone()Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    move-result-object v0

    return-object v0
.end method

.method public final copyColumnInfoFrom(Lio/realm/internal/ColumnInfo;)V
    .locals 4
    .param p1, "other"    # Lio/realm/internal/ColumnInfo;

    .prologue
    .line 67
    move-object v0, p1

    check-cast v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;

    .line 68
    .local v0, "otherInfo":Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->idIndex:J

    .line 69
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->trainingShowCountIndex:J

    .line 70
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->minComplexityIndex:J

    .line 71
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->maxComplexityIndex:J

    .line 72
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->complexityIndex:J

    .line 73
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityUpIndex:J

    .line 74
    iget-wide v2, v0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    iput-wide v2, p0, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->answersToComplexityDownIndex:J

    .line 76
    invoke-virtual {v0}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->getIndicesMap()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {p0, v1}, Lio/realm/RememberNumberConfigRealmProxy$RememberNumberConfigColumnInfo;->setIndicesMap(Ljava/util/Map;)V

    .line 77
    return-void
.end method
